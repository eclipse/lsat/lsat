/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MPA</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPA()
 * @model
 * @generated
 */
public interface MPA extends Graph<MPAState, MPATransition> {
} // MPA
