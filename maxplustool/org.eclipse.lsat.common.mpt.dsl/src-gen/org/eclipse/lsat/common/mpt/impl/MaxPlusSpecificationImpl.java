/**
 */
package org.eclipse.lsat.common.mpt.impl;

import java.util.Collection;

import org.eclipse.lsat.common.mpt.Graph;
import org.eclipse.lsat.common.mpt.MPTPackage;
import org.eclipse.lsat.common.mpt.Matrix;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.lsat.common.mpt.Event;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Max Plus Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl#getMatrices <em>Matrices</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl#getGraphs <em>Graphs</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl#getControllable <em>Controllable</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl#getUncontrollable <em>Uncontrollable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MaxPlusSpecificationImpl extends MinimalEObjectImpl.Container implements MaxPlusSpecification {
	/**
	 * The cached value of the '{@link #getMatrices() <em>Matrices</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatrices()
	 * @generated
	 * @ordered
	 */
	protected EList<Matrix> matrices;

	/**
	 * The cached value of the '{@link #getGraphs() <em>Graphs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphs()
	 * @generated
	 * @ordered
	 */
	protected EList<Graph<?, ?>> graphs;

	/**
	 * The cached value of the '{@link #getControllable() <em>Controllable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControllable()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> controllable;

	/**
	 * The cached value of the '{@link #getUncontrollable() <em>Uncontrollable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncontrollable()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> uncontrollable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaxPlusSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MPTPackage.Literals.MAX_PLUS_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Matrix> getMatrices() {
		if (matrices == null) {
			matrices = new EObjectContainmentEList<Matrix>(Matrix.class, this, MPTPackage.MAX_PLUS_SPECIFICATION__MATRICES);
		}
		return matrices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Graph<?, ?>> getGraphs() {
		if (graphs == null) {
			graphs = new EObjectContainmentEList<Graph<?, ?>>(Graph.class, this, MPTPackage.MAX_PLUS_SPECIFICATION__GRAPHS);
		}
		return graphs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Event> getControllable() {
		if (controllable == null) {
			controllable = new EObjectContainmentEList<Event>(Event.class, this, MPTPackage.MAX_PLUS_SPECIFICATION__CONTROLLABLE);
		}
		return controllable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Event> getUncontrollable() {
		if (uncontrollable == null) {
			uncontrollable = new EObjectContainmentEList<Event>(Event.class, this, MPTPackage.MAX_PLUS_SPECIFICATION__UNCONTROLLABLE);
		}
		return uncontrollable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MPTPackage.MAX_PLUS_SPECIFICATION__MATRICES:
				return ((InternalEList<?>)getMatrices()).basicRemove(otherEnd, msgs);
			case MPTPackage.MAX_PLUS_SPECIFICATION__GRAPHS:
				return ((InternalEList<?>)getGraphs()).basicRemove(otherEnd, msgs);
			case MPTPackage.MAX_PLUS_SPECIFICATION__CONTROLLABLE:
				return ((InternalEList<?>)getControllable()).basicRemove(otherEnd, msgs);
			case MPTPackage.MAX_PLUS_SPECIFICATION__UNCONTROLLABLE:
				return ((InternalEList<?>)getUncontrollable()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MPTPackage.MAX_PLUS_SPECIFICATION__MATRICES:
				return getMatrices();
			case MPTPackage.MAX_PLUS_SPECIFICATION__GRAPHS:
				return getGraphs();
			case MPTPackage.MAX_PLUS_SPECIFICATION__CONTROLLABLE:
				return getControllable();
			case MPTPackage.MAX_PLUS_SPECIFICATION__UNCONTROLLABLE:
				return getUncontrollable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MPTPackage.MAX_PLUS_SPECIFICATION__MATRICES:
				getMatrices().clear();
				getMatrices().addAll((Collection<? extends Matrix>)newValue);
				return;
			case MPTPackage.MAX_PLUS_SPECIFICATION__GRAPHS:
				getGraphs().clear();
				getGraphs().addAll((Collection<? extends Graph<?, ?>>)newValue);
				return;
			case MPTPackage.MAX_PLUS_SPECIFICATION__CONTROLLABLE:
				getControllable().clear();
				getControllable().addAll((Collection<? extends Event>)newValue);
				return;
			case MPTPackage.MAX_PLUS_SPECIFICATION__UNCONTROLLABLE:
				getUncontrollable().clear();
				getUncontrollable().addAll((Collection<? extends Event>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MPTPackage.MAX_PLUS_SPECIFICATION__MATRICES:
				getMatrices().clear();
				return;
			case MPTPackage.MAX_PLUS_SPECIFICATION__GRAPHS:
				getGraphs().clear();
				return;
			case MPTPackage.MAX_PLUS_SPECIFICATION__CONTROLLABLE:
				getControllable().clear();
				return;
			case MPTPackage.MAX_PLUS_SPECIFICATION__UNCONTROLLABLE:
				getUncontrollable().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MPTPackage.MAX_PLUS_SPECIFICATION__MATRICES:
				return matrices != null && !matrices.isEmpty();
			case MPTPackage.MAX_PLUS_SPECIFICATION__GRAPHS:
				return graphs != null && !graphs.isEmpty();
			case MPTPackage.MAX_PLUS_SPECIFICATION__CONTROLLABLE:
				return controllable != null && !controllable.isEmpty();
			case MPTPackage.MAX_PLUS_SPECIFICATION__UNCONTROLLABLE:
				return uncontrollable != null && !uncontrollable.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MaxPlusSpecificationImpl
