/**
 */
package org.eclipse.lsat.common.mpt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.Graph#getVertices <em>Vertices</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.Graph#getEdges <em>Edges</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getGraph()
 * @model abstract="true"
 * @generated
 */
public interface Graph<V, E> extends NamedObject {
	/**
	 * Returns the value of the '<em><b>Vertices</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Vertex}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vertices</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vertices</em>' containment reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getGraph_Vertices()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Vertex> getVertices();

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.mpt.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getGraph_Edges()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Edge> getEdges();

} // Graph
