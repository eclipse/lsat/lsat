/**
 */
package org.eclipse.lsat.common.mpt;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.mpt.MPTPackage
 * @generated
 */
public interface MPTFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MPTFactory eINSTANCE = org.eclipse.lsat.common.mpt.impl.MPTFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Matrix</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Matrix</em>'.
	 * @generated
	 */
	Matrix createMatrix();

	/**
	 * Returns a new object of class '<em>Max Plus Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Max Plus Specification</em>'.
	 * @generated
	 */
	MaxPlusSpecification createMaxPlusSpecification();

	/**
	 * Returns a new object of class '<em>FSM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM</em>'.
	 * @generated
	 */
	FSM createFSM();

	/**
	 * Returns a new object of class '<em>FSM State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM State</em>'.
	 * @generated
	 */
	FSMState createFSMState();

	/**
	 * Returns a new object of class '<em>FSM Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FSM Transition</em>'.
	 * @generated
	 */
	FSMTransition createFSMTransition();

	/**
	 * Returns a new object of class '<em>MPS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPS</em>'.
	 * @generated
	 */
	MPS createMPS();

	/**
	 * Returns a new object of class '<em>MPS Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPS Configuration</em>'.
	 * @generated
	 */
	MPSConfiguration createMPSConfiguration();

	/**
	 * Returns a new object of class '<em>MPS Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPS Transition</em>'.
	 * @generated
	 */
	MPSTransition createMPSTransition();

	/**
	 * Returns a new object of class '<em>MPA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPA</em>'.
	 * @generated
	 */
	MPA createMPA();

	/**
	 * Returns a new object of class '<em>MPA State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPA State</em>'.
	 * @generated
	 */
	MPAState createMPAState();

	/**
	 * Returns a new object of class '<em>MPA Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MPA Transition</em>'.
	 * @generated
	 */
	MPATransition createMPATransition();

	/**
	 * Returns a new object of class '<em>Row Vector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Row Vector</em>'.
	 * @generated
	 */
	RowVector createRowVector();

	/**
	 * Returns a new object of class '<em>Column Vector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Column Vector</em>'.
	 * @generated
	 */
	ColumnVector createColumnVector();

	/**
	 * Returns a new object of class '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event</em>'.
	 * @generated
	 */
	Event createEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MPTPackage getMPTPackage();

} //MPTFactory
