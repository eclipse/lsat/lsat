/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MPS Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPSConfiguration#getState <em>State</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPSConfiguration#getColumns <em>Columns</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPSConfiguration()
 * @model
 * @generated
 */
public interface MPSConfiguration extends Vertex {
	/**
	 * Returns the value of the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference.
	 * @see #setState(FSMState)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPSConfiguration_State()
	 * @model required="true"
	 * @generated
	 */
	FSMState getState();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPSConfiguration#getState <em>State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(FSMState value);

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Columns</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' containment reference.
	 * @see #setColumns(ColumnVector)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPSConfiguration_Columns()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ColumnVector getColumns();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPSConfiguration#getColumns <em>Columns</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Columns</em>' containment reference.
	 * @see #getColumns()
	 * @generated
	 */
	void setColumns(ColumnVector value);

} // MPSConfiguration
