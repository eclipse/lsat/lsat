/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.FSMState#isMarked <em>Marked</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getFSMState()
 * @model
 * @generated
 */
public interface FSMState extends Vertex {

	/**
	 * Returns the value of the '<em><b>Marked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Marked</em>' attribute.
	 * @see #setMarked(boolean)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getFSMState_Marked()
	 * @model
	 * @generated
	 */
	boolean isMarked();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.FSMState#isMarked <em>Marked</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Marked</em>' attribute.
	 * @see #isMarked()
	 * @generated
	 */
	void setMarked(boolean value);
} // FSMState
