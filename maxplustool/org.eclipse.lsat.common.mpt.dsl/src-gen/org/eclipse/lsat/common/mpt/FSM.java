/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.FSM#getInitial <em>Initial</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.FSM#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getFSM()
 * @model
 * @generated
 */
public interface FSM extends Graph<FSMState, FSMTransition> {
	/**
	 * Returns the value of the '<em><b>Initial</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' reference.
	 * @see #setInitial(FSMState)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getFSM_Initial()
	 * @model required="true"
	 * @generated
	 */
	FSMState getInitial();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.FSM#getInitial <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial</em>' reference.
	 * @see #getInitial()
	 * @generated
	 */
	void setInitial(FSMState value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.lsat.common.mpt.FSMType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.lsat.common.mpt.FSMType
	 * @see #setType(FSMType)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getFSM_Type()
	 * @model
	 * @generated
	 */
	FSMType getType();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.FSM#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipse.lsat.common.mpt.FSMType
	 * @see #getType()
	 * @generated
	 */
	void setType(FSMType value);

} // FSM
