/**
 */
package org.eclipse.lsat.common.mpt.impl;

import org.eclipse.lsat.common.mpt.MPA;
import org.eclipse.lsat.common.mpt.MPAState;
import org.eclipse.lsat.common.mpt.MPATransition;
import org.eclipse.lsat.common.mpt.MPTPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MPA</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MPAImpl extends GraphImpl<MPAState, MPATransition> implements MPA {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MPTPackage.Literals.MPA;
	}

} //MPAImpl
