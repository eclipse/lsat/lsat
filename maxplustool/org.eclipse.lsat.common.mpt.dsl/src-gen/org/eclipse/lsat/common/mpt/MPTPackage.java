/**
 */
package org.eclipse.lsat.common.mpt;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.mpt.MPTFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 * @generated
 */
public interface MPTPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mpt";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/lsat/mpt";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mpt";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MPTPackage eINSTANCE = org.eclipse.lsat.common.mpt.impl.MPTPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.VectorImpl <em>Vector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.VectorImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getVector()
	 * @generated
	 */
	int VECTOR = 0;

	/**
	 * The feature id for the '<em><b>Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VECTOR__VALUES = 0;

	/**
	 * The number of structural features of the '<em>Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VECTOR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VECTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.NamedObject <em>Named Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.NamedObject
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getNamedObject()
	 * @generated
	 */
	int NAMED_OBJECT = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_OBJECT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_OBJECT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_OBJECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MatrixImpl <em>Matrix</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MatrixImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMatrix()
	 * @generated
	 */
	int MATRIX = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATRIX__NAME = NAMED_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Rows</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATRIX__ROWS = NAMED_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Matrix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATRIX_FEATURE_COUNT = NAMED_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATRIX___GET_VALUE__INT_INT = NAMED_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Matrix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATRIX_OPERATION_COUNT = NAMED_OBJECT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl <em>Max Plus Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMaxPlusSpecification()
	 * @generated
	 */
	int MAX_PLUS_SPECIFICATION = 2;

	/**
	 * The feature id for the '<em><b>Matrices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_PLUS_SPECIFICATION__MATRICES = 0;

	/**
	 * The feature id for the '<em><b>Graphs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_PLUS_SPECIFICATION__GRAPHS = 1;

	/**
	 * The feature id for the '<em><b>Controllable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_PLUS_SPECIFICATION__CONTROLLABLE = 2;

	/**
	 * The feature id for the '<em><b>Uncontrollable</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_PLUS_SPECIFICATION__UNCONTROLLABLE = 3;

	/**
	 * The number of structural features of the '<em>Max Plus Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_PLUS_SPECIFICATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Max Plus Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_PLUS_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.GraphImpl <em>Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.GraphImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getGraph()
	 * @generated
	 */
	int GRAPH = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH__NAME = NAMED_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Vertices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH__VERTICES = NAMED_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH__EDGES = NAMED_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_FEATURE_COUNT = NAMED_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPH_OPERATION_COUNT = NAMED_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.FSMImpl <em>FSM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.FSMImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSM()
	 * @generated
	 */
	int FSM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__NAME = GRAPH__NAME;

	/**
	 * The feature id for the '<em><b>Vertices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__VERTICES = GRAPH__VERTICES;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__EDGES = GRAPH__EDGES;

	/**
	 * The feature id for the '<em><b>Initial</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__INITIAL = GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__TYPE = GRAPH_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>FSM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FEATURE_COUNT = GRAPH_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>FSM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_OPERATION_COUNT = GRAPH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.VertexImpl <em>Vertex</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.VertexImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getVertex()
	 * @generated
	 */
	int VERTEX = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTEX__NAME = NAMED_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTEX__OUTGOING = NAMED_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTEX__INCOMING = NAMED_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Vertex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTEX_FEATURE_COUNT = NAMED_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Vertex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTEX_OPERATION_COUNT = NAMED_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.FSMStateImpl <em>FSM State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.FSMStateImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSMState()
	 * @generated
	 */
	int FSM_STATE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__NAME = VERTEX__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__OUTGOING = VERTEX__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__INCOMING = VERTEX__INCOMING;

	/**
	 * The feature id for the '<em><b>Marked</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE__MARKED = VERTEX_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE_FEATURE_COUNT = VERTEX_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>FSM State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_STATE_OPERATION_COUNT = VERTEX_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.EdgeImpl <em>Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.EdgeImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getEdge()
	 * @generated
	 */
	int EDGE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__NAME = NAMED_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__SOURCE = NAMED_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TARGET = NAMED_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_FEATURE_COUNT = NAMED_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_OPERATION_COUNT = NAMED_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.FSMTransitionImpl <em>FSM Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.FSMTransitionImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSMTransition()
	 * @generated
	 */
	int FSM_TRANSITION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRANSITION__NAME = EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRANSITION__SOURCE = EDGE__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRANSITION__TARGET = EDGE__TARGET;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRANSITION__MATRIX = EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>FSM Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRANSITION_FEATURE_COUNT = EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>FSM Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_TRANSITION_OPERATION_COUNT = EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MPSImpl <em>MPS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MPSImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPS()
	 * @generated
	 */
	int MPS = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS__NAME = GRAPH__NAME;

	/**
	 * The feature id for the '<em><b>Vertices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS__VERTICES = GRAPH__VERTICES;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS__EDGES = GRAPH__EDGES;

	/**
	 * The number of structural features of the '<em>MPS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_FEATURE_COUNT = GRAPH_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>MPS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_OPERATION_COUNT = GRAPH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MPSConfigurationImpl <em>MPS Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MPSConfigurationImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPSConfiguration()
	 * @generated
	 */
	int MPS_CONFIGURATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_CONFIGURATION__NAME = VERTEX__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_CONFIGURATION__OUTGOING = VERTEX__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_CONFIGURATION__INCOMING = VERTEX__INCOMING;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_CONFIGURATION__STATE = VERTEX_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_CONFIGURATION__COLUMNS = VERTEX_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>MPS Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_CONFIGURATION_FEATURE_COUNT = VERTEX_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>MPS Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_CONFIGURATION_OPERATION_COUNT = VERTEX_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MPSTransitionImpl <em>MPS Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MPSTransitionImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPSTransition()
	 * @generated
	 */
	int MPS_TRANSITION = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_TRANSITION__NAME = EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_TRANSITION__SOURCE = EDGE__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_TRANSITION__TARGET = EDGE__TARGET;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_TRANSITION__DURATION = EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reward</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_TRANSITION__REWARD = EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>MPS Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_TRANSITION_FEATURE_COUNT = EDGE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>MPS Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPS_TRANSITION_OPERATION_COUNT = EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MPAImpl <em>MPA</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MPAImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPA()
	 * @generated
	 */
	int MPA = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA__NAME = GRAPH__NAME;

	/**
	 * The feature id for the '<em><b>Vertices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA__VERTICES = GRAPH__VERTICES;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA__EDGES = GRAPH__EDGES;

	/**
	 * The number of structural features of the '<em>MPA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_FEATURE_COUNT = GRAPH_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>MPA</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_OPERATION_COUNT = GRAPH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MPAStateImpl <em>MPA State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MPAStateImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPAState()
	 * @generated
	 */
	int MPA_STATE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__NAME = VERTEX__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__OUTGOING = VERTEX__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__INCOMING = VERTEX__INCOMING;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__STATE = VERTEX_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Matrix Row Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__MATRIX_ROW_INDEX = VERTEX_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Matrix Column Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__MATRIX_COLUMN_INDEX = VERTEX_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Matrix</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__MATRIX = VERTEX_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE__VALUE = VERTEX_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>MPA State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE_FEATURE_COUNT = VERTEX_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>MPA State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_STATE_OPERATION_COUNT = VERTEX_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.MPATransitionImpl <em>MPA Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MPATransitionImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPATransition()
	 * @generated
	 */
	int MPA_TRANSITION = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_TRANSITION__NAME = EDGE__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_TRANSITION__SOURCE = EDGE__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_TRANSITION__TARGET = EDGE__TARGET;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_TRANSITION__DURATION = EDGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reward</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_TRANSITION__REWARD = EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>MPA Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_TRANSITION_FEATURE_COUNT = EDGE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>MPA Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPA_TRANSITION_OPERATION_COUNT = EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.RowVectorImpl <em>Row Vector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.RowVectorImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getRowVector()
	 * @generated
	 */
	int ROW_VECTOR = 16;

	/**
	 * The feature id for the '<em><b>Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_VECTOR__VALUES = VECTOR__VALUES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_VECTOR__NAME = VECTOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Row Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_VECTOR_FEATURE_COUNT = VECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Row Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROW_VECTOR_OPERATION_COUNT = VECTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.ColumnVectorImpl <em>Column Vector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.ColumnVectorImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getColumnVector()
	 * @generated
	 */
	int COLUMN_VECTOR = 17;

	/**
	 * The feature id for the '<em><b>Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_VECTOR__VALUES = VECTOR__VALUES;

	/**
	 * The number of structural features of the '<em>Column Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_VECTOR_FEATURE_COUNT = VECTOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Column Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLUMN_VECTOR_OPERATION_COUNT = VECTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.EventImpl
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = NAMED_OBJECT__NAME;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = NAMED_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = NAMED_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.mpt.FSMType <em>FSM Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.FSMType
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSMType()
	 * @generated
	 */
	int FSM_TYPE = 19;

	/**
	 * The meta object id for the '<em>Value</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 20;


	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.Vector <em>Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vector</em>'.
	 * @see org.eclipse.lsat.common.mpt.Vector
	 * @generated
	 */
	EClass getVector();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.lsat.common.mpt.Vector#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Values</em>'.
	 * @see org.eclipse.lsat.common.mpt.Vector#getValues()
	 * @see #getVector()
	 * @generated
	 */
	EAttribute getVector_Values();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.Matrix <em>Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Matrix</em>'.
	 * @see org.eclipse.lsat.common.mpt.Matrix
	 * @generated
	 */
	EClass getMatrix();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.mpt.Matrix#getRows <em>Rows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rows</em>'.
	 * @see org.eclipse.lsat.common.mpt.Matrix#getRows()
	 * @see #getMatrix()
	 * @generated
	 */
	EReference getMatrix_Rows();

	/**
	 * Returns the meta object for the '{@link org.eclipse.lsat.common.mpt.Matrix#getValue(int, int) <em>Get Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Value</em>' operation.
	 * @see org.eclipse.lsat.common.mpt.Matrix#getValue(int, int)
	 * @generated
	 */
	EOperation getMatrix__GetValue__int_int();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification <em>Max Plus Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Max Plus Specification</em>'.
	 * @see org.eclipse.lsat.common.mpt.MaxPlusSpecification
	 * @generated
	 */
	EClass getMaxPlusSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getMatrices <em>Matrices</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Matrices</em>'.
	 * @see org.eclipse.lsat.common.mpt.MaxPlusSpecification#getMatrices()
	 * @see #getMaxPlusSpecification()
	 * @generated
	 */
	EReference getMaxPlusSpecification_Matrices();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getGraphs <em>Graphs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Graphs</em>'.
	 * @see org.eclipse.lsat.common.mpt.MaxPlusSpecification#getGraphs()
	 * @see #getMaxPlusSpecification()
	 * @generated
	 */
	EReference getMaxPlusSpecification_Graphs();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getControllable <em>Controllable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Controllable</em>'.
	 * @see org.eclipse.lsat.common.mpt.MaxPlusSpecification#getControllable()
	 * @see #getMaxPlusSpecification()
	 * @generated
	 */
	EReference getMaxPlusSpecification_Controllable();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.mpt.MaxPlusSpecification#getUncontrollable <em>Uncontrollable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Uncontrollable</em>'.
	 * @see org.eclipse.lsat.common.mpt.MaxPlusSpecification#getUncontrollable()
	 * @see #getMaxPlusSpecification()
	 * @generated
	 */
	EReference getMaxPlusSpecification_Uncontrollable();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.Graph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graph</em>'.
	 * @see org.eclipse.lsat.common.mpt.Graph
	 * @generated
	 */
	EClass getGraph();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.mpt.Graph#getVertices <em>Vertices</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vertices</em>'.
	 * @see org.eclipse.lsat.common.mpt.Graph#getVertices()
	 * @see #getGraph()
	 * @generated
	 */
	EReference getGraph_Vertices();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.mpt.Graph#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see org.eclipse.lsat.common.mpt.Graph#getEdges()
	 * @see #getGraph()
	 * @generated
	 */
	EReference getGraph_Edges();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.FSM <em>FSM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSM
	 * @generated
	 */
	EClass getFSM();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.mpt.FSM#getInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSM#getInitial()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_Initial();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.FSM#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSM#getType()
	 * @see #getFSM()
	 * @generated
	 */
	EAttribute getFSM_Type();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.FSMState <em>FSM State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM State</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSMState
	 * @generated
	 */
	EClass getFSMState();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.FSMState#isMarked <em>Marked</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Marked</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSMState#isMarked()
	 * @see #getFSMState()
	 * @generated
	 */
	EAttribute getFSMState_Marked();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.FSMTransition <em>FSM Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM Transition</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSMTransition
	 * @generated
	 */
	EClass getFSMTransition();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.mpt.FSMTransition#getMatrix <em>Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Matrix</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSMTransition#getMatrix()
	 * @see #getFSMTransition()
	 * @generated
	 */
	EReference getFSMTransition_Matrix();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.Vertex <em>Vertex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertex</em>'.
	 * @see org.eclipse.lsat.common.mpt.Vertex
	 * @generated
	 */
	EClass getVertex();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.mpt.Vertex#getOutgoing <em>Outgoing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing</em>'.
	 * @see org.eclipse.lsat.common.mpt.Vertex#getOutgoing()
	 * @see #getVertex()
	 * @generated
	 */
	EReference getVertex_Outgoing();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.mpt.Vertex#getIncoming <em>Incoming</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming</em>'.
	 * @see org.eclipse.lsat.common.mpt.Vertex#getIncoming()
	 * @see #getVertex()
	 * @generated
	 */
	EReference getVertex_Incoming();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.Edge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge</em>'.
	 * @see org.eclipse.lsat.common.mpt.Edge
	 * @generated
	 */
	EClass getEdge();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.mpt.Edge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.lsat.common.mpt.Edge#getSource()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.mpt.Edge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.lsat.common.mpt.Edge#getTarget()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Target();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.MPS <em>MPS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MPS</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPS
	 * @generated
	 */
	EClass getMPS();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.MPSConfiguration <em>MPS Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MPS Configuration</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPSConfiguration
	 * @generated
	 */
	EClass getMPSConfiguration();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.mpt.MPSConfiguration#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPSConfiguration#getState()
	 * @see #getMPSConfiguration()
	 * @generated
	 */
	EReference getMPSConfiguration_State();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.lsat.common.mpt.MPSConfiguration#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Columns</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPSConfiguration#getColumns()
	 * @see #getMPSConfiguration()
	 * @generated
	 */
	EReference getMPSConfiguration_Columns();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.MPSTransition <em>MPS Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MPS Transition</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPSTransition
	 * @generated
	 */
	EClass getMPSTransition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.MPSTransition#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPSTransition#getDuration()
	 * @see #getMPSTransition()
	 * @generated
	 */
	EAttribute getMPSTransition_Duration();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.MPSTransition#getReward <em>Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reward</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPSTransition#getReward()
	 * @see #getMPSTransition()
	 * @generated
	 */
	EAttribute getMPSTransition_Reward();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.MPA <em>MPA</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MPA</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPA
	 * @generated
	 */
	EClass getMPA();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.MPAState <em>MPA State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MPA State</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPAState
	 * @generated
	 */
	EClass getMPAState();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.mpt.MPAState#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPAState#getState()
	 * @see #getMPAState()
	 * @generated
	 */
	EReference getMPAState_State();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.MPAState#getMatrixRowIndex <em>Matrix Row Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Matrix Row Index</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPAState#getMatrixRowIndex()
	 * @see #getMPAState()
	 * @generated
	 */
	EAttribute getMPAState_MatrixRowIndex();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.MPAState#getMatrixColumnIndex <em>Matrix Column Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Matrix Column Index</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPAState#getMatrixColumnIndex()
	 * @see #getMPAState()
	 * @generated
	 */
	EAttribute getMPAState_MatrixColumnIndex();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.mpt.MPAState#getMatrix <em>Matrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Matrix</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPAState#getMatrix()
	 * @see #getMPAState()
	 * @generated
	 */
	EReference getMPAState_Matrix();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.MPAState#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPAState#getValue()
	 * @see #getMPAState()
	 * @generated
	 */
	EAttribute getMPAState_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.MPATransition <em>MPA Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MPA Transition</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPATransition
	 * @generated
	 */
	EClass getMPATransition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.MPATransition#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPATransition#getDuration()
	 * @see #getMPATransition()
	 * @generated
	 */
	EAttribute getMPATransition_Duration();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.MPATransition#getReward <em>Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reward</em>'.
	 * @see org.eclipse.lsat.common.mpt.MPATransition#getReward()
	 * @see #getMPATransition()
	 * @generated
	 */
	EAttribute getMPATransition_Reward();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.NamedObject <em>Named Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Object</em>'.
	 * @see org.eclipse.lsat.common.mpt.NamedObject
	 * @generated
	 */
	EClass getNamedObject();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.mpt.NamedObject#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.lsat.common.mpt.NamedObject#getName()
	 * @see #getNamedObject()
	 * @generated
	 */
	EAttribute getNamedObject_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.RowVector <em>Row Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Row Vector</em>'.
	 * @see org.eclipse.lsat.common.mpt.RowVector
	 * @generated
	 */
	EClass getRowVector();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.ColumnVector <em>Column Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Column Vector</em>'.
	 * @see org.eclipse.lsat.common.mpt.ColumnVector
	 * @generated
	 */
	EClass getColumnVector();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.mpt.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.eclipse.lsat.common.mpt.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.lsat.common.mpt.FSMType <em>FSM Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>FSM Type</em>'.
	 * @see org.eclipse.lsat.common.mpt.FSMType
	 * @generated
	 */
	EEnum getFSMType();

	/**
	 * Returns the meta object for data type '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Value</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="pattern=''"
	 * @generated
	 */
	EDataType getValue();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MPTFactory getMPTFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.VectorImpl <em>Vector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.VectorImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getVector()
		 * @generated
		 */
		EClass VECTOR = eINSTANCE.getVector();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VECTOR__VALUES = eINSTANCE.getVector_Values();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MatrixImpl <em>Matrix</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MatrixImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMatrix()
		 * @generated
		 */
		EClass MATRIX = eINSTANCE.getMatrix();

		/**
		 * The meta object literal for the '<em><b>Rows</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATRIX__ROWS = eINSTANCE.getMatrix_Rows();

		/**
		 * The meta object literal for the '<em><b>Get Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MATRIX___GET_VALUE__INT_INT = eINSTANCE.getMatrix__GetValue__int_int();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl <em>Max Plus Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MaxPlusSpecificationImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMaxPlusSpecification()
		 * @generated
		 */
		EClass MAX_PLUS_SPECIFICATION = eINSTANCE.getMaxPlusSpecification();

		/**
		 * The meta object literal for the '<em><b>Matrices</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAX_PLUS_SPECIFICATION__MATRICES = eINSTANCE.getMaxPlusSpecification_Matrices();

		/**
		 * The meta object literal for the '<em><b>Graphs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAX_PLUS_SPECIFICATION__GRAPHS = eINSTANCE.getMaxPlusSpecification_Graphs();

		/**
		 * The meta object literal for the '<em><b>Controllable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAX_PLUS_SPECIFICATION__CONTROLLABLE = eINSTANCE.getMaxPlusSpecification_Controllable();

		/**
		 * The meta object literal for the '<em><b>Uncontrollable</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAX_PLUS_SPECIFICATION__UNCONTROLLABLE = eINSTANCE.getMaxPlusSpecification_Uncontrollable();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.GraphImpl <em>Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.GraphImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getGraph()
		 * @generated
		 */
		EClass GRAPH = eINSTANCE.getGraph();

		/**
		 * The meta object literal for the '<em><b>Vertices</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH__VERTICES = eINSTANCE.getGraph_Vertices();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPH__EDGES = eINSTANCE.getGraph_Edges();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.FSMImpl <em>FSM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.FSMImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSM()
		 * @generated
		 */
		EClass FSM = eINSTANCE.getFSM();

		/**
		 * The meta object literal for the '<em><b>Initial</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__INITIAL = eINSTANCE.getFSM_Initial();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM__TYPE = eINSTANCE.getFSM_Type();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.FSMStateImpl <em>FSM State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.FSMStateImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSMState()
		 * @generated
		 */
		EClass FSM_STATE = eINSTANCE.getFSMState();

		/**
		 * The meta object literal for the '<em><b>Marked</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FSM_STATE__MARKED = eINSTANCE.getFSMState_Marked();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.FSMTransitionImpl <em>FSM Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.FSMTransitionImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSMTransition()
		 * @generated
		 */
		EClass FSM_TRANSITION = eINSTANCE.getFSMTransition();

		/**
		 * The meta object literal for the '<em><b>Matrix</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM_TRANSITION__MATRIX = eINSTANCE.getFSMTransition_Matrix();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.VertexImpl <em>Vertex</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.VertexImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getVertex()
		 * @generated
		 */
		EClass VERTEX = eINSTANCE.getVertex();

		/**
		 * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VERTEX__OUTGOING = eINSTANCE.getVertex_Outgoing();

		/**
		 * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VERTEX__INCOMING = eINSTANCE.getVertex_Incoming();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.EdgeImpl <em>Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.EdgeImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getEdge()
		 * @generated
		 */
		EClass EDGE = eINSTANCE.getEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__SOURCE = eINSTANCE.getEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__TARGET = eINSTANCE.getEdge_Target();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MPSImpl <em>MPS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MPSImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPS()
		 * @generated
		 */
		EClass MPS = eINSTANCE.getMPS();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MPSConfigurationImpl <em>MPS Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MPSConfigurationImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPSConfiguration()
		 * @generated
		 */
		EClass MPS_CONFIGURATION = eINSTANCE.getMPSConfiguration();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPS_CONFIGURATION__STATE = eINSTANCE.getMPSConfiguration_State();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPS_CONFIGURATION__COLUMNS = eINSTANCE.getMPSConfiguration_Columns();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MPSTransitionImpl <em>MPS Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MPSTransitionImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPSTransition()
		 * @generated
		 */
		EClass MPS_TRANSITION = eINSTANCE.getMPSTransition();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPS_TRANSITION__DURATION = eINSTANCE.getMPSTransition_Duration();

		/**
		 * The meta object literal for the '<em><b>Reward</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPS_TRANSITION__REWARD = eINSTANCE.getMPSTransition_Reward();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MPAImpl <em>MPA</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MPAImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPA()
		 * @generated
		 */
		EClass MPA = eINSTANCE.getMPA();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MPAStateImpl <em>MPA State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MPAStateImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPAState()
		 * @generated
		 */
		EClass MPA_STATE = eINSTANCE.getMPAState();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPA_STATE__STATE = eINSTANCE.getMPAState_State();

		/**
		 * The meta object literal for the '<em><b>Matrix Row Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPA_STATE__MATRIX_ROW_INDEX = eINSTANCE.getMPAState_MatrixRowIndex();

		/**
		 * The meta object literal for the '<em><b>Matrix Column Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPA_STATE__MATRIX_COLUMN_INDEX = eINSTANCE.getMPAState_MatrixColumnIndex();

		/**
		 * The meta object literal for the '<em><b>Matrix</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPA_STATE__MATRIX = eINSTANCE.getMPAState_Matrix();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPA_STATE__VALUE = eINSTANCE.getMPAState_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.MPATransitionImpl <em>MPA Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MPATransitionImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getMPATransition()
		 * @generated
		 */
		EClass MPA_TRANSITION = eINSTANCE.getMPATransition();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPA_TRANSITION__DURATION = eINSTANCE.getMPATransition_Duration();

		/**
		 * The meta object literal for the '<em><b>Reward</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPA_TRANSITION__REWARD = eINSTANCE.getMPATransition_Reward();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.NamedObject <em>Named Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.NamedObject
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getNamedObject()
		 * @generated
		 */
		EClass NAMED_OBJECT = eINSTANCE.getNamedObject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_OBJECT__NAME = eINSTANCE.getNamedObject_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.RowVectorImpl <em>Row Vector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.RowVectorImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getRowVector()
		 * @generated
		 */
		EClass ROW_VECTOR = eINSTANCE.getRowVector();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.ColumnVectorImpl <em>Column Vector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.ColumnVectorImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getColumnVector()
		 * @generated
		 */
		EClass COLUMN_VECTOR = eINSTANCE.getColumnVector();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.EventImpl
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.mpt.FSMType <em>FSM Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.FSMType
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getFSMType()
		 * @generated
		 */
		EEnum FSM_TYPE = eINSTANCE.getFSMType();

		/**
		 * The meta object literal for the '<em>Value</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.mpt.impl.MPTPackageImpl#getValue()
		 * @generated
		 */
		EDataType VALUE = eINSTANCE.getValue();

	}

} //MPTPackage
