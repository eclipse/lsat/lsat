/**
 */
package org.eclipse.lsat.common.mpt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MPS Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPSTransition#getDuration <em>Duration</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.mpt.MPSTransition#getReward <em>Reward</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPSTransition()
 * @model
 * @generated
 */
public interface MPSTransition extends Edge {
	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(double)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPSTransition_Duration()
	 * @model dataType="org.eclipse.lsat.common.mpt.Value" required="true"
	 * @generated
	 */
	double getDuration();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPSTransition#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(double value);

	/**
	 * Returns the value of the '<em><b>Reward</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reward</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reward</em>' attribute.
	 * @see #setReward(double)
	 * @see org.eclipse.lsat.common.mpt.MPTPackage#getMPSTransition_Reward()
	 * @model dataType="org.eclipse.lsat.common.mpt.Value" required="true"
	 * @generated
	 */
	double getReward();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.mpt.MPSTransition#getReward <em>Reward</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reward</em>' attribute.
	 * @see #getReward()
	 * @generated
	 */
	void setReward(double value);

} // MPSTransition
