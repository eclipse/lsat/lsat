/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.PrintToCIF;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.ludus.backend.por.ClusterPORPerformance;
import org.eclipse.lsat.common.ludus.backend.por.DependencyInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReductionAlgorithms {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReductionAlgorithms.class);

    private ReductionAlgorithms() {
        // Empty
    }

    /**
     * Given a set of FSMs calculate a reduced FSM.
     *
     * @param fsmList list of FSMs
     * @param mapping maps each event to a matrix
     * @return reduced composition of the given list of FSMs
     * @throws MaxPlusException thrown when the fsmList is empty
     */
    public static FSM<Location, Edge> computeClusterReductionPerformance(List<FSM<Location, Edge>> fsmList,
            Map<String, Matrix> mapping) throws MaxPlusException
    {
        // Pre-conditions.
        if (fsmList.isEmpty()) {
            throw new MaxPlusException("The specification contains no FSM.");
        }

        // Compute the dependency graph by looking at resource sharing.
        DependencyInterface dependencies = MatrixDependencies.getDependencyGraphResourceSharing(mapping);

        LOGGER.info("Finished calculating the dependencies.");

        ClusterPORPerformance por = new ClusterPORPerformance();
        FSM<Location, Edge> result = por.compute(fsmList, dependencies);

        LOGGER.info("Computed the reduced FSM with " + result.getVertices().size() + " vertices and "
                + result.getEdges().size() + " edges.");
        return result;
    }

    public static void writeToFile(FSM<Location, Edge> fsm, String fsmName, String filePath) throws IOException {
        File file = new File(filePath);
        file.createNewFile();
        FileWriter writer = new FileWriter(file);
        writer.write(PrintToCIF.print(fsm, fsmName));
        writer.close();
    }
}
