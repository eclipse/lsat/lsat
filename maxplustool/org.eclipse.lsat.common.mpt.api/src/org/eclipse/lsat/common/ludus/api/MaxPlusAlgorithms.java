/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api;

import java.util.Map;

import org.eclipse.lsat.common.ludus.api.algorithm.MaximumMakespanAlgorithm;
import org.eclipse.lsat.common.ludus.api.algorithm.MaximumThroughputAlgorithm;
import org.eclipse.lsat.common.ludus.api.algorithm.MinimumMakespanAlgorithm;
import org.eclipse.lsat.common.ludus.api.algorithm.MinimumThroughputAlgorithm;
import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.mpt.api.NotAllResourcesLinkedException;
import org.eclipse.lsat.common.mpt.api.UnconnectedResourceException;

/**
 * Algorithms that are provided by the max-plus toolbox.
 *
 * @author Bram van der Sanden
 * @since 2017-04-27
 */
public final class MaxPlusAlgorithms {
    private MaxPlusAlgorithms() {
        // Empty for utility classes
    }

    /**
     * Given a max-plus specification with an FSM and a timing matrix for each event, calculate the minimum makespan
     * value and the corresponding event sequence.
     *
     * @param fsm finite-state machine
     * @param matrixMap map with activity to activity matrix
     * @return minimum makespan value and corresponding event sequence
     * @throws MaxPlusException if the matrices or FSM are incorrect
     */
    public static MinimumMakespanResult calculateMinimumMakespan(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException, UnconnectedResourceException
    {
        return MinimumMakespanAlgorithm.run(fsm, matrixMap);
    }

    /**
     * Given a max-plus specification with an FSM and a timing matrix for each event, calculate the maximum makespan
     * value and the corresponding event sequence.
     *
     * @param fsm finite-state machine
     * @param matrixMap map with activity to activity matrix
     * @return maximum makespan value and corresponding event sequence
     * @throws MaxPlusException if the matrices or FSM are incorrect
     */
    public static MaximumMakespanResult calculateMaximumMakespan(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException, UnconnectedResourceException
    {
        return MaximumMakespanAlgorithm.run(fsm, matrixMap);
    }

    /**
     * Given a max-plus specification with an FSM and a timing matrix for each event, calculate the minimum (lowest,
     * worst-case) throughput value and the corresponding event sequence.
     *
     * @param fsm finite-state machine
     * @param matrixMap map with activity to activity matrix
     * @return maximum throughput value and corresponding event sequence
     * @throws MaxPlusException if the matrices or FSM are incorrect
     */
    public static MinimumThroughputResult calculateMinimumThroughput(FSM<Location, Edge> fsm,
            Map<String, Matrix> matrixMap)
            throws MaxPlusException, UnconnectedResourceException, NotAllResourcesLinkedException
    {
        return MinimumThroughputAlgorithm.runMaxPlusStateSpace(fsm, matrixMap);
    }

    /**
     * Given a max-plus specification with an FSM and a timing matrix for each event, calculate the maximum (highest,
     * best-case) throughput value and the corresponding event sequence.
     *
     * @param fsm finite-state machine
     * @param matrixMap map with activity to activity matrix
     * @return minimum throughput value and corresponding event sequence
     * @throws MaxPlusException if the matrices or FSM are incorrect
     */
    public static MaximumThroughputResult calculateMaximumThroughput(FSM<Location, Edge> fsm,
            Map<String, Matrix> matrixMap)
            throws MaxPlusException, UnconnectedResourceException, NotAllResourcesLinkedException
    {
        return MaximumThroughputAlgorithm.run(fsm, matrixMap);
    }
}
