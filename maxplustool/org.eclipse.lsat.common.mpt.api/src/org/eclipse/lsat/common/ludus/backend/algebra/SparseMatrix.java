/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.algebra;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;

/**
 * Max-plus sparse matrix.
 *
 * @author Bram van der Sanden
 */
public class SparseMatrix extends Matrix {
    private int rows;

    private int columns;

    private Map<Tuple<Integer, Integer>, Value> valueMap;

    public SparseMatrix(int rows, int columns) {
        this.columns = columns;
        this.rows = rows;
        valueMap = new HashMap<>();
    }

    @Override
    public void put(int row, int column, Value value) {
        valueMap.put(Tuple.of(row, column), value);
    }

    @Override
    public Value get(int row, int column) {
        Tuple<Integer, Integer> index = Tuple.of(row, column);
        if (valueMap.containsKey(index)) {
            return valueMap.get(index);
        } else {
            // Default value needed.
            if (row == column) {
                return new Value(0.0);
            } else {
                return Value.NEGATIVE_INFINITY;
            }
        }
    }

    @Override
    public int getColumns() {
        return columns;
    }

    @Override
    public int getRows() {
        return rows;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            sb.append("| ");
            for (int j = 0; j < columns; j++) {
                sb.append(get(i, j).toString());
                sb.append("\t");
            }
            sb.append("|\n");
        }
        return sb.toString();
    }
}
