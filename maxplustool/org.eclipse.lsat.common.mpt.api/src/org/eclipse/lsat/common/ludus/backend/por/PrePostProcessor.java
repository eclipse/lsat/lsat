/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.por;

import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.FSMImpl;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;

public class PrePostProcessor {
    public static final String BOTTOM = "_dump";

    public static final String OMEGA = "_omega";

    private PrePostProcessor() {
        // Empty
    }

    /**
     * Add an omega-selfloop to each marked state in the given automaton.
     *
     * @param fsm input finite state machine
     * @return copy of the fsm where each marked state has a self-loop with an omega event.
     */
    public static FSMImpl addOmegaLoops(FSMImpl fsm) {
        FSMImpl newFSM = FSMImpl.clone(fsm);
        for (Location m: newFSM.getMarkedVertices()) {
            newFSM.addEdge(m, m, OMEGA);
            // Add _omega to the FSM as controllable event.
            newFSM.addControllable(OMEGA);
        }
        return newFSM;
    }

    /**
     * Add an omega-selfloop to each marked state in the given automaton.
     *
     * @param fsm input finite state machine
     * @return copy of the fsm where each marked state has a self-loop with an omega event.
     */
    public static FSMImpl removeOmegaLoops(FSMImpl fsm) {
        FSMImpl newFSM = FSMImpl.clone(fsm);
        for (Location m: newFSM.getMarkedVertices()) {
            newFSM.removeEdge(m, m, OMEGA);
        }
        // Remove _omega from the FSM
        newFSM.removeControllable(OMEGA);

        return newFSM;
    }

    /**
     * Plantify a given requirement automaton. This transformation preserves controllability.
     *
     * @param fsm input finite state machine
     * @return plantified automaton
     */
    public static FSMImpl plantify(FSMImpl fsm) {
        FSMImpl newFSM = FSMImpl.clone(fsm);
        Set<String> uEvents = newFSM.getUncontrollable();

        // Add the dump state with the self loop.
        Location dump = new Location(BOTTOM);
        newFSM.addLocation(dump);
        for (String event: newFSM.getAlphabet()) {
            newFSM.addEdge(dump, dump, event);
        }

        // For each location, if there is no edge for some uncontrollable event
        // add it to the dump state.
        for (Location l: newFSM.getVertices()) {
            for (String uEvent: uEvents) {
                Set<String> outgoingEvents = newFSM.outgoingEdgesOf(l).stream().map(Edge::getEvent)
                        .collect(Collectors.toSet());
                if (!outgoingEvents.contains(uEvent)) {
                    // Create an edge to the dump state.
                    newFSM.addEdge(l, dump, uEvent);
                }
            }
        }

        return newFSM;
    }
}
