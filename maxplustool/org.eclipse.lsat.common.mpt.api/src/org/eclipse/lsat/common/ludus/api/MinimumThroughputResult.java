/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api;

import java.util.List;

/**
 * Minimum throughput result: throughput and the events on the corresponding cycle.
 *
 * @author Bram van der Sanden
 */
public class MinimumThroughputResult {
    private final double throughput;

    private final List<String> events;

    public MinimumThroughputResult(double throughput, List<String> events) {
        this.throughput = throughput;
        this.events = events;
    }

    public double getThroughput() {
        return throughput;
    }

    public List<String> getEvents() {
        return events;
    }

    @Override
    public String toString() {
        return "MinimumThroughputResult [throughput=" + throughput + ", events=" + events + "]";
    }
}
