/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api.algorithm;

import java.util.Collection;
import java.util.Map;

import org.eclipse.lsat.common.ludus.api.MaxPlusException;
import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.algorithms.CycleCheck;
import org.eclipse.lsat.common.ludus.backend.algorithms.SimpleCyclesResourceCheck;
import org.eclipse.lsat.common.ludus.backend.algorithms.SimpleCyclesResourceCheckResult;
import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.mpt.api.NotAllResourcesLinkedException;

public class MaxPlusAlgorithm {
    protected MaxPlusAlgorithm() {
        // Empty
    }

    protected static <V, E> void checkNotCyclic(FSM<V, E> fsm) throws MaxPlusException {
        // Check if the FSM is cyclic.
        if (CycleCheck.check(fsm)) {
            throw new MaxPlusException("Finite-state machine contains a cycle.");
        }
    }

    protected static <V, E> void checkCyclic(FSM<V, E> fsm) throws MaxPlusException {
        // Check if the FSM is cyclic.
        if (!CycleCheck.check(fsm)) {
            throw new MaxPlusException("Finite-state machine does not contain a cycle.");
        }
    }

    protected static <V, E> void checkNoDeadlocks(FSM<V, E> fsm) throws MaxPlusException {
        // Check if there are no deadlocks.
        for (V l: fsm.getVertices()) {
            if (fsm.outgoingEdgesOf(l).isEmpty()) {
                throw new MaxPlusException("Location " + l.toString() + " has no outgoing edges.");
            }
        }
    }

    /**
     * Check that all the matrices have the same number of rows and columns.
     *
     * @param matrixCollection collection of matrices to compare
     * @throws MaxPlusException thrown when matrices do not have the same size
     */
    protected static void checkAllMatricesSameSize(Collection<Matrix> matrixCollection) throws MaxPlusException {
        Matrix first = matrixCollection.iterator().next();
        int rows = first.getRows();
        int columns = first.getColumns();

        for (Matrix m: matrixCollection) {
            if (m.getRows() != rows) {
                throw new MaxPlusException("Matrices have different row sizes.");
            }
            if (m.getColumns() != columns) {
                throw new MaxPlusException("Matrices have different column sizes.");
            }
        }
    }

    protected static void checkEventMapping(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException
    {
        for (Edge e: fsm.getEdges()) {
            if (!matrixMap.containsKey(e.getEvent())) {
                throw new MaxPlusException("No matrix found for event " + e.getEvent());
            }
        }
    }

    /**
     * Check that all resources are used on each cycle in the FSM.
     *
     * @throws NotAllResourcesLinkedException
     */
    protected static void checkResourcesConnected(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws NotAllResourcesLinkedException
    {
        SimpleCyclesResourceCheck simpleCycleCheck = new SimpleCyclesResourceCheck();
        SimpleCyclesResourceCheckResult result = simpleCycleCheck.check(fsm, matrixMap);
        if (!result.isAllResourcesUsed()) {
            throw new NotAllResourcesLinkedException(result.getSimpleCycle(), result.getMissingClaimPairs());
        }
    }
}
