/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.ratio.solvers.energy;

import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.games.ratio.RatioGame;

/**
 * Ratio game interface for energy games that also allows the construction of subgraphs.
 *
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 * @param <T> edge weight type
 */
public interface RatioGameEnergy<V, E, T> extends RatioGame<V, E, T> {
    RatioGameEnergy<V, E, T> getSubGraph(Set<V> vertexSubset);

    RatioGameEnergy<V, E, T> getSwappedSubGraph(Set<V> vertexSubset);
}
