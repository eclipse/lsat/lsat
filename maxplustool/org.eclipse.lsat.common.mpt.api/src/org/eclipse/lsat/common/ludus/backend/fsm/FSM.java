/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.fsm;

import java.util.Collection;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.graph.Graph;

/**
 * Finite-state machine with marked states and controllable and uncontrollable events.
 *
 * @author Bram van der Sanden
 */
public interface FSM<V, E> extends Graph<V, E> {
    Set<String> getControllable();

    Set<String> getUncontrollable();

    V getInitial();

    String getEvent(E e);

    Set<String> getAlphabet();

    boolean hasEdge(V source, V target, String event);

    /**
     * Return all edges from source to target.
     *
     * @param source source vertex
     * @param target target vertex
     * @return all edges from source to target
     */
    Collection<E> getEdges(V source, V target);

    E getEdge(V source, V target, String event);

    boolean isMarked(V v);
}
