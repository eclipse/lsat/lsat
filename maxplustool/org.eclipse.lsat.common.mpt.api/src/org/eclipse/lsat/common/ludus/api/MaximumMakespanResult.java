/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api;

import java.util.List;

/**
 * Maximum makespan result: makespan and the events on the corresponding trace.
 *
 * @author Bram van der Sanden
 */
public class MaximumMakespanResult {
    private final double makespan;

    private final List<String> events;

    public MaximumMakespanResult(double makespan, List<String> events) {
        this.makespan = makespan;
        this.events = events;
    }

    public double getMakespan() {
        return makespan;
    }

    public List<String> getEvents() {
        return events;
    }

    @Override
    public String toString() {
        return "MaximumMakespanResult [makespan=" + makespan + ", events=" + events + "]";
    }
}
