/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.graph.simple_double;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.graph.DoubleWeightedGraph;

/**
 * A graph implementation using an edge set, vertex set. Each vertex stores the incoming and outgoing edges. Each edge
 * stores the two weights.
 *
 * @author Bram van der Sanden
 */
public class SDGraph implements DoubleWeightedGraph<SDVertex, SDEdge, Double> {
    private static final long serialVersionUID = 1L;

    private final Set<SDVertex> vertexSet;

    private final Set<SDEdge> edgeSet;

    public SDGraph() {
        vertexSet = new HashSet<>();
        edgeSet = new HashSet<>();
    }

    @Override
    public Set<SDEdge> getEdges() {
        return edgeSet;
    }

    public void addVertex(SDVertex vertex) {
        vertexSet.add(vertex);
    }

    public SDEdge addEdge(SDVertex source, SDVertex target, Double w1, Double w2) {
        SDEdge e = new SDEdge(source, target, w1, w2);
        source.addOutgoing(e);
        target.addIncoming(e);
        edgeSet.add(e);
        return e;
    }

    @Override
    public Set<SDVertex> getVertices() {
        return vertexSet;
    }

    @Override
    public Collection<SDEdge> incomingEdgesOf(SDVertex v) {
        return v.getIncoming();
    }

    @Override
    public Collection<SDEdge> outgoingEdgesOf(SDVertex v) {
        return v.getOutgoing();
    }

    @Override
    public SDVertex getEdgeSource(SDEdge e) {
        return e.getSource();
    }

    @Override
    public SDVertex getEdgeTarget(SDEdge e) {
        return e.getTarget();
    }

    @Override
    public SDEdge getEdge(SDVertex source, SDVertex target) {
        return source.getOutgoing(target);
    }

    @Override
    public Double getWeight1(SDEdge edge) {
        return edge.getWeight1();
    }

    @Override
    public Double getWeight2(SDEdge edge) {
        return edge.getWeight2();
    }
}
