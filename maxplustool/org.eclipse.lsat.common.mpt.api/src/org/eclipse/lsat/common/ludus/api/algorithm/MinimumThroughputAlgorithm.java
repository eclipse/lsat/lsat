/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api.algorithm;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.ludus.api.MaxPlusException;
import org.eclipse.lsat.common.ludus.api.MinimumThroughputResult;
import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.algorithms.Howard;
import org.eclipse.lsat.common.ludus.backend.automaton.ComputeMPA;
import org.eclipse.lsat.common.ludus.backend.automaton.MPATransition;
import org.eclipse.lsat.common.ludus.backend.automaton.MaxPlusAutomaton;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.ludus.backend.statespace.ComputeStateSpace;
import org.eclipse.lsat.common.ludus.backend.statespace.MaxPlusStateSpace;
import org.eclipse.lsat.common.ludus.backend.statespace.Transition;
import org.eclipse.lsat.common.mpt.api.NotAllResourcesLinkedException;
import org.eclipse.lsat.common.mpt.api.UnconnectedResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Compute the minimum (worst-case) throughput using Howard's minimum cycle ratio algorithm.
 *
 */
public class MinimumThroughputAlgorithm extends MaxPlusAlgorithm {
    private static final Logger LOGGER = LoggerFactory.getLogger(MinimumThroughputAlgorithm.class);

    private static void runChecks(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException, NotAllResourcesLinkedException
    {
        checkCyclic(fsm);
        checkNoDeadlocks(fsm);
        checkAllMatricesSameSize(matrixMap.values());
        // checkResourcesConnected(fsm,matrixMap);
    }

    public static MinimumThroughputResult runMaxPlusStateSpace(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException, NotAllResourcesLinkedException, UnconnectedResourceException
    {
        runChecks(fsm, matrixMap);

        // Number of resources.
        Integer resourceCount = matrixMap.values().iterator().next().getRows();

        // Compute the max-plus state space.
        MaxPlusStateSpace mpss = ComputeStateSpace.computeMaxPlusStateSpace(fsm, resourceCount, matrixMap);

        LOGGER.info("Max-plus state space constructed: " + mpss.getVertices().size() + " states and "
                + mpss.getEdges().size() + " edges.");

        // Perform the minimum cycle mean computation on the SCCs.
        List<MaxPlusStateSpace> mpssSCCs = ComputeStateSpace.getSCCs(mpss);

        LOGGER.info("Computed the " + mpssSCCs.size() + " strongly connected components.");

        Tuple<Value, List<Transition>> result = Tuple.of(Value.POSITIVE_INFINITY, new LinkedList<Transition>());
        int i = 1;
        for (MaxPlusStateSpace mpssSCC: mpssSCCs) {
            Tuple<Value, List<Transition>> sccResult = Howard.runHoward(mpssSCC);
            LOGGER.info("Running Howard on component " + i + "/" + mpssSCCs.size());
            if (sccResult.getRight() == null) {
                throw new MaxPlusException("Throughput for component " + i
                        + " cannot be determined due to floating-point precision issues.");
            }

            if (sccResult.getLeft().smallerThan(result.getLeft())) {
                result = sccResult;
            }
            i++;
        }

        // Create the output.
        List<String> listOfEventNames = result.getRight().stream().map(Transition::getEvent)
                .collect(Collectors.toList());
        return new MinimumThroughputResult(result.getLeft().toDouble(), listOfEventNames);
    }

    public static MinimumThroughputResult runMaxPlusAutomaton(FSM<Location, Edge> fsm, Map<String, Matrix> matrixMap)
            throws MaxPlusException, NotAllResourcesLinkedException
    {
        runChecks(fsm, matrixMap);

        // Number of resources.
        Integer resourceCount = matrixMap.values().iterator().next().getRows();

        // Compute the max-plus automaton.
        MaxPlusAutomaton<Location> mpa = ComputeMPA.computeMaxPlusAutomaton(fsm, resourceCount, matrixMap);

        LOGGER.info("Max-plus automaton constructed: " + mpa.getVertices().size() + " states and "
                + mpa.getEdges().size() + " edges.");

        // Perform the minimum cycle mean computation on the SCCs.
        List<MaxPlusAutomaton<Location>> mpaSCCs = ComputeMPA.getSCCs(mpa);

        LOGGER.info("Computed the " + mpaSCCs.size() + " strongly connected components.");

        Tuple<Value, List<MPATransition<Location>>> result = Tuple.of(Value.POSITIVE_INFINITY, new LinkedList<>());
        int i = 1;
        for (MaxPlusAutomaton<Location> mpaSCC: mpaSCCs) {
            Tuple<Value, List<MPATransition<Location>>> sccResult = Howard.runHoward(mpaSCC);
            LOGGER.info("Running Howard on component " + i + "/" + mpaSCCs.size());
            if (sccResult.getRight() == null) {
                throw new MaxPlusException("Throughput for component " + i
                        + " cannot be determined due to floating-point precision issues.");
            }
            if (sccResult.getLeft().smallerThan(result.getLeft())) {
                result = sccResult;
            }
            i++;
        }

        // Create the output.
        List<String> listOfEventNames = result.getRight().stream().map(MPATransition::getEvent)
                .collect(Collectors.toList());
        return new MinimumThroughputResult(result.getLeft().toDouble(), listOfEventNames);
    }
}
