/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.algorithms;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.lsat.common.ludus.api.MatrixDependencies;
import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;

/**
 * Algorithm that checks whether all resources are linked in each simple cycle of the automaton. If a link is missing,
 * an exception is generated. Note that the algorithm does not consider the labeling of the transitions. With this
 * check, we can guarantee that never an infinite state space is generated.
 * <p>
 * Generation of the simple cycles is done with Tarjans algorithm, using a slightly adapted version of the
 * implementation provided by JGraphT
 * (https://jgrapht.org/javadoc/org.jgrapht.core/org/jgrapht/alg/cycle/TarjanSimpleCycles.html).
 * </p>
 *
 * @author Bram van der Sanden
 *
 */
public class SimpleCyclesResourceCheck {
    private FSM<Location, Edge> fsm;

    private Map<String, Matrix> matrices;

    private Set<Integer> usedResources = new HashSet<>();

    private Set<Location> marked;

    private ArrayDeque<Location> markedStack;

    private ArrayDeque<Location> pointStack;

    private Map<Location, Integer> vToI;

    private Map<Location, Set<Location>> removed;

    // For debugging purposes.
    // private int simpleCycleCount = 0;

    public SimpleCyclesResourceCheckResult check(FSM<Location, Edge> finiteStateMachine,
            Map<String, Matrix> matrixMap)
    {
        matrices = matrixMap;
        fsm = finiteStateMachine;

        // Compute which resources are updated in any of activities.
        for (Edge e: finiteStateMachine.getEdges()) {
            usedResources.addAll(MatrixDependencies.getUsedResources(matrices.get(e.getEvent())));
        }

        // Run Tarjan's algorithm to find the simple cycles.
        // For each simple cycle, check if all resource claims are connected.
        initState();

        for (Location start: fsm.getVertices()) {
            try {
                backtrack(start, start);
            } catch (SimpleCyclesResourceCheckException e) {
                clearState();
                return new SimpleCyclesResourceCheckResult(e.getSimpleCycle(), e.getMissingClaimPairs());
            }
            while (!markedStack.isEmpty()) {
                marked.remove(markedStack.pop());
            }
        }

        clearState();
        return new SimpleCyclesResourceCheckResult();
    }

    private boolean backtrack(Location start, Location vertex) throws SimpleCyclesResourceCheckException {
        boolean foundCycle = false;
        pointStack.push(vertex);
        marked.add(vertex);
        markedStack.push(vertex);

        for (Edge currentEdge: fsm.outgoingEdgesOf(vertex)) {
            Location currentVertex = fsm.getEdgeTarget(currentEdge);
            if (getRemoved(vertex).contains(currentVertex)) {
                continue;
            }
            int comparison = toI(currentVertex).compareTo(toI(start));
            if (comparison < 0) {
                getRemoved(vertex).add(currentVertex);
            } else if (comparison == 0) {
                // Found a new simple cycle.
                foundCycle = true;
                List<Location> cycle = new ArrayList<>();
                Iterator<Location> it = pointStack.descendingIterator();
                Location v;
                while (it.hasNext()) {
                    v = it.next();
                    if (start.equals(v)) {
                        break;
                    }
                }
                cycle.add(start);
                while (it.hasNext()) {
                    cycle.add(it.next());
                }

                // For debugging purposes.
                // System.out.println("simple cycle found " + simpleCycleCount + " " +
                // cycle.stream().map(loc -> String.valueOf(loc)).collect(Collectors.joining("->", "(", ")")));
                // simpleCycleCount += 1;

                // Check the simple cycle for linked resource claims.
                List<Tuple<Integer, Integer>> checkResult = checkSimpleCycle(cycle);

                if (!checkResult.isEmpty()) {
                    // Connections missing.
                    throw new SimpleCyclesResourceCheckException(cycle, checkResult);
                }
            } else if (!marked.contains(currentVertex)) {
                boolean gotCycle = backtrack(start, currentVertex);
                foundCycle = foundCycle || gotCycle;
            }
        }

        if (foundCycle) {
            while (!markedStack.peek().equals(vertex)) {
                marked.remove(markedStack.pop());
            }
            marked.remove(markedStack.pop());
        }

        pointStack.pop();
        return foundCycle;
    }

    private void initState() {
        marked = new HashSet<>();
        markedStack = new ArrayDeque<>();
        pointStack = new ArrayDeque<>();
        vToI = new HashMap<>();
        removed = new HashMap<>();
        int index = 0;
        for (Location v: fsm.getVertices()) {
            vToI.put(v, index++);
        }
    }

    private void clearState() {
        marked = null;
        markedStack = null;
        pointStack = null;
        vToI = null;
    }

    private Integer toI(Location v) {
        return vToI.get(v);
    }

    private Set<Location> getRemoved(Location v) {
        // Removed sets typically not all needed, so instantiate lazily.
        return removed.computeIfAbsent(v, k -> new HashSet<>());
    }

    private List<Tuple<Integer, Integer>> checkSimpleCycle(List<Location> cycle) {
        // Create matrix to store the connections; a true at [i][j] means that there is
        // a dependency from the claim of resource i to the claim of resource j.
        int resCount = matrices.values().iterator().next().getRows();
        boolean[][] connections = new boolean[resCount][resCount];

        // Traverse the simple cycle to fill the connections matrix.
        for (int i = 0; i < cycle.size(); i++) {
            Edge edge = fsm.getEdge(cycle.get(i), cycle.get((i + 1) % cycle.size()));
            Matrix matrix = matrices.get(fsm.getEvent(edge));

            for (int k = 0; k < resCount; k++) {
                for (int l = 0; l < resCount; l++) {
                    if (!connections[k][l] && !matrix.get(k, l).equals(Value.NEGATIVE_INFINITY)) {
                        connections[k][l] = true;
                    }
                }
            }
        }

        // Take the transitive closure of the matrix using the Floyd-Warshall algorithm.
        for (int k = 0; k < resCount; k++) {
            for (int i = 0; i < resCount; i++) {
                for (int j = 0; j < resCount; j++) {
                    connections[i][j] = connections[i][j] || (connections[i][k] && connections[k][j]);
                }
            }
        }

        // Check if all connections are present, for resources that are used in the FSM.
        List<Tuple<Integer, Integer>> missing = new LinkedList<>();
        for (int k = 0; k < resCount; k++) {
            for (int l = 0; l < resCount; l++) {
                if (usedResources.contains(k) && usedResources.contains(l) && !connections[k][l]) {
                    missing.add(Tuple.of(k, l));
                }
            }
        }

        return missing;
    }
}
