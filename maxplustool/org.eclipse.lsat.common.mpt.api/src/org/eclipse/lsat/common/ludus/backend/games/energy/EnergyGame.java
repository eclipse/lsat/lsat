/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.energy;

import org.eclipse.lsat.common.ludus.backend.datastructures.weights.SingleWeightFunction;
import org.eclipse.lsat.common.ludus.backend.games.GameGraph;

/**
 * Interface to access energy games.
 *
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 * @param <T> edge weight type
 */
public interface EnergyGame<V, E, T> extends GameGraph<V, E>, SingleWeightFunction<T, E> {
    T getSumNegWeights();
}
