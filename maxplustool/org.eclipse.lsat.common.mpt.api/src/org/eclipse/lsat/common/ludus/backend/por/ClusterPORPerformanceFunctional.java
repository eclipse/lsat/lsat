/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.por;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import org.eclipse.lsat.common.ludus.backend.fsm.FSM;
import org.eclipse.lsat.common.ludus.backend.fsm.FSMComposition;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.FSMImpl;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;

/**
 * Cluster partial-order reduction that preserves nonblockingness, controllability, and reduced least-restictiveness.
 * Special event omega is considered as a controllable event.
 * <p>
 * The algorithm is explained in the following journal article and report: - van der Sanden, B., Geilen, M., Reniers,
 * M., & Basten, T. (2021). Partial-order reduction for Supervisory Controller Synthesis IEEE Transactions on Automatic
 * Control, January 2022 - Bram van der Sanden, Marc Geilen, Michel Reniers, Twan Basten Partial-Order Reduction for
 * Synthesis and Performance Analysis of Supervisory Controllers. ESR-2019-02, 11 November 2019. (pdf)
 * </p>
 *
 * @author Bram van der Sanden
 */
public class ClusterPORPerformanceFunctional {
    private Map<List<Location>, Location> stateMap;

    private Integer currentStateId;

    private List<FSM<Location, Edge>> fsmList;

    private FSMImpl cFSM;

    private Set<String> alphabet;

    private Set<String> controllableEvents;

    private Set<String> uncontrollableEvents;

    private Set<Edge> visitedEdges;

    private Set<Location> visitedLocations;

    private DependencyInterface dependencies;

    private Map<String, Set<Integer>> occursInMap;

    private Map<String, Set<String>> alphabetMap;

    private Set<String> setU;

    // During exploration, we do not explore all (_dump, X, Y, _dump) states,
    // but instead use one (_dump, _dump, _dump, _dump) state to avoid an exponential state-space blow up.
    private List<Location> globalDumpState = new ArrayList<>();

    private static final String OMEGA = PrePostProcessor.OMEGA;

    public enum Approach {
        ALL_CANDIDATES_HEURISTIC, SMART_CANDIDATE_HEURISTIC, RANDOM_CANDIDATE
    }

    private final Approach approach;

    /** Heuristic for the local event check to find a smaller reduction at the cost of a larger computation time. */
    private static final boolean OPTIMIZE_LOCAL_EVENT_CHECK = true;

    public ClusterPORPerformanceFunctional() {
        this.approach = Approach.ALL_CANDIDATES_HEURISTIC;
    }

    public ClusterPORPerformanceFunctional(Approach approach) {
        this.approach = approach;
    }

    /**
     * Compute the clustered campleAll composition of the given FSMs.
     *
     * @param fsmList list of individual FSMs
     * @param dependencies map of event dependencies
     */
    public FSMImpl compute(List<FSM<Location, Edge>> fsmList, DependencyInterface dependencies) {
        stateMap = new HashMap<>();
        currentStateId = 0;
        cFSM = new FSMImpl();
        visitedEdges = new HashSet<>();
        visitedLocations = new HashSet<>();
        this.fsmList = fsmList;

        // Dependent events.
        this.dependencies = dependencies;

        // Compute the combined alphabet, controllable events, and uncontrollable events.
        alphabet = new HashSet<>();
        uncontrollableEvents = new HashSet<>();
        controllableEvents = new HashSet<>();
        for (FSM<Location, Edge> fsm: fsmList) {
            alphabet.addAll(fsm.getAlphabet());
            uncontrollableEvents.addAll(fsm.getUncontrollable());
            controllableEvents.addAll(fsm.getControllable());
        }

        // For each event, find all FSMs that have this event in their alphabet, and also compute the combined alphabet.
        occursInMap = new HashMap<>();
        alphabetMap = new HashMap<>();
        for (String event: alphabet) {
            Set<Integer> occursList = new HashSet<>();
            Set<String> alphabetList = new HashSet<>();
            // Check if FSM contains event.
            for (int fsmId = 0; fsmId < this.fsmList.size(); fsmId++) {
                FSM<Location, Edge> fsm = fsmList.get(fsmId);
                if (fsm.getAlphabet().contains(event)) {
                    occursList.add(fsmId);
                    alphabetList.addAll(fsm.getAlphabet());
                }
            }
            occursInMap.put(event, occursList);
            alphabetMap.put(event, alphabetList);
        }

        // Create the initial state.
        List<Location> initialState = new ArrayList<>();
        for (int fsmId = 0; fsmId < this.fsmList.size(); fsmId++) {
            initialState.add(fsmId, this.fsmList.get(fsmId).getInitial());
        }
        Location stateLocation = getLocation(initialState);
        cFSM.setInitial(stateLocation);

        // Create the global dump state.
        globalDumpState = new ArrayList<>();
        for (int fsmId = 0; fsmId < this.fsmList.size(); fsmId++) {
            globalDumpState.add(fsmId, new Location(PrePostProcessor.BOTTOM));
        }

        // Set the alphabet.
        for (String uEvent: uncontrollableEvents) {
            cFSM.addUncontrollable(uEvent);
        }
        for (String cEvent: controllableEvents) {
            cFSM.addControllable(cEvent);
        }

        // Compute the set of events that might enable an uncontrollable event.
        setU = new HashSet<>();

        for (int fsmId = 0; fsmId < this.fsmList.size(); fsmId++) {
            FSM<Location, Edge> fsm = this.fsmList.get(fsmId);
            for (Edge e: fsm.getEdges()) {
                Location target = fsm.getEdgeTarget(e);
                // See which events are enabled in the state reached after executing this edge.
                Set<String> enabled_in_target = FSMComposition.enabled(fsm, target);
                for (String event_enabled: enabled_in_target) {
                    if (uncontrollableEvents.contains(event_enabled)) {
                        // Edge leads to state with uncontrollable events, add the edge event.
                        setU.add(e.getEvent());
                    }
                }
            }
        }

        // Compute the composition on the fly using a DFS traversal.
        dfsTraversal(initialState);
        return cFSM;
    }

    /**
     * Iterative template method for a generic DFS traversal on the combined FSM.
     *
     * @param initialState current state in the composition
     */
    private void dfsTraversal(List<Location> initialState) {
        Stack<List<Location>> stack = new Stack<>();
        stack.push(initialState);

        while (!stack.isEmpty()) {
            List<Location> state = stack.pop();
            // Visit the current location.
            if (!isVisited(getLocation(state))) {
                visit(state);
            }

            Queue<String> queueAmple = new LinkedList<>();

            Set<String> ample;
            switch (approach) {
                case ALL_CANDIDATES_HEURISTIC:
                    ample = campleAll(fsmList, state);
                    break;
                case SMART_CANDIDATE_HEURISTIC:
                    ample = campleHeuristic(fsmList, state);
                    break;
                case RANDOM_CANDIDATE:
                    ample = campleAny(fsmList, state);
                    break;
                default:
                    ample = campleAll(fsmList, state);
                    break;
            }

            queueAmple.addAll(ample);

            while (!queueAmple.isEmpty()) {
                String a = queueAmple.remove();
                List<Location> targetState = FSMComposition.getEdgeTarget(fsmList, state, a);

                // If we observe a dump state in one of the automata, then use the global dump state.
                Location dump_state = new Location(PrePostProcessor.BOTTOM);
                if (targetState.contains(dump_state)) {
                    targetState = globalDumpState;
                }

                Edge e = new Edge(getLocation(state), a, getLocation(targetState));
                if (!isVisited(e)) {
                    // Found an unexplored edge, explore it.
                    visit(e);
                    if (!isVisited(getLocation(targetState))) {
                        stack.push(targetState);
                    }
                }
            }
        }
    }

    /**
     * Recursive template method for a generic DFS traversal on the combined FSM.
     *
     * @param state current state in the composition
     */
    @SuppressWarnings("unused")
    private void dfsTraversalRecursive(List<Location> state) {
        visit(state);

        Set<String> ample;
        switch (approach) {
            case ALL_CANDIDATES_HEURISTIC:
                ample = campleAll(fsmList, state);
                break;
            case SMART_CANDIDATE_HEURISTIC:
                ample = campleHeuristic(fsmList, state);
                break;
            case RANDOM_CANDIDATE:
                ample = campleAny(fsmList, state);
                break;
            default:
                ample = campleAll(fsmList, state);
                break;
        }

        for (String a: ample) {
            // Each event in campleAll is by definition an enabled event.
            List<Location> targetState = FSMComposition.getEdgeTarget(fsmList, state, a);

            Edge e = new Edge(getLocation(state), a, getLocation(targetState));
            if (!isVisited(e)) {
                // Found an unexplored edge, explore it.
                visit(e);
                if (!isVisited(getLocation(targetState))) {
                    dfsTraversalRecursive(targetState);
                }
            }
        }
    }

    /**
     * Try to compute the smallest ample set, otherwise return the set of all enabled events. Note that the smallest
     * ample set is a heuristic; always choosing the smallest set does not always lead to the smallest reduced state
     * space.
     *
     * @param fsmList list of individual FSMs
     * @param state current state in the composition
     * @return ample set with a subset of all enabled events in {@code state}
     */
    private Set<String> campleAll(List<FSM<Location, Edge>> fsmList, List<Location> state) {
        // Compute the enabled set enabled(c).
        Set<String> enabled = FSMComposition.enabled(fsmList, state);

        if (enabled.size() <= 1) {
            return enabled;
        }

        // Compute the smallest clusterEnabled set starting from each enabled event (fixed point algorithm).
        Set<String> clusterEnabled = new HashSet<>(enabled);
        for (String event: enabled) {
            // Compute a valid cluster.
            Set<Integer> cluster = getCluster(fsmList, state, enabled, event);

            // Compute cluster alphabet.
            Set<String> clusterAlphabet = new HashSet<>();
            for (int fsmId = 0; fsmId < fsmList.size(); fsmId++) {
                if (cluster.contains(fsmId)) {
                    clusterAlphabet.addAll(fsmList.get(fsmId).getAlphabet());
                }
            }

            // Compute clusterEnabled(c) = enabled(c) && cluster(c)
            Set<String> clusterEnabledCheck = new HashSet<>(enabled);
            clusterEnabledCheck.retainAll(clusterAlphabet);

            if (clusterEnabledCheck.size() < clusterEnabled.size()) {
                clusterEnabled = clusterEnabledCheck;
            }
        }

        return clusterEnabled;
    }

    /**
     * Try to compute a small ample set.
     *
     * @param fsmList list of individual FSMs
     * @param state current state in the composition
     * @return ample set with a subset of all enabled events in {@code state}
     */
    private Set<String> campleHeuristic(List<FSM<Location, Edge>> fsmList, List<Location> state) {
        // Compute the enabled set enabled(c).
        Set<String> enabled = FSMComposition.enabled(fsmList, state);

        if (enabled.size() <= 1) {
            return enabled;
        }

        String candidate = "_undefined";

        if (enabled.contains(OMEGA)) {
            // Check if the enabled set contains OMEGA.
            candidate = OMEGA;
        } else {
            Set<String> unctrlEnabled = new HashSet<>(enabled);
            unctrlEnabled.retainAll(uncontrollableEvents);
            if (!unctrlEnabled.isEmpty()) {
                // Check if uncontrollable events.
                candidate = unctrlEnabled.iterator().next();
            } else {
                // Check the brotherhood.
                HashMap<String, Integer> eventValueMap = new HashMap<>();

                for (String event: enabled) {
                    // Calculate the value.
                    Integer v = 0;
                    for (int fsmId = 0; fsmId < fsmList.size(); fsmId++) {
                        if (fsmList.get(fsmId).getAlphabet().contains(event)) {
                            for (String localEvent: FSMComposition.enabled(fsmList.get(fsmId), state.get(fsmId))) {
                                if (enabled.contains(localEvent)) {
                                    v += 100;
                                } else {
                                    v += 1;
                                }
                            }
                        }
                    }
                    eventValueMap.put(event, v);
                }

                Integer minVal = Integer.MAX_VALUE;
                for (String event: enabled) {
                    if (eventValueMap.get(event) < minVal) {
                        candidate = event;
                        minVal = eventValueMap.get(event);
                    }
                }
            }
        }

        // Compute a set starting from the given candidate.
        Set<Integer> cluster = getCluster(fsmList, state, enabled, candidate);

        // Compute cluster alphabet.
        Set<String> clusterAlphabet = new HashSet<>();
        for (int fsmId = 0; fsmId < fsmList.size(); fsmId++) {
            if (cluster.contains(fsmId)) {
                clusterAlphabet.addAll(fsmList.get(fsmId).getAlphabet());
            }
        }

        // Compute clusterEnabled(c) = enabled(c) && cluster(c)
        Set<String> clusterEnabledCheck = new HashSet<>(enabled);
        clusterEnabledCheck.retainAll(clusterAlphabet);

        return clusterEnabledCheck;
    }

    /**
     * Try to compute a small ample set.
     *
     * @param fsmList list of individual FSMs
     * @param state current state in the composition
     * @return ample set with a subset of all enabled events in {@code state}
     */
    private Set<String> campleAny(List<FSM<Location, Edge>> fsmList, List<Location> state) {
        // Compute the enabled set enabled(c).
        Set<String> enabled = FSMComposition.enabled(fsmList, state);

        if (enabled.size() <= 1) {
            return enabled;
        }

        String candidate = enabled.iterator().next();

        // Compute a set starting from the given candidate.
        Set<Integer> cluster = getCluster(fsmList, state, enabled, candidate);

        // Compute cluster alphabet.
        Set<String> clusterAlphabet = new HashSet<>();
        for (int fsmId = 0; fsmId < fsmList.size(); fsmId++) {
            if (cluster.contains(fsmId)) {
                clusterAlphabet.addAll(fsmList.get(fsmId).getAlphabet());
            }
        }

        // Compute clusterEnabled(c) = enabled(c) && cluster(c)
        Set<String> clusterEnabledCheck = new HashSet<>(enabled);
        clusterEnabledCheck.retainAll(clusterAlphabet);

        return clusterEnabledCheck;
    }

    /**
     * Compute a cluster starting form a candidate event. Iterate until a fixed point is reached.
     *
     * @param enabled set of all enabled events in {@code state}
     * @param candidate cluster is build starting from this candidate
     * @return cluster
     */
    private Set<Integer> getCluster(List<FSM<Location, Edge>> fsmList, List<Location> state, Set<String> enabled,
            String candidate)
    {
        Set<String> processed = new HashSet<>();
        Set<String> toProcess = new HashSet<>();
        Set<Integer> cluster = new HashSet<>();
        Set<String> clusterAlphabet = new HashSet<>();
        Set<String> locallyEnabled;

        toProcess.add(candidate);

        // Controllability: find all automata that have uncontrollable enabled events.
        Set<String> uncontrollableEnabled = new HashSet<>(enabled);
        uncontrollableEnabled.retainAll(uncontrollableEvents);

        for (String uEnabled: uncontrollableEnabled) {
            cluster.addAll(occursInMap.get(uEnabled));
            clusterAlphabet.addAll(alphabetMap.get(uEnabled));
        }

        // Nonblockingness: add all automata with the OMEGA event.
        // Note that this event MUST be in the cluster reduction, therefore we need this additional code.
        if (enabled.contains(OMEGA)) {
            cluster.addAll(occursInMap.get(OMEGA));
            clusterAlphabet.addAll(alphabetMap.get(OMEGA));
        }

        while (!toProcess.isEmpty()) {
            String event = toProcess.iterator().next();
            processed.add(event);

            // Globally enabled event.
            if (enabled.contains(event)) {
                if (setU.contains(event)) {
                    // return all automata.
                    for (Integer fsmId = 0; fsmId < fsmList.size(); fsmId++) {
                        cluster.add(fsmId);
                    }
                    return cluster;
                }

                // 1. Add all FSMs that contain this event.
                cluster.addAll(occursInMap.get(event));
                clusterAlphabet.addAll(alphabetMap.get(event));

                // 2. For each resource-dependent event, add one corresponding automaton to the cluster.
                // This means that the dependent event becomes part of the cluster.
                for (String dep: getDependencies(event)) {
                    if (!clusterAlphabet.contains(dep)) {
                        // Pick the first automaton that has event {@code dep} and add this automaton to the cluster.
                        Integer fsmId = occursInMap.get(dep).iterator().next();
                        cluster.add(fsmId);
                        clusterAlphabet.addAll(fsmList.get(fsmId).getAlphabet());
                    }
                }
            }

            // Update locally enabled information.
            locallyEnabled = getLocallyEnabled(fsmList, cluster, state);

            // Pure locally enabled event.
            if (!enabled.contains(event) && locallyEnabled.contains(event)) {
                // Find one automaton outside C that has this event, and disables it in the composition.
                // After we add this automaton, the event is no longer locally enabled.

                if (OPTIMIZE_LOCAL_EVENT_CHECK) {
                    // We try to select an automaton that does not have anything from the enabled set
                    // in it's currently enabled state, as that will lead to worse results.

                    int validFSM = -1;
                    int candidateFSM = -1;

                    for (int fsmId = 0; fsmId < fsmList.size(); fsmId++) {
                        Set<String> alphabet = fsmList.get(fsmId).getAlphabet();
                        Set<String> alphabetEnabledEvents = new HashSet<>(alphabet);
                        alphabetEnabledEvents.retainAll(enabled);

                        if (alphabet.contains(event) && !cluster.contains(fsmId)
                                && !FSMComposition.isEnabled(fsmList.get(fsmId), state.get(fsmId), event))
                        {
                            // Valid automaton
                            validFSM = fsmId;

                            // Preferred automaton
                            if (alphabetEnabledEvents.isEmpty()) {
                                candidateFSM = fsmId;
                                cluster.add(fsmId);
                                clusterAlphabet.addAll(fsmList.get(fsmId).getAlphabet());
                                break;
                            }
                        }
                    }

                    if (candidateFSM == -1) {
                        // No preferred automaton found. Add the other valid automaton.
                        cluster.add(validFSM);
                        clusterAlphabet.addAll(fsmList.get(validFSM).getAlphabet());
                    }
                } else {
                    for (int fsmId = 0; fsmId < fsmList.size(); fsmId++) {
                        if (fsmList.get(fsmId).getAlphabet().contains(event) && !cluster.contains(fsmId)
                                && !FSMComposition.isEnabled(fsmList.get(fsmId), state.get(fsmId), event))
                        {
                            cluster.add(fsmId);
                            clusterAlphabet.addAll(fsmList.get(fsmId).getAlphabet());
                            break;
                        }
                    }
                }

                // Update locally enabled information.
                locallyEnabled = getLocallyEnabled(fsmList, cluster, state);
            }

            // Continue with the remaining activities.
            Set<String> newQueue = new HashSet<>(locallyEnabled);
            newQueue.removeAll(processed);
            toProcess = newQueue;
        }
        return cluster;
    }

    /**
     * Get all the events that are dependent on {@code event}.
     *
     * @param event input event
     * @return set of all events that are dependent on input event
     */
    private Set<String> getDependencies(String event) {
        return dependencies.getDependencies(event);
    }

    /**
     * Get all events that are enabled in the cluster, without considering any FSM outside of the cluster.
     *
     * @param fsmList list of individual FSMs
     * @param cluster list of the ids of FSMs that are in the cluster
     * @param state current state in the composition
     * @return all events that are enabled in the cluster composition, not looking at FSMs outside cluster
     */
    private Set<String> getLocallyEnabled(List<FSM<Location, Edge>> fsmList, Set<Integer> cluster,
            List<Location> state)
    {
        List<FSM<Location, Edge>> clusterFSMs = new ArrayList<>();
        List<Location> clusterState = new ArrayList<>();
        // Create the FSM list and the state in the cluster.
        for (Integer fid: cluster) {
            clusterFSMs.add(fsmList.get(fid));
            clusterState.add(state.get(fid));
        }
        // Compute all events that are enabled just looking at the cluster.
        return FSMComposition.enabled(clusterFSMs, clusterState);
    }

    /**
     * Mark a state as visited, and add to the combined FSM.
     *
     * @param state current state in the composition
     */
    private void visit(List<Location> state) {
        visitedLocations.add(getLocation(state));

        Location location = stateMap.get(state);
        // Check whether the location is marked.
        if (FSMComposition.isMarked(fsmList, state)) {
            cFSM.setMarked(location);
        }

        cFSM.addLocation(location);
    }

    /**
     * Mark an edge as visited, and add to the combined FSM.
     *
     * @param e combined edge
     */
    private void visit(Edge e) {
        visitedEdges.add(e);
        cFSM.addEdge(e);
    }

    /**
     * Return a location for the given state.
     *
     * @param state current state in the composition
     * @return the location corresponding to the given state
     */
    private Location getLocation(List<Location> state) {
        if (!stateMap.containsKey(state)) {
            currentStateId++;
            stateMap.put(state, new Location("s" + currentStateId));
        }
        return stateMap.get(state);
    }

    /**
     * Test if a location in the composition has been visited.
     *
     * @param l location to test
     * @return true if the location in the composition has been visited
     */
    private boolean isVisited(Location l) {
        return visitedLocations.contains(l);
    }

    /**
     * Test if an edge in the composition has been visited.
     *
     * @param e edge to test
     * @return true if the edge in the composition has been visited
     */
    private boolean isVisited(Edge e) {
        return visitedEdges.contains(e);
    }
}
