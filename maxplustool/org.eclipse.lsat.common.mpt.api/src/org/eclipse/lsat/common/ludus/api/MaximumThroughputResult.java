/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.api;

import java.util.Collections;
import java.util.List;

/**
 * Maximum throughput result: throughput and the events on the corresponding trace.
 *
 * @author Bram van der Sanden
 */
public class MaximumThroughputResult {
    private final double throughput;

    private final List<String> transientStateEvents;

    private final List<String> steadyStateEvents;

    public MaximumThroughputResult(double throughput) {
        this.throughput = throughput;
        this.steadyStateEvents = Collections.emptyList();
        this.transientStateEvents = Collections.emptyList();
    }

    public MaximumThroughputResult(double throughput, List<String> steadyStateEvents,
            List<String> transientStateEvents)
    {
        this.throughput = throughput;
        this.steadyStateEvents = steadyStateEvents;
        this.transientStateEvents = transientStateEvents;
    }

    public double getThroughput() {
        return throughput;
    }

    public List<String> getSteadyStateEvents() {
        return steadyStateEvents;
    }

    public List<String> getTransientStateEvents() {
        return transientStateEvents;
    }

    @Override
    public String toString() {
        return "MaximumThroughputResult [throughput=" + throughput + ", steady state events=" + steadyStateEvents
                + ", transient state events=" + transientStateEvents + "]";
    }
}
