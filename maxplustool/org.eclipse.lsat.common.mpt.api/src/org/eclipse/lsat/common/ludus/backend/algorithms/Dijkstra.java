/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.algorithms;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.function.Predicate;

import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.graph.SingleWeightedGraph;

/**
 * Dijkstra's shortest path algorithm.
 *
 * @author Bram van der Sanden
 */
@SuppressWarnings("rawtypes")
public final class Dijkstra {
    private Dijkstra() {
        // Empty
    }

    private static <V, E, T> Predicate<V> predHasNoSuccessors(SingleWeightedGraph<V, E, T> graph) {
        return v -> graph.outgoingEdgesOf(v).isEmpty();
    }

    private static <V> Predicate<V> predInCollection(Collection<V> collection) {
        return collection::contains;
    }

    private static <V> Predicate<V> predEqualTo(V target) {
        return v -> v.equals(target);
    }

    /**
     * Dijkstra's shortest path algorithm. Note: algorithm assumes that the graph has at least one vertex without
     * outgoing edges.
     *
     * @param graph input graph
     * @param source source vertex
     * @param <V> vertex type
     * @param <E> edge type
     * @return shortest path from {@code source} to some vertex without outgoing edges in input graph {@code graph}.
     */
    public static <V, E> Tuple<Value, List<E>> runDijkstra(SingleWeightedGraph<V, E, Value> graph, V source) {
        return runDijkstra(graph, source, predHasNoSuccessors(graph));
    }

    /**
     * Dijkstra's shortest path algorithm.
     *
     * @param graph input graph
     * @param source source vertex
     * @param targetSet set of target vertices
     * @param <V> vertex type
     * @param <E> edge type
     * @return shortest path from {@code source} to some target in {@code targetSet} in input graph {@code graph}.
     */
    public static <V, E> Tuple<Value, List<E>> runDijkstra(SingleWeightedGraph<V, E, Value> graph, V source,
            Collection<V> targetSet)
    {
        return runDijkstra(graph, source, predInCollection(targetSet));
    }

    /**
     * Dijkstra's shortest path algorithm.
     *
     * @param graph input graph
     * @param source source vertex
     * @param target target vertex
     * @param <V> vertex type
     * @param <E> edge type
     * @return shortest path from {@code source} to {@code target} in input graph {@code graph}.
     */
    public static <V, E> Tuple<Value, List<E>> runDijkstra(SingleWeightedGraph<V, E, Value> graph, V source, V target) {
        return runDijkstra(graph, source, predEqualTo(target));
    }

    /**
     * Dijkstra's shortest path algorithm.
     *
     * @param graph input graph
     * @param source source vertex
     * @param terminationPredicate exploration stops as soon as the predicate is satisfied
     * @param <V> vertex type
     * @param <E> edge type
     * @return shortest path from {@code source} to {@code target} in input graph {@code graph}.
     */
    public static <V, E> Tuple<Value, List<E>> runDijkstra(SingleWeightedGraph<V, E, Value> graph, V source,
            Predicate<V> terminationPredicate)
    {
        Map<V, Value> dist = new HashMap<>(graph.getVertices().size());
        Map<V, V> prev = new HashMap<>(graph.getVertices().size());

        // Create a new candidate list and search tree.
        Comparator<CostVertex<V>> comparator = (CostVertex<V> v1, CostVertex<V> v2) -> (v1.cost).subtract(v2.cost)
                .signum();

        PriorityQueue<CostVertex<V>> q = new PriorityQueue<>(graph.getEdges().size(), comparator);

        Map<V, CostVertex<V>> vtoCVMap = new HashMap<>();

        // Initialization.
        dist.put(source, new Value(0.0));
        for (V v: graph.getVertices()) {
            if (!v.equals(source)) {
                dist.put(v, Value.POSITIVE_INFINITY);
                prev.put(v, null);
            }
            CostVertex<V> cv = new CostVertex<>(v, dist.get(v));
            q.add(cv);
            vtoCVMap.put(v, cv);
        }

        // Main loop.
        V target = null;
        while (!q.isEmpty()) {
            CostVertex<V> u = q.remove();
            vtoCVMap.remove(u.vertex);

            // We have found our target.
            if (terminationPredicate.test(u.vertex)) {
                target = u.vertex;
                break;
            }

            for (E e: graph.outgoingEdgesOf(u.vertex)) {
                V neighbor = graph.getEdgeTarget(e);
                if (vtoCVMap.containsKey(neighbor)) {
                    Value alt = dist.get(u.vertex).add(graph.getWeight(e));
                    if (alt.smallerThan(dist.get(neighbor))) {
                        dist.put(neighbor, alt);
                        prev.put(neighbor, u.vertex);

                        // Decrease with priority. Remove the element from the queue, update priority, and re-insert.
                        CostVertex cvNeighbor = vtoCVMap.get(neighbor);
                        q.remove(cvNeighbor);

                        CostVertex<V> costNeighbor = vtoCVMap.get(neighbor);
                        costNeighbor.cost = alt;
                        q.add(costNeighbor);
                    }
                }
            }
        }

        // Construct the shortest path from source to target.
        List<E> path = new LinkedList<>();
        V u = target;
        while (prev.containsKey(u)) {
            path.add(0, graph.getEdge(prev.get(u), u));
            u = prev.get(u);
        }

        return Tuple.of(dist.get(target), path);
    }

    /**
     * A cost vertex contains the current cost value of a given vertex.
     *
     * @param <V> vertex type
     */
    private static class CostVertex<V> {
        public final V vertex;

        public Value cost;

        /**
         * Create a new cost vertex.
         *
         * @param v vertex
         * @param cost current cost of the vertex
         */
        public CostVertex(V v, Value cost) {
            vertex = v;
            this.cost = cost;
        }
    }
}
