/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.meanpayoff.solvers.policy;

import org.eclipse.lsat.common.ludus.backend.games.VertexId;
import org.eclipse.lsat.common.ludus.backend.games.meanpayoff.MeanPayoffGame;

/**
 * Interface to access mean-payoff games for policy iteration. Here every vertex in the game graph must have a unique
 * identifier.
 *
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 * @param <T> edge weight type
 */
public interface MeanPayoffGamePolicyIteration<V, E, T> extends MeanPayoffGame<V, E, T>, VertexId<V> {

}
