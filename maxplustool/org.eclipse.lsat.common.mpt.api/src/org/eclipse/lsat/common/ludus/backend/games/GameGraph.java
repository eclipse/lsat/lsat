/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games;

import java.io.Serializable;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.graph.Graph;

/**
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 */
public interface GameGraph<V, E> extends Graph<V, E>, Serializable {
    Set<V> getV0();

    Set<V> getV1();
}
