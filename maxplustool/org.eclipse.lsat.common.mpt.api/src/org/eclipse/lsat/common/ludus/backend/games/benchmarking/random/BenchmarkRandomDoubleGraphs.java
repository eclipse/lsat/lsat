/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.benchmarking.random;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.datastructures.weights.SingleWeightFunctionDouble;
import org.eclipse.lsat.common.ludus.backend.games.StrategyVector;
import org.eclipse.lsat.common.ludus.backend.games.meanpayoff.solvers.policy.PolicyIterationDouble;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTEdge;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTGraph;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTVertex;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.meanpayoff.MPGDoubleImplJGraphT;

/**
 * @author Bram van der Sanden
 */
public class BenchmarkRandomDoubleGraphs {
    private BenchmarkRandomDoubleGraphs() {
        // Empty
    }

    public static MPGDoubleImplJGraphT constructGameGraphImpl() {
        JGraphTGraph paperGraph = new JGraphTGraph();
        Set<JGraphTVertex> list = new HashSet<>();

        int n = 10;
        int w = 100;

        for (int i = 0; i < n; i++) {
            JGraphTVertex v = new JGraphTVertex();
            list.add(v);
            if (i % 2 == 0) {
                paperGraph.addToV0(v);
            } else {
                paperGraph.addToV1(v);
            }
        }

        SingleWeightFunctionDouble<JGraphTEdge> wf = new SingleWeightFunctionDouble<>();

        Iterator<JGraphTVertex> slowI = list.iterator();
        Iterator<JGraphTVertex> fastI;

        while (slowI.hasNext()) { // While there are more vertices in the set
            JGraphTVertex latestVertex = slowI.next();
            fastI = list.iterator();
            // Jump to the first vertex *past* latestVertex
            while (fastI.next() != latestVertex) {
            }
            // And, add edges to all remaining vertices
            JGraphTVertex temp;
            while (fastI.hasNext()) {
                temp = fastI.next();
                Random r = new Random();
                Double rand1 = 1.0 + (w - 1.0) * r.nextDouble();
                Double rand2 = 1.0 + (w - 1.0) * r.nextDouble();
                addEdge(paperGraph, wf, latestVertex, temp, rand1);
                addEdge(paperGraph, wf, temp, latestVertex, rand2);
            }
        }
        return new MPGDoubleImplJGraphT(paperGraph, wf);
    }

    public static void runBenchmark() {
        for (int i = 0; i < 10; i++) {
            // Construct the game graph.
            MPGDoubleImplJGraphT game = constructGameGraphImpl();

            long start, end;

            start = System.nanoTime();
            @SuppressWarnings("unused")
            Tuple<Map<JGraphTVertex, Double>, StrategyVector<JGraphTVertex, JGraphTEdge>> resultPI = PolicyIterationDouble
                    .solve(game);
            end = System.nanoTime();
            System.out.println("Solved using policy iteration: " + ((end - start) / 1000000000.0) + " sec");
        }

        System.out.println("Finished!");
    }

    public static void main(String[] args) {
        BenchmarkRandomDoubleGraphs.runBenchmark();
    }

    private static void addEdge(JGraphTGraph g, SingleWeightFunctionDouble<JGraphTEdge> wf, JGraphTVertex src,
            JGraphTVertex target, Double weight)
    {
        JGraphTEdge e = g.addEdge(src, target);
        wf.addWeight(e, weight);
    }
}
