/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.graph;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * Graph interface.
 *
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 */
public interface Graph<V, E> extends Serializable {
    Set<V> getVertices();

    Set<E> getEdges();

    Collection<E> incomingEdgesOf(V v);

    Collection<E> outgoingEdgesOf(V v);

    V getEdgeSource(E e);

    V getEdgeTarget(E e);

    /**
     * Return an edge that connects source and target.
     *
     * @param source source vertex
     * @param target target vertex
     * @return edge that connects source and target
     */
    E getEdge(V source, V target);
}
