/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.algorithms;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;

public class SimpleCyclesResourceCheckResult {
    private final boolean allResourcesUsed;

    private final List<Location> simpleCycle;

    private final List<Tuple<Integer, Integer>> missingClaimPairs;

    public SimpleCyclesResourceCheckResult() {
        super();
        this.allResourcesUsed = true;
        this.simpleCycle = new ArrayList<>();
        this.missingClaimPairs = new ArrayList<>();
    }

    public SimpleCyclesResourceCheckResult(List<Location> simpleCycle,
            List<Tuple<Integer, Integer>> missingClaimPairs)
    {
        super();
        this.allResourcesUsed = false;
        this.simpleCycle = simpleCycle;
        this.missingClaimPairs = missingClaimPairs;
    }

    public boolean isAllResourcesUsed() {
        return allResourcesUsed;
    }

    public List<Location> getSimpleCycle() {
        return simpleCycle;
    }

    public List<Tuple<Integer, Integer>> getMissingClaimPairs() {
        return missingClaimPairs;
    }
}
