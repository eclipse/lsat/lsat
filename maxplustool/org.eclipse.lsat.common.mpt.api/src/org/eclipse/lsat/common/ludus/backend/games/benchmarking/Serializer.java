/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.benchmarking;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.lsat.common.ludus.backend.games.GameGraph;

/**
 * @author Bram van der Sanden
 */
public class Serializer {
    private Serializer() {
        // Empty
    }

    public static <V, E> String serialize(GameGraph<V, E> graph, String fileName) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss").format(new Date());
        String outputName = fileName + "_" + timeStamp;
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(outputName));
        out.writeObject(graph);
        out.close();
        return outputName;
    }

    public static GameGraph<?, ?> load(String fileName) throws IOException, ClassNotFoundException {
        System.out.println(Paths.get(".").toAbsolutePath().normalize().toString());
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        final GameGraph<?, ?> restoredGraph = (GameGraph<?, ?>)in.readObject();
        in.close();
        return restoredGraph;
    }
}
