/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.statespace;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.algebra.Value;
import org.eclipse.lsat.common.ludus.backend.graph.DoubleWeightedGraph;
import org.eclipse.lsat.common.ludus.backend.graph.SingleWeightedGraph;

/**
 * @author Bram van der Sanden
 */
@SuppressWarnings("rawtypes")
public class MaxPlusStateSpace implements SingleWeightedGraph<Configuration, Transition, Value>,
        DoubleWeightedGraph<Configuration, Transition, Value>
{
    private static final long serialVersionUID = 1L;

    private final Set<Configuration> configurations;

    private final Set<Transition> transitions;

    private Configuration initial;

    private final Map<Configuration, Set<Transition>> outgoingMap;

    private final Map<Configuration, Set<Transition>> incomingMap;

    public MaxPlusStateSpace() {
        configurations = new HashSet<>();
        transitions = new HashSet<>();
        outgoingMap = new HashMap<>();
        incomingMap = new HashMap<>();
    }

    public Set<Configuration> getConfigurations() {
        return configurations;
    }

    public Configuration getInitialConfiguration() {
        return initial;
    }

    public void setInitialConfiguration(Configuration initialConfiguration) {
        initial = initialConfiguration;
    }

    public boolean hasInitialConfiguration() {
        return initial != null;
    }

    public void addConfiguration(Configuration configuration) {
        configurations.add(configuration);
    }

    public void addTransition(Transition transition) {
        Configuration src = transition.getSource();
        Configuration tgt = transition.getTarget();

        outgoingMap.putIfAbsent(src, new HashSet<>());
        Set<Transition> out = outgoingMap.get(src);
        out.add(transition);

        incomingMap.putIfAbsent(tgt, new HashSet<>());
        Set<Transition> in = incomingMap.get(tgt);
        in.add(transition);

        transitions.add(transition);
    }

    @Override
    public Value getWeight1(Transition edge) {
        return edge.getReward();
    }

    @Override
    public Value getWeight2(Transition edge) {
        return edge.getDuration();
    }

    @Override
    public Value getWeight(Transition edge) {
        return edge.getDuration();
    }

    @Override
    public Set<Configuration> getVertices() {
        return getConfigurations();
    }

    @Override
    public Set<Transition> getEdges() {
        return transitions;
    }

    @Override
    public Collection<Transition> incomingEdgesOf(Configuration configuration) {
        return incomingMap.getOrDefault(configuration, Collections.emptySet());
    }

    @Override
    public Collection<Transition> outgoingEdgesOf(Configuration configuration) {
        return outgoingMap.getOrDefault(configuration, Collections.emptySet());
    }

    @Override
    public Configuration getEdgeSource(Transition transition) {
        return transition.getSource();
    }

    @Override
    public Configuration getEdgeTarget(Transition transition) {
        return transition.getTarget();
    }

    @Override
    public Transition getEdge(Configuration source, Configuration target) {
        for (Transition t: outgoingEdgesOf(source)) {
            if (t.getTarget().equals(target)) {
                return t;
            }
        }
        return null;
    }
}
