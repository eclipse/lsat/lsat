/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games.meanpayoff.solvers.energy;

import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.games.meanpayoff.MeanPayoffGame;

/**
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 * @param <T> edge weight type
 */
public interface MeanPayoffGameEnergy<V, E, T> extends MeanPayoffGame<V, E, T> {
    /**
     * Construct a subgraph given the set of vertices
     *
     * @param vertexSubset subset of vertices
     * @return subgraph induces by the vertices in vertexSubset
     */
    MeanPayoffGameEnergy<V, E, T> getSubGraph(Set<V> vertexSubset);

    /**
     * Construct a subgraph given the set of vertices where the vertices of Player 0 and Player 1 are swapped
     *
     * @param vertexSubset subset of vertices
     * @return subgraph induces by the vertices in vertexSubset where vertices of Player 0 and Player 1 are swapped
     */
    MeanPayoffGameEnergy<V, E, T> getSwappedSubGraph(Set<V> vertexSubset);
}
