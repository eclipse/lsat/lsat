/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.graph;

/**
 * Single weighted graph interface. Each edge in this graph has one weight.
 *
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 * @param <T> edge weights type
 */
public interface SingleWeightedGraph<V, E, T> extends Graph<V, E> {
    T getWeight(E edge);
}
