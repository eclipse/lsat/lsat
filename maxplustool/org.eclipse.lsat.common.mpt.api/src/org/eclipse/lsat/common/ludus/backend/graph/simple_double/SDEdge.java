/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.graph.simple_double;

/**
 * @author Bram van der Sanden
 */
public class SDEdge {
    Double w1;

    Double w2;

    SDVertex src;

    SDVertex tgt;

    public SDEdge(SDVertex source, SDVertex target, Double w1, Double w2) {
        this.w1 = w1;
        this.w2 = w2;
        this.src = source;
        this.tgt = target;
    }

    public Double getWeight1() {
        return w1;
    }

    public Double getWeight2() {
        return w2;
    }

    public SDVertex getSource() {
        return src;
    }

    public SDVertex getTarget() {
        return tgt;
    }
}
