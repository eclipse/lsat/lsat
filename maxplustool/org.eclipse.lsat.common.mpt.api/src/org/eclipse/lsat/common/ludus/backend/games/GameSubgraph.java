/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.games;

/**
 * @author Bram van der Sanden
 * @param <V> vertex type
 * @param <E> edge type
 */
public interface GameSubgraph<V, E> extends GameGraph<V, E> {
    /**
     * Remove all redundant edges given the current strategy.
     *
     * @param strategyVector strategy vector
     * @return subgraph induced by the current strategy
     */
    GameGraph<V, E> getSubgraph(StrategyVector<V, E> strategyVector);
}
