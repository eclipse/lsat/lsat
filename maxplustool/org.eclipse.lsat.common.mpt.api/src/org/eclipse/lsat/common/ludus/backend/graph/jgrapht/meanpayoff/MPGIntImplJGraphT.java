/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.ludus.backend.graph.jgrapht.meanpayoff;

import java.util.Collection;
import java.util.Set;

import org.eclipse.lsat.common.ludus.backend.datastructures.weights.SingleWeightFunctionInt;
import org.eclipse.lsat.common.ludus.backend.games.meanpayoff.solvers.energy.MeanPayoffGameEnergy;
import org.eclipse.lsat.common.ludus.backend.games.meanpayoff.solvers.policy.MeanPayoffGamePolicyIteration;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTEdge;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTGraph;
import org.eclipse.lsat.common.ludus.backend.graph.jgrapht.JGraphTVertex;

/**
 * @author Bram van der Sanden
 */
public class MPGIntImplJGraphT implements MeanPayoffGamePolicyIteration<JGraphTVertex, JGraphTEdge, Integer>,
        MeanPayoffGameEnergy<JGraphTVertex, JGraphTEdge, Integer>
{
    private static final long serialVersionUID = 1L;

    private final JGraphTGraph graph;

    private final SingleWeightFunctionInt<JGraphTEdge> edgeWeights;

    public MPGIntImplJGraphT(JGraphTGraph graph, SingleWeightFunctionInt<JGraphTEdge> edgeWeights) {
        this.graph = graph;
        this.edgeWeights = edgeWeights;
    }

    public SingleWeightFunctionInt<JGraphTEdge> getEdgeWeights() {
        return edgeWeights;
    }

    @Override
    public Set<JGraphTVertex> getV0() {
        return graph.getV0();
    }

    @Override
    public Set<JGraphTVertex> getV1() {
        return graph.getV1();
    }

    @Override
    public Set<JGraphTVertex> getVertices() {
        return graph.getVertices();
    }

    @Override
    public Set<JGraphTEdge> getEdges() {
        return graph.getEdges();
    }

    @Override
    public Collection<JGraphTEdge> incomingEdgesOf(JGraphTVertex v) {
        return graph.incomingEdgesOf(v);
    }

    @Override
    public Collection<JGraphTEdge> outgoingEdgesOf(JGraphTVertex v) {
        return graph.outgoingEdgesOf(v);
    }

    @Override
    public JGraphTVertex getEdgeSource(JGraphTEdge e) {
        return graph.getEdgeSource(e);
    }

    @Override
    public JGraphTVertex getEdgeTarget(JGraphTEdge e) {
        return graph.getEdgeTarget(e);
    }

    @Override
    public JGraphTEdge getEdge(JGraphTVertex source, JGraphTVertex target) {
        return graph.getEdge(source, target);
    }

    @Override
    public Integer getWeight(JGraphTEdge edge) {
        return edgeWeights.getWeight(edge);
    }

    @Override
    public Integer getMaxAbsValue() {
        return edgeWeights.getMaxAbsValue();
    }

    @Override
    public MPGIntImplJGraphT getSubGraph(Set<JGraphTVertex> vertexSubset) {
        // Construct subgraph.
        JGraphTGraph subGraph = graph.getSubgraph(vertexSubset);
        return new MPGIntImplJGraphT(subGraph, edgeWeights);
    }

    @Override
    public MPGIntImplJGraphT getSwappedSubGraph(Set<JGraphTVertex> vertexSubset) {
        // Construct subgraph.
        JGraphTGraph subGraph = graph.getSwappedSubgraph(vertexSubset);
        return new MPGIntImplJGraphT(subGraph, edgeWeights);
    }

    @Override
    public Integer getId(JGraphTVertex vertex) {
        return vertex.getId();
    }
}
