/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.Collections;
import java.util.List;

public class MaximumThroughputResult {
    private final double throughput;

    private final List<String> transientStateActivities;

    private final List<String> steadyStateActivities;

    public MaximumThroughputResult(double throughput) {
        this.throughput = throughput;
        this.steadyStateActivities = Collections.emptyList();
        this.transientStateActivities = Collections.emptyList();
    }

    public MaximumThroughputResult(double throughput, List<String> steadyStateActivities,
            List<String> transientStateActivities)
    {
        this.throughput = throughput;
        this.steadyStateActivities = steadyStateActivities;
        this.transientStateActivities = transientStateActivities;
    }

    public double getThroughput() {
        return throughput;
    }

    public List<String> getSteadyStateActivities() {
        return steadyStateActivities;
    }

    public List<String> getTransientStateActivities() {
        return transientStateActivities;
    }

    @Override
    public String toString() {
        return "MaximumThroughputResult [throughput=" + throughput + ", steady state activities="
                + steadyStateActivities + ", transient state activities=" + transientStateActivities + "]";
    }
}
