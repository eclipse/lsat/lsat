/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.mpt.Matrix;

/**
 * Validation of max-plus specifications.
 */
public final class SpecificationValidator {
    private SpecificationValidator() {
        // Empty
    }

    public static boolean allMatricesSameSize(Collection<Matrix> matrixCollection) {
        Matrix first = matrixCollection.iterator().next();
        int rows = first.getRows().size();
        int columns = first.getRows().get(0).getValues().size();

        for (Matrix m: matrixCollection) {
            if (m.getRows().size() != rows) {
                return false;
            }
            if (m.getRows().get(0).getValues().size() != columns) {
                return false;
            }
        }
        return true;
    }

    /**
     * Find all resources that are not used in any of the matrices.
     *
     * @param matrices of same size
     * @return
     */
    public static Set<Integer> findUnusedResources(Collection<Matrix> matrices) {
        int resourceCount = matrices.iterator().next().getRows().size();

        List<Set<Integer>> usedResourcesPerMatrix = matrices.stream().map(m -> findUnusedResourcesInMatrix(m))
                .collect(Collectors.toList());

        Set<Integer> unusedResources = new HashSet<>();

        for (int resId = 0; resId < resourceCount; resId++) {
            final int res = resId;
            // Check if resource is unused in all matrices.
            boolean unusedInAllMatrices = usedResourcesPerMatrix.stream().allMatch(s -> s.contains(res));

            if (unusedInAllMatrices) {
                unusedResources.add(resId);
            }
        }

        return unusedResources;
    }

    /**
     * Find all resources in the matrix that are unused, i.e., that have an identity row and identity column, meaning
     * that it has no connections with other resources and does not change the time stamp of the resource itself.
     *
     * @param matrix
     * @return a list of all unused resource identifiers
     */
    public static Set<Integer> findUnusedResourcesInMatrix(Matrix matrix) {
        int matrixSize = matrix.getRows().size();
        Set<Integer> unusedResources = new HashSet<>();

        for (int resId = 0; resId < matrixSize; resId++) {
            // Check the resource row
            boolean identityRow = true;
            for (int column = 0; column < matrixSize; column++) {
                if (resId == column) { // We expect 0.
                    if (matrix.getValue(resId, column) != 0.0d) {
                        identityRow = false;
                        break;
                    }
                } else if (resId != column) { // We expect -Infinity
                    if (matrix.getValue(resId, column) != Double.NEGATIVE_INFINITY) {
                        identityRow = false;
                        break;
                    }
                }
            }

            // Check the resource column
            boolean identityColumn = true;
            for (int row = 0; row < matrixSize; row++) {
                if (resId == row) { // We expect 0.
                    if (matrix.getValue(row, resId) != 0.0d) {
                        identityColumn = false;
                        break;
                    }
                } else if (resId != row) { // We expect -Infinity
                    if (matrix.getValue(row, resId) != Double.NEGATIVE_INFINITY) {
                        identityColumn = false;
                        break;
                    }
                }
            }

            if (identityRow && identityColumn) {
                unusedResources.add(resId);
            }
        }
        return unusedResources;
    }

    /**
     * Find all resources that are not connected in any of the matrices.
     *
     * @param matrices of same size
     * @return
     */
    public static Set<Integer> findUnconnectedResources(Collection<Matrix> matrices) {
        int resourceCount = matrices.iterator().next().getRows().size();

        List<Set<Integer>> unconnectedResourcesPerMatrix = matrices.stream()
                .map(m -> findUnconnectedResourcesInMatrix(m)).collect(Collectors.toList());

        Set<Integer> unconnectedResources = new HashSet<>();

        for (int resId = 0; resId < resourceCount; resId++) {
            final int res = resId;
            // Check if resource is unconnected in all matrices.
            boolean unconnectedInAllMatrices = unconnectedResourcesPerMatrix.stream().allMatch(s -> s.contains(res));

            if (unconnectedInAllMatrices) {
                unconnectedResources.add(resId);
            }
        }

        return unconnectedResources;
    }

    public static Set<Integer> findUnconnectedResourcesInMatrix(Matrix matrix) {
        int matrixSize = matrix.getRows().size();
        Set<Integer> unconnectedResources = new HashSet<>();

        for (int resId = 0; resId < matrixSize; resId++) {
            // Check the resource row (entry (i,j) represents the longest path
            // from the claim of resource j to the release of resource i)
            boolean identityColumn = true;
            for (int column = 0; column < matrixSize; column++) {
                if (resId == column) { // We expect 0.
                    if (matrix.getValue(resId, column) != 0.0d) {
                        identityColumn = false;
                        break;
                    }
                } else if (resId != column) { // We expect -Infinity
                    if (matrix.getValue(resId, column) != Double.NEGATIVE_INFINITY) {
                        identityColumn = false;
                        break;
                    }
                }
            }

            if (identityColumn) {
                unconnectedResources.add(resId);
            }
        }
        return unconnectedResources;
    }
}
