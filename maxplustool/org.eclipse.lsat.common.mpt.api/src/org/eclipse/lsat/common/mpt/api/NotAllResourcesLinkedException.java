/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.List;

import org.eclipse.lsat.common.ludus.backend.datastructures.tuple.Tuple;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;

public class NotAllResourcesLinkedException extends MaxPlusException {
    private static final long serialVersionUID = 5762300638917949782L;

    private final List<Location> simpleCycle;

    private final List<Tuple<Integer, Integer>> missingClaimPairs;

    public NotAllResourcesLinkedException(List<Location> cycle, List<Tuple<Integer, Integer>> missingPairs) {
        super("not all resources are used on the simple cycle");
        simpleCycle = cycle;
        missingClaimPairs = missingPairs;
    }

    public List<Location> getSimpleCycle() {
        return simpleCycle;
    }

    public List<Tuple<Integer, Integer>> getMissingClaimPairs() {
        return missingClaimPairs;
    }
}
