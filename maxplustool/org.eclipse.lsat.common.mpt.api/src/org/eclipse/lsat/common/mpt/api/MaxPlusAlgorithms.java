/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.common.ludus.api.MatrixDependencies;
import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.fsm.PrintToCIF;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.FSMImpl;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;
import org.eclipse.lsat.common.ludus.backend.por.ClusterPORPerformance;
import org.eclipse.lsat.common.ludus.backend.por.ClusterPORPerformanceFunctional;
import org.eclipse.lsat.common.ludus.backend.por.DependencyGraph;
import org.eclipse.lsat.common.ludus.backend.por.PrePostProcessor;
import org.eclipse.lsat.common.mpt.Edge;
import org.eclipse.lsat.common.mpt.FSM;
import org.eclipse.lsat.common.mpt.FSMTransition;
import org.eclipse.lsat.common.mpt.FSMType;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;
import org.eclipse.lsat.common.mpt.api.adapter.FSMAdapter;
import org.eclipse.lsat.common.mpt.api.adapter.MatrixAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Algorithms that are provided by the max-plus toolbox.
 *
 * @author Bram van der Sanden
 */
public final class MaxPlusAlgorithms {
    private static final Logger LOGGER = LoggerFactory.getLogger(MaxPlusAlgorithms.class);

    public static final double SCALING_FACTOR = 1000000d; // Scale all values by 6 digits and round resulting values.

    private MaxPlusAlgorithms() {
        // Empty for utility classes
    }

    /**
     * Given a max-plus specification with an FSMImpl and a timing matrix for each activity, calculate the minimum
     * makespan value and the corresponding activity sequence. Note that the notion of marked states is not taken into
     * account during this analysis. Any state without outgoing edges is considered to be a valid final state.
     *
     * @param specification max-plus specification
     * @return minimum makespan value and corresponding activity sequence
     * @throws MaxPlusException if the FSMImpl has cycles or if some timing matrices are missing
     */
    public static MinimumMakespanResult calculateMinimumMakespan(MaxPlusSpecification specification)
            throws MaxPlusException
    {
        String startErrorMessage = "Minimum makespan cannot be calculated. ";

        try {
            SpecificationFSMResult fsmResult = loadFSM(specification);

            // Calculate minimum makespan.
            org.eclipse.lsat.common.ludus.api.MinimumMakespanResult result = org.eclipse.lsat.common.ludus.api.MaxPlusAlgorithms
                    .calculateMinimumMakespan(fsmResult.getFSM(), fsmResult.getActivityMatrixMap());

            // Transform to local makespan result.
            return new MinimumMakespanResult(result.getMakespan() / SCALING_FACTOR, result.getEvents());
        } catch (org.eclipse.lsat.common.ludus.api.MaxPlusException e) {
            throw new MaxPlusException(startErrorMessage + e.getMessage());
        }
    }

    /**
     * Given a max-plus specification with an FSMImpl and a timing matrix for each activity, calculate the maximum
     * makespan value and the corresponding activity sequence.
     *
     * @param specification max-plus specification
     * @return maximum makespan value and corresponding activity sequence
     * @throws MaxPlusException if the FSMImpl has cycles or if some timing matrices are missing
     */
    public static MaximumMakespanResult calculateMaximumMakespan(MaxPlusSpecification specification)
            throws MaxPlusException
    {
        String startErrorMessage = "Maximum makespan cannot be calculated. ";

        try {
            SpecificationFSMResult fsmResult = loadFSM(specification);

            // Calculate maximum makespan.
            org.eclipse.lsat.common.ludus.api.MaximumMakespanResult result = org.eclipse.lsat.common.ludus.api.MaxPlusAlgorithms
                    .calculateMaximumMakespan(fsmResult.getFSM(), fsmResult.getActivityMatrixMap());

            // Transform to local makespan result.
            return new MaximumMakespanResult(result.getMakespan() / SCALING_FACTOR, result.getEvents());
        } catch (org.eclipse.lsat.common.ludus.api.MaxPlusException e) {
            throw new MaxPlusException(startErrorMessage + e.getMessage());
        }
    }

    /**
     * Given a max-plus specification with an FSMImpl and a timing matrix for each activity, calculate the maximum
     * (highest, best-case) throughput value and the corresponding activity sequence.
     *
     * @param specification max-plus specification
     * @return maximum throughput value and corresponding activity sequence
     * @throws MaxPlusException if the FSMImpl has no cycles or if some timing matrices are missing
     */
    public static MaximumThroughputResult calculateMaximumThroughput(MaxPlusSpecification specification)
            throws MaxPlusException
    {
        String startErrorMessage = "Maximum throughput cannot be calculated. ";

        try {
            SpecificationFSMResult fsmResult = loadFSM(specification);

            // Calculate maximum throughput.
            org.eclipse.lsat.common.ludus.api.MaximumThroughputResult result = org.eclipse.lsat.common.ludus.api.MaxPlusAlgorithms
                    .calculateMaximumThroughput(fsmResult.getFSM(), fsmResult.getActivityMatrixMap());

            // Transform to local throughput result.
            return new MaximumThroughputResult(result.getThroughput() * SCALING_FACTOR, result.getSteadyStateEvents(),
                    result.getTransientStateEvents());
        } catch (org.eclipse.lsat.common.ludus.api.MaxPlusException e) {
            throw new MaxPlusException(startErrorMessage + e.getMessage());
        } catch (NotAllResourcesLinkedException e) {
            String cycle = e.getSimpleCycle().stream().map(loc -> String.valueOf(loc))
                    .collect(Collectors.joining("->", "(", ")"));
            List<String> resourceNames = specification.getMatrices().iterator().next().getRows().stream()
                    .map(row -> row.getName()).collect(Collectors.toList());

            String missingClaimDependencies = e.getMissingClaimPairs().stream()
                    .map(tup -> resourceNames.get(tup.getLeft()) + " to " + resourceNames.get(tup.getRight()))
                    .collect(Collectors.joining(", ", "", ""));

            throw new MaxPlusException(startErrorMessage + e.getMessage() + " " + cycle
                    + " missing claim dependencies from " + missingClaimDependencies);
        } catch (UnconnectedResourceException e) {
            List<String> resourceNames = specification.getMatrices().iterator().next().getRows().stream()
                    .map(row -> row.getName()).collect(Collectors.toList());
            String resName = resourceNames.get(e.getResourceId());

            throw new MaxPlusException(
                    e.getMessage() + " Resource " + resName + " is not connected to other resources.");
        }
    }

    /**
     * Given a max-plus specification with an FSMImpl and a timing matrix for activity event, calculate the minimum
     * (lowest, worst-case) throughput value and the corresponding activity sequence.
     *
     * @param specification max-plus specification
     * @return minimum throughput value and corresponding activity sequence
     * @throws MaxPlusException if the FSMImpl has no cycles or if some timing matrices are missing
     */
    public static MinimumThroughputResult calculateMinimumThroughput(MaxPlusSpecification specification)
            throws MaxPlusException
    {
        String startErrorMessage = "Minimum throughput cannot be calculated. ";

        try {
            SpecificationFSMResult fsmResult = loadFSM(specification);

            // Calculate minimum throughput.
            org.eclipse.lsat.common.ludus.api.MinimumThroughputResult result = org.eclipse.lsat.common.ludus.api.MaxPlusAlgorithms
                    .calculateMinimumThroughput(fsmResult.getFSM(), fsmResult.getActivityMatrixMap());

            // Transform to local throughput result.
            return new MinimumThroughputResult(result.getThroughput() * SCALING_FACTOR, result.getEvents());
        } catch (org.eclipse.lsat.common.ludus.api.MaxPlusException e) {
            throw new MaxPlusException(startErrorMessage + e.getMessage());
        }
    }

    /**
     * Given a max-plus specification with an FSMImpl and a timing matrix for each activity, calculate a reduced FSM.
     * <p>
     * This POR preserves only performance aspects.
     * </p>
     *
     * @param specification max-plus specification
     * @param compositionName name for the resulting composition automaton
     * @return reduced FSM as CIF specification
     * @throws MaxPlusException if the FSMImpl has no cycles, or if some timing matrices are missing
     */
    public static String applyClusterReductionPerformance(MaxPlusSpecification specification, String compositionName)
            throws MaxPlusException
    {
        // Retrieve a list of all FSMs.
        List<FSM> fsmList = SpecificationTraverser.findAllFSMs(specification);

        if (fsmList.isEmpty()) {
            throw new MaxPlusException("The specification contains no FSM.");
        }

        // Compute a dependency graph.
        Map<String, org.eclipse.lsat.common.ludus.backend.algebra.Matrix> mapping = new HashMap<>();
        for (FSM fsm: fsmList) {
            for (Edge e: fsm.getEdges()) {
                FSMTransition t = (FSMTransition)e;
                if (t.getMatrix() == null) {
                    throw new MaxPlusException("Cannot find matrix for event " + t.getName() + ".");
                }
                mapping.put(t.getName(), MatrixAdapter.getMatrix(t.getMatrix()).getLeft());
            }
        }
        DependencyGraph dependencies = MatrixDependencies.getDependencyGraphResourceSharing(mapping);

        // Create input specification for back-end.
        List<org.eclipse.lsat.common.ludus.backend.fsm.FSM<Location, org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge>> backendFSMlist = new ArrayList<>();
        for (FSM fsm: fsmList) {
            org.eclipse.lsat.common.ludus.backend.fsm.FSM<Location, org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge> backendFSM = FSMAdapter
                    .getFSM(specification, fsm);
            backendFSMlist.add(backendFSM);
        }

        // Transform backendFSM into a smaller FSM using the cluster reduction.
        ClusterPORPerformance clusterPOR = new ClusterPORPerformance();
        FSMImpl result = clusterPOR.compute(backendFSMlist, dependencies);

        LOGGER.info("POR finished. Size of resulting supervisor: " + result.getVertices().size() + " vertices, "
                + result.getEdges().size() + " edges.");

        return (PrintToCIF.print(result, compositionName));
    }

    /**
     * Given a max-plus specification with an FSMImpl and a timing matrix for each activity, calculate a reduced FSM.
     * <p>
     * This POR preserves both performance and functional aspects.
     * </p>
     *
     * @param specification max-plus specification
     * @param compositionName name for the resulting composition automaton
     * @return reduced FSM as CIF specification
     * @throws MaxPlusException if the FSMImpl has no cycles, or if some timing matrices are missing
     */
    public static String applyClusterReductionPerformanceFunctional(MaxPlusSpecification specification,
            String compositionName) throws MaxPlusException
    {
        // Retrieve a list of all FSMs.
        List<FSM> fsmList = SpecificationTraverser.findAllFSMs(specification);

        if (fsmList.isEmpty()) {
            throw new MaxPlusException("The specification contains no FSM.");
        }

        // Compute a dependency graph.
        Map<String, org.eclipse.lsat.common.ludus.backend.algebra.Matrix> mapping = new HashMap<>();
        for (FSM fsm: fsmList) {
            for (Edge e: fsm.getEdges()) {
                FSMTransition t = (FSMTransition)e;
                if (t.getMatrix() == null) {
                    throw new MaxPlusException("Cannot find matrix for event " + t.getName() + ".");
                }
                mapping.put(t.getName(), MatrixAdapter.getMatrix(t.getMatrix()).getLeft());
            }
        }
        DependencyGraph dependencies = MatrixDependencies.getDependencyGraphResourceSharing(mapping);

        // Create input specification for back-end.
        List<org.eclipse.lsat.common.ludus.backend.fsm.FSM<Location, org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge>> backendFSMlist = new ArrayList<>();
        for (FSM fsm: fsmList) {
            FSMImpl backendFSM = FSMAdapter.getFSM(specification, fsm);

            // Plantify if needed.
            if (fsm.getType().equals(FSMType.REQUIREMENT)) {
                backendFSM = PrePostProcessor.plantify(backendFSM);
            }
            backendFSM = PrePostProcessor.addOmegaLoops(backendFSM);

            backendFSMlist.add(backendFSM);
        }

        // Transform backendFSM into a smaller FSM using the cluster reduction.
        ClusterPORPerformanceFunctional clusterPOR = new ClusterPORPerformanceFunctional();
        FSMImpl result = clusterPOR.compute(backendFSMlist, dependencies);

        // Remove the omega loops, as we have the "marked" keyword in CIF.
        result = PrePostProcessor.removeOmegaLoops(result);

        LOGGER.info("POR finished. Size of resulting supervisor: " + result.getVertices().size() + " vertices, "
                + result.getEdges().size() + " edges.");

        return (PrintToCIF.print(result, compositionName));
    }

    /**
     * Check if there is a single FSM in the specification, and if so return the loaded FSM.
     *
     * @param specification max-plus specification
     * @return single FSM present in the specification
     * @throws MaxPlusException if none or multiple FSMs are found in the specification
     */
    private static SpecificationFSMResult loadFSM(MaxPlusSpecification specification) throws MaxPlusException {
        List<FSM> fsms = SpecificationTraverser.findAllFSMs(specification);
        if (fsms.isEmpty()) {
            throw new MaxPlusException("The specification contains no FSM.");
        } else if (fsms.size() > 1) {
            throw new MaxPlusException("The specification contains multiple FSMs.");
        } else {
            return computeSpecificationFSM(specification, fsms.get(0));
        }
    }

    /**
     * In the given specification, find an FSM and the matrix corresponding to each activity.
     *
     * @param specification max-plus specification
     * @param fsm FSM present in the max-plus specification
     * @return triple with the FSM, the vector size, and the matrix mapping
     * @throws MaxPlusException throws exception if the specification contains no FSM
     */
    private static SpecificationFSMResult computeSpecificationFSM(MaxPlusSpecification specification, FSM fsm)
            throws MaxPlusException
    {
        // Load the FSM.
        org.eclipse.lsat.common.ludus.backend.fsm.FSM<Location, org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge> backendFSM = FSMAdapter
                .getFSM(specification, fsm);

        // Find the LSAT matrices of all activities in the FSM.
        Map<String, Matrix> mapping = new HashMap<>();
        List<org.eclipse.lsat.common.mpt.Matrix> matrixList = new LinkedList<>();
        for (Edge e: fsm.getEdges()) {
            FSMTransition t = (FSMTransition)e;
            if (t.getMatrix() == null) {
                throw new MaxPlusException("Cannot find matrix for activity " + t.getName() + ".");
            }
            matrixList.add(t.getMatrix());
        }

        // Check that all matrices have the same size.
        if (!SpecificationValidator.allMatricesSameSize(matrixList)) {
            throw new MaxPlusException("Matrices have different sizes!");
        }

        // Check whether there are resources that have no incoming dependency from other
        // resources.
        Set<Integer> notConnectedResourceIds = SpecificationValidator.findUnconnectedResources(matrixList);

        Set<String> notConnectedResources = notConnectedResourceIds.stream()
                .map(id -> matrixList.iterator().next().getRows().get(id).getName()).collect(Collectors.toSet());

        if (notConnectedResources.size() == 1) {
            LOGGER.warn("Resource " + notConnectedResources.iterator().next()
                    + " does not have any incoming dependency from another resource. "
                    + "This might lead to generation of an infinite max-plus state space.");
        } else if (notConnectedResources.size() > 1) {
            LOGGER.warn("Resources " + String.join(",", notConnectedResources)
                    + " do not have any incoming dependency from another resource. "
                    + "This might lead to generation of an infinite max-plus state space.");
        }

        // Remove unused resources from all matrices.
        Set<Integer> unusedResourcesById = SpecificationValidator.findUnusedResources(matrixList);

        Set<String> unusedResources = unusedResourcesById.stream()
                .map(id -> matrixList.iterator().next().getRows().get(id).getName()).collect(Collectors.toSet());

        if (unusedResources.size() == 1) {
            LOGGER.warn("Resource " + unusedResources.iterator().next() + " is not updated by any of the activities. "
                    + "Resource is not taken into account during performance analysis.");
        } else if (unusedResources.size() > 1) {
            LOGGER.warn("Resources " + String.join(",", unusedResources) + " are not updated by any of the activities. "
                    + "Resources are not taken into account during performance analysis.");
        }

        for (Edge e: fsm.getEdges()) {
            FSMTransition t = (FSMTransition)e;
            mapping.put(t.getName(), MatrixAdapter.getMatrix(t.getMatrix(), unusedResourcesById).getLeft());
        }

        int vecSize = mapping.values().iterator().next().getColumns();

        return new SpecificationFSMResult(backendFSM, vecSize, mapping);
    }
}
