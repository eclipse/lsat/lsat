/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.Map;

import org.eclipse.lsat.common.ludus.backend.algebra.Matrix;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge;
import org.eclipse.lsat.common.ludus.backend.fsm.impl.Location;

public class SpecificationFSMResult {
    private final org.eclipse.lsat.common.ludus.backend.fsm.FSM<Location, org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge> fsm;

    private final Integer vectorSize;

    private final Map<String, Matrix> activityMatrixMap;

    public SpecificationFSMResult(org.eclipse.lsat.common.ludus.backend.fsm.FSM<Location, Edge> fsm, Integer vectorSize,
            Map<String, Matrix> activityMatrixMap)
    {
        super();
        this.fsm = fsm;
        this.vectorSize = vectorSize;
        this.activityMatrixMap = activityMatrixMap;
    }

    public Integer getVectorSize() {
        return vectorSize;
    }

    public Map<String, Matrix> getActivityMatrixMap() {
        return activityMatrixMap;
    }

    public org.eclipse.lsat.common.ludus.backend.fsm.FSM<Location, org.eclipse.lsat.common.ludus.backend.fsm.impl.Edge>
            getFSM()
    {
        return fsm;
    }

    public Matrix getMatrix(String activity) {
        return activityMatrixMap.get(activity);
    }
}
