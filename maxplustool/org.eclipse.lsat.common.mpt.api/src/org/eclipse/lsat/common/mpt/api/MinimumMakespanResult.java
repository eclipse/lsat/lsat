/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

import java.util.List;

public class MinimumMakespanResult {
    private final double makespan;

    private final List<String> activities;

    public MinimumMakespanResult(double makespan, List<String> activities) {
        this.makespan = makespan;
        this.activities = activities;
    }

    public double getMakespan() {
        return makespan;
    }

    public List<String> getActivities() {
        return activities;
    }

    @Override
    public String toString() {
        return "MinimalMakespanResult [makespan=" + makespan + ", activities=" + activities + "]";
    }
}
