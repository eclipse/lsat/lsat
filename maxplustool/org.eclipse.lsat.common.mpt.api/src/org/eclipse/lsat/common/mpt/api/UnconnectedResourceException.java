/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.mpt.api;

public class UnconnectedResourceException extends MaxPlusException {
    private static final long serialVersionUID = 5762300638917949782L;

    private final Integer resourceId;

    public UnconnectedResourceException(Integer resId) {
        super("Stopping exploration. " + "Infinite state space will be generated. ");
        resourceId = resId;
    }

    public Integer getResourceId() {
        return resourceId;
    }
}
