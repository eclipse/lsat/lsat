/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.log4j.eclipse;

import static java.lang.String.format;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;

public class EclipseLogAppender extends AppenderSkeleton {
    private static final String DEFAULT_BUNDLE_ID = "org.apache.log4j";

    private String bundleId = null;

    private ILog eclipseLog = null;

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getBundleId() {
        return bundleId;
    }

    @Override
    public void activateOptions() {
        if (null == bundleId) {
            bundleId = DEFAULT_BUNDLE_ID;
        }

        eclipseLog = getLog(bundleId);
        if (null == eclipseLog) {
            // Use the default bundle, but write an error
            String errorMsg = format("Log4j-appender %s failed to create Eclipse logger for %s: bundle does not exist",
                    name, bundleId);
            bundleId = DEFAULT_BUNDLE_ID;
            eclipseLog = getLog(bundleId);
            if (null == eclipseLog) {
                errorHandler.error(errorMsg);
            } else {
                IStatus status = new Status(IStatus.WARNING, bundleId, errorMsg);
                eclipseLog.log(status);
            }
        } else {
            String msg = "The Eclipse log log4j-appender is successfully installed";
            IStatus status = new Status(IStatus.INFO, bundleId, msg);
            eclipseLog.log(status);
        }

        super.activateOptions();
    }

    @Override
    public void close() {
        closed = true;
    }

    @Override
    public boolean requiresLayout() {
        return true;
    }

    @Override
    protected void append(LoggingEvent event) {
        final int severity = getSeverity(event.getLevel());
        final Layout layout = getLayout();
        final String message = null == layout ? event.getRenderedMessage() : layout.format(event);
        final Throwable throwable = getThrowable(event);
        IStatus status = new Status(severity, bundleId, message, throwable);
        eclipseLog.log(status);
    }

    private final int getSeverity(Level level) {
        // Using if/else makes it safe for new/custom levels
        if (level.isGreaterOrEqual(Level.ERROR)) {
            return IStatus.ERROR;
        }
        if (level.isGreaterOrEqual(Level.WARN)) {
            return IStatus.WARNING;
        }
        return IStatus.INFO;
    }

    private final Throwable getThrowable(LoggingEvent event) {
        final ThrowableInformation ti = event.getThrowableInformation();
        return null == ti ? null : ti.getThrowable();
    }

    private final ILog getLog(String bundleId) {
        if (null == bundleId) {
            return null;
        }
        Bundle bundle = Platform.getBundle(bundleId);
        if (null == bundle) {
            return null;
        }
        return Platform.getLog(bundle);
    }
}
