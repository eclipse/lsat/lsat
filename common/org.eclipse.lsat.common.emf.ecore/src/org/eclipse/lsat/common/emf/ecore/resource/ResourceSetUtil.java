/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.ExternalCrossReferencer;

public final class ResourceSetUtil {
    public static final Object OPTION_RESOURCE_RENAME_FILTER = ResourceRenameFilter.class;

    public static final Object OPTION_PROGRESS_MONITOR = IProgressMonitor.class;

    private ResourceSetUtil() {
        // Empty
    }

    public static URI[] getNormalizedResourceURIs(ResourceSet aResourceSet) {
        final List<Resource> resources = aResourceSet.getResources();
        final URI[] result = new URI[resources.size()];
        for (int i = 0; i < resources.size(); i++) {
            result[i] = aResourceSet.getURIConverter().normalize(resources.get(i).getURI());
        }
        return result;
    }

    public static void saveResources(ResourceSet aResourceSet, Map<?, ?> aOptions) throws IOException {
        final IProgressMonitor progressMonitor = getProgressMonitor(aOptions);
        try {
            List<Resource> resources = getResourcesInSaveOrder(aResourceSet);
            progressMonitor.beginTask("Saving resources", resources.size());
            for (Resource resource: resources) {
                if (resource.isLoaded()) {
                    progressMonitor.subTask("Saving resource: " + resource.getURI());
                    saveResource(resource, aOptions);
                    progressMonitor.worked(1);
                }
            }
        } finally {
            progressMonitor.done();
        }
    }

    private static IProgressMonitor getProgressMonitor(Map<?, ?> aOptions) {
        final IProgressMonitor progressMonitor = (null == aOptions) ? null
                : (IProgressMonitor)aOptions.get(OPTION_PROGRESS_MONITOR);
        return (null == progressMonitor) ? new NullProgressMonitor() : progressMonitor;
    }

    public static void saveResource(Resource aResource, Map<?, ?> aOptions) throws IOException {
        final ResourceRenameFilter renameFilter = (aOptions == null) ? null
                : (ResourceRenameFilter)aOptions.get(OPTION_RESOURCE_RENAME_FILTER);
        if (null == renameFilter) {
            // Just save the resource
            aResource.save(aOptions);
            return;
        }

        final ResourceSet resourceSet = aResource.getResourceSet();
        final URI resourceURI = aResource.getURI();
        final URI normalizedURI = resourceSet.getURIConverter().normalize(resourceURI);
        final boolean map = !resourceURI.equals(normalizedURI);

        final URI saveURI = renameFilter.renameAccept(normalizedURI);
        if (null == saveURI) {
            // Do not save this resource
            return;
        } else if (saveURI.equals(normalizedURI)) {
            // Just save the resource
            aResource.save(aOptions);
        } else if (map) {
            // Only changing the mapping is enough
            resourceSet.getURIConverter().getURIMap().put(resourceURI, saveURI);
            aResource.save(aOptions);
        } else {
            // Create the new resource and save it
            Resource saveResource = resourceSet.createResource(saveURI);
            saveResource.getContents().addAll(aResource.getContents());
            saveResource.save(aOptions);
            aResource.unload();
        }
    }

    /**
     * Returns the {@link ResourceSet#getResources()} in a order in which they should be saved. The first resource in
     * the list will not depend upon any other resource and the last resource in the list might depend upon all other
     * resources in the list.
     */
    public static List<Resource> getResourcesInSaveOrder(ResourceSet resourceSet) {
        // Resolve all cross references to avoid concurrent modification exceptions
        EcoreUtil.resolveAll(resourceSet);

        HashMap<Resource, Set<Resource>> references = new HashMap<Resource, Set<Resource>>();
        for (Resource resource: resourceSet.getResources()) {
            if (!references.containsKey(resource)) {
                references.put(resource, new HashSet<Resource>());
            }
            if (!resource.isLoaded()) {
                // Nothing to do anymore
                continue;
            }
            for (EObject reference: ExternalCrossReferencer.find(resource).keySet()) {
                if (null == reference.eResource()) {
                    continue;
                }
                if (!references.containsKey(reference.eResource())) {
                    references.put(reference.eResource(), new HashSet<Resource>());
                }
                references.get(reference.eResource()).add(resource);
            }
        }

        ArrayList<Resource> ordered = new ArrayList<Resource>(references.size());
        while (!references.isEmpty()) {
            Resource resourceWithoutReferences = null;
            for (Map.Entry<Resource, Set<Resource>> resource: references.entrySet()) {
                if (resource.getValue().isEmpty()) {
                    // found next candidate!
                    resourceWithoutReferences = resource.getKey();
                    break;
                }
            }
            if (resourceWithoutReferences == null) {
                throw new IllegalArgumentException("Found dependency cycle in resources: " + references);
            }
            ordered.add(0, resourceWithoutReferences);
            references.remove(resourceWithoutReferences);
            // Dereference resource
            for (Set<Resource> refs: references.values()) {
                refs.remove(resourceWithoutReferences);
            }
        }
        return ordered;
    }

    public static <T extends EObject> List<T> getObjectsByType(ResourceSet resourceSet, Class<T> clazz) {
        return resourceSet.getResources().stream().flatMap(r -> r.getContents().stream())
                .filter(c -> clazz.isInstance(c)).map(c -> clazz.cast(c)).collect(Collectors.toList());
    }

    public static <T extends EObject> T getObjectByType(ResourceSet resourceSet, Class<T> clazz) {
        return resourceSet.getResources().stream().flatMap(r -> r.getContents().stream())
                .filter(c -> clazz.isInstance(c)).map(c -> clazz.cast(c)).findFirst().orElseGet(null);
    }
}
