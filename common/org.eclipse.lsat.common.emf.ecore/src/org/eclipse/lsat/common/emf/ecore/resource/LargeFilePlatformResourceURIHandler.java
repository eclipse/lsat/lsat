/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.URIHandler;
import org.eclipse.emf.ecore.resource.impl.PlatformResourceURIHandlerImpl;
import org.eclipse.lsat.common.emf.common.util.URIHelper;

/**
 * The standard {@link PlatformResourceURIHandlerImpl} used a byte[] to cache the complete file content before writing
 * it to disk. This does not work for large files as it causes enormous memory consumption. This class therefore uses a
 * {@link FileOutputStream}.
 */
public class LargeFilePlatformResourceURIHandler extends PlatformResourceURIHandlerImpl {
    public static final URIHandler INSTANCE = new LargeFilePlatformResourceURIHandler();

    /**
     * Convenience method to register the {@link #INSTANCE} to the specified {@link URIConverter} with the highest
     * priority.
     *
     * @return the specified <code>uriConverter</code>
     */
    public static final URIConverter registerTo(final URIConverter uriConverter) {
        uriConverter.getURIHandlers().add(0, INSTANCE);
        return uriConverter;
    }

    @Override
    public boolean canHandle(URI uri) {
        return uri.isPlatformResource() && URIHelper.asFile(uri) != null;
    }

    @Override
    public OutputStream createOutputStream(URI uri, Map<?, ?> options) throws IOException {
        final File file = URIHelper.asFile(uri);
        final String parent = file.getParent();
        if (parent != null) {
            new File(parent).mkdirs();
        }

        final Map<Object, Object> response = getResponse(options);
        final OutputStream outputStream = new FileOutputStream(file) {
            @Override
            public void close() throws IOException {
                try {
                    super.close();
                } finally {
                    if (response != null) {
                        response.put(URIConverter.RESPONSE_TIME_STAMP_PROPERTY, file.lastModified());
                    }
                }
            }
        };
        return outputStream;
    }
}
