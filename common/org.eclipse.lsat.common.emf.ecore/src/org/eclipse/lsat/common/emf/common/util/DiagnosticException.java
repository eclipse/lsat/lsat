/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

import static org.eclipse.lsat.common.emf.common.util.DiagnosticUtil.getFullMessage;

import org.eclipse.emf.common.util.Diagnostic;

public class DiagnosticException extends Exception {
    private static final long serialVersionUID = 158137653239577439L;

    private final Diagnostic itsDiagnostic;

    public DiagnosticException(Diagnostic aDiagnostic) {
        super(getFullMessage(aDiagnostic), aDiagnostic.getException());
        itsDiagnostic = aDiagnostic;
    }

    public Diagnostic getDiagnostic() {
        return itsDiagnostic;
    }
}
