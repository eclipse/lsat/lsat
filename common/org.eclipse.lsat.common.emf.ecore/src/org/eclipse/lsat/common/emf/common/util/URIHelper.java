/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.annotation.NonNull;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public final class URIHelper {
    public static final long UNKNOWN_CONTENT_LENGTH = IProgressMonitor.UNKNOWN;

    private URIHelper() {
        // Empty
    }

    /**
     * Returns resource name without file extension.
     *
     * @param resource
     * @return
     */
    public static final String baseName(IResource resource) {
        final String name = resource.getName();
        final String extension = resource.getFileExtension();
        return null == extension ? name : name.substring(0, name.length() - extension.length() - 1);
    }

    /**
     * Returns file name without file extension.
     *
     * @param file
     * @return
     */
    public static final String baseName(File file) {
        final String name = file.getName();
        return name.indexOf('.') < 0 ? name : name.substring(0, name.lastIndexOf('.'));
    }

    /**
     * Returns the last segment of the URI without its file extension.
     *
     * @param uri the uri
     * @return the last segment of the URI without its {@link URI#fileExtension() file extension}
     * @throws UnsupportedURIException if uri is not hierarchical
     */
    public static final String baseName(URI uri) throws UnsupportedURIException {
        if (!uri.isHierarchical()) {
            throw new UnsupportedURIException("Only hierarchical URI's are supported: " + uri);
        }
        return uri.trimFileExtension().lastSegment();
    }

    /**
     * Finds a resource with a given name and converts it to an {@link URI}. If Eclipse is running a platform-plugin URI
     * is returned else the resource is searched on the classpath and its URL is returned.
     *
     * <p>
     * Before delegation, an absolute resource name is constructed from the given resource name using this algorithm:
     * </p>
     * <ul>
     *
     * <li>If the {@code name} begins with a {@code '/'} (<tt>'&#92;u002f'</tt>), then the absolute name of the resource
     * is the portion of the {@code name} following the {@code '/'}.</li>
     *
     * <li>Otherwise, the absolute name is of the following form:
     *
     * <blockquote> {@code modified_package_name/name} </blockquote>
     *
     * <p>
     * Where the {@code modified_package_name} is the package name of <tt>clazz</tt> with {@code '/'} substituted for
     * {@code '.'} (<tt>'&#92;u002e'</tt>).
     * </p>
     * </li>
     * </ul>
     *
     * @param clazz associated class for the resource
     * @param name resourceName of the desired resource
     * @see Class#getResource(String)
     */
    public static URI asURI(Class<?> clazz, String resourceName) {
        if (!Platform.isRunning()) {
            return URIHelper.asURI(clazz.getResource(resourceName));
        }
        final Bundle bundle = FrameworkUtil.getBundle(clazz);
        if (null == bundle) {
            return null;
        }
        final String pathName = "/" + bundle.getSymbolicName() + "/" + resolveName(clazz, resourceName);
        return URI.createPlatformPluginURI(pathName, true);
    }

    /**
     * Add a package name prefix if the name is not absolute Remove leading "/" if name is absolute.
     * <p>
     * Copied from {@link Class}.resolveName(java.lang.String)
     * </p>
     *
     * @see Class
     */
    private static String resolveName(Class<?> clazz, String resourceName) {
        if (resourceName == null) {
            return resourceName;
        }
        if (!resourceName.startsWith("/")) {
            Class<?> c = clazz;
            while (c.isArray()) {
                c = c.getComponentType();
            }
            String baseName = c.getName();
            int index = baseName.lastIndexOf('.');
            if (index != -1) {
                resourceName = baseName.substring(0, index).replace('.', '/') + "/" + resourceName;
            }
        } else {
            resourceName = resourceName.substring(1);
        }
        return resourceName;
    }

    /**
     * Converts an {@link URL} to a platform resource {@link URI}
     */
    public static URI asURI(URL url) {
        if (null == url) {
            return null;
        }
        return URI.createURI(url.toString());
    }

    /**
     * Converts an {@link IResource} to a platform resource {@link URI}
     *
     * @param resource
     * @return
     */
    public static URI asURI(IResource resource) {
        if (null == resource) {
            return null;
        }
        return URI.createPlatformResourceURI(resource.getFullPath().toString(), true);
    }

    /**
     * Converts a {@link File} to an {@link URI} (platform resource if possible)
     *
     * @param file
     * @return
     */
    public static URI asURI(File file) {
        if (null == file) {
            return null;
        }
        IResource resource = asIResource(file);
        return null == resource ? URI.createFileURI(file.getAbsolutePath()) : asURI(resource);
    }

    /**
     * Converts a platform resource {@link URI} to an {@link IResource}.
     *
     * @param uri the uri
     * @return the resource if available in the workspace.
     * @throws UnsupportedURIException if <tt>uri</tt> is not a platform resource or file URI
     */
    public static IResource asIResource(URI uri) throws UnsupportedURIException {
        if (null == uri) {
            return null;
        }
        if (uri.isPlatformResource()) {
            return getWorkspaceRoot().findMember(uri.toPlatformString(true), true);
        }
        if (uri.isFile()) {
            return asIResource(new File(uri.toFileString()));
        }
        throw new UnsupportedURIException("URI scheme is not supported: " + uri);
    }

    /**
     * Converts a {@link File} to an {@link IResource}.
     *
     * @param file
     * @return The corresponding IResource or null if the file cannot be found within the workspace.
     */
    public static IResource asIResource(File file) {
        if (null == file) {
            return null;
        }
        final IPath fileLocation = Path.fromOSString(file.getAbsolutePath());
        final IWorkspaceRoot workspaceRoot = getWorkspaceRoot();
        final IProject[] projects = workspaceRoot.getProjects();

        IResource resource = findIResource(fileLocation, workspaceRoot);
        for (int i = 0; null == resource && i < projects.length; i++) {
            resource = findIResource(fileLocation, projects[i]);
        }
        return resource;
    }

    /**
     * Converts an {@link URI} to a {@link File}.
     *
     * @param uri the uri
     * @return the file if <tt>uri</tt> represents a location on the file system
     * @throws UnsupportedURIException if <tt>uri</tt> is not a platform resource or file URI
     */
    public static File asFile(URI uri) throws UnsupportedURIException {
        if (null == uri) {
            return null;
        }
        if (uri.isPlatformResource()) {
            return asFile(asIResource(uri));
        }
        if (uri.isFile()) {
            return new File(uri.toFileString());
        }
        throw new UnsupportedURIException("URI scheme is not supported: " + uri);
    }

    /**
     * Converts an {@link IResource} to a {@link File}.
     *
     * @param resource
     * @return
     */
    public static File asFile(IResource resource) {
        if (null == resource) {
            return null;
        }
        final IPath location = resource.getLocation();
        if (null == location) {
            return null;
        }
        return location.toFile();
    }

    /**
     * Adds a decoration scheme part to the URI.
     *
     * @param uri <tt>uri</tt> if uri is already decorated with <tt>decorationScheme</tt> and the decorated URI
     *     otherwise.
     * @param decorationScheme decoration scheme part to add.
     * @return the decorated uri.
     * @see #isSchemeDecoratedWith(URI, String)
     */
    public static URI decorateScheme(URI uri, String decorationScheme) {
        if (isSchemeDecoratedWith(uri, decorationScheme)) {
            // Nothing to do
            return uri;
        } else if (null == uri.scheme()) {
            return URI.createURI(decorationScheme + ':' + uri);
        } else {
            return URI.createURI(decorationScheme + '+' + uri);
        }
    }

    /**
     * Removes the decoration scheme part from the URI.
     *
     * @param uri <tt>uri</tt> if uri is not decorated with <tt>decorationScheme</tt> and the undecorated URI otherwise.
     * @param decorationScheme decoration scheme part to remove.
     * @return the undecorated uri.
     * @see #isSchemeDecoratedWith(URI, String)
     */
    public static URI undecorateScheme(URI uri, String decorationScheme) {
        if (!isSchemeDecoratedWith(uri, decorationScheme)) {
            // Nothing to do
            return uri;
        }
        // +1: strip either the ':' (removes scheme) or '+' (removes decorationScheme part from scheme)
        return URI.createURI(uri.toString().substring(decorationScheme.length() + 1));
    }

    /**
     * A scheme of an URI may consist out of multiple scheme parts separated with a '+' sign (e.g.:
     * secure+http://dummy). The first scheme part is called a decoration of the remainder scheme parts (if any).
     *
     * @param uri URI to test.
     * @param decorationScheme decoration scheme part to test for.
     * @return <tt>true</tt> if the URI is decorated with the specified scheme part, <tt>false</tt> otherwise.
     * @see URIHelper#decorateScheme(URI, String)
     * @see #undecorateScheme(URI, String)
     */
    public static boolean isSchemeDecoratedWith(URI uri, String decorationScheme) {
        if (null == decorationScheme || !URI.validScheme(decorationScheme)) {
            throw new IllegalArgumentException("Not a valid scheme: " + decorationScheme);
        }
        final String scheme = uri.scheme();
        return null != scheme && (decorationScheme.equals(scheme) || scheme.startsWith(decorationScheme + '+'));
    }

    /**
     * Returns the URI of the parent IResource of this URI. If no parent, this URI is returned unchanged.
     *
     * @param uri child
     * @return parent
     * @see URI#trimSegments(int)
     */
    public static URI getParent(URI uri) {
        return uri.trimSegments(1);
    }

    /**
     * Returns the URI of the project containing the uri resource.<br>
     * Equivalent to {@link #asIResource(URI)} {@link IResource#getProject()} {@link #asURI(IResource)}, but optimized
     * for platform resource URI.
     *
     * @param uri resource
     * @return project URI
     * @throws UnsupportedURIException if <tt>uri</tt> is not a platform resource or file URI
     * @see #asIResource(File)
     * @see #asURI(IResource)
     */
    public static URI getProject(URI uri) throws UnsupportedURIException {
        if (null == uri) {
            return null;
        }
        if (uri.isPlatformResource()) {
            return uri.trimSegments(uri.segmentCount() - 2);
        }
        if (uri.isFile()) {
            final IResource resource = asIResource(new File(uri.toFileString()));
            return null == resource ? null : asURI(resource.getProject());
        }
        throw new UnsupportedURIException("URI scheme is not supported: " + uri);
    }

    /**
     * Returns the File of the project containing the file resource.<br>
     * Equivalent to {@link #asIResource(File)} {@link IResource#getProject()} {@link #asFile(IResource)}, but save.
     *
     * @param file
     * @return the project file, or <tt>null</tt> if the file cannot be found within the workspace.
     * @see #asIResource(File)
     */
    public static File getProject(File file) {
        if (null == file) {
            return null;
        }
        final IResource resource = asIResource(file);
        return null == resource ? null : asFile(resource.getProject());
    }

    /**
     * Returns the length of the content of {@code uri} if available, or {@link #UNKNOWN_CONTENT_LENGTH} otherwise.
     *
     * @param uri Ecore URI to query for its content length.
     *
     * @return The length of the content of {@code uri} if available, or {@link #UNKNOWN_CONTENT_LENGTH} otherwise.
     * @throws UnsupportedURIException If the {@code uri} <i>should</i> provide a content length, but actually does not.
     */
    public static long determineContentLength(final @NonNull URI uri) throws UnsupportedURIException {
        Assert.isNotNull(uri, "uri must not be null");

        if (uri.isPlatformResource()) {
            return determineContentLength(asIResource(uri));
        } else if (uri.isFile()) {
            return determineContentLength(asFile(uri));
        }

        return UNKNOWN_CONTENT_LENGTH;
    }

    /**
     * Returns the length of the content of {@code resource} if available, or {@link #UNKNOWN_CONTENT_LENGTH} otherwise.
     *
     * @param resource Eclipse IResource to query for its content length.
     *
     * @return The length of the content of {@code resource} if {@code resource} exists, or
     *     {@link #UNKNOWN_CONTENT_LENGTH} otherwise.
     * @throws UnsupportedURIException If the {@code resource} <i>should</i> provide a content length, but actually does
     *     not.
     */
    public static long determineContentLength(final @NonNull IResource resource) throws UnsupportedURIException {
        Assert.isNotNull(resource, "resource must not be null");

        try {
            if (!resource.exists()) {
                return UNKNOWN_CONTENT_LENGTH;
            }

            return EFS.getStore(resource.getLocationURI()).fetchInfo(EFS.SET_ATTRIBUTES, null).getLength();
        } catch (final CoreException e) {
            throw new UnsupportedURIException(e);
        }
    }

    /**
     * Returns the length of the content of {@code file} if available, or {@link #UNKNOWN_CONTENT_LENGTH} otherwise.
     *
     * @param file Java File to query for its content length.
     *
     * @return The length of the content of {@code file} if {@code file} exists, or {@link #UNKNOWN_CONTENT_LENGTH}
     *     otherwise.
     */
    public static long determineContentLength(final @NonNull File file) {
        Assert.isNotNull(file, "file must not be null");

        if (!file.exists()) {
            return UNKNOWN_CONTENT_LENGTH;
        }

        return file.length();
    }

    /**
     * Returns a map of decoded key-value pairs which represent the query parameters of <tt>uri</tt>.
     *
     * @param uri URI to retrieve {@link URI#query()} from.
     * @return a map of decoded key-value pairs which represent the query parameters of <tt>uri</tt>.
     */
    public static Map<String, String> getQueryParameters(URI uri) {
        final String query = uri.query();
        if (null == query) {
            return Collections.emptyMap();
        }
        try {
            String[] paramParts = query.split("&");
            Map<String, String> parameters = new LinkedHashMap<String, String>(paramParts.length);
            for (String paramPart: paramParts) {
                String[] keyValue = paramPart.split("=", 2);
                String value = keyValue.length < 2 ? null
                        : URLDecoder.decode(keyValue[1], StandardCharsets.UTF_8.toString());
                parameters.put(keyValue[0], value);
            }
            return parameters;
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Programming error: " + e.getMessage(), e);
        }
    }

    /**
     * Encodes and joins a map of key-value pairs as a query part and appends it to <tt>uri</tt>.
     *
     * @param uri uri to {@link URI#appendQuery(String)}
     * @param parameter map of key-value pairs to encode as query part
     * @return the uri including the appended query part
     */
    public static URI setQueryParameters(URI uri, Map<String, String> parameters) {
        String query = parameters.entrySet().stream().map(URIHelper::encodeQueryParameter)
                .collect(Collectors.joining("&"));
        return uri.appendQuery(query);
    }

    private static String encodeQueryParameter(Map.Entry<String, String> param) {
        try {
            StringBuilder paramString = new StringBuilder();
            paramString.append(param.getKey());
            if (null != param.getValue()) {
                paramString.append('=').append(URLEncoder.encode(param.getValue(), StandardCharsets.UTF_8.toString()));
            }
            return paramString.toString();
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Programming error: " + e.getMessage(), e);
        }
    }

    private static IResource findIResource(IPath resourceLocation, IContainer container) {
        final IPath containerLocation = container.getLocation();
        if (!containerLocation.isPrefixOf(resourceLocation)) {
            return null;
        }
        final IPath relativeLocation = resourceLocation.removeFirstSegments(containerLocation.segmentCount());
        return container.findMember(relativeLocation, true);
    }

    private static final IWorkspaceRoot getWorkspaceRoot() {
        return ResourcesPlugin.getWorkspace().getRoot();
    }

    public static void main(String[] args) {
        System.out.println(baseName(new File("File.txt")));
        System.out.println(baseName(URI.createFileURI("URI.txt")));
    }
}
