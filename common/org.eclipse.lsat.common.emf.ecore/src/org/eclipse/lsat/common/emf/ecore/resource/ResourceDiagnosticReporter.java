/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import static org.eclipse.lsat.common.emf.ecore.resource.ResourceDiagnosticException.UNKNOWN_COLUMN;
import static org.eclipse.lsat.common.emf.ecore.resource.ResourceDiagnosticException.Severity.ERROR;
import static org.eclipse.lsat.common.emf.ecore.resource.ResourceDiagnosticException.Severity.WARNING;

public interface ResourceDiagnosticReporter {
    /**
     * Reports a warning for line at <tt>lineNr</tt>. This warning is for the entire line, or there is no specific
     * column information available.
     */
    default void reportWarning(String message, int lineNr) {
        reportProblem(new ResourceDiagnosticException(WARNING, null, UNKNOWN_COLUMN, lineNr, message, null));
    }

    /**
     * Reports a warning for line at <tt>lineNr</tt>.
     */
    default void reportWarning(String message, int lineNr, int column) {
        reportProblem(new ResourceDiagnosticException(WARNING, null, column, lineNr, message, null));
    }

    /**
     * Reports an error for line at <tt>lineNr</tt>. This error is for the entire line, or there is no specific column
     * information available.
     */
    default void reportError(String message, int lineNr) {
        reportProblem(new ResourceDiagnosticException(ERROR, null, UNKNOWN_COLUMN, lineNr, message, null));
    }

    /**
     * Reports an error for line at <tt>lineNr</tt>.
     */
    default void reportError(String message, int lineNr, int column) {
        reportProblem(new ResourceDiagnosticException(ERROR, null, column, lineNr, message, null));
    }

    /**
     * Reports a {@link ResourceDiagnosticException}.
     */
    void reportProblem(ResourceDiagnosticException ex);
}
