/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import java.io.IOException;

public class ResourceIOException extends IOException {
    private static final long serialVersionUID = 346332256617932116L;

    public ResourceIOException() {
        super();
    }

    public ResourceIOException(String message) {
        super(message);
    }

    public ResourceIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceIOException(Throwable cause) {
        super(cause);
    }
}
