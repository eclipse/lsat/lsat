/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

/**
 * Convenience wrapper around a {@link ResourceSet} that allows to specify or manually detect a model repository
 * context.
 */
public class PersistorFactory {
    private final ResourceSet itsResourceSet;

    public PersistorFactory() {
        this(createDefaultResourceSet());
    }

    public PersistorFactory(ResourceSet aResourceSet) {
        itsResourceSet = aResourceSet;
    }

    public Persistor<EObject> getPersistor() {
        return getPersistor(EObject.class);
    }

    public <T extends EObject> Persistor<T> getPersistor(Class<T> aType) {
        return getPersistor(aType, false);
    }

    public <T extends EObject> Persistor<T> getPersistor(Class<T> aType, boolean aMultipleRootTypesAllowed) {
        return getPersistor(aType, aMultipleRootTypesAllowed, false);
    }

    public <T extends EObject> Persistor<T> getPersistor(Class<T> aType, boolean aMultipleRootTypesAllowed,
            boolean aIgnoreProblems)
    {
        return new Persistor<T>(itsResourceSet, aType, aMultipleRootTypesAllowed, aIgnoreProblems);
    }

    public ResourceSet getResourceSet() {
        return itsResourceSet;
    }

    public void unloadAll() {
        for (Resource resource: itsResourceSet.getResources()) {
            resource.unload();
        }
    }

    private static ResourceSet createDefaultResourceSet() {
        ResourceSetImpl resourceSet = new ResourceSetImpl();
        resourceSet.setURIResourceMap(new HashMap<URI, Resource>());
        return resourceSet;
    }
}
