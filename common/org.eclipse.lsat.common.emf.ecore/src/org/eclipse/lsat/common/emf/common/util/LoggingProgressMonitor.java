/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link IProgressMonitor} that logs all task names and optionally delegates to another {@link IProgressMonitor}.
 *
 */
public class LoggingProgressMonitor implements IProgressMonitor {
    public static enum LogLevel {
        Trace, Debug, Info, Warn, Error
    }

    private static final IProgressMonitor DEV_NULL = new NullProgressMonitor();

    private final IProgressMonitor delegate;

    private final Logger logger;

    private final LogLevel level;

    public LoggingProgressMonitor() {
        this(LoggerFactory.getLogger(LoggingProgressMonitor.class), LogLevel.Debug, DEV_NULL);
    }

    public LoggingProgressMonitor(Logger logger, LogLevel level) {
        this(logger, level, DEV_NULL);
    }

    public LoggingProgressMonitor(Logger logger, LogLevel level, IProgressMonitor delegate) {
        this.logger = logger;
        this.level = level;
        this.delegate = delegate;
    }

    protected void log(String msg) {
        switch (level) {
            case Trace:
                logger.trace(msg);
                break;
            case Debug:
                logger.debug(msg);
                break;
            case Info:
                logger.info(msg);
                break;
            case Warn:
                logger.warn(msg);
                break;
            case Error:
                logger.error(msg);
                break;
        }
    }

    @Override
    public void beginTask(String name, int totalWork) {
        log(name);
        delegate.beginTask(name, totalWork);
    }

    @Override
    public void done() {
        delegate.done();
    }

    @Override
    public void internalWorked(double work) {
        delegate.internalWorked(work);
    }

    @Override
    public boolean isCanceled() {
        return delegate.isCanceled();
    }

    @Override
    public void setCanceled(boolean value) {
        delegate.setCanceled(value);
    }

    @Override
    public void setTaskName(String name) {
        log(name);
        delegate.setTaskName(name);
    }

    @Override
    public void subTask(String name) {
        log(name);
        delegate.subTask(name);
    }

    @Override
    public void worked(int work) {
        delegate.worked(work);
    }
}
