/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;

/**
 * @see <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=509675">Eclipse bug 509675</a>
 */
public class FastContainsEList<E> extends BasicEList<E> {
    static final int DEFAULT_INITIAL_CAPACITY = 16;

    private static final long serialVersionUID = 1L;

    private final Set<E> set;

    private transient Integer expectedModCount;

    public FastContainsEList() {
        this(DEFAULT_INITIAL_CAPACITY);
    }

    public FastContainsEList(int initialCapacity) {
        this(initialCapacity, new HashSet<E>(initialCapacity));
    }

    protected FastContainsEList(int initialCapacity, Set<E> set) {
        super(initialCapacity);
        this.set = set;
    }

    @Override
    public boolean contains(Object object) {
        if (null == expectedModCount || modCount != expectedModCount) {
            set.clear();
            set.addAll(this);
            expectedModCount = modCount;
        }
        return set.contains(object);
    }

    public static class FastCompare<E> extends FastContainsEList<E> {
        private static final long serialVersionUID = 1L;

        public FastCompare() {
            this(DEFAULT_INITIAL_CAPACITY);
        }

        public FastCompare(int initialCapacity) {
            super(initialCapacity, Collections.newSetFromMap(new IdentityHashMap<E, Boolean>(initialCapacity)));
        }
    }
}
