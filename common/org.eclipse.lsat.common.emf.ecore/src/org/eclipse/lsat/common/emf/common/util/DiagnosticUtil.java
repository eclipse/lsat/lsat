/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.util;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.slf4j.Logger;

public class DiagnosticUtil {
    private DiagnosticUtil() {
        // Empty
    }

    /**
     * Logs all aDiagnostics and all their children on the appropriate level(s).
     *
     * @param aDiagnostics
     * @param aLogger
     */
    public static void logFull(Iterable<Diagnostic> aDiagnostics, Logger aLogger) {
        if (null == aDiagnostics || null == aLogger) {
            return;
        }
        for (Diagnostic diagnostic: aDiagnostics) {
            logFull(diagnostic, aLogger);
        }
    }

    /**
     * Logs aDiagnostic and all its children on the appropriate level(s).
     *
     * @param aDiagnostic
     * @param aLogger
     */
    public static void logFull(Diagnostic aDiagnostic, Logger aLogger) {
        if (null == aDiagnostic || null == aLogger) {
            return;
        }
        switch (aDiagnostic.getSeverity()) {
            case (Diagnostic.CANCEL):
            case (Diagnostic.ERROR):
                aLogger.error(aDiagnostic.getMessage(), aDiagnostic.getException());
                break;
            case (Diagnostic.WARNING):
                aLogger.warn(aDiagnostic.getMessage(), aDiagnostic.getException());
                break;
            case (Diagnostic.INFO):
                aLogger.info(aDiagnostic.getMessage(), aDiagnostic.getException());
                break;
            case (Diagnostic.OK):
                aLogger.debug(aDiagnostic.getMessage(), aDiagnostic.getException());
                break;
            default:
                aLogger.error("Unknown severity " + aDiagnostic.getSeverity() + ": " + aDiagnostic.getMessage(),
                        aDiagnostic.getException());
                break;
        }
        logFull(aDiagnostic.getChildren(), aLogger);
    }

    public static String getFullMessage(BufferedDiagnosticChain diagnosticsChain) {
        return getFullMessage(diagnosticsChain.getDiagnostics());
    }

    public static String getFullMessage(Iterable<Diagnostic> diagnostics) {
        StringBuffer str = new StringBuffer();
        for (Iterator<Diagnostic> i = diagnostics.iterator(); i.hasNext();) {
            str.append(getFullMessage(i.next(), "- "));
            if (i.hasNext()) {
                str.append('\n');
            }
        }
        return str.toString();
    }

    /**
     * @param aDiagnostic
     * @return the message of the diagnostic including its child messages
     */
    public static String getFullMessage(Diagnostic aDiagnostic) {
        return getFullMessage(aDiagnostic, "");
    }

    private static String getFullMessage(Diagnostic aDiagnostic, String aPrefix) {
        StringBuffer str = new StringBuffer();
        str.append(aPrefix);
        str.append(getSeverity(aDiagnostic)).append(": ");
        str.append(aDiagnostic.getMessage());

        String childPrefix = aPrefix.length() == 0 ? "- " : "  " + aPrefix;
        // Filter out duplicate messages (can occur in model validations)
        LinkedHashSet<String> childMessages = new LinkedHashSet<String>();
        for (Diagnostic child: aDiagnostic.getChildren()) {
            childMessages.add(getFullMessage(child, childPrefix));
        }
        for (String childMsg: childMessages) {
            str.append('\n').append(childMsg);
        }
        return str.toString();
    }

    public static String getSeverity(Diagnostic aDiagnostic) {
        switch (aDiagnostic.getSeverity()) {
            case Diagnostic.CANCEL:
                return "CANCEL";
            case Diagnostic.ERROR:
                return "ERROR";
            case Diagnostic.INFO:
                return "INFO";
            case Diagnostic.OK:
                return "OK";
            case Diagnostic.WARNING:
                return "WARNING";
            default:
                return "Unknown";
        }
    }

    /**
     * This method differs from {@link BasicDiagnostic#toIStatus(Diagnostic)} in the sense that it will NOT return
     * wrapped diagnostics. The benefit of this is that the referred {@link EObject}s can be released from memory when
     * the diagnostic is converted to IStatus.
     *
     * @see BasicDiagnostic#toIStatus(Diagnostic)
     */
    public static IStatus toIStatus(Diagnostic diagnostic) {
        return new DiagnosticStatus(diagnostic);
    }

    private static class DiagnosticStatus implements IStatus {
        final int severity;

        final int code;

        final String pluginId;

        final String message;

        final Throwable exception;

        final IStatus[] children;

        private DiagnosticStatus(Diagnostic diagnostic) {
            // Do NOT keep the diagnostic, but copy all its information
            severity = diagnostic.getSeverity();
            code = diagnostic.getCode();
            pluginId = diagnostic.getSource();
            message = diagnostic.getMessage();
            exception = diagnostic.getException();

            List<Diagnostic> diagnosticChildren = diagnostic.getChildren();
            children = new IStatus[diagnosticChildren.size()];
            for (int i = 0; i < children.length; i++) {
                children[i] = new DiagnosticStatus(diagnosticChildren.get(i));
            }
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public Throwable getException() {
            return exception;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public String getPlugin() {
            return pluginId;
        }

        @Override
        public int getSeverity() {
            return severity;
        }

        @Override
        public boolean isMultiStatus() {
            return children.length > 0;
        }

        @Override
        public IStatus[] getChildren() {
            return children;
        }

        @Override
        public boolean isOK() {
            return severity == OK;
        }

        @Override
        public boolean matches(int severityMask) {
            return (severity & severityMask) != 0;
        }
    }
}
