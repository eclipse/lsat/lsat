/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ecore.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ContentHandler;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.emf.common.util.DiagnosticUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public final class Persistor<T extends EObject> {
    private static final Logger LOGGER = LoggerFactory.getLogger(Persistor.class);

    private static final Marker LOG_MARKER = MarkerFactory.getMarker("PERSIST");

    private final ResourceSet resourceSet;

    private final Class<T> type;

    private final boolean multipleRootTypesAllowed;

    private final boolean ignoreProblems;

    Persistor(ResourceSet resourceSet, Class<T> type, boolean multipleRootTypesAllowed, boolean ignoreProblems) {
        this.resourceSet = resourceSet;
        this.type = type;
        this.multipleRootTypesAllowed = multipleRootTypesAllowed;
        this.ignoreProblems = ignoreProblems;
    }

    public T loadOne(URI aUri) throws IOException {
        return loadOne(aUri, null);
    }

    public T loadOne(URI aUri, Map<?, ?> options) throws IOException {
        // Get the root object
        List<T> contents = loadAll(aUri, options);
        if (contents.size() != 1) {
            throw new ResourceIOException(
                    "Number of models in file '" + aUri + "' not equal to 1; actual number: " + contents.size());
        }
        return contents.iterator().next();
    }

    public List<T> loadAll(URI aUri) throws IOException {
        return loadAll(aUri, null);
    }

    @SuppressWarnings("unchecked")
    public List<T> loadAll(URI aUri, Map<?, ?> options) throws IOException {
        try {
            // get resource, load resource
            Resource resource = resourceSet.getResource(aUri, false);
            if (null == resource) {
                resource = resourceSet.createResource(aUri, ContentHandler.UNSPECIFIED_CONTENT_TYPE);
            }
            resource.load(options);

            org.eclipse.emf.common.util.Diagnostic resourceDiagnostic = EcoreUtil.computeDiagnostic(resource, true);
            // If errors should be ignored, the user is taking control hence also skip logging
            if (!ignoreProblems) {
                if (resourceDiagnostic.getSeverity() >= org.eclipse.emf.common.util.Diagnostic.ERROR) {
                    throw new ResourceIOException("Failed to load resource with the following message(s):\n"
                            + DiagnosticUtil.getFullMessage(resourceDiagnostic.getChildren()));
                } else {
                    DiagnosticUtil.logFull(resourceDiagnostic.getChildren(), LOGGER);
                }
            }

            EList<EObject> rawContents = resource.getContents();
            if (type == null || EObject.class.equals(type) || !multipleRootTypesAllowed) {
                // Old fashioned way, just cast
                // We might want to do this nicer by checking and throwing a
                // well formatted message
                return (EList<T>)rawContents;
            }

            ArrayList<T> contents = new ArrayList<T>(rawContents.size());
            for (EObject eObject: rawContents) {
                if (type.isInstance(eObject)) {
                    contents.add(type.cast(eObject));
                }
            }
            return contents;
        } catch (WrappedException e) {
            if (e.getCause() instanceof IOException) {
                throw (IOException)e.getCause();
            }
            throw e;
        }
    }

    @SafeVarargs
    public final void save(URI aUri, EObject... eObjects) throws IOException {
        save(aUri, null, false, Arrays.asList(eObjects));
    }

    @SafeVarargs
    public final void save(URI aUri, Collection<? extends EObject>... eObjectCollections) throws IOException {
        save(aUri, null, false, eObjectCollections);
    }

    @SafeVarargs
    public final void save(URI uri, boolean unload, Collection<? extends EObject>... eObjectCollections)
            throws IOException
    {
        save(uri, null, unload, eObjectCollections);
    }

    @SafeVarargs
    public final void save(URI aUri, Map<?, ?> options, EObject... eObjects) throws IOException {
        save(aUri, options, false, Arrays.asList(eObjects));
    }

    @SafeVarargs
    public final void save(URI aUri, Map<?, ?> options, Collection<? extends EObject>... eObjectCollections)
            throws IOException
    {
        save(aUri, options, false, eObjectCollections);
    }

    @SafeVarargs
    public final void save(URI uri, Map<?, ?> options, boolean unload,
            Collection<? extends EObject>... eObjectCollections) throws IOException
    {
        Resource resource = resourceSet.createResource(uri);

        for (Collection<? extends EObject> eObjects: eObjectCollections) {
            resource.getContents().addAll(eObjects);
        }
        LOGGER.debug(LOG_MARKER, "Starting: Writing {}", uri.lastSegment());
        // save
        resource.save(options);

        if (unload) {
            resource.unload();
        }
        LOGGER.debug(LOG_MARKER, "Finished: Writing {}", uri.lastSegment());
    }
}
