/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.lsat.common.emf.common.util.DiagnosticException;
import org.eclipse.lsat.common.emf.common.util.DiagnosticUtil;
import org.eclipse.lsat.common.qvto.util.internal.Log4Slf4jLog;
import org.eclipse.lsat.common.qvto.util.internal.TraceTransformatorExecutor;
import org.eclipse.m2m.qvt.oml.ExecutionContext;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.util.Log;
import org.eclipse.ocl.ecore.EcoreEnvironment;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public abstract class AbstractModelTransformer<Input, Output> {
    // Workaround for bug in OCL < 4.0
    // Registers a dummy URI for the oclStdlib URI,
    static {
        URIConverter.URI_MAP.put(URI.createURI(EcoreEnvironment.OCL_STANDARD_LIBRARY_NS_URI),
                URI.createURI("no-such-protocol:/this/does/not/exist"));
    }

    public static final String QVT_MARKER_STR = "QVTORunner";

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final HashMap<String, Object> itsConfigProperties = new HashMap<String, Object>();

    private TraceTransformatorExecutor itsExecutor = null;

    protected IProgressMonitor itsMonitor = null;

    public void setConfigProperty(String aName, Object aValue) {
        itsConfigProperties.put(aName, aValue);
    }

    public void setTraceURI(URI aTraceURI) {
        getExecutor().setTraceURI(aTraceURI);
    }

    /**
     * Placeholder for subclasses to call {@link #registerImportedUnit(String)} for their imports.
     *
     * @throws QvtoTransformationException
     */
    protected void registerImportedUnits() throws QvtoTransformationException {
        // Empty
    }

    protected final void registerImportedUnit(String unit) throws QvtoTransformationException {
        // FIXME fix Qvto to register units

        // Lookup imported unit on the classpath.
        URL unitURL = this.getClass().getResource(unit);
        if (unitURL == null) {
            throw new QvtoTransformationException("Imported unit could not be found in classpath, unit:" + unit);
        }

        URI unitURI = URI.createURI(unitURL.toString());

        // Append imported unit to the directory of the uri of the
        // transformations.
        URI tformationURI = getTransformationURI();
        URI baseURI = tformationURI.trimSegments(1);
        URI resolvedUnitURI = baseURI.appendSegments(URI.createURI(unit).segments());

        // Add to the uri map, so the transformation can find the imported unit.
        URIConverter.URI_MAP.put(resolvedUnitURI, unitURI);
    }

    protected void execute(ModelExtent... aModelParameters) throws QvtoTransformationException {
        final Log4Slf4jLog omlLogger = new Log4Slf4jLog(LoggerFactory.getLogger(logger.getName() + ".qvto"));

        registerImportedUnits();

        @SuppressWarnings("restriction")
        ExecutionDiagnostic diagnostic = getExecutor().execute(createExecutionContext(omlLogger, itsMonitor),
                aModelParameters);

        DiagnosticUtil.logFull(diagnostic, logger);
        if (diagnostic.getCode() == ExecutionDiagnostic.FATAL_ASSERTION && omlLogger.hasLastMessage()
                && omlLogger.getLastMessage().isAssert())
        {
            throw new QvtoTransformationException(omlLogger.getLastMessage().getMessage(),
                    new ExecutionDiagnosticException(diagnostic));
        }
        if (diagnostic.getSeverity() >= Diagnostic.ERROR) {
            throw new QvtoTransformationException(new ExecutionDiagnosticException(diagnostic));
        }
    }

    protected void doValidate(EObject eObject) throws QvtoTransformationException {
        Diagnostic diagnostic = new Diagnostician().validate(eObject);
        DiagnosticUtil.logFull(diagnostic, logger);
        if (diagnostic.getSeverity() >= Diagnostic.WARNING) {
            throw new QvtoTransformationException(new DiagnosticException(diagnostic));
        }
    }

    protected abstract String getDefaultTransformation();

    protected URI getTransformationURI() {
        String defaultTransformation = getDefaultTransformation();
        Bundle bundle = FrameworkUtil.getBundle(getClass());
        if (null == bundle) {
            // Load default transformation from class-path via the class-loader
            URL url = this.getClass().getResource(defaultTransformation);
            return URI.createURI(url.toString());
        } else {
            // Try using platform plugin uri's when possible
            return URI.createPlatformPluginURI(bundle.getSymbolicName() + defaultTransformation, true);
        }
    }

    protected TraceTransformatorExecutor getExecutor() {
        // Cache the executor
        if (itsExecutor != null) {
            return itsExecutor;
        }

        final URI transformationURI = getTransformationURI();
        logger.debug("Loading transformation: " + transformationURI);
        // Create executor for the given transformation
        itsExecutor = new TraceTransformatorExecutor(transformationURI);
        return itsExecutor;
    }

    protected ExecutionContext createExecutionContext(Log aLogger, IProgressMonitor monitor) {
        ExecutionContextImpl context = new ExecutionContextImpl();
        if (monitor != null) {
            context.setProgressMonitor(monitor);
        }
        context.setLog(aLogger);
        for (Map.Entry<String, Object> property: itsConfigProperties.entrySet()) {
            context.setConfigProperty(property.getKey(), property.getValue());
        }
        return context;
    }

    public final synchronized Output transformModel(Input input, IProgressMonitor monitor)
            throws QvtoTransformationException
    {
        final Marker qvtMarker = MarkerFactory.getMarker(QVT_MARKER_STR);
        itsMonitor = SubMonitor.convert(monitor);
        logger.debug(qvtMarker, "Starting transformation");
        Output output = doTransformModel(input);
        logger.debug(qvtMarker, "Finished transformation");
        return output;
    }

    protected abstract Output doTransformModel(Input input) throws QvtoTransformationException;

    protected <T extends EObject> List<T> validateMinMax(Class<T> type, ModelExtent modelExtent, int minCount,
            int maxCount) throws QvtoTransformationException
    {
        List<T> result = validateMany(type, modelExtent);
        if (result.size() < minCount) {
            throw new QvtoTransformationException(
                    "Expected at least " + minCount + " " + type.getSimpleName() + ": " + result);
        }
        if (result.size() > maxCount) {
            throw new QvtoTransformationException(
                    "Expected at most " + maxCount + " " + type.getSimpleName() + ": " + result);
        }
        return result;
    }

    protected <T extends EObject> T validateAtMostOne(Class<T> type, ModelExtent modelExtent)
            throws QvtoTransformationException
    {
        List<T> result = validateMinMax(type, modelExtent, 0, 1);
        return result.isEmpty() ? null : result.get(0);
    }

    protected <T extends EObject> List<T> validateAtLeastOne(Class<T> type, ModelExtent modelExtent)
            throws QvtoTransformationException
    {
        List<T> result = validateMinMax(type, modelExtent, 1, Integer.MAX_VALUE);
        return result;
    }

    protected <T extends EObject> T validateOneAndOnlyOne(Class<T> type, ModelExtent modelExtent)
            throws QvtoTransformationException
    {
        List<T> result = validateMinMax(type, modelExtent, 1, 1);
        return result.get(0);
    }

    @SuppressWarnings("unchecked")
    protected <T extends EObject> List<T> validateMany(Class<T> type, ModelExtent modelExtent)
            throws QvtoTransformationException
    {
        List<EObject> result = modelExtent.getContents();
        for (EObject object: result) {
            if (!type.isInstance(object)) {
                throw new QvtoTransformationException("Unexpected model extend content: " + result);
            }
        }
        return (List<T>)result;
    }
}
