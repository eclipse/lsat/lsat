/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util;

import org.eclipse.core.runtime.IStatus;

public class QvtoErrorStatusException extends QvtoTransformationException {
    private static final long serialVersionUID = 3894199931539871745L;

    private final IStatus status;

    public QvtoErrorStatusException(IStatus status) {
        super(status.getMessage(), status.getException());
        this.status = status;
        if (status.getSeverity() < IStatus.ERROR) {
            throw new IllegalArgumentException("Status severity should be at least ERROR", this);
        }
    }

    public IStatus getStatus() {
        return status;
    }
}
