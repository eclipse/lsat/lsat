/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util.internal;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.m2m.qvt.oml.util.Log;
import org.slf4j.helpers.MessageFormatter;

public abstract class AbstractQvtLog implements Log {
    protected enum LogLevel {
        Trace(IStatus.OK), Debug(IStatus.OK), Info(IStatus.INFO), Warning(IStatus.WARNING), Error(IStatus.ERROR),
        Fatal(IStatus.ERROR);

        private final int severity;

        private LogLevel(int severity) {
            this.severity = severity;
        }

        public int toSeverity() {
            return severity;
        }

        public static LogLevel valueOf(int aLevel) {
            final int levelInt = aLevel % 10;
            for (LogLevel level: LogLevel.values()) {
                if (level.ordinal() == levelInt) {
                    return level;
                }
            }
            return null;
        }
    }

    private enum LogParameterFormat {
        Data, Single, Multi;

        public static LogParameterFormat valueOf(int aLevel) {
            final int formatInt = aLevel / 10;
            for (LogParameterFormat format: LogParameterFormat.values()) {
                if (format.ordinal() == formatInt) {
                    return format;
                }
            }
            return null;
        }
    }

    protected abstract void doLog(LogLevel aLevel, String aFormat, Object... aArgs);

    protected String createMessage(String aFormat, Object... aArgs) {
        return MessageFormatter.arrayFormat(aFormat, aArgs).getMessage();
    }

    /**
     * <h2>How to use logging</h2> Using string concatenation to build log statements is discouraged. Instead, the
     * preferred method of using logging is by means of slf4j style logging. The logging string contains placeholders
     * for the data elements that need to be shown. <br>
     * This method avoids having to perform the string concatenation and evaluation of the arguments in case the
     * statement is not logged because of the logging level. Multiple parameters are passed by means of a sequence or a
     * list.<br>
     * There are three ways of logging. The logging engine will determine which style of logging to use depending on the
     * log level that is passed to the function.
     * <ul>
     * <li>Logging without parameter substitution (log level 1-5):<br>
     * This will log a string, optionally accompanied by the textual representation of a data object.<br>
     * <code>log("This is my log text", dataObject, logLevel);</code></li>
     * <li>Logging with a single parameter substitution (log level 11-15):<br>
     * This will log a string in which the textual representation of the data object is substituted at the position of
     * the curly braces.<br>
     * <code>log("This is the object: {}, followed by some more text.", dataObject, logLevel);</code></li>
     * <li>Logging with a multiple parameter substitution (log level 21-25):<br>
     * This will log a string in which a List or Sequence of parameters is substituted at the position of the curly
     * braces in the formatted string. There is no check on whether the list of parameters matches the number of braces
     * in the target string.<br>
     * <code>log("This is the object: {}, This is another object: {}.", List{dataObject1, dataObject2}, logLevel);</code></li>
     * </ul>
     * Note that when no log level is passed, it defaults to log level 1. placeholders in the log string will not be
     * filled out.<br>
     * <br>
     * The different debug levels are:
     * <ul>
     * <li>0-10-20 : Trace</li>
     * <li>1-11-21 : Debug</li>
     * <li>2-12-22 : Info</li>
     * <li>3-13-23 : Warning</li>
     * <li>4-14-24 : Error</li>
     * <li>5-15-25 : Fatal</li>
     * </ul>
     * The slf4j interface does not support fatal error logging. Therefore, these kinds of errors are logged as Error.
     * Trace logging is discouraged by the slf4j logging framework. For this reason, support for trace level logging has
     * been removed for QVTo transformations.
     */
    @Override
    public void log(int aLevel, String aMessage, Object aParam) {
        final LogLevel level = LogLevel.valueOf(aLevel);
        final LogParameterFormat paramFormat = LogParameterFormat.valueOf(aLevel);

        if (level == null || paramFormat == null) {
            doLog(LogLevel.Error,
                    "Unknown log level {} is used in the current QVTO transformation. Only log levels 0-5, 10-15 and 20-25 are allowed.\nThe log message is: {}\nThe parameter is: {}",
                    aLevel, aMessage, aParam);
            return;
        }

        switch (paramFormat) {
            case Data:
                doLog(level, "{}, data: {}", aMessage, aParam);
                break;
            case Single:
                doLog(level, aMessage, aParam);
                break;
            case Multi:
                if (aParam instanceof List) {
                    doLog(level, aMessage, List.class.cast(aParam).toArray());
                } else {
                    doLog(LogLevel.Error,
                            "Unrecognized datatype {} is passed to a log statement with format string substitution. Use a List or Sequence to pass multiple arguments.\nThe log message is: {}\nThe parameter is: {}",
                            aParam.getClass(), aMessage, aParam);
                }
                break;
        }
    }

    /**
     * @see #log(int, String, Object)
     */
    @Override
    public void log(int aLevel, String aMessage) {
        final LogLevel level = LogLevel.valueOf(aLevel);
        if (level == null) {
            doLog(LogLevel.Error,
                    "Unknown log level {} is used in the current QVTO transformation. Only log levels 0-5, 10-15 and 20-25 are allowed.\nThe log message is: {}",
                    aLevel, aMessage);
            return;
        }

        doLog(level, "{}", aMessage);
    }

    /**
     * @see #log(int, String, Object)
     */
    @Override
    public void log(String aMessage, Object aParam) {
        doLog(LogLevel.Debug, "{}, data: {}", aMessage, aParam);
    }

    /**
     * @see #log(int, String, Object)
     */
    @Override
    public void log(String aMessage) {
        doLog(LogLevel.Debug, "{}", aMessage);
    }
}
