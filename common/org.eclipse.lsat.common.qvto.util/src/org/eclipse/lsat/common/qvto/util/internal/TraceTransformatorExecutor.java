/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util.internal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.m2m.internal.qvt.oml.InternalTransformationExecutor;
import org.eclipse.m2m.internal.qvt.oml.trace.Trace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("restriction")
public class TraceTransformatorExecutor extends InternalTransformationExecutor {
    private static final Logger LOGGER = LoggerFactory.getLogger(TraceTransformatorExecutor.class);

    private URI itsTraceURI = null;

    public TraceTransformatorExecutor(URI uri) {
        super(uri);
    }

    public TraceTransformatorExecutor(URI uri, EPackage.Registry registry) {
        super(uri, registry);
    }

    public void setTraceURI(URI uri) {
        this.itsTraceURI = uri;
    }

    @Override
    protected void handleExecutionTraces(Trace traces) {
        if (itsTraceURI == null) {
            return;
        }

        ResourceSet resourceSet = this.getResourceSet();
        Resource resource = resourceSet.createResource(itsTraceURI);
        resource.getContents().add(traces);

        Map<String, String> options = new HashMap<String, String>();
        options.put(XMLResource.OPTION_PROCESS_DANGLING_HREF, XMLResource.OPTION_PROCESS_DANGLING_HREF_DISCARD);

        LOGGER.debug("Saving trace file: {}", itsTraceURI);
        try {
            resource.save(options);
        } catch (IOException e) {
            LOGGER.error("Failed to save trace file: " + e.getMessage(), e);
        }
    }
}
