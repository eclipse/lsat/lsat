/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.qvto.util;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.lsat.common.qvto.util.internal.StatusLogger;
import org.eclipse.lsat.common.qvto.util.internal.StatusLogger.LogStatus;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;

/**
 * A model transformer that logs all output of the transformation to an {@link IStatus}. The status can be retrieved
 * when the transformation finished.
 *
 * @param <Input>
 * @param <Output>
 */
public abstract class AbstractModelTransformerWithStatus<Input, Output>
        extends AbstractModelTransformer<Input, Output>
{
    private MultiStatus status = null;

    public IStatus getStatus() {
        if (null == status) {
            throw new IllegalStateException("Transformation should be started before querying its status.");
        }
        return status;
    }

    protected String getPluginID() {
        URI uri = getTransformationURI();
        if (uri.isPlatform() && uri.segmentCount() > 1) {
            return uri.segment(0);
        }
        return "org.eclipse.lsat.common.qvto.util";
    }

    /**
     * @throws QvtoErrorStatusException when execution of transformation fails.
     * @throws QvtoTransformationException Only when transformation cannot be executed (i.e. compilation errors)
     * @see org.eclipse.lsat.common.qvto.util.AbstractModelTransformer#execute(org.eclipse.m2m.qvt.oml.ModelExtent[])
     */
    @Override
    protected void execute(ModelExtent... aModelParameters) throws QvtoTransformationException {
        status = new MultiStatus(getPluginID(), 0, "Executed transformation: " + getTransformationURI(), null);

        registerImportedUnits();

        @SuppressWarnings("restriction")
        ExecutionDiagnostic diagnostic = getExecutor()
                .execute(createExecutionContext(new StatusLogger(status), itsMonitor), aModelParameters);

        if (diagnostic.getCode() == ExecutionDiagnostic.FATAL_ASSERTION) {
            // If a fatal assertion occurred, try to add the exception to the last fatal log
            LogStatus fatalLog = findFatalLog(status);
            if (null == fatalLog) {
                status.add(BasicDiagnostic.toIStatus(diagnostic));
            } else {
                fatalLog.setException(new ExecutionDiagnosticException(diagnostic));
            }
        } else {
            status.add(BasicDiagnostic.toIStatus(diagnostic));
        }
        if (status.getSeverity() >= IStatus.ERROR) {
            throw new QvtoErrorStatusException(status);
        }
    }

    private LogStatus findFatalLog(IStatus status) {
        IStatus[] statuses = status.getChildren();
        for (int i = statuses.length - 1; i >= 0; i--) {
            if (LogStatus.LEVEL_FATAL == statuses[i].getCode()) {
                return (LogStatus)statuses[i];
            }
        }
        return null;
    }
}
