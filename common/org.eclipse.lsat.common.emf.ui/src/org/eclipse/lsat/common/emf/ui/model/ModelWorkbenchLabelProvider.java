/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ui.model;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class ModelWorkbenchLabelProvider implements ILabelProvider {
    public static final ModelWorkbenchLabelProvider REFLECTIVE = new ModelWorkbenchLabelProvider();

    private final WorkbenchLabelProvider workbenchLabelProvider = new WorkbenchLabelProvider();

    private final AdapterFactoryLabelProvider adapterFactoryLabelProvider;

    protected static AdapterFactory createAdapterFactory(AdapterFactory... adapterFactories) {
        if (adapterFactories.length == 1 && adapterFactories[0] instanceof ComposedAdapterFactory) {
            // User taking control, don't add default reflective factory
            return adapterFactories[0];
        }
        ComposedAdapterFactory composedAdapterFactory = new ComposedAdapterFactory(
                ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
        for (AdapterFactory adapterFactory: adapterFactories) {
            composedAdapterFactory.addAdapterFactory(adapterFactory);
        }
        composedAdapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
        return composedAdapterFactory;
    }

    public ModelWorkbenchLabelProvider(AdapterFactory... adapterFactories) {
        adapterFactoryLabelProvider = new AdapterFactoryLabelProvider(createAdapterFactory(adapterFactories));
    }

    protected ILabelProvider getLabelProviderFor(Object element) {
        final boolean useAdapterFactory = adapterFactoryLabelProvider.getAdapterFactory().isFactoryForType(element);
        return useAdapterFactory ? adapterFactoryLabelProvider : workbenchLabelProvider;
    }

    public AdapterFactory getAdapterFactory() {
        return adapterFactoryLabelProvider.getAdapterFactory();
    }

    public Image getImage(Object element) {
        return getLabelProviderFor(element).getImage(element);
    }

    public String getText(Object element) {
        return getLabelProviderFor(element).getText(element);
    }

    public boolean isLabelProperty(Object element, String property) {
        return getLabelProviderFor(element).isLabelProperty(element, property);
    }

    public void addListener(ILabelProviderListener listener) {
        workbenchLabelProvider.addListener(listener);
        adapterFactoryLabelProvider.addListener(listener);
    }

    public void dispose() {
        workbenchLabelProvider.dispose();
        adapterFactoryLabelProvider.dispose();
    }

    public void removeListener(ILabelProviderListener listener) {
        workbenchLabelProvider.removeListener(listener);
        adapterFactoryLabelProvider.removeListener(listener);
    }
}
