/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ui;

import org.eclipse.emf.common.ui.ViewerPane;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;

public abstract class ActionsViewerPane extends ViewerPane {
    protected static final String GROUP_ACTIONS = "actions";

    protected final IAction refreshAction = new Action("Refresh", Activator.getDescriptor(Activator.IMAGE_REFRESH)) {
        @Override
        public void run() {
            refreshViewer();
        }
    };

    private final IWorkbenchPage page;

    private final IWorkbenchPart part;

    public ActionsViewerPane(IWorkbenchPage page, IWorkbenchPart part) {
        super(page, part);
        this.page = page;
        this.part = part;
    }

    protected IWorkbenchPage getPage() {
        return page;
    }

    protected IWorkbenchPart getPart() {
        return part;
    }

    protected EditingDomain getEditingDomain() {
        if (getPart() instanceof IEditingDomainProvider) {
            return ((IEditingDomainProvider)getPart()).getEditingDomain();
        }
        return null;
    }

    protected void refreshViewer() {
        viewer.refresh();
    }

    @Override
    public void createControl(Composite parent) {
        super.createControl(parent);

        getMenuManager().add(new Separator(GROUP_ACTIONS));
        getMenuManager().appendToGroup(GROUP_ACTIONS, refreshAction);

        getToolBarManager().add(new Separator(GROUP_ACTIONS));
        getToolBarManager().appendToGroup(GROUP_ACTIONS, refreshAction);

        updateActionBars();
    }
}
