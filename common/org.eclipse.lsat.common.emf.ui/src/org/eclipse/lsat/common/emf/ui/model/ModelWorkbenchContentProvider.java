/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ui.model;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;

public class ModelWorkbenchContentProvider extends BaseWorkbenchContentProvider {
    private final AdapterFactory adapterFactory;

    private final ResourceSet resourceSet;

    public ModelWorkbenchContentProvider(AdapterFactory... adapterFactories) {
        this(new ResourceSetImpl(), adapterFactories);
    }

    public ModelWorkbenchContentProvider(ResourceSet resourceSet, AdapterFactory... adapterFactories) {
        this.resourceSet = resourceSet;
        this.adapterFactory = ModelWorkbenchLabelProvider.createAdapterFactory(adapterFactories);
    }

    public AdapterFactory getAdapterFactory() {
        return adapterFactory;
    }

    @Override
    public Object[] getElements(Object element) {
        if (element instanceof Collection) {
            return ((Collection<?>)element).toArray();
        }
        return super.getElements(element);
    }

    @Override
    public Object getParent(Object element) {
        if (element instanceof EObject) {
            return ((EObject)element).eContainer();
        }
        return super.getParent(element);
    }

    @Override
    public Object[] getChildren(Object element) {
        if (element instanceof IFile) {
            try {
                URI modelURI = URI.createPlatformResourceURI(((IFile)element).getFullPath().toString(), true);
                Resource resource = resourceSet.getResource(modelURI, true);
                return (null != resource ? resource.getContents() : Collections.EMPTY_LIST).toArray();
            } catch (WrappedException e) {
                e.printStackTrace();
            }
        } else if (element instanceof EObject) {
            ITreeItemContentProvider adapter = (ITreeItemContentProvider)adapterFactory.adapt(element,
                    ITreeItemContentProvider.class);
            return (null != adapter ? adapter.getChildren(element) : Collections.EMPTY_LIST).toArray();
        }
        return super.getChildren(element);
    }
}
