/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.ui.wizards;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

/**
 * A class to select elements out of a tree structure.<br>
 * {@link WizardPage} equivalent of {@link ElementTreeSelectionDialog}.
 */
public class ElementTreeSelectionWizardPage extends WizardPage {
    private final List<ViewerFilter> fFilters = new ArrayList<ViewerFilter>(4);

    private final IBaseLabelProvider fLabelProvider;

    private final ITreeContentProvider fContentProvider;

    private TreeViewer fViewer;

    private ISelectionStatusValidator fValidator = null;

    private ViewerComparator fComparator = null;

    private boolean fAllowMultiple = true;

    private Object fInput = null;

    private Object[] fResult;

    private Object[] fInitialSelection;

    private int fWidth = 60;

    private int fHeight = 18;

    public ElementTreeSelectionWizardPage(String pageName, IBaseLabelProvider labelProvider,
            ITreeContentProvider contentProvider)
    {
        super(pageName);
        fLabelProvider = labelProvider;
        fContentProvider = contentProvider;
    }

    /**
     * Sets the size of the tree in unit of characters.
     *
     * @param width the width of the tree.
     * @param height the height of the tree.
     */
    public void setSize(int width, int height) {
        fWidth = width;
        fHeight = height;
    }

    @Override
    public void createControl(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout());
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        final int style = SWT.BORDER | (fAllowMultiple ? SWT.MULTI : SWT.SINGLE);
        fViewer = new TreeViewer(composite, style);

        GridData data = new GridData(GridData.FILL_BOTH);
        data.widthHint = convertWidthInCharsToPixels(fWidth);
        data.heightHint = convertHeightInCharsToPixels(fHeight);
        fViewer.getTree().setLayoutData(data);

        fViewer.setContentProvider(fContentProvider);
        fViewer.setLabelProvider(fLabelProvider);
        fViewer.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                setResult(((IStructuredSelection)event.getSelection()).toList());
                updateNextStatus();
            }
        });

        fViewer.setComparator(fComparator);
        for (ViewerFilter filter: fFilters) {
            fViewer.addFilter(filter);
        }
        fViewer.addDoubleClickListener(new IDoubleClickListener() {
            @Override
            public void doubleClick(DoubleClickEvent event) {
                updateNextStatus();
                ISelection selection = event.getSelection();
                if (selection instanceof IStructuredSelection) {
                    Object item = ((IStructuredSelection)selection).getFirstElement();
                    if (fViewer.getExpandedState(item)) {
                        fViewer.collapseToLevel(item, 1);
                    } else {
                        fViewer.expandToLevel(item, 1);
                    }
                }
            }
        });

        fViewer.setInput(fInput);
        if (null != fInitialSelection) {
            fViewer.setSelection(new StructuredSelection(fInitialSelection), true);
        }

        setControl(composite);
    }

    public void addFilter(ViewerFilter filter) {
        fFilters.add(filter);
    }

    public void setAllowMultiple(boolean allowMultiple) {
        this.fAllowMultiple = allowMultiple;
    }

    public void setComparator(ViewerComparator comparator) {
        this.fComparator = comparator;
    }

    public void setInitialSelections(Collection<?> initialSelection) {
        this.fInitialSelection = initialSelection.toArray();
    }

    public void setInitialSelections(Object... initialSelection) {
        this.fInitialSelection = initialSelection;
    }

    public void setInput(Object input) {
        this.fInput = input;
    }

    public void setValidator(ISelectionStatusValidator validator) {
        this.fValidator = validator;
    }

    public Object[] getResult() {
        return fResult;
    }

    protected void setResult(Collection<?> result) {
        fResult = result.toArray();
    }

    protected void setSelectionResult(Object... result) {
        fResult = result;
    }

    /**
     * Validate the receiver and update the ok status.
     */
    protected void updateNextStatus() {
        IStatus status = null == fValidator ? Status.OK_STATUS : fValidator.validate(getResult());
        updateStatus(status);
    }

    /**
     * Update the dialog's status line to reflect the given status.
     *
     * @param status
     */
    protected void updateStatus(IStatus status) {
        switch (status.getSeverity()) {
            case IStatus.INFO:
                setMessage(status.getMessage());
            case IStatus.OK:
                setErrorMessage(null);
                setPageComplete(true);
                break;
            default:
                setErrorMessage(status.getMessage());
                setPageComplete(false);
                break;
        }
    }
}
