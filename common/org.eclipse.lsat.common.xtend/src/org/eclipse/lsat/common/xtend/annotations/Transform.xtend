/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.Target
import java.util.Queue
import org.eclipse.xtend.lib.macro.AbstractMethodProcessor
import org.eclipse.xtend.lib.macro.Active
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableMethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.Visibility

/**
 * Transform adds late support to the enclosing class, see {@code late(Runnable)} method.
 */
@Retention(SOURCE)
@Target(ElementType.METHOD)
@Active(TransformCompilationParticipant)
annotation Transform {
}

class TransformCompilationParticipant extends AbstractMethodProcessor {
    override doTransform(MutableMethodDeclaration annotatedMethod, extension TransformationContext context) {
        annotatedMethod.markAsRead // We processed this method
        if (annotatedMethod.static) {
            annotatedMethod.addError('Transformation methods cannot be static')
            return
        } else if (!(annotatedMethod.declaringType instanceof MutableClassDeclaration)) {
            annotatedMethod.addError('Transformation methods should be declared on class')
            return
        }
        
        val transformName = annotatedMethod.simpleName
        annotatedMethod.simpleName = '_transform_' + transformName
        annotatedMethod.visibility = Visibility::PRIVATE
        
        annotatedMethod.declaringType.addMethod(transformName) [
            visibility = Visibility::PUBLIC
            final = annotatedMethod.final
            synchronized = true
            returnType = annotatedMethod.returnType
            exceptions = annotatedMethod.exceptions
            for (parameter : annotatedMethod.parameters) {
                addParameter(parameter.simpleName, parameter.type)
            }
            body = '''
                «IF !returnType.void»«returnType.simpleName» result = «ENDIF»«annotatedMethod.simpleName»(«FOR parameter : annotatedMethod.parameters SEPARATOR ', '»«parameter.simpleName»«ENDFOR»);
                _performLateFunctions();
                «IF !returnType.void»return result;«ENDIF»
            '''            
        ]
        
        doTransform(annotatedMethod.declaringType as MutableClassDeclaration, context)
    }
    
    def void doTransform(MutableClassDeclaration annotatedClass, extension TransformationContext context) {
        if (newTypeReference(annotatedClass).allResolvedMethods.map[declaration.simpleName].contains('late')) {
            // Already processed
            return
        }
        
        annotatedClass.addField('_lateFunctions') [
            visibility = Visibility::PRIVATE
            final = true
            type = newTypeReference(Queue, newTypeReference(Runnable))
            initializer = '''new java.util.LinkedList<>()'''
        ]

        annotatedClass.addMethod('_performLateFunctions') [
            visibility = Visibility::PROTECTED
            body = '''
                while (!_lateFunctions.isEmpty()) {
                    _lateFunctions.remove().run();
                }
            '''
        ]

        annotatedClass.addMethod('late') [
            docComment = 'Late functions are executed at the end of the transformation, just before returning the result.'
            visibility = Visibility::PROTECTED
            final = true
            addParameter('function', newTypeReference(Runnable))
            body = '''_lateFunctions.add(function);'''
        ]
    }
}