/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.eclipse.lsat.common.xtend.annotations.ResolvableCompilationParticipant;
import org.eclipse.xtend.lib.macro.Active;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
@Active(ResolvableCompilationParticipant.class)
@SuppressWarnings("all")
public @interface Resolvable {
}
