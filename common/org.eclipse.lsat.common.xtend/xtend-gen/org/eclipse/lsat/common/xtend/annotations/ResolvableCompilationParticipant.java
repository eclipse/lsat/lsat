/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtend.annotations;

import com.google.common.base.Objects;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import org.eclipse.lsat.common.xtend.annotations.Resolvable;
import org.eclipse.lsat.common.xtend.annotations.TypeReferenceUtil;
import org.eclipse.xtend.lib.macro.AbstractMethodProcessor;
import org.eclipse.xtend.lib.macro.TransformationContext;
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference;
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableMethodDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableParameterDeclaration;
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration;
import org.eclipse.xtend.lib.macro.declaration.Type;
import org.eclipse.xtend.lib.macro.declaration.TypeReference;
import org.eclipse.xtend.lib.macro.declaration.Visibility;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class ResolvableCompilationParticipant extends AbstractMethodProcessor {
  @Override
  public void doTransform(final MutableMethodDeclaration annotatedMethod, @Extension final TransformationContext context) {
    MutableTypeDeclaration _declaringType = annotatedMethod.getDeclaringType();
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("_createCache_");
    String _simpleName = annotatedMethod.getSimpleName();
    _builder.append(_simpleName);
    final MutableFieldDeclaration createCache = _declaringType.findDeclaredField(_builder.toString());
    if ((createCache == null)) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("@Resolvable can only be used on create methods.");
      context.addError(annotatedMethod, _builder_1.toString());
      return;
    } else {
      boolean _isInferred = annotatedMethod.getReturnType().isInferred();
      if (_isInferred) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("Please specify an explicit return type for @Resolvable methods");
        context.addError(annotatedMethod, _builder_2.toString());
        return;
      } else {
        boolean _isEmpty = IterableExtensions.isEmpty(annotatedMethod.getParameters());
        if (_isEmpty) {
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("At least 1 parameter is required for @Resolvable methods.");
          context.addError(annotatedMethod, _builder_3.toString());
          return;
        } else {
          MutableTypeDeclaration _declaringType_1 = annotatedMethod.getDeclaringType();
          boolean _not = (!(_declaringType_1 instanceof MutableClassDeclaration));
          if (_not) {
            StringConcatenation _builder_4 = new StringConcatenation();
            _builder_4.append("@Resolvable methods should be declared on class.");
            context.addError(annotatedMethod, _builder_4.toString());
            return;
          }
        }
      }
    }
    MutableTypeDeclaration _declaringType_2 = annotatedMethod.getDeclaringType();
    final MutableClassDeclaration declaringClass = ((MutableClassDeclaration) _declaringType_2);
    int _length = ((Object[])Conversions.unwrapArray(annotatedMethod.getParameters(), Object.class)).length;
    boolean _equals = (_length == 1);
    if (_equals) {
      final MutableParameterDeclaration sourceParameter = IterableExtensions.head(annotatedMethod.getParameters());
      StringConcatenation _builder_5 = new StringConcatenation();
      _builder_5.append("_resolveOne_");
      String _simpleName_1 = annotatedMethod.getSimpleName();
      _builder_5.append(_simpleName_1);
      final Procedure1<MutableMethodDeclaration> _function = (MutableMethodDeclaration it) -> {
        it.setVisibility(Visibility.PRIVATE);
        it.setReturnType(context.newTypeReference(Optional.class, annotatedMethod.getReturnType()));
        it.addParameter(sourceParameter.getSimpleName(), sourceParameter.getType());
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("final ArrayList<?> _cacheKey = CollectionLiterals.newArrayList(");
            String _simpleName = sourceParameter.getSimpleName();
            _builder.append(_simpleName);
            _builder.append(");");
            _builder.newLineIfNotEmpty();
            _builder.append("synchronized (");
            String _simpleName_1 = createCache.getSimpleName();
            _builder.append(_simpleName_1);
            _builder.append(") {");
            _builder.newLineIfNotEmpty();
            _builder.append("    ");
            _builder.append("if (");
            String _simpleName_2 = createCache.getSimpleName();
            _builder.append(_simpleName_2, "    ");
            _builder.append(".containsKey(_cacheKey)) {");
            _builder.newLineIfNotEmpty();
            _builder.append("        ");
            _builder.append("return Optional.ofNullable(");
            String _simpleName_3 = createCache.getSimpleName();
            _builder.append(_simpleName_3, "        ");
            _builder.append(".get(_cacheKey));");
            _builder.newLineIfNotEmpty();
            _builder.append("    ");
            _builder.append("} else {");
            _builder.newLine();
            _builder.append("        ");
            _builder.append("return Optional.empty();");
            _builder.newLine();
            _builder.append("    ");
            _builder.append("}");
            _builder.newLine();
            _builder.append("}");
            _builder.newLine();
          }
        };
        it.setBody(_client);
      };
      declaringClass.addMethod(_builder_5.toString(), _function);
      StringConcatenation _builder_6 = new StringConcatenation();
      _builder_6.append("_resolveAll_");
      String _simpleName_2 = annotatedMethod.getSimpleName();
      _builder_6.append(_simpleName_2);
      final Procedure1<MutableMethodDeclaration> _function_1 = (MutableMethodDeclaration it) -> {
        it.setVisibility(Visibility.PRIVATE);
        it.setReturnType(context.newTypeReference(Stream.class, annotatedMethod.getReturnType()));
        it.addParameter(sourceParameter.getSimpleName(), sourceParameter.getType());
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("final Optional<");
            TypeReference _returnType = annotatedMethod.getReturnType();
            _builder.append(_returnType);
            _builder.append("> resolved = _resolveOne_");
            String _simpleName = annotatedMethod.getSimpleName();
            _builder.append(_simpleName);
            _builder.append("(");
            String _simpleName_1 = sourceParameter.getSimpleName();
            _builder.append(_simpleName_1);
            _builder.append(");");
            _builder.newLineIfNotEmpty();
            _builder.append("return Stream.of(resolved).filter(Optional::isPresent).map(Optional::get);");
            _builder.newLine();
          }
        };
        it.setBody(_client);
      };
      declaringClass.addMethod(_builder_6.toString(), _function_1);
    } else {
      final Function1<MutableParameterDeclaration, TypeReference> _function_2 = (MutableParameterDeclaration it) -> {
        return it.getType();
      };
      Set<TypeReference> _set = IterableExtensions.<TypeReference>toSet(IterableExtensions.map(annotatedMethod.getParameters(), _function_2));
      for (final TypeReference sourceParameterType : _set) {
        {
          StringConcatenation _builder_7 = new StringConcatenation();
          _builder_7.append("_resolveOne_");
          String _simpleName_3 = annotatedMethod.getSimpleName();
          _builder_7.append(_simpleName_3);
          final Procedure1<MutableMethodDeclaration> _function_3 = (MutableMethodDeclaration it) -> {
            it.setVisibility(Visibility.PRIVATE);
            it.setReturnType(context.newTypeReference(Optional.class, annotatedMethod.getReturnType()));
            it.addParameter("source", sourceParameterType);
            StringConcatenationClient _client = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append("return _resolveAll_events2Dependency(source).findAny();");
              }
            };
            it.setBody(_client);
          };
          declaringClass.addMethod(_builder_7.toString(), _function_3);
          StringConcatenation _builder_8 = new StringConcatenation();
          _builder_8.append("_resolveAll_");
          String _simpleName_4 = annotatedMethod.getSimpleName();
          _builder_8.append(_simpleName_4);
          final Procedure1<MutableMethodDeclaration> _function_4 = (MutableMethodDeclaration it) -> {
            it.setVisibility(Visibility.PRIVATE);
            it.setReturnType(context.newTypeReference(Stream.class, annotatedMethod.getReturnType()));
            it.addParameter("source", sourceParameterType);
            StringConcatenationClient _client = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append("// TODO what about synchronization here?");
                _builder.newLine();
                _builder.append("return ");
                String _simpleName = createCache.getSimpleName();
                _builder.append(_simpleName);
                _builder.append(".entrySet().stream().filter(e -> e.getKey().contains(source)).map(java.util.Map.Entry::getValue);");
                _builder.newLineIfNotEmpty();
              }
            };
            it.setBody(_client);
          };
          declaringClass.addMethod(_builder_8.toString(), _function_4);
        }
      }
    }
    StringConcatenation _builder_7 = new StringConcatenation();
    _builder_7.append("_invResolveOne_");
    String _simpleName_3 = annotatedMethod.getSimpleName();
    _builder_7.append(_simpleName_3);
    final Procedure1<MutableMethodDeclaration> _function_3 = (MutableMethodDeclaration it) -> {
      final TypeReference genericType = context.newTypeReference(it.addTypeParameter("T"));
      final TypeReference resolveType = context.newTypeReference(Class.class, genericType);
      it.setVisibility(Visibility.PRIVATE);
      it.setReturnType(context.newTypeReference(Optional.class, genericType));
      it.addParameter("target", annotatedMethod.getReturnType());
      it.addParameter("typeToResolve", resolveType);
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("return _invResolveAll_");
          String _simpleName = annotatedMethod.getSimpleName();
          _builder.append(_simpleName);
          _builder.append("(target, typeToResolve).findFirst();");
        }
      };
      it.setBody(_client);
    };
    declaringClass.addMethod(_builder_7.toString(), _function_3);
    StringConcatenation _builder_8 = new StringConcatenation();
    _builder_8.append("_invResolveAll_");
    String _simpleName_4 = annotatedMethod.getSimpleName();
    _builder_8.append(_simpleName_4);
    final Procedure1<MutableMethodDeclaration> _function_4 = (MutableMethodDeclaration it) -> {
      final TypeReference genericType = context.newTypeReference(it.addTypeParameter("T"));
      final TypeReference resolveType = context.newTypeReference(Class.class, genericType);
      it.setVisibility(Visibility.PRIVATE);
      it.setReturnType(context.newTypeReference(Stream.class, genericType));
      it.addParameter("target", annotatedMethod.getReturnType());
      it.addParameter("typeToResolve", resolveType);
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("// TODO what about synchronization here?");
          _builder.newLine();
          _builder.append("return ");
          String _simpleName = createCache.getSimpleName();
          _builder.append(_simpleName);
          _builder.append(".entrySet().stream().filter(e -> e.getValue() == target)");
          _builder.newLineIfNotEmpty();
          _builder.append("        ");
          _builder.append(".flatMap(e -> e.getKey().stream()).filter(typeToResolve::isInstance).map(typeToResolve::cast);");
          _builder.newLine();
        }
      };
      it.setBody(_client);
    };
    declaringClass.addMethod(_builder_8.toString(), _function_4);
    this.doTransform(declaringClass, context);
  }
  
  public void doTransform(final MutableClassDeclaration declaringClass, @Extension final TransformationContext context) {
    final Function1<MutableMethodDeclaration, Boolean> _function = (MutableMethodDeclaration it) -> {
      String _simpleName = it.getSimpleName();
      return Boolean.valueOf(Objects.equal(_simpleName, "resolveOne"));
    };
    boolean _exists = IterableExtensions.exists(declaringClass.getDeclaredMethods(), _function);
    if (_exists) {
      return;
    }
    final Type annotationType = context.findTypeGlobally(Resolvable.class);
    final Function1<MutableMethodDeclaration, Boolean> _function_1 = (MutableMethodDeclaration it) -> {
      AnnotationReference _findAnnotation = it.findAnnotation(annotationType);
      return Boolean.valueOf((_findAnnotation != null));
    };
    final Iterable<? extends MutableMethodDeclaration> annotatedMethods = IterableExtensions.filter(declaringClass.getDeclaredMethods(), _function_1);
    final LinkedHashMap<TypeReference, LinkedHashSet<MutableMethodDeclaration>> sourceTypes = CollectionLiterals.<TypeReference, LinkedHashSet<MutableMethodDeclaration>>newLinkedHashMap();
    final LinkedHashMap<TypeReference, LinkedHashSet<MutableMethodDeclaration>> targetTypes = CollectionLiterals.<TypeReference, LinkedHashSet<MutableMethodDeclaration>>newLinkedHashMap();
    final Consumer<MutableMethodDeclaration> _function_2 = (MutableMethodDeclaration method) -> {
      final Function1<MutableParameterDeclaration, TypeReference> _function_3 = (MutableParameterDeclaration it) -> {
        return it.getType();
      };
      final Consumer<TypeReference> _function_4 = (TypeReference type) -> {
        final Function<TypeReference, LinkedHashSet<MutableMethodDeclaration>> _function_5 = (TypeReference it) -> {
          return CollectionLiterals.<MutableMethodDeclaration>newLinkedHashSet();
        };
        LinkedHashSet<MutableMethodDeclaration> _computeIfAbsent = sourceTypes.computeIfAbsent(type, _function_5);
        _computeIfAbsent.add(method);
      };
      IterableExtensions.map(method.getParameters(), _function_3).forEach(_function_4);
      final Function<TypeReference, LinkedHashSet<MutableMethodDeclaration>> _function_5 = (TypeReference it) -> {
        return CollectionLiterals.<MutableMethodDeclaration>newLinkedHashSet();
      };
      LinkedHashSet<MutableMethodDeclaration> _computeIfAbsent = targetTypes.computeIfAbsent(method.getReturnType(), _function_5);
      _computeIfAbsent.add(method);
    };
    annotatedMethods.forEach(_function_2);
    final BiConsumer<TypeReference, LinkedHashSet<MutableMethodDeclaration>> _function_3 = (TypeReference sourceType, LinkedHashSet<MutableMethodDeclaration> annotatedMethodsForSourceType) -> {
      final Procedure1<MutableMethodDeclaration> _function_4 = (MutableMethodDeclaration it) -> {
        final TypeReference genericType = context.newTypeReference(it.addTypeParameter("T"));
        final TypeReference resolveType = context.newTypeReference(Class.class, genericType);
        final MethodDeclaration superResolve = TypeReferenceUtil.findResolvedMethod(context.newTypeReference(declaringClass), "resolveOne", sourceType, resolveType);
        it.setVisibility(Visibility.PROTECTED);
        it.setReturnType(genericType);
        it.addParameter("source", sourceType);
        it.addParameter("typeToResolve", resolveType);
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            {
              for(final MutableMethodDeclaration annotatedMethod : annotatedMethodsForSourceType) {
                _builder.append("if (typeToResolve.isAssignableFrom(");
                String _simpleName = annotatedMethod.getReturnType().getType().getSimpleName();
                _builder.append(_simpleName);
                _builder.append(".class) || ");
                String _simpleName_1 = annotatedMethod.getReturnType().getType().getSimpleName();
                _builder.append(_simpleName_1);
                _builder.append(".class.isAssignableFrom(typeToResolve)) {");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("final Optional<");
                String _simpleName_2 = annotatedMethod.getReturnType().getSimpleName();
                _builder.append(_simpleName_2, "    ");
                _builder.append("> resolved = _resolveOne_");
                String _simpleName_3 = annotatedMethod.getSimpleName();
                _builder.append(_simpleName_3, "    ");
                _builder.append("(source);");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("if (resolved.isPresent() && typeToResolve.isInstance(resolved.get())) {");
                _builder.newLine();
                _builder.append("        ");
                _builder.append("return typeToResolve.cast(resolved.get());");
                _builder.newLine();
                _builder.append("    ");
                _builder.append("}");
                _builder.newLine();
                _builder.append("}");
                _builder.newLine();
              }
            }
            {
              if ((superResolve == null)) {
                _builder.append("return null;");
                _builder.newLine();
              } else {
                _builder.append("return super.resolveOne(source);");
                _builder.newLine();
              }
            }
          }
        };
        it.setBody(_client);
      };
      declaringClass.addMethod("resolveOne", _function_4);
      final Procedure1<MutableMethodDeclaration> _function_5 = (MutableMethodDeclaration it) -> {
        final TypeReference genericType = context.newTypeReference(it.addTypeParameter("T"));
        final TypeReference resolveType = context.newTypeReference(Class.class, genericType);
        final MethodDeclaration superResolve = TypeReferenceUtil.findResolvedMethod(context.newTypeReference(declaringClass), "resolveAll", sourceType, resolveType);
        it.setVisibility(Visibility.PROTECTED);
        it.setReturnType(context.newTypeReference(List.class, genericType));
        it.addParameter("source", sourceType);
        it.addParameter("typeToResolve", resolveType);
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("List<T> resolved = CollectionLiterals.newLinkedList();");
            _builder.newLine();
            {
              for(final MutableMethodDeclaration annotatedMethod : annotatedMethodsForSourceType) {
                _builder.append("if (typeToResolve.isAssignableFrom(");
                String _simpleName = annotatedMethod.getReturnType().getType().getSimpleName();
                _builder.append(_simpleName);
                _builder.append(".class) || ");
                String _simpleName_1 = annotatedMethod.getReturnType().getType().getSimpleName();
                _builder.append(_simpleName_1);
                _builder.append(".class.isAssignableFrom(typeToResolve)) {");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("final Stream<");
                String _simpleName_2 = annotatedMethod.getReturnType().getSimpleName();
                _builder.append(_simpleName_2, "    ");
                _builder.append("> result = _resolveAll_");
                String _simpleName_3 = annotatedMethod.getSimpleName();
                _builder.append(_simpleName_3, "    ");
                _builder.append("(source);");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("result.filter(typeToResolve::isInstance).map(typeToResolve::cast).forEachOrdered(resolved::add);");
                _builder.newLine();
                _builder.append("}");
                _builder.newLine();
              }
            }
            {
              if ((superResolve != null)) {
                _builder.append("resolved.addAll(super.resolveAll(source));");
                _builder.newLine();
              }
            }
            _builder.append("return resolved;");
            _builder.newLine();
          }
        };
        it.setBody(_client);
      };
      declaringClass.addMethod("resolveAll", _function_5);
    };
    sourceTypes.forEach(_function_3);
    final BiConsumer<TypeReference, LinkedHashSet<MutableMethodDeclaration>> _function_4 = (TypeReference targetType, LinkedHashSet<MutableMethodDeclaration> annotatedMethodsForTargetType) -> {
      final Procedure1<MutableMethodDeclaration> _function_5 = (MutableMethodDeclaration it) -> {
        final TypeReference genericType = context.newTypeReference(it.addTypeParameter("T"));
        final TypeReference resolveType = context.newTypeReference(Class.class, genericType);
        final MethodDeclaration superResolve = TypeReferenceUtil.findResolvedMethod(context.newTypeReference(declaringClass), "invResolveOne", targetType, resolveType);
        it.setReturnType(genericType);
        it.setVisibility(Visibility.PROTECTED);
        it.addParameter("target", targetType);
        it.addParameter("typeToResolve", resolveType);
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            {
              for(final MutableMethodDeclaration annotatedMethod : annotatedMethodsForTargetType) {
                _builder.append("if (");
                {
                  Iterable<? extends MutableParameterDeclaration> _parameters = annotatedMethod.getParameters();
                  boolean _hasElements = false;
                  for(final MutableParameterDeclaration param : _parameters) {
                    if (!_hasElements) {
                      _hasElements = true;
                    } else {
                      _builder.appendImmediate("||", "");
                    }
                    String _simpleName = param.getType().getType().getSimpleName();
                    _builder.append(_simpleName);
                    _builder.append(".class.isAssignableFrom(typeToResolve)");
                  }
                }
                _builder.append(") {");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("final Optional<");
                String _simpleName_1 = genericType.getSimpleName();
                _builder.append(_simpleName_1, "    ");
                _builder.append("> resolved = _invResolveOne_");
                String _simpleName_2 = annotatedMethod.getSimpleName();
                _builder.append(_simpleName_2, "    ");
                _builder.append("(target, typeToResolve);");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("if (resolved.isPresent()) {");
                _builder.newLine();
                _builder.append("        ");
                _builder.append("return resolved.get();");
                _builder.newLine();
                _builder.append("    ");
                _builder.append("}");
                _builder.newLine();
                _builder.append("}");
                _builder.newLine();
              }
            }
            {
              if ((superResolve == null)) {
                _builder.append("return null;");
                _builder.newLine();
              } else {
                _builder.append("return super.resolveOne(source);");
                _builder.newLine();
              }
            }
          }
        };
        it.setBody(_client);
      };
      declaringClass.addMethod("invResolveOne", _function_5);
      final Procedure1<MutableMethodDeclaration> _function_6 = (MutableMethodDeclaration it) -> {
        final TypeReference genericType = context.newTypeReference(it.addTypeParameter("T"));
        final TypeReference resolveType = context.newTypeReference(Class.class, genericType);
        final MethodDeclaration superResolve = TypeReferenceUtil.findResolvedMethod(context.newTypeReference(declaringClass), "invResolveAll", targetType, resolveType);
        it.setReturnType(context.newTypeReference(List.class, genericType));
        it.setVisibility(Visibility.PROTECTED);
        it.addParameter("target", targetType);
        it.addParameter("typeToResolve", resolveType);
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("List<T> resolved = CollectionLiterals.newLinkedList();");
            _builder.newLine();
            {
              for(final MutableMethodDeclaration annotatedMethod : annotatedMethodsForTargetType) {
                _builder.append("if (");
                {
                  Iterable<? extends MutableParameterDeclaration> _parameters = annotatedMethod.getParameters();
                  boolean _hasElements = false;
                  for(final MutableParameterDeclaration param : _parameters) {
                    if (!_hasElements) {
                      _hasElements = true;
                    } else {
                      _builder.appendImmediate("||", "");
                    }
                    String _simpleName = param.getType().getType().getSimpleName();
                    _builder.append(_simpleName);
                    _builder.append(".class.isAssignableFrom(typeToResolve)");
                  }
                }
                _builder.append(") {");
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                _builder.append("_invResolveAll_");
                String _simpleName_1 = annotatedMethod.getSimpleName();
                _builder.append(_simpleName_1, "    ");
                _builder.append("(target, typeToResolve).forEachOrdered(resolved::add);");
                _builder.newLineIfNotEmpty();
                _builder.append("}");
                _builder.newLine();
              }
            }
            {
              if ((superResolve != null)) {
                _builder.append("resolved.addAll(super.resolveAll(source));");
                _builder.newLine();
              }
            }
            _builder.append("return resolved;");
            _builder.newLine();
          }
        };
        it.setBody(_client);
      };
      declaringClass.addMethod("invResolveAll", _function_6);
    };
    targetTypes.forEach(_function_4);
  }
}
