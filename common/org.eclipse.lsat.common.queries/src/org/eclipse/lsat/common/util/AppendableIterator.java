/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * An implementation of {@link Iterator} to which elements can be appended while iterating.
 *
 * @param <E> type of element
 */
public class AppendableIterator<E> implements Iterator<E> {
    private final Iterator<? extends E> initial;

    private final LinkedList<E> queue = new LinkedList<E>();

    public AppendableIterator() {
        this(Collections.emptyIterator());
    }

    public AppendableIterator(Iterator<? extends E> initial) {
        this.initial = initial;
    }

    /**
     * Appends an element to the end of this iterator.
     *
     * @param e element to append
     * @return true if this iterator changed as a result of the call
     */
    public boolean append(E e) {
        return queue.add(e);
    }

    /**
     * Undos the last call to {@link #append(Object)} and
     *
     * @return the last appended value
     * @throws NoSuchElementException if no value was appended or if it was already consumed by {@link #next()}
     */
    public E undoAppend() {
        return queue.removeLast();
    }

    @Override
    public boolean hasNext() {
        return initial.hasNext() || !queue.isEmpty();
    }

    @Override
    public E next() {
        return initial.hasNext() ? initial.next() : queue.removeFirst();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
