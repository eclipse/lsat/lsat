/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

// Deliberately chosen not to extend LoggableIterable as this iterable can be iterated only once
public class IterableIterator<T> implements Iterable<T> {
    private Iterator<T> iterator;

    public IterableIterator(Iterator<T> iterator) {
        this.iterator = iterator;
    }

    @Override
    public synchronized Iterator<T> iterator() {
        if (null == iterator) {
            throw new IllegalStateException("IterableIterator can be iterated only once.");
        }
        try {
            return iterator;
        } finally {
            iterator = null;
        }
    }
}
