/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public final class CollectionUtil {
    private CollectionUtil() {
        /* Empty */
    }

    /**
     * Adds all elements of the specified iterable to the specified collection.
     *
     * @param c the collection into which <tt>elements</tt> are to be inserted
     * @param iterable the elements to insert into <tt>c</tt>
     * @return <tt>true</tt> if the collection changed as a result of the call
     * @throws UnsupportedOperationException if <tt>c</tt> does not support the <tt>add</tt> operation
     * @throws NullPointerException if <tt>elements</tt> contains one or more null values and <tt>c</tt> does not permit
     *     null elements, or if <tt>c</tt> or <tt>elements</tt> are <tt>null</tt>
     * @throws IllegalArgumentException if some property of a value in <tt>elements</tt> prevents it from being added to
     *     <tt>c</tt>
     * @see Collection#addAll(Collection)
     */
    public static final <T> boolean addAll(Collection<? super T> c, Iterable<T> iterable) {
        return addAll(c, iterable.iterator());
    }

    /**
     * Adds all elements of the specified iterator to the specified collection.
     *
     * @param c the collection into which <tt>elements</tt> are to be inserted
     * @param iterator the elements to insert into <tt>c</tt>
     * @return <tt>true</tt> if the collection changed as a result of the call
     * @throws UnsupportedOperationException if <tt>c</tt> does not support the <tt>add</tt> operation
     * @throws NullPointerException if <tt>elements</tt> contains one or more null values and <tt>c</tt> does not permit
     *     null elements, or if <tt>c</tt> or <tt>elements</tt> are <tt>null</tt>
     * @throws IllegalArgumentException if some property of a value in <tt>elements</tt> prevents it from being added to
     *     <tt>c</tt>
     * @see Collection#addAll(Collection)
     */
    public static final <T> boolean addAll(Collection<? super T> c, Iterator<T> iterator) {
        boolean changed = false;
        while (iterator.hasNext()) {
            changed |= c.add(iterator.next());
        }
        return changed;
    }

    /**
     * Adds all of the specified elements to the specified collection. Elements to be added may be specified
     * individually or as an array. The behavior of this convenience method is identical to that of
     * <tt>c.addAll(Arrays.asList(elements))</tt>, but this method is likely to run significantly faster under most
     * implementations.
     *
     * <p>
     * When elements are specified individually, this method provides a convenient way to add a few elements to an
     * existing collection:
     * </p>
     *
     * <pre>
     * Collections.addAll(flavors, "Peaches 'n Plutonium", "Rocky Racoon");
     * </pre>
     *
     * @param c the collection into which <tt>elements</tt> are to be inserted
     * @param elements the elements to insert into <tt>c</tt>
     * @return <tt>true</tt> if the collection changed as a result of the call
     * @throws UnsupportedOperationException if <tt>c</tt> does not support the <tt>add</tt> operation
     * @throws NullPointerException if <tt>elements</tt> contains one or more null values and <tt>c</tt> does not permit
     *     null elements, or if <tt>c</tt> or <tt>elements</tt> are <tt>null</tt>
     * @throws IllegalArgumentException if some property of a value in <tt>elements</tt> prevents it from being added to
     *     <tt>c</tt>
     * @see Collections#addAll(Collection, Object...)
     */
    @SafeVarargs
    public static final <T> boolean addAll(Collection<? super T> c, T... elements) {
        return Collections.addAll(c, elements);
    }

    /**
     * Returns <tt>true</tt> if the collection contains one of the specified elements. More formally, returns
     * <tt>true</tt> if and only if this collection contains at least one element <tt>e</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param c collection
     * @param objects elements whom presence in this collection is to be tested
     * @return <tt>true</tt> if this collection contains one of the specified elements
     * @throws ClassCastException if the type of the specified element is incompatible with this collection
     *     (<a href="#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this collection does not permit null elements
     *     (<a href="#optional-restrictions">optional</a>)
     * @see Collection#contains(Object)
     */
    @SafeVarargs
    public static final boolean containsOneOf(Collection<?> c, Object... objects) {
        return containsOneOf(c, Arrays.asList(objects));
    }

    /**
     * Returns <tt>true</tt> if the collection contains one of the specified elements. More formally, returns
     * <tt>true</tt> if and only if this collection contains at least one element <tt>e</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param c collection
     * @param objects elements whom presence in this collection is to be tested
     * @return <tt>true</tt> if this collection contains one of the specified elements
     * @throws ClassCastException if the type of the specified element is incompatible with this collection
     *     (<a href="#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this collection does not permit null elements
     *     (<a href="#optional-restrictions">optional</a>)
     * @see Collection#contains(Object)
     */
    public static final boolean containsOneOf(Collection<?> c, Iterable<?> objects) {
        for (Object o: objects) {
            if (c.contains(o)) {
                return true;
            }
        }
        return false;
    }
}
