/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * An {@link Iterator} that segments the original into groups, based on a predicate.
 */
public class SegmentIterator<E> extends ProcessingIterator<List<E>> {
    private final Iterator<? extends E> source;

    private final BiPredicate<? super E, ? super E> predicate;

    private LinkedList<E> group;

    public SegmentIterator(Iterator<? extends E> source, BiPredicate<? super E, ? super E> predicate) {
        this.source = source;
        this.predicate = predicate;
    }

    @Override
    protected boolean toNext() {
        while (source.hasNext()) {
            E element = source.next();

            if (group == null) {
                group = new LinkedList<E>();
                group.add(element);
            } else if (predicate.test(group.getLast(), element)) {
                group.add(element);
            } else {
                LinkedList<E> next = group;
                group = new LinkedList<E>();
                group.add(element);
                return setNext(next);
            }
        }
        if (group != null) {
            LinkedList<E> next = group;
            group = null;
            return setNext(next);
        }
        return done();
    }
}
