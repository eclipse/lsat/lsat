/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

public abstract class LoggableIterable<T> implements Iterable<T> {
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append('[');
        for (Iterator<T> i = iterator(); i.hasNext();) {
            str.append(i.next());
            if (i.hasNext()) {
                str.append(", ");
            }
        }
        str.append(']');
        return str.toString();
    }
}
