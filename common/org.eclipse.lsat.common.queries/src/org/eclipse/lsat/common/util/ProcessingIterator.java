/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This iterator allows you to iterate over something which is processed on-the-fly.
 *
 * @param <E> type of element
 */
public abstract class ProcessingIterator<E> implements Iterator<E> {
    /**
     * A unique object instance to indicate that this Iterator does not have anymore elements.
     */
    private static final Object DONE = new Object() {
        // Empty
    };

    /**
     * A unique object instance to indicate that the iterator value was consumed and the iterator is waiting for the
     * next value.
     */
    private static final Object CONSUMED = new Object() {
        // Empty
    };

    /**
     * A unique object instance to indicate that the iterator is not initialized yet and it is waiting for the first
     * value. Basically the same state as CONSUMED, but subclasses can use this knowledge to postpone initialization
     * until first consumption.
     */
    private static final Object INITIAL = new Object() {
        // Empty
    };

    private Object next = INITIAL;

    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove");
    }

    @Override
    public boolean hasNext() {
        return DONE != prepareNext();
    }

    @Override
    @SuppressWarnings("unchecked")
    public E next() {
        Object n = prepareNext();
        if (DONE == n) {
            throw new NoSuchElementException();
        } else {
            // Consume next
            next = CONSUMED;
        }
        return (E)n;
    }

    private Object prepareNext() {
        if (CONSUMED == next || INITIAL == next) {
            toNext();
        }
        if (CONSUMED == next || INITIAL == next) {
            throw new IllegalStateException("Either call setNext() or done() from toNext()");
        }
        return next;
    }

    /**
     * Subclasses can use this method to postpone initialization until first consumption.
     *
     * @return <tt>true</tt> if this is the first time {@link #toNext()} is being called, <tt>false</tt> otherwise.
     * @see #toNext()
     */
    protected boolean isFirst() {
        return next == INITIAL;
    }

    /**
     * This method is invoked when the iterator needs the next item. The implementer of this method should return the
     * result of calling either {@link #setNext(Object)} with the next item or {@link #done()} if no more items are
     * available. see {@link #isFirst()}
     */
    protected abstract boolean toNext();

    /**
     * Set the next iterator item. May only be called from {@link #toNext()}.
     *
     * @param next next item
     */
    protected final boolean setNext(E next) {
        if (CONSUMED != this.next && INITIAL != this.next) {
            throw new IllegalStateException("Only call setNext() from toNext(): iterator was not consumed yet.");
        }
        this.next = next;
        return true;
    }

    /**
     * Indicates this iterator has come to its end. May only be called from {@link #toNext()}.
     */
    protected final boolean done() {
        if (CONSUMED != this.next && INITIAL != this.next) {
            throw new IllegalStateException("Only call done() from toNext(): iterator was not consumed yet.");
        }
        this.next = DONE;
        return false;
    }

    /**
     * Returns the next element in this iterator, without advancing this iterator.
     *
     * @throws NoSuchElementException if this iterator has no more elements according to {@link #hasNext()}
     */
    @SuppressWarnings("unchecked")
    protected E peek() {
        Object n = prepareNext();
        if (DONE == n) {
            throw new NoSuchElementException();
        }
        return (E)n;
    }

    protected void skip() {
        Object n = prepareNext();
        if (DONE == n) {
            throw new NoSuchElementException();
        } else {
            // Consume next
            next = CONSUMED;
        }
    }
}
