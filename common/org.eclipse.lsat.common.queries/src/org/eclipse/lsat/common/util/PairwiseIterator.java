/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

import org.apache.commons.lang3.tuple.Pair;

/**
 * An {@link Iterator} which returns elements in a pair wise fashion. As example: input [1,2,3,4] will be returned as
 * [{1,2},{2,3},{3,4}]
 *
 * @param <E>
 */
public class PairwiseIterator<E> extends ProcessingIterator<Pair<E, E>> {
    private final Iterator<? extends E> source;

    private E previous;

    public static <E> PairwiseIterator<E> of(Iterator<? extends E> source) {
        return new PairwiseIterator<E>(source);
    }

    public PairwiseIterator(Iterator<? extends E> source) {
        this.source = source;
    }

    @Override
    protected boolean toNext() {
        if (isFirst()) {
            previous = source.hasNext() ? source.next() : null;
        }
        if (source.hasNext()) {
            final Pair<E, E> next = Pair.of(previous, source.next());
            previous = next.getRight();
            return setNext(next);
        } else {
            return done();
        }
    }
}
