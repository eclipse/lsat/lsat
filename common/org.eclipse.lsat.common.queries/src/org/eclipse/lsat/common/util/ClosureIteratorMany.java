/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import static org.eclipse.lsat.common.util.IteratorUtil.flatten;
import static org.eclipse.lsat.common.util.IteratorUtil.map;
import static org.eclipse.lsat.common.util.IteratorUtil.notNull;

import java.util.Iterator;
import java.util.function.Function;

public class ClosureIteratorMany<E> extends UniqueIterator<E> implements BranchIterator<E> {
    private final AppendableIterator<E> closureCandidates;

    boolean canPrune = false;

    public ClosureIteratorMany(Iterator<? extends E> iterator, Function<E, Iterator<? extends E>> functor) {
        // The initial candidates for collecting the closure are the iterator, but during
        // the closure new candidates might be added, hence the AppendableIterator
        this(new AppendableIterator<>(iterator), functor);
    }

    private ClosureIteratorMany(AppendableIterator<E> closureCandidates, Function<E, Iterator<? extends E>> functor) {
        // Now collect all closure results, but each closure result is also a closure
        // candidate. Using a an unique iterator in between (super class) will guard for
        // cycles and guarantee that each element is returned once only
        super(flatten(notNull(map(closureCandidates, functor))));
        this.closureCandidates = closureCandidates;
    }

    @Override
    public E next() {
        E next = super.next();
        // Now close the loop, as each result is a candidate for the 'next' closure
        if (closureCandidates.append(next)) {
            canPrune = true;
        }
        return next;
    }

    @Override
    public void prune() {
        if (canPrune) {
            closureCandidates.undoAppend();
            canPrune = false;
        } else {
            throw new IllegalStateException("Prune is not allowed");
        }
    }
}
