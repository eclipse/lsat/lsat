/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

public class PeekingIterator<E> extends ProcessingIterator<E> {
    private final Iterator<? extends E> source;

    public PeekingIterator(Iterator<? extends E> source) {
        this.source = source;
    }

    @Override
    public E peek() {
        return super.peek();
    }

    @Override
    protected boolean toNext() {
        return source.hasNext() ? setNext(source.next()) : done();
    }
}
