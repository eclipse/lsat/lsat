/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;
import java.util.function.Supplier;

public class SuppliedIterable<T> extends LoggableIterable<T> {
    private final Supplier<? extends Iterator<T>> supplier;

    public SuppliedIterable(Supplier<? extends Iterator<T>> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Iterator<T> iterator() {
        return supplier.get();
    }
}
