/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.function.Function;

/**
 * Represents an function that accepts an object-valued and a {@code int}-valued argument, and produces a result. This
 * is the {@code (reference, int)} specialization for {@link Function}.
 *
 * <p>
 * This is a {@link FunctionalInterface functional interface} whose functional method is {@link #apply(Object, int)}.
 * </p>
 *
 * @param <T> the type of the object argument to the operation
 * @param <R> the type of the result of the function
 */
@FunctionalInterface
public interface ObjIntFunction<T, R> {
    /**
     * Applies this function to the given arguments.
     *
     * @param t the first input argument
     * @param value the second input argument
     */
    R apply(T t, int value);
}
