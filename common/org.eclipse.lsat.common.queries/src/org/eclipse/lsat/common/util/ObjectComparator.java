/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Comparator;

public class ObjectComparator implements Comparator<Object> {
    public static final ObjectComparator INSTANCE = new ObjectComparator(false);

    private final boolean nullFirst;

    public ObjectComparator(boolean nullFirst) {
        this.nullFirst = nullFirst;
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public int compare(final Object o1, final Object o2) {
        if (o1 == o2) {
            return 0;
        }
        if (o1 == null) {
            return nullFirst ? -1 : 1;
        }
        if (o1 instanceof Comparable && o2 instanceof Comparable) {
            if (o1.getClass().isInstance(o2)) {
                return ((Comparable)o1).compareTo(o2);
            }
            if (o2.getClass().isInstance(o1)) {
                return -1 * ((Comparable)o2).compareTo(o1);
            }
        }
        return Integer.compare(System.identityHashCode(o1), System.identityHashCode(o2));
    }

    public boolean equals(final Object o1, final Object o2) {
        return compare(o1, o2) == 0;
    }
}
