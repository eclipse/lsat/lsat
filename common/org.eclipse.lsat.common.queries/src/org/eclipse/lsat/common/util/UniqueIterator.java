/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Set;

public class UniqueIterator<E> extends ProcessingIterator<E> {
    private final Iterator<? extends E> source;

    private final Set<? super E> cache;

    public UniqueIterator(Iterator<? extends E> source) {
        this(source, true);
    }

    public UniqueIterator(Iterator<? extends E> source, boolean useEquals) {
        this(source, useEquals ? new HashSet<E>() : Collections.newSetFromMap(new IdentityHashMap<E, Boolean>()));
    }

    protected UniqueIterator(Iterator<? extends E> source, Set<? super E> cache) {
        this.source = source;
        this.cache = cache;
    }

    @Override
    protected boolean toNext() {
        while (source.hasNext()) {
            E next = source.next();
            if (cache.add(next)) {
                return setNext(next);
            }
        }
        return done();
    }
}
