/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

public final class IteratorUtil {
    private IteratorUtil() {
        // Empty for utility classes
    }

    /**
     * Returns an iterator containing only the specified {@code element}.
     *
     * @param <E> the class of the objects in the set
     * @param element the sole element to be stored in the returned iterator.
     * @return an iterator containing only the specified {@code element}.
     * @see Collections#singletonIterator(Object)
     */
    public static <E> Iterator<E> singletonIterator(final E element) {
        return new Iterator<E>() {
            private boolean hasNext = true;

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public E next() {
                if (hasNext) {
                    hasNext = false;
                    return element;
                }
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("remove");
            }
        };
    }

    /**
     * @return all values of {@code getter}, applied on {@code owner}, from 0 until {@code size}
     */
    public static <E, T> Iterator<E> iterate(T owner, ObjIntFunction<? super T, E> getter,
            ToIntFunction<? super T> size)
    {
        return new Iterator<E>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < size.applyAsInt(owner);
            }

            @Override
            public E next() {
                return getter.apply(owner, index++);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("remove");
            }
        };
    }

    /**
     * Returns the next element (if any) in the iteration, null otherwise.
     *
     * @param source the iteration
     * @return the next element (if any) in the iteration, null otherwise
     * @see Iterator#next()
     */
    public static <E> E safeNext(Iterator<E> source) {
        return source.hasNext() ? source.next() : null;
    }

    public static <E> Iterator<E> unique(Iterator<E> source) {
        return new UniqueIterator<E>(source);
    }

    public static <E> Iterator<E> notNull(Iterator<E> source) {
        return new NullSkippingIterator<E>(source);
    }

    /**
     * Returns an iterator consisting of the results of applying the given function to the elements of this stream.
     *
     * @see Stream#map(Function)
     */
    public static <Input, Output> Iterator<Output> map(Iterator<? extends Input> iterator,
            Function<Input, Output> functor)
    {
        return new Iterator<Output>() {
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public Output next() {
                return functor.apply(iterator.next());
            }

            @Override
            public void remove() {
                iterator.remove();
            }
        };
    }

    /**
     * Joins the iterators into one iterator while iterating.
     *
     * @param iterators the iterators to join.
     * @return the single joined/merged iterator.
     */
    public static final <T> Iterator<T> flatten(final Iterator<? extends Iterator<? extends T>> iterator) {
        return new CompoundIterator<T>(iterator);
    }

    /**
     * Shorthand for {@link #flatten(Iterator)}, {@link #notNull(Iterator)} and {@link #map(Iterator, Function)}
     */
    public static final <Input, Output> Iterator<Output> flatMap(Iterator<? extends Input> iterator,
            Function<Input, Iterator<? extends Output>> functor)
    {
        return flatten(notNull(map(iterator, functor)));
    }

    public static <Input> Iterator<Input> filter(Iterator<Input> iterator, Predicate<? super Input> predicate) {
        return new ProcessingIterator<Input>() {
            @Override
            protected boolean toNext() {
                while (iterator.hasNext()) {
                    final Input next = iterator.next();
                    if (predicate.test(next)) {
                        return setNext(next);
                    }
                }
                return done();
            }
        };
    }

    /**
     * Joins the iterators into one iterator while iterating.
     *
     * @param iterators the iterators to join.
     * @return the single joined/merged iterator.
     */
    @SafeVarargs
    public static final <T> Iterator<T> join(final Iterator<? extends T>... iterators) {
        return new CompoundIterator<T>(iterators);
    }

    /**
     * Returns <tt>true</tt> if the iterator contains the specified element. More formally, returns <tt>true</tt> if and
     * only if this iterator contains at least one element <tt>e</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param iterator iterator
     * @param o element whose presence in this iterator is to be tested
     * @return <tt>true</tt> if this iterator contains the specified element
     * @throws ClassCastException if the type of the specified element is incompatible with this collection
     *     (<a href="#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this collection does not permit null elements
     *     (<a href="#optional-restrictions">optional</a>)
     * @see Collection#contains(Object)
     */
    public static final boolean contains(Iterator<?> iterator, Object o) {
        while (iterator.hasNext()) {
            Object e = iterator.next();
            if (o == null ? e == null : o.equals(e)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the minimum element of the given iterator, according to the <i>natural ordering</i> of its elements. All
     * elements in the iterator must implement the <tt>Comparable</tt> interface. Furthermore, all elements in the
     * iterator must be <i>mutually comparable</i> (that is, <tt>e1.compareTo(e2)</tt> must not throw a
     * <tt>ClassCastException</tt> for any elements <tt>e1</tt> and <tt>e2</tt> in the iterator).
     *
     * <p>
     * This method iterates over the entire iterator, hence it requires time proportional to the size of the iterator.
     * </p>
     *
     * @param iterator the iterator whose minimum element is to be determined.
     * @param _default result if the collection is empty.
     * @return the minimum element of the given iterator, according to the <i>natural ordering</i> of its elements, or
     *     <tt>_default</tt> if the iterator is empty.
     * @see Comparable
     * @see Collections#min(Collection)
     */
    public static final <T extends Comparable<? super T>> T min(Iterator<? extends T> iterator, T _default) {
        if (null == iterator) {
            return _default;
        }
        if (!iterator.hasNext()) {
            return _default;
        }
        T candidate = iterator.next();
        while (iterator.hasNext()) {
            T next = iterator.next();
            if (next.compareTo(candidate) < 0) {
                candidate = next;
            }
        }
        return candidate;
    }

    /**
     * Returns the maximum element of the given iterator, according to the <i>natural ordering</i> of its elements. All
     * elements in the iterator must implement the <tt>Comparable</tt> interface. Furthermore, all elements in the
     * iterator must be <i>mutually comparable</i> (that is, <tt>e1.compareTo(e2)</tt> must not throw a
     * <tt>ClassCastException</tt> for any elements <tt>e1</tt> and <tt>e2</tt> in the iterator).
     *
     * <p>
     * This method iterates over the entire iterator, hence it requires time proportional to the size of the iterator.
     * </p>
     *
     * @param iterator the iterator whose maximum element is to be determined.
     * @param _default result if the collection is empty.
     * @return the maximum element of the given iterator, according to the <i>natural ordering</i> of its elements, or
     *     <tt>_default</tt> if the iterator is empty.
     * @see Comparable
     * @see Collections#max(Collection)
     */
    public static final <T extends Comparable<? super T>> T max(Iterator<? extends T> iterator, T _default) {
        if (null == iterator) {
            return _default;
        }
        if (!iterator.hasNext()) {
            return _default;
        }
        T candidate = iterator.next();
        while (iterator.hasNext()) {
            T next = iterator.next();
            if (next.compareTo(candidate) > 0) {
                candidate = next;
            }
        }
        return candidate;
    }

    public static final <T> Iterator<T> drop(final Iterator<? extends T> iterator, final int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount should be positive");
        }
        return new ProcessingIterator<T>() {
            private int dropped = 0;

            @Override
            protected boolean toNext() {
                while (dropped++ < amount && iterator.hasNext()) {
                    iterator.next();
                }
                return iterator.hasNext() ? setNext(iterator.next()) : done();
            }
        };
    }

    /**
     * Returns an array containing all of the elements in this iterator in proper sequence (from first to last element).
     *
     * <p>
     * The returned array will be "safe" in that no references to it are maintained by this iterator. (In other words,
     * this method must allocate a new array). The caller is thus free to modify the returned array.
     * </p>
     * <p>
     * This method acts as bridge between array-based and collection-based APIs.
     * </p>
     *
     * @param iterator the elements of the iterator are to be stored
     * @return an array containing the elements of the iterator
     * @throws NullPointerException if the specified array is null
     * @see Collection#toArray()
     */
    public static Object[] toArray(Iterator<?> iterator) {
        return asList(iterator).toArray();
    }

    /**
     * Returns an array containing all of the elements in this iterator in proper sequence (from first to last element);
     * the runtime type of the returned array is that of the specified type.
     *
     * <p>
     * Like the {@link #toArray(Iterator)} method, this method acts as bridge between array-based and collection-based
     * APIs. Further, this method allows precise control over the runtime type of the output array, and may, under
     * certain circumstances, be used to save allocation costs.
     * </p>
     *
     * @param type type of the returning array
     * @param iterator the elements of the iterator are to be stored
     * @return an array containing the elements of the iterator
     * @throws ArrayStoreException if the runtime type of the specified array is not a supertype of the runtime type of
     *     every element in this list
     * @throws NullPointerException if the specified array is null
     * @see Collection#toArray(Object[])
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] toArray(Iterator<?> iterator, Class<T> type) {
        final LinkedList<?> list = asList(iterator);
        return list.toArray((T[])Array.newInstance(type, list.size()));
    }

    /**
     * Returns a {@link LinkedList} filled with the content of iterator.
     *
     * @param iterator content
     * @return the {@link LinkedList}
     */
    public static <T> LinkedList<T> asList(Iterator<T> iterator) {
        LinkedList<T> list = new LinkedList<>();
        CollectionUtil.addAll(list, iterator);
        return list;
    }

    /**
     * Returns a {@link ArrayList} with the specified initial capacity, filled with the content of iterator.
     *
     * @param iterator content
     * @param initialCapacity the initial capacity of the resulting list.
     * @return the {@link ArrayList}
     */
    public static <T> ArrayList<T> asList(Iterator<T> iterator, int initialCapacity) {
        ArrayList<T> list = new ArrayList<>(initialCapacity);
        CollectionUtil.addAll(list, iterator);
        return list;
    }

    /**
     * Returns a {@link Set} filled with the content of iterator.
     *
     * @param iterator content
     * @return the {@link Set}
     */
    public static <T> Set<T> asSet(Iterator<T> iterator) {
        HashSet<T> set = new HashSet<>();
        CollectionUtil.addAll(set, iterator);
        return set;
    }

    /**
     * Returns a {@link LinkedHashSet} filled with the content of iterator.
     *
     * @param iterator content
     * @return the {@link LinkedHashSet}
     */
    public static <T> LinkedHashSet<T> asOrderedSet(Iterator<T> iterator) {
        LinkedHashSet<T> set = new LinkedHashSet<>();
        CollectionUtil.addAll(set, iterator);
        return set;
    }

    /**
     * <b>NOTE: The {@link Iterable} can be iterated only once!</b>
     *
     * @param source iterator to be iterable
     * @return the iterator as iterable-once
     */
    public static <E> Iterable<E> toIterable(Iterator<E> source) {
        return new IterableIterator<E>(source);
    }

    /**
     * @param source iterator to be appendable
     * @return an {@link AppendableIterator} with source as initial content
     */
    public static <E> AppendableIterator<E> toAppendable(Iterator<E> source) {
        return new AppendableIterator<E>(source);
    }

    /**
     * @param source iterator to be buffered
     * @return an {@link BufferedIterator} with source as initial content
     */
    public static <E> BufferedIterator<E> toBuffered(Iterator<E> source) {
        return new BufferedIterator<E>(source);
    }

    /**
     * @param source iterator to be peeked
     * @return an {@link PeekingIterator} with source as initial content
     */
    public static <E> PeekingIterator<E> toPeeking(Iterator<E> source) {
        return new PeekingIterator<E>(source);
    }
}
