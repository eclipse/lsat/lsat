/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.util;

import java.util.Iterator;

import org.apache.commons.lang3.tuple.Pair;

/**
 * An {@link Iterable} which returns elements in a pair wise fashion. As example: input [1,2,3,4] will be returned as
 * [{1,2},{2,3},{3,4}]
 *
 * @param <E>
 */
public class PairwiseIterable<E> extends LoggableIterable<Pair<E, E>> {
    private final Iterable<? extends E> source;

    public static <E> PairwiseIterable<E> of(Iterable<? extends E> source) {
        return new PairwiseIterable<E>(source);
    }

    public PairwiseIterable(Iterable<? extends E> source) {
        this.source = source;
    }

    @Override
    public Iterator<Pair<E, E>> iterator() {
        return new PairwiseIterator<E>(source.iterator());
    }
}
