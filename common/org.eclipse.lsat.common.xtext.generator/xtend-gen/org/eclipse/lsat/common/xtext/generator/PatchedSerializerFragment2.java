/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.xtext.generator;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.apache.log4j.Logger;
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.analysis.IGrammarConstraintProvider;
import org.eclipse.xtext.serializer.analysis.ISemanticSequencerNfaProvider;
import org.eclipse.xtext.serializer.analysis.SerializationContext;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.util.internal.Log;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.eclipse.xtext.xtext.generator.grammarAccess.GrammarAccessExtensions;
import org.eclipse.xtext.xtext.generator.model.FileAccessFactory;
import org.eclipse.xtext.xtext.generator.model.GeneratedJavaFileAccess;
import org.eclipse.xtext.xtext.generator.model.TypeReference;
import org.eclipse.xtext.xtext.generator.model.annotations.IClassAnnotation;
import org.eclipse.xtext.xtext.generator.model.annotations.SuppressWarningsAnnotation;
import org.eclipse.xtext.xtext.generator.serializer.SemanticSequencerExtensions;
import org.eclipse.xtext.xtext.generator.serializer.SerializerFragment2;
import org.eclipse.xtext.xtext.generator.util.GenModelUtil2;

@Log
@SuppressWarnings("all")
public class PatchedSerializerFragment2 extends SerializerFragment2 {
  private static <K extends Object, V extends Object> Map<K, V> toMap(final Iterable<Pair<K, V>> items) {
    LinkedHashMap<K, V> _xblockexpression = null;
    {
      final LinkedHashMap<K, V> result = CollectionLiterals.<K, V>newLinkedHashMap();
      for (final Pair<K, V> i : items) {
        result.put(i.getKey(), i.getValue());
      }
      _xblockexpression = result;
    }
    return _xblockexpression;
  }
  
  @Inject
  @Extension
  private SemanticSequencerExtensions _semanticSequencerExtensions;
  
  @Inject
  @Extension
  private GrammarAccessExtensions _grammarAccessExtensions;
  
  @Inject
  private FileAccessFactory fileAccessFactory;
  
  @Override
  public void generateAbstractSemanticSequencer() {
    final Collection<IGrammarConstraintProvider.IConstraint> localConstraints = this._semanticSequencerExtensions.getGrammarConstraints(this.getGrammar());
    final Collection<IGrammarConstraintProvider.IConstraint> superConstraints = this._semanticSequencerExtensions.getGrammarConstraints(this._semanticSequencerExtensions.getSuperGrammar(this.getGrammar()));
    final Function1<IGrammarConstraintProvider.IConstraint, Boolean> _function = (IGrammarConstraintProvider.IConstraint it) -> {
      return Boolean.valueOf(((it.getType() != null) && (!superConstraints.contains(it))));
    };
    final Set<IGrammarConstraintProvider.IConstraint> newLocalConstraints = IterableExtensions.<IGrammarConstraintProvider.IConstraint>toSet(IterableExtensions.<IGrammarConstraintProvider.IConstraint>filter(localConstraints, _function));
    TypeReference _xifexpression = null;
    boolean _isGenerateStub = this.isGenerateStub();
    if (_isGenerateStub) {
      _xifexpression = this.getAbstractSemanticSequencerClass(this.getGrammar());
    } else {
      _xifexpression = this.getSemanticSequencerClass(this.getGrammar());
    }
    final TypeReference clazz = _xifexpression;
    TypeReference _xifexpression_1 = null;
    final Function1<IGrammarConstraintProvider.IConstraint, Boolean> _function_1 = (IGrammarConstraintProvider.IConstraint it) -> {
      return Boolean.valueOf(superConstraints.contains(it));
    };
    boolean _exists = IterableExtensions.<IGrammarConstraintProvider.IConstraint>exists(localConstraints, _function_1);
    if (_exists) {
      _xifexpression_1 = this.getSemanticSequencerClass(IterableExtensions.<Grammar>head(this.getGrammar().getUsedGrammars()));
    } else {
      _xifexpression_1 = TypeReference.typeRef(AbstractDelegatingSemanticSequencer.class);
    }
    final TypeReference superClazz = _xifexpression_1;
    final GeneratedJavaFileAccess javaFile = this.fileAccessFactory.createGeneratedJavaFile(clazz);
    javaFile.setResourceSet(this.getLanguage().getResourceSet());
    final HashSet<Pair<String, EClass>> methodSignatures = CollectionLiterals.<Pair<String, EClass>>newHashSet();
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("public ");
        {
          boolean _isGenerateStub = PatchedSerializerFragment2.this.isGenerateStub();
          if (_isGenerateStub) {
            _builder.append("abstract ");
          }
        }
        _builder.append("class ");
        String _simpleName = clazz.getSimpleName();
        _builder.append(_simpleName);
        _builder.append(" extends ");
        _builder.append(superClazz);
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("    ");
        _builder.append("@");
        _builder.append(Inject.class, "    ");
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.append("private ");
        TypeReference _grammarAccess = PatchedSerializerFragment2.this._grammarAccessExtensions.getGrammarAccess(PatchedSerializerFragment2.this.getGrammar());
        _builder.append(_grammarAccess, "    ");
        _builder.append(" grammarAccess;");
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.newLine();
        _builder.append("    ");
        StringConcatenationClient _genMethodCreateSequence = PatchedSerializerFragment2.this.genMethodCreateSequence();
        _builder.append(_genMethodCreateSequence, "    ");
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.newLine();
        {
          List<IGrammarConstraintProvider.IConstraint> _sort = IterableExtensions.<IGrammarConstraintProvider.IConstraint>sort(newLocalConstraints);
          for(final IGrammarConstraintProvider.IConstraint c : _sort) {
            {
              String _simpleName_1 = c.getSimpleName();
              EClass _type = c.getType();
              Pair<String, EClass> _mappedTo = Pair.<String, EClass>of(_simpleName_1, _type);
              boolean _add = methodSignatures.add(_mappedTo);
              if (_add) {
                _builder.append("    ");
                StringConcatenationClient _genMethodSequence = PatchedSerializerFragment2.this.genMethodSequence(c);
                _builder.append(_genMethodSequence, "    ");
                _builder.newLineIfNotEmpty();
              } else {
                _builder.append("    ");
                String _simpleName_2 = clazz.getSimpleName();
                String _plus = ("Skipped generating duplicate method in " + _simpleName_2);
                PatchedSerializerFragment2.LOG.warn(_plus);
                _builder.newLineIfNotEmpty();
                _builder.append("    ");
                StringConcatenationClient _genMethodSequenceComment = PatchedSerializerFragment2.this.genMethodSequenceComment(c);
                _builder.append(_genMethodSequenceComment, "    ");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("    ");
            _builder.newLine();
          }
        }
        _builder.append("}");
        _builder.newLine();
      }
    };
    javaFile.setContent(_client);
    List<IClassAnnotation> _annotations = javaFile.getAnnotations();
    SuppressWarningsAnnotation _suppressWarningsAnnotation = new SuppressWarningsAnnotation();
    _annotations.add(_suppressWarningsAnnotation);
    javaFile.writeTo(this.getProjectConfig().getRuntime().getSrcGen());
  }
  
  private Iterable<EPackage> getAccessedPackages() {
    final Function1<IGrammarConstraintProvider.IConstraint, Boolean> _function = (IGrammarConstraintProvider.IConstraint it) -> {
      EClass _type = it.getType();
      return Boolean.valueOf((_type != null));
    };
    final Function1<IGrammarConstraintProvider.IConstraint, EPackage> _function_1 = (IGrammarConstraintProvider.IConstraint it) -> {
      return it.getType().getEPackage();
    };
    final Function1<EPackage, String> _function_2 = (EPackage it) -> {
      return it.getName();
    };
    return IterableExtensions.<EPackage, String>sortBy(IterableExtensions.<EPackage>toSet(IterableExtensions.<IGrammarConstraintProvider.IConstraint, EPackage>map(IterableExtensions.<IGrammarConstraintProvider.IConstraint>filter(this._semanticSequencerExtensions.getGrammarConstraints(this.getGrammar()), _function), _function_1)), _function_2);
  }
  
  private Iterable<EClass> getAccessedClasses(final EPackage pkg) {
    final Function1<IGrammarConstraintProvider.IConstraint, EClass> _function = (IGrammarConstraintProvider.IConstraint it) -> {
      return it.getType();
    };
    final Function1<EClass, Boolean> _function_1 = (EClass it) -> {
      return Boolean.valueOf(((it != null) && Objects.equal(it.getEPackage(), pkg)));
    };
    final Function1<EClass, String> _function_2 = (EClass it) -> {
      return it.getName();
    };
    return IterableExtensions.<EClass, String>sortBy(IterableExtensions.<EClass>toSet(IterableExtensions.<EClass>filter(IterableExtensions.<IGrammarConstraintProvider.IConstraint, EClass>map(this._semanticSequencerExtensions.getGrammarConstraints(this.getGrammar()), _function), _function_1)), _function_2);
  }
  
  private StringConcatenationClient genMethodCreateSequence() {
    StringConcatenationClient _xblockexpression = null;
    {
      final Function1<IGrammarConstraintProvider.IConstraint, Pair<IGrammarConstraintProvider.IConstraint, IGrammarConstraintProvider.IConstraint>> _function = (IGrammarConstraintProvider.IConstraint it) -> {
        return Pair.<IGrammarConstraintProvider.IConstraint, IGrammarConstraintProvider.IConstraint>of(it, it);
      };
      final Map<IGrammarConstraintProvider.IConstraint, IGrammarConstraintProvider.IConstraint> superConstraints = PatchedSerializerFragment2.<IGrammarConstraintProvider.IConstraint, IGrammarConstraintProvider.IConstraint>toMap(IterableExtensions.<IGrammarConstraintProvider.IConstraint, Pair<IGrammarConstraintProvider.IConstraint, IGrammarConstraintProvider.IConstraint>>map(this._semanticSequencerExtensions.getGrammarConstraints(this._semanticSequencerExtensions.getSuperGrammar(this.getGrammar())), _function));
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("@Override");
          _builder.newLine();
          _builder.append("public void sequence(");
          _builder.append(ISerializationContext.class);
          _builder.append(" context, ");
          _builder.append(EObject.class);
          _builder.append(" semanticObject) {");
          _builder.newLineIfNotEmpty();
          _builder.append("    ");
          _builder.append(EPackage.class, "    ");
          _builder.append(" epackage = semanticObject.eClass().getEPackage();");
          _builder.newLineIfNotEmpty();
          _builder.append("    ");
          _builder.append(ParserRule.class, "    ");
          _builder.append(" rule = context.getParserRule();");
          _builder.newLineIfNotEmpty();
          _builder.append("    ");
          _builder.append(Action.class, "    ");
          _builder.append(" action = context.getAssignedAction();");
          _builder.newLineIfNotEmpty();
          _builder.append("    ");
          _builder.append(Set.class, "    ");
          _builder.append("<");
          _builder.append(Parameter.class, "    ");
          _builder.append("> parameters = context.getEnabledBooleanParameters();");
          _builder.newLineIfNotEmpty();
          {
            Iterable<Pair<Integer, EPackage>> _indexed = IterableExtensions.<EPackage>indexed(PatchedSerializerFragment2.this.getAccessedPackages());
            for(final Pair<Integer, EPackage> pkg : _indexed) {
              _builder.append("    ");
              {
                Integer _key = pkg.getKey();
                boolean _greaterThan = ((_key).intValue() > 0);
                if (_greaterThan) {
                  _builder.append("else ");
                }
              }
              _builder.append("if (epackage == ");
              EPackage _value = pkg.getValue();
              _builder.append(_value, "    ");
              _builder.append(".");
              String _packageLiteral = GenModelUtil2.getPackageLiteral();
              _builder.append(_packageLiteral, "    ");
              _builder.append(")");
              _builder.newLineIfNotEmpty();
              _builder.append("    ");
              _builder.append("    ");
              _builder.append("switch (semanticObject.eClass().getClassifierID()) {");
              _builder.newLine();
              {
                Iterable<EClass> _accessedClasses = PatchedSerializerFragment2.this.getAccessedClasses(pkg.getValue());
                for(final EClass type : _accessedClasses) {
                  _builder.append("    ");
                  _builder.append("    ");
                  _builder.append("case ");
                  EPackage _value_1 = pkg.getValue();
                  _builder.append(_value_1, "        ");
                  _builder.append(".");
                  String _intLiteral = GenModelUtil2.getIntLiteral(type, PatchedSerializerFragment2.this.getLanguage().getResourceSet());
                  _builder.append(_intLiteral, "        ");
                  _builder.append(":");
                  _builder.newLineIfNotEmpty();
                  _builder.append("    ");
                  _builder.append("    ");
                  _builder.append("    ");
                  StringConcatenationClient _genMethodCreateSequenceCaseBody = PatchedSerializerFragment2.this.genMethodCreateSequenceCaseBody(superConstraints, type);
                  _builder.append(_genMethodCreateSequenceCaseBody, "            ");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("    ");
              _builder.append("    ");
              _builder.append("}");
              _builder.newLine();
            }
          }
          _builder.append("    ");
          _builder.append("if (errorAcceptor != null)");
          _builder.newLine();
          _builder.append("        ");
          _builder.append("errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
        }
      };
      _xblockexpression = _client;
    }
    return _xblockexpression;
  }
  
  private StringConcatenationClient genParameterCondition(final ISerializationContext context, final IGrammarConstraintProvider.IConstraint constraint) {
    StringConcatenationClient _xblockexpression = null;
    {
      final Set<Parameter> values = context.getEnabledBooleanParameters();
      StringConcatenationClient _xifexpression = null;
      boolean _isEmpty = values.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append(ImmutableSet.class);
            _builder.append(".of(");
            final Function1<Parameter, String> _function = (Parameter it) -> {
              String _gaAccessor = PatchedSerializerFragment2.this._grammarAccessExtensions.gaAccessor(it);
              return ("grammarAccess." + _gaAccessor);
            };
            String _join = IterableExtensions.join(IterableExtensions.<Parameter, String>map(values, _function), ", ");
            _builder.append(_join);
            _builder.append(").equals(parameters)");
          }
        };
        _xifexpression = _client;
      } else {
        StringConcatenationClient _xifexpression_1 = null;
        final Function1<ISerializationContext, Boolean> _function = (ISerializationContext it) -> {
          boolean _isEmpty_1 = ((SerializationContext) it).getDeclaredParameters().isEmpty();
          return Boolean.valueOf((!_isEmpty_1));
        };
        boolean _exists = IterableExtensions.<ISerializationContext>exists(constraint.getContexts(), _function);
        if (_exists) {
          StringConcatenationClient _client_1 = new StringConcatenationClient() {
            @Override
            protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
              _builder.append("parameters.isEmpty()");
            }
          };
          _xifexpression_1 = _client_1;
        } else {
          StringConcatenationClient _client_2 = new StringConcatenationClient() {
            @Override
            protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            }
          };
          _xifexpression_1 = _client_2;
        }
        _xifexpression = _xifexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  private StringConcatenationClient genMethodCreateSequenceCaseBody(final Map<IGrammarConstraintProvider.IConstraint, IGrammarConstraintProvider.IConstraint> superConstraints, final EClass type) {
    StringConcatenationClient _xblockexpression = null;
    {
      final Function1<Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>>, String> _function = (Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>> it) -> {
        return it.getKey().getName();
      };
      final List<Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>>> contexts = IterableExtensions.<Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>>, String>sortBy(this._semanticSequencerExtensions.getGrammarConstraints(this.getGrammar(), type).entrySet(), _function);
      final LinkedHashMultimap<EObject, IGrammarConstraintProvider.IConstraint> context2constraint = LinkedHashMultimap.<EObject, IGrammarConstraintProvider.IConstraint>create();
      for (final Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>> e : contexts) {
        List<ISerializationContext> _value = e.getValue();
        for (final ISerializationContext ctx : _value) {
          context2constraint.put(((SerializationContext) ctx).getActionOrRule(), e.getKey());
        }
      }
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          {
            int _size = contexts.size();
            boolean _greaterThan = (_size > 1);
            if (_greaterThan) {
              {
                Iterable<Pair<Integer, Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>>>> _indexed = IterableExtensions.<Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>>>indexed(contexts);
                for(final Pair<Integer, Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>>> ctx : _indexed) {
                  {
                    Integer _key = ctx.getKey();
                    boolean _greaterThan_1 = ((_key).intValue() > 0);
                    if (_greaterThan_1) {
                      _builder.append("else ");
                    }
                  }
                  _builder.append("if (");
                  StringConcatenationClient _genCondition = PatchedSerializerFragment2.this.genCondition(ctx.getValue().getValue(), ctx.getValue().getKey(), context2constraint);
                  _builder.append(_genCondition);
                  _builder.append(") {");
                  _builder.newLineIfNotEmpty();
                  _builder.append("    ");
                  StringConcatenationClient _genMethodCreateSequenceCall = PatchedSerializerFragment2.this.genMethodCreateSequenceCall(superConstraints, type, ctx.getValue().getKey());
                  _builder.append(_genMethodCreateSequenceCall, "    ");
                  _builder.newLineIfNotEmpty();
                  _builder.append("}");
                  _builder.newLine();
                }
              }
              _builder.append("else break;");
              _builder.newLine();
            } else {
              int _size_1 = contexts.size();
              boolean _equals = (_size_1 == 1);
              if (_equals) {
                StringConcatenationClient _genMethodCreateSequenceCall_1 = PatchedSerializerFragment2.this.genMethodCreateSequenceCall(superConstraints, type, IterableExtensions.<Map.Entry<IGrammarConstraintProvider.IConstraint, List<ISerializationContext>>>head(contexts).getKey());
                _builder.append(_genMethodCreateSequenceCall_1);
                _builder.newLineIfNotEmpty();
              } else {
                _builder.append("// error, no contexts. ");
                _builder.newLine();
              }
            }
          }
        }
      };
      _xblockexpression = _client;
    }
    return _xblockexpression;
  }
  
  private StringConcatenationClient genCondition(final List<ISerializationContext> contexts, final IGrammarConstraintProvider.IConstraint constraint, final Multimap<EObject, IGrammarConstraintProvider.IConstraint> ctx2ctr) {
    StringConcatenationClient _xblockexpression = null;
    {
      final List<ISerializationContext> sorted = IterableExtensions.<ISerializationContext>sort(contexts);
      final LinkedHashMultimap<EObject, ISerializationContext> index = LinkedHashMultimap.<EObject, ISerializationContext>create();
      final Consumer<ISerializationContext> _function = (ISerializationContext it) -> {
        index.put(this.getContextObject(it), it);
      };
      sorted.forEach(_function);
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          {
            Set<EObject> _keySet = index.keySet();
            boolean _hasElements = false;
            for(final EObject obj : _keySet) {
              if (!_hasElements) {
                _hasElements = true;
              } else {
                _builder.appendImmediate("\n\t\t|| ", "");
              }
              StringConcatenationClient _genObjectSelector = PatchedSerializerFragment2.this.genObjectSelector(obj);
              _builder.append(_genObjectSelector);
              {
                int _size = ctx2ctr.get(obj).size();
                boolean _greaterThan = (_size > 1);
                if (_greaterThan) {
                  StringConcatenationClient _genParameterSelector = PatchedSerializerFragment2.this.genParameterSelector(obj, index.get(obj), constraint);
                  _builder.append(_genParameterSelector);
                }
              }
            }
          }
        }
      };
      _xblockexpression = _client;
    }
    return _xblockexpression;
  }
  
  private StringConcatenationClient genObjectSelector(final EObject obj) {
    StringConcatenationClient _switchResult = null;
    boolean _matched = false;
    if (obj instanceof Action) {
      _matched=true;
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("action == grammarAccess.");
          String _gaAccessor = PatchedSerializerFragment2.this._grammarAccessExtensions.gaAccessor(obj);
          _builder.append(_gaAccessor);
        }
      };
      _switchResult = _client;
    }
    if (!_matched) {
      if (obj instanceof ParserRule) {
        _matched=true;
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("rule == grammarAccess.");
            String _gaAccessor = PatchedSerializerFragment2.this._grammarAccessExtensions.gaAccessor(obj);
            _builder.append(_gaAccessor);
          }
        };
        _switchResult = _client;
      }
    }
    return _switchResult;
  }
  
  private StringConcatenationClient genParameterSelector(final EObject obj, final Set<ISerializationContext> contexts, final IGrammarConstraintProvider.IConstraint constraint) {
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append(" ");
        _builder.append("&& (");
        {
          boolean _hasElements = false;
          for(final ISerializationContext context : contexts) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate("\n\t\t\t|| ", " ");
            }
            StringConcatenationClient _genParameterCondition = PatchedSerializerFragment2.this.genParameterCondition(context, constraint);
            _builder.append(_genParameterCondition, " ");
          }
        }
        _builder.append(")");
      }
    };
    return _client;
  }
  
  private EObject getContextObject(final ISerializationContext context) {
    EObject _elvis = null;
    Action _assignedAction = context.getAssignedAction();
    if (_assignedAction != null) {
      _elvis = _assignedAction;
    } else {
      ParserRule _parserRule = context.getParserRule();
      _elvis = _parserRule;
    }
    return _elvis;
  }
  
  private StringConcatenationClient genMethodCreateSequenceCall(final Map<IGrammarConstraintProvider.IConstraint, IGrammarConstraintProvider.IConstraint> superConstraints, final EClass type, final IGrammarConstraintProvider.IConstraint key) {
    final IGrammarConstraintProvider.IConstraint superConstraint = superConstraints.get(key);
    IGrammarConstraintProvider.IConstraint _elvis = null;
    if (superConstraint != null) {
      _elvis = superConstraint;
    } else {
      _elvis = key;
    }
    final IGrammarConstraintProvider.IConstraint constraint = _elvis;
    StringConcatenationClient _xifexpression = null;
    boolean _isMapEntry = this.isMapEntry(type);
    if (_isMapEntry) {
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("sequence_");
          String _simpleName = constraint.getSimpleName();
          _builder.append(_simpleName);
          _builder.append("(context, (");
          _builder.append(type);
          _builder.append(" & EObject) semanticObject); ");
          _builder.newLineIfNotEmpty();
          _builder.append("return; ");
          _builder.newLine();
        }
      };
      _xifexpression = _client;
    } else {
      StringConcatenationClient _client_1 = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("sequence_");
          String _simpleName = constraint.getSimpleName();
          _builder.append(_simpleName);
          _builder.append("(context, (");
          _builder.append(type);
          _builder.append(") semanticObject); ");
          _builder.newLineIfNotEmpty();
          _builder.append("return; ");
          _builder.newLine();
        }
      };
      _xifexpression = _client_1;
    }
    return _xifexpression;
  }
  
  private StringConcatenationClient genMethodSequenceComment(final IGrammarConstraintProvider.IConstraint c) {
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("// This method is commented out because it has the same signature as another method in this class.");
        _builder.newLine();
        _builder.append("// This is probably a bug in Xtext\'s serializer, please report it here: ");
        _builder.newLine();
        _builder.append("// https://bugs.eclipse.org/bugs/enter_bug.cgi?product=TMF");
        _builder.newLine();
        _builder.append("//");
        _builder.newLine();
        _builder.append("// Contexts:");
        _builder.newLine();
        _builder.append("//     ");
        String _replaceAll = IterableExtensions.join(IterableExtensions.<ISerializationContext>sort(c.getContexts()), "\n").replaceAll("\\n", "\n//     ");
        _builder.append(_replaceAll);
        _builder.newLineIfNotEmpty();
        _builder.append("//");
        _builder.newLine();
        _builder.append("// Constraint:");
        _builder.newLine();
        _builder.append("//     ");
        {
          IGrammarConstraintProvider.IConstraintElement _body = c.getBody();
          boolean _tripleEquals = (_body == null);
          if (_tripleEquals) {
            _builder.append("{");
            String _name = c.getType().getName();
            _builder.append(_name);
            _builder.append("}");
          } else {
            String _replaceAll_1 = c.getBody().toString().replaceAll("\\n", "\n//     ");
            _builder.append(_replaceAll_1);
          }
        }
        _builder.newLineIfNotEmpty();
        _builder.append("//");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("// ");
        StringConcatenationClient _genMethodSequenceDeclaration = PatchedSerializerFragment2.this.genMethodSequenceDeclaration(c);
        _builder.append(_genMethodSequenceDeclaration, "\t\t");
        _builder.append(" { }");
        _builder.newLineIfNotEmpty();
      }
    };
    return _client;
  }
  
  private StringConcatenationClient genMethodSequence(final IGrammarConstraintProvider.IConstraint c) {
    StringConcatenationClient _xblockexpression = null;
    {
      final ResourceSet rs = this.getLanguage().getResourceSet();
      StringConcatenationClient _xifexpression = null;
      boolean _isEObjectExtension = GenModelUtil2.getGenClass(c.getType(), rs).isEObjectExtension();
      if (_isEObjectExtension) {
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          }
        };
        _xifexpression = _client;
      } else {
        StringConcatenationClient _client_1 = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("(");
            _builder.append(EObject.class);
            _builder.append(") ");
          }
        };
        _xifexpression = _client_1;
      }
      final StringConcatenationClient cast = _xifexpression;
      final List<ISemanticSequencerNfaProvider.ISemState> states = this._semanticSequencerExtensions.getLinearListOfMandatoryAssignments(c);
      StringConcatenationClient _client_2 = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("/**");
          _builder.newLine();
          _builder.append(" ");
          _builder.append("* Contexts:");
          _builder.newLine();
          _builder.append(" ");
          _builder.append("*     ");
          String _replaceAll = IterableExtensions.join(IterableExtensions.<ISerializationContext>sort(c.getContexts()), "\n").replaceAll("\\n", "\n*     ");
          _builder.append(_replaceAll, " ");
          _builder.newLineIfNotEmpty();
          _builder.append(" ");
          _builder.append("*");
          _builder.newLine();
          _builder.append(" ");
          _builder.append("* Constraint:");
          _builder.newLine();
          _builder.append(" ");
          _builder.append("*     ");
          {
            IGrammarConstraintProvider.IConstraintElement _body = c.getBody();
            boolean _tripleEquals = (_body == null);
            if (_tripleEquals) {
              _builder.append("{");
              String _name = c.getType().getName();
              _builder.append(_name, " ");
              _builder.append("}");
            } else {
              String _replaceAll_1 = c.getBody().toString().replaceAll("\\n", "\n*     ");
              _builder.append(_replaceAll_1, " ");
            }
          }
          _builder.newLineIfNotEmpty();
          _builder.append(" ");
          _builder.append("*/");
          _builder.newLine();
          _builder.append("\t\t\t");
          StringConcatenationClient _genMethodSequenceDeclaration = PatchedSerializerFragment2.this.genMethodSequenceDeclaration(c);
          _builder.append(_genMethodSequenceDeclaration, "\t\t\t");
          _builder.append(" {");
          _builder.newLineIfNotEmpty();
          {
            if ((states != null)) {
              _builder.append("if (errorAcceptor != null) {");
              _builder.newLine();
              {
                for(final ISemanticSequencerNfaProvider.ISemState s : states) {
                  _builder.append("    ");
                  _builder.append("if (transientValues.isValueTransient(");
                  _builder.append(cast, "    ");
                  _builder.append("semanticObject, ");
                  EPackage _ePackage = s.getFeature().getEContainingClass().getEPackage();
                  _builder.append(_ePackage, "    ");
                  _builder.append(".");
                  String _featureLiteral = GenModelUtil2.getFeatureLiteral(s.getFeature(), rs);
                  _builder.append(_featureLiteral, "    ");
                  _builder.append(") == ");
                  _builder.append(ITransientValueService.ValueTransient.class, "    ");
                  _builder.append(".YES)");
                  _builder.newLineIfNotEmpty();
                  _builder.append("    ");
                  _builder.append("    ");
                  _builder.append("errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(");
                  _builder.append(cast, "        ");
                  _builder.append("semanticObject, ");
                  EPackage _ePackage_1 = s.getFeature().getEContainingClass().getEPackage();
                  _builder.append(_ePackage_1, "        ");
                  _builder.append(".");
                  String _featureLiteral_1 = GenModelUtil2.getFeatureLiteral(s.getFeature(), rs);
                  _builder.append(_featureLiteral_1, "        ");
                  _builder.append("));");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("}");
              _builder.newLine();
              _builder.append(SequenceFeeder.class);
              _builder.append(" feeder = createSequencerFeeder(context, ");
              _builder.append(cast);
              _builder.append("semanticObject);");
              _builder.newLineIfNotEmpty();
              {
                for(final ISemanticSequencerNfaProvider.ISemState f : states) {
                  _builder.append("feeder.accept(grammarAccess.");
                  String _gaAccessor = PatchedSerializerFragment2.this._grammarAccessExtensions.gaAccessor(f.getAssignedGrammarElement());
                  _builder.append(_gaAccessor);
                  _builder.append(", semanticObject.");
                  StringConcatenationClient _unresolvingGetAccessor = PatchedSerializerFragment2.this.getUnresolvingGetAccessor(f.getFeature(), rs);
                  _builder.append(_unresolvingGetAccessor);
                  _builder.append(");");
                  _builder.newLineIfNotEmpty();
                }
              }
              _builder.append("feeder.finish();");
              _builder.newLine();
            } else {
              _builder.append("genericSequencer.createSequence(context, ");
              _builder.append(cast);
              _builder.append("semanticObject);");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("}");
          _builder.newLine();
          _builder.newLine();
          {
            boolean _isGenerateSupportForDeprecatedContextEObject = PatchedSerializerFragment2.this.isGenerateSupportForDeprecatedContextEObject();
            if (_isGenerateSupportForDeprecatedContextEObject) {
              _builder.append("@Deprecated");
              _builder.newLine();
              _builder.append("protected void sequence_");
              String _simpleName = c.getSimpleName();
              _builder.append(_simpleName);
              _builder.append("(");
              _builder.append(EObject.class);
              _builder.append(" context, ");
              EClass _type = c.getType();
              _builder.append(_type);
              _builder.append(" semanticObject) {");
              _builder.newLineIfNotEmpty();
              _builder.append("    ");
              _builder.append("sequence_");
              String _simpleName_1 = c.getSimpleName();
              _builder.append(_simpleName_1, "    ");
              _builder.append("(createContext(context, semanticObject), semanticObject);");
              _builder.newLineIfNotEmpty();
              _builder.append("}");
              _builder.newLine();
            }
          }
        }
      };
      _xblockexpression = _client_2;
    }
    return _xblockexpression;
  }
  
  private StringConcatenationClient genMethodSequenceDeclaration(final IGrammarConstraintProvider.IConstraint c) {
    StringConcatenationClient _xifexpression = null;
    boolean _isMapEntry = this.isMapEntry(c.getType());
    if (_isMapEntry) {
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("protected <T extends ");
          EClass _type = c.getType();
          _builder.append(_type);
          _builder.append(" & EObject> void sequence_");
          String _simpleName = c.getSimpleName();
          _builder.append(_simpleName);
          _builder.append("(");
          _builder.append(ISerializationContext.class);
          _builder.append(" context, T semanticObject)");
        }
      };
      _xifexpression = _client;
    } else {
      StringConcatenationClient _client_1 = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("protected void sequence_");
          String _simpleName = c.getSimpleName();
          _builder.append(_simpleName);
          _builder.append("(");
          _builder.append(ISerializationContext.class);
          _builder.append(" context, ");
          EClass _type = c.getType();
          _builder.append(_type);
          _builder.append(" semanticObject)");
        }
      };
      _xifexpression = _client_1;
    }
    return _xifexpression;
  }
  
  private boolean isMapEntry(final EClass type) {
    String _instanceClassName = type.getInstanceClassName();
    String _name = Map.Entry.class.getName();
    return Objects.equal(_instanceClassName, _name);
  }
  
  private StringConcatenationClient getUnresolvingGetAccessor(final EStructuralFeature feature, final ResourceSet resourceSet) {
    final GenFeature genFeature = GenModelUtil2.getGenFeature(feature, resourceSet);
    boolean _isResolveProxies = genFeature.isResolveProxies();
    if (_isResolveProxies) {
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("eGet(");
          EPackage _ecorePackage = genFeature.getGenPackage().getEcorePackage();
          _builder.append(_ecorePackage);
          _builder.append(".");
          String _featureLiteral = GenModelUtil2.getFeatureLiteral(genFeature, resourceSet);
          _builder.append(_featureLiteral);
          _builder.append(", false)");
        }
      };
      return _client;
    } else {
      StringConcatenationClient _client_1 = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          String _getAccessor = GenModelUtil2.getGetAccessor(genFeature, resourceSet);
          _builder.append(_getAccessor);
          _builder.append("()");
        }
      };
      return _client_1;
    }
  }
  
  private static final Logger LOG = Logger.getLogger(PatchedSerializerFragment2.class);
}
