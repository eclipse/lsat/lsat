////
  // Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
  //
  // This program and the accompanying materials are made
  // available under the terms of the Eclipse Public License 2.0
  // which is available at https://www.eclipse.org/legal/epl-2.0/
  //
  // SPDX-License-Identifier: EPL-2.0
////

include::_initCommon.adoc[]

== Graphical Viewers

[[add-viewpoints-selection]]
=== Add Viewpoints Selection

Right click on the project folder, select Viewpoints Selection.

image::{imgsdir}/image29.png[]

The following viewpoints are available:

[horizontal]
Machine:: shows the resources with their peripherals and illustrates symbolic position graphs for movement-based peripherals.
Activities:: shows activity diagrams based on activity specifications.
Physical settings:: shows settings for motion profiles and physical locations.

Check the relevant viewpoints and click OK.

image::{imgsdir}/viewpoints.png[]

=== Generate Graphs

==== Peripheral types in machine
*Target element*: a machine

Right click on a target element, select New Representation and choose
the corresponding Peripheral Types for the target element.

image::{imgsdir}/peripheraltypes_create.png[]

The peripheral types graph for the target element includes all action and movement based peripheral types for the target element. 

TIP: This viewer can also be used to edit the peripheral types by using the palette.

image::{imgsdir}/peripheraltypes_editor.png[]
 
==== Resources and peripherals in machine
*Target element*: a machine

Right click on a target element, select New Representation and choose
the corresponding Machine for the target element.

image::{imgsdir}/machine_create.png[]

The graph for the target element includes all resources with their action-based and movement-based peripherals for the target element. 

TIP: This viewer can also be used to edit the resources and their peripherals by using the palette.

image::{imgsdir}/machine_editor.png[]

==== Axis position graph for peripheral
*Target element*: a movable peripheral defined in a resource

Right click on a target element, select New Representation and choose
the corresponding Axis positions for the target element.

image::{imgsdir}/axis-pos-menu.png[]

The axis position graph for the target element includes specific information 
about axes and corresponding positions.

image::{imgsdir}/Axis-positions-for-Robot.png[]

The axes in a axis position graph are shown as compartments, and the positions associated to an axis are shown under the axis. For example, in the figure above, the position _Above_ is associated to the axis _Y_.

===== Adding/removing axes
To add a new axis to a peripheral, select the empty canvas and select the desired axis from the _Available axes_ list in the properties view. Press _Add Axis_ on the right side.

image::{imgsdir}/axis-pos-axes.png[]

To remove an axis from a peripheral, select it and press Delete.

NOTE: Deleting an axis will also delete all corresponding positions.

===== Adding/deleting positions
The positions can be associated to an axis by selecting the _Position_ tool from the _Axis Positions Editor Tools_ palette on the right side, and selecting the axis compartment.

By default, name of the newly created position is _new_. The name of a position can be changed by selecting it, typing the name directly and pressing Enter. Alternatively, the name can be changed using the properties editor, as shown in the figure below.

image::{imgsdir}/axis-pos-name.png[]

To delete a position, select it and press Delete.

NOTE: If a position is specified in both axis position graph and Settings <<Create Settings Specification>>, deleting that position from the axis position graph will also delete it from the Settings specifications.

===== Validation

An axis must not be added more than once. To validate this property, right click on the canvas and click _Validate diagram_. If any axis is added more than once, a red error sign will appear on those axes, as shown in the following figure.

image::{imgsdir}/axis-pos-error.png[]

==== Symbolic position graph for peripheral
*Target element*: a movable peripheral defined in a resource

Right click on a target element, select New Representation and choose
the corresponding Symbolic positions for the target element.

image::{imgsdir}/symb-pos-menu.png[]

The symbolic position graph for the target element includes specific information
about profiles and settling on the paths.

CAUTION: Layout of existing symbolic position graphs will be reset to default layout. As manual layout of these diagrams may require extra time and labor, it may not be wise to upgrade to the latest release of {lsat} for existing symbolic position specifications.

image::{imgsdir}/Symb-positions-for-Robot.png[]


The symbolic positions are shown as compartments. The axis positions corresponding to a symbolic position are shown inside the respective compartment. 

The full mesh paths are shown as gray vertical rectangles. The single-headed arrows between symbolic positions represent unidirectional paths, whereas double-headed arrows represent bidirectional paths. If a symbolic position is a part of a full mesh path, then there is also an arrow between the symbolic position and the full mesh path.

The labels in the center of paths represent motion profiles. Whereas, the labels at the end of paths represent settling.

===== Adding/deleting motion profiles
To add a new motion profile to a peripheral, click on the empty canvas. As a result, properties editor will appear at the bottom. Enter the name of the motion profile without a white space in the field _Add a motion profile_, as shown in the following figure, and hit Enter.

image::{imgsdir}/symb-pos-motion-profile.png[]

The available profiles are visible in the _Available Profiles_ field. To delete a profile, select it and press _Delete Profile_ on the right side.

===== Adding/deleting symbolic positions
To create a new symbolic position, select the respective tool from the _Symbolic Positions Editor Tools_ palette on the right side, and then click on the canvas. The name of the symbolic position can be typed directly while selecting the newly created symbolic position. Alternatively, the name can also be typed in the properties editor at the bottom, as shown in the following figure.

image::{imgsdir}/symb-pos-name.png[]

To delete a symbolic position, select it and press Delete.

===== Adding/deleting axis positions
The axis positions can be added to a symbolic position compartment by selecting the respective tool and selecting the symbolic position compartment. The name of the axis position can be typed directly in the format _Axis_._Position_ (same as textual representation). Alternatively, the name can also be typed in the properties editor at the bottom, as shown in the following figure. Here, _Key_ represents an axis and _Value_ represents a position. To delete a axis position, select it and press Delete.

image::{imgsdir}/symb-pos-axis-pos.png[]

NOTE: The typed axis position name cannot be entered if either of the typed axis or position does not exist.

===== Adding/deleting paths
Uni/bidirectional paths between two symbolic positions can be added by selecting the respective tool and clicking the symbolic positions. To add a symbolic position to a full mesh path, select the _Full Mesh Target_ tool, and then click the full mesh path and the symbolic position. To delete a path, select it and press Delete.

A settling to a unidirectional path can be added in the properties editor by selecting the path, as shown in the figure below.

image::{imgsdir}/symb-pos-path-settling.png[]

In the _Assign a settling_ field, a settling can be selected for a path. The settlings currently assigned to a path are visible in the _Current settlings_ field. To remove a settling from a path, first select the path and then select the settling in the _Current settlings_ field and press _Delete Settling_ on the right side.

In bidirectional paths, we can add two settlings, i.e., one settling for each connected symbolic position.  A settling to a bidirectional path can be added in the properties editor by selecting the path, and then assigning a settling at each connected symbolic position, as shown in the figure below.

image::{imgsdir}/symb-pos-bi-path-settling.png[]

The settlings currently assigned to a path at a symbolic position are visible in the _Current settlings_ field. To remove a settling from a path, first select the path and then select the settling in the _Current settlings_ field and press _Delete Settling_ on the right side.

<<<
===== Assigning existing motion profiles to a path
To add a motion profile to a path, choose one of the options below:

Option I::
* Select the path by left clicking on it.
* Select the _General_ tab.
* Scroll down to the _Motion Profiles_ section.
* Select your the profile you want to add from the _Available profiles_ list.
* Press Add to path.

image::{imgsdir}/symb-pos-path-motion-profile.png[]

Option II::
* Select the motion profile from the diagram by left clicking on it.
* Press _F2_ to enable direct editing.
* Add a comma after the existing profile and write the name of the profile you want to add, e.g _normal, slow_ .Please note _slow_ should have been initialized earlier.
* Save your diagram. The changes are then saved to the model.

image::{imgsdir}/symb-pos-path-motion-profile2.png[]


===== Hiding axis positions/paths

In order to visualize a diagram clearly, it is possible to hide axis positions or paths or both from a diagram. To hide axis positions, click on the canvas, and then click on the _Filter_ icon on the top tool bar, and click _Axis Positions_, as shown in the figure below.

image::{imgsdir}/symb-pos-filter.png[]

Paths from a diagram can also hidden in the same way.

===== Layout
A symbolic position graph can be layout by right clicking on the canvas and selecting Show Layout View. 

image::{imgsdir}/symb-pos-layout-selection.png[]

As a result, a new tab named _Layout_ will appear at the bottom. Click on the empty canvas, and then in the _Layout_ tab, a new Property _Layout Algorithm_ with a Value _Activity Diagram_ will appear. 

image::{imgsdir}/symb-pos-layout-selection-algo.png[]

Select _Activity Diagram_, and double click it or use the button on the right side. 

//image::{imgsdir}/layout-button.png[]. 

A pop-up will appear showing all pre-defined layout algorithms. In our experience, the _ELK Layered_ algorithm is most suitable for symbolic position graphs. However, please feel free to explore other algorithms also. 

image::{imgsdir}/symb-pos-layout-selection-ELK.png[]

Select an algorithm and press OK. Afterwards, apply the automatic layout by right clicking on the canvas and selecting Layout Selection from the context menu, or use the button from the toolbar  image:{imgsdir}/activity_layout_btn.png[].

===== Validation

Each symbolic position must have a unique name. To validate this property, right click on the canvas and click _Validate diagram_. If any symbolic positions do not have unique names, a red error sign will appear on those symbolic positions, as shown in the following figure.

image::{imgsdir}/symb-pos-error.png[]

To quickly fix this error, a quick fix is available. Go to the _Problems_ tab on bottom and right click on the error message and select _Quick Fix_, as shown in the figure below.

image::{imgsdir}/symb-pos-error-quick-fix.png[]
 
Then select the _Change name_ and press Finish.

image::{imgsdir}/symb-pos-error-quick-fix2.png[]

As a result __new_ will be added at the end of the name of the symbolic position.

image::{imgsdir}/Symb-positions-for-Robot-quick.png[]

==== Motion profile settings table for peripheral
*Target element*: a movable peripheral defined in a resource.

Right click on a target element, select New Representation and choose
the corresponding motion profile for the target element.

TIP: If the representation is not listed in the context menu, most likely the _physical settings_ viewpoint needs to be activated for the project, see <<add-viewpoints-selection>>.

Motion profile settings data is from the first setting file in the
project directory.

Column specifies profile names and row represents one axis. 
Each row contains sub rows to specify VAJs for an axis.

image::{imgsdir}/image35.png[]

==== Physical location settings table for peripheral
*Target element*: a movable peripheral defined in a resource.

Right click on a target element, select New Representation and choose
the corresponding physical location settings for the target element.

TIP: If the representation is not listed in the context menu, most likely the _physical settings_ viewpoint needs to be activated for the project, see <<add-viewpoints-selection>>.

The same to motion profile settings table, the physical location
settings data is also from the first setting file in the project
directory.

In the table, each column represents one symbolic position for the
target element. Each row represents its axes and the physical location values
(min, default, and max) are shown on the sub rows.

image::{imgsdir}/image37.png[]

==== Activity diagram
*Target element*: an activity defined in an activity specification

Right click on a target element, select New Representation and choose
the corresponding activity for the target element.

image::{imgsdir}/image39.png[]

TIP: If the representation is not listed in the context menu, most likely the _activities_ viewpoint needs to be activated for the project, see <<add-viewpoints-selection>>.

It shows an activity diagram with an action flow. Green swim-lanes represent
resources and their peripherals, gray blocks represent
either claiming a resource or releasing a resource and brown blocks
represent actions for peripherals. 
If an action is specified to be scheduled As Late As Possible (ALAP), then an icon with an arrow pointing down is shown. image:{imgsdir}/alap.png[]

The generated activity diagram (probably) needs some layout to be done, but an automated default layout is applied.
The initial layout is not optimal yet, therefore it is wise to apply the automatic layout once the Activity diagram is visible, using the tip below. 

TIP: To (re)apply the automatic layout, select _Layout Selection_ from the context menu on the canvas, or use the button from the toolbar image:{imgsdir}/activity_layout_btn.png[]

TIP: This viewer can also be used to edit the activity by using the palette.

image::{imgsdir}/activity_layout_nice.png[]

===== Adding a new action

image::{imgsdir}/add-action-new.png[]

. Select **Action** from the Palette at the right side of the editor.
. Left click somewhere in the green swim-lane of a peripheral. A popup window as the one above will show up.
. Provide a name for the action.
. Use the radio button to select the type of action you want to create. If the action is of type _Move_, then you must also specify if it is point-to-point (stop at target position) or on-the-fly (don't stop at target position) and the speed profile to use.
. Select the scheduling type - As soon as possible (ASAP) or As late as possible (ALAP)
. Click Finish. 
. <<newaction>> illustrates an example. In this case, the new action is A7.

.New action 
[#newaction]
image::{imgsdir}/new-action.png[]

===== Modifying the properties of an existing action

. Select the action you would like to modify from the activity diagram.
. Select the **Properties** tab in Eclipse. It is usually located below the diagram.
. You should now have access to a menu that looks like the image below.

image::{imgsdir}/modify-properties-of-an-action-new.png[]

TIP: You can use the **Properties** view to change the scheduling type of an action from **ASAP** to **ALAP** or vice versa.



=== Location of Generated Graphs

The generated graphs are located inside its target element. By unfolding
the element from the Model Explorer, the generated graphs will appear.

image::{imgsdir}/image41.png[]

All the graphs can also be found under the “representations.aird” file.

image::{imgsdir}/image42.png[]

=== Navigation among graphs

In graphical viewer, if one semantic element can be generated more than
one graph, these graphs can be navigated. Navigation only happens when
the semantic element is shown as a container/block in a source graph.
Since there is no container concept in table, there is no navigation
starting from a table view. Besides, navigations also request the target
graphs have already been generated.

==== Navigate from activity diagram

A peripheral is represented as a write block in an activity diagram.
Some white blocks contain a small graph icon image:{imgsdir}/image43.png[] at right bottom corner. 
It means there are other graphs can be generated for the semantic element 
of this block. 

Right click on the write block, open with a desired graph. Movable
peripherals can produce five types of graphs: axis position graph, symbolic position graph, motion profile settings table, physical location settings table and physical location overview table.

image::{imgsdir}/navigation-activity-diagram.png[]

==== Navigate from axis position graph

It is the same as navigation from activity diagram. Right click the
container which has a small graph icon and open with a desired graph.
From a axis position graph, it is possible to navigate to symbolic position graph, motion profile settings table, physical location settings table and physical location overview table.

image::{imgsdir}/navigation-axis.png[]

==== Navigate from symbolic position graph

Right click the container which has a small graph icon and open with a desired graph.
From a symbolic position graph, it is possible to navigate to axis position graph, motion profile settings table, physical location settings table and physical location overview table.

image::{imgsdir}/navigation-symb.png[]