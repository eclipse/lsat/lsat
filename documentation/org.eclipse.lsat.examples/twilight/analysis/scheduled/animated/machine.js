/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
var ballSize = 40;
var ballTemplate;

function createBall() {
  if (ballTemplate == undefined) {
    ballTemplate = new Path.Circle(new Point(0, 0), ballSize);
    ballTemplate.fillColor = {
      gradient : {
        stops : [ [ 'Chocolate', 0.05 ], [ 'Brown', 0.2 ], [ 'Black', 1 ] ],
        radial : true
      },
      origin : new Point(-20, -20),
      destination : ballTemplate.bounds.rightCenter
    };
    return ballTemplate;
  } else {
    var copy = ballTemplate.clone();
    copy.bringToFront();
    return copy;
  }
}

function rail(anchor1, anchor2) {
  var rail = new Path.Rectangle({
    topLeft : new Point(anchor1.X - 75, anchor1.Y - 42),
    bottomRight : new Point(anchor2.X + 75, anchor1.Y - 32),
    fillColor : 'SteelBlue',
    // Set the shadow color of the circle to RGB black:
    shadowColor : new Color(0, 0, 0),
    // Set the shadow blur radius to 12:
    shadowBlur : 12,
    // Offset the shadow by { x: 5, y: 5 }
    shadowOffset : new Point(5, 5)
  });
}

function container(anchor) {
  var container = new CompoundPath({
    children : [ new Path.Rectangle(new Point(anchor.X - 75, anchor.Y), new Size(150, 200)),
        new Path.Rectangle(new Point(anchor.X - 65, anchor.Y), new Size(130, 80)) ],
    fillRule : 'evenodd',
    fillColor : 'SteelBlue',
    // Set the shadow color of the circle to RGB black:
    shadowColor : new Color(0, 0, 0),
    // Set the shadow blur radius to 12:
    shadowBlur : 12,
    // Offset the shadow by { x: 5, y: 5 }
    shadowOffset : new Point(5, 5)
  });
  var ball = createBall();
  ball.position = new Point(anchor.X - 15, anchor.Y + ballSize)
  ball = createBall();
  ball.position = new Point(anchor.X + 24, anchor.Y + ballSize)
  ball = createBall();
  ball.position = new Point(anchor.X, anchor.Y + ballSize)
}

var Conditioner_CD = {
  plate : null,
  ducts : null,
  init : function(anchor) {
    var table = new CompoundPath({
      children : [ new Path.Rectangle(new Point(anchor.X - 75, anchor.Y + (ballSize * 2)), new Size(150, 30)),
                   new Path.Rectangle(new Point(anchor.X - 30, anchor.Y + (ballSize * 2) + 30), new Size(60, 90))],
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 12:
      shadowBlur : 12,
      // Offset the shadow by { x: 5, y: 5 }
      shadowOffset : new Point(5, 5)
    });
    this.plate = new Path.Rectangle(new Point(anchor.X - 75, anchor.Y + (ballSize * 2)), new Size(150, 30));
    this.plate.fillColor = new Color(0, 1, 1);
    this.ducts = new Path.Circle(new Point(500, 500), 9);
    this.ducts.fillColor = new Color(0, 1, 1);
    var ductSymbol = new Symbol(this.ducts);
    for (var i = 0; i < 6; i++) {
      var x = (anchor.X - 61) + i * 24;
      var y = anchor.Y + (ballSize * 2) + 15;
      ductSymbol.place(new Point(x, y));
    }
  },
  condition : function(percentageComplete, heater) {
    if (heater)
      this.ducts.fillColor = 'red';
    else
      this.ducts.fillColor = new Color(0, 1, 1);
    this.plate.fillColor = new Color(percentageComplete, 1 - percentageComplete, 1 - percentageComplete);
  }
}

var Conditioner_CL = {
  ball : null,
  init : function(anchor) {
    this.ball = createBall();
    this.ball.visible = false;
    this.ball.position = new Point(anchor.X, anchor.Y + ballSize);
  },
  clamp : function(percentageComplete) {
    this.ball.visible = true;
  },
  unclamp : function(percentageComplete) {
    this.ball.visible = false;
  }
}

var Drill_CL = {
  ball : null,
  table : null,
  init : function(anchor) {
    this.table = new CompoundPath({
      children : [ new Path.Rectangle(new Point(anchor.X - 75, anchor.Y + (ballSize * 2)), new Size(150, 120)),
                   new Path.Rectangle(new Point(anchor.X - 75, anchor.Y + (ballSize * 2) + 60), new Size(20, 60)),
                   new Path.Rectangle(new Point(anchor.X + 55, anchor.Y + (ballSize * 2) + 60), new Size(20, 60)),
                   new Path.Rectangle(new Point(anchor.X - 8, anchor.Y + (ballSize * 2)), new Size(16, 120))],
      fillRule : 'evenodd',
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 12:
      shadowBlur : 12,
      // Offset the shadow by { x: 5, y: 5 }
      shadowOffset : new Point(5, 5)
    });

    this.ball = createBall();
    this.ball.visible = false;
    this.ball.position = new Point(anchor.X, anchor.Y + ballSize);
  },
  clamp : function(percentageComplete) {
    if (percentageComplete == 1) {
      this.ball.visible = true;
    }
  },
  unclamp : function(percentageComplete) {
    if (percentageComplete == 1) {
      this.ball.visible = false;
    }
  }
}

var Drill_DL = {
  on : function(percentageComplete) {
    if (percentageComplete < 1) {
      Drill_ZR.drill.fillColor = 'DimGray';
    } else {
      Drill_ZR.drill.fillColor = 'Black';
    }
  },
  off : function(percentageComplete) {
    if (percentageComplete < 1) {
      Drill_ZR.drill.fillColor = 'DimGray';
    } else {
      Drill_ZR.drill.fillColor = {
          gradient : {
            stops : [ 'DimGray', 'Black', 'DimGray', 'Black', 'DimGray', 'Black', 'DimGray', 'Black' ]
          },
          origin : new Point(Drill_ZR.posX - 5, Drill_ZR.posY - 58),
          destination : new Point(Drill_ZR.posX + 5, Drill_ZR.posY + 58)
        };
    }
  }
}

var Drill_ZR = {
  drill : null,
  posX : null,
  posY : null,
  init : function(position, anchor) {
    this.posX = anchor.X;
    this.drill = new CompoundPath({
      children : [ new Path.Rectangle(new Point(0, 8.66), new Size(10, 106)), 
                   new Path.RegularPolygon(new Point(5, 5.774), 3, 5.774) ],
      fillColor : {
        gradient : {
          stops : [ 'DimGray', 'Black', 'DimGray', 'Black', 'DimGray', 'Black', 'DimGray', 'Black' ]
        },
        origin : new Point(0,0),
        destination : new Point(10, 116)
      }
    });
    this.move(position, position, 1);
  },
  move : function(from, to, percentageComplete) {
    var newZ = from.Z + (to.Z - from.Z) * percentageComplete;
    this.posY = newZ + 62;
    this.drill.position = new Point(this.posX, this.posY);
  }
}

var LoadRobot_CL = {
  clamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      LoadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      LoadRobot_XY.clamp.fillColor = 'Crimson';
      LoadRobot_XY.ball.visible = true;
    }
  },
  unclamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      LoadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      LoadRobot_XY.clamp.fillColor = 'SteelBlue';
      LoadRobot_XY.ball.visible = false;
    }
  }
}

var LoadRobot_XY = {
  ball : null,
  clamp : null,
  shaft : null,
  init : function(position) {
    this.ball = createBall();
    this.ball.visible = false;
    //new Point(0, 0), new Size(ballSize * 2, 40));
    //this.clamp.fillColor = 'SteelBlue';
    this.shaft = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(10,2),
      fillColor : 'LightSteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 12:
      shadowBlur : 12,
      // Offset the shadow by { x: 5, y: 5 }
      shadowOffset : new Point(5, 5)
    });
    //new Path.Rectangle(new Point(0, 0), new Size(10, 10));
    //this.shaft.fillColor = 'LightSteelBlue';
    this.clamp = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(ballSize * 2, 40),
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 12:
      shadowBlur : 12,
      // Offset the shadow by { x: 5, y: 5 }
      shadowOffset : new Point(5, 5)
    });
    this.move(position, position, 1);
  },
  move : function(from, to, percentageComplete) {
    var newX = from.X + (to.X - from.X) * percentageComplete;
    var newY = from.Y + (to.Y - from.Y) * percentageComplete;
    this.shaft.bounds.height = newY - LoadRobot_XY_ABOVE_Y + 2;
    this.shaft.position = new Point(newX, LoadRobot_XY_ABOVE_Y - 31 + (newY - LoadRobot_XY_ABOVE_Y) / 2);
    this.clamp.position = new Point(newX, newY - 10)
    this.ball.position = new Point(newX, newY + ballSize);
  }
}

var UnloadRobot_CL = {
  clamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      UnloadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      UnloadRobot_XY.clamp.fillColor = 'Crimson';
      UnloadRobot_XY.ball.visible = true;
    }
  },
  unclamp : function(percentageComplete) {
    if (percentageComplete < 1) {
      UnloadRobot_XY.clamp.fillColor = 'LightSteelBlue';
    } else {
      UnloadRobot_XY.clamp.fillColor = 'SteelBlue';
      UnloadRobot_XY.ball.visible = false;
    }
  }
}

var UnloadRobot_XY = {
  ball : null,
  clamp : null,
  shaft : null,
  init : function(position) {
    this.ball = createBall();
    this.ball.visible = false;
    //new Point(0, 0), new Size(ballSize * 2, 40));
    //this.clamp.fillColor = 'SteelBlue';
    this.shaft = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(10,2),
      fillColor : 'LightSteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 12:
      shadowBlur : 12,
      // Offset the shadow by { x: 5, y: 5 }
      shadowOffset : new Point(5, 5)
    });
    //new Path.Rectangle(new Point(0, 0), new Size(10, 10));
    //this.shaft.fillColor = 'LightSteelBlue';
    this.clamp = new Path.Rectangle({
      topLeft : new Point(0, 0),
      bottomRight : new Point(ballSize * 2, 40),
      fillColor : 'SteelBlue',
      // Set the shadow color of the circle to RGB black:
      shadowColor : new Color(0, 0, 0),
      // Set the shadow blur radius to 12:
      shadowBlur : 12,
      // Offset the shadow by { x: 5, y: 5 }
      shadowOffset : new Point(5, 5)
    });
    this.move(position, position, 1);
  },
  move : function(from, to, percentageComplete) {
    var newX = from.X + (to.X - from.X) * percentageComplete;
    var newY = from.Y + (to.Y - from.Y) * percentageComplete;
    this.shaft.bounds.height = newY - LoadRobot_XY_ABOVE_Y + 2;
    this.shaft.position = new Point(newX, LoadRobot_XY_ABOVE_Y - 31 + (newY - LoadRobot_XY_ABOVE_Y) / 2);
    this.clamp.position = new Point(newX, newY - 10)
    this.ball.position = new Point(newX, newY + ballSize);
  }
}

// Auto generated, do not modify!

function unclamp(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined)
    return; // Outside time scope
  peripheral.unclamp(percentageComplete);
}

// Manually adapted as conditioner needs to cool down ;)
function condition(peripheral, timeFrom, timeTo, time) {
  var deltaTime = timeTo - timeFrom;
  if (time < timeFrom)
    return; // Outside time scope
  if (time > (timeTo + deltaTime))
    return; // Outside time scope

  var percentageComplete = (time - timeFrom) / deltaTime;

  if (percentageComplete < 1) {
    peripheral.condition(percentageComplete, true);
  } else {
    // It takes 40% of the time to cool down
    percentageComplete = Math.max((1.4 - percentageComplete) * 2.5, 0);
    peripheral.condition(percentageComplete, false);
  }
}

function clamp(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined)
    return; // Outside time scope
  peripheral.clamp(percentageComplete);
}

function off(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined)
    return; // Outside time scope
  peripheral.off(percentageComplete);
}

function on(peripheral, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined)
    return; // Outside time scope
  peripheral.on(percentageComplete);
}

function move(peripheral, from, to, timeFrom, timeTo, time) {
  var percentageComplete = toPercentageComplete(timeFrom, timeTo, time);
  if (percentageComplete == undefined)
    return; // Outside time scope
  peripheral.move(from, to, percentageComplete);
}

function toPercentageComplete(timeFrom, timeTo, time) {
  if (time < timeFrom)
    return;
  if (time > timeTo + 0.1)
    return;

  var deltaTime = timeTo - timeFrom;
  var timeIn = time - timeFrom;
  var ratio = timeIn / deltaTime;
  return Math.min(ratio, 1);
}