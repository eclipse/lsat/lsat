/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
import "twilight.machine"

LoadRobot.CL {
  Timings {
    clamp = Pert(min=0.1, max=1, mode=0.250, gamma=10)
    unclamp = 0.200
  }
}

LoadRobot.XY {
  Axis X {
    Profiles {
      normal (V = 1, A = 8, J = 20)
    }
    Positions {
      IN = 1
      COND = 2
      DRILL = 3
    }
  }
  Axis Y {
    Profiles {
      normal (V = 2, A = 15, J = 35)
    }
    Positions {
      ABOVE = 0
      OUT_DRILL = 0.8
      AT = 2
    }
  }
}

UnloadRobot.CL {
  Timings {
    clamp = 0.250
    unclamp = 0.200
  }
}

UnloadRobot.XY {
  Axis X {
    Profiles {
      normal (V = 8, A = 8, J = 20)
    }
    Positions {
      COND = 2
      DRILL = 3
      OUT = 4
    }
  }
  Axis Y {
    Profiles {
      normal (V = 15, A = 15, J = 35)
    }
    Positions {
      ABOVE = 0
      OUT_DRILL = 0.8
      AT = 2
    }
  }
}

Conditioner.CL {
  Timings {
    clamp = 0.250
    unclamp = 0.200
  }
}

Conditioner.CD {
  Timings {
    condition = 5.0
  }
}

Drill.CL {
  Timings {
    clamp = 0.250
    unclamp = 0.200
  }
}

Drill.DL{
	Timings {
    on = 0.5
    off = 0.5
	}
}

Drill.ZR {
  Axis Z {
    Profiles {
      normal (V = 0.1, A = 1, J = 5)
      slow (V = 0.1, A = 0.1, J = 1.0)
    }
    Positions {
      UP = 100
      DOWN = 0
    }
  }
}
