/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// TODO: Adapt these locations to match the screen resolution
var width = 1000;
var height = 250; 

var LoadRobot_XY_IN_X = 100;
var LoadRobot_XY_COND_X = LoadRobot_XY_IN_X + width * 1/3;
var LoadRobot_XY_CA_COND_L_X = LoadRobot_XY_COND_X - width / 8;
var LoadRobot_XY_CA_COND_R_X = LoadRobot_XY_COND_X + width / 8;
var LoadRobot_XY_DRILL_X = LoadRobot_XY_IN_X + width * 2/3;
var LoadRobot_XY_CA_DRILL_L_X = LoadRobot_XY_DRILL_X - width / 8;
var LoadRobot_XY_CA_DRILL_R_X = LoadRobot_XY_DRILL_X + width / 8;
var LoadRobot_XY_ABOVE_Y = 50;
var LoadRobot_XY_AT_Y = LoadRobot_XY_ABOVE_Y + height;
var LoadRobot_XY_OUT_DRILL_Y = LoadRobot_XY_AT_Y - 40;

var UnloadRobot_XY_OUT_X = LoadRobot_XY_IN_X + width;// * 3/3;
// Reusing locations of load robot!
//var UnloadRobot_XY_COND_X = 2;
//var UnloadRobot_XY_DRILL_X = 3;
//var UnloadRobot_XY_ABOVE_Y = 0;
//var UnloadRobot_XY_OUT_DRILL_Y = 0.8;
//var UnloadRobot_XY_AT_Y = 2;

var Drill_DR_DOWN_Z = 0;
var Drill_DR_UP_Z = 40;
var DrillTable_CA__INITIAL_POSITION_Theta = 30;
var DrillTable_CA_INDEX_Theta = 120;
var DrillTable_CA_FULL_TURN_Theta = 360;
var DrillTable_XY_AT_INDEX_FINGER_X = 20;
var DrillTable_XY_AT_MIDDLE_FINGER_X = 40;
var DrillTable_XY_AT_THUMB_X = 0;
var DrillTable_XY_AT_INDEX_FINGER_Y = 40;
var DrillTable_XY_AT_MIDDLE_FINGER_Y = 20;
var DrillTable_XY_AT_THUMB_Y = 0;

// Auto generated, do not modify!
var Drill_Thumb_DR_UP = {Z:Drill_DR_UP_Z};
var Drill_Thumb_DR_DOWN = {Z:Drill_DR_DOWN_Z};
var Drill_IndexFinger_DR_UP = {Z:Drill_DR_UP_Z};
var Drill_IndexFinger_DR_DOWN = {Z:Drill_DR_DOWN_Z};
var Drill_MiddleFinger_DR_UP = {Z:Drill_DR_UP_Z};
var Drill_MiddleFinger_DR_DOWN = {Z:Drill_DR_DOWN_Z};
var DrillTable_CA__INITIAL_POSITION = {Theta:DrillTable_CA__INITIAL_POSITION_Theta};
var DrillTable_CA_INDEX = {Theta:DrillTable_CA_INDEX_Theta};
var DrillTable_CA_FULL_TURN = {Theta:DrillTable_CA_FULL_TURN_Theta};
var DrillTable_XY_AT_THUMB = {X:DrillTable_XY_AT_THUMB_X, Y:DrillTable_XY_AT_THUMB_Y};
var DrillTable_XY_AT_INDEX_FINGER = {X:DrillTable_XY_AT_INDEX_FINGER_X, Y:DrillTable_XY_AT_INDEX_FINGER_Y};
var DrillTable_XY_AT_MIDDLE_FINGER = {X:DrillTable_XY_AT_MIDDLE_FINGER_X, Y:DrillTable_XY_AT_MIDDLE_FINGER_Y};
var LoadRobot_XY_ABOVE_IN = {X:LoadRobot_XY_IN_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_ABOVE_COND = {X:LoadRobot_XY_COND_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_ABOVE_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_ABOVE_CA_COND_L = {X:LoadRobot_XY_CA_COND_L_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_ABOVE_CA_COND_R = {X:LoadRobot_XY_CA_COND_R_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_ABOVE_CA_DRILL_L = {X:LoadRobot_XY_CA_DRILL_L_X, Y:LoadRobot_XY_ABOVE_Y};
var LoadRobot_XY_AT_IN = {X:LoadRobot_XY_IN_X, Y:LoadRobot_XY_AT_Y};
var LoadRobot_XY_AT_COND = {X:LoadRobot_XY_COND_X, Y:LoadRobot_XY_AT_Y};
var LoadRobot_XY_AT_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_AT_Y};
var LoadRobot_XY_OUT_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_OUT_DRILL_Y};
var UnloadRobot_XY_ABOVE_OUT = {X:UnloadRobot_XY_OUT_X, Y:LoadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_ABOVE_COND = {X:LoadRobot_XY_COND_X, Y:LoadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_ABOVE_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_ABOVE_CA_COND_R = {X:LoadRobot_XY_CA_COND_R_X, Y:LoadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_ABOVE_CA_DRILL_L = {X:LoadRobot_XY_CA_DRILL_L_X, Y:LoadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_ABOVE_CA_DRILL_R = {X:LoadRobot_XY_CA_DRILL_R_X, Y:LoadRobot_XY_ABOVE_Y};
var UnloadRobot_XY_AT_OUT = {X:UnloadRobot_XY_OUT_X, Y:LoadRobot_XY_AT_Y};
var UnloadRobot_XY_AT_COND = {X:LoadRobot_XY_COND_X, Y:LoadRobot_XY_AT_Y};
var UnloadRobot_XY_AT_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_AT_Y};
var UnloadRobot_XY_OUT_DRILL = {X:LoadRobot_XY_DRILL_X, Y:LoadRobot_XY_OUT_DRILL_Y};
