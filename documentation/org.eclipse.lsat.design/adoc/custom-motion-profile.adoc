////
  // Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
  //
  // This program and the accompanying materials are made
  // available under the terms of the Eclipse Public License 2.0
  // which is available at https://www.eclipse.org/legal/epl-2.0/
  //
  // SPDX-License-Identifier: EPL-2.0
////

include::_initCommon.adoc[]
ifndef::motioncalculatorcustomdir[:motioncalculatorcustomdir: ../org.eclipse.lsat.motioncalculator.custom]

:numbered!:

[appendix]
[[motioncalculator]]
== How to implement your own custom motion profile

IMPORTANT: Before you start, a fully setup LSAT development environment in Eclipse is required.

=== Create a new motion calculator

WARNING: In order to benefit fully from this explanation, you must be at least familiar with Eclipse Plugin Development. 
If this is not the case, please have a look https://www.vogella.com/tutorials/EclipsePlugin/article.html[Eclipse Plugin Development tutorial] and https://www.vogella.com/tutorials/EclipseExtensionPoint/article.html[Eclipse Extension Points and Extensions - Tutorial]

. Create a new Eclipse plugin project. 
This project will implement your custom motion profile. 
Make sure the box for making changes to the UI is not checked.
+
image::{imgsdir}/motion-calc-wizard-new-content.png[]
. Add an extension point of type *org.eclipse.lsat.timing.calculator* in the *plugin.xml* file. 
This extension point is needed to define the capabilities of your new motion calculator. 
When asked if you want to add the project as a dependency, select yes.
+
image::{imgsdir}/motion-calc-wizard-new-extension.png[]
. Add a *MotionProfile* for each supported motion profile in the **Extensions** tab. 
Fill in the *name* of the new profile and the required *key* that will be used in the specification. 
For each motion profile, declare their motion **Parameter**s. A graphical and textual example of the **plugin.xml** are shown below.
+
NOTE: Our custom motion calculator also supports the default _third order point-to-point_ motion profile.
+
.Example with a custom (Linear) motion profile and the default (Third order point-to-point) motion profile.
image::{imgsdir}/extension-points-example.png[]
+ 
TIP: You can also modify the <<plugin.xml, plugin.xml>> file of your motion calculator plugin to define motion profiles and parameters.
+
[[plugin.xml]]
.plugin.xml showing the motion profile and parameter specifications 
[source,xml]
---- 
include::{motioncalculatorcustomdir}/plugin.xml[lines=21..54]
----
<1> Parameters can specified to be optional, but default values cannot be specified for these parameters.
The defaults must be part of the implementation 
. Select the extension point and create a motion calculator *class* by clicking the **class:*** hyperlink. +
Below you can see an example of our <<CustomMotionCalculator.java, CustomMotionCalculator>> class.
+
TIP: Do not name your class MotionCalculator as it can lead to name clashes. 
Use a name like e.g *CustomMotionCalculator*.
+
IMPORTANT: Make sure to implement the *org.eclipse.lsat.timing.calculator.MotionCalculator* interface and optionally extend the *org.eclipse.lsat.timing.calculator.PointToPointMotionCalculator* if you want your motion calculator to support the default _third order point-to-point_ motion profile, like our CustomMotionCalculator.
+
[[CustomMotionCalculator.java]]
.CustomMotionCalculator.java as an example of the motion calculator implementations.
[source,java]
---- 
include::{motioncalculatorcustomdir}/src/org/eclipse/lsat/motioncalculator/custom/CustomMotionCalculator.java[lines=30..]
----
<1> To reuse the built in default motion calculator of {lsat} extend your newly created class with *PointToPointMotionCalculator*. 
<2> Start with the implementation of the *validate* method.
The constraints can be on an individual level or combined. 
<3> If you are reusing the *PointToPointMotionCalculator*, you can reuse the validate from the super class.
<4> Continue with the implementation of *calculateTimes*.
This method is called after validation. It is mandatory and its purpose is to determine the duration of the move.
<5> Finally implement the *getPositionInfo*.
This method is called after validation when a motion plot is created and calculates for each setpoint its position over time. For incremental development this function could first be left empty, till the calculator works.
Additionally, *getPositionInfo* is used to visualizes the profile. For more info, check out **Plotting a move** in Chapter 3 from the user guide. 

.Implementor notes

* The motion calculator always calculates a move for a single peripheral from standstill to standstill.
* A move consists of one or more (in case of a passing move) **MotionSegment**s.
* The default PointToPointMotionCalculator only supports a passing move, when for all **MotionSegment**s the motion profile is the same and its parameter values are the same.
* Each *MotionSegment* contains a *MotionSetPoint* for every setpoint of the peripheral.
* A *MotionSetPoint* indicates where the movement comes from and where it needs to go.

===  Test your new motion calculator

. Start the {lsat} runtime environment from within the development environment by means of the predefined {lsat} launch target.
. Select your custom motion calculator via the menu:Window[Preferences, Logistics, Motion Calculator] menu.
+
image::{imgsdir}/motion-calculator-selection.png[]
. Open the .settings file of an {lsat} specification.
.. Go to the Axis and then Profiles section. 
.. Put your mouse cursor the existing profile e.g normal.
.. Press Ctrl + Space. Select the previously defined motion profile, *Linear* in our code example.
+
image::{imgsdir}/new-profile-visible.png[]
.. Finish by defining the values for the parameters that the custom motion profile requires.
+
image::{imgsdir}/new-profile-defined.png[]
