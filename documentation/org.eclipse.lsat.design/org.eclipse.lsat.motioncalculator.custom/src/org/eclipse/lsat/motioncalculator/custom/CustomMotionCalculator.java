/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.custom;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.lsat.motioncalculator.MotionCalculator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionSetPoint;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.PositionInfo;
import org.eclipse.lsat.motioncalculator.util.MotionSegmentUtilities;
import org.eclipse.lsat.motioncalculator.util.MotionSetPointUtilities;
import org.eclipse.lsat.timing.calculator.PointToPointMotionCalculator;

public class CustomMotionCalculator extends PointToPointMotionCalculator implements MotionCalculator { // <1>
    protected static final String LINEAR_MOTION_PROFILE_KEY = "Linear";

    @Override
    public void validate(List<MotionSegment> segments) throws MotionValidationException { // <2>
        if (MotionSegmentUtilities.isMotionProfile(segments, THIRD_ORDER_POINT_TO_POINT_MOTION_PROFILE_KEY)) {
            super.validate(segments); // <3>
        } else if (MotionSegmentUtilities.isMotionProfile(segments, LINEAR_MOTION_PROFILE_KEY)) {
            // Add your custom motion validations here
            if (segments.size() > 1) {
                throw new MotionValidationException(
                        "Event-on-the-fly (move passing) is not supported by CustomMotionCalculator", segments);
            }
            if (MotionSegmentUtilities.getSettledSetPointIds(segments).size() > 0) {
                throw new MotionValidationException("Settling is not supported by CustomMotionCalculator", segments);
            }
        } else {
            throw new MotionValidationException(
                    "Mixing motion profiles in one move is not supported by CustomMotionCalculator", segments);
        }
    }

    @Override
    public List<Double> calculateTimes(List<MotionSegment> segments) throws MotionException { // <4>
        if (MotionSegmentUtilities.isMotionProfile(segments, THIRD_ORDER_POINT_TO_POINT_MOTION_PROFILE_KEY)) {
            return super.calculateTimes(segments);
        }

        // Validate method already validated that motion profile equals LINEAR_MOTION_PROFILE_KEY in this case
        // Validate method already validated that the segments array contains exactly 1 element
        return Arrays.asList(calculateLinearTime(segments.iterator().next()).doubleValue());
    }

    @Override
    public Collection<PositionInfo> getPositionInfo(List<MotionSegment> segments) throws MotionException { // <5>
        if (MotionSegmentUtilities.isMotionProfile(segments, THIRD_ORDER_POINT_TO_POINT_MOTION_PROFILE_KEY)) {
            return super.getPositionInfo(segments);
        }

        // Validate method already validated that motion profile equals LINEAR_MOTION_PROFILE_KEY in this case
        // Validate method already validated that the segments array contains exactly 1 element
        MotionSegment segment = segments.iterator().next();

        BigDecimal segmentTime = calculateLinearTime(segment);
        List<PositionInfo> result = new ArrayList<>();
        BigDecimal position = BigDecimal.valueOf(0);
        for (MotionSetPoint motionSetPoint: segment.getMotionSetpoints()) {
            BigDecimal setPointTime = calculateLinearTime(motionSetPoint);

            PositionInfo positionInfo = new PositionInfo(motionSetPoint.getId());
            position = MotionSetPointUtilities.getFrom(motionSetPoint, position);
            positionInfo.addTimePosition(0d, position.doubleValue());
            position = MotionSetPointUtilities.getFrom(motionSetPoint, position.add(motionSetPoint.getDistance()));
            positionInfo.addTimePosition(setPointTime.doubleValue(), position.doubleValue());
            // If setPoint arrives early at To, add a third sample
            if (setPointTime.compareTo(segmentTime) < 0) {
                positionInfo.addTimePosition(segmentTime.doubleValue(), position.doubleValue());
            }
            result.add(positionInfo);
        }
        return Collections.unmodifiableCollection(result);
    }

    private BigDecimal calculateLinearTime(MotionSegment segment) {
        BigDecimal maxTime = BigDecimal.ZERO;
        for (MotionSetPoint motionSetPoint: segment.getMotionSetpoints()) {
            maxTime = maxTime.max(calculateLinearTime(motionSetPoint));
        }
        return maxTime;
    }

    private BigDecimal calculateLinearTime(MotionSetPoint motionSetPoint) {
        if (!motionSetPoint.doesMove()) {
            return BigDecimal.ZERO;
        }
        BigDecimal velocity = motionSetPoint.getMotionProfileArgument(VELOCITY_PARAMETER_KEY);
        return motionSetPoint.getDistance().abs().divide(velocity, 6, RoundingMode.HALF_UP);
    }
}
