/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.ui;

import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;

public class SortHandler {
    // @Inject UISynchronize sync;
    @Execute
    public void execute(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {
        if (null == selection || !(selection.getFirstElement() instanceof IFile)) {
            return;
        }
        if (!PlatformUI.getWorkbench().saveAllEditors(true)) {
            return;
        }

        SortJob job = new SortJob((IFile)selection.getFirstElement());
        job.setUser(true);
        job.schedule();
    }
}
