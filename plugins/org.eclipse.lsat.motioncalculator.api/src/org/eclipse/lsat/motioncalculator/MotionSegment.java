/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Specifies a move from position A to B along one or more specified axes. Each axis movement is specified in a
 * {@link MotionSetPoint} <br>
 * <br>
 * If a {@link MotionSegment} array ([])is passed to the {@link MotionCalculator} it means the following:<br>
 * <br>
 * A concatenated set of moves from position to position where in between segments specify passing positions while
 * moving. The first segment starts with velocity 0 and the last segment ends with velocity 0. This is typically called
 * a point-to-point move.
 */
public final class MotionSegment implements Serializable {
    private static final long serialVersionUID = 4929045032161274589L;

    private final List<MotionSetPoint> setPoints = new ArrayList<>();

    private final String id;

    public MotionSegment(String id) {
        this.id = id;
    }

    public void addMotionSetpoint(MotionSetPoint motionSetPoint) {
        if (setPoints.stream().anyMatch(m -> m.getId().equals(motionSetPoint.getId()))) {
            throw new IllegalArgumentException("Motion already added for setpoint: " + motionSetPoint.getId());
        }
        setPoints.add(motionSetPoint);
    }

    public Collection<MotionSetPoint> getMotionSetpoints() {
        return Collections.unmodifiableCollection(setPoints);
    }

    public MotionSetPoint getMotionSetpoint(String id) {
        return setPoints.stream().filter(s -> s.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public String toString() {
        return id;
    }

    public String getId() {
        return id;
    }
}
