/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionSetPoint;

public final class MotionSetPointUtilities {
    private MotionSetPointUtilities() {
        // Empty for utility classes
    }

    public static boolean isMotionProfile(Collection<MotionSetPoint> setPoints, String motionProfileKey) {
        return setPoints.stream().map(MotionSetPoint::getMotionProfile)
                .allMatch(p -> Objects.equals(p.getKey(), motionProfileKey));
    }

    public static Set<MotionProfile> getMotionProfiles(Collection<MotionSetPoint> setPoints) {
        return setPoints.stream().map(MotionSetPoint::getMotionProfile)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    /**
     * @return the first from from list of setPoints or {@link BigDecimal}.ZERO if it is null
     */
    public static BigDecimal getFrom(List<MotionSetPoint> setPoints) {
        if (setPoints.size() > 0) {
            BigDecimal from = setPoints.get(0).getFrom();
            if (from != null) {
                return from;
            }
        }
        return BigDecimal.ZERO;
    }

    /**
     * @return the last to from list of setPoints or from + the distance if it is null.
     */
    public static BigDecimal getTo(List<MotionSetPoint> setPoints) {
        if (setPoints.size() > 0) {
            BigDecimal to = setPoints.get(setPoints.size() - 1).getTo();
            if (to != null) {
                return to;
            }
        }
        return getFrom(setPoints).add(getDistance(setPoints));
    }

    /**
     * @return the MotionSetPoint::from or defaultValue if from is null
     */
    public static BigDecimal getFrom(MotionSetPoint setPoint, BigDecimal defaultValue) {
        BigDecimal from = setPoint.getFrom();
        if (from != null) {
            return from;
        }
        return defaultValue;
    }

    /**
     * @return null safe to.
     */
    public static BigDecimal getTo(MotionSetPoint setPoint, BigDecimal defaultValue) {
        BigDecimal to = setPoint.getTo();
        if (to != null) {
            return to;
        }
        return defaultValue;
    }

    /**
     * @return the sum of all distances
     */
    public static BigDecimal getDistance(List<MotionSetPoint> setPoints) {
        BigDecimal distance = setPoints.stream().map(MotionSetPoint::getDistance).reduce((a, b) -> a.add(b)).get();
        final double sAbs = distance.abs().doubleValue();
        // TODO Remove check below if confident
        final MotionSetPoint first = setPoints.get(0);
        final MotionSetPoint last = setPoints.get(setPoints.size() - 1);
        if (first.getFrom() != null && last.getTo() != null) {
            final double sAbsOld = last.getTo().subtract(first.getFrom()).abs().doubleValue();
            if (sAbsOld != sAbs) {
                throw new RuntimeException("New Calculation method differs from old method");
            }
        }
        return distance;
    }
}
