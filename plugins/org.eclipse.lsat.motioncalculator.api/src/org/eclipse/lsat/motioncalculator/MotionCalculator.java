/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import java.util.Collection;
import java.util.List;

public interface MotionCalculator {
    /**
     * Validates if this {@link MotionCalculator} is able to calculate the motion as specified by <tt>segments</tt>.
     *
     * @param segments the specified motion.
     * @throws MotionException If this motion calculator is not able to calculate this motion
     */
    void validate(List<MotionSegment> segments) throws MotionValidationException;

    /**
     * Calculate the motion times for the an array of concatenated segments.<br>
     * <b>IMPORTANT:</b> The time in the array is the end-time of the motion segment measured from the start of the
     * concatenated move.
     *
     * @param segments
     * @return A list of {@link Double}s representing time in seconds where the index of the array corresponds with the
     *     index of the motion segment in the provided segment list.
     * @throws MotionException
     */
    List<Double> calculateTimes(List<MotionSegment> segments) throws MotionException;

    /**
     * Calculates the position information for all set-points in an array of concatenated segments.
     *
     * @param segments
     * @return The PositionInfo per set point.
     * @throws MotionException
     */
    Collection<PositionInfo> getPositionInfo(List<MotionSegment> segments) throws MotionException;
}
