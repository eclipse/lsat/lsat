/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A set of parameters for a {@link MotionCalculator} implementation to calculate times or position info. A motion
 * profile can be supplied via a {@link MotionProfileProvider}<br>
 * <br>
 * <B>Example</B>: ThirdOrderProfile with parameters Velocity, Acceleration, Jerk
 */
public final class MotionProfile implements Serializable {
    private static final long serialVersionUID = 2661785996931068260L;

    private final String key;

    private final String name;

    private boolean defaultProfile;

    private final URL url;

    private final List<MotionProfileParameter> parameters = new ArrayList<MotionProfileParameter>();

    public MotionProfile(String key, String name, URL url, Collection<MotionProfileParameter> parameters) {
        this.key = key;
        this.name = name;
        this.url = url;
        List<String> parameterKeys = parameters.stream().map(MotionProfileParameter::getKey)
                .collect(Collectors.toList());
        Set<String> parameterKeysSet = new HashSet<>(parameterKeys);
        if (parameterKeys.size() != parameterKeysSet.size()) {
            // Only report the duplicate values
            parameterKeys.removeAll(parameterKeysSet);
            throw new IllegalArgumentException("Parameter keys should be unique: " + parameterKeys);
        }

        for (MotionProfileParameter parameter: parameters) {
            this.parameters.add(parameter);
        }
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public URL getUrl() {
        return url;
    }

    /**
     * @return Whether this is the profile that does not have to be specified in a setting file
     */
    public boolean isDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(boolean defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public Collection<MotionProfileParameter> getParameters() {
        return Collections.unmodifiableCollection(parameters);
    }

    public MotionProfileParameter getParameter(String key) {
        return parameters.stream().filter(p -> p.getKey().equals(key)).findFirst().orElse(null);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((parameters == null) ? 0 : parameters.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MotionProfile other = (MotionProfile)obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (parameters == null) {
            if (other.parameters != null) {
                return false;
            }
        } else if (!parameters.equals(other.parameters)) {
            return false;
        }
        if (url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        return true;
    }
}
