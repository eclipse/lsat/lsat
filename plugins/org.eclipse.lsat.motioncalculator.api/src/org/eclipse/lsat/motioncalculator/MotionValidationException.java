/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MotionValidationException extends MotionException {
    private static final long serialVersionUID = -7727043825271883048L;

    private final List<MotionSegment> segments;

    public MotionValidationException(String message, List<MotionSegment> segments) {
        super(message);
        this.segments = segments;
    }

    public MotionValidationException(String message, MotionSegment... segments) {
        super(message);
        this.segments = Arrays.asList(segments);
    }

    public MotionValidationException(String message, Throwable cause, MotionSegment... segments) {
        super(message, cause);
        this.segments = Arrays.asList(segments);
    }

    public Collection<MotionSegment> getSegments() {
        return segments;
    }
}
