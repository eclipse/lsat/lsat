/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator;

import java.io.Serializable;
import java.util.Map;

/**
 * Class for a TimePosition Pair that can be used as MapEntry. Time is the key and used for equals and hashCode.
 *
 */
public class TimePosition implements Map.Entry<Double, Double>, Serializable {
    private static final long serialVersionUID = -3138486696941343985L;

    private double time;

    private double position;

    public TimePosition(double time, double position) {
        this.time = time;
        this.position = position;
    }

    public double getTime() {
        return time;
    }

    public double getPosition() {
        return position;
    }

    @Override
    public Double getKey() {
        return time;
    }

    @Override
    public Double getValue() {
        return position;
    }

    @Override
    public Double setValue(Double value) {
        double old = position;
        position = value;
        return old;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp = Double.doubleToLongBits(time);
        result = prime * result + (int)(temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TimePosition other = (TimePosition)obj;
        if (Double.doubleToLongBits(time) != Double.doubleToLongBits(other.time))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "{time=" + time + ", position=" + position + "}";
    }
}
