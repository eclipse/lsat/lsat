/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// automatically generated by Xtext
grammar org.eclipse.lsat.setting.teditor.Setting with org.eclipse.xtext.common.Terminals

import "platform:/resource/org.eclipse.lsat.setting.dsl/model/setting.ecore" 
import "platform:/resource/org.eclipse.lsat.machine.dsl/model/machine.ecore" as machine
import "platform:/resource/org.eclipse.lsat.setting.dsl/model/timing.ecore" as timing
import "platform:/resource/org.eclipse.lsat.setting.dsl/model/expressions.ecore" as expressions
import "http://www.eclipse.org/emf/2002/Ecore" as ecore

Settings returns Settings:
	{Settings}
	imports+=Import*
	declarations += Declaration*
    physicalSettings += PhysicalSettings*;

Import returns machine::Import:
    "import" importURI=STRING;


PhysicalSettings returns PhysicalSettings:
    {PhysicalSettings}
    resource=[machine::IResource|ResourceQualifiedName] '.' (peripheral=[machine::Peripheral|IID]) '{'
        ('Timings' '{' timingSettings+=TimingSettingsMapEntry* '}' )?
        motionSettings+=MotionSettingsMapEntry* 
    '}';

MotionSettingsMapEntry returns MotionSettingsMapEntry:
    {MotionSettingsMapEntry}
    'Axis' key=[machine::Axis|ID] '{' value=MotionSettings '}';

MotionSettings returns MotionSettings:
    {MotionSettings}
    'Profiles' '{' profileSettings+=ProfileSettingsMapEntry* '}' 
    ('Positions' '{' locationSettings+=LocationSettingsMapEntry* '}')?
    ('Distances' '{' distanceSettings+=DistanceSettingsMapEntry* '}')?;

ProfileSettingsMapEntry returns ProfileSettingsMapEntry:
    key=[machine::Profile|ID] value=MotionProfileSettings;

LocationSettingsMapEntry returns LocationSettingsMapEntry:
    key=[machine::Position|ID] value=PhysicalLocation;

DistanceSettingsMapEntry returns DistanceSettingsMapEntry:
    key=[machine::Distance|ID] '=' value=Expression;

TimingSettingsMapEntry returns TimingSettingsMapEntry:
    key = [machine::ActionType|ID] '=' value = Timing;

MotionProfileSettings returns MotionProfileSettings:
    {MotionProfileSettings}
    motionProfile=ID? '(' (motionArguments+= MotionArgumentsMapEntry (',' motionArguments+= MotionArgumentsMapEntry)*)? ')';

MotionArgumentsMapEntry returns MotionArgumentsMapEntry:
    key=ID '=' value=Expression;

PhysicalLocation returns PhysicalLocation:
    ('=' defaultExp=Expression) | '(' ('min' '=' minExp=Expression ',')? ('max' '=' maxExp=Expression ',')? 'default' '=' defaultExp=Expression ')';

// timing part

Timing returns timing::Timing:
    FixedValue|Array|TriangularDistribution|PertDistribution|NormalDistribution|EnumeratedDistribution;

FixedValue returns timing::FixedValue:
    valueExp=Expression; 

Array returns timing::Array:
    'Array' '(' valuesExp+=Expression ( "," valuesExp+=Expression)* ')';

TriangularDistribution returns timing::TriangularDistribution:
    'Triangular' '(' 'min' '=' minExp=Expression ',' 'max' '=' maxExp=Expression ',' 'mode' '=' modeExp=Expression (',' 'default' '=' defaultExp=Expression)? ')';

PertDistribution returns timing::PertDistribution:
    'Pert' '(' 'min' '=' minExp=Expression ',' 'max' '=' maxExp=Expression ',' 'mode' '=' modeExp=Expression ',' 'gamma' '=' gammaExp=Expression (',' 'default' '=' defaultExp=Expression)? ')';

NormalDistribution returns timing::NormalDistribution:
    'Normal' '(' 'mean' '=' meanExp=Expression ',' 'sd' '=' sdExp=Expression (',' 'default' '=' defaultExp=Expression)? ')';

EnumeratedDistribution returns timing::EnumeratedDistribution:
    'Enumerated' '(' valuesExp+=Expression ( "," valuesExp+=Expression)* (',' 'default' '=' defaultExp=Expression)? ')';

// expressions part

Declaration returns expressions::Declaration:
    {expressions::Declaration}
    'val' name=ID '=' expression=Expression
;

Expression returns expressions::Expression:
    PlusOrMinus
;

PlusOrMinus returns expressions::Expression:
    MulOrDiv (
        ({expressions::Add.left=current} '+' | {expressions::Subtract.left=current} '-') 
        right=MulOrDiv
    )*
;

MulOrDiv returns expressions::Expression:
    Primary (
        ({expressions::Multiply.left=current} '*' | {expressions::Divide.left=current} '/') 
        right=Primary
    )*
;

Primary returns expressions::Expression:
    '(' Expression ')' |  Atomic
;

Atomic returns expressions::Expression:
    {expressions::BigDecimalConstant} value=EBigDecimal |
    {expressions::DeclarationRef} declaration=[expressions::Declaration]
;

// general part
EBigDecimal returns ecore::EBigDecimal:
    '-'? INT ('.' INT)? (('E'|'e') '-'? INT)?;


IID returns ecore::EString: 
    INT | ID
;
 
ResourceQualifiedName:
   IID ('.' IID)?;
