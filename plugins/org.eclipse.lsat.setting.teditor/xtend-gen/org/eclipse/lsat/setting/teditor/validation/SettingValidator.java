/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor.validation;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import expressions.Declaration;
import expressions.Expression;
import expressions.ExpressionsPackage;
import expressions.impl.DeclarationRefImpl;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import machine.ActionType;
import machine.Axis;
import machine.IResource;
import machine.Import;
import machine.Machine;
import machine.MachinePackage;
import machine.Peripheral;
import machine.Position;
import machine.Profile;
import machine.Resource;
import machine.SymbolicPosition;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileParameter;
import org.eclipse.lsat.setting.teditor.validation.AbstractSettingValidator;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2;
import setting.MotionProfileSettings;
import setting.MotionSettings;
import setting.PhysicalSettings;
import setting.SettingPackage;
import setting.Settings;
import setting.impl.MotionSettingsMapEntryImpl;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class SettingValidator extends AbstractSettingValidator {
  public static final String INVALID_IMPORT = "invalidImport";
  
  @Check
  public void checkImportIsValid(final Import imp) {
    try {
      final boolean isImportUriValid = EcoreUtil2.isValidUri(imp, URI.createURI(imp.getImportURI()));
      if ((!isImportUriValid)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("The import ");
        String _importURI = imp.getImportURI();
        _builder.append(_importURI);
        _builder.append(" cannot be resolved. Make sure that the name is spelled correctly.");
        this.error(_builder.toString(), imp, MachinePackage.Literals.IMPORT__IMPORT_URI, SettingValidator.INVALID_IMPORT);
      }
      final boolean isUnderstood = imp.getImportURI().matches(".*\\.(machine|setting)");
      if ((!isUnderstood)) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Importing ");
        String _importURI_1 = imp.getImportURI();
        _builder_1.append(_importURI_1);
        _builder_1.append(" is not allowed. Only \'machine\' and \'setting\' files are allowed");
        this.error(_builder_1.toString(), imp, MachinePackage.Literals.IMPORT__IMPORT_URI, SettingValidator.INVALID_IMPORT);
      }
    } catch (final Throwable _t) {
      if (_t instanceof IllegalArgumentException) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("The import ");
        String _importURI_2 = imp.getImportURI();
        _builder_2.append(_importURI_2);
        _builder_2.append(" is not a valid URI.");
        this.error(_builder_2.toString(), imp, 
          MachinePackage.Literals.IMPORT__IMPORT_URI, SettingValidator.INVALID_IMPORT);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  @Check
  public void checkPhysicalSettingsForDuplicates(final Settings settings) {
    final Function1<PhysicalSettings, IResource> _function = (PhysicalSettings it) -> {
      return it.getResource();
    };
    final Consumer<List<PhysicalSettings>> _function_1 = (List<PhysicalSettings> it) -> {
      final Function1<PhysicalSettings, Peripheral> _function_2 = (PhysicalSettings it_1) -> {
        return it_1.getPeripheral();
      };
      final Function1<List<PhysicalSettings>, Boolean> _function_3 = (List<PhysicalSettings> it_1) -> {
        int _size = it_1.size();
        return Boolean.valueOf((_size > 1));
      };
      final Consumer<PhysicalSettings> _function_4 = (PhysicalSettings duplicate) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Physical settings have been defined more than once for peripheral ");
        String _fqn = duplicate.fqn();
        _builder.append(_fqn);
        _builder.append(". Please remove all duplicate instances.");
        this.error(_builder.toString(), settings, 
          SettingPackage.Literals.SETTINGS__PHYSICAL_SETTINGS, settings.getPhysicalSettings().indexOf(duplicate));
      };
      Iterables.<PhysicalSettings>concat(IterableExtensions.<List<PhysicalSettings>>filter(IterableExtensions.<Peripheral, PhysicalSettings>groupBy(it, _function_2).values(), _function_3)).forEach(_function_4);
    };
    IterableExtensions.<IResource, PhysicalSettings>groupBy(settings.getPhysicalSettings(), _function).values().forEach(_function_1);
  }
  
  @Check
  public void checkPhysicalSettingsForDuplicatesinResourceResourceImpl(final Settings settings) {
    final Function1<PhysicalSettings, Resource> _function = (PhysicalSettings it) -> {
      return it.getResource().getResource();
    };
    final Consumer<List<PhysicalSettings>> _function_1 = (List<PhysicalSettings> it) -> {
      final Function1<PhysicalSettings, Peripheral> _function_2 = (PhysicalSettings it_1) -> {
        return it_1.getPeripheral();
      };
      final Function1<List<PhysicalSettings>, Boolean> _function_3 = (List<PhysicalSettings> it_1) -> {
        final Function1<PhysicalSettings, Class<? extends IResource>> _function_4 = (PhysicalSettings it_2) -> {
          return it_2.getResource().getClass();
        };
        int _size = IterableExtensions.<Class<? extends IResource>, PhysicalSettings>groupBy(it_1, _function_4).size();
        return Boolean.valueOf((_size > 1));
      };
      final Consumer<PhysicalSettings> _function_4 = (PhysicalSettings it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Physical settings have been defined for resource and resource item. Settings must be defined on Resource or all Resource items.");
        this.error(_builder.toString(), settings, 
          SettingPackage.Literals.SETTINGS__PHYSICAL_SETTINGS, settings.getPhysicalSettings().indexOf(it_1));
      };
      Iterables.<PhysicalSettings>concat(IterableExtensions.<List<PhysicalSettings>>filter(IterableExtensions.<Peripheral, PhysicalSettings>groupBy(it, _function_2).values(), _function_3)).forEach(_function_4);
    };
    IterableExtensions.<Resource, PhysicalSettings>groupBy(settings.getPhysicalSettings(), _function).values().forEach(_function_1);
  }
  
  @Check
  public void checkTransitivePhysicalSettingsForDuplicates(final Settings settings) {
    final Consumer<Import> _function = (Import imp) -> {
      final Function1<Settings, EList<PhysicalSettings>> _function_1 = (Settings it) -> {
        return it.getPhysicalSettings();
      };
      final Iterable<PhysicalSettings> importedSettings = Queries.<Settings, PhysicalSettings>collect(this.loadSettings(imp), _function_1);
      final Function1<PhysicalSettings, Boolean> _function_2 = (PhysicalSettings ps) -> {
        return Boolean.valueOf(this.hasDuplicate(importedSettings, ps));
      };
      final Consumer<PhysicalSettings> _function_3 = (PhysicalSettings it) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Settings for peripheral \'");
        String _fqn = it.fqn();
        _builder.append(_fqn);
        _builder.append("\' are already defined in \'");
        String _importURI = imp.getImportURI();
        _builder.append(_importURI);
        _builder.append("\'. Remove either the import or this definition.");
        final String errorText = _builder.toString();
        this.error(errorText, settings, 
          SettingPackage.Literals.SETTINGS__PHYSICAL_SETTINGS, settings.getPhysicalSettings().indexOf(it));
        this.error(errorText, imp, MachinePackage.Literals.IMPORT__IMPORT_URI);
      };
      IterableExtensions.<PhysicalSettings>filter(settings.getPhysicalSettings(), _function_2).forEach(_function_3);
    };
    settings.getImports().forEach(_function);
  }
  
  /**
   * Duplicate if a peripheral setting exists for this resource or the resource parent
   */
  public boolean hasDuplicate(final Iterable<PhysicalSettings> col, final PhysicalSettings ps) {
    final Function1<PhysicalSettings, Boolean> _function = (PhysicalSettings it) -> {
      return Boolean.valueOf((Objects.equal(it.getPeripheral(), ps.getPeripheral()) && (Objects.equal(it.getResource(), ps.getResource()) || Objects.equal(it.getResource(), ps.getResource().getResource()))));
    };
    boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<PhysicalSettings>filter(col, _function));
    return (!_isEmpty);
  }
  
  @Check
  public void checkTransitivePhysicalSettingsForDuplicatesInImports(final Settings settings) {
    final HashMap<Import, Collection<PhysicalSettings>> importContents = new HashMap<Import, Collection<PhysicalSettings>>();
    final Consumer<Import> _function = (Import it) -> {
      final Function1<Settings, EList<PhysicalSettings>> _function_1 = (Settings it_1) -> {
        return it_1.getPhysicalSettings();
      };
      importContents.put(it, IterableExtensions.<PhysicalSettings>toSet(Queries.<Settings, PhysicalSettings>collect(this.loadSettings(it), _function_1)));
    };
    settings.getImports().forEach(_function);
    final Function1<PhysicalSettings, IResource> _function_1 = (PhysicalSettings it) -> {
      return it.getResource();
    };
    final Consumer<List<PhysicalSettings>> _function_2 = (List<PhysicalSettings> it) -> {
      final Function1<PhysicalSettings, Peripheral> _function_3 = (PhysicalSettings it_1) -> {
        return it_1.getPeripheral();
      };
      final Function1<List<PhysicalSettings>, Boolean> _function_4 = (List<PhysicalSettings> it_1) -> {
        int _size = it_1.size();
        return Boolean.valueOf((_size > 1));
      };
      final Consumer<PhysicalSettings> _function_5 = (PhysicalSettings setting) -> {
        final Function1<Map.Entry<Import, Collection<PhysicalSettings>>, Boolean> _function_6 = (Map.Entry<Import, Collection<PhysicalSettings>> it_1) -> {
          return Boolean.valueOf(it_1.getValue().contains(setting));
        };
        final Import imp = IterableExtensions.<Map.Entry<Import, Collection<PhysicalSettings>>>findFirst(importContents.entrySet(), _function_6).getKey();
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Duplicate definition of \'");
        String _fqn = setting.fqn();
        _builder.append(_fqn);
        _builder.append("\'. Remove import or one of the \'");
        String _fqn_1 = setting.fqn();
        _builder.append(_fqn_1);
        _builder.append("\' definitions.");
        final String errorText = _builder.toString();
        this.error(errorText, imp, MachinePackage.Literals.IMPORT__IMPORT_URI);
      };
      Iterables.<PhysicalSettings>concat(IterableExtensions.<List<PhysicalSettings>>filter(IterableExtensions.<Peripheral, PhysicalSettings>groupBy(it, _function_3).values(), _function_4)).forEach(_function_5);
    };
    IterableExtensions.<IResource, PhysicalSettings>groupBy(Iterables.<PhysicalSettings>concat(importContents.values()), _function_1).values().forEach(_function_2);
  }
  
  @Check
  public void checkDeclarationsForDuplicates(final Settings settings) {
    final Function1<Declaration, String> _function = (Declaration it) -> {
      return it.getName();
    };
    final Function1<List<Declaration>, Boolean> _function_1 = (List<Declaration> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<Declaration> _function_2 = (Declaration it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Declaration \'");
      String _name = it.getName();
      _builder.append(_name);
      _builder.append("\' declared more than once, please remove one");
      this.error(_builder.toString(), settings, 
        SettingPackage.Literals.SETTINGS__DECLARATIONS, settings.getDeclarations().indexOf(it));
    };
    Iterables.<Declaration>concat(IterableExtensions.<List<Declaration>>filter(IterableExtensions.<String, Declaration>groupBy(settings.getDeclarations(), _function).values(), _function_1)).forEach(_function_2);
  }
  
  @Check
  public void checkTransitiveDeclarationsForDuplicates(final Settings settings) {
    final Consumer<Import> _function = (Import imp) -> {
      final Function1<Settings, EList<Declaration>> _function_1 = (Settings it) -> {
        return it.getDeclarations();
      };
      final Iterable<Declaration> importedSettings = Queries.<Settings, Declaration>collect(this.loadSettings(imp), _function_1);
      final Function1<Declaration, Boolean> _function_2 = (Declaration d) -> {
        return Boolean.valueOf(this.hasDuplicate(importedSettings, d));
      };
      final Consumer<Declaration> _function_3 = (Declaration it) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Declaration \'");
        String _name = it.getName();
        _builder.append(_name);
        _builder.append("\' is already defined in \'");
        String _importURI = imp.getImportURI();
        _builder.append(_importURI);
        _builder.append("\'. Remove either the import or the declaration.");
        final String errorText = _builder.toString();
        this.error(errorText, settings, 
          SettingPackage.Literals.SETTINGS__DECLARATIONS, settings.getDeclarations().indexOf(it));
      };
      IterableExtensions.<Declaration>filter(settings.getDeclarations(), _function_2).forEach(_function_3);
    };
    settings.getImports().forEach(_function);
  }
  
  @Check
  public void checkTransitiveDeclarationForDuplicatesInImports(final Settings settings) {
    final HashMap<Import, Collection<Declaration>> importContents = new HashMap<Import, Collection<Declaration>>();
    final Consumer<Import> _function = (Import it) -> {
      final Function1<Settings, EList<Declaration>> _function_1 = (Settings it_1) -> {
        return it_1.getDeclarations();
      };
      importContents.put(it, IterableExtensions.<Declaration>toSet(Queries.<Settings, Declaration>collect(this.loadSettings(it), _function_1)));
    };
    settings.getImports().forEach(_function);
    final Function1<Declaration, String> _function_1 = (Declaration it) -> {
      return it.getName();
    };
    final Function1<List<Declaration>, Boolean> _function_2 = (List<Declaration> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<Declaration> _function_3 = (Declaration d) -> {
      final Function1<Map.Entry<Import, Collection<Declaration>>, Boolean> _function_4 = (Map.Entry<Import, Collection<Declaration>> it) -> {
        return Boolean.valueOf(it.getValue().contains(d));
      };
      final Import imp = IterableExtensions.<Map.Entry<Import, Collection<Declaration>>>findFirst(importContents.entrySet(), _function_4).getKey();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Declaration \'");
      String _name = d.getName();
      _builder.append(_name);
      _builder.append("\' declared more than once in imports. Remove import or one of the \'");
      String _name_1 = d.getName();
      _builder.append(_name_1);
      _builder.append("\' declarations.");
      final String errorText = _builder.toString();
      this.error(errorText, imp, MachinePackage.Literals.IMPORT__IMPORT_URI);
    };
    Iterables.<Declaration>concat(IterableExtensions.<List<Declaration>>filter(IterableExtensions.<String, Declaration>groupBy(Queries.<Declaration>unique(Iterables.<Declaration>concat(importContents.values())), _function_1).values(), _function_2)).forEach(_function_3);
  }
  
  /**
   * Duplicate if a declaration with name exist in the collection
   */
  public boolean hasDuplicate(final Iterable<Declaration> col, final Declaration d) {
    final Function1<Declaration, Boolean> _function = (Declaration it) -> {
      String _name = it.getName();
      String _name_1 = d.getName();
      return Boolean.valueOf(Objects.equal(_name, _name_1));
    };
    boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<Declaration>filter(col, _function));
    return (!_isEmpty);
  }
  
  private HashSet<Settings> loadSettings(final Import imp) {
    final HashSet<Settings> result = new HashSet<Settings>();
    try {
      final Set<Settings> load = IterableExtensions.<Settings>toSet(Iterables.<Settings>filter(imp.load(), Settings.class));
      result.addAll(load);
      final Function1<Settings, EList<EObject>> _function = (Settings it) -> {
        return it.loadAll();
      };
      final Consumer<Settings> _function_1 = (Settings it) -> {
        result.add(it);
      };
      Iterables.<Settings>filter(Queries.<Settings, EObject>collect(load, _function), Settings.class).forEach(_function_1);
    } catch (final Throwable _t) {
      if (_t instanceof Exception) {
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return result;
  }
  
  /**
   * use impl because of suppressed visibility of hasCircularDependencies
   */
  @Check
  public void checkCircularReference(final DeclarationRefImpl ref) {
    boolean _hasCircularDependencies = ref.hasCircularDependencies(ref);
    if (_hasCircularDependencies) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Circular reference");
      this.error(_builder.toString(), ref, ExpressionsPackage.Literals.DECLARATION_REF__DECLARATION);
    }
  }
  
  @Check
  public void checkPhysicalSettingsForResourceComplete(final Settings settings) {
    final Procedure2<Import, Integer> _function = (Import machineImport, Integer index) -> {
      final Function1<Machine, EList<Resource>> _function_1 = (Machine it) -> {
        return it.getResources();
      };
      final Function1<Resource, EList<Peripheral>> _function_2 = (Resource it) -> {
        return it.getPeripherals();
      };
      final Set<Peripheral> importedPeripherals = IterableExtensions.<Peripheral>toSet(Queries.<Resource, Peripheral>collect(Queries.<Machine, Resource>collect(Iterables.<Machine>filter(machineImport.load(), Machine.class), _function_1), _function_2));
      final Function1<Peripheral, Boolean> _function_3 = (Peripheral peripheral) -> {
        boolean _isEmpty = IterableExtensions.isEmpty(this.incompleteResourceSettings(peripheral, settings));
        return Boolean.valueOf((!_isEmpty));
      };
      final Set<Peripheral> incompletePeripherals = IterableExtensions.<Peripheral>toSet(IterableExtensions.<Peripheral>filter(importedPeripherals, _function_3));
      boolean _isEmpty = incompletePeripherals.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("No settings specified for peripheral ");
        {
          boolean _hasElements = false;
          for(final Peripheral peripheral : incompletePeripherals) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(", ", "");
            }
            String _incompleteSettingsStr = this.incompleteSettingsStr(peripheral, settings);
            _builder.append(_incompleteSettingsStr);
          }
        }
        final String text = _builder.toString();
        this.warning(text, MachinePackage.Literals.IMPORT_CONTAINER__IMPORTS, (index).intValue());
      }
    };
    IterableExtensions.<Import>forEach(settings.getImports(), _function);
  }
  
  public String incompleteSettingsStr(final Peripheral peripheral, final Settings settings) {
    final Function1<IResource, String> _function = (IResource it) -> {
      String _fqn = it.fqn();
      String _plus = ("\'" + _fqn);
      String _plus_1 = (_plus + ".");
      String _name = peripheral.getName();
      String _plus_2 = (_plus_1 + _name);
      return (_plus_2 + "\'");
    };
    return String.join(",", IterableExtensions.map(this.incompleteResourceSettings(peripheral, settings), _function));
  }
  
  public Iterable<? extends IResource> incompleteResourceSettings(final Peripheral peripheral, final Settings settings) {
    final Function1<IResource, Boolean> _function = (IResource it) -> {
      PhysicalSettings _physicalSettings = settings.getPhysicalSettings(it, peripheral);
      return Boolean.valueOf((_physicalSettings == null));
    };
    return IterableExtensions.filter(this.resourceOrItems(peripheral.getResource()), _function);
  }
  
  public List<? extends IResource> resourceOrItems(final Resource resource) {
    List<? extends IResource> _xifexpression = null;
    boolean _isEmpty = resource.getItems().isEmpty();
    if (_isEmpty) {
      _xifexpression = CollectionLiterals.<Resource>newArrayList(resource);
    } else {
      _xifexpression = resource.getItems();
    }
    return _xifexpression;
  }
  
  @Check
  public void checkMotionSettingsComplete(final PhysicalSettings physicalSettings) {
    final Set<Axis> peripheralAxes = this.<Axis>noSettings(physicalSettings.getPeripheral().getType().getAxes(), physicalSettings.getMotionSettings());
    boolean _isEmpty = peripheralAxes.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("No motion settings specified for axes ");
      {
        boolean _hasElements = false;
        for(final Axis axis : peripheralAxes) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(", ", "");
          }
          String _name = axis.getName();
          _builder.append(_name);
        }
      }
      this.warning(_builder.toString(), 
        SettingPackage.Literals.PHYSICAL_SETTINGS__MOTION_SETTINGS);
    }
  }
  
  @Check
  public void checkTimingSettingsComplete(final PhysicalSettings physicalSettings) {
    final Set<ActionType> peripheralActions = this.<ActionType>noSettings(physicalSettings.getPeripheral().getType().getActions(), physicalSettings.getTimingSettings());
    boolean _isEmpty = peripheralActions.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("No timing settings specified for actions ");
      {
        boolean _hasElements = false;
        for(final ActionType action : peripheralActions) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(", ", "");
          }
          String _name = action.getName();
          _builder.append(_name);
        }
      }
      this.warning(_builder.toString(), 
        SettingPackage.Literals.PHYSICAL_SETTINGS__TIMING_SETTINGS);
    }
  }
  
  @Check
  public void checkProfileSettingsComplete(final MotionSettings motionSettings) {
    Map.Entry<Axis, MotionSettings> _entry = motionSettings.getEntry();
    final Peripheral peripheral = ((MotionSettingsMapEntryImpl) _entry).getSettings().getPeripheral();
    final Set<Profile> peripheralProfiles = this.<Profile>noSettings(peripheral.getProfiles(), motionSettings.getProfileSettings());
    boolean _isEmpty = peripheralProfiles.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("No settings specified for profiles ");
      {
        boolean _hasElements = false;
        for(final Profile profile : peripheralProfiles) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(", ", "");
          }
          String _name = profile.getName();
          _builder.append(_name);
        }
      }
      this.warning(_builder.toString(), 
        SettingPackage.Literals.MOTION_SETTINGS__PROFILE_SETTINGS);
    }
  }
  
  @Check
  public void checkTimingSettingsComplete(final MotionSettings motionSettings) {
    Map.Entry<Axis, MotionSettings> _entry = motionSettings.getEntry();
    final Axis axis = ((MotionSettingsMapEntryImpl) _entry).getKey();
    Map.Entry<Axis, MotionSettings> _entry_1 = motionSettings.getEntry();
    final Peripheral peripheral = ((MotionSettingsMapEntryImpl) _entry_1).getSettings().getPeripheral();
    final Function1<SymbolicPosition, Position> _function = (SymbolicPosition it) -> {
      return it.getPosition(axis);
    };
    final Set<Position> peripheralPositions = this.<Position>noSettings(ListExtensions.<SymbolicPosition, Position>map(peripheral.getPositions(), _function), motionSettings.getLocationSettings());
    boolean _isEmpty = peripheralPositions.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("No settings specified for positions ");
      {
        boolean _hasElements = false;
        for(final Position position : peripheralPositions) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(", ", "");
          }
          String _name = position.getName();
          _builder.append(_name);
        }
      }
      this.warning(_builder.toString(), 
        SettingPackage.Literals.MOTION_SETTINGS__LOCATION_SETTINGS);
    }
  }
  
  @Check
  public void checkMotionProfileSettings(final MotionProfileSettings mps) {
    try {
      final MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
      final MotionProfile motionProfile = motionCalculator.getMotionProfile(mps.getMotionProfile());
      if ((motionProfile == null)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Selected motion calculator ");
        String _name = motionCalculator.getName();
        _builder.append(_name);
        _builder.append(" does not support ");
        String _xifexpression = null;
        String _motionProfile = mps.getMotionProfile();
        boolean _tripleEquals = (_motionProfile == null);
        if (_tripleEquals) {
          _xifexpression = "default ";
        } else {
          _xifexpression = "";
        }
        _builder.append(_xifexpression);
        _builder.append("motion profile ");
        String _motionProfile_1 = mps.getMotionProfile();
        _builder.append(_motionProfile_1);
        this.error(_builder.toString(), 
          SettingPackage.Literals.MOTION_PROFILE_SETTINGS__MOTION_PROFILE);
        return;
      }
      final Function1<MotionProfileParameter, Boolean> _function = (MotionProfileParameter it) -> {
        return Boolean.valueOf(it.isRequired());
      };
      final Function1<MotionProfileParameter, String> _function_1 = (MotionProfileParameter it) -> {
        return it.getKey();
      };
      final Set<String> requiredParameterKeys = this.<String>noSettings(IterableExtensions.<MotionProfileParameter, String>map(IterableExtensions.<MotionProfileParameter>filter(motionProfile.getParameters(), _function), _function_1), mps.getMotionArguments());
      boolean _isEmpty = requiredParameterKeys.isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("No settings specified for required parameters ");
        {
          boolean _hasElements = false;
          for(final String parameter : requiredParameterKeys) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder_1.appendImmediate(", ", "");
            }
            _builder_1.append(parameter);
          }
        }
        this.error(_builder_1.toString(), 
          SettingPackage.Literals.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS);
      }
      final Function1<MotionProfileParameter, String> _function_2 = (MotionProfileParameter it) -> {
        return it.getKey();
      };
      final Iterable<String> parameterKeys = IterableExtensions.<MotionProfileParameter, String>map(motionProfile.getParameters(), _function_2);
      final Procedure2<Map.Entry<String, Expression>, Integer> _function_3 = (Map.Entry<String, Expression> argument, Integer index) -> {
        boolean _contains = IterableExtensions.contains(parameterKeys, argument.getKey());
        boolean _not_1 = (!_contains);
        if (_not_1) {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("Unknown parameter ");
          String _key = argument.getKey();
          _builder_2.append(_key);
          _builder_2.append("  for motion profile ");
          String _motionProfile_2 = mps.getMotionProfile();
          _builder_2.append(_motionProfile_2);
          this.error(_builder_2.toString(), 
            SettingPackage.Literals.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS, (index).intValue());
        }
      };
      IterableExtensions.<Map.Entry<String, Expression>>forEach(mps.getMotionArguments(), _function_3);
    } catch (final Throwable _t) {
      if (_t instanceof MotionException) {
        final MotionException e = (MotionException)_t;
        this.error(e.getMessage(), null);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  public <T extends Object> Set<T> noSettings(final Iterable<T> col, final EMap<T, ?> settings) {
    final Function1<T, Boolean> _function = (T k) -> {
      final Function1<T, Boolean> _function_1 = (T it) -> {
        return Boolean.valueOf(Objects.equal(it, k));
      };
      T _findFirst = IterableExtensions.<T>findFirst(settings.keySet(), _function_1);
      return Boolean.valueOf((_findFirst == null));
    };
    return IterableExtensions.<T>toSet(IterableExtensions.<T>filter(col, _function));
  }
}
