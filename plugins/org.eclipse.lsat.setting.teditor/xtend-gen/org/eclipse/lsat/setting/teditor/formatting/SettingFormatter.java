/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor.formatting;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.lsat.setting.teditor.services.SettingGrammarAccess;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;
import org.eclipse.xtext.util.Pair;
import org.eclipse.xtext.xbase.lib.Extension;

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
@SuppressWarnings("all")
public class SettingFormatter extends AbstractDeclarativeFormatter {
  @Inject
  @Extension
  private SettingGrammarAccess _settingGrammarAccess;
  
  @Override
  protected void configureFormatting(final FormattingConfig c) {
    c.setAutoLinewrap(120);
    List<Pair<Keyword, Keyword>> _findKeywordPairs = this._settingGrammarAccess.findKeywordPairs("{", "}");
    for (final Pair<Keyword, Keyword> pair : _findKeywordPairs) {
      {
        c.setIndentation(pair.getFirst(), pair.getSecond());
        c.setLinewrap(1).after(pair.getFirst());
        c.setLinewrap(1).before(pair.getSecond());
        c.setLinewrap(1).after(pair.getSecond());
      }
    }
    List<Pair<Keyword, Keyword>> _findKeywordPairs_1 = this._settingGrammarAccess.findKeywordPairs("(", ")");
    for (final Pair<Keyword, Keyword> pair_1 : _findKeywordPairs_1) {
      {
        c.setNoSpace().after(pair_1.getFirst());
        c.setNoSpace().before(pair_1.getSecond());
      }
    }
    List<Keyword> _findKeywords = this._settingGrammarAccess.findKeywords(".");
    for (final Keyword dot : _findKeywords) {
      {
        c.setNoLinewrap().around(dot);
        c.setNoSpace().around(dot);
      }
    }
    List<Keyword> _findKeywords_1 = this._settingGrammarAccess.findKeywords(",");
    for (final Keyword comma : _findKeywords_1) {
      {
        c.setNoLinewrap().before(comma);
        c.setNoSpace().before(comma);
      }
    }
    c.setLinewrap(1).after(this._settingGrammarAccess.getImportRule());
    c.setLinewrap(2, 2, 2).before(this._settingGrammarAccess.getPhysicalSettingsRule());
    c.setLinewrap(2, 2, 2).after(this._settingGrammarAccess.getPhysicalSettingsRule());
    c.setLinewrap(1).before(this._settingGrammarAccess.getTimingSettingsMapEntryRule());
    c.setLinewrap(1).before(this._settingGrammarAccess.getProfileSettingsMapEntryRule());
    c.setLinewrap(1).before(this._settingGrammarAccess.getLocationSettingsMapEntryRule());
    c.setLinewrap(0, 1, 2).before(this._settingGrammarAccess.getSL_COMMENTRule());
    c.setLinewrap(0, 1, 2).before(this._settingGrammarAccess.getML_COMMENTRule());
    c.setLinewrap(0, 1, 1).after(this._settingGrammarAccess.getML_COMMENTRule());
  }
}
