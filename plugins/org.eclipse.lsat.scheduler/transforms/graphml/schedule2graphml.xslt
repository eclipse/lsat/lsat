<?xml version="1.0"?>
<!--

    Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation

    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:y="http://www.yworks.com/xml/graphml" xmlns:gml="http://graphml.graphdrawing.org/xmlns"
    xmlns:str="http://exslt.org/strings"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.eclipse.org/lsat/scheduler/schedule"
    extension-element-prefixes="str">
    <xsl:output method="xml" indent="yes" />

    <xsl:template match='/'>
        <graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
            <key id="name" for="node" attr.name="name" attr.type="string" />
            <key id="executionTime" for="node" attr.name="executionTime" attr.type="double" />
            <key id="type" for="node" attr.name="type" attr.type="string" />
            <key for="node" id="d10" yfiles.type="nodegraphics" />
            <key id="symbol" for="node" attr.name="symbol" attr.type="string" />
            <key id="lineNumber" for="node" attr.name="lineNumber" attr.type="int" />
            <key id="type" for="edge" attr.name="type" attr.type="string" />
            <key id="sourceLineNumber" for="edge" attr.name="sourceLineNumber" attr.type="int" />
            <key id="labelE" for="edge" attr.name="labelE" attr.type="string" />
            <key id="labelV" for="node" attr.name="labelV" attr.type="string" />
            <key for="edge" id="d18" yfiles.type="edgegraphics"/>
            <graph edgedefault="directed">
                <xsl:apply-templates select="*" />
            </graph>
        </graphml>
    </xsl:template>


    <!-- edge id="e0" source="n0" target="n1" -->
    <xsl:template match="edges">
        <edge source="{@sourceNode}" target="{@targetNode}" xmlns="http://graphml.graphdrawing.org/xmlns">
            <xsl:attribute name="id">//@edges.<xsl:value-of select='count(preceding-sibling::edges)' /></xsl:attribute>
        </edge>
    </xsl:template>

    <!-- <node id="n0"> <data key="d4" xml:space="preserve"><![CDATA[Top Description]]></data> -->
    <xsl:template match="nodes">
        <node labels=":{@name} ({@startTime}:{@endTime})" xmlns="http://graphml.graphdrawing.org/xmlns">
            <xsl:attribute name="id">//@nodes.<xsl:value-of select='count(preceding-sibling::nodes)' /></xsl:attribute>
            <xsl:apply-templates select="@name" mode="key"/>
            <data key="labelV" xmlns="http://graphml.graphdrawing.org/xmlns">
                <xsl:value-of select='@name' />
            </data>
            <data key="d10" xmlns="http://graphml.graphdrawing.org/xmlns">
           <y:ShapeNode>
                  <y:Fill transparent="false">
                       <xsl:attribute name="color"><xsl:apply-templates select="@xsi:type" mode="color"/></xsl:attribute>
                  </y:Fill>
                  <y:Geometry width="200" height="30"/>
                  <y:NodeLabel>
                      <xsl:value-of select='@name' />(<xsl:value-of select='@startTime' />:<xsl:value-of select='@endTime' />)
                  </y:NodeLabel>
                </y:ShapeNode>
            </data>

            <desc xmlns="http://graphml.graphdrawing.org/xmlns">
                <xsl:value-of select='@name' />
            </desc>
        </node>
    </xsl:template>

    <xsl:template match="sequences">
     <xsl:variable name="count" select="count(preceding-sibling::sequences)"/>
     <xsl:variable name="name" select="@name"/>
        <xsl:message>aal string: <xsl:value-of select='@scheduledTasks'/></xsl:message>
     <xsl:for-each select="str:tokenize(@scheduledTasks, ' ')">
        <xsl:message>string: <xsl:value-of select='.'/> <xsl:value-of select='preceding::*[1]'/></xsl:message>
        <xsl:if test="preceding::*[1]">
          <edge source="{preceding::*[1]}" target="{.}" xmlns="http://graphml.graphdrawing.org/xmlns">
              <xsl:attribute name="id">//@edges.<xsl:value-of select='$count' />.<xsl:value-of select="count(preceding::*)-1"></xsl:value-of></xsl:attribute>
      <data key="d18">
        <y:PolyLineEdge>
          <y:LineStyle color="#99CC00" type="line" width="1.0"/>
          <y:Arrows source="none" target="standard"/>
          <y:EdgeLabel><xsl:value-of select="$count +1"/>/<xsl:value-of select="$name"/></y:EdgeLabel>
        </y:PolyLineEdge>
      </data>
          </edge>
        </xsl:if>
     </xsl:for-each>
   </xsl:template>

    <xsl:template match="@*" mode="key">
        <data key="{name(.)}" xmlns="http://graphml.graphdrawing.org/xmlns">
            <xsl:value-of select='.' />
        </data>
    </xsl:template>

    <xsl:template match="*">
        <xsl:apply-templates select="node()|@*" />
    </xsl:template>
    
    <xsl:template match="@*" />
    
    <xsl:template match="@xsi:type" mode="color">
      <xsl:message>color: <xsl:value-of select="."/></xsl:message>
      <xsl:choose>
        <xsl:when test=". = 'schedule:ScheduledTask'">#ffff99</xsl:when>
        <xsl:when test=". = 'lsat_graph:ClaimedByScheduledTask'">#ff99cc</xsl:when>
        <xsl:when test=". = 'lsat_graph:EventStatusTask'">#00DDDD</xsl:when>
        <xsl:otherwise test=". = 'schedule:ScheduledTask'">#FF0000</xsl:otherwise>
      </xsl:choose>
    </xsl:template>


</xsl:stylesheet>   