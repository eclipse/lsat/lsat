/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
modeltype m_schedule "strict" uses schedule('http://www.eclipse.org/lsat/scheduler/schedule');
modeltype m_lsat_graph "strict" uses lsat_graph('http://www.eclipse.org/lsat/lsat_graph');
modeltype m_graph "strict" uses graph('http://www.eclipse.org/lsat/scheduler/graph');
modeltype m_ecore "strict" uses ecore('http://www.eclipse.org/emf/2002/Ecore');

transformation visualizeClaimedBy(inout inoutSchedule : m_schedule);

main() {
    var schedules := inoutSchedule.rootObjects()[m_schedule::Schedule];
    schedules.sequences.map insertOutstandingTask();
    schedules.sequences.map insertAwaitingTask();
}

/**
* Place an awaiting time block if the event causes a wait time. 
* The awaiting time: 
*     endTime of previous tasks except the previous raise task
*/
mapping inout m_schedule::_Sequence::insertAwaitingTask()
 {
	self.scheduledTasks->select(tsk| tsk.isRequireEventTask())->forEach(e) {
    	    var eTime := e.calculateAwaitingTime();
    	    if( eTime < e.startTime){
            var dep := e.getPreviousRaiseDependency();
            var prevRaiseTask := dep.sourceNode.oclAsType(m_schedule::ScheduledTask);
            var newTask:= object m_lsat_graph::EventStatusTask @inoutSchedule {
                name := "Awaiting: " + e.getEventName();
                startTime := eTime;
                endTime := e.startTime;
                task := e.task;
                graph := e.graph;
            };
            newTask.task.aspects += e.task.aspects;
            //place the new task in between
            e.incomingEdges[m_schedule::ScheduledDependency]->select( ie | ie!=dep)->forEach(ie){ 
                ie.targetNode := newTask
            };
            e.graph.edges += object m_schedule::ScheduledDependency {
                    sourceNode := newTask;
                    targetNode := e;
                };
            self.scheduledTasks += newTask;
        };
	};
}	

/**
* Place an outstanding task if it takes>0 before a raised event is consumed 
*/
mapping inout m_schedule::_Sequence::insertOutstandingTask()
 {
    self.scheduledTasks->select(tsk| tsk.isRaiseEventTask())->forEach(e) {
        var dep := e.getNextRequireDependency();
        if(not dep.oclIsInvalid()) {
            var nextRequireTask := dep.targetNode.oclAsType(m_schedule::ScheduledTask);
            if( nextRequireTask.startTime > e.endTime){
                var newTask:= object m_lsat_graph::EventStatusTask @inoutSchedule {
                    name := "Outstanding: " + e.getEventName();
                    startTime := e.startTime;
                    endTime := nextRequireTask.startTime;
                    task := e.task;
                    graph := e.graph;
                };
                newTask.task.aspects += e.task.aspects;
                //place the new task in between
                dep.targetNode := newTask;
                e.graph.edges += object m_schedule::ScheduledDependency {
                        sourceNode := newTask;
                        targetNode := nextRequireTask;
                    };
                
                self.scheduledTasks += newTask;
            };
        }
    };
}   
	
query m_schedule::ScheduledTask::isRequireEventTask(): Boolean {
    var r := self.task.aspects[m_lsat_graph::EventAnnotation]->select(e |  e.requireEvent) ->notEmpty();
    return r;
}

query m_schedule::ScheduledTask::isRaiseEventTask(): Boolean {
    var r :=  self.task.aspects[m_lsat_graph::EventAnnotation]->select(e |  not e.requireEvent) ->notEmpty();
    return r;
}

query m_schedule::ScheduledTask::getEventName(): String {
    return self.task.aspects[m_lsat_graph::EventAnnotation]->first().name;
}

// is event take the max endTime of all non Event and DispatchGroupTask edges or else original end time
query m_schedule::ScheduledTask::calculateAwaitingTime(): EBigDecimal {
    if(self.incomingEdges->size()>1 and not(self.incomingEdges.sourceNode.oclAsType(m_schedule::ScheduledTask)->select(tsk| tsk.isDispatchGroupTask())->size()>0)){
        var  eventName = self.getEventName();
        return self.incomingEdges.sourceNode.oclAsType(m_schedule::ScheduledTask)->select(tsk| not (tsk.isRaiseEventTask() and tsk.getEventName()=eventName))->collect(s | s.endTime)->max();
    };
    return self.startTime;
}

query m_schedule::ScheduledTask::getNextRequireDependency(): m_schedule::ScheduledDependency {
    var  eventName := self.getEventName();
    return self.outgoingEdges.oclAsType(m_schedule::ScheduledDependency)->select( e | e.targetNode.oclAsType(m_schedule::ScheduledTask).isRequireEvent(eventName))->first();
}

query m_schedule::ScheduledTask::isRequireEvent(eventName: String): Boolean {
        return self.isRequireEventTask() and (self.getEventName()=eventName);
}

query m_schedule::ScheduledTask::isDispatchGroupTask(): Boolean {
        return self.task.oclIsTypeOf(lsat_graph::DispatchGroupTask);
}

query m_schedule::ScheduledTask::getPreviousRaiseDependency(): m_schedule::ScheduledDependency {
    var  eventName := self.getEventName();
    return self.incomingEdges[m_schedule::ScheduledDependency]->select( e | e.sourceNode.oclAsType(m_schedule::ScheduledTask).isRaiseEvent(eventName))->first();
}

query m_schedule::ScheduledTask::isRaiseEvent(eventName: String): Boolean {
        return self.isRaiseEventTask() and (self.getEventName()=eventName);
}

