/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler

import lsat_graph.ActionTask
import lsat_graph.DispatchGroupTask
import org.eclipse.lsat.common.graph.directed.DirectedGraph
import org.eclipse.lsat.common.graph.directed.Edge
import org.eclipse.lsat.motioncalculator.MotionException
import org.eclipse.lsat.timing.util.ITimingCalculator
import org.eclipse.lsat.timing.util.SpecificationException
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor
import org.slf4j.LoggerFactory

@FinalFieldsConstructor
class AddExecutionTimes {
    static val LOGGER = LoggerFactory.getLogger(AddExecutionTimes)

    extension val ITimingCalculator timingCalculator;

    def <T extends Task> TaskDependencyGraph<T> transformModel(
        TaskDependencyGraph<T> graph) throws SpecificationException, MotionException {
        LOGGER.debug('Starting transformation')
        graph.addExecutionTimes
        LOGGER.debug('Finished transformation')
        return graph
    }

    private def <T extends Task, E extends Edge> void addExecutionTimes(
        DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
        graph.subGraphs.forEach[addExecutionTimes]
        graph.nodes.forEach[addExecutionTime]
    }

    private def dispatch void addExecutionTime(DispatchGroupTask task) {
        if (task.executionTime === null) {
            throw new IllegalStateException('''Expected execution time to be set for task: «task»''')
        }
    }

    private def dispatch void addExecutionTime(ActionTask<?> task) throws SpecificationException, MotionException {
        task.executionTime = task.action.calculateDuration
    }
}
