/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler

import activity.EventAction
import activity.RequireEvent
import java.util.HashMap
import lsat_graph.ClaimTask
import lsat_graph.EventAnnotation
import lsat_graph.ReleaseTask
import lsat_graph.lsat_graphFactory
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.lsat.common.graph.directed.DirectedGraph
import org.eclipse.lsat.common.graph.directed.Edge
import org.eclipse.lsat.common.graph.directed.Node
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph
import org.eclipse.lsat.motioncalculator.MotionException
import org.eclipse.lsat.timing.util.SpecificationException
import org.slf4j.LoggerFactory

/**
 * Removes the generated event resources and inserts a direct dependency between the actions
 */
class AnnotateClaimRelease<T extends Task, E extends Edge> {
    static val LOGGER = LoggerFactory.getLogger(AnnotateClaimRelease)

    final TaskDependencyGraph<T> root

    private new(TaskDependencyGraph<T> graph) {
        this.root = graph
        if (graph.eResource === null) {
            // attach a resource set if there is none
            createResourceSet.createResource(URI.createURI("graph")).contents += graph;
        }
    }

    static def <T extends Task> transformModel(TaskDependencyGraph<T> graph) {
        LOGGER.debug('Starting transformation')
        val scr = new AnnotateClaimRelease(graph)
        scr.processGraph(scr.root)
        LOGGER.debug('Finished transformation')
        return graph
    }

    private def <T extends Task, E extends Edge> void processGraph(
        DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
        // creating a  copy first because the graph is modified
        graph.subGraphs.clone.forEach[processGraph]
        graph.nodes.clone.forEach[annotate]
    }

    /**
     * search the original node in the activity an check if it is has type event.
     */
    private def dispatch void annotate(Node node) {
    }

    private def dispatch void annotate(ClaimTask task) throws SpecificationException, MotionException {
        val event = task.action.outgoingEdges.map[targetNode].filter(EventAction).findFirst[true]
        if (event !== null) {
            task.aspects += queryCreateEventAnnotation(this.root, event.resource.fqn, event instanceof RequireEvent);
        }
    }

    private def dispatch void annotate(ReleaseTask task) throws SpecificationException, MotionException {
        val event = task.action.incomingEdges.map[sourceNode].filter(EventAction).findFirst[true]
        if (event !== null) {
            task.aspects += queryCreateEventAnnotation(this.root, event.resource.fqn, event instanceof RequireEvent);
        }
    }

    def static <T extends Task> EventAnnotation<T> queryCreateEventAnnotation(TaskDependencyGraph<T> graph,
        String eventName, boolean require) {
        var result = graph.aspects.filter(EventAnnotation).map[EventAnnotation<T> a|a].filter[requireEvent == require].
            findFirst[name == eventName]
        if (result === null) {
            result = lsat_graphFactory.eINSTANCE.createEventAnnotation();
            result.name = eventName
            result.requireEvent = require
            graph.aspects += result
            graph.eResource().resourceSet.createResource(URI.createURI("events/" + eventName + "/" + require)).
                contents += result;
        }
        return result
    }

    def static ResourceSet createResourceSet() {
        val resourceSet = new ResourceSetImpl();
        resourceSet.setURIResourceMap(new HashMap<URI, Resource>());
        return resourceSet;
    }

}
