/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.slf4j.Logger;
import org.xml.sax.InputSource;

public class Graph2GraphML {
    private static final Logger LOGGER = getLogger(Graph2GraphML.class);

    private Graph2GraphML() {
        // Empty
    }

    public static void transform(URI input, URI output) {
        try {
            xTransform(input, output);
        } catch (Exception e) {
            LOGGER.error("xTRansform failed", e);
        }
    }

    public static void xTransform(URI input, URI output) throws TransformerException, IOException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        SAXSource xslt = new SAXSource(
                new InputSource(Graph2GraphML.class.getResourceAsStream("/transforms/graphml/graph2graphml.xslt")));
        Transformer transformer = transformerFactory.newTransformer(xslt);
        ExtensibleURIConverterImpl convertor = new ExtensibleURIConverterImpl();

        try (InputStream in = convertor.createInputStream(input)) {
            Source inSource = new SAXSource(new InputSource(convertor.createInputStream(input)));
            try (OutputStream out = convertor.createOutputStream(output)) {
                Result outResult = new StreamResult(out);
                transformer.transform(inSource, outResult);
            }
        }
    }
}
