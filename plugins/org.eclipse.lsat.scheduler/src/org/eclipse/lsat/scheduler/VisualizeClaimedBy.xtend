/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler

import lsat_graph.ClaimReleaseResource
import lsat_graph.ClaimTask
import lsat_graph.ClaimedByScheduledTask
import lsat_graph.ReleaseTask
import lsat_graph.lsat_graphFactory
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.schedule.Schedule
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask
import org.eclipse.lsat.common.scheduler.schedule.Sequence

import static extension org.eclipse.lsat.common.xtend.Queries.*

/**
 * 
 */
class VisualizeClaimedBy<T extends Task> {
    extension val lsat_graphFactory m_lsat_graph = lsat_graphFactory::eINSTANCE

    def void transformModel(Schedule<T> schedule, IProgressMonitor monitor) {
        schedule.sequences.filter[resource instanceof ClaimReleaseResource].forEach[createClaimedByTasks]
    }

    def private void createClaimedByTasks(Sequence<T> claimReleaseSequence) {
        // The next query groups claims or releases when they occur at the same time
        // This can only happen for passive claims
        val claimReleaseGroups = claimReleaseSequence.scheduledTasks.segment [ l, r |
            l.task.class == r.task.class && l.startTime == r.startTime
        ].toList

        var ClaimedByScheduledTask currentClaimedBy = null
        for (group : claimReleaseGroups) {
            if (group.forall[task instanceof ClaimTask]) {
                val firstClaim = group.head as ScheduledTask<ClaimTask>
                val previousClaimedBy = currentClaimedBy
                currentClaimedBy = createClaimedByScheduledTask => [
                    startTime = firstClaim.startTime
                    task = firstClaim.task
                    graph = firstClaim.graph
                    sequence = claimReleaseSequence
                ]
                if (previousClaimedBy !== null) {
                    previousClaimedBy.endTime = currentClaimedBy.startTime
                    currentClaimedBy.claims += previousClaimedBy.claims
                }
                currentClaimedBy.claims += group as Iterable<? extends ScheduledTask<Task>>
                currentClaimedBy.deriveNameFromClaims
            } else if (currentClaimedBy !== null && group.forall[task instanceof ReleaseTask]) {
                val lastRelease = group.last as ScheduledTask<ReleaseTask>
                currentClaimedBy.endTime = lastRelease.endTime
                currentClaimedBy.releases += group as Iterable<? extends ScheduledTask<Task>>

                if (currentClaimedBy.claims.size == currentClaimedBy.releases.size) {
                    currentClaimedBy = null
                } else {
                    // There are some more passive claims
                    val remainingClaims = currentClaimedBy.claims.toSet
                    remainingClaims -= currentClaimedBy.releases.map[claim].toSet

                    currentClaimedBy = createClaimedByScheduledTask => [
                        startTime = lastRelease.endTime
                        task = lastRelease.task
                        graph = lastRelease.graph
                        sequence = claimReleaseSequence
                        claims += remainingClaims
                    ]
                    currentClaimedBy.deriveNameFromClaims
                }
            }
        }
    }

    def private getClaim(ScheduledTask<Task> scheduledTask) {
        val sequence = scheduledTask.sequence as Sequence<Task>
        return sequence.scheduledTasks.filter[task instanceof ClaimTask].findFirst[task.graph == scheduledTask.task.graph]
    }

    def private void deriveNameFromClaims(ClaimedByScheduledTask claimedBy)  {
        val isPassive = claimedBy.claims.map[task].filter(ClaimTask).forall[action.isPassive]
        if (isPassive) {
            if (claimedBy.claims.size > 1) {
                claimedBy.name = '''Claimed passively by «claimedBy.claims.size» activities'''
            } else {
                claimedBy.name = '''Claimed passively by «claimedBy.claims.head.task.graph.name»'''
            }
        } else {
            claimedBy.name = '''Claimed by «claimedBy.claims.head.task.graph.name»'''
        }
    }
}
