/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;

import setting.Settings;

public final class StochasticImpactAnalysisInput<T extends Task> {
    private final Schedule<T> itsSchedule;

    private final Settings itsSettings;

    public StochasticImpactAnalysisInput(Schedule<T> schedule, Settings setting) {
        itsSchedule = schedule;
        itsSettings = setting;
    }

    public Schedule<T> getSchedule() {
        return itsSchedule;
    }

    Settings getSettings() {
        return itsSettings;
    }
}
