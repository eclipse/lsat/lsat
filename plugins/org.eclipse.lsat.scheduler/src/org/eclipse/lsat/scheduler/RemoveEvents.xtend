/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler

import activity.EventAction
import java.util.HashMap
import lsat_graph.EventTask
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.lsat.common.graph.directed.DirectedGraph
import org.eclipse.lsat.common.graph.directed.Edge
import org.eclipse.lsat.common.graph.directed.Node
import org.eclipse.lsat.common.graph.directed.util.DirectedGraphUtil
import org.eclipse.lsat.common.scheduler.graph.GraphFactory
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph
import org.eclipse.lsat.motioncalculator.MotionException
import org.eclipse.lsat.timing.util.SpecificationException
import org.slf4j.LoggerFactory

/**
 * Removes the generated event resources and inserts a direct dependency between the actions
 */
class RemoveEvents<T extends Task, E extends Edge> {
    static val LOGGER = LoggerFactory.getLogger(RemoveEvents)

    final TaskDependencyGraph<T> root

    private new(TaskDependencyGraph<T> graph) {
        this.root = graph
        if (graph.eResource === null) {
            // attach a resource set if there is none
            createResourceSet.createResource(URI.createURI("graph")).contents += graph;
        }
    }

    static def <T extends Task> transformModel(TaskDependencyGraph<T> graph) {
        LOGGER.debug('Starting transformation')
        val scr = new RemoveEvents(graph)
        scr.processGraph(scr.root)
        LOGGER.debug('Finished transformation')
        return graph
    }

    private def <T extends Task, E extends Edge> void processGraph(
        DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
        // creating a  copy first because the graph is modified
        graph.subGraphs.clone.forEach[processGraph]
        graph.nodes.clone.forEach[process]
    }

    /**
     * search the original node in the activity an check if it is has type event.
     */
    private def dispatch void process(Node node) {
    }

    private def dispatch void process(EventTask<EventAction> task) throws SpecificationException, MotionException {
        DirectedGraphUtil.shortcutAndRemoveNode(task, [GraphFactory.eINSTANCE.createDependency] )
    }
 
    def static ResourceSet createResourceSet() {
        val resourceSet = new ResourceSetImpl();
        resourceSet.setURIResourceMap(new HashMap<URI, Resource>());
        return resourceSet;
    }

}
