/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

public class CriticalPathAnalysis<T extends Task> extends AbstractModelTransformer<Schedule<T>, Schedule<T>> {
    @Override
    protected String getDefaultTransformation() {
        return "/transforms/criticalPathAnalysis.qvto";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Schedule<T> doTransformModel(Schedule<T> input) throws QvtoTransformationException {
        BasicModelExtent inoutSchedule = new BasicModelExtent();
        inoutSchedule.add(input);

        execute(inoutSchedule);

        return validateOneAndOnlyOne(Schedule.class, inoutSchedule);
    }
}
