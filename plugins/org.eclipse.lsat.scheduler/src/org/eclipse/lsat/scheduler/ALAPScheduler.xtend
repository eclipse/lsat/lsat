/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler

import activity.SchedulingType
import lsat_graph.PeripheralActionTask
import lsat_graph.ReleaseTask
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.schedule.Schedule
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask
import org.slf4j.LoggerFactory

import static extension org.eclipse.lsat.common.graph.directed.util.DirectedGraphQueries.*
import static extension org.eclipse.lsat.common.xtend.Queries.*

class ALAPScheduler {
    static val LOGGER = LoggerFactory.getLogger(ALAPScheduler)

    def static <T extends Task> Schedule<T> applyALAPScheduling(Schedule<T> schedule) {
        LOGGER.debug('Starting transformation')
        schedule.allSubNodes.reverseTopologicalOrdering.filter[supportsALAP].forEach[moveALAP]
        schedule.allSubNodes.topologicalOrdering.filter[supportsASAP].forEach[moveASAP]
        LOGGER.debug('Finished transformation')
        return schedule
    }

    def private static <T extends Task> boolean supportsALAP(ScheduledTask<T> scheduledTask) {
        return switch it: scheduledTask.task {
            PeripheralActionTask: action.schedulingType == SchedulingType::ALAP
            ReleaseTask: false
            default: true
        }
    }

    def private static <T extends Task> void moveALAP(ScheduledTask<T> scheduledTask) {
        if (scheduledTask.outgoingEdges.isEmpty) {
            // Graph end-nodes are considered to be ALAP
            return
        }
        // The new end-time will be the minimum of the start-times of its successors
        val newEndTime = scheduledTask.outgoingEdges.map[targetNode as ScheduledTask<?>].map[startTime].min
        if (newEndTime > scheduledTask.endTime) {
            val timeShift = newEndTime - scheduledTask.endTime
            scheduledTask.startTime = (scheduledTask.startTime + timeShift).stripTrailingZeros
            scheduledTask.endTime = (scheduledTask.endTime + timeShift).stripTrailingZeros
        }
    }

    def private static <T extends Task> boolean supportsASAP(ScheduledTask<T> scheduledTask) {
        return switch it: scheduledTask.task {
            PeripheralActionTask: action.schedulingType == SchedulingType::ASAP
            default: true
        }
    }

    def private static <T extends Task> void moveASAP(ScheduledTask<T> scheduledTask) {
        // The new start-time will be the minimum of the start-times of its predecessors
        val newStartTime = scheduledTask.incomingEdges.map[sourceNode as ScheduledTask<?>].map[endTime].max(0bd)
        if (newStartTime < scheduledTask.startTime) {
            val timeShift = newStartTime - scheduledTask.startTime
            scheduledTask.startTime = (scheduledTask.startTime + timeShift).stripTrailingZeros
            scheduledTask.endTime = (scheduledTask.endTime + timeShift).stripTrailingZeros
        }
    }
}