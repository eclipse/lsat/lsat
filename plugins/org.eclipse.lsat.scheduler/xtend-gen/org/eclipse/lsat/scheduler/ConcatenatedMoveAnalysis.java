/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import activity.Move;
import activity.PeripheralAction;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.Consumer;
import lsat_graph.PeripheralActionTask;
import machine.IResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraphFactory;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2;

@SuppressWarnings("all")
public class ConcatenatedMoveAnalysis<T extends Task> {
  public static final String PASSING_MOVE_TIMING_GAP = "PassingMoveTimingGap";
  
  public Collection<Collection<Move>> getErroneousPassingMoves(final Schedule<T> schedule) {
    final LinkedHashSet<Collection<Move>> result = new LinkedHashSet<Collection<Move>>();
    final Function1<ScheduledTask<T>, Boolean> _function = (ScheduledTask<T> it) -> {
      Move _move = ConcatenatedMoveAnalysis.<T>getMove(it);
      return Boolean.valueOf((_move != null));
    };
    final Function1<ScheduledTask<T>, IResource> _function_1 = (ScheduledTask<T> it) -> {
      return ConcatenatedMoveAnalysis.<T>getMove(it).getResource();
    };
    Collection<List<ScheduledTask<T>>> _values = IterableExtensions.<IResource, ScheduledTask<T>>groupBy(IterableExtensions.<ScheduledTask<T>>filter(schedule.allNodesInTopologicalOrder(), _function), _function_1).values();
    for (final List<ScheduledTask<T>> sequence : _values) {
      final Procedure2<ScheduledTask<T>, Integer> _function_2 = (ScheduledTask<T> it, Integer index) -> {
        boolean _isPassing = ConcatenatedMoveAnalysis.<T>getMove(it).isPassing();
        if (_isPassing) {
          boolean _eq = this.eq(sequence.get(((index).intValue() + 1)).getStartTime(), it.getEndTime());
          boolean _not = (!_eq);
          if (_not) {
            result.add(Arrays.<Move>asList(ConcatenatedMoveAnalysis.<T>getMove(it), ConcatenatedMoveAnalysis.<T>getMove(sequence.get(((index).intValue() + 1)))));
          }
        }
      };
      IterableExtensions.<ScheduledTask<T>>forEach(sequence, _function_2);
    }
    return result;
  }
  
  public void annotateErroneousPassingMoves(final Schedule<T> schedule) {
    final Aspect<ScheduledTask<T>, ScheduledDependency> aspect = DirectedGraphFactory.eINSTANCE.<ScheduledTask<T>, ScheduledDependency>createAspect();
    aspect.setName(ConcatenatedMoveAnalysis.PASSING_MOVE_TIMING_GAP);
    final Function1<ScheduledTask<T>, Boolean> _function = (ScheduledTask<T> it) -> {
      Move _move = ConcatenatedMoveAnalysis.<T>getMove(it);
      return Boolean.valueOf((_move != null));
    };
    final Function1<ScheduledTask<T>, IResource> _function_1 = (ScheduledTask<T> it) -> {
      return ConcatenatedMoveAnalysis.<T>getMove(it).getResource();
    };
    final Consumer<List<ScheduledTask<T>>> _function_2 = (List<ScheduledTask<T>> sequence) -> {
      final Procedure2<ScheduledTask<T>, Integer> _function_3 = (ScheduledTask<T> it, Integer index) -> {
        boolean _isPassing = ConcatenatedMoveAnalysis.<T>getMove(it).isPassing();
        if (_isPassing) {
          boolean _eq = this.eq(sequence.get(((index).intValue() + 1)).getStartTime(), it.getEndTime());
          boolean _not = (!_eq);
          if (_not) {
            EList<ScheduledTask<T>> _nodes = aspect.getNodes();
            ScheduledTask<T> _get = sequence.get(((index).intValue() + 1));
            _nodes.add(_get);
            EList<ScheduledTask<T>> _nodes_1 = aspect.getNodes();
            ScheduledTask<T> _get_1 = sequence.get((index).intValue());
            _nodes_1.add(_get_1);
          }
        }
      };
      IterableExtensions.<ScheduledTask<T>>forEach(sequence, _function_3);
    };
    IterableExtensions.<IResource, ScheduledTask<T>>groupBy(IterableExtensions.<ScheduledTask<T>>filter(schedule.allNodesInTopologicalOrder(), _function), _function_1).values().forEach(_function_2);
    boolean _isEmpty = aspect.getNodes().isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      aspect.setGraph(schedule);
    }
  }
  
  public boolean eq(final BigDecimal a, final BigDecimal b) {
    if ((a == b)) {
      return true;
    }
    if (((a == null) || (b == null))) {
      return false;
    }
    int _compareTo = a.compareTo(b);
    return (_compareTo == 0);
  }
  
  public static <T extends Task> Move getMove(final ScheduledTask<T> task) {
    T childTask = task.getTask();
    if ((childTask instanceof PeripheralActionTask)) {
      PeripheralAction _action = ((PeripheralActionTask)childTask).getAction();
      if ((_action instanceof Move)) {
        PeripheralAction _action_1 = ((PeripheralActionTask)childTask).getAction();
        return ((Move) _action_1);
      }
    }
    return null;
  }
}
