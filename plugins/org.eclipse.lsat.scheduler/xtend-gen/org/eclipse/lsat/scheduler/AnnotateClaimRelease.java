/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler;

import activity.EventAction;
import activity.RequireEvent;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import lsat_graph.ClaimTask;
import lsat_graph.EventAnnotation;
import lsat_graph.ReleaseTask;
import lsat_graph.lsat_graphFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Removes the generated event resources and inserts a direct dependency between the actions
 */
@SuppressWarnings("all")
public class AnnotateClaimRelease<T extends Task, E extends Edge> {
  private static final Logger LOGGER = LoggerFactory.getLogger(AnnotateClaimRelease.class);
  
  private final TaskDependencyGraph<T> root;
  
  private AnnotateClaimRelease(final TaskDependencyGraph<T> graph) {
    this.root = graph;
    Resource _eResource = graph.eResource();
    boolean _tripleEquals = (_eResource == null);
    if (_tripleEquals) {
      EList<EObject> _contents = AnnotateClaimRelease.createResourceSet().createResource(URI.createURI("graph")).getContents();
      _contents.add(graph);
    }
  }
  
  public static <T extends Task> TaskDependencyGraph<T> transformModel(final TaskDependencyGraph<T> graph) {
    try {
      AnnotateClaimRelease.LOGGER.debug("Starting transformation");
      final AnnotateClaimRelease<T, Edge> scr = new AnnotateClaimRelease<T, Edge>(graph);
      scr.<T, Dependency>processGraph(scr.root);
      AnnotateClaimRelease.LOGGER.debug("Finished transformation");
      return graph;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private <T extends Task, E extends Edge> void processGraph(final DirectedGraph<T, E> graph) throws SpecificationException, MotionException {
    final Consumer<DirectedGraph<T, E>> _function = (DirectedGraph<T, E> it) -> {
      try {
        this.<T, E>processGraph(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    ((List<DirectedGraph<T, E>>)Conversions.doWrapArray(((DirectedGraph<T, E>[])Conversions.unwrapArray(graph.getSubGraphs(), DirectedGraph.class)).clone())).forEach(_function);
    final Consumer<T> _function_1 = (T it) -> {
      try {
        this.annotate(it);
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    };
    ((List<T>)Conversions.doWrapArray(((T[])Conversions.unwrapArray(graph.getNodes(), Task.class)).clone())).forEach(_function_1);
  }
  
  /**
   * search the original node in the activity an check if it is has type event.
   */
  private void _annotate(final Node node) {
  }
  
  private void _annotate(final ClaimTask task) throws SpecificationException, MotionException {
    final Function1<org.eclipse.lsat.common.graph.directed.editable.Edge, org.eclipse.lsat.common.graph.directed.editable.Node> _function = (org.eclipse.lsat.common.graph.directed.editable.Edge it) -> {
      return it.getTargetNode();
    };
    final Function1<EventAction, Boolean> _function_1 = (EventAction it) -> {
      return Boolean.valueOf(true);
    };
    final EventAction event = IterableExtensions.<EventAction>findFirst(Iterables.<EventAction>filter(ListExtensions.<org.eclipse.lsat.common.graph.directed.editable.Edge, org.eclipse.lsat.common.graph.directed.editable.Node>map(task.getAction().getOutgoingEdges(), _function), EventAction.class), _function_1);
    if ((event != null)) {
      EList<Aspect<?, ?>> _aspects = task.getAspects();
      EventAnnotation<T> _queryCreateEventAnnotation = AnnotateClaimRelease.<T>queryCreateEventAnnotation(this.root, event.getResource().fqn(), (event instanceof RequireEvent));
      _aspects.add(_queryCreateEventAnnotation);
    }
  }
  
  private void _annotate(final ReleaseTask task) throws SpecificationException, MotionException {
    final Function1<org.eclipse.lsat.common.graph.directed.editable.Edge, org.eclipse.lsat.common.graph.directed.editable.Node> _function = (org.eclipse.lsat.common.graph.directed.editable.Edge it) -> {
      return it.getSourceNode();
    };
    final Function1<EventAction, Boolean> _function_1 = (EventAction it) -> {
      return Boolean.valueOf(true);
    };
    final EventAction event = IterableExtensions.<EventAction>findFirst(Iterables.<EventAction>filter(ListExtensions.<org.eclipse.lsat.common.graph.directed.editable.Edge, org.eclipse.lsat.common.graph.directed.editable.Node>map(task.getAction().getIncomingEdges(), _function), EventAction.class), _function_1);
    if ((event != null)) {
      EList<Aspect<?, ?>> _aspects = task.getAspects();
      EventAnnotation<T> _queryCreateEventAnnotation = AnnotateClaimRelease.<T>queryCreateEventAnnotation(this.root, event.getResource().fqn(), (event instanceof RequireEvent));
      _aspects.add(_queryCreateEventAnnotation);
    }
  }
  
  public static <T extends Task> EventAnnotation<T> queryCreateEventAnnotation(final TaskDependencyGraph<T> graph, final String eventName, final boolean require) {
    final Function1<EventAnnotation<T>, EventAnnotation<T>> _function = (EventAnnotation<T> a) -> {
      return a;
    };
    final Function1<EventAnnotation<T>, EventAnnotation<T>> _final_function = _function;
    final Function1<EventAnnotation<T>, Boolean> _function_1 = (EventAnnotation<T> it) -> {
      boolean _isRequireEvent = it.isRequireEvent();
      return Boolean.valueOf((_isRequireEvent == require));
    };
    final Function1<EventAnnotation<T>, Boolean> _function_2 = (EventAnnotation<T> it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, eventName));
    };
    EventAnnotation<T> result = IterableExtensions.<EventAnnotation<T>>findFirst(IterableExtensions.<EventAnnotation<T>>filter(IterableExtensions.<EventAnnotation, EventAnnotation<T>>map(Iterables.<EventAnnotation>filter(graph.getAspects(), EventAnnotation.class), ((Function1<? super EventAnnotation, ? extends EventAnnotation<T>>)_final_function)), _function_1), _function_2);
    if ((result == null)) {
      result = lsat_graphFactory.eINSTANCE.<T>createEventAnnotation();
      result.setName(eventName);
      result.setRequireEvent(require);
      EList<Aspect<T, Dependency>> _aspects = graph.getAspects();
      _aspects.add(result);
      EList<EObject> _contents = graph.eResource().getResourceSet().createResource(URI.createURI(((("events/" + eventName) + "/") + Boolean.valueOf(require)))).getContents();
      _contents.add(result);
    }
    return result;
  }
  
  public static ResourceSet createResourceSet() {
    final ResourceSetImpl resourceSet = new ResourceSetImpl();
    HashMap<URI, Resource> _hashMap = new HashMap<URI, Resource>();
    resourceSet.setURIResourceMap(_hashMap);
    return resourceSet;
  }
  
  private void annotate(final Node task) throws SpecificationException, MotionException {
    if (task instanceof ClaimTask) {
      _annotate((ClaimTask)task);
      return;
    } else if (task instanceof ReleaseTask) {
      _annotate((ReleaseTask)task);
      return;
    } else if (task != null) {
      _annotate(task);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(task).toString());
    }
  }
}
