/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.ui;

import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.lsat.common.emf.common.util.URIHelper;
import org.eclipse.lsat.common.emf.ecore.resource.Persistor;
import org.eclipse.lsat.common.emf.ecore.resource.PersistorFactory;
import org.eclipse.lsat.common.emf.ecore.resource.ResourceSetUtil;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.conformance.ConformanceCheckInput;
import org.eclipse.lsat.conformance.ConformanceCheckJava;
import org.eclipse.lsat.conformance.Dispatching2PetriNet;
import org.eclipse.lsat.petri_net.PetriNet;
import org.eclipse.lsat.trace.TraceModel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;

import activity.ActivitySet;
import activity.util.Event2Resource;
import activity.util.PassiveClaimTransformer;
import dispatching.ActivityDispatching;
import dispatching.util.DispatchingUtil;
import machine.util.ImportsFlattener;

public class ConformanceCheckJob extends Job {
    private final PersistorFactory factory = new PersistorFactory();

    private final ConformanceCheckOptions options;

    private final UISynchronize sync;

    private final Shell shell;

    public ConformanceCheckJob(ConformanceCheckOptions options, UISynchronize sync, Shell shell) {
        super("ConformanceCheckJob");
        this.options = options;
        this.sync = sync;
        this.shell = shell;
    }

    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            monitor.beginTask("Performing conformance check on specification", 100);

            URI modelURI = URIHelper.asURI(options.getModelFile());
            IFolder saveIFolder = options.getModelFile().getProject().getFolder("analysis").getFolder("conformance");
            URI saveFolderURI = URIHelper.asURI(saveIFolder);
            URI saveBaseURI = saveFolderURI.appendSegment(URIHelper.baseName(modelURI));
            URI intermediateURI = URIHelper.asURI(saveIFolder).appendSegment(".intermediate")
                    .appendSegment(URIHelper.baseName(modelURI));
            Persistor<EObject> savePersistor = factory.getPersistor(EObject.class);

            monitor.subTask("Creating a petri net from specification " + options.getModelFile().getName());
            Persistor<ActivityDispatching> dispatchingPersistor = factory.getPersistor(ActivityDispatching.class);
            ActivityDispatching activityDispatching = dispatchingPersistor.loadOne(modelURI);
            // flatten the import containers to a single root per type and grab them again
            ResourceSet resourceSet = ImportsFlattener.flatten(intermediateURI, activityDispatching);
            activityDispatching = ResourceSetUtil.getObjectByType(resourceSet, ActivityDispatching.class);

            // convert event to resources in loaded model
            ActivitySet activitySet = ResourceSetUtil.getObjectByType(resourceSet, ActivitySet.class);
            Event2Resource
                    .replaceEventsWithClaimRelease(activitySet);
            PassiveClaimTransformer.transform(activitySet);
            DispatchingUtil.expand(activityDispatching);
            DispatchingUtil.removeUnusedActivities(activityDispatching);

            Dispatching2PetriNet dispatching2PetriNet = new Dispatching2PetriNet(options.isDipatchingLoop());
            PetriNet petriNet = dispatching2PetriNet.transformModel(activityDispatching, monitor);
            if (Activator.DEBUG) {
                savePersistor.save(saveBaseURI.appendFileExtension("petrinet"), petriNet);
            }
            if (monitor.isCanceled()) {
                return Status.CANCEL_STATUS;
            }
            monitor.worked(40);

            monitor.subTask("Running conformance check on petri net");
            ConformanceCheckInput conformanceCheckInput = new ConformanceCheckInput(petriNet);
            Persistor<TraceModel> tpPersistor = factory.getPersistor(TraceModel.class);
            for (IFile traceFile: options.getTraceFiles()) {
                URI traceURI = URIHelper.asURI(traceFile);
                conformanceCheckInput.getTraces().add(tpPersistor.loadOne(traceURI));
            }
            try {
                ConformanceCheckJava conformanceCheck = new ConformanceCheckJava(options.getConformanceLevel());
                petriNet = conformanceCheck.transformModel(conformanceCheckInput);
                if (Activator.DEBUG) {
                    savePersistor.save(saveBaseURI.appendFileExtension("petrinet"), petriNet);
                }
            } catch (QvtoTransformationException e) {
                savePersistor.save(saveBaseURI.appendFileExtension("petrinet"), petriNet);
                throw e;
            }
            if (monitor.isCanceled()) {
                return Status.CANCEL_STATUS;
            }
            monitor.worked(60);

            sync.asyncExec(new Runnable() {
                @Override
                public void run() {
                    new SuccessDialog(shell, "Perform conformance check on specification",
                            "The trace(s) conform to the specification").open();
                }
            });
            return Status.OK_STATUS;
        } catch (Exception e) {
            return new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
        } finally {
            monitor.done();
        }
    }

    private static class SuccessDialog extends MessageDialog {
        public SuccessDialog(Shell parent, String title, String message) {
            super(parent, title, null, message, INFORMATION, new String[] {IDialogConstants.OK_LABEL}, 0);
        }

        @Override
        public Image getInfoImage() {
            Optional<ImageDescriptor> imageDescriptor = ResourceLocator.imageDescriptorFromBundle(SuccessDialog.class,
                    "icons/run_wiz.png");
            return imageDescriptor.isPresent() ? imageDescriptor.get().createImage() : super.getInfoImage();
        }
    }
}
