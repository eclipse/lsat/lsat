/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.ui;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.lsat.common.emf.common.util.URIHelper;
import org.eclipse.lsat.common.emf.ecore.resource.Persistor;
import org.eclipse.lsat.common.emf.ecore.resource.PersistorFactory;
import org.eclipse.lsat.common.emf.ui.model.ModelWorkbenchContentProvider;
import org.eclipse.lsat.common.emf.ui.model.ModelWorkbenchLabelProvider;
import org.eclipse.lsat.conformance.FilterTPTracePointsInput;
import org.eclipse.lsat.conformance.FilterTPTracePointsJava;
import org.eclipse.lsat.trace.TraceModel;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionDelegate;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

import activity.Activity;
import activity.ActivitySet;

public class FilterTPTraceAction extends ActionDelegate {
    /**
     * Selected model files.
     */
    private List<IFile> files = Collections.emptyList();

    /**
     * {@inheritDoc}
     *
     * @see org.eclipse.ui.actions.ActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
     *     org.eclipse.jface.viewers.ISelection)
     * @generated
     */
    @Override
    @SuppressWarnings("unchecked")
    public void selectionChanged(IAction action, ISelection selection) {
        if (selection instanceof IStructuredSelection) {
            files = ((IStructuredSelection)selection).toList();
        } else {
            files = Collections.emptyList();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see org.eclipse.ui.actions.ActionDelegate#run(org.eclipse.jface.action.IAction)
     * @generated
     */
    @Override
    public void run(IAction action) {
        if (!files.isEmpty()) {
            try {
                final List<Activity> activities = selectActivities();
                if (null == activities || activities.isEmpty()) {
                    // User canceled
                    return;
                }
                IRunnableWithProgress operation = new IRunnableWithProgress() {
                    @Override
                    public void run(IProgressMonitor monitor) {
                        runWithProgress(monitor, activities);
                    }
                };
                PlatformUI.getWorkbench().getProgressService().run(true, true, operation);
            } catch (Exception e) {
                IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
                Activator.getDefault().getLog().log(status);
            }
        }
    }

    private void runWithProgress(IProgressMonitor monitor, List<Activity> activities) {
        try {
            monitor.beginTask("Filtering trace files", files.size());
            for (IFile modelIFile: files) {
                monitor.subTask("Filtering " + modelIFile.getName());

                PersistorFactory factory = new PersistorFactory();
                URI modelURI = URIHelper.asURI(modelIFile);

                Persistor<TraceModel> tracePersistor = factory.getPersistor(TraceModel.class);
                TraceModel trace = tracePersistor.loadOne(modelURI);
                FilterTPTracePointsJava filterTPTracePoints = new FilterTPTracePointsJava();
                List<TraceModel> traces = filterTPTracePoints
                        .transformModel(new FilterTPTracePointsInput(activities, trace));
                tracePersistor.save(modelURI, traces);

                monitor.worked(1);
                if (monitor.isCanceled()) {
                    break;
                }
            }
        } catch (final Exception e) {
            IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
            Activator.getDefault().getLog().log(status);
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                            "Filter trace files",
                            "Filter trace files failed:\n" + e.getMessage() + "\n\nSee Error Log for details.");
                }
            });
        } finally {
            monitor.done();
        }
    }

    @SuppressWarnings("unchecked")
    private List<Activity> selectActivities() {
        ModelWorkbenchLabelProvider modelWorkbenchLabelProvider = new ModelWorkbenchLabelProvider();
        ModelWorkbenchContentProvider modelWorkbenchContentProvider = new ModelWorkbenchContentProvider(
                modelWorkbenchLabelProvider.getAdapterFactory());
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), modelWorkbenchLabelProvider,
                modelWorkbenchContentProvider);
        dialog.setTitle("Select activities");
        dialog.setMessage("Select activities to filter from trace");
        dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
        // filter for specific files and elements only
        dialog.addFilter(new ViewerFilter() {
            @Override
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                if (element instanceof IFile) {
                    return Objects.equals("activity", ((IFile)element).getFileExtension());
                }
                return element instanceof IContainer || element instanceof ActivitySet || element instanceof Activity;
            }
        });
        dialog.setValidator(new ISelectionStatusValidator() {
            @Override
            public IStatus validate(Object[] selection) {
                if (selection.length <= 0) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select one or more activities!",
                            null);
                }
                for (Object selected: selection) {
                    if (!(selected instanceof Activity)) {
                        return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Only activity selection is allowed!",
                                null);
                    }
                }
                return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, "OK", null);
            }
        });
        dialog.open();
        return null == dialog.getResult() ? null : List.class.cast(Arrays.asList(dialog.getResult()));
    }
}
