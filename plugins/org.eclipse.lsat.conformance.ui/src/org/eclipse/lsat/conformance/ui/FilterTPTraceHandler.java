/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance.ui;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.inject.Named;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.lsat.common.emf.common.util.URIHelper;
import org.eclipse.lsat.common.emf.ecore.resource.Persistor;
import org.eclipse.lsat.common.emf.ecore.resource.PersistorFactory;
import org.eclipse.lsat.common.emf.ui.model.ModelWorkbenchContentProvider;
import org.eclipse.lsat.common.emf.ui.model.ModelWorkbenchLabelProvider;
import org.eclipse.lsat.conformance.FilterTPTracePointsInput;
import org.eclipse.lsat.conformance.FilterTPTracePointsJava;
import org.eclipse.lsat.trace.TraceModel;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

import activity.Activity;
import activity.ActivitySet;

public class FilterTPTraceHandler {
    @Execute
    public void execute(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
            Shell shell, IWorkspace workspace)
    {
        if (null == selection || selection.isEmpty()) {
            return;
        }
        if (!PlatformUI.getWorkbench().saveAllEditors(true)) {
            return;
        }
        final List<Activity> activities = selectActivities();
        if (null == activities || activities.isEmpty()) {
            // User canceled
            return;
        }
        FilterTPTraceJob job = new FilterTPTraceJob(selection.toList(), activities);
        job.setUser(true);
        job.schedule();
    }

    @SuppressWarnings("unchecked")
    private List<Activity> selectActivities() {
        ModelWorkbenchLabelProvider modelWorkbenchLabelProvider = new ModelWorkbenchLabelProvider();
        ModelWorkbenchContentProvider modelWorkbenchContentProvider = new ModelWorkbenchContentProvider(
                modelWorkbenchLabelProvider.getAdapterFactory());
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), modelWorkbenchLabelProvider,
                modelWorkbenchContentProvider);
        dialog.setTitle("Select activities");
        dialog.setMessage("Select activities to filter from trace");
        dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
        // filter for specific files and elements only
        dialog.addFilter(new ViewerFilter() {
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                if (element instanceof IFile) {
                    return Objects.equals("activity", ((IFile)element).getFileExtension());
                }
                return element instanceof IContainer || element instanceof ActivitySet || element instanceof Activity;
            }
        });
        dialog.setValidator(new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                if (selection.length <= 0) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select one or more activities!",
                            null);
                }
                for (Object selected: selection) {
                    if (!(selected instanceof Activity)) {
                        return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Only activity selection is allowed!",
                                null);
                    }
                }
                return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, "OK", null);
            }
        });
        dialog.open();
        return null == dialog.getResult() ? null : List.class.cast(Arrays.asList(dialog.getResult()));
    }

    private static class FilterTPTraceJob extends Job {
        private final List<?> traces;

        private final List<Activity> activities;

        public FilterTPTraceJob(List<?> traces, List<Activity> activities) {
            super("FilterTPTraceJob");
            this.traces = traces;
            this.activities = activities;
        }

        @Override
        protected IStatus run(IProgressMonitor monitor) {
            try {
                monitor.beginTask("Filtering trace files", traces.size());
                for (Object traceIFile: traces) {
                    PersistorFactory factory = new PersistorFactory();
                    URI modelURI = URIHelper.asURI((IFile)traceIFile);
                    monitor.subTask("Filtering " + modelURI);

                    Persistor<TraceModel> tracePersistor = factory.getPersistor(TraceModel.class);
                    TraceModel tpTraceModel = tracePersistor.loadOne(modelURI);
                    FilterTPTracePointsJava filterTPTracePoints = new FilterTPTracePointsJava();
                    List<TraceModel> tpTraceModels = filterTPTracePoints
                            .transformModel(new FilterTPTracePointsInput(activities, tpTraceModel));
                    tracePersistor.save(modelURI, tpTraceModels);

                    monitor.worked(1);
                    if (monitor.isCanceled())
                        return Status.CANCEL_STATUS;
                }
                return Status.OK_STATUS;
            } catch (final Exception e) {
                return new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
            } finally {
                monitor.done();
            }
        }
    }
}
