/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.ui.quickfix;

import dispatching.ActivityDispatching;
import dispatching.util.DispatchingUtil;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.dispatching.teditor.validation.DispatchingValidator;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.edit.IModification;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.xbase.lib.Conversions;

/**
 * Custom quickfixes.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#quickfixes
 */
@SuppressWarnings("all")
public class DispatchingQuickfixProvider extends DefaultQuickfixProvider {
  @Fix(DispatchingValidator.REPLACE_ITEM_FOR_RESOURCE)
  public void chooseItem(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final Consumer<String> _function = (String it) -> {
      final Object noImage = null;
      final IModification _function_1 = (IModificationContext context) -> {
        final IXtextDocument xtextDocument = context.getXtextDocument();
        xtextDocument.replace((issue.getOffset()).intValue(), (issue.getLength()).intValue(), it);
      };
      acceptor.accept(issue, ("Select " + it), it, ((String)noImage), _function_1);
    };
    ((List<String>)Conversions.doWrapArray(issue.getData())).forEach(_function);
  }
  
  @Fix(DispatchingValidator.REMOVE_ITEM)
  public void removeDuplicate(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final Object noImage = null;
    final IModification _function = (IModificationContext context) -> {
      final IXtextDocument xtextDocument = context.getXtextDocument();
      xtextDocument.replace((issue.getOffset()).intValue(), (issue.getLength()).intValue(), "");
    };
    acceptor.accept(issue, "Remove", "Remove", ((String)noImage), _function);
  }
  
  @Fix(DispatchingValidator.CONVERT_YIELD)
  public void convertYield(final Issue issue, final IssueResolutionAcceptor acceptor) {
    final ISemanticModification _function = (EObject element, IModificationContext context) -> {
      final ActivityDispatching ad = ((ActivityDispatching) element);
      DispatchingUtil.convertYield(ad);
    };
    acceptor.accept(issue, "Convert", "Convert", "", _function);
  }
}
