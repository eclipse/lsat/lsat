/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.ui.contentassist;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import dispatching.ActivityDispatching;
import dispatching.Attribute;
import dispatching.Dispatch;
import dispatching.HasUserAttributes;
import dispatching.impl.AttributesMapEntryImpl;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import machine.Resource;
import machine.ResourceItem;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.dispatching.teditor.ui.contentassist.AbstractDispatchingProposalProvider;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
@SuppressWarnings("all")
public class DispatchingProposalProvider extends AbstractDispatchingProposalProvider {
  @Override
  public void completeDispatch_ResourceItems(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final Dispatch dis = ((Dispatch) model);
    final Function1<Resource, EList<ResourceItem>> _function = (Resource it) -> {
      return it.getItems();
    };
    final Set<ResourceItem> eligibleItems = IterableExtensions.<ResourceItem>toSet(IterableExtensions.<Resource, ResourceItem>flatMap(dis.getActivity().getResourcesNeedingItem(), _function));
    final Function1<ResourceItem, String> _function_1 = (ResourceItem it) -> {
      return it.getName();
    };
    final Function1<List<ResourceItem>, Boolean> _function_2 = (List<ResourceItem> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size == 1));
    };
    final Set<ResourceItem> validNameItems = IterableExtensions.<ResourceItem>toSet(Iterables.<ResourceItem>concat(IterableExtensions.<List<ResourceItem>>filter(IterableExtensions.<String, ResourceItem>groupBy(eligibleItems, _function_1).values(), _function_2)));
    final Function1<ResourceItem, Resource> _function_3 = (ResourceItem it) -> {
      return it.getResource();
    };
    final Set<Resource> filledResources = IterableExtensions.<Resource>toSet(IterableExtensions.<Resource>filterNull(ListExtensions.<ResourceItem, Resource>map(dis.getResourceItems(), _function_3)));
    final Predicate<ResourceItem> _function_4 = (ResourceItem it) -> {
      return filledResources.contains(it.getResource());
    };
    eligibleItems.removeIf(_function_4);
    final Predicate<ResourceItem> _function_5 = (ResourceItem it) -> {
      return filledResources.contains(it.getResource());
    };
    validNameItems.removeIf(_function_5);
    AbstractElement _terminal = assignment.getTerminal();
    final com.google.common.base.Predicate<IEObjectDescription> _function_6 = (IEObjectDescription desc) -> {
      final String qName = desc.getQualifiedName().toString();
      final Function1<ResourceItem, Boolean> _function_7 = (ResourceItem it) -> {
        String _fqn = it.fqn();
        return Boolean.valueOf(Objects.equal(_fqn, qName));
      };
      ResourceItem _findFirst = IterableExtensions.<ResourceItem>findFirst(eligibleItems, _function_7);
      boolean _tripleNotEquals = (_findFirst != null);
      if (_tripleNotEquals) {
        return true;
      }
      final Function1<ResourceItem, Boolean> _function_8 = (ResourceItem it) -> {
        String _name = it.getName();
        return Boolean.valueOf(Objects.equal(_name, qName));
      };
      ResourceItem _findFirst_1 = IterableExtensions.<ResourceItem>findFirst(validNameItems, _function_8);
      boolean _tripleNotEquals_1 = (_findFirst_1 != null);
      if (_tripleNotEquals_1) {
        return true;
      }
      return false;
    };
    this.lookupCrossReference(((CrossReference) _terminal), context, acceptor, _function_6);
  }
  
  @Override
  public void complete_AttributesMapEntry(final EObject model, final RuleCall rule, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.complete_AttributesMapEntry(model, rule, context, acceptor);
    final ActivityDispatching activityDispatching = EcoreUtil2.<ActivityDispatching>getContainerOfType(model, ActivityDispatching.class);
    final HasUserAttributes container = EcoreUtil2.<HasUserAttributes>getContainerOfType(model, HasUserAttributes.class);
    final LinkedHashMap<Attribute, String> attrs = CollectionLiterals.<Attribute, String>newLinkedHashMap();
    this.collectUserAttributes(attrs, activityDispatching, container.getClass());
    boolean _isEmpty = attrs.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      final Function1<Attribute, String> _function = (Attribute it) -> {
        return it.getName();
      };
      final Function1<String, Boolean> _function_1 = (String nm) -> {
        final Function1<Attribute, Boolean> _function_2 = (Attribute it) -> {
          String _name = it.getName();
          return Boolean.valueOf(Objects.equal(nm, _name));
        };
        Attribute _findFirst = IterableExtensions.<Attribute>findFirst(container.getUserAttributes().keySet(), _function_2);
        return Boolean.valueOf((_findFirst == null));
      };
      final Consumer<String> _function_2 = (String it) -> {
        acceptor.accept(this.createCompletionProposal((it + ": "), it, null, context));
      };
      IterableExtensions.<String>filter(IterableExtensions.<Attribute, String>map(attrs.keySet(), _function), _function_1).forEach(_function_2);
    }
  }
  
  @Override
  public void complete_IDString(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.complete_IDString(model, ruleCall, context, acceptor);
    this.complete_AttributesMapEntryValue(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_INTASString(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.complete_INTASString(model, ruleCall, context, acceptor);
    this.complete_AttributesMapEntryValue(model, ruleCall, context, acceptor);
  }
  
  private void collectUserAttributes(final Map<Attribute, String> result, final EObject object, final Class<?> clazz) {
    if ((object instanceof HasUserAttributes)) {
      boolean _isInstance = clazz.isInstance(object);
      if (_isInstance) {
        final Consumer<Map.Entry<Attribute, String>> _function = (Map.Entry<Attribute, String> it) -> {
          result.put(it.getKey(), it.getValue());
        };
        ((HasUserAttributes)object).getUserAttributes().forEach(_function);
      }
    }
    if ((object != null)) {
      final Consumer<EObject> _function_1 = (EObject it) -> {
        this.collectUserAttributes(result, it, clazz);
      };
      object.eContents().forEach(_function_1);
    }
  }
  
  private void complete_AttributesMapEntryValue(final EObject model, final RuleCall rule, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    final ActivityDispatching activityDispatching = EcoreUtil2.<ActivityDispatching>getContainerOfType(model, ActivityDispatching.class);
    final HasUserAttributes container = EcoreUtil2.<HasUserAttributes>getContainerOfType(model, HasUserAttributes.class);
    Attribute _key = null;
    if (((AttributesMapEntryImpl) model)!=null) {
      _key=((AttributesMapEntryImpl) model).getKey();
    }
    String _name = null;
    if (_key!=null) {
      _name=_key.getName();
    }
    final String keyName = _name;
    final LinkedHashMap<Attribute, String> attrs = CollectionLiterals.<Attribute, String>newLinkedHashMap();
    this.collectUserAttributes(attrs, activityDispatching, container.getClass());
    if (((!attrs.isEmpty()) && (keyName != null))) {
      final Function1<Map.Entry<Attribute, String>, Boolean> _function = (Map.Entry<Attribute, String> it) -> {
        String _name_1 = it.getKey().getName();
        return Boolean.valueOf(Objects.equal(keyName, _name_1));
      };
      final Function1<Map.Entry<Attribute, String>, String> _function_1 = (Map.Entry<Attribute, String> it) -> {
        return it.getValue();
      };
      final Consumer<String> _function_2 = (String it) -> {
        acceptor.accept(this.createCompletionProposal(this.quote(it), it, null, context));
      };
      IterableExtensions.<String>toSet(IterableExtensions.<String>filterNull(IterableExtensions.<Map.Entry<Attribute, String>, String>map(IterableExtensions.<Map.Entry<Attribute, String>>filter(attrs.entrySet(), _function), _function_1))).forEach(_function_2);
    }
  }
  
  private String quote(final String string) {
    String _xifexpression = null;
    boolean _matches = string.matches("(\\w)*");
    if (_matches) {
      _xifexpression = string;
    } else {
      _xifexpression = (("\"" + string) + "\"");
    }
    return _xifexpression;
  }
}
