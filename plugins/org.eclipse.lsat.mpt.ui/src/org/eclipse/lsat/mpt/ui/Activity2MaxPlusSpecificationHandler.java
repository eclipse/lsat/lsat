/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.ui;

import javax.inject.Named;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;

public class Activity2MaxPlusSpecificationHandler extends ActivityTransformationHandler {
    @Execute
    public void execute(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
            Shell shell, IWorkspace workspace)
    {
        super.execute(selection, shell, workspace);

        // Create the max-plus specification.
        ActivityTransformationOptions options = new ActivityTransformationOptions(activityFile, cifFile, workspace);
        Activity2MaxPlusSpecificationJob job = new Activity2MaxPlusSpecificationJob(options, sync, shell);
        job.setUser(true);
        job.schedule();
    }
}
