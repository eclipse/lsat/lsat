/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.ui;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class ActivityTransformationHandler {
    @Inject
    UISynchronize sync;

    IFile activityFile;

    IFile cifFile;

    @Execute
    public void execute(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
            Shell shell, IWorkspace workspace)
    {
        // Get the activity file.
        if (null == selection || !(selection.getFirstElement() instanceof IFile)) {
            return;
        }
        if (!PlatformUI.getWorkbench().saveAllEditors(true)) {
            return;
        }

        activityFile = (IFile)selection.getFirstElement();

        // Get the CIF file.
        cifFile = selectCifSpecification();
        if (null == cifFile) {
            // User canceled
            return;
        }
    }

    protected IFile selectCifSpecification() {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), new WorkbenchLabelProvider(),
                new BaseWorkbenchContentProvider());
        dialog.setTitle("Select a CIF specification");
        dialog.setMessage("Select a CIF specification to attach to the activity specification.");
        dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
        dialog.setAllowMultiple(false);
        // filter for specific files and elements only
        dialog.addFilter(new ViewerFilter() {
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                if (element instanceof IFile) {
                    return ((IFile)element).getFileExtension().equalsIgnoreCase("cif");
                }
                return element instanceof IContainer;
            }
        });
        dialog.setValidator(new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                if (selection.length != 1 || !(selection[0] instanceof IFile)) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select one CIF specification.",
                            null);
                }
                return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, "OK", null);
            }
        });
        // Convenience: try to select the CIF file with the same name the activity file
        dialog.setInitialSelection(ResourcesPlugin.getWorkspace().getRoot()
                .getFile(activityFile.getFullPath().removeFileExtension().addFileExtension("cif")));
        dialog.open();
        return null == dialog.getResult() ? null : (IFile)dialog.getResult()[0];
    }
}
