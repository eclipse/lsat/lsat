/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.automata.Edge;
import org.eclipse.escet.cif.metamodel.cif.automata.EdgeEvent;
import org.eclipse.escet.cif.metamodel.cif.automata.Location;
import org.eclipse.escet.cif.metamodel.cif.expressions.EventExpression;

public class CifSpecificationHelper {
    private CifSpecificationHelper() {
    }

    public static List<List<String>> getPossiblePaths(Specification cifSpecification) {
        List<Location> locations = cifSpecification.getComponents().stream().filter(Automaton.class::isInstance)
                .map(Automaton.class::cast).flatMap(a -> a.getLocations().stream()).collect(Collectors.toList());
        Location initialLocation = locations.stream().filter(l -> !l.getInitials().isEmpty()).findFirst().get();
        List<Location> finalLocations = locations.stream().filter(l -> l.getEdges().isEmpty())
                .collect(Collectors.toList());
        List<List<String>> results = new ArrayList<>();
        // non cyclic: then there are finalLocations
        for (Location endLoc: finalLocations) {
            addPaths(initialLocation, endLoc, results);
        }
        // cyclic: then go from initial to initial
        addPaths(initialLocation, initialLocation, results);
        return results;
    }

    private static void addPaths(Location from, Location to, List<List<String>> result) {
        List<Edge> visited = new ArrayList<>();
        List<Location> path = new ArrayList<>();

        // path starts with from
        path.add(from);

        // call recursively to find all paths
        findAllPaths(from, to, visited, path, result);
    }

    /**
     * Find all paths from <b>from</b> till <b>to</b>
     *
     * @param visited is used for tracking already visited locations
     * @param path contains the found path up to from
     * @param result is used to store found paths
     */
    private static void findAllPaths(Location from, Location to, List<Edge> visited, List<Location> path,
            List<List<String>> result)
    {
        // if start and 'from' and 'to' are equal then search for all paths the eventually lead from 'from' till 'to'
        boolean start = path.size() == 1;
        if (!start && from.equals(to)) {
            List<String> activities = getEvents(path);
            result.add(activities);
            // if match found then no need to traverse more till depth
            return;
        }

        // Recur for all locations succeeding to the current location
        for (Edge edge: from.getEdges()) {
            if (!visited.contains(edge)) {
                visited.add(edge);
                Location successor = edge.getTarget();
                // store found location in path
                path.add(successor);
                // and recursively continue with the successor
                findAllPaths(successor, to, visited, path, result);
                // remove current (last) node and edge. using index! as path may contain node twice!
                path.remove(path.size() - 1);
                visited.remove(visited.size() - 1);
            }
        }
    }

    private static List<String> getEvents(List<Location> path) {
        List<String> activities = new ArrayList<>(path.size() - 1);
        for (int i = 0; i < path.size() - 1; i++) {
            activities.add(getEvent(path.get(i), path.get(i + 1)));
        }
        return activities;
    }

    private static String getEvent(Location a, Location b) {
        EventExpression exp = a.getEdges().stream().filter(edge -> edge.getTarget() == b)
                .flatMap(edge -> edge.getEvents().stream()).map(EdgeEvent::getEvent)
                .filter(EventExpression.class::isInstance).map(EventExpression.class::cast).findFirst().get();
        return exp.getEvent().getName();
    }
}
