/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timinganalysis.ui.launch;

import org.eclipse.core.resources.IFile;
import org.eclipse.lsat.common.emf.common.util.URIHelper;

public class TimingAnalysisUtil {
    private TimingAnalysisUtil() {
        // prohibit instantation
    }

    public static String getLaunchName(IFile dispatchFile, IFile settingFile, String showTargetName) {
        String prefix = getPrefix(dispatchFile, settingFile);
        String targetName = getShowTargetName(showTargetName);
        if (targetName.isEmpty()) {
            return prefix;
        }
        return prefix + " (" + targetName + ')';
    }

    public static String getName(IFile file) {
        return file == null ? "Unknown" : URIHelper.baseName(file);
    }

    private static String getPrefix(IFile dispatchFile, IFile settingFile) {
        return getPrefix(getName(dispatchFile), getName(settingFile));
    }

    private static String getPrefix(String dispatchName, String settingName) {
        if (dispatchName.contains(settingName)) {
            return cap(dispatchName);
        }
        if (settingName.contains(dispatchName)) {
            return cap(settingName);
        }
        return cap(dispatchName) + ' ' + cap(settingName);
    }

    private static String cap(String str) {
        if (str.length() > 0) {
            return str.substring(0, 1).toUpperCase() + str.substring(1);
        }
        return str;
    }

    private static String getShowTargetName(String target) {
        if (target == null) {
            return "";
        }
        if (TimingAnalysisLaunchAttributes.GANTT_CHART.endsWith(target)) {
            return "";
        }
        if (TimingAnalysisLaunchAttributes.CRITICAL_PATH.endsWith(target)) {
            return "Critical path";
        }
        if (TimingAnalysisLaunchAttributes.STOCHASTIC_IMPACT.endsWith(target)) {
            return "Stochastic impact";
        }
        if (TimingAnalysisLaunchAttributes.NO_GANTT_CHART.endsWith(target)) {
            return "No gantt chart";
        }
        return target;
    }
}
