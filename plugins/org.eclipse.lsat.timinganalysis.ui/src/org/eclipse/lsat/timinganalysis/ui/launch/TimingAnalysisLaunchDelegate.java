/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timinganalysis.ui.launch;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.lsat.common.emf.common.util.URIHelper;
import org.eclipse.lsat.common.emf.ecore.resource.Persistor;
import org.eclipse.lsat.common.emf.ecore.resource.PersistorFactory;
import org.eclipse.lsat.common.emf.ecore.resource.ResourceSetUtil;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.common.scheduler.algorithm.AsapScheduler;
import org.eclipse.lsat.common.scheduler.algorithm.CycleFoundException;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.common.scheduler.resources.ResourceModel;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.scheduler.ALAPScheduler;
import org.eclipse.lsat.scheduler.AddExecutionTimes;
import org.eclipse.lsat.scheduler.AnnotateClaimRelease;
import org.eclipse.lsat.scheduler.CleanupGraph;
import org.eclipse.lsat.scheduler.CleanupGraph.RemoveClaimReleaseStrategy;
import org.eclipse.lsat.scheduler.CleanupSchedule;
import org.eclipse.lsat.scheduler.ConcatenatedMoveAnalysis;
import org.eclipse.lsat.scheduler.CriticalPathAnalysis;
import org.eclipse.lsat.scheduler.Dispatching2Graph;
import org.eclipse.lsat.scheduler.Dispatching2GraphOutput;
import org.eclipse.lsat.scheduler.Graph2GraphML;
import org.eclipse.lsat.scheduler.MergeClaimedBy;
import org.eclipse.lsat.scheduler.RemoveEvents;
import org.eclipse.lsat.scheduler.Schedule2GraphML;
import org.eclipse.lsat.scheduler.StochasticImpactAnalysis;
import org.eclipse.lsat.scheduler.StochasticImpactAnalysisInput;
import org.eclipse.lsat.scheduler.VisualizeClaimedBy;
import org.eclipse.lsat.scheduler.VisualizeEvents;
import org.eclipse.lsat.scheduler.etfgen.GenerateAll;
import org.eclipse.lsat.scheduler.simulator.main.GenerateMachineTemplate;
import org.eclipse.lsat.scheduler.simulator.main.GenerateScheduleTemplate;
import org.eclipse.lsat.scheduler.simulator.main.GenerateSettingsTemplate;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.lsat.timing.util.TimingCalculator;
import org.eclipse.lsat.timinganalysis.ui.Activator;
import org.eclipse.lsat.timinganalysis.ui.AnimationView;
import org.eclipse.swt.widgets.Display;
import org.eclipse.trace4cps.ui.view.TraceView;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;

import activity.ActivitySet;
import activity.Move;
import activity.util.Event2Resource;
import dispatching.ActivityDispatching;
import dispatching.util.DispatchingUtil;
import machine.Import;
import machine.ImportContainer;
import machine.Machine;
import machine.util.ImportsFlattener;
import setting.SettingUtil;
import setting.Settings;

public class TimingAnalysisLaunchDelegate extends LaunchConfigurationDelegate
        implements TimingAnalysisLaunchAttributes
{
    private static final String BULLET = "\u2022";

    private IProgressMonitor monitor;

    private boolean runningInDebugMode;

    private PersistorFactory persistorFactory;

    private IFile dispatchingIFile;

    private IFolder saveIFolder;

    private URI saveBaseURI;

    @Override
    public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
            throws CoreException
    {
        this.monitor = monitor;
        this.runningInDebugMode = mode.equals("debug");
        this.persistorFactory = new PersistorFactory();
        this.dispatchingIFile = ResourcesPlugin.getWorkspace().getRoot()
                .getFile(new Path(configuration.getAttribute(MODEL_IFILE, MODEL_IFILE_DEFAULT)));
        this.saveIFolder = dispatchingIFile.getProject().getFolder("analysis").getFolder("scheduled");
        this.saveBaseURI = URIHelper.asURI(saveIFolder).appendSegment(URIHelper.baseName(dispatchingIFile));
        URI intermediateURI = URIHelper.asURI(saveIFolder).appendSegment(".intermediate")
                .appendSegment(URIHelper.baseName(dispatchingIFile));

        boolean criticalPath = configuration.getAttribute(CRITICAL_PATH, CRITICAL_PATH_DEFAULT);
        boolean stochasticImpact = configuration.getAttribute(STOCHASTIC_IMPACT, STOCHASTIC_IMPACT_DEFAULT);
        boolean ganttChart = criticalPath || stochasticImpact
                || configuration.getAttribute(GANTT_CHART, GANTT_CHART_DEFAULT);
        boolean paperscriptAnimation = configuration.getAttribute(PAPERSCRIPT_ANIMATION, PAPERSCRIPT_ANIMATION_DEFAULT);
        boolean removeClaimReleaseDependencies = configuration.getAttribute(REMOVE_CLAIMS_RELEASES_DEPENDENCIES,
                REMOVE_CLAIMS_RELEASES_DEPENDENCIES_DEFAULT);

        int prepareSchedulingTicks = 10;
        int generateGraphFromActivityDispatchingTicks = 30;
        int shortcutClaimReleaseTicks = 2;
        int refineGraphWithExecutionTimesTicks = 18;
        int scheduleGraphOnResourcesTicks = 10;
        int visualizeResourceClaimsTicks = 20;
        int generateGanttChartTicks = 10;
        int generateAnimationsTicks = 20;
        int stochasticImpactTicks = 50;
        int totalTicks = prepareSchedulingTicks + generateGraphFromActivityDispatchingTicks
                + refineGraphWithExecutionTimesTicks + scheduleGraphOnResourcesTicks + visualizeResourceClaimsTicks;
        if (ganttChart) {
            totalTicks += generateGanttChartTicks;
        }
        if (paperscriptAnimation) {
            totalTicks += generateAnimationsTicks;
        }
        if (stochasticImpact) {
            totalTicks += stochasticImpactTicks;
        }

        monitor.beginTask("Timing Analysis", totalTicks);
        try {
            if (URIHelper.asFile(saveIFolder).mkdirs()) {
                dispatchingIFile.getProject().refreshLocal(IFile.DEPTH_INFINITE, monitor);
            }

            monitor.subTask("Preparing specification for scheduling");
            Persistor<ActivityDispatching> dispatchingPersistor = persistorFactory
                    .getPersistor(ActivityDispatching.class);
            ActivityDispatching activityDispatching = dispatchingPersistor.loadOne(URIHelper.asURI(dispatchingIFile));
            monitor.subTask("Validating dispatching file");
            validate(activityDispatching, dispatchingIFile.getName());

            String settingFilePath = configuration.getAttribute(SETTING_IFILE, SETTING_IFILE_DEFAULT);
            IFile settingsIFile = (!SETTING_IFILE_DEFAULT.equals(settingFilePath))
                    ? ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(settingFilePath)) : null;
            Settings settings = SettingUtil.getSettings(activityDispatching.eResource(), settingsIFile);
            monitor.subTask("Validating settings file");
            validate(settings, settings.eResource().getURI().lastSegment());

            // Make sure all resources are loaded before processing
            EcoreUtil.resolveAll(persistorFactory.getResourceSet());

            // flatten the import containers to a single root per type and grab them again
            ResourceSet resourceSet = ImportsFlattener.flatten(intermediateURI, activityDispatching, settings);
            activityDispatching = ResourceSetUtil.getObjectByType(resourceSet, ActivityDispatching.class);

            settings = ResourceSetUtil.getObjectByType(resourceSet, Settings.class);
            // important the next line must be done before expanding the activities!!
            Event2Resource.surroundEventsWithClaimRelease(ResourceSetUtil.getObjectByType(resourceSet, ActivitySet.class));

            DispatchingUtil.expand(activityDispatching);
            DispatchingUtil.removeUnusedActivities(activityDispatching);

            if (runningInDebugMode) {
                ResourceSetUtil.saveResources(resourceSet, Collections.emptyMap());
                IFolder intermediateFolder = saveIFolder.getFolder(".intermediate");
                intermediateFolder.setHidden(true);
            }

            if (monitor.isCanceled()) {
                return;
            }
            monitor.worked(prepareSchedulingTicks);

            monitor.subTask("Generating graph from activity dispatching");
            TaskDependencyGraph<Task> graph = generateGraphFromActivityDispatching(activityDispatching);

            if (monitor.isCanceled()) {
                return;
            }
            monitor.worked(generateGraphFromActivityDispatchingTicks);

            monitor.subTask("Shortcut claim release");
            graph = annotateClaimRelease(graph);
            if (monitor.isCanceled()) {
                return;
            }
            monitor.worked(shortcutClaimReleaseTicks);

            monitor.subTask("Refining graph with execution times");
            graph = refineGraphWithExecutionTimes(graph, settings);
            if (monitor.isCanceled()) {
                return;
            }
            monitor.worked(refineGraphWithExecutionTimesTicks);

            monitor.subTask("Scheduling graph on resources");
            Schedule<Task> schedule = scheduleGraphOnResources(graph, removeClaimReleaseDependencies);
            if (monitor.isCanceled()) {
                return;
            }
            monitor.worked(scheduleGraphOnResourcesTicks);

            if (criticalPath) {
                monitor.subTask("Performing critical path analysis");
                schedule = criticalPathAnalysis(schedule);
            }

            if (stochasticImpact) {
                monitor.subTask("Stochastic impact analysis");
                schedule = stochasticImpactAnalysis(schedule, settings);
            }

            monitor.subTask("Analyzing concatenated moves");
            schedule = analyzeConcatenatedMoves(schedule);

            monitor.subTask("Visualizing resource claims");
            schedule = visualizeResourceClaims(schedule); // add filler bars between claim and corresponding release

            monitor.subTask("Visualizing events ");
            schedule = visualizeEvents(schedule); // add filler bars between awaiting and raise

            monitor.subTask("Visualizing resource claims");
            schedule = mergeClaims(schedule); // replication of filler bars between claims and releases for all related
                                              // resources
            if (monitor.isCanceled()) {
                return;
            }
            monitor.worked(visualizeResourceClaimsTicks);

            OpenEditor<TraceView> traceEditor = null;
            if (ganttChart) {
                monitor.subTask("Generating gantt chart");
                traceEditor = generateGanttChart(schedule, criticalPath, stochasticImpact);
                if (!traceEditor.getStatus().isOK()) {
                    throw new CoreException(traceEditor.getStatus());
                }
                monitor.worked(generateGanttChartTicks);
            }

            OpenEditor<AnimationView> animationEditor = null;
            if (paperscriptAnimation) {
                monitor.subTask("Generating Animations");
                animationEditor = generateAnimations(schedule);
                if (!animationEditor.getStatus().isOK()) {
                    throw new CoreException(animationEditor.getStatus());
                }
                monitor.worked(generateAnimationsTicks);
            }

            if (ganttChart && paperscriptAnimation) {
                linkTraceAndAnimations(traceEditor, animationEditor);
            }
        } catch (CoreException ce) {
            throw new CoreException(new Status(ce.getStatus().getSeverity(), Activator.PLUGIN_ID, ce.getMessage(), ce));
        } catch (Exception e) {
            throw new CoreException(new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
        } finally {
            // Update the workspace
            dispatchingIFile.getProject().refreshLocal(IFile.DEPTH_INFINITE, monitor);
            monitor.done();
        }
    }

    private TaskDependencyGraph<Task> generateGraphFromActivityDispatching(ActivityDispatching activityDispatching)
            throws QvtoTransformationException, CoreException, IOException
    {
        Dispatching2Graph<Task> d2g = new Dispatching2Graph<Task>();

        Dispatching2GraphOutput<Task> d2gResult = d2g.transformModel(activityDispatching, monitor);
        TaskDependencyGraph<Task> taskDependencyGraph = d2gResult.getTaskDependencyGraph();

        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(d2gResult.getResourceModel(), taskDependencyGraph, null);
        }

        taskDependencyGraph = removeEvents(taskDependencyGraph);

        CleanupGraph<Task> cleanupGraph = new CleanupGraph<Task>(false, RemoveClaimReleaseStrategy.KeepAll);
        TaskDependencyGraph<Task> graph = cleanupGraph.transformModel(taskDependencyGraph, monitor);

        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(graph.getResourceModel(), graph, null);
        }

        return graph;
    }

    private TaskDependencyGraph<Task> removeEvents(TaskDependencyGraph<Task> taskDependencyGraph)
            throws CoreException, IOException
    {
        taskDependencyGraph = RemoveEvents.transformModel(taskDependencyGraph);
        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(taskDependencyGraph.getResourceModel(), taskDependencyGraph, null);
        }
        return taskDependencyGraph;
    }

    private TaskDependencyGraph<Task> annotateClaimRelease(TaskDependencyGraph<Task> graph)
            throws IOException, MotionException, CoreException, SpecificationException
    {
        graph = AnnotateClaimRelease.transformModel(graph);
        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(graph.getResourceModel(), graph, null);
        }

        return graph;
    }

    private TaskDependencyGraph<Task> refineGraphWithExecutionTimes(TaskDependencyGraph<Task> graph, Settings settings)
            throws IOException, MotionException, CoreException, SpecificationException
    {
        MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
        AddExecutionTimes addExecutionTimes = new AddExecutionTimes(new TimingCalculator(settings, motionCalculator));
        graph = addExecutionTimes.transformModel(graph);

        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(graph.getResourceModel(), graph, null);
        }

        return graph;
    }

    private Schedule<Task> scheduleGraphOnResources(TaskDependencyGraph<Task> graph,
            boolean performCriticalPathAnalysisNoClaims) throws Exception
    {
        AsapScheduler<Task> scheduler = new AsapScheduler<Task>();

        Schedule<Task> schedule;
        try {
            schedule = scheduler.createSchedule(graph);
        } catch (CycleFoundException e) {
            throw new Exception("One or more activities contain cycles.", e);
        }
        schedule.setName(saveBaseURI.lastSegment());
        schedule = ALAPScheduler.applyALAPScheduling(schedule);

        // Scheduling modifies the graph, so save it again
        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(schedule.getResourceModel(), graph, schedule);
        }

        CleanupSchedule<Task> cleanupSchedule = new CleanupSchedule<Task>(performCriticalPathAnalysisNoClaims
                ? CleanupSchedule.ClaimReleaseStrategy.RemoveDependencies : CleanupSchedule.ClaimReleaseStrategy.Keep);
        schedule = cleanupSchedule.transformModel(schedule, monitor);

        // Scheduling modifies the graph, so save it again
        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(schedule.getResourceModel(), null, schedule);
        }

        return (schedule);
    }

    private Schedule<Task> criticalPathAnalysis(Schedule<Task> schedule)
            throws QvtoTransformationException, CoreException, IOException
    {
        CriticalPathAnalysis<Task> criticalPathAnalysis = new CriticalPathAnalysis<Task>();
        criticalPathAnalysis.transformModel(schedule, monitor);

        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(null, null, schedule);
        }
        return schedule;
    }

    private Schedule<Task> stochasticImpactAnalysis(Schedule<Task> schedule, Settings settings)
            throws QvtoTransformationException, CoreException, IOException
    {
        StochasticImpactAnalysis<Task> stochasticImpactAnalysis = new StochasticImpactAnalysis<Task>();
        stochasticImpactAnalysis.transformModel(new StochasticImpactAnalysisInput<Task>(schedule, settings), monitor);

        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(null, null, schedule);
        }
        return schedule;
    }

    private Schedule<Task> analyzeConcatenatedMoves(Schedule<Task> schedule)
            throws QvtoTransformationException, CoreException, IOException
    {
        ConcatenatedMoveAnalysis<Task> concatenatedMoveAnalysis = new ConcatenatedMoveAnalysis<Task>();
        Collection<Collection<Move>> erroneousMoves = concatenatedMoveAnalysis.getErroneousPassingMoves(schedule);

        if (!erroneousMoves.isEmpty()) {
            List<Boolean> confirmed = new ArrayList<>();
            PlatformUI.getWorkbench().getDisplay().syncExec(() -> {
                confirmed.add(MessageDialog.openQuestion(
                        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                        "Parallel actions interrupt concatenated move [" + erroneousMoves.stream()
                                .map(c -> c.stream().map(Move::getName).collect(Collectors.joining("->"))).distinct()
                                .collect(Collectors.joining(",")) + "]",
                        "Would you like to analyse the erroneous moves by coloring them orange in the Gantt chart?"));
            });
            if (confirmed.get(0)) {
                concatenatedMoveAnalysis.annotateErroneousPassingMoves(schedule);
            }
        }
        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(null, null, schedule);
        }
        return schedule;
    }

    private Schedule<Task> visualizeResourceClaims(Schedule<Task> schedule)
            throws QvtoTransformationException, CoreException, IOException
    {
        VisualizeClaimedBy<Task> visualizeClaimedBy = new VisualizeClaimedBy<Task>();
        visualizeClaimedBy.transformModel(schedule, monitor);

        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(null, null, schedule);
        }
        return schedule;
    }

    private Schedule<Task> visualizeEvents(Schedule<Task> schedule)
            throws QvtoTransformationException, CoreException, IOException
    {
        VisualizeEvents<Task> visualizeClaimedBy = new VisualizeEvents<Task>();
        visualizeClaimedBy.transformModel(schedule, monitor);

        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(null, null, schedule);
        }
        return schedule;
    }

    private Schedule<Task> mergeClaims(Schedule<Task> schedule)
            throws QvtoTransformationException, CoreException, IOException
    {
        MergeClaimedBy<Task> mergeClaimedBy = new MergeClaimedBy<Task>();
        mergeClaimedBy.transformModel(schedule, monitor);

        // This might also modify the resources, so save it again
        if (runningInDebugMode) {
            persistorSaveResourcesGraphSchedule(schedule.getResourceModel(), null, schedule);
        }

        return schedule;
    }

    private OpenEditor<TraceView> generateGanttChart(Schedule<Task> schedule, boolean criticalPath,
            boolean stochasticImpact) throws IOException, CoreException
    {
        String traceFileName = saveBaseURI.lastSegment();
        if (criticalPath) {
            traceFileName += "-critical";
        }
        if (stochasticImpact) {
            traceFileName += "-stochastic";
        }
        GenerateAll etfGenerator = new GenerateAll(schedule, saveIFolder, criticalPath, stochasticImpact,
                traceFileName);
        IFile traceFile = etfGenerator.generate(monitor);

        saveIFolder.refreshLocal(IFile.DEPTH_ONE, monitor);
        return openTrace(traceFile);
    }

    private OpenEditor<AnimationView> generateAnimations(Schedule<Task> schedule) throws IOException, CoreException {
        IFolder animatedIFolder = saveIFolder.getFolder("animated");
        File animatedFolder = URIHelper.asFile(animatedIFolder);

        if (animatedFolder.mkdirs()) { // animated folder did not exist yet, generate skeletons
            // creating paper.js requires the workspace to be up-to-date
            saveIFolder.refreshLocal(IFile.DEPTH_ONE, monitor);

            Machine machine = getMachine();

            IFile machineJsFile = animatedIFolder.getFile("machine.js");
            createIFile(machineJsFile, GenerateMachineTemplate.generateMachineJavaScript(machine), monitor);

            IFile scheduleHTML = animatedIFolder.getFile("schedule.html");
            createIFile(scheduleHTML, GenerateScheduleTemplate.generateScheduleHtml(machine), monitor);

            Settings settings = SettingUtil.getSettings(machine.eResource());
            IFile settingsJsFile = animatedIFolder.getFile("settings.js");
            createIFile(settingsJsFile, GenerateSettingsTemplate.generateSettingsJavaScript(settings), monitor);
        }

        IFile scheduleJs = animatedIFolder.getFile("schedule.js");
        if (scheduleJs.exists()) {
            scheduleJs.delete(true, monitor);
        }
        createIFile(scheduleJs, GenerateScheduleTemplate.generateScheduleJavaScript(schedule), monitor);

        animatedIFolder.refreshLocal(IFile.DEPTH_ONE, monitor);
        return openAnimation(animatedIFolder.getFile("schedule.html"));
    }

    private void createIFile(IFile file, CharSequence content, IProgressMonitor monitor)
            throws UnsupportedEncodingException, CoreException
    {
        final byte[] bytes = content.toString().getBytes(file.getCharset(true));
        file.create(new ByteArrayInputStream(bytes), true, monitor);
    }

    private Machine getMachine() throws CoreException {
        for (Resource resource: persistorFactory.getResourceSet().getResources()) {
            for (EObject eObject: resource.getContents()) {
                if (eObject instanceof Machine) {
                    return (Machine)eObject;
                }
            }
        }
        throw new CoreException(
                new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Dispatching does not refer to any machine"));
    }

    private void linkTraceAndAnimations(OpenEditor<TraceView> traceEditor, OpenEditor<AnimationView> animationEditor) {
        animationEditor.getEditor().linkTraceEditor(traceEditor.getEditor());
    }

    private void persistorSaveResourcesGraphSchedule(ResourceModel resourceModel, TaskDependencyGraph<Task> graph,
            Schedule<Task> schedule) throws CoreException, IOException
    {
        Persistor<EObject> savePersistor = persistorFactory.getPersistor(EObject.class);

        if (resourceModel != null) {
            savePersistor.save(saveBaseURI.appendFileExtension("resources"), resourceModel);
        }
        if (graph != null) {
            savePersistor.save(saveBaseURI.appendFileExtension("graph"), graph);
            Graph2GraphML.transform(saveBaseURI.appendFileExtension("graph"),
                    saveBaseURI.appendFileExtension("graphml"));
        }
        if (schedule != null) {
            savePersistor.save(saveBaseURI.appendFileExtension("schedule"), schedule);
            Schedule2GraphML.transform(saveBaseURI.appendFileExtension("schedule"),
                    saveBaseURI.appendFileExtension("schedule.graphml"));
        }
    }

    private void validate(EObject object, String fileName) throws IOException {
        if (object.eResource() instanceof XtextResource) {
            IResourceValidator validator = ((XtextResource)object.eResource()).getResourceServiceProvider()
                    .getResourceValidator();
            List<Issue> issues = validator.validate(object.eResource(), CheckMode.ALL, CancelIndicator.NullImpl)
                    .stream().filter(i -> i.getSeverity() == Severity.ERROR).collect(Collectors.toList());

            // Diagnostic diagnostics = Diagnostician.INSTANCE.validate(object );
            if (!issues.isEmpty()) {
                String msg = "\n'" + fileName + "' file is invalid:\n\n" + issues.stream()
                        .map(i -> "  " + BULLET + ' ' + i.getMessage()).collect(Collectors.joining("\n"));
                throw new IllegalArgumentException(msg);
            }
        }
        if (object instanceof ImportContainer) {
            ImportContainer container = (ImportContainer)object;
            for (Import i: container.getImports()) {
                for (EObject o: i.load()) {
                    validate(o, i.getImportURI());
                }
            }
        }
    }

    private OpenEditor<TraceView> openTrace(IFile trace) {
        OpenEditor<TraceView> openEditor = new OpenEditor<>(trace, TraceView.VIEW_ID, true);
        Display.getDefault().syncExec(openEditor);
        return openEditor;
    }

    private OpenEditor<AnimationView> openAnimation(IFile animation) {
        OpenEditor<AnimationView> openEditor = new OpenEditor<AnimationView>(animation, AnimationView.class.getName());
        Display.getDefault().syncExec(openEditor);
        return openEditor;
    }

    private static final class OpenEditor<T extends IEditorPart> implements Runnable {
        private final IFile file;

        private final String editorId;

        private final boolean reOpen;

        private T editor;

        private IStatus status;

        private OpenEditor(IFile file, String editorId) {
            this(file, editorId, false);
        }

        private OpenEditor(IFile file, String editorId, boolean reOpen) {
            this.file = file;
            this.editorId = editorId;
            this.reOpen = reOpen;
        }

        public IStatus getStatus() {
            return status;
        }

        public T getEditor() {
            return editor;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void run() {
            try {
                IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                FileEditorInput input = new FileEditorInput(file);
                if (reOpen) {
                    IEditorPart editorToClose = activePage.findEditor(input);
                    if (editorToClose != null) {
                        activePage.closeEditor(editorToClose, false);
                    }
                }
                editor = (T)IDE.openEditor(activePage, input, editorId);
                status = Status.OK_STATUS;
            } catch (CoreException e) {
                status = new Status(e.getStatus().getSeverity(), Activator.PLUGIN_ID, e.getMessage(), e);
            } catch (Exception e) {
                status = new Status(Status.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
            }
        }
    }
}
