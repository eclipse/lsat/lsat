/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timinganalysis.ui.launch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.lsat.timinganalysis.ui.Activator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class TimingAnalysisLaunchMainTab extends AbstractLaunchConfigurationTab
        implements TimingAnalysisLaunchAttributes
{
    @SuppressWarnings("unused")
    private DataBindingContext m_bindingContext;

    private final SelectionListener checkbox_selection_listener = new SelectionAdapter() {
        @Override
        public void widgetSelected(SelectionEvent e) {
            scheduleUpdateJob();
        }
    };

    private Text textModelIFile;

    private Text textSettingIFile;

    private Button btnPaperscript;

    private Group grpGanttType;

    private Button btnGantt;

    private Button btnCriticalPathGantt;

    private Button btnNoGantt;

    private Button btnRemoveClaimReleaseDependencies;

    private Button btnStochasticImpact;

    public TimingAnalysisLaunchMainTab() {
    }

    @Override
    public String getName() {
        return "Timing Analysis";
    }

    /**
     * @wbp.parser.entryPoint
     */
    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        setControl(container);
        GridLayout gl_container = new GridLayout();
        container.setLayout(gl_container);
        container.setLayoutData(new GridData(GridData.FILL_BOTH));

        textModelIFile = createFileDialog(container, "Activity Dispatching", "dispatching");
        textSettingIFile = createFileDialog(container, "Physical settings file", "setting");

        grpGanttType = new Group(container, SWT.NONE);
        grpGanttType.setLayout(new GridLayout(1, false));
        grpGanttType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        grpGanttType.setText("Show");

        btnNoGantt = new Button(grpGanttType, SWT.RADIO);
        btnNoGantt.setText("no Gantt chart");
        btnNoGantt.addSelectionListener(checkbox_selection_listener);

        btnGantt = new Button(grpGanttType, SWT.RADIO);
        btnGantt.addSelectionListener(checkbox_selection_listener);
        btnGantt.setText("Gantt chart");

        btnCriticalPathGantt = new Button(grpGanttType, SWT.RADIO);
        btnCriticalPathGantt.setText("Gantt chart with critical path");
        btnCriticalPathGantt.addSelectionListener(checkbox_selection_listener);

        btnStochasticImpact = new Button(grpGanttType, SWT.RADIO);
        btnStochasticImpact.setText("Gantt chart using stochastic impact analysis");
        btnStochasticImpact.addSelectionListener(checkbox_selection_listener);

        Group grpOptions = new Group(container, SWT.NONE);
        grpOptions.setLayout(new GridLayout(1, false));
        grpOptions.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        grpOptions.setText("Options");

        btnPaperscript = new Button(grpOptions, SWT.CHECK);
        btnPaperscript.setText("Show web animation");
        btnPaperscript.addSelectionListener(checkbox_selection_listener);

        btnRemoveClaimReleaseDependencies = new Button(grpOptions, SWT.CHECK);
        btnRemoveClaimReleaseDependencies.setText("Remove dependencies for claims and releases");
        btnRemoveClaimReleaseDependencies.addSelectionListener(checkbox_selection_listener);

        m_bindingContext = initDataBindings();
    }

    @Override
    public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
    }

    @Override
    public void initializeFrom(ILaunchConfiguration configuration) {
        try {
            textModelIFile.setText(configuration.getAttribute(MODEL_IFILE, MODEL_IFILE_DEFAULT));
            textSettingIFile.setText(configuration.getAttribute(SETTING_IFILE, SETTING_IFILE_DEFAULT));
            btnNoGantt.setSelection(configuration.getAttribute(NO_GANTT_CHART, NO_GANTT_CHART_DEFAULT));
            btnGantt.setSelection(configuration.getAttribute(GANTT_CHART, GANTT_CHART_DEFAULT));
            btnCriticalPathGantt.setSelection(configuration.getAttribute(CRITICAL_PATH, CRITICAL_PATH_DEFAULT));
            btnStochasticImpact.setSelection(configuration.getAttribute(STOCHASTIC_IMPACT, STOCHASTIC_IMPACT_DEFAULT));
            btnPaperscript
                    .setSelection(configuration.getAttribute(PAPERSCRIPT_ANIMATION, PAPERSCRIPT_ANIMATION_DEFAULT));
            btnRemoveClaimReleaseDependencies.setSelection(configuration
                    .getAttribute(REMOVE_CLAIMS_RELEASES_DEPENDENCIES, REMOVE_CLAIMS_RELEASES_DEPENDENCIES_DEFAULT));
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void performApply(ILaunchConfigurationWorkingCopy configuration) {
        IFile modelIFile = getSelectedIFile(textModelIFile);
        List<IResource> mappedResources = new ArrayList<>();
        if (null != modelIFile) {
            configuration.setAttribute(MODEL_IFILE, modelIFile.getFullPath().toString());
            mappedResources.add(modelIFile);
        }

        IFile settingIFile = getSelectedIFile(textSettingIFile);
        if (null != settingIFile) {
            configuration.setAttribute(SETTING_IFILE, settingIFile.getFullPath().toString());
            mappedResources.add(settingIFile);
        }
        configuration.setMappedResources(mappedResources.toArray(new IResource[mappedResources.size()]));

        configuration.setAttribute(NO_GANTT_CHART, btnNoGantt.getSelection());
        configuration.setAttribute(GANTT_CHART, btnGantt.getSelection());
        configuration.setAttribute(CRITICAL_PATH, btnCriticalPathGantt.getSelection());
        configuration.setAttribute(STOCHASTIC_IMPACT, btnStochasticImpact.getSelection());
        configuration.setAttribute(PAPERSCRIPT_ANIMATION, btnPaperscript.getSelection());
        configuration.setAttribute(REMOVE_CLAIMS_RELEASES_DEPENDENCIES,
                btnRemoveClaimReleaseDependencies.getSelection());
    }

    @Override
    public boolean isValid(ILaunchConfiguration launchConfig) {
        setErrorMessage(null);
        setWarningMessage(null);
        if (null == getSelectedIFile(textModelIFile)) {
            setErrorMessage("Please select an existing activity dispatching file");
            return false;
        }
        if (null == getSelectedIFile(textSettingIFile)) {
            setWarningMessage("Physical settings file not selected. The project default will be used");
            return true;
        }
        String preferredName = getPreferredName();
        List<String> possibleNames = new ArrayList<>(
                Arrays.asList(getPreferredName(TimingAnalysisLaunchAttributes.GANTT_CHART),
                        getPreferredName(TimingAnalysisLaunchAttributes.CRITICAL_PATH),
                        getPreferredName(TimingAnalysisLaunchAttributes.STOCHASTIC_IMPACT),
                        getPreferredName(TimingAnalysisLaunchAttributes.NO_GANTT_CHART)));
        possibleNames.remove(getPreferredName());

        if (possibleNames.contains(launchConfig.getName())) {
            setWarningMessage("Reserved name, consider change to: " + preferredName);
            return true;
        }
        return true;
    }

    private Text createFileDialog(Composite container, String title, String fileExtension) {
        Group grpActivityDispatching = new Group(container, SWT.NONE);
        grpActivityDispatching.setLayout(new GridLayout(2, false));
        grpActivityDispatching.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        grpActivityDispatching.setText(title);

        Text textField = new Text(grpActivityDispatching, SWT.BORDER);
        textField.setEditable(false);
        textField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        Button btnBrowseMdl = new Button(grpActivityDispatching, SWT.NONE);
        btnBrowseMdl.setText("Browse...");
        btnBrowseMdl.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                IFile iFile = selectFile(fileExtension, textField);
                if (null != iFile) {
                    textField.setText(iFile.getFullPath().toString());
                    scheduleUpdateJob();
                }
            }
        });
        return textField;
    }

    private String getPreferredName(String showTargetName) {
        return TimingAnalysisUtil.getLaunchName(getSelectedIFile(textModelIFile), getSelectedIFile(textSettingIFile),
                showTargetName);
    }

    private String getPreferredName() {
        if (btnPaperscript.getSelection() || btnRemoveClaimReleaseDependencies.getSelection()) {
            // free name allowed
            return null;
        }
        return TimingAnalysisUtil.getLaunchName(getSelectedIFile(textModelIFile), getSelectedIFile(textSettingIFile),
                getSelectedShow());
    }

    private String getSelectedShow() {
        if (btnGantt.getSelection()) {
            return GANTT_CHART;
        }
        if (btnCriticalPathGantt.getSelection()) {
            return CRITICAL_PATH;
        }
        if (btnStochasticImpact.getSelection()) {
            return STOCHASTIC_IMPACT;
        }
        return NO_GANTT_CHART;
    }

    private IFile selectFile(String fileExtension, Text field) {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(getShell(), new WorkbenchLabelProvider(),
                new BaseWorkbenchContentProvider());
        dialog.setTitle("Select " + fileExtension + " file");
        dialog.setMessage("Select " + fileExtension + " file to execute scheduling");
        dialog.setAllowMultiple(false);
        dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
        // filter for specific files and elements only
        dialog.addFilter(new ViewerFilter() {
            @Override
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                if (element instanceof IFile) {
                    return Objects.equals(((IFile)element).getFileExtension(), fileExtension);
                }
                return element instanceof IContainer;
            }
        });
        dialog.setValidator(new ISelectionStatusValidator() {
            @Override
            public IStatus validate(Object[] selection) {
                if (selection.length <= 0) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0,
                            "Please select a " + fileExtension + " file!", null);
                }
                return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, "OK", null);
            }
        });
        IFile selectedFile = getSelectedIFile(field);
        if (null != selectedFile) {
            dialog.setInitialSelection(selectedFile);
        }
        dialog.open();
        return (IFile)dialog.getFirstResult();
    }

    private static IFile getSelectedIFile(Text textField) {
        IWorkspaceRoot wsRoot = ResourcesPlugin.getWorkspace().getRoot();
        IPath modelIPath = new Path(textField.getText());
        if (!modelIPath.isEmpty() && wsRoot.exists(modelIPath)) {
            return wsRoot.getFile(modelIPath);
        }
        return null;
    }

    protected DataBindingContext initDataBindings() {
        DataBindingContext bindingContext = new DataBindingContext();
        //
        return bindingContext;
    }
}
