/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timinganalysis.ui.launch;

import java.util.stream.Stream;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

public class TimingAnalysisLaunchShortcut implements ILaunchShortcut, TimingAnalysisLaunchAttributes {
    private static final String SETTING = "setting";

    private static final String DISPATCHING = "dispatching";

    private final String chartType;

    public static class GanttChart extends TimingAnalysisLaunchShortcut {
        public GanttChart() {
            super(TimingAnalysisLaunchAttributes.GANTT_CHART);
        }
    }

    public static class CriticalPath extends TimingAnalysisLaunchShortcut {
        public CriticalPath() {
            super(TimingAnalysisLaunchAttributes.CRITICAL_PATH);
        }
    }

    public static class StochasticImpact extends TimingAnalysisLaunchShortcut {
        public StochasticImpact() {
            super(TimingAnalysisLaunchAttributes.STOCHASTIC_IMPACT);
        }
    }

    private TimingAnalysisLaunchShortcut(String chartType) {
        this.chartType = chartType;
    }

    @Override
    public void launch(ISelection selection, String mode) {
        if (selection instanceof IStructuredSelection) {
            IFile[] files = Stream.of(((IStructuredSelection)selection).toArray()).map(f -> (IFile)f)
                    .toArray(IFile[]::new);
            doLaunch(files, mode);
        }
    }

    @Override
    public void launch(IEditorPart editor, String mode) {
        if (editor.getEditorInput() instanceof FileEditorInput) {
            IFile file = ((FileEditorInput)editor.getEditorInput()).getFile();
            doLaunch(new IFile[] {file}, mode);
        }
    }

    protected void doLaunch(IFile[] files, String mode) {
        if (checkValid(files)) {
            TimingAnalysisLaunchJob job = new TimingAnalysisLaunchJob(files, mode, chartType);
            job.setUser(true);
            job.schedule();
        }
    }

    private static boolean checkValid(IFile[] files) {
        if (files.length == 1) {
            return true;
        }
        if (files.length == 2) {
            // eligible if the selection contains one dispatching and one setting file.
            if (1 == Stream.of(files).filter(f -> DISPATCHING.equals(f.getFileExtension())).count()
                    && 1 == Stream.of(files).filter(f -> SETTING.equals(f.getFileExtension())).count())
                return true;
        }
        informUser();
        return false;
    }

    private static void informUser() {
        Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
        MessageDialog.openError(shell, "Wrong selection for timing analysis",
                "Please select one " + DISPATCHING + " and/or one " + SETTING + " file");
    }
}
