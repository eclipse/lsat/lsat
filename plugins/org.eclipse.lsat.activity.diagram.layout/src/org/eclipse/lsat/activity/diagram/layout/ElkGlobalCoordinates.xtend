/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout

import java.awt.geom.Point2D
import java.awt.geom.Rectangle2D
import org.eclipse.elk.graph.ElkEdgeSection
import org.eclipse.elk.graph.ElkGraphElement
import org.eclipse.elk.graph.ElkLabel
import org.eclipse.elk.graph.ElkNode
import org.eclipse.elk.graph.ElkPort
import org.eclipse.elk.graph.ElkShape

class ElkGlobalCoordinates {
    private new () {
        // Empty for utilities
    }

    static def Point2D.Double getGlobalLocation(ElkShape shape) {
        val parent = shape.parent
        val anchor = if (parent instanceof ElkShape) {
            parent.globalLocation
        } else {
            new Point2D.Double()
        }
        return anchor.translate(shape.x, shape.y)
    }
    
    static def Rectangle2D.Double getGlobalBounds(ElkShape shape) {
        val globalLocation = shape.globalLocation
        return new Rectangle2D.Double(globalLocation.x, globalLocation.y, shape.width, shape.height)
    }
    
    static def void setGlobalLocation(ElkShape shape, double x, double y) {
        val parent = shape.parent
        if (parent instanceof ElkShape) {
            val parentLocation = parent.globalLocation
            shape.setLocation(x - parentLocation.x, y - parentLocation.y)
        } else {
            shape.setLocation(x, y)
        }
    }
    
    static def Point2D.Double getGlobalStartLocation(ElkEdgeSection section) {
        val container = section.parent.containingNode
        return if (container === null) {
            new Point2D.Double(section.startX, section.startY)
        } else {
            container.globalLocation.translate(section.startX, section.startY)
        }
    }

    static def void setGlobalStartLocation(ElkEdgeSection section, double x, double y) {
        val container = section.parent.containingNode
        if (container === null) {
            section.setStartLocation(x, y)
        } else {
            val containerLocation = container.globalLocation
            section.setStartLocation(x - containerLocation.x, y - containerLocation.y)
        }
    }

    static def Point2D.Double getGlobalEndLocation(ElkEdgeSection section) {
        val container = section.parent.containingNode
        return if (container === null) {
            new Point2D.Double(section.endX, section.endY)
        } else {
            container.globalLocation.translate(section.endX, section.endY)
        }
    }

    static def void setGlobalEndLocation(ElkEdgeSection section, double x, double y) {
        val container = section.parent.containingNode
        if (container === null) {
            section.setEndLocation(x, y)
        } else {
            val containerLocation = container.globalLocation
            section.setEndLocation(x - containerLocation.x, y - containerLocation.y)
        }
    }

    private static def ElkGraphElement getParent(ElkShape shape) {
        return switch it: shape {
            ElkNode: parent
            ElkPort: parent
            ElkLabel: parent
            default: null
        }
    }

    static def Point2D.Double translate(Point2D.Double point, double deltaX, double deltaY) {
        point.x = point.x + deltaX
        point.y = point.y + deltaY
        return point
    }

    static def Rectangle2D.Double translate(Rectangle2D.Double bounds, double deltaX, double deltaY) {
        bounds.x = bounds.x + deltaX
        bounds.y = bounds.y + deltaY
        return bounds
    }
}