/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout;

import activity.ActivityPackage
import java.util.Collections
import machine.MachinePackage
import org.eclipse.elk.core.AbstractLayoutProvider
import org.eclipse.elk.core.math.ElkPadding
import org.eclipse.elk.core.options.CoreOptions
import org.eclipse.elk.core.util.IElkProgressMonitor
import org.eclipse.elk.graph.ElkConnectableShape
import org.eclipse.elk.graph.ElkEdge
import org.eclipse.elk.graph.ElkGraphElement
import org.eclipse.elk.graph.ElkNode
import org.eclipse.elk.graph.properties.IPropertyHolder
import org.eclipse.emf.ecore.EClass
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static extension org.eclipse.lsat.activity.diagram.layout.ElkGlobalCoordinates.*
import static extension org.eclipse.lsat.common.xtend.Queries.*

class ActivityDiagramLayout {
    static class Provider extends AbstractLayoutProvider {
        override getDefaultLayoutAlgorithmID() {
            return ActivityDiagramLayoutOptions.ALGORITHM_ID
        }

        override layout(ElkNode layoutGraph, IElkProgressMonitor progressMonitor) {
            progressMonitor.begin("Activity Diagram Layout", 1)
            new ActivityDiagramLayout().layout(layoutGraph)
            progressMonitor.done()
        }
    }

    static val Logger LOGGER = LoggerFactory.getLogger(ActivityDiagramLayout)

    static val rowHeight = 60.0d
    static val actionSpacingX = 20.0d
    static val actionWidth = 140.0d
    static val actionHeight = 50.0d

    static val syncBarHeight = 10.0d
    static val syncBarSpacingY = (actionHeight - syncBarHeight) / 2.0d
    
    static val resourceSpacingX = actionWidth * 0.25d
    static val resourcePaddingY = 11.0d // used to provide spacing for resource name
    static val claimReleaseSpacingX = 10.0d

    static val peripheralPaddingX = 2.0
    static val peripheralPaddingY = 28.0 // used to provide spacing for resource name
    static val peripheralWidth = actionWidth + (actionSpacingX * 2)
    
    static val eventSpacingX = resourceSpacingX
    static val eventWidth = actionWidth
    static val eventHeight = actionHeight

//-------------------------------------------
//      Main entry point
//-------------------------------------------
    def private layout(ElkNode layoutGraph) {
        if (layoutGraph.isActivity) {
            val canvasMinX = 0.0d
            var canvasMaxX = layoutGraph.layoutResources(canvasMinX)
            canvasMaxX = layoutGraph.layoutEvents(canvasMaxX + eventSpacingX)
            layoutGraph.layoutSyncBars(canvasMinX, canvasMaxX)
            layoutGraph.layoutEdges()
        }
    }

//-------------------------------------------
//      Layout everything rectangular
//-------------------------------------------
    def private layoutResources(ElkNode topNode, double startX) {
        var resourceX = startX
        for (resourceNode : topNode.children.filter[isResource]) {
            // determine the row for the claim and release, as it will determine height of resource
            // ports = sirius node that sits on the edge of an other element
            // NB all nodes and ports already known their layout row
            var claimRowNumber = resourceNode.ports.filter[isClaim].map[layoutRow].max(0)
            var releaseRowNumber = resourceNode.ports.filter[isRelease].map[layoutRow].max(0)
            var peripheralHeight = ((releaseRowNumber - claimRowNumber) * rowHeight)
            var peripheralsWidth = resourceNode.children.size * (peripheralWidth + peripheralPaddingX)
            resourceNode.setGlobalLocation(resourceX, claimRowNumber.toGlobalY)
            resourceNode.setDimensions(peripheralsWidth, peripheralHeight + resourcePaddingY)

            resourceNode.layoutPeripherals(peripheralHeight)
            resourceNode.layoutClaimsAndReleases

            resourceX += resourceNode.width + resourceSpacingX
        }
        return Math.max(resourceX - resourceSpacingX, startX)
    }

    /** put all events with the same name in the same vertical lane on the right side of resources */
    def private layoutEvents(ElkNode topNode, double startX) {
        var eventX = startX
        for (events : topNode.children.xselect[isEventAction].groupBy[eventName].entrySet.sortBy[key]) {
            for(eventNode: events.value) { 
                eventNode.setLocation(eventX, eventNode.layoutRow.toGlobalY)
                eventNode.setDimensions(eventWidth, eventHeight)
            }
            eventX += eventWidth + eventSpacingX
        }
        return Math.max(eventX - eventSpacingX, startX)
    }

    def private layoutPeripherals(ElkNode resourceNode, double peripheralHeight) {
        var peripheralX = 0.0d
        for (resourcePlaceholder : resourceNode.children.xselect[isResource]) {
            resourcePlaceholder.setLocation(peripheralX, 0.0d)
            resourcePlaceholder.setDimensions(peripheralWidth, peripheralHeight)
            peripheralX += peripheralWidth
        }
        for (peripheral : resourceNode.children.xselect[isPeripheral]) {
            peripheral.setLocation(peripheralX, 0.0d)
            peripheral.setDimensions(peripheralWidth, peripheralHeight)
            peripheral.layoutActions()

            peripheralX += peripheralWidth
        }
    }

    def private layoutActions(ElkNode peripheralNode) {
        val peripheralGlobalY = peripheralNode.globalLocation.y + peripheralPaddingY
        for (action : peripheralNode.children.xselect[isAction]) {
            action.setLocation(actionSpacingX, action.layoutRow.toGlobalY - peripheralGlobalY)
            action.setDimensions(actionWidth, actionHeight)
        }
    }

    def private layoutClaimsAndReleases(ElkNode resource) {
        for (port : resource.ports) {
            if (port.isClaim) {
                port.setLocation(claimReleaseSpacingX, 0)
            } else if (port.isRelease) {
                port.setLocation(claimReleaseSpacingX, resource.height)
            }
        }
    }

//-------------------------------------------
//      Layout Edges
//-------------------------------------------
    def private layoutEdges(ElkNode topNode) {
        val allEdges = Collections::singleton(topNode).closure(true, [children]).collect[containedEdges]
        for (ElkEdge edge : allEdges.filter[isConnected]) {
            val containedByRoot = edge.containingNode.parent === null
            val section = edge.straighten
            val source = edge.source
            val target = edge.target
            
            val sourceOrgGlbLoc = section.globalStartLocation
            val targetOrgGlbLoc = section.globalEndLocation
            
            val sourceBounds = source.globalBounds
            val targetBounds = target.globalBounds
            if (containedByRoot) {
                // FIXME: Somehow edges need some kind of offset, it seems related to the offset of the picture on the canvas,
                // but I don't know how to get a hold on this. It is also different for edges that are contained by child
                // nodes. This part of the layout needs to be investigated.
                sourceBounds.translate(100, 35)
                targetBounds.translate(100, 35)
            }
            
            if (sourceBounds.x > targetBounds.maxX || targetBounds.x > sourceBounds.maxX) {
                // No overlap in x
                section.setGlobalStartLocation(sourceBounds.centerX, sourceBounds.centerY)
                section.setGlobalEndLocation(targetBounds.centerX, targetBounds.centerY)
            } else {
                val centerX = (Math.max(sourceBounds.x, targetBounds.x) + Math.min(sourceBounds.maxX, targetBounds.maxX)) / 2.0d
                section.setGlobalStartLocation(centerX, sourceBounds.centerY)
                section.setGlobalEndLocation(centerX, targetBounds.centerY)
            }
            
            val sourceLydGlbLoc = section.globalStartLocation
            val targetLydGlbLoc = section.globalEndLocation
            
            LOGGER.debug('''«source.name» [«sourceLydGlbLoc.x - sourceOrgGlbLoc.x», «sourceLydGlbLoc.y - sourceOrgGlbLoc.y»] «IF !containedByRoot»-«ENDIF»-> «target.name» [«targetLydGlbLoc.x - targetOrgGlbLoc.x», «targetLydGlbLoc.y - targetOrgGlbLoc.y»]''')
        }
    }

    def private straighten(ElkEdge edge) {
        val section = edge.sections.head
        edge.sections.retainAll(Collections.singleton(section))
        section.bendPoints.clear
        return section
    }

//-------------------------------------------
//      Layout Syncbars
//-------------------------------------------
    def private layoutSyncBars(ElkNode topNode, double minX, double maxX) {
        for (syncBar : topNode.children.xselect[isSyncBar]) {
            val adjacentNodes = syncBar.incomingEdges.map[source].union(syncBar.outgoingEdges.map[target])
            // Adjacent sync bars may not have been laid out yet
            val adjacentLaidOutNodes = adjacentNodes.reject[isSyncBar].toSet

            val syncBarY = syncBar.layoutRow.toGlobalY + syncBarSpacingY
            if (adjacentLaidOutNodes.isEmpty) {
                // if both source and target nodes of a syncbar are also syncbars,
                // width of the syncbar is equal to the full width
                syncBar.setLocation(minX, syncBarY)
                syncBar.setDimensions(maxX - minX, syncBarHeight)
            } else {
                val adjecentMinX = adjacentLaidOutNodes.map[globalLocation.x].min
                val adjecentMaxX = adjacentLaidOutNodes.map[globalLocation.x + width].max
                syncBar.setLocation(adjecentMinX, syncBarY)
                syncBar.setDimensions(adjecentMaxX - adjecentMinX, syncBarHeight)
            }
        }
    }

//-------------------------------------------
//      Helpers
//-------------------------------------------
    // TODO the source, target and section method assume there is only one source,
    // target, section. At this moment the ELK documentation does not explain the use cases
    // of multiple sources etc... As it stands the algorithm probably won't function properly
    // if more than one source etc. is used.
    def private static ElkConnectableShape source(ElkEdge edge) {
        edge.sources.head
    }

    def private static ElkConnectableShape target(ElkEdge edge) {
        edge.targets.head
    }

    def private static boolean isActivity(ElkGraphElement node) {
        node.layoutEClass === ActivityPackage.Literals.ACTIVITY
    }

    def private static boolean isAction(ElkGraphElement node) {
        ActivityPackage.Literals.ACTION.isSuperTypeOf(node.layoutEClass)
    }

    def private static boolean isPeripheral(ElkGraphElement node) {
        node.layoutEClass === MachinePackage.Literals.PERIPHERAL
    }

    def private static boolean isResource(ElkGraphElement node) {
        MachinePackage.Literals.IRESOURCE.isSuperTypeOf(node.layoutEClass)
    }

    def private static boolean isEventAction(ElkGraphElement node) {
        ActivityPackage.Literals.EVENT_ACTION.isSuperTypeOf(node.layoutEClass)
    }

    def private static boolean isSyncBar(ElkGraphElement node) {
        node.layoutEClass === ActivityPackage.Literals.SYNC_BAR
    }

    def private static boolean isClaim(ElkGraphElement port) {
        port.layoutEClass === ActivityPackage.Literals.CLAIM
    }

    def private static boolean isRelease(ElkGraphElement port) {
        port.layoutEClass === ActivityPackage.Literals.RELEASE
    }

    def private static EClass getLayoutEClass(ElkGraphElement element) {
        element.getProperty(ActivityDiagramLayoutOptions.E_CLASS)
    }

    // name is not used in the algorithm, but can be convenient for debugging
    def private static String getName(ElkGraphElement element) {
        element.getProperty(ActivityDiagramLayoutOptions.NAME)
    }

    def private static String getEventName(ElkGraphElement element) {
        element.getProperty(ActivityDiagramLayoutOptions.EVENT_NAME)
    }

    // represents the topological ordering of scheduled nodes
    def private static Integer getLayoutRow(ElkGraphElement element) {
        element.getProperty(ActivityDiagramLayoutOptions.LAYOUT_ROW)
    }
    
    static def ElkPadding getPadding(IPropertyHolder propertyHolder) {
        return propertyHolder.getProperty(CoreOptions.PADDING)
    }
    
    def private static double toGlobalY(Integer rowNumber) {
        return rowNumber * rowHeight
    }
}
