/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.layout;

import activity.ActivityPackage;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import machine.MachinePackage;
import org.eclipse.elk.core.AbstractLayoutProvider;
import org.eclipse.elk.core.math.ElkPadding;
import org.eclipse.elk.core.options.CoreOptions;
import org.eclipse.elk.core.util.IElkProgressMonitor;
import org.eclipse.elk.graph.ElkConnectableShape;
import org.eclipse.elk.graph.ElkEdge;
import org.eclipse.elk.graph.ElkEdgeSection;
import org.eclipse.elk.graph.ElkGraphElement;
import org.eclipse.elk.graph.ElkNode;
import org.eclipse.elk.graph.ElkPort;
import org.eclipse.elk.graph.properties.IPropertyHolder;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.lsat.activity.diagram.layout.ActivityDiagramLayoutOptions;
import org.eclipse.lsat.activity.diagram.layout.ElkGlobalCoordinates;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.DoubleExtensions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class ActivityDiagramLayout {
  public static class Provider extends AbstractLayoutProvider {
    @Override
    public String getDefaultLayoutAlgorithmID() {
      return ActivityDiagramLayoutOptions.ALGORITHM_ID;
    }
    
    @Override
    public void layout(final ElkNode layoutGraph, final IElkProgressMonitor progressMonitor) {
      progressMonitor.begin("Activity Diagram Layout", 1);
      new ActivityDiagramLayout().layout(layoutGraph);
      progressMonitor.done();
    }
  }
  
  private static final Logger LOGGER = LoggerFactory.getLogger(ActivityDiagramLayout.class);
  
  private static final double rowHeight = 60.0d;
  
  private static final double actionSpacingX = 20.0d;
  
  private static final double actionWidth = 140.0d;
  
  private static final double actionHeight = 50.0d;
  
  private static final double syncBarHeight = 10.0d;
  
  private static final double syncBarSpacingY = ((ActivityDiagramLayout.actionHeight - ActivityDiagramLayout.syncBarHeight) / 2.0d);
  
  private static final double resourceSpacingX = (ActivityDiagramLayout.actionWidth * 0.25d);
  
  private static final double resourcePaddingY = 11.0d;
  
  private static final double claimReleaseSpacingX = 10.0d;
  
  private static final double peripheralPaddingX = 2.0;
  
  private static final double peripheralPaddingY = 28.0;
  
  private static final double peripheralWidth = (ActivityDiagramLayout.actionWidth + (ActivityDiagramLayout.actionSpacingX * 2));
  
  private static final double eventSpacingX = ActivityDiagramLayout.resourceSpacingX;
  
  private static final double eventWidth = ActivityDiagramLayout.actionWidth;
  
  private static final double eventHeight = ActivityDiagramLayout.actionHeight;
  
  private void layout(final ElkNode layoutGraph) {
    boolean _isActivity = ActivityDiagramLayout.isActivity(layoutGraph);
    if (_isActivity) {
      final double canvasMinX = 0.0d;
      double canvasMaxX = this.layoutResources(layoutGraph, canvasMinX);
      canvasMaxX = this.layoutEvents(layoutGraph, (canvasMaxX + ActivityDiagramLayout.eventSpacingX));
      this.layoutSyncBars(layoutGraph, canvasMinX, canvasMaxX);
      this.layoutEdges(layoutGraph);
    }
  }
  
  private double layoutResources(final ElkNode topNode, final double startX) {
    double resourceX = startX;
    final Function1<ElkNode, Boolean> _function = (ElkNode it) -> {
      return Boolean.valueOf(ActivityDiagramLayout.isResource(it));
    };
    Iterable<ElkNode> _filter = IterableExtensions.<ElkNode>filter(topNode.getChildren(), _function);
    for (final ElkNode resourceNode : _filter) {
      {
        final Function1<ElkPort, Boolean> _function_1 = (ElkPort it) -> {
          return Boolean.valueOf(ActivityDiagramLayout.isClaim(it));
        };
        final Function1<ElkPort, Integer> _function_2 = (ElkPort it) -> {
          return ActivityDiagramLayout.getLayoutRow(it);
        };
        Integer claimRowNumber = Queries.<Integer>max(IterableExtensions.<ElkPort, Integer>map(IterableExtensions.<ElkPort>filter(resourceNode.getPorts(), _function_1), _function_2), Integer.valueOf(0));
        final Function1<ElkPort, Boolean> _function_3 = (ElkPort it) -> {
          return Boolean.valueOf(ActivityDiagramLayout.isRelease(it));
        };
        final Function1<ElkPort, Integer> _function_4 = (ElkPort it) -> {
          return ActivityDiagramLayout.getLayoutRow(it);
        };
        Integer releaseRowNumber = Queries.<Integer>max(IterableExtensions.<ElkPort, Integer>map(IterableExtensions.<ElkPort>filter(resourceNode.getPorts(), _function_3), _function_4), Integer.valueOf(0));
        double peripheralHeight = (((releaseRowNumber).intValue() - (claimRowNumber).intValue()) * ActivityDiagramLayout.rowHeight);
        int _size = resourceNode.getChildren().size();
        double peripheralsWidth = (_size * (ActivityDiagramLayout.peripheralWidth + ActivityDiagramLayout.peripheralPaddingX));
        ElkGlobalCoordinates.setGlobalLocation(resourceNode, resourceX, ActivityDiagramLayout.toGlobalY(claimRowNumber));
        resourceNode.setDimensions(peripheralsWidth, (peripheralHeight + ActivityDiagramLayout.resourcePaddingY));
        this.layoutPeripherals(resourceNode, peripheralHeight);
        this.layoutClaimsAndReleases(resourceNode);
        double _resourceX = resourceX;
        double _width = resourceNode.getWidth();
        double _plus = (_width + ActivityDiagramLayout.resourceSpacingX);
        resourceX = (_resourceX + _plus);
      }
    }
    return Math.max((resourceX - ActivityDiagramLayout.resourceSpacingX), startX);
  }
  
  /**
   * put all events with the same name in the same vertical lane on the right side of resources
   */
  private double layoutEvents(final ElkNode topNode, final double startX) {
    double eventX = startX;
    final Function1<ElkNode, Boolean> _function = (ElkNode it) -> {
      return Boolean.valueOf(ActivityDiagramLayout.isEventAction(it));
    };
    final Function1<ElkNode, String> _function_1 = (ElkNode it) -> {
      return ActivityDiagramLayout.getEventName(it);
    };
    final Function1<Map.Entry<String, List<ElkNode>>, String> _function_2 = (Map.Entry<String, List<ElkNode>> it) -> {
      return it.getKey();
    };
    List<Map.Entry<String, List<ElkNode>>> _sortBy = IterableExtensions.<Map.Entry<String, List<ElkNode>>, String>sortBy(IterableExtensions.<String, ElkNode>groupBy(Queries.<ElkNode>xselect(topNode.getChildren(), _function), _function_1).entrySet(), _function_2);
    for (final Map.Entry<String, List<ElkNode>> events : _sortBy) {
      {
        List<ElkNode> _value = events.getValue();
        for (final ElkNode eventNode : _value) {
          {
            eventNode.setLocation(eventX, ActivityDiagramLayout.toGlobalY(ActivityDiagramLayout.getLayoutRow(eventNode)));
            eventNode.setDimensions(ActivityDiagramLayout.eventWidth, ActivityDiagramLayout.eventHeight);
          }
        }
        double _eventX = eventX;
        eventX = (_eventX + (ActivityDiagramLayout.eventWidth + ActivityDiagramLayout.eventSpacingX));
      }
    }
    return Math.max((eventX - ActivityDiagramLayout.eventSpacingX), startX);
  }
  
  private void layoutPeripherals(final ElkNode resourceNode, final double peripheralHeight) {
    double peripheralX = 0.0d;
    final Function1<ElkNode, Boolean> _function = (ElkNode it) -> {
      return Boolean.valueOf(ActivityDiagramLayout.isResource(it));
    };
    Iterable<ElkNode> _xselect = Queries.<ElkNode>xselect(resourceNode.getChildren(), _function);
    for (final ElkNode resourcePlaceholder : _xselect) {
      {
        resourcePlaceholder.setLocation(peripheralX, 0.0d);
        resourcePlaceholder.setDimensions(ActivityDiagramLayout.peripheralWidth, peripheralHeight);
        double _peripheralX = peripheralX;
        peripheralX = (_peripheralX + ActivityDiagramLayout.peripheralWidth);
      }
    }
    final Function1<ElkNode, Boolean> _function_1 = (ElkNode it) -> {
      return Boolean.valueOf(ActivityDiagramLayout.isPeripheral(it));
    };
    Iterable<ElkNode> _xselect_1 = Queries.<ElkNode>xselect(resourceNode.getChildren(), _function_1);
    for (final ElkNode peripheral : _xselect_1) {
      {
        peripheral.setLocation(peripheralX, 0.0d);
        peripheral.setDimensions(ActivityDiagramLayout.peripheralWidth, peripheralHeight);
        this.layoutActions(peripheral);
        double _peripheralX = peripheralX;
        peripheralX = (_peripheralX + ActivityDiagramLayout.peripheralWidth);
      }
    }
  }
  
  private void layoutActions(final ElkNode peripheralNode) {
    final double peripheralGlobalY = (ElkGlobalCoordinates.getGlobalLocation(peripheralNode).y + ActivityDiagramLayout.peripheralPaddingY);
    final Function1<ElkNode, Boolean> _function = (ElkNode it) -> {
      return Boolean.valueOf(ActivityDiagramLayout.isAction(it));
    };
    Iterable<ElkNode> _xselect = Queries.<ElkNode>xselect(peripheralNode.getChildren(), _function);
    for (final ElkNode action : _xselect) {
      {
        double _globalY = ActivityDiagramLayout.toGlobalY(ActivityDiagramLayout.getLayoutRow(action));
        double _minus = (_globalY - peripheralGlobalY);
        action.setLocation(ActivityDiagramLayout.actionSpacingX, _minus);
        action.setDimensions(ActivityDiagramLayout.actionWidth, ActivityDiagramLayout.actionHeight);
      }
    }
  }
  
  private void layoutClaimsAndReleases(final ElkNode resource) {
    EList<ElkPort> _ports = resource.getPorts();
    for (final ElkPort port : _ports) {
      boolean _isClaim = ActivityDiagramLayout.isClaim(port);
      if (_isClaim) {
        port.setLocation(ActivityDiagramLayout.claimReleaseSpacingX, 0);
      } else {
        boolean _isRelease = ActivityDiagramLayout.isRelease(port);
        if (_isRelease) {
          port.setLocation(ActivityDiagramLayout.claimReleaseSpacingX, resource.getHeight());
        }
      }
    }
  }
  
  private void layoutEdges(final ElkNode topNode) {
    final Function1<ElkNode, Iterable<? extends ElkNode>> _function = (ElkNode it) -> {
      return it.getChildren();
    };
    final Function1<ElkNode, EList<ElkEdge>> _function_1 = (ElkNode it) -> {
      return it.getContainedEdges();
    };
    final Iterable<ElkEdge> allEdges = Queries.<ElkNode, ElkEdge>collect(Queries.<ElkNode>closure(Collections.<ElkNode>singleton(topNode), true, _function), _function_1);
    final Function1<ElkEdge, Boolean> _function_2 = (ElkEdge it) -> {
      return Boolean.valueOf(it.isConnected());
    };
    Iterable<ElkEdge> _filter = IterableExtensions.<ElkEdge>filter(allEdges, _function_2);
    for (final ElkEdge edge : _filter) {
      {
        ElkNode _parent = edge.getContainingNode().getParent();
        final boolean containedByRoot = (_parent == null);
        final ElkEdgeSection section = this.straighten(edge);
        final ElkConnectableShape source = ActivityDiagramLayout.source(edge);
        final ElkConnectableShape target = ActivityDiagramLayout.target(edge);
        final Point2D.Double sourceOrgGlbLoc = ElkGlobalCoordinates.getGlobalStartLocation(section);
        final Point2D.Double targetOrgGlbLoc = ElkGlobalCoordinates.getGlobalEndLocation(section);
        final Rectangle2D.Double sourceBounds = ElkGlobalCoordinates.getGlobalBounds(source);
        final Rectangle2D.Double targetBounds = ElkGlobalCoordinates.getGlobalBounds(target);
        if (containedByRoot) {
          ElkGlobalCoordinates.translate(sourceBounds, 100, 35);
          ElkGlobalCoordinates.translate(targetBounds, 100, 35);
        }
        if (((sourceBounds.x > targetBounds.getMaxX()) || (targetBounds.x > sourceBounds.getMaxX()))) {
          ElkGlobalCoordinates.setGlobalStartLocation(section, sourceBounds.getCenterX(), sourceBounds.getCenterY());
          ElkGlobalCoordinates.setGlobalEndLocation(section, targetBounds.getCenterX(), targetBounds.getCenterY());
        } else {
          double _max = Math.max(sourceBounds.x, targetBounds.x);
          double _min = Math.min(sourceBounds.getMaxX(), targetBounds.getMaxX());
          double _plus = (_max + _min);
          final double centerX = (_plus / 2.0d);
          ElkGlobalCoordinates.setGlobalStartLocation(section, centerX, sourceBounds.getCenterY());
          ElkGlobalCoordinates.setGlobalEndLocation(section, centerX, targetBounds.getCenterY());
        }
        final Point2D.Double sourceLydGlbLoc = ElkGlobalCoordinates.getGlobalStartLocation(section);
        final Point2D.Double targetLydGlbLoc = ElkGlobalCoordinates.getGlobalEndLocation(section);
        StringConcatenation _builder = new StringConcatenation();
        String _name = ActivityDiagramLayout.getName(source);
        _builder.append(_name);
        _builder.append(" [");
        _builder.append((sourceLydGlbLoc.x - sourceOrgGlbLoc.x));
        _builder.append(", ");
        _builder.append((sourceLydGlbLoc.y - sourceOrgGlbLoc.y));
        _builder.append("] ");
        {
          if ((!containedByRoot)) {
            _builder.append("-");
          }
        }
        _builder.append("-> ");
        String _name_1 = ActivityDiagramLayout.getName(target);
        _builder.append(_name_1);
        _builder.append(" [");
        _builder.append((targetLydGlbLoc.x - targetOrgGlbLoc.x));
        _builder.append(", ");
        _builder.append((targetLydGlbLoc.y - targetOrgGlbLoc.y));
        _builder.append("]");
        ActivityDiagramLayout.LOGGER.debug(_builder.toString());
      }
    }
  }
  
  private ElkEdgeSection straighten(final ElkEdge edge) {
    final ElkEdgeSection section = IterableExtensions.<ElkEdgeSection>head(edge.getSections());
    edge.getSections().retainAll(Collections.<ElkEdgeSection>singleton(section));
    section.getBendPoints().clear();
    return section;
  }
  
  private void layoutSyncBars(final ElkNode topNode, final double minX, final double maxX) {
    final Function1<ElkNode, Boolean> _function = (ElkNode it) -> {
      return Boolean.valueOf(ActivityDiagramLayout.isSyncBar(it));
    };
    Iterable<ElkNode> _xselect = Queries.<ElkNode>xselect(topNode.getChildren(), _function);
    for (final ElkNode syncBar : _xselect) {
      {
        final Function1<ElkEdge, ElkConnectableShape> _function_1 = (ElkEdge it) -> {
          return ActivityDiagramLayout.source(it);
        };
        final Function1<ElkEdge, ElkConnectableShape> _function_2 = (ElkEdge it) -> {
          return ActivityDiagramLayout.target(it);
        };
        final Iterable<ElkConnectableShape> adjacentNodes = Queries.<ElkConnectableShape>union(ListExtensions.<ElkEdge, ElkConnectableShape>map(syncBar.getIncomingEdges(), _function_1), ListExtensions.<ElkEdge, ElkConnectableShape>map(syncBar.getOutgoingEdges(), _function_2));
        final Function1<ElkConnectableShape, Boolean> _function_3 = (ElkConnectableShape it) -> {
          return Boolean.valueOf(ActivityDiagramLayout.isSyncBar(it));
        };
        final Set<ElkConnectableShape> adjacentLaidOutNodes = IterableExtensions.<ElkConnectableShape>toSet(IterableExtensions.<ElkConnectableShape>reject(adjacentNodes, _function_3));
        double _globalY = ActivityDiagramLayout.toGlobalY(ActivityDiagramLayout.getLayoutRow(syncBar));
        final double syncBarY = (_globalY + ActivityDiagramLayout.syncBarSpacingY);
        boolean _isEmpty = adjacentLaidOutNodes.isEmpty();
        if (_isEmpty) {
          syncBar.setLocation(minX, syncBarY);
          syncBar.setDimensions((maxX - minX), ActivityDiagramLayout.syncBarHeight);
        } else {
          final Function1<ElkConnectableShape, Double> _function_4 = (ElkConnectableShape it) -> {
            return Double.valueOf(ElkGlobalCoordinates.getGlobalLocation(it).x);
          };
          final Double adjecentMinX = IterableExtensions.<Double>min(IterableExtensions.<ElkConnectableShape, Double>map(adjacentLaidOutNodes, _function_4));
          final Function1<ElkConnectableShape, Double> _function_5 = (ElkConnectableShape it) -> {
            double _width = it.getWidth();
            return Double.valueOf((ElkGlobalCoordinates.getGlobalLocation(it).x + _width));
          };
          final Double adjecentMaxX = IterableExtensions.<Double>max(IterableExtensions.<ElkConnectableShape, Double>map(adjacentLaidOutNodes, _function_5));
          syncBar.setLocation((adjecentMinX).doubleValue(), syncBarY);
          double _minus = DoubleExtensions.operator_minus(adjecentMaxX, adjecentMinX);
          syncBar.setDimensions(_minus, ActivityDiagramLayout.syncBarHeight);
        }
      }
    }
  }
  
  private static ElkConnectableShape source(final ElkEdge edge) {
    return IterableExtensions.<ElkConnectableShape>head(edge.getSources());
  }
  
  private static ElkConnectableShape target(final ElkEdge edge) {
    return IterableExtensions.<ElkConnectableShape>head(edge.getTargets());
  }
  
  private static boolean isActivity(final ElkGraphElement node) {
    EClass _layoutEClass = ActivityDiagramLayout.getLayoutEClass(node);
    return (_layoutEClass == ActivityPackage.Literals.ACTIVITY);
  }
  
  private static boolean isAction(final ElkGraphElement node) {
    return ActivityPackage.Literals.ACTION.isSuperTypeOf(ActivityDiagramLayout.getLayoutEClass(node));
  }
  
  private static boolean isPeripheral(final ElkGraphElement node) {
    EClass _layoutEClass = ActivityDiagramLayout.getLayoutEClass(node);
    return (_layoutEClass == MachinePackage.Literals.PERIPHERAL);
  }
  
  private static boolean isResource(final ElkGraphElement node) {
    return MachinePackage.Literals.IRESOURCE.isSuperTypeOf(ActivityDiagramLayout.getLayoutEClass(node));
  }
  
  private static boolean isEventAction(final ElkGraphElement node) {
    return ActivityPackage.Literals.EVENT_ACTION.isSuperTypeOf(ActivityDiagramLayout.getLayoutEClass(node));
  }
  
  private static boolean isSyncBar(final ElkGraphElement node) {
    EClass _layoutEClass = ActivityDiagramLayout.getLayoutEClass(node);
    return (_layoutEClass == ActivityPackage.Literals.SYNC_BAR);
  }
  
  private static boolean isClaim(final ElkGraphElement port) {
    EClass _layoutEClass = ActivityDiagramLayout.getLayoutEClass(port);
    return (_layoutEClass == ActivityPackage.Literals.CLAIM);
  }
  
  private static boolean isRelease(final ElkGraphElement port) {
    EClass _layoutEClass = ActivityDiagramLayout.getLayoutEClass(port);
    return (_layoutEClass == ActivityPackage.Literals.RELEASE);
  }
  
  private static EClass getLayoutEClass(final ElkGraphElement element) {
    return element.<EClass>getProperty(ActivityDiagramLayoutOptions.E_CLASS);
  }
  
  private static String getName(final ElkGraphElement element) {
    return element.<String>getProperty(ActivityDiagramLayoutOptions.NAME);
  }
  
  private static String getEventName(final ElkGraphElement element) {
    return element.<String>getProperty(ActivityDiagramLayoutOptions.EVENT_NAME);
  }
  
  private static Integer getLayoutRow(final ElkGraphElement element) {
    return element.<Integer>getProperty(ActivityDiagramLayoutOptions.LAYOUT_ROW);
  }
  
  public static ElkPadding getPadding(final IPropertyHolder propertyHolder) {
    return propertyHolder.<ElkPadding>getProperty(CoreOptions.PADDING);
  }
  
  private static double toGlobalY(final Integer rowNumber) {
    return ((rowNumber).intValue() * ActivityDiagramLayout.rowHeight);
  }
}
