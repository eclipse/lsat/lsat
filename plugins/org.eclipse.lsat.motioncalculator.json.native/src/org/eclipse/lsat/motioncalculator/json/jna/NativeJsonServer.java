/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.jna;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.lsat.motioncalculator.json.JsonRequest;
import org.eclipse.lsat.motioncalculator.json.JsonRequestType;
import org.eclipse.lsat.motioncalculator.json.JsonResponse;
import org.eclipse.lsat.motioncalculator.json.JsonSerializer;
import org.eclipse.lsat.motioncalculator.json.JsonServer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.osgi.service.prefs.Preferences;

import com.sun.jna.Library;
import com.sun.jna.Native;

public class NativeJsonServer implements JsonServer {
    private static final String ARROW_LEFT = "\u2190 ";

    private static final String ARROW_RIGHT = "\u2192 ";

    public static final Color REQUEST_COLOR = new Color(null, 29, 53, 87);

    public static final Color RESPONSE_COLOR = new Color(null, 69, 123, 157);

    private static final int MAX_BUFFER_SIZE = 100000;

    private static final int SMALL_BUFFER_SIZE = 10000;

    public static interface JsonServerJNA extends Library {
        /**
         * JSON Motion calculator request to a Native library modelled after stdlib/strncpy example implementors should
         * create a share lib with this call. request is UTF-8 encoded. response MUST be UTF-8 encoded.
         * <p>
         * <code>void request(char* response, const char* request, size_t len);</code>
         * </p>
         */
        int request(byte[] response, byte[] request, int maxResponseSize);
    }

    private static final Preferences PREFERENCES = InstanceScope.INSTANCE
            .getNode("org.eclipse.lsat.motioncalculator.json.jna");

    private static String sharedLibName = null;

    private static JsonServerJNA instance;

    private static Throwable error;

    private static MessageConsole console;

    private static JsonServerJNA getServer() {
        synchronized (PREFERENCES) {
            boolean writeMessages = PREFERENCES.getBoolean("WRITE_MESSAGES", false);
            if (writeMessages && console == null) {
                console = new MessageConsole("Motion calculator", null);
                // myConsole.activate();
                ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] {console});
                ConsolePlugin.getDefault().getConsoleManager().showConsoleView(console);
            } else if (!writeMessages) {
                if (console != null) {
                    ConsolePlugin.getDefault().getConsoleManager().removeConsoles(new IConsole[] {console});
                }
                console = null;
            }
            String libName = PREFERENCES.get("LIBRARY", sharedLibName);

            if (instance == null || libName != sharedLibName) {
                sharedLibName = libName;
                try {
                    instance = (sharedLibName != null) ? (JsonServerJNA)Native.loadLibrary(sharedLibName,
                            JsonServerJNA.class, new HashMap<String, Object>()) : null;
                    error = null;
                } catch (Throwable e) {
                    error = e;
                }
            }
        }
        return instance;
    }

    private final JsonServerJNA server = getServer();

    @Override
    public final String request(String request) {
        if (server != null) {
            try {
                logRequest(request);
                byte[] responseBytes = new byte[SMALL_BUFFER_SIZE];

                byte[] utf8Request = request.getBytes(StandardCharsets.UTF_8);
                // make sure it's null terminated
                byte[] requestBytes = new byte[utf8Request.length + 1];
                System.arraycopy(utf8Request, 0, requestBytes, 0, utf8Request.length);
                int errorCode = server.request(responseBytes, requestBytes, SMALL_BUFFER_SIZE);
                if (errorCode != 0) {
                    responseBytes = new byte[MAX_BUFFER_SIZE];
                    Arrays.fill(responseBytes, (byte)0xFF);
                    errorCode = server.request(responseBytes, requestBytes, MAX_BUFFER_SIZE);
                    if (errorCode != 0) {
                        return error(request, "Buffer overflow detected error code (" + errorCode + ")", null);
                    }
                }
                String response = Native.toString(responseBytes, "UTF-8");
                logResponse(response);
                return response;
            } catch (Throwable e) {
                return error(request, "Invokation Error", e);
            }
        } else {
            JsonRequest jsonRequest = JsonSerializer.createRequest(request);
            if (jsonRequest.getRequestType() == JsonRequestType.SupportedProfiles) {
                // return a response without profiles so that this function will not be used.
                JsonResponse jsonResponse = new JsonResponse();
                jsonResponse.setRequestType(jsonRequest.getRequestType());
                return JsonSerializer.toJson(jsonResponse);
            }
            return error(request, "Unable to instantiate shared library", error);
        }
    }

    private void logRequest(String request) {
        // no need to prettify
        log(console, REQUEST_COLOR, ARROW_RIGHT, request);
    }

    private void logResponse(String response) {
        if (console != null) {
            // prettify
            try {
                JsonResponse r = JsonSerializer.createResponse(response);
                response = JsonSerializer.toJson(r);
                if (response == null || response.isEmpty()) {
                    response = "{}";
                }
            } catch (Exception e) {
                // use original if it can't be prettified;
            }
            log(console, RESPONSE_COLOR, ARROW_LEFT, response);
        }
    }

    private static void log(MessageConsole console, Color color, String... strs) {
        if (console != null) {
            try (MessageConsoleStream stream = console.newMessageStream()) {
                stream.setCharset(StandardCharsets.UTF_8);
                stream.setColor(color);
                stream.setActivateOnWrite(false);
                for (String str: strs) {
                    stream.print(str);
                }
                stream.println();
            } catch (IOException e) {
                // Intentionally ignore close exception
            }
        }
    }

    private static String error(String request, String message, Throwable error) {
        JsonRequest jsonRequest = JsonSerializer.createRequest(request);
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setRequestType(jsonRequest.getRequestType());
        jsonResponse.setErrorMessage(
                message + " " + sharedLibName + ((error != null) ? " error: " + error.getMessage() : ""));
        String response = JsonSerializer.toJson(jsonResponse);
        return response;
    }
}
