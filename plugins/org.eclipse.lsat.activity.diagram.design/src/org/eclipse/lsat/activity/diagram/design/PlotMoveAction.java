/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.timing.view.MotionViewJob;
import org.eclipse.sirius.business.api.action.AbstractExternalJavaAction;

import activity.Move;

public class PlotMoveAction extends AbstractExternalJavaAction {
    @Override
    public boolean canExecute(Collection<? extends EObject> selections) {
        return null != selections && selections.size() == 1 && selections.iterator().next() instanceof Move;
    }

    @Override
    public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
        MotionViewJob job = new MotionViewJob((Move)selections.iterator().next());
        job.setUser(true);
        job.schedule();
    }
}
