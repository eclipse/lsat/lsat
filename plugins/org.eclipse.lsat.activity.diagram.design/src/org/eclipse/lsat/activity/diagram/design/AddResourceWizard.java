/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.Collection;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.lsat.activity.diagram.services.ActivityServices;
import org.eclipse.lsat.common.emf.ui.model.ModelWorkbenchContentProvider;
import org.eclipse.lsat.common.emf.ui.wizards.ElementTreeSelectionWizardPage;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

import activity.Activity;
import machine.IResource;
import machine.Machine;
import machine.provider.MachineItemProviderAdapterFactory;

public class AddResourceWizard extends Wizard {
    private final ElementTreeSelectionWizardPage resourceSelectionPage;

    private IResource resource;

    private final Activity activity;

    public AddResourceWizard(Collection<EObject> roots, Activity activity, EObject initialSelection) {
        ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
                ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
        adapterFactory.addAdapterFactory(new MachineItemProviderAdapterFactory());
        adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
        this.activity = activity;

        resourceSelectionPage = createResourceSelectionPage(roots, initialSelection, adapterFactory);
    }

    private ElementTreeSelectionWizardPage createResourceSelectionPage(Collection<EObject> roots,
            EObject initialSelection, AdapterFactory adapterFactory)
    {
        ElementTreeSelectionWizardPage resourceSelectionPage = new ElementTreeSelectionWizardPage(
                "PeripheralSelectionPage", new AdapterFactoryLabelProvider(adapterFactory),
                new ModelWorkbenchContentProvider());
        resourceSelectionPage.setTitle("Select a resource");
        resourceSelectionPage.setMessage("Select an existing resource to add to the diagram.");
        resourceSelectionPage.setInput(roots);
        resourceSelectionPage.setAllowMultiple(false);
        Collection<IResource> existing = ActivityServices.getResources(activity);
        resourceSelectionPage.addFilter(new ViewerFilter() {
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                return !existing.contains(element) && (element instanceof Machine || element instanceof IResource);
            }
        });
        resourceSelectionPage.setValidator(new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                if (selection.length <= 0) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select a resource!", null);
                }
                for (Object selected: selection) {
                    if (!(selected instanceof IResource)) {
                        return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Only resource selection is allowed!",
                                null);
                    }
                }
                return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, "OK", null);
            }
        });
        resourceSelectionPage.setInitialSelections(initialSelection);
        return resourceSelectionPage;
    }

    @Override
    public void addPages() {
        addPage(resourceSelectionPage);
    }

    @Override
    public IWizardPage getStartingPage() {
        if (resourceSelectionPage.isPageComplete()) {
            return getNextPage(resourceSelectionPage);
        }
        return super.getStartingPage();
    }

    @Override
    public boolean performFinish() {
        resource = (IResource)resourceSelectionPage.getResult()[0];
        return true;
    }

    public IResource getResource() {
        return resource;
    }
}
