/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import activity.ActivityFactory;
import activity.ActivitySet;
import activity.Event;
import activity.EventAction;
import machine.IResource;

public class AddEventActionPage extends WizardPage {
    private enum EventType {
        Require("Require event"), Raise("Raise event");

        private final String description;

        private EventType(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    private final String initialActionName;

    private final Collection<EObject> roots;

    private Text textName;

    private Text newEventName;

    private ComboViewer comboEventType;

    private ComboViewer comboEventResource;

    public AddEventActionPage(Collection<EObject> roots, String initialActionName) {
        super(AddEventActionPage.class.getSimpleName());
        this.roots = roots;
        setTitle("EventAction details");
        setDescription("Provide the details for the event to add");
        this.initialActionName = initialActionName;
        setPageComplete(false);
    }

    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);
        setControl(container);
        container.setLayout(new GridLayout(2, false));

        Label lblName = new Label(container, SWT.NONE);
        lblName.setAlignment(SWT.RIGHT);
        lblName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblName.setText("Action Name:");

        textName = new Text(container, SWT.BORDER);
        textName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        textName.setText(initialActionName);

        Label evtName = new Label(container, SWT.NONE);
        evtName.setAlignment(SWT.RIGHT);
        evtName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        evtName.setText("Select Event :");

        comboEventResource = new ComboViewer(container, SWT.NONE);
        comboEventResource.setContentProvider(ArrayContentProvider.getInstance());
        comboEventResource.setLabelProvider(new LabelProvider() {
            @Override
            public String getText(Object element) {
                return ((IResource)element).fqn();
            }
        });
        comboEventResource.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboEventResource.getCombo().setEnabled(true);
        comboEventResource.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                updatePageComplete();
            }
        });

        // get all resources of type EventAction
        List<IResource> eventResources = roots.stream().filter(ActivitySet.class::isInstance).map(ActivitySet.class::cast)
            .flatMap(m -> m.getEvents().stream())
            .flatMap(r-> Stream.concat(Stream.of(r), r.getResource().getItems().stream()))
            .map(IResource.class::cast)
            .sorted((r1, r2) -> r1.fqn().compareTo(r2.fqn()))
            .collect(Collectors.toList());

        Event newEvent = ActivityFactory.eINSTANCE.createEvent();
        newEvent.setName("NEW EVENT");
        eventResources.add(0, newEvent); // add it as the first

        comboEventResource.setInput(eventResources);

        Label lblEventName = new Label(container, SWT.NONE);
        lblEventName.setAlignment(SWT.RIGHT);
        lblEventName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        lblEventName.setText("Add New Event:");

        newEventName = new Text(container, SWT.BORDER);
        newEventName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        newEventName.setText("");

        Label lblType = new Label(container, SWT.NONE);
        lblType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblType.setText("EventAction type:");

        comboEventType = new ComboViewer(container, SWT.NONE);
        comboEventType.setContentProvider(ArrayContentProvider.getInstance());
        comboEventType.setLabelProvider(new LabelProvider());
        comboEventType.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboEventType.getCombo().setEnabled(true);
        comboEventType.addSelectionChangedListener(new ISelectionChangedListener() {
            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                updatePageComplete();
            }
        });
        comboEventType.setInput(EventType.values());
    }

    public EventAction createEventAction() {
        String actionName = textName.getText();
        String newEvent = newEventName.getText();
        EventType type = (EventType)((StructuredSelection)comboEventType.getSelection()).getFirstElement();
        EventAction eventAction = (type == EventType.Raise) ? ActivityFactory.eINSTANCE.createRaiseEvent()
                : ActivityFactory.eINSTANCE.createRequireEvent();
        IResource resource = (IResource)((StructuredSelection)comboEventResource.getSelection()).getFirstElement();
        if ("NEW EVENT".equals(resource.getName())) {
            resource.setName(newEvent);
        }
        eventAction.setName(actionName);
        eventAction.setResource(resource);
        return eventAction;
    }

    private void updatePageComplete() {
        boolean pageComplete = true;
        boolean eventResourceSelected = !comboEventResource.getSelection().isEmpty();
        pageComplete &= !textName.getText().isEmpty();
        pageComplete &= eventResourceSelected;
        pageComplete &= !comboEventType.getSelection().isEmpty();
        if ( eventResourceSelected ) {
            IResource resource = (IResource)((StructuredSelection)comboEventResource.getSelection()).getFirstElement();
            if ("NEW EVENT".equals(resource.getName())) {
                newEventName.setEditable(true);
                pageComplete &= !newEventName.getText().isEmpty();
            } else {
                newEventName.setEditable(false);
            }
        }
        setPageComplete(pageComplete);
    }
}
