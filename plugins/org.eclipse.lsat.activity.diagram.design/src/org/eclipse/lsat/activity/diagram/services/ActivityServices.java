/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.services;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.util.CollectionUtil;
import org.eclipse.lsat.timing.util.MoveHelper;
import org.eclipse.sirius.diagram.DSemanticDiagram;
import org.eclipse.sirius.viewpoint.DSemanticDecorator;

import activity.Action;
import activity.Activity;
import activity.Claim;
import activity.EventAction;
import activity.Move;
import activity.PeripheralAction;
import activity.RaiseEvent;
import activity.Release;
import activity.RequireEvent;
import activity.ResourceAction;
import activity.SchedulingType;
import activity.SimpleAction;
import activity.SyncBar;
import activity.impl.ActivityQueries;
import activity.util.ActivityUtil;
import machine.IResource;
import machine.Peripheral;
import machine.ResourceType;

public class ActivityServices {
    /**
     * Checks if PeripheralAction is ALAP.
     *
     * @param action
     * @return true if peripheral action is ALAP.
     */
    public static boolean isALAP(PeripheralAction action) {
        return action.getSchedulingType().equals(SchedulingType.ALAP);
    }

    /**
     * Get actions which use a certain resource
     */
    public static Collection<Action> getResourceActions(Activity activity, IResource resource) {
        ArrayList<Action> result = new ArrayList<Action>();
        CollectionUtil.addAll(result, ActivityQueries.getActionsFor(resource, ResourceAction.class, activity.getNodes()));
        return result;
    }

    public Collection<IResource> getResources(DSemanticDiagram element) {
        return getResources((Activity)element.getTarget());
    }

    public Collection<EventAction> getEvents(DSemanticDiagram element) {
        return getEvents((Activity)element.getTarget());
    }

    /**
     * Get the in-use resources in the activity
     */
    public static Collection<EventAction> getEvents(Activity activity) {
        if (activity == null) {
            return Collections.emptySet();
        }
        return from(activity.getNodes()).objectsOfKind(EventAction.class).asSet();
    }

    /**
     * Get the in-use resources in the activity
     */
    public static Collection<IResource> getResources(Activity activity) {
        if (activity == null) {
            return Collections.emptySet();
        }
        return from(activity.getNodes()).objectsOfKind(ResourceAction.class)
                .xcollectOne(a -> a.getResource())
                .select(r -> ResourceType.EVENT != r.getResource().getResourceType()).asSet();
    }

    /**
     * Get the in-use resources in the activity
     */
    public static Collection<IResource> getEventResources(Activity activity) {
        if (activity == null) {
            return Collections.emptySet();
        }
        return from(activity.getNodes()).objectsOfKind(ResourceAction.class)
                .xcollectOne(a -> a.getResource())
                .select(r -> r.getResource().getResourceType() == ResourceType.EVENT).asSet();
    }

    public static Collection<IResource> getResourcePlaceholder(DSemanticDiagram element, IResource resource) {
        return hasPeripherals(element, resource) ? Collections.emptyList() : Collections.singleton(resource);
    }

    public static boolean hasPeripherals(DSemanticDiagram element, IResource resource) {
        Activity activity = getActivity(element);
        return !ActivityQueries.getActionsFor(resource, PeripheralAction.class, activity.getNodes()).isEmpty();
    }

    public static Collection<Peripheral> getPeripherals(DSemanticDiagram element, IResource resource) {
        return getPeripherals(getActivity(element), resource);
    }

    public static Collection<PeripheralAction> getPeripheralActions(DSemanticDecorator element, Peripheral peripheral) {
        IResource resource = getResource(element);
        Activity activity = getActivity(element);
        return ActivityQueries.getActionsFor(resource, PeripheralAction.class, activity.getNodes())
                .select(a -> a.getPeripheral() == peripheral).asList();
    }

    public static IResource getResource(DSemanticDecorator element) {
        return getTarget(IResource.class, element);
    }

    public static Activity getActivity(DSemanticDecorator element) {
        return getTarget(Activity.class, element);
    }

    private static <T extends EObject> T getTarget(Class<T> clazz, DSemanticDecorator element) {
        while (!(clazz.isInstance(element.getTarget()))) {
            EObject parent = element.eContainer();
            if (!(parent instanceof DSemanticDecorator)) {
                return null;
            }
            element = (DSemanticDecorator)parent;
        }
        return clazz.cast(element.getTarget());
    }

    public static Collection<PeripheralAction> getPeripheralActions(Activity activity, IResource resource) {
        return ActivityQueries.getActionsFor(resource, PeripheralAction.class, activity.getNodes()).asOrderedSet();
    }

    /**
     * Get the in-use peripherals in the activity which belong to a certain resource
     */
    public static Collection<Peripheral> getPeripherals(Activity activity, IResource resource) {
        return ActivityQueries.getActionsFor(resource, PeripheralAction.class, activity.getNodes())
                .xcollectOne(pa -> pa.getPeripheral()).asOrderedSet();
    }

    public Collection<PeripheralAction> getPeripheralActions(DSemanticDiagram element, Activity activity) {
        return Collections.emptyList();
    }

    public Collection<Claim> getClaims(DSemanticDiagram element, IResource resource) {
        return ActivityUtil.getClaims((Activity)element.getTarget(), resource);
    }

    public boolean isPassive(DSemanticDecorator element) {
        Activity activity = getActivity(element);
        IResource resource = getResource(element);
        if (activity == null || resource == null ) {
            return false;
        }
        return ActivityUtil.getClaims(activity, resource).stream().allMatch(Claim::isPassive);
    }

    public static void addClaimRelease(Activity activity, IResource resource) {
        ActivityUtil.addClaim(activity, resource);
        ActivityUtil.addRelease(activity, resource);
    }

    public Collection<Release> getReleases(DSemanticDiagram element, IResource resource) {
        return ActivityUtil.getReleases((Activity)element.getTarget(), resource);
    }

    public String getDescription(EventAction eventAction) {
        StringBuffer description = new StringBuffer(eventAction.getName()).append(": ");
        if (eventAction instanceof RequireEvent) {
            description.append("Require ");
        }
        if (eventAction instanceof RaiseEvent) {
            description.append("Raise ");
        }
        description.append(eventAction.getResource().fqn());
        return description.toString();
    }

    public String getTooltip(EventAction eventAction) {
        return getDescription(eventAction);
    }

    public String getDescription(PeripheralAction action) {
        StringBuffer description = new StringBuffer(action.getName()).append(": ");
        if (action instanceof SimpleAction) {
            description.append(((SimpleAction)action).getType().getName());
        } else if (action instanceof Move) {
            description.append(MoveHelper.getDescription((Move)action, false));
        }
        if (isALAP(action)) {
            description.append(System.lineSeparator()).append("ALAP");
        }
        return description.toString();
    }

    public String getTooltip(PeripheralAction action) {
        StringBuffer description = new StringBuffer(action.getName()).append(": ");
        if (action instanceof SimpleAction) {
            description.append(action.getResource().fqn()).append('.');
            description.append(action.getPeripheral().getName()).append('.');
            description.append(((SimpleAction)action).getType().getName());
        } else if (action instanceof Move) {
            description.append(MoveHelper.getDescription((Move)action, true));
        }
        if (isALAP(action)) {
            description.append(" ").append("ALAP");
        }
        return description.toString();
    }

    public String getTooltip(SyncBar syncbar) {
        return syncbar.getName();
    }

    public String getTooltip(Claim claim) {
        StringBuffer description = new StringBuffer(claim.getName()).append(": ");
        description.append("Claim ");
        description.append(claim.getResource().fqn());
        return description.toString();
    }

    public String getTooltip(Release release) {
        StringBuffer description = new StringBuffer(release.getName()).append(": ");
        description.append("Release ");
        description.append(release.getResource().fqn());
        return description.toString();
    }

    public String getBeginLabel(Edge edge) {
        if (!(edge.getSourceNode() instanceof Action)) {
            return null;
        }
        Action action = (Action)edge.getSourceNode();
        if (null == action.getExit()) {
            return null;
        }
        StringBuffer label = new StringBuffer(action.getExit().getValue());
        if (null != action.getOuterExit()) {
            label.append("\n[").append(action.getOuterExit().getValue()).append("]");
        }
        return label.toString();
    }

    public String getEndLabel(Edge edge) {
        if (!(edge.getTargetNode() instanceof Action)) {
            return null;
        }
        Action action = (Action)edge.getTargetNode();
        if (null == action.getEntry()) {
            return null;
        }
        StringBuffer label = new StringBuffer();
        if (null != action.getOuterEntry()) {
            label.append("[").append(action.getOuterEntry().getValue()).append("]\n");
        }
        label.append(action.getEntry().getValue());
        return label.toString();
    }
}
