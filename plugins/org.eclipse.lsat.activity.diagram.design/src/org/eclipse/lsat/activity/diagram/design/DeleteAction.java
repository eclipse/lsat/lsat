/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.diagram.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.lsat.activity.diagram.services.ActivityServices;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.sirius.business.api.action.AbstractExternalJavaAction;
import org.eclipse.sirius.viewpoint.DSemanticDecorator;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import activity.Action;
import activity.Activity;
import activity.EventAction;
import activity.PeripheralAction;
import activity.SyncBar;
import activity.util.ActivityUtil;
import machine.IResource;
import machine.Peripheral;

public class DeleteAction extends AbstractExternalJavaAction {
    @Override
    public boolean canExecute(Collection<? extends EObject> selections) {
        return null != selections && selections.size() == 1;
    }

    @Override
    public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
        final DSemanticDecorator view = getParameter(parameters, "view", DSemanticDecorator.class);
        final Activity activity = ActivityServices.getActivity(view);

        for (EObject selection: selections) {
            if (selection instanceof PeripheralAction || selection instanceof SyncBar || selection instanceof EventAction) {
                delete((Node)selection);
            } else if (selection instanceof Edge) {
                delete((Edge)selection);
            } else if (selection instanceof IResource) {
                // Delete all resource actions
                for (Action action: ActivityServices.getResourceActions(activity, (IResource)selection)) {
                    delete(action);
                }
            } else if (selection instanceof Peripheral) {
                // Delete all peripheral actions
                final IResource resource = ActivityServices.getResource(view);
                for (PeripheralAction peripheralAction: ActivityServices.getPeripheralActions(view,
                        (Peripheral)selection))
                {
                    delete(peripheralAction);
                }
                // If no peripheral left remove all actions
                if (ActivityServices.getPeripherals(activity, resource).isEmpty()) {
                    for (Action action: ActivityServices.getResourceActions(activity, resource)) {
                        delete(action);
                    }
                }
            } else {
                final String type = selection.eClass().getName();
                Display.getDefault().asyncExec(new Runnable() {
                    @Override
                    public void run() {
                        MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                                "Delete " + type,
                                "Not allowed to delete " + type + ". Please delete the resource in stead.");
                    }
                });
            }
        }
    }

    private static void delete(Node node) {
        ActivityUtil.delete(node);
    }

    private static void delete(Edge edge) {
        ActivityUtil.delete(edge);
    }
}
