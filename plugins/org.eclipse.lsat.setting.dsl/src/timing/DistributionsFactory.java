/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package timing;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;

import timing.distribution.EnumeratedModeDistribution;
import timing.distribution.ModeDistribution;
import timing.distribution.NormalModeDistribution;
import timing.distribution.PertDistribution;
import timing.distribution.TriangularModeDistribution;

public final class DistributionsFactory {
    private static final RandomGenerator DEFAULT_RANDOM = new JDKRandomGenerator();

    private DistributionsFactory() {
        /* Empty */
    }

    public static ModeDistribution createRealDistribution(Distribution distribution) {
        return createRealDistribution(distribution, DEFAULT_RANDOM);
    }

    public static ModeDistribution createRealDistribution(Distribution distribution, RandomGenerator random) {
        switch (distribution.eClass().getClassifierID()) {
            case TimingPackage.TRIANGULAR_DISTRIBUTION:
                timing.TriangularDistribution td = (timing.TriangularDistribution)distribution;
                return new TriangularModeDistribution(random, td.getMin().doubleValue(), td.getMode().doubleValue(),
                        td.getMax().doubleValue(), getDefault(distribution));
            case TimingPackage.NORMAL_DISTRIBUTION:
                timing.NormalDistribution nd = (timing.NormalDistribution)distribution;
                return new NormalModeDistribution(random, nd.getMean().doubleValue(), nd.getSd().doubleValue(),
                        NormalDistribution.DEFAULT_INVERSE_ABSOLUTE_ACCURACY, getDefault(distribution));
            case TimingPackage.ENUMERATED_DISTRIBUTION:
                timing.EnumeratedDistribution ed = (timing.EnumeratedDistribution)distribution;
                return new EnumeratedModeDistribution(random, toDouble(ed.getValues()), getDefault(distribution));
            case TimingPackage.PERT_DISTRIBUTION:
                timing.PertDistribution pd = (timing.PertDistribution)distribution;
                return new PertDistribution(random, pd.getMin().doubleValue(), pd.getMax().doubleValue(),
                        pd.getMode().doubleValue(), pd.getGamma().doubleValue(), getDefault(distribution));
            default:
                throw new IllegalArgumentException("Distribution type not supported: " + distribution.getClass());
        }
    }

    private static double[] toDouble(List<BigDecimal> values) {
        double[] result = new double[values.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = values.get(i).doubleValue();
        }
        return result;
    }

    private static Double getDefault(Distribution distribution) {
        return null == distribution.getDefault() ? null : distribution.getDefault().doubleValue();
    }
}
