/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package timing.distribution;

import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.exception.MathIllegalNumberException;

public interface ModeDistribution extends RealDistribution {
    /**
     * The ‘mode’ is the value that occurs most often.
     *
     * @return the mode, if applicable
     * @throws MathIllegalNumberException if the mode cannot be determined for the distributiuon
     */
    double getMode() throws ModeNotSupportedException;

    /**
     * Returns the default value to use for scheduling for this distribution.<br>
     * The default value can either be specified by the end user or it typically defaults to either the 'mode' or 'mean'
     * of this distribution.
     */
    double getDefault();
}
