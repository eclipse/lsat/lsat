/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package timing.distribution;

public class ModeNotSupportedException extends RuntimeException {
    private static final long serialVersionUID = 9041737413484800349L;

    public ModeNotSupportedException(String arg0) {
        super(arg0);
    }
}
