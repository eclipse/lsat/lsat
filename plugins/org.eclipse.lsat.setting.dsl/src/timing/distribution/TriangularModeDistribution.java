/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package timing.distribution;

import org.apache.commons.math3.distribution.TriangularDistribution;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.random.RandomGenerator;

public class TriangularModeDistribution extends TriangularDistribution implements ModeDistribution {
    private static final long serialVersionUID = -1672927578232975262L;

    private final Double itsDefault;

    public TriangularModeDistribution(RandomGenerator rng, double a, double c, double b, Double _default)
            throws NumberIsTooLargeException, NumberIsTooSmallException
    {
        super(rng, a, c, b);
        itsDefault = _default;
    }

    @Override
    public double getDefault() {
        if (null != itsDefault)
            return itsDefault;
        return getMode();
    }
}
