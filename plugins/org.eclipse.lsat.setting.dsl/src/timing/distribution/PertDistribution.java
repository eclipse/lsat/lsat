/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package timing.distribution;

import org.apache.commons.math3.distribution.BetaDistribution;
import org.apache.commons.math3.random.RandomGenerator;

/**
 * The PERT distribution is a transformation of the four-parameter beta distribution, see
 * <a href="https://en.wikipedia.org/wiki/PERT_distribution">wikipedia</a>
 */
public class PertDistribution extends BetaDistribution implements ModeDistribution {
    private static final long serialVersionUID = -6681144758330302722L;

    private final double min;

    private final double max;

    private final double mode;

    private final double gamma;

    private final Double itsDefault;

    public PertDistribution(RandomGenerator rng, double min, double max, double mode, double gamma, Double _default) {
        super(rng, (((mode - min) / (max - min)) * gamma) + 1, (((max - mode) / (max - min)) * gamma) + 1,
                DEFAULT_INVERSE_ABSOLUTE_ACCURACY);
        this.min = min;
        this.max = max;
        this.mode = mode;
        this.gamma = gamma;
        itsDefault = _default;
    }

    @Override
    public double getDefault() {
        if (null != itsDefault) {
            return itsDefault;
        }
        return getMode();
    }

    @Override
    public double getMode() {
        return mode;
    }

    public double getGamma() {
        return gamma;
    }

    @Override
    public double density(double x) {
        return super.density(toBeta(x));
    }

    @Override
    public double getNumericalMean() {
        return toPert(super.getNumericalMean());
    }

    @Override
    public double getSupportLowerBound() {
        return toPert(super.getSupportLowerBound());
    }

    @Override
    public double getSupportUpperBound() {
        return toPert(super.getSupportUpperBound());
    }

    @Override
    public double sample() {
        return toPert(super.sample());
    }

    protected double toPert(double beta) {
        if (Double.isNaN(beta)) {
            return beta;
        }
        return (beta * (max - min)) + min;
    }

    protected double toBeta(double pert) {
        if (Double.isNaN(pert)) {
            return pert;
        }
        return (pert - min) / (max - min);
    }
}
