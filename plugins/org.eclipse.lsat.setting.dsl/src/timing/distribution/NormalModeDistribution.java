/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package timing.distribution;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.random.RandomGenerator;

public class NormalModeDistribution extends NormalDistribution implements ModeDistribution {
    private static final long serialVersionUID = -480146659936840078L;

    private final Double itsDefault;

    public NormalModeDistribution(RandomGenerator rng, double mean, double sd, double inverseCumAccuracy,
            Double _default) throws NotStrictlyPositiveException
    {
        super(rng, mean, sd, inverseCumAccuracy);
        itsDefault = _default;
    }

    /**
     * Mode equals mean for a normal distribution
     *
     * @see timing.distribution.ModeDistribution#getMode()
     */
    @Override
    public double getMode() {
        return getMean();
    }

    @Override
    public double getDefault() {
        if (null != itsDefault) {
            return itsDefault;
        }
        return getMean();
    }
}
