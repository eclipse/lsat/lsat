/**
 */
package setting.impl;

import expressions.Expression;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import setting.MotionProfileSettings;
import setting.SettingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Motion Profile Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link setting.impl.MotionProfileSettingsImpl#getMotionProfile <em>Motion Profile</em>}</li>
 *   <li>{@link setting.impl.MotionProfileSettingsImpl#getMotionArguments <em>Motion Arguments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MotionProfileSettingsImpl extends MinimalEObjectImpl.Container implements MotionProfileSettings {
	/**
     * The default value of the '{@link #getMotionProfile() <em>Motion Profile</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMotionProfile()
     * @generated
     * @ordered
     */
	protected static final String MOTION_PROFILE_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getMotionProfile() <em>Motion Profile</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMotionProfile()
     * @generated
     * @ordered
     */
	protected String motionProfile = MOTION_PROFILE_EDEFAULT;

	/**
     * The cached value of the '{@link #getMotionArguments() <em>Motion Arguments</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMotionArguments()
     * @generated
     * @ordered
     */
	protected EMap<String, Expression> motionArguments;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MotionProfileSettingsImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return SettingPackage.Literals.MOTION_PROFILE_SETTINGS;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getMotionProfile() {
        return motionProfile;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMotionProfile(String newMotionProfile) {
        String oldMotionProfile = motionProfile;
        motionProfile = newMotionProfile;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_PROFILE, oldMotionProfile, motionProfile));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<String, Expression> getMotionArguments() {
        if (motionArguments == null)
        {
            motionArguments = new EcoreEMap<String,Expression>(SettingPackage.Literals.MOTION_ARGUMENTS_MAP_ENTRY, MotionArgumentsMapEntryImpl.class, this, SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS);
        }
        return motionArguments;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS:
                return ((InternalEList<?>)getMotionArguments()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_PROFILE:
                return getMotionProfile();
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS:
                if (coreType) return getMotionArguments();
                else return getMotionArguments().map();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_PROFILE:
                setMotionProfile((String)newValue);
                return;
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS:
                ((EStructuralFeature.Setting)getMotionArguments()).set(newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_PROFILE:
                setMotionProfile(MOTION_PROFILE_EDEFAULT);
                return;
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS:
                getMotionArguments().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_PROFILE:
                return MOTION_PROFILE_EDEFAULT == null ? motionProfile != null : !MOTION_PROFILE_EDEFAULT.equals(motionProfile);
            case SettingPackage.MOTION_PROFILE_SETTINGS__MOTION_ARGUMENTS:
                return motionArguments != null && !motionArguments.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (motionProfile: ");
        result.append(motionProfile);
        result.append(')');
        return result.toString();
    }

} //MotionProfileSettingsImpl
