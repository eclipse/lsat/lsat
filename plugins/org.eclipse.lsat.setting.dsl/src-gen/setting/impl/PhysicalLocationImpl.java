/**
 */
package setting.impl;

import expressions.Expression;

import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import setting.PhysicalLocation;
import setting.SettingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Location</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link setting.impl.PhysicalLocationImpl#getDefaultExp <em>Default Exp</em>}</li>
 *   <li>{@link setting.impl.PhysicalLocationImpl#getMinExp <em>Min Exp</em>}</li>
 *   <li>{@link setting.impl.PhysicalLocationImpl#getMaxExp <em>Max Exp</em>}</li>
 *   <li>{@link setting.impl.PhysicalLocationImpl#getDefault <em>Default</em>}</li>
 *   <li>{@link setting.impl.PhysicalLocationImpl#getMin <em>Min</em>}</li>
 *   <li>{@link setting.impl.PhysicalLocationImpl#getMax <em>Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalLocationImpl extends MinimalEObjectImpl.Container implements PhysicalLocation {
	/**
     * The cached value of the '{@link #getDefaultExp() <em>Default Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDefaultExp()
     * @generated
     * @ordered
     */
	protected Expression defaultExp;

	/**
     * The cached value of the '{@link #getMinExp() <em>Min Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMinExp()
     * @generated
     * @ordered
     */
	protected Expression minExp;

	/**
     * The cached value of the '{@link #getMaxExp() <em>Max Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMaxExp()
     * @generated
     * @ordered
     */
	protected Expression maxExp;

	/**
     * The default value of the '{@link #getDefault() <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDefault()
     * @generated
     * @ordered
     */
	protected static final BigDecimal DEFAULT_EDEFAULT = null;

	/**
     * The default value of the '{@link #getMin() <em>Min</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMin()
     * @generated
     * @ordered
     */
	protected static final BigDecimal MIN_EDEFAULT = null;

	/**
     * The default value of the '{@link #getMax() <em>Max</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMax()
     * @generated
     * @ordered
     */
	protected static final BigDecimal MAX_EDEFAULT = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PhysicalLocationImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return SettingPackage.Literals.PHYSICAL_LOCATION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getDefaultExp() {
        return defaultExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetDefaultExp(Expression newDefaultExp, NotificationChain msgs) {
        Expression oldDefaultExp = defaultExp;
        defaultExp = newDefaultExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP, oldDefaultExp, newDefaultExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDefaultExp(Expression newDefaultExp) {
        if (newDefaultExp != defaultExp)
        {
            NotificationChain msgs = null;
            if (defaultExp != null)
                msgs = ((InternalEObject)defaultExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP, null, msgs);
            if (newDefaultExp != null)
                msgs = ((InternalEObject)newDefaultExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP, null, msgs);
            msgs = basicSetDefaultExp(newDefaultExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP, newDefaultExp, newDefaultExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getMinExp() {
        return minExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetMinExp(Expression newMinExp, NotificationChain msgs) {
        Expression oldMinExp = minExp;
        minExp = newMinExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_LOCATION__MIN_EXP, oldMinExp, newMinExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMinExp(Expression newMinExp) {
        if (newMinExp != minExp)
        {
            NotificationChain msgs = null;
            if (minExp != null)
                msgs = ((InternalEObject)minExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SettingPackage.PHYSICAL_LOCATION__MIN_EXP, null, msgs);
            if (newMinExp != null)
                msgs = ((InternalEObject)newMinExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SettingPackage.PHYSICAL_LOCATION__MIN_EXP, null, msgs);
            msgs = basicSetMinExp(newMinExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_LOCATION__MIN_EXP, newMinExp, newMinExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getMaxExp() {
        return maxExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetMaxExp(Expression newMaxExp, NotificationChain msgs) {
        Expression oldMaxExp = maxExp;
        maxExp = newMaxExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_LOCATION__MAX_EXP, oldMaxExp, newMaxExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMaxExp(Expression newMaxExp) {
        if (newMaxExp != maxExp)
        {
            NotificationChain msgs = null;
            if (maxExp != null)
                msgs = ((InternalEObject)maxExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SettingPackage.PHYSICAL_LOCATION__MAX_EXP, null, msgs);
            if (newMaxExp != null)
                msgs = ((InternalEObject)newMaxExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SettingPackage.PHYSICAL_LOCATION__MAX_EXP, null, msgs);
            msgs = basicSetMaxExp(newMaxExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_LOCATION__MAX_EXP, newMaxExp, newMaxExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getDefault() {
        return null == defaultExp? null : defaultExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getMin() {
        return null == minExp? null : minExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getMax() {
        return null == maxExp? null : maxExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP:
                return basicSetDefaultExp(null, msgs);
            case SettingPackage.PHYSICAL_LOCATION__MIN_EXP:
                return basicSetMinExp(null, msgs);
            case SettingPackage.PHYSICAL_LOCATION__MAX_EXP:
                return basicSetMaxExp(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP:
                return getDefaultExp();
            case SettingPackage.PHYSICAL_LOCATION__MIN_EXP:
                return getMinExp();
            case SettingPackage.PHYSICAL_LOCATION__MAX_EXP:
                return getMaxExp();
            case SettingPackage.PHYSICAL_LOCATION__DEFAULT:
                return getDefault();
            case SettingPackage.PHYSICAL_LOCATION__MIN:
                return getMin();
            case SettingPackage.PHYSICAL_LOCATION__MAX:
                return getMax();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP:
                setDefaultExp((Expression)newValue);
                return;
            case SettingPackage.PHYSICAL_LOCATION__MIN_EXP:
                setMinExp((Expression)newValue);
                return;
            case SettingPackage.PHYSICAL_LOCATION__MAX_EXP:
                setMaxExp((Expression)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP:
                setDefaultExp((Expression)null);
                return;
            case SettingPackage.PHYSICAL_LOCATION__MIN_EXP:
                setMinExp((Expression)null);
                return;
            case SettingPackage.PHYSICAL_LOCATION__MAX_EXP:
                setMaxExp((Expression)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_LOCATION__DEFAULT_EXP:
                return defaultExp != null;
            case SettingPackage.PHYSICAL_LOCATION__MIN_EXP:
                return minExp != null;
            case SettingPackage.PHYSICAL_LOCATION__MAX_EXP:
                return maxExp != null;
            case SettingPackage.PHYSICAL_LOCATION__DEFAULT:
                return DEFAULT_EDEFAULT == null ? getDefault() != null : !DEFAULT_EDEFAULT.equals(getDefault());
            case SettingPackage.PHYSICAL_LOCATION__MIN:
                return MIN_EDEFAULT == null ? getMin() != null : !MIN_EDEFAULT.equals(getMin());
            case SettingPackage.PHYSICAL_LOCATION__MAX:
                return MAX_EDEFAULT == null ? getMax() != null : !MAX_EDEFAULT.equals(getMax());
        }
        return super.eIsSet(featureID);
    }

} //PhysicalLocationImpl
