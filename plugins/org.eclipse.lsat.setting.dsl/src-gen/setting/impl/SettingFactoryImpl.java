/**
 */
package setting.impl;

import expressions.Expression;
import java.util.Map;

import machine.ActionType;
import machine.Axis;
import machine.Distance;
import machine.Position;
import machine.Profile;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import setting.*;

import timing.Timing;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SettingFactoryImpl extends EFactoryImpl implements SettingFactory {
	/**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static SettingFactory init() {
        try
        {
            SettingFactory theSettingFactory = (SettingFactory)EPackage.Registry.INSTANCE.getEFactory(SettingPackage.eNS_URI);
            if (theSettingFactory != null)
            {
                return theSettingFactory;
            }
        }
        catch (Exception exception)
        {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new SettingFactoryImpl();
    }

	/**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public SettingFactoryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EObject create(EClass eClass) {
        switch (eClass.getClassifierID())
        {
            case SettingPackage.SETTINGS: return createSettings();
            case SettingPackage.PROFILE_SETTINGS_MAP_ENTRY: return (EObject)createProfileSettingsMapEntry();
            case SettingPackage.LOCATION_SETTINGS_MAP_ENTRY: return (EObject)createLocationSettingsMapEntry();
            case SettingPackage.TIMING_SETTINGS_MAP_ENTRY: return (EObject)createTimingSettingsMapEntry();
            case SettingPackage.PHYSICAL_SETTINGS: return createPhysicalSettings();
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY: return (EObject)createMotionSettingsMapEntry();
            case SettingPackage.MOTION_SETTINGS: return createMotionSettings();
            case SettingPackage.PHYSICAL_LOCATION: return createPhysicalLocation();
            case SettingPackage.MOTION_PROFILE_SETTINGS: return createMotionProfileSettings();
            case SettingPackage.MOTION_ARGUMENTS_MAP_ENTRY: return (EObject)createMotionArgumentsMapEntry();
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY: return (EObject)createDistanceSettingsMapEntry();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Settings createSettings() {
        SettingsImpl settings = new SettingsImpl();
        return settings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<Profile, MotionProfileSettings> createProfileSettingsMapEntry() {
        ProfileSettingsMapEntryImpl profileSettingsMapEntry = new ProfileSettingsMapEntryImpl();
        return profileSettingsMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<Position, PhysicalLocation> createLocationSettingsMapEntry() {
        LocationSettingsMapEntryImpl locationSettingsMapEntry = new LocationSettingsMapEntryImpl();
        return locationSettingsMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<ActionType, Timing> createTimingSettingsMapEntry() {
        TimingSettingsMapEntryImpl timingSettingsMapEntry = new TimingSettingsMapEntryImpl();
        return timingSettingsMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PhysicalSettings createPhysicalSettings() {
        PhysicalSettingsImpl physicalSettings = new PhysicalSettingsImpl();
        return physicalSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<Axis, MotionSettings> createMotionSettingsMapEntry() {
        MotionSettingsMapEntryImpl motionSettingsMapEntry = new MotionSettingsMapEntryImpl();
        return motionSettingsMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MotionSettings createMotionSettings() {
        MotionSettingsImpl motionSettings = new MotionSettingsImpl();
        return motionSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PhysicalLocation createPhysicalLocation() {
        PhysicalLocationImpl physicalLocation = new PhysicalLocationImpl();
        return physicalLocation;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MotionProfileSettings createMotionProfileSettings() {
        MotionProfileSettingsImpl motionProfileSettings = new MotionProfileSettingsImpl();
        return motionProfileSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<String, Expression> createMotionArgumentsMapEntry() {
        MotionArgumentsMapEntryImpl motionArgumentsMapEntry = new MotionArgumentsMapEntryImpl();
        return motionArgumentsMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Map.Entry<Distance, Expression> createDistanceSettingsMapEntry() {
        DistanceSettingsMapEntryImpl distanceSettingsMapEntry = new DistanceSettingsMapEntryImpl();
        return distanceSettingsMapEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SettingPackage getSettingPackage() {
        return (SettingPackage)getEPackage();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
	@Deprecated
	public static SettingPackage getPackage() {
        return SettingPackage.eINSTANCE;
    }

} //SettingFactoryImpl
