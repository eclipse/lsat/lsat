/**
 */
package setting.impl;

import expressions.Expression;
import java.util.Map;

import machine.Axis;
import machine.Distance;
import machine.Position;
import machine.Profile;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import setting.MotionProfileSettings;
import setting.MotionSettings;
import setting.PhysicalLocation;
import setting.SettingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Motion Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link setting.impl.MotionSettingsImpl#getLocationSettings <em>Location Settings</em>}</li>
 *   <li>{@link setting.impl.MotionSettingsImpl#getProfileSettings <em>Profile Settings</em>}</li>
 *   <li>{@link setting.impl.MotionSettingsImpl#getDistanceSettings <em>Distance Settings</em>}</li>
 *   <li>{@link setting.impl.MotionSettingsImpl#getEntry <em>Entry</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MotionSettingsImpl extends MinimalEObjectImpl.Container implements MotionSettings {
	/**
     * The cached value of the '{@link #getLocationSettings() <em>Location Settings</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getLocationSettings()
     * @generated
     * @ordered
     */
	protected EMap<Position, PhysicalLocation> locationSettings;

	/**
     * The cached value of the '{@link #getProfileSettings() <em>Profile Settings</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getProfileSettings()
     * @generated
     * @ordered
     */
	protected EMap<Profile, MotionProfileSettings> profileSettings;

	/**
     * The cached value of the '{@link #getDistanceSettings() <em>Distance Settings</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDistanceSettings()
     * @generated
     * @ordered
     */
	protected EMap<Distance, Expression> distanceSettings;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected MotionSettingsImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return SettingPackage.Literals.MOTION_SETTINGS;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<Position, PhysicalLocation> getLocationSettings() {
        if (locationSettings == null)
        {
            locationSettings = new EcoreEMap<Position,PhysicalLocation>(SettingPackage.Literals.LOCATION_SETTINGS_MAP_ENTRY, LocationSettingsMapEntryImpl.class, this, SettingPackage.MOTION_SETTINGS__LOCATION_SETTINGS);
        }
        return locationSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<Profile, MotionProfileSettings> getProfileSettings() {
        if (profileSettings == null)
        {
            profileSettings = new EcoreEMap<Profile,MotionProfileSettings>(SettingPackage.Literals.PROFILE_SETTINGS_MAP_ENTRY, ProfileSettingsMapEntryImpl.class, this, SettingPackage.MOTION_SETTINGS__PROFILE_SETTINGS);
        }
        return profileSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<Distance, Expression> getDistanceSettings() {
        if (distanceSettings == null)
        {
            distanceSettings = new EcoreEMap<Distance,Expression>(SettingPackage.Literals.DISTANCE_SETTINGS_MAP_ENTRY, DistanceSettingsMapEntryImpl.class, this, SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS, SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS);
        }
        return distanceSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public Map.Entry<Axis, MotionSettings> getEntry() {
        if (eContainerFeatureID() != SettingPackage.MOTION_SETTINGS__ENTRY) return null;
        return (Map.Entry<Axis, MotionSettings>)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetEntry(Map.Entry<Axis, MotionSettings> newEntry, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newEntry, SettingPackage.MOTION_SETTINGS__ENTRY, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setEntry(Map.Entry<Axis, MotionSettings> newEntry) {
        if (newEntry != eInternalContainer() || (eContainerFeatureID() != SettingPackage.MOTION_SETTINGS__ENTRY && newEntry != null))
        {
            if (EcoreUtil.isAncestor(this, (EObject)newEntry))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newEntry != null)
                msgs = ((InternalEObject)newEntry).eInverseAdd(this, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE, Map.Entry.class, msgs);
            msgs = basicSetEntry(newEntry, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.MOTION_SETTINGS__ENTRY, newEntry, newEntry));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getDistanceSettings()).basicAdd(otherEnd, msgs);
            case SettingPackage.MOTION_SETTINGS__ENTRY:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetEntry((Map.Entry<Axis, MotionSettings>)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS__LOCATION_SETTINGS:
                return ((InternalEList<?>)getLocationSettings()).basicRemove(otherEnd, msgs);
            case SettingPackage.MOTION_SETTINGS__PROFILE_SETTINGS:
                return ((InternalEList<?>)getProfileSettings()).basicRemove(otherEnd, msgs);
            case SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS:
                return ((InternalEList<?>)getDistanceSettings()).basicRemove(otherEnd, msgs);
            case SettingPackage.MOTION_SETTINGS__ENTRY:
                return basicSetEntry(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID())
        {
            case SettingPackage.MOTION_SETTINGS__ENTRY:
                return eInternalContainer().eInverseRemove(this, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__VALUE, Map.Entry.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS__LOCATION_SETTINGS:
                if (coreType) return getLocationSettings();
                else return getLocationSettings().map();
            case SettingPackage.MOTION_SETTINGS__PROFILE_SETTINGS:
                if (coreType) return getProfileSettings();
                else return getProfileSettings().map();
            case SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS:
                if (coreType) return getDistanceSettings();
                else return getDistanceSettings().map();
            case SettingPackage.MOTION_SETTINGS__ENTRY:
                return getEntry();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS__LOCATION_SETTINGS:
                ((EStructuralFeature.Setting)getLocationSettings()).set(newValue);
                return;
            case SettingPackage.MOTION_SETTINGS__PROFILE_SETTINGS:
                ((EStructuralFeature.Setting)getProfileSettings()).set(newValue);
                return;
            case SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS:
                ((EStructuralFeature.Setting)getDistanceSettings()).set(newValue);
                return;
            case SettingPackage.MOTION_SETTINGS__ENTRY:
                setEntry((Map.Entry<Axis, MotionSettings>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS__LOCATION_SETTINGS:
                getLocationSettings().clear();
                return;
            case SettingPackage.MOTION_SETTINGS__PROFILE_SETTINGS:
                getProfileSettings().clear();
                return;
            case SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS:
                getDistanceSettings().clear();
                return;
            case SettingPackage.MOTION_SETTINGS__ENTRY:
                setEntry((Map.Entry<Axis, MotionSettings>)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case SettingPackage.MOTION_SETTINGS__LOCATION_SETTINGS:
                return locationSettings != null && !locationSettings.isEmpty();
            case SettingPackage.MOTION_SETTINGS__PROFILE_SETTINGS:
                return profileSettings != null && !profileSettings.isEmpty();
            case SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS:
                return distanceSettings != null && !distanceSettings.isEmpty();
            case SettingPackage.MOTION_SETTINGS__ENTRY:
                return getEntry() != null;
        }
        return super.eIsSet(featureID);
    }

} //MotionSettingsImpl
