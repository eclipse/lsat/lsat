/**
 */
package setting.impl;

import expressions.Expression;

import java.math.BigDecimal;
import machine.Distance;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;
import setting.MotionSettings;
import setting.SettingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Distance Settings Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link setting.impl.DistanceSettingsMapEntryImpl#getName <em>Name</em>}</li>
 *   <li>{@link setting.impl.DistanceSettingsMapEntryImpl#getTypedValue <em>Value</em>}</li>
 *   <li>{@link setting.impl.DistanceSettingsMapEntryImpl#getTypedKey <em>Key</em>}</li>
 *   <li>{@link setting.impl.DistanceSettingsMapEntryImpl#getBdValue <em>Bd Value</em>}</li>
 *   <li>{@link setting.impl.DistanceSettingsMapEntryImpl#getMotionSettings <em>Motion Settings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DistanceSettingsMapEntryImpl extends MinimalEObjectImpl.Container implements BasicEMap.Entry<Distance,Expression> {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getTypedValue() <em>Value</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTypedValue()
     * @generated
     * @ordered
     */
	protected Expression value;

	/**
     * The cached value of the '{@link #getTypedKey() <em>Key</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTypedKey()
     * @generated
     * @ordered
     */
	protected Distance key;

	/**
     * The default value of the '{@link #getBdValue() <em>Bd Value</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getBdValue()
     * @generated
     * @ordered
     */
	protected static final BigDecimal BD_VALUE_EDEFAULT = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DistanceSettingsMapEntryImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return SettingPackage.Literals.DISTANCE_SETTINGS_MAP_ENTRY;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public String getName() {
        return null == key ? null : key.getName();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Expression getTypedValue() {
        return value;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetTypedValue(Expression newValue, NotificationChain msgs) {
        Expression oldValue = value;
        value = newValue;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE, oldValue, newValue);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setTypedValue(Expression newValue) {
        if (newValue != value)
        {
            NotificationChain msgs = null;
            if (value != null)
                msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE, null, msgs);
            if (newValue != null)
                msgs = ((InternalEObject)newValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE, null, msgs);
            msgs = basicSetTypedValue(newValue, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE, newValue, newValue));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Distance getTypedKey() {
        if (key != null && key.eIsProxy())
        {
            InternalEObject oldKey = (InternalEObject)key;
            key = (Distance)eResolveProxy(oldKey);
            if (key != oldKey)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__KEY, oldKey, key));
            }
        }
        return key;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Distance basicGetTypedKey() {
        return key;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setTypedKey(Distance newKey) {
        Distance oldKey = key;
        key = newKey;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__KEY, oldKey, key));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public BigDecimal getBdValue() {
        return null == getValue()? null : getValue().evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public MotionSettings getMotionSettings() {
        if (eContainerFeatureID() != SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS) return null;
        return (MotionSettings)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetMotionSettings(MotionSettings newMotionSettings, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newMotionSettings, SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void setMotionSettings(MotionSettings newMotionSettings) {
        if (newMotionSettings != eInternalContainer() || (eContainerFeatureID() != SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS && newMotionSettings != null))
        {
            if (EcoreUtil.isAncestor(this, newMotionSettings))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newMotionSettings != null)
                msgs = ((InternalEObject)newMotionSettings).eInverseAdd(this, SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS, MotionSettings.class, msgs);
            msgs = basicSetMotionSettings(newMotionSettings, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS, newMotionSettings, newMotionSettings));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetMotionSettings((MotionSettings)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE:
                return basicSetTypedValue(null, msgs);
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS:
                return basicSetMotionSettings(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID())
        {
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS:
                return eInternalContainer().eInverseRemove(this, SettingPackage.MOTION_SETTINGS__DISTANCE_SETTINGS, MotionSettings.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__NAME:
                return getName();
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE:
                return getTypedValue();
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__KEY:
                if (resolve) return getTypedKey();
                return basicGetTypedKey();
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__BD_VALUE:
                return getBdValue();
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS:
                return getMotionSettings();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE:
                setTypedValue((Expression)newValue);
                return;
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__KEY:
                setTypedKey((Distance)newValue);
                return;
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS:
                setMotionSettings((MotionSettings)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE:
                setTypedValue((Expression)null);
                return;
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__KEY:
                setTypedKey((Distance)null);
                return;
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS:
                setMotionSettings((MotionSettings)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__NAME:
                return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__VALUE:
                return value != null;
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__KEY:
                return key != null;
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__BD_VALUE:
                return BD_VALUE_EDEFAULT == null ? getBdValue() != null : !BD_VALUE_EDEFAULT.equals(getBdValue());
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY__MOTION_SETTINGS:
                return getMotionSettings() != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected int hash = -1;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public int getHash() {
        if (hash == -1)
        {
            Object theKey = getKey();
            hash = (theKey == null ? 0 : theKey.hashCode());
        }
        return hash;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setHash(int hash) {
        this.hash = hash;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Distance getKey() {
        return getTypedKey();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setKey(Distance key) {
        setTypedKey(key);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getValue() {
        return getTypedValue();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression setValue(Expression value) {
        Expression oldValue = getValue();
        setTypedValue(value);
        return oldValue;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	public EMap<Distance, Expression> getEMap() {
        EObject container = eContainer();
        return container == null ? null : (EMap<Distance, Expression>)container.eGet(eContainmentFeature());
    }

} //DistanceSettingsMapEntryImpl
