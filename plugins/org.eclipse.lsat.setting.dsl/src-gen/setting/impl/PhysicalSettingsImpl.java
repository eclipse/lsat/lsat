/**
 */
package setting.impl;

import java.lang.reflect.InvocationTargetException;

import machine.ActionType;
import machine.Axis;
import machine.HasResourcePeripheral;
import machine.IResource;
import machine.Peripheral;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import setting.MotionSettings;
import setting.PhysicalSettings;
import setting.SettingPackage;
import setting.Settings;

import timing.Timing;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Settings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link setting.impl.PhysicalSettingsImpl#getTimingSettings <em>Timing Settings</em>}</li>
 *   <li>{@link setting.impl.PhysicalSettingsImpl#getMotionSettings <em>Motion Settings</em>}</li>
 *   <li>{@link setting.impl.PhysicalSettingsImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link setting.impl.PhysicalSettingsImpl#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link setting.impl.PhysicalSettingsImpl#getSettings <em>Settings</em>}</li>
 *   <li>{@link setting.impl.PhysicalSettingsImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhysicalSettingsImpl extends MinimalEObjectImpl.Container implements PhysicalSettings {
	/**
     * The cached value of the '{@link #getTimingSettings() <em>Timing Settings</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTimingSettings()
     * @generated
     * @ordered
     */
	protected EMap<ActionType, Timing> timingSettings;

	/**
     * The cached value of the '{@link #getMotionSettings() <em>Motion Settings</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMotionSettings()
     * @generated
     * @ordered
     */
	protected EMap<Axis, MotionSettings> motionSettings;

	/**
     * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getResource()
     * @generated
     * @ordered
     */
	protected IResource resource;

	/**
     * The cached value of the '{@link #getPeripheral() <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPeripheral()
     * @generated
     * @ordered
     */
	protected Peripheral peripheral;

	/**
     * The cached value of the '{@link #getSettings() <em>Settings</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSettings()
     * @generated
     * @ordered
     */
	protected Settings settings;

	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PhysicalSettingsImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return SettingPackage.Literals.PHYSICAL_SETTINGS;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<ActionType, Timing> getTimingSettings() {
        if (timingSettings == null)
        {
            timingSettings = new EcoreEMap<ActionType,Timing>(SettingPackage.Literals.TIMING_SETTINGS_MAP_ENTRY, TimingSettingsMapEntryImpl.class, this, SettingPackage.PHYSICAL_SETTINGS__TIMING_SETTINGS);
        }
        return timingSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<Axis, MotionSettings> getMotionSettings() {
        if (motionSettings == null)
        {
            motionSettings = new EcoreEMap<Axis,MotionSettings>(SettingPackage.Literals.MOTION_SETTINGS_MAP_ENTRY, MotionSettingsMapEntryImpl.class, this, SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS, SettingPackage.MOTION_SETTINGS_MAP_ENTRY__SETTINGS);
        }
        return motionSettings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public IResource getResource() {
        if (resource != null && resource.eIsProxy())
        {
            InternalEObject oldResource = (InternalEObject)resource;
            resource = (IResource)eResolveProxy(oldResource);
            if (resource != oldResource)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, SettingPackage.PHYSICAL_SETTINGS__RESOURCE, oldResource, resource));
            }
        }
        return resource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public IResource basicGetResource() {
        return resource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setResource(IResource newResource) {
        IResource oldResource = resource;
        resource = newResource;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_SETTINGS__RESOURCE, oldResource, resource));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Peripheral getPeripheral() {
        if (peripheral != null && peripheral.eIsProxy())
        {
            InternalEObject oldPeripheral = (InternalEObject)peripheral;
            peripheral = (Peripheral)eResolveProxy(oldPeripheral);
            if (peripheral != oldPeripheral)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, SettingPackage.PHYSICAL_SETTINGS__PERIPHERAL, oldPeripheral, peripheral));
            }
        }
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Peripheral basicGetPeripheral() {
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPeripheral(Peripheral newPeripheral) {
        Peripheral oldPeripheral = peripheral;
        peripheral = newPeripheral;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_SETTINGS__PERIPHERAL, oldPeripheral, peripheral));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Settings getSettings() {
        if (settings != null && settings.eIsProxy())
        {
            InternalEObject oldSettings = (InternalEObject)settings;
            settings = (Settings)eResolveProxy(oldSettings);
            if (settings != oldSettings)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, SettingPackage.PHYSICAL_SETTINGS__SETTINGS, oldSettings, settings));
            }
        }
        return settings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Settings basicGetSettings() {
        return settings;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setSettings(Settings newSettings) {
        Settings oldSettings = settings;
        settings = newSettings;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, SettingPackage.PHYSICAL_SETTINGS__SETTINGS, oldSettings, settings));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return (null == resource || null== peripheral) ? null : resource.fqn() + '.' + peripheral.getName();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String fqn() {
        return (getResource()!=null && getPeripheral() != null) ? getResource().fqn()+"."+getPeripheral().getName():null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean rpEquals(final HasResourcePeripheral cmp) {
        if (cmp ==null) return false; 
        return org.eclipse.emf.ecore.util.EcoreUtil.equals(getResource(),cmp.getResource()) && org.eclipse.emf.ecore.util.EcoreUtil.equals(getPeripheral(),cmp.getPeripheral());
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getMotionSettings()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_SETTINGS__TIMING_SETTINGS:
                return ((InternalEList<?>)getTimingSettings()).basicRemove(otherEnd, msgs);
            case SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS:
                return ((InternalEList<?>)getMotionSettings()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_SETTINGS__TIMING_SETTINGS:
                if (coreType) return getTimingSettings();
                else return getTimingSettings().map();
            case SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS:
                if (coreType) return getMotionSettings();
                else return getMotionSettings().map();
            case SettingPackage.PHYSICAL_SETTINGS__RESOURCE:
                if (resolve) return getResource();
                return basicGetResource();
            case SettingPackage.PHYSICAL_SETTINGS__PERIPHERAL:
                if (resolve) return getPeripheral();
                return basicGetPeripheral();
            case SettingPackage.PHYSICAL_SETTINGS__SETTINGS:
                if (resolve) return getSettings();
                return basicGetSettings();
            case SettingPackage.PHYSICAL_SETTINGS__NAME:
                return getName();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_SETTINGS__TIMING_SETTINGS:
                ((EStructuralFeature.Setting)getTimingSettings()).set(newValue);
                return;
            case SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS:
                ((EStructuralFeature.Setting)getMotionSettings()).set(newValue);
                return;
            case SettingPackage.PHYSICAL_SETTINGS__RESOURCE:
                setResource((IResource)newValue);
                return;
            case SettingPackage.PHYSICAL_SETTINGS__PERIPHERAL:
                setPeripheral((Peripheral)newValue);
                return;
            case SettingPackage.PHYSICAL_SETTINGS__SETTINGS:
                setSettings((Settings)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_SETTINGS__TIMING_SETTINGS:
                getTimingSettings().clear();
                return;
            case SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS:
                getMotionSettings().clear();
                return;
            case SettingPackage.PHYSICAL_SETTINGS__RESOURCE:
                setResource((IResource)null);
                return;
            case SettingPackage.PHYSICAL_SETTINGS__PERIPHERAL:
                setPeripheral((Peripheral)null);
                return;
            case SettingPackage.PHYSICAL_SETTINGS__SETTINGS:
                setSettings((Settings)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case SettingPackage.PHYSICAL_SETTINGS__TIMING_SETTINGS:
                return timingSettings != null && !timingSettings.isEmpty();
            case SettingPackage.PHYSICAL_SETTINGS__MOTION_SETTINGS:
                return motionSettings != null && !motionSettings.isEmpty();
            case SettingPackage.PHYSICAL_SETTINGS__RESOURCE:
                return resource != null;
            case SettingPackage.PHYSICAL_SETTINGS__PERIPHERAL:
                return peripheral != null;
            case SettingPackage.PHYSICAL_SETTINGS__SETTINGS:
                return settings != null;
            case SettingPackage.PHYSICAL_SETTINGS__NAME:
                return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case SettingPackage.PHYSICAL_SETTINGS___FQN:
                return fqn();
            case SettingPackage.PHYSICAL_SETTINGS___RP_EQUALS__HASRESOURCEPERIPHERAL:
                return rpEquals((HasResourcePeripheral)arguments.get(0));
        }
        return super.eInvoke(operationID, arguments);
    }

} //PhysicalSettingsImpl
