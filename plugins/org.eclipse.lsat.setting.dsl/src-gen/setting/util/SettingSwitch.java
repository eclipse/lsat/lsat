/**
 */
package setting.util;

import expressions.Expression;
import java.util.Map;

import machine.ActionType;
import machine.Axis;
import machine.Distance;
import machine.HasResourcePeripheral;
import machine.ImportContainer;
import machine.Position;
import machine.Profile;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import setting.*;

import timing.Timing;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see setting.SettingPackage
 * @generated
 */
public class SettingSwitch<T> extends Switch<T> {
	/**
     * The cached model package
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static SettingPackage modelPackage;

	/**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public SettingSwitch() {
        if (modelPackage == null)
        {
            modelPackage = SettingPackage.eINSTANCE;
        }
    }

	/**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

	/**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID)
        {
            case SettingPackage.SETTINGS:
            {
                Settings settings = (Settings)theEObject;
                T result = caseSettings(settings);
                if (result == null) result = caseImportContainer(settings);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.PROFILE_SETTINGS_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<Profile, MotionProfileSettings> profileSettingsMapEntry = (Map.Entry<Profile, MotionProfileSettings>)theEObject;
                T result = caseProfileSettingsMapEntry(profileSettingsMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.LOCATION_SETTINGS_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<Position, PhysicalLocation> locationSettingsMapEntry = (Map.Entry<Position, PhysicalLocation>)theEObject;
                T result = caseLocationSettingsMapEntry(locationSettingsMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.TIMING_SETTINGS_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<ActionType, Timing> timingSettingsMapEntry = (Map.Entry<ActionType, Timing>)theEObject;
                T result = caseTimingSettingsMapEntry(timingSettingsMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.PHYSICAL_SETTINGS:
            {
                PhysicalSettings physicalSettings = (PhysicalSettings)theEObject;
                T result = casePhysicalSettings(physicalSettings);
                if (result == null) result = caseHasResourcePeripheral(physicalSettings);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.MOTION_SETTINGS_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<Axis, MotionSettings> motionSettingsMapEntry = (Map.Entry<Axis, MotionSettings>)theEObject;
                T result = caseMotionSettingsMapEntry(motionSettingsMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.MOTION_SETTINGS:
            {
                MotionSettings motionSettings = (MotionSettings)theEObject;
                T result = caseMotionSettings(motionSettings);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.PHYSICAL_LOCATION:
            {
                PhysicalLocation physicalLocation = (PhysicalLocation)theEObject;
                T result = casePhysicalLocation(physicalLocation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.MOTION_PROFILE_SETTINGS:
            {
                MotionProfileSettings motionProfileSettings = (MotionProfileSettings)theEObject;
                T result = caseMotionProfileSettings(motionProfileSettings);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.MOTION_ARGUMENTS_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<String, Expression> motionArgumentsMapEntry = (Map.Entry<String, Expression>)theEObject;
                T result = caseMotionArgumentsMapEntry(motionArgumentsMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case SettingPackage.DISTANCE_SETTINGS_MAP_ENTRY:
            {
                @SuppressWarnings("unchecked") Map.Entry<Distance, Expression> distanceSettingsMapEntry = (Map.Entry<Distance, Expression>)theEObject;
                T result = caseDistanceSettingsMapEntry(distanceSettingsMapEntry);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Settings</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Settings</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseSettings(Settings object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Profile Settings Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Profile Settings Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseProfileSettingsMapEntry(Map.Entry<Profile, MotionProfileSettings> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Location Settings Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Location Settings Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseLocationSettingsMapEntry(Map.Entry<Position, PhysicalLocation> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Timing Settings Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Timing Settings Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseTimingSettingsMapEntry(Map.Entry<ActionType, Timing> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Physical Settings</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Physical Settings</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePhysicalSettings(PhysicalSettings object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Motion Settings Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Motion Settings Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseMotionSettingsMapEntry(Map.Entry<Axis, MotionSettings> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Motion Settings</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Motion Settings</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseMotionSettings(MotionSettings object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Physical Location</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Physical Location</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePhysicalLocation(PhysicalLocation object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Motion Profile Settings</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Motion Profile Settings</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseMotionProfileSettings(MotionProfileSettings object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Motion Arguments Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Motion Arguments Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseMotionArgumentsMapEntry(Map.Entry<String, Expression> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Distance Settings Map Entry</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Distance Settings Map Entry</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseDistanceSettingsMapEntry(Map.Entry<Distance, Expression> object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Import Container</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Import Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseImportContainer(ImportContainer object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Has Resource Peripheral</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Has Resource Peripheral</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseHasResourcePeripheral(HasResourcePeripheral object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
	@Override
	public T defaultCase(EObject object) {
        return null;
    }

} //SettingSwitch
