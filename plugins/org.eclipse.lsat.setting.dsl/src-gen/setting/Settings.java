/**
 */
package setting;

import expressions.Declaration;
import machine.IResource;
import machine.ImportContainer;
import machine.Peripheral;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Settings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link setting.Settings#getDeclarations <em>Declarations</em>}</li>
 *   <li>{@link setting.Settings#getPhysicalSettings <em>Physical Settings</em>}</li>
 * </ul>
 *
 * @see setting.SettingPackage#getSettings()
 * @model
 * @generated
 */
public interface Settings extends ImportContainer {
	/**
     * Returns the value of the '<em><b>Declarations</b></em>' containment reference list.
     * The list contents are of type {@link expressions.Declaration}.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Declarations</em>' containment reference list.
     * @see setting.SettingPackage#getSettings_Declarations()
     * @model containment="true"
     * @generated
     */
	EList<Declaration> getDeclarations();

	/**
     * Returns the value of the '<em><b>Physical Settings</b></em>' containment reference list.
     * The list contents are of type {@link setting.PhysicalSettings}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Settings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Physical Settings</em>' containment reference list.
     * @see setting.SettingPackage#getSettings_PhysicalSettings()
     * @model containment="true"
     * @generated
     */
	EList<PhysicalSettings> getPhysicalSettings();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * 
     * <!-- end-model-doc -->
     * @model kind="operation"
     * @generated
     */
	EList<PhysicalSettings> getTransitivePhysicalSettings();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * returns the physical settings or null for IResource and Peripheral
     *     falls back to Resource in case of an ResourceItem, 
     *     falls back to peripheral.getResource() if resource == null 
     * <!-- end-model-doc -->
     * @model peripheralRequired="true"
     * @generated
     */
	PhysicalSettings getPhysicalSettings(IResource resource, Peripheral peripheral);

} // Settings
