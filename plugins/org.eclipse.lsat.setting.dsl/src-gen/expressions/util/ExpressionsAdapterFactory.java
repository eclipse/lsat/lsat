/**
 */
package expressions.util;

import expressions.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see expressions.ExpressionsPackage
 * @generated
 */
public class ExpressionsAdapterFactory extends AdapterFactoryImpl {
	/**
     * The cached model package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static ExpressionsPackage modelPackage;

	/**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ExpressionsAdapterFactory() {
        if (modelPackage == null)
        {
            modelPackage = ExpressionsPackage.eINSTANCE;
        }
    }

	/**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
	@Override
	public boolean isFactoryForType(Object object) {
        if (object == modelPackage)
        {
            return true;
        }
        if (object instanceof EObject)
        {
            return ((EObject)object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

	/**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ExpressionsSwitch<Adapter> modelSwitch =
		new ExpressionsSwitch<Adapter>()
        {
            @Override
            public Adapter caseAbstractElement(AbstractElement object)
            {
                return createAbstractElementAdapter();
            }
            @Override
            public Adapter caseDeclaration(Declaration object)
            {
                return createDeclarationAdapter();
            }
            @Override
            public Adapter caseExpression(Expression object)
            {
                return createExpressionAdapter();
            }
            @Override
            public Adapter caseAdd(Add object)
            {
                return createAddAdapter();
            }
            @Override
            public Adapter caseSubtract(Subtract object)
            {
                return createSubtractAdapter();
            }
            @Override
            public Adapter caseMultiply(Multiply object)
            {
                return createMultiplyAdapter();
            }
            @Override
            public Adapter caseBigDecimalConstant(BigDecimalConstant object)
            {
                return createBigDecimalConstantAdapter();
            }
            @Override
            public Adapter caseDeclarationRef(DeclarationRef object)
            {
                return createDeclarationRefAdapter();
            }
            @Override
            public Adapter caseDivide(Divide object)
            {
                return createDivideAdapter();
            }
            @Override
            public Adapter caseBinaryExpression(BinaryExpression object)
            {
                return createBinaryExpressionAdapter();
            }
            @Override
            public Adapter defaultCase(EObject object)
            {
                return createEObjectAdapter();
            }
        };

	/**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
	@Override
	public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject)target);
    }


	/**
     * Creates a new adapter for an object of class '{@link expressions.AbstractElement <em>Abstract Element</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.AbstractElement
     * @generated
     */
	public Adapter createAbstractElementAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.Declaration <em>Declaration</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.Declaration
     * @generated
     */
	public Adapter createDeclarationAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.Expression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.Expression
     * @generated
     */
	public Adapter createExpressionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.Add <em>Add</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.Add
     * @generated
     */
	public Adapter createAddAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.Subtract <em>Subtract</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.Subtract
     * @generated
     */
	public Adapter createSubtractAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.Multiply <em>Multiply</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.Multiply
     * @generated
     */
	public Adapter createMultiplyAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.BigDecimalConstant <em>Big Decimal Constant</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.BigDecimalConstant
     * @generated
     */
	public Adapter createBigDecimalConstantAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.DeclarationRef <em>Declaration Ref</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.DeclarationRef
     * @generated
     */
	public Adapter createDeclarationRefAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.Divide <em>Divide</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.Divide
     * @generated
     */
	public Adapter createDivideAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for an object of class '{@link expressions.BinaryExpression <em>Binary Expression</em>}'.
     * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @see expressions.BinaryExpression
     * @generated
     */
	public Adapter createBinaryExpressionAdapter() {
        return null;
    }

	/**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
     * @return the new adapter.
     * @generated
     */
	public Adapter createEObjectAdapter() {
        return null;
    }

} //ExpressionsAdapterFactory
