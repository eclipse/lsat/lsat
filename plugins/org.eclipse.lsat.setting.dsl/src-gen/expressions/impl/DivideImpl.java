/**
 */
package expressions.impl;

import expressions.Divide;
import expressions.ExpressionsPackage;

import java.lang.reflect.InvocationTargetException;

import java.math.BigDecimal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Divide</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DivideImpl extends BinaryExpressionImpl implements Divide {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DivideImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ExpressionsPackage.Literals.DIVIDE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal operation(final BigDecimal left, final BigDecimal right) {
        return left.divide(right, 20, java.math.RoundingMode.HALF_UP);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case ExpressionsPackage.DIVIDE___OPERATION__BIGDECIMAL_BIGDECIMAL:
                return operation((BigDecimal)arguments.get(0), (BigDecimal)arguments.get(1));
        }
        return super.eInvoke(operationID, arguments);
    }

} //DivideImpl
