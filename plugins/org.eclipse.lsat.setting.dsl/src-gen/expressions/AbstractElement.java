/**
 */
package expressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see expressions.ExpressionsPackage#getAbstractElement()
 * @model abstract="true"
 * @generated
 */
public interface AbstractElement extends EObject {
} // AbstractElement
