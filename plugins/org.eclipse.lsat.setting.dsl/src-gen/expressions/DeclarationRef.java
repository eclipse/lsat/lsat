/**
 */
package expressions;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declaration Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link expressions.DeclarationRef#getDeclaration <em>Declaration</em>}</li>
 * </ul>
 *
 * @see expressions.ExpressionsPackage#getDeclarationRef()
 * @model
 * @generated
 */
public interface DeclarationRef extends Expression {
	/**
     * Returns the value of the '<em><b>Declaration</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Declaration</em>' reference.
     * @see #setDeclaration(Declaration)
     * @see expressions.ExpressionsPackage#getDeclarationRef_Declaration()
     * @model
     * @generated
     */
	Declaration getDeclaration();

	/**
     * Sets the value of the '{@link expressions.DeclarationRef#getDeclaration <em>Declaration</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Declaration</em>' reference.
     * @see #getDeclaration()
     * @generated
     */
	void setDeclaration(Declaration value);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	BigDecimal evaluate();

} // DeclarationRef
