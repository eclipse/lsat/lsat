/**
 */
package expressions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see expressions.ExpressionsFactory
 * @model kind="package"
 * @generated
 */
public interface ExpressionsPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "expressions";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.eclipse.org/lsat/expressions";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "expressions";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	ExpressionsPackage eINSTANCE = expressions.impl.ExpressionsPackageImpl.init();

	/**
     * The meta object id for the '{@link expressions.impl.AbstractElementImpl <em>Abstract Element</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.AbstractElementImpl
     * @see expressions.impl.ExpressionsPackageImpl#getAbstractElement()
     * @generated
     */
	int ABSTRACT_ELEMENT = 0;

	/**
     * The number of structural features of the '<em>Abstract Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ABSTRACT_ELEMENT_FEATURE_COUNT = 0;

	/**
     * The number of operations of the '<em>Abstract Element</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ABSTRACT_ELEMENT_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link expressions.impl.DeclarationImpl <em>Declaration</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.DeclarationImpl
     * @see expressions.impl.ExpressionsPackageImpl#getDeclaration()
     * @generated
     */
	int DECLARATION = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION__NAME = ABSTRACT_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Expression</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION__EXPRESSION = ABSTRACT_ELEMENT_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Declaration</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION_FEATURE_COUNT = ABSTRACT_ELEMENT_FEATURE_COUNT + 2;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION___FQN = ABSTRACT_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Declaration</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION_OPERATION_COUNT = ABSTRACT_ELEMENT_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link expressions.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.ExpressionImpl
     * @see expressions.impl.ExpressionsPackageImpl#getExpression()
     * @generated
     */
	int EXPRESSION = 2;

	/**
     * The number of structural features of the '<em>Expression</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EXPRESSION_FEATURE_COUNT = ABSTRACT_ELEMENT_FEATURE_COUNT + 0;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EXPRESSION___EVALUATE = ABSTRACT_ELEMENT_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Expression</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EXPRESSION_OPERATION_COUNT = ABSTRACT_ELEMENT_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link expressions.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.BinaryExpressionImpl
     * @see expressions.impl.ExpressionsPackageImpl#getBinaryExpression()
     * @generated
     */
	int BINARY_EXPRESSION = 9;

	/**
     * The feature id for the '<em><b>Left</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BINARY_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Right</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BINARY_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Binary Expression</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BINARY_EXPRESSION___EVALUATE = EXPRESSION_OPERATION_COUNT + 0;

	/**
     * The operation id for the '<em>Operation</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BINARY_EXPRESSION___OPERATION__BIGDECIMAL_BIGDECIMAL = EXPRESSION_OPERATION_COUNT + 1;

	/**
     * The number of operations of the '<em>Binary Expression</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BINARY_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 2;

	/**
     * The meta object id for the '{@link expressions.impl.AddImpl <em>Add</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.AddImpl
     * @see expressions.impl.ExpressionsPackageImpl#getAdd()
     * @generated
     */
	int ADD = 3;

	/**
     * The feature id for the '<em><b>Left</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ADD__LEFT = BINARY_EXPRESSION__LEFT;

	/**
     * The feature id for the '<em><b>Right</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ADD__RIGHT = BINARY_EXPRESSION__RIGHT;

	/**
     * The number of structural features of the '<em>Add</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ADD_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ADD___EVALUATE = BINARY_EXPRESSION___EVALUATE;

	/**
     * The operation id for the '<em>Operation</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ADD___OPERATION__BIGDECIMAL_BIGDECIMAL = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Add</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ADD_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link expressions.impl.SubtractImpl <em>Subtract</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.SubtractImpl
     * @see expressions.impl.ExpressionsPackageImpl#getSubtract()
     * @generated
     */
	int SUBTRACT = 4;

	/**
     * The feature id for the '<em><b>Left</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SUBTRACT__LEFT = BINARY_EXPRESSION__LEFT;

	/**
     * The feature id for the '<em><b>Right</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SUBTRACT__RIGHT = BINARY_EXPRESSION__RIGHT;

	/**
     * The number of structural features of the '<em>Subtract</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SUBTRACT_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SUBTRACT___EVALUATE = BINARY_EXPRESSION___EVALUATE;

	/**
     * The operation id for the '<em>Operation</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SUBTRACT___OPERATION__BIGDECIMAL_BIGDECIMAL = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Subtract</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SUBTRACT_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link expressions.impl.MultiplyImpl <em>Multiply</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.MultiplyImpl
     * @see expressions.impl.ExpressionsPackageImpl#getMultiply()
     * @generated
     */
	int MULTIPLY = 5;

	/**
     * The feature id for the '<em><b>Left</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MULTIPLY__LEFT = BINARY_EXPRESSION__LEFT;

	/**
     * The feature id for the '<em><b>Right</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MULTIPLY__RIGHT = BINARY_EXPRESSION__RIGHT;

	/**
     * The number of structural features of the '<em>Multiply</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MULTIPLY_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MULTIPLY___EVALUATE = BINARY_EXPRESSION___EVALUATE;

	/**
     * The operation id for the '<em>Operation</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MULTIPLY___OPERATION__BIGDECIMAL_BIGDECIMAL = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Multiply</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MULTIPLY_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link expressions.impl.BigDecimalConstantImpl <em>Big Decimal Constant</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.BigDecimalConstantImpl
     * @see expressions.impl.ExpressionsPackageImpl#getBigDecimalConstant()
     * @generated
     */
	int BIG_DECIMAL_CONSTANT = 6;

	/**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIG_DECIMAL_CONSTANT__VALUE = EXPRESSION_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Big Decimal Constant</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIG_DECIMAL_CONSTANT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIG_DECIMAL_CONSTANT___EVALUATE = EXPRESSION_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Big Decimal Constant</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIG_DECIMAL_CONSTANT_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link expressions.impl.DeclarationRefImpl <em>Declaration Ref</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.DeclarationRefImpl
     * @see expressions.impl.ExpressionsPackageImpl#getDeclarationRef()
     * @generated
     */
	int DECLARATION_REF = 7;

	/**
     * The feature id for the '<em><b>Declaration</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION_REF__DECLARATION = EXPRESSION_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Declaration Ref</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION_REF_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION_REF___EVALUATE = EXPRESSION_OPERATION_COUNT + 0;

	/**
     * The operation id for the '<em>Has Circular Dependencies</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION_REF___HAS_CIRCULAR_DEPENDENCIES__EOBJECT = EXPRESSION_OPERATION_COUNT + 1;

	/**
     * The number of operations of the '<em>Declaration Ref</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DECLARATION_REF_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 2;

	/**
     * The meta object id for the '{@link expressions.impl.DivideImpl <em>Divide</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see expressions.impl.DivideImpl
     * @see expressions.impl.ExpressionsPackageImpl#getDivide()
     * @generated
     */
	int DIVIDE = 8;

	/**
     * The feature id for the '<em><b>Left</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DIVIDE__LEFT = BINARY_EXPRESSION__LEFT;

	/**
     * The feature id for the '<em><b>Right</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DIVIDE__RIGHT = BINARY_EXPRESSION__RIGHT;

	/**
     * The number of structural features of the '<em>Divide</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DIVIDE_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
     * The operation id for the '<em>Evaluate</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DIVIDE___EVALUATE = BINARY_EXPRESSION___EVALUATE;

	/**
     * The operation id for the '<em>Operation</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DIVIDE___OPERATION__BIGDECIMAL_BIGDECIMAL = BINARY_EXPRESSION_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Divide</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DIVIDE_OPERATION_COUNT = BINARY_EXPRESSION_OPERATION_COUNT + 1;


	/**
     * Returns the meta object for class '{@link expressions.AbstractElement <em>Abstract Element</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Abstract Element</em>'.
     * @see expressions.AbstractElement
     * @generated
     */
	EClass getAbstractElement();

	/**
     * Returns the meta object for class '{@link expressions.Declaration <em>Declaration</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Declaration</em>'.
     * @see expressions.Declaration
     * @generated
     */
	EClass getDeclaration();

	/**
     * Returns the meta object for the attribute '{@link expressions.Declaration#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see expressions.Declaration#getName()
     * @see #getDeclaration()
     * @generated
     */
	EAttribute getDeclaration_Name();

	/**
     * Returns the meta object for the containment reference '{@link expressions.Declaration#getExpression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Expression</em>'.
     * @see expressions.Declaration#getExpression()
     * @see #getDeclaration()
     * @generated
     */
	EReference getDeclaration_Expression();

	/**
     * Returns the meta object for the '{@link expressions.Declaration#fqn() <em>Fqn</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Fqn</em>' operation.
     * @see expressions.Declaration#fqn()
     * @generated
     */
	EOperation getDeclaration__Fqn();

	/**
     * Returns the meta object for class '{@link expressions.Expression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Expression</em>'.
     * @see expressions.Expression
     * @generated
     */
	EClass getExpression();

	/**
     * Returns the meta object for the '{@link expressions.Expression#evaluate() <em>Evaluate</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Evaluate</em>' operation.
     * @see expressions.Expression#evaluate()
     * @generated
     */
	EOperation getExpression__Evaluate();

	/**
     * Returns the meta object for class '{@link expressions.Add <em>Add</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Add</em>'.
     * @see expressions.Add
     * @generated
     */
	EClass getAdd();

	/**
     * Returns the meta object for the '{@link expressions.Add#operation(java.math.BigDecimal, java.math.BigDecimal) <em>Operation</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Operation</em>' operation.
     * @see expressions.Add#operation(java.math.BigDecimal, java.math.BigDecimal)
     * @generated
     */
	EOperation getAdd__Operation__BigDecimal_BigDecimal();

	/**
     * Returns the meta object for class '{@link expressions.Subtract <em>Subtract</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Subtract</em>'.
     * @see expressions.Subtract
     * @generated
     */
	EClass getSubtract();

	/**
     * Returns the meta object for the '{@link expressions.Subtract#operation(java.math.BigDecimal, java.math.BigDecimal) <em>Operation</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Operation</em>' operation.
     * @see expressions.Subtract#operation(java.math.BigDecimal, java.math.BigDecimal)
     * @generated
     */
	EOperation getSubtract__Operation__BigDecimal_BigDecimal();

	/**
     * Returns the meta object for class '{@link expressions.Multiply <em>Multiply</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Multiply</em>'.
     * @see expressions.Multiply
     * @generated
     */
	EClass getMultiply();

	/**
     * Returns the meta object for the '{@link expressions.Multiply#operation(java.math.BigDecimal, java.math.BigDecimal) <em>Operation</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Operation</em>' operation.
     * @see expressions.Multiply#operation(java.math.BigDecimal, java.math.BigDecimal)
     * @generated
     */
	EOperation getMultiply__Operation__BigDecimal_BigDecimal();

	/**
     * Returns the meta object for class '{@link expressions.BigDecimalConstant <em>Big Decimal Constant</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Big Decimal Constant</em>'.
     * @see expressions.BigDecimalConstant
     * @generated
     */
	EClass getBigDecimalConstant();

	/**
     * Returns the meta object for the attribute '{@link expressions.BigDecimalConstant#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see expressions.BigDecimalConstant#getValue()
     * @see #getBigDecimalConstant()
     * @generated
     */
	EAttribute getBigDecimalConstant_Value();

	/**
     * Returns the meta object for the '{@link expressions.BigDecimalConstant#evaluate() <em>Evaluate</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Evaluate</em>' operation.
     * @see expressions.BigDecimalConstant#evaluate()
     * @generated
     */
	EOperation getBigDecimalConstant__Evaluate();

	/**
     * Returns the meta object for class '{@link expressions.DeclarationRef <em>Declaration Ref</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Declaration Ref</em>'.
     * @see expressions.DeclarationRef
     * @generated
     */
	EClass getDeclarationRef();

	/**
     * Returns the meta object for the reference '{@link expressions.DeclarationRef#getDeclaration <em>Declaration</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Declaration</em>'.
     * @see expressions.DeclarationRef#getDeclaration()
     * @see #getDeclarationRef()
     * @generated
     */
	EReference getDeclarationRef_Declaration();

	/**
     * Returns the meta object for the '{@link expressions.DeclarationRef#evaluate() <em>Evaluate</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Evaluate</em>' operation.
     * @see expressions.DeclarationRef#evaluate()
     * @generated
     */
	EOperation getDeclarationRef__Evaluate();

	/**
     * Returns the meta object for the '{@link expressions.DeclarationRef#hasCircularDependencies(org.eclipse.emf.ecore.EObject) <em>Has Circular Dependencies</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Has Circular Dependencies</em>' operation.
     * @see expressions.DeclarationRef#hasCircularDependencies(org.eclipse.emf.ecore.EObject)
     * @generated
     */
	EOperation getDeclarationRef__HasCircularDependencies__EObject();

	/**
     * Returns the meta object for class '{@link expressions.Divide <em>Divide</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Divide</em>'.
     * @see expressions.Divide
     * @generated
     */
	EClass getDivide();

	/**
     * Returns the meta object for the '{@link expressions.Divide#operation(java.math.BigDecimal, java.math.BigDecimal) <em>Operation</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Operation</em>' operation.
     * @see expressions.Divide#operation(java.math.BigDecimal, java.math.BigDecimal)
     * @generated
     */
	EOperation getDivide__Operation__BigDecimal_BigDecimal();

	/**
     * Returns the meta object for class '{@link expressions.BinaryExpression <em>Binary Expression</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Binary Expression</em>'.
     * @see expressions.BinaryExpression
     * @generated
     */
	EClass getBinaryExpression();

	/**
     * Returns the meta object for the containment reference '{@link expressions.BinaryExpression#getLeft <em>Left</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Left</em>'.
     * @see expressions.BinaryExpression#getLeft()
     * @see #getBinaryExpression()
     * @generated
     */
	EReference getBinaryExpression_Left();

	/**
     * Returns the meta object for the containment reference '{@link expressions.BinaryExpression#getRight <em>Right</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Right</em>'.
     * @see expressions.BinaryExpression#getRight()
     * @see #getBinaryExpression()
     * @generated
     */
	EReference getBinaryExpression_Right();

	/**
     * Returns the meta object for the '{@link expressions.BinaryExpression#evaluate() <em>Evaluate</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Evaluate</em>' operation.
     * @see expressions.BinaryExpression#evaluate()
     * @generated
     */
	EOperation getBinaryExpression__Evaluate();

	/**
     * Returns the meta object for the '{@link expressions.BinaryExpression#operation(java.math.BigDecimal, java.math.BigDecimal) <em>Operation</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Operation</em>' operation.
     * @see expressions.BinaryExpression#operation(java.math.BigDecimal, java.math.BigDecimal)
     * @generated
     */
	EOperation getBinaryExpression__Operation__BigDecimal_BigDecimal();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	ExpressionsFactory getExpressionsFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link expressions.impl.AbstractElementImpl <em>Abstract Element</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.AbstractElementImpl
         * @see expressions.impl.ExpressionsPackageImpl#getAbstractElement()
         * @generated
         */
		EClass ABSTRACT_ELEMENT = eINSTANCE.getAbstractElement();

		/**
         * The meta object literal for the '{@link expressions.impl.DeclarationImpl <em>Declaration</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.DeclarationImpl
         * @see expressions.impl.ExpressionsPackageImpl#getDeclaration()
         * @generated
         */
		EClass DECLARATION = eINSTANCE.getDeclaration();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute DECLARATION__NAME = eINSTANCE.getDeclaration_Name();

		/**
         * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DECLARATION__EXPRESSION = eINSTANCE.getDeclaration_Expression();

		/**
         * The meta object literal for the '<em><b>Fqn</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation DECLARATION___FQN = eINSTANCE.getDeclaration__Fqn();

		/**
         * The meta object literal for the '{@link expressions.impl.ExpressionImpl <em>Expression</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.ExpressionImpl
         * @see expressions.impl.ExpressionsPackageImpl#getExpression()
         * @generated
         */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
         * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation EXPRESSION___EVALUATE = eINSTANCE.getExpression__Evaluate();

		/**
         * The meta object literal for the '{@link expressions.impl.AddImpl <em>Add</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.AddImpl
         * @see expressions.impl.ExpressionsPackageImpl#getAdd()
         * @generated
         */
		EClass ADD = eINSTANCE.getAdd();

		/**
         * The meta object literal for the '<em><b>Operation</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation ADD___OPERATION__BIGDECIMAL_BIGDECIMAL = eINSTANCE.getAdd__Operation__BigDecimal_BigDecimal();

		/**
         * The meta object literal for the '{@link expressions.impl.SubtractImpl <em>Subtract</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.SubtractImpl
         * @see expressions.impl.ExpressionsPackageImpl#getSubtract()
         * @generated
         */
		EClass SUBTRACT = eINSTANCE.getSubtract();

		/**
         * The meta object literal for the '<em><b>Operation</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation SUBTRACT___OPERATION__BIGDECIMAL_BIGDECIMAL = eINSTANCE.getSubtract__Operation__BigDecimal_BigDecimal();

		/**
         * The meta object literal for the '{@link expressions.impl.MultiplyImpl <em>Multiply</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.MultiplyImpl
         * @see expressions.impl.ExpressionsPackageImpl#getMultiply()
         * @generated
         */
		EClass MULTIPLY = eINSTANCE.getMultiply();

		/**
         * The meta object literal for the '<em><b>Operation</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation MULTIPLY___OPERATION__BIGDECIMAL_BIGDECIMAL = eINSTANCE.getMultiply__Operation__BigDecimal_BigDecimal();

		/**
         * The meta object literal for the '{@link expressions.impl.BigDecimalConstantImpl <em>Big Decimal Constant</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.BigDecimalConstantImpl
         * @see expressions.impl.ExpressionsPackageImpl#getBigDecimalConstant()
         * @generated
         */
		EClass BIG_DECIMAL_CONSTANT = eINSTANCE.getBigDecimalConstant();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute BIG_DECIMAL_CONSTANT__VALUE = eINSTANCE.getBigDecimalConstant_Value();

		/**
         * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation BIG_DECIMAL_CONSTANT___EVALUATE = eINSTANCE.getBigDecimalConstant__Evaluate();

		/**
         * The meta object literal for the '{@link expressions.impl.DeclarationRefImpl <em>Declaration Ref</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.DeclarationRefImpl
         * @see expressions.impl.ExpressionsPackageImpl#getDeclarationRef()
         * @generated
         */
		EClass DECLARATION_REF = eINSTANCE.getDeclarationRef();

		/**
         * The meta object literal for the '<em><b>Declaration</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DECLARATION_REF__DECLARATION = eINSTANCE.getDeclarationRef_Declaration();

		/**
         * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation DECLARATION_REF___EVALUATE = eINSTANCE.getDeclarationRef__Evaluate();

		/**
         * The meta object literal for the '<em><b>Has Circular Dependencies</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation DECLARATION_REF___HAS_CIRCULAR_DEPENDENCIES__EOBJECT = eINSTANCE.getDeclarationRef__HasCircularDependencies__EObject();

		/**
         * The meta object literal for the '{@link expressions.impl.DivideImpl <em>Divide</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.DivideImpl
         * @see expressions.impl.ExpressionsPackageImpl#getDivide()
         * @generated
         */
		EClass DIVIDE = eINSTANCE.getDivide();

		/**
         * The meta object literal for the '<em><b>Operation</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation DIVIDE___OPERATION__BIGDECIMAL_BIGDECIMAL = eINSTANCE.getDivide__Operation__BigDecimal_BigDecimal();

		/**
         * The meta object literal for the '{@link expressions.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see expressions.impl.BinaryExpressionImpl
         * @see expressions.impl.ExpressionsPackageImpl#getBinaryExpression()
         * @generated
         */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
         * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference BINARY_EXPRESSION__LEFT = eINSTANCE.getBinaryExpression_Left();

		/**
         * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference BINARY_EXPRESSION__RIGHT = eINSTANCE.getBinaryExpression_Right();

		/**
         * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation BINARY_EXPRESSION___EVALUATE = eINSTANCE.getBinaryExpression__Evaluate();

		/**
         * The meta object literal for the '<em><b>Operation</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation BINARY_EXPRESSION___OPERATION__BIGDECIMAL_BIGDECIMAL = eINSTANCE.getBinaryExpression__Operation__BigDecimal_BigDecimal();

	}

} //ExpressionsPackage
