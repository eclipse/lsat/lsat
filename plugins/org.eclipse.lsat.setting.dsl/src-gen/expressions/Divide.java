/**
 */
package expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Divide</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see expressions.ExpressionsPackage#getDivide()
 * @model
 * @generated
 */
public interface Divide extends BinaryExpression {

} // Divide
