/**
 */
package expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subtract</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see expressions.ExpressionsPackage#getSubtract()
 * @model
 * @generated
 */
public interface Subtract extends BinaryExpression {

} // Subtract
