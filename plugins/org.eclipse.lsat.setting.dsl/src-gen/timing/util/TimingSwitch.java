/**
 */
package timing.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import timing.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see timing.TimingPackage
 * @generated
 */
public class TimingSwitch<T> extends Switch<T> {
	/**
     * The cached model package
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected static TimingPackage modelPackage;

	/**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public TimingSwitch() {
        if (modelPackage == null)
        {
            modelPackage = TimingPackage.eINSTANCE;
        }
    }

	/**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

	/**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID)
        {
            case TimingPackage.TIMING:
            {
                Timing timing = (Timing)theEObject;
                T result = caseTiming(timing);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case TimingPackage.FIXED_VALUE:
            {
                FixedValue fixedValue = (FixedValue)theEObject;
                T result = caseFixedValue(fixedValue);
                if (result == null) result = caseTiming(fixedValue);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case TimingPackage.ARRAY:
            {
                Array array = (Array)theEObject;
                T result = caseArray(array);
                if (result == null) result = caseTiming(array);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case TimingPackage.TRIANGULAR_DISTRIBUTION:
            {
                TriangularDistribution triangularDistribution = (TriangularDistribution)theEObject;
                T result = caseTriangularDistribution(triangularDistribution);
                if (result == null) result = caseDistribution(triangularDistribution);
                if (result == null) result = caseTiming(triangularDistribution);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case TimingPackage.DISTRIBUTION:
            {
                Distribution distribution = (Distribution)theEObject;
                T result = caseDistribution(distribution);
                if (result == null) result = caseTiming(distribution);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case TimingPackage.PERT_DISTRIBUTION:
            {
                PertDistribution pertDistribution = (PertDistribution)theEObject;
                T result = casePertDistribution(pertDistribution);
                if (result == null) result = caseDistribution(pertDistribution);
                if (result == null) result = caseTiming(pertDistribution);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case TimingPackage.NORMAL_DISTRIBUTION:
            {
                NormalDistribution normalDistribution = (NormalDistribution)theEObject;
                T result = caseNormalDistribution(normalDistribution);
                if (result == null) result = caseDistribution(normalDistribution);
                if (result == null) result = caseTiming(normalDistribution);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case TimingPackage.ENUMERATED_DISTRIBUTION:
            {
                EnumeratedDistribution enumeratedDistribution = (EnumeratedDistribution)theEObject;
                T result = caseEnumeratedDistribution(enumeratedDistribution);
                if (result == null) result = caseDistribution(enumeratedDistribution);
                if (result == null) result = caseTiming(enumeratedDistribution);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Timing</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Timing</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseTiming(Timing object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Fixed Value</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Fixed Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseFixedValue(FixedValue object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Array</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Array</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseArray(Array object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Triangular Distribution</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Triangular Distribution</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseTriangularDistribution(TriangularDistribution object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Distribution</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Distribution</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseDistribution(Distribution object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Pert Distribution</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Pert Distribution</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T casePertDistribution(PertDistribution object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Normal Distribution</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Normal Distribution</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseNormalDistribution(NormalDistribution object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>Enumerated Distribution</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Enumerated Distribution</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
	public T caseEnumeratedDistribution(EnumeratedDistribution object) {
        return null;
    }

	/**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
	@Override
	public T defaultCase(EObject object) {
        return null;
    }

} //TimingSwitch
