/**
 */
package timing.impl;

import expressions.Expression;
import java.math.BigDecimal;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import timing.Array;
import timing.TimingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link timing.impl.ArrayImpl#getValues <em>Values</em>}</li>
 *   <li>{@link timing.impl.ArrayImpl#getValuesExp <em>Values Exp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArrayImpl extends MinimalEObjectImpl.Container implements Array {
	/**
     * The cached value of the '{@link #getValuesExp() <em>Values Exp</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getValuesExp()
     * @generated
     * @ordered
     */
	protected EList<Expression> valuesExp;
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ArrayImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return TimingPackage.Literals.ARRAY;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<BigDecimal> getValues() {
        return valuesExp==null ? null:  new org.eclipse.emf.common.util.BasicEList<BigDecimal>(valuesExp.stream().map(e->e.evaluate()).collect(java.util.stream.Collectors.toList()));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Expression> getValuesExp() {
        if (valuesExp == null)
        {
            valuesExp = new EObjectContainmentEList<Expression>(Expression.class, this, TimingPackage.ARRAY__VALUES_EXP);
        }
        return valuesExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case TimingPackage.ARRAY__VALUES_EXP:
                return ((InternalEList<?>)getValuesExp()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case TimingPackage.ARRAY__VALUES:
                return getValues();
            case TimingPackage.ARRAY__VALUES_EXP:
                return getValuesExp();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case TimingPackage.ARRAY__VALUES_EXP:
                getValuesExp().clear();
                getValuesExp().addAll((Collection<? extends Expression>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case TimingPackage.ARRAY__VALUES_EXP:
                getValuesExp().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case TimingPackage.ARRAY__VALUES:
                return !getValues().isEmpty();
            case TimingPackage.ARRAY__VALUES_EXP:
                return valuesExp != null && !valuesExp.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //ArrayImpl
