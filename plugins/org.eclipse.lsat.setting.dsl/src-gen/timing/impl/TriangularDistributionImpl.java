/**
 */
package timing.impl;

import expressions.Expression;
import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import timing.TimingPackage;
import timing.TriangularDistribution;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triangular Distribution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link timing.impl.TriangularDistributionImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link timing.impl.TriangularDistributionImpl#getMin <em>Min</em>}</li>
 *   <li>{@link timing.impl.TriangularDistributionImpl#getMax <em>Max</em>}</li>
 *   <li>{@link timing.impl.TriangularDistributionImpl#getModeExp <em>Mode Exp</em>}</li>
 *   <li>{@link timing.impl.TriangularDistributionImpl#getMinExp <em>Min Exp</em>}</li>
 *   <li>{@link timing.impl.TriangularDistributionImpl#getMaxExp <em>Max Exp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TriangularDistributionImpl extends DistributionImpl implements TriangularDistribution {
	/**
     * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMode()
     * @generated
     * @ordered
     */
	protected static final BigDecimal MODE_EDEFAULT = null;

	/**
     * The default value of the '{@link #getMin() <em>Min</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMin()
     * @generated
     * @ordered
     */
	protected static final BigDecimal MIN_EDEFAULT = null;

	/**
     * The default value of the '{@link #getMax() <em>Max</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMax()
     * @generated
     * @ordered
     */
	protected static final BigDecimal MAX_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getModeExp() <em>Mode Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getModeExp()
     * @generated
     * @ordered
     */
	protected Expression modeExp;

	/**
     * The cached value of the '{@link #getMinExp() <em>Min Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMinExp()
     * @generated
     * @ordered
     */
	protected Expression minExp;

	/**
     * The cached value of the '{@link #getMaxExp() <em>Max Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getMaxExp()
     * @generated
     * @ordered
     */
	protected Expression maxExp;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected TriangularDistributionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return TimingPackage.Literals.TRIANGULAR_DISTRIBUTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getMode() {
        return modeExp==null ? null:  modeExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getMin() {
        return minExp==null ? null:  minExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getMax() {
        return maxExp==null ? null:  maxExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getModeExp() {
        return modeExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetModeExp(Expression newModeExp, NotificationChain msgs) {
        Expression oldModeExp = modeExp;
        modeExp = newModeExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP, oldModeExp, newModeExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setModeExp(Expression newModeExp) {
        if (newModeExp != modeExp)
        {
            NotificationChain msgs = null;
            if (modeExp != null)
                msgs = ((InternalEObject)modeExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP, null, msgs);
            if (newModeExp != null)
                msgs = ((InternalEObject)newModeExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP, null, msgs);
            msgs = basicSetModeExp(newModeExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP, newModeExp, newModeExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getMinExp() {
        return minExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetMinExp(Expression newMinExp, NotificationChain msgs) {
        Expression oldMinExp = minExp;
        minExp = newMinExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP, oldMinExp, newMinExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMinExp(Expression newMinExp) {
        if (newMinExp != minExp)
        {
            NotificationChain msgs = null;
            if (minExp != null)
                msgs = ((InternalEObject)minExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP, null, msgs);
            if (newMinExp != null)
                msgs = ((InternalEObject)newMinExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP, null, msgs);
            msgs = basicSetMinExp(newMinExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP, newMinExp, newMinExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getMaxExp() {
        return maxExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetMaxExp(Expression newMaxExp, NotificationChain msgs) {
        Expression oldMaxExp = maxExp;
        maxExp = newMaxExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP, oldMaxExp, newMaxExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setMaxExp(Expression newMaxExp) {
        if (newMaxExp != maxExp)
        {
            NotificationChain msgs = null;
            if (maxExp != null)
                msgs = ((InternalEObject)maxExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP, null, msgs);
            if (newMaxExp != null)
                msgs = ((InternalEObject)newMaxExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP, null, msgs);
            msgs = basicSetMaxExp(newMaxExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP, newMaxExp, newMaxExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP:
                return basicSetModeExp(null, msgs);
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP:
                return basicSetMinExp(null, msgs);
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP:
                return basicSetMaxExp(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MODE:
                return getMode();
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MIN:
                return getMin();
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MAX:
                return getMax();
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP:
                return getModeExp();
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP:
                return getMinExp();
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP:
                return getMaxExp();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP:
                setModeExp((Expression)newValue);
                return;
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP:
                setMinExp((Expression)newValue);
                return;
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP:
                setMaxExp((Expression)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP:
                setModeExp((Expression)null);
                return;
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP:
                setMinExp((Expression)null);
                return;
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP:
                setMaxExp((Expression)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MODE:
                return MODE_EDEFAULT == null ? getMode() != null : !MODE_EDEFAULT.equals(getMode());
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MIN:
                return MIN_EDEFAULT == null ? getMin() != null : !MIN_EDEFAULT.equals(getMin());
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MAX:
                return MAX_EDEFAULT == null ? getMax() != null : !MAX_EDEFAULT.equals(getMax());
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MODE_EXP:
                return modeExp != null;
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MIN_EXP:
                return minExp != null;
            case TimingPackage.TRIANGULAR_DISTRIBUTION__MAX_EXP:
                return maxExp != null;
        }
        return super.eIsSet(featureID);
    }

} //TriangularDistributionImpl
