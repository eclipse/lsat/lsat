/**
 */
package timing.impl;

import expressions.Expression;
import java.math.BigDecimal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import timing.Distribution;
import timing.TimingPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Distribution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link timing.impl.DistributionImpl#getDefault <em>Default</em>}</li>
 *   <li>{@link timing.impl.DistributionImpl#getDefaultExp <em>Default Exp</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DistributionImpl extends MinimalEObjectImpl.Container implements Distribution {
	/**
     * The default value of the '{@link #getDefault() <em>Default</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDefault()
     * @generated
     * @ordered
     */
	protected static final BigDecimal DEFAULT_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getDefaultExp() <em>Default Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDefaultExp()
     * @generated
     * @ordered
     */
	protected Expression defaultExp;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DistributionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return TimingPackage.Literals.DISTRIBUTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public BigDecimal getDefault() {
        return defaultExp==null ? null:  defaultExp.evaluate();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Expression getDefaultExp() {
        return defaultExp;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetDefaultExp(Expression newDefaultExp, NotificationChain msgs) {
        Expression oldDefaultExp = defaultExp;
        defaultExp = newDefaultExp;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TimingPackage.DISTRIBUTION__DEFAULT_EXP, oldDefaultExp, newDefaultExp);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setDefaultExp(Expression newDefaultExp) {
        if (newDefaultExp != defaultExp)
        {
            NotificationChain msgs = null;
            if (defaultExp != null)
                msgs = ((InternalEObject)defaultExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TimingPackage.DISTRIBUTION__DEFAULT_EXP, null, msgs);
            if (newDefaultExp != null)
                msgs = ((InternalEObject)newDefaultExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TimingPackage.DISTRIBUTION__DEFAULT_EXP, null, msgs);
            msgs = basicSetDefaultExp(newDefaultExp, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, TimingPackage.DISTRIBUTION__DEFAULT_EXP, newDefaultExp, newDefaultExp));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case TimingPackage.DISTRIBUTION__DEFAULT_EXP:
                return basicSetDefaultExp(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case TimingPackage.DISTRIBUTION__DEFAULT:
                return getDefault();
            case TimingPackage.DISTRIBUTION__DEFAULT_EXP:
                return getDefaultExp();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case TimingPackage.DISTRIBUTION__DEFAULT_EXP:
                setDefaultExp((Expression)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case TimingPackage.DISTRIBUTION__DEFAULT_EXP:
                setDefaultExp((Expression)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case TimingPackage.DISTRIBUTION__DEFAULT:
                return DEFAULT_EDEFAULT == null ? getDefault() != null : !DEFAULT_EDEFAULT.equals(getDefault());
            case TimingPackage.DISTRIBUTION__DEFAULT_EXP:
                return defaultExp != null;
        }
        return super.eIsSet(featureID);
    }

} //DistributionImpl
