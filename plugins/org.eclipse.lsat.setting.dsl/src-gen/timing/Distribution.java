/**
 */
package timing;

import expressions.Expression;
import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Distribution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link timing.Distribution#getDefault <em>Default</em>}</li>
 *   <li>{@link timing.Distribution#getDefaultExp <em>Default Exp</em>}</li>
 * </ul>
 *
 * @see timing.TimingPackage#getDistribution()
 * @model abstract="true"
 * @generated
 */
public interface Distribution extends Timing {
	/**
     * Returns the value of the '<em><b>Default</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Default</em>' attribute.
     * @see timing.TimingPackage#getDistribution_Default()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getDefault();

	/**
     * Returns the value of the '<em><b>Default Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Default Exp</em>' containment reference.
     * @see #setDefaultExp(Expression)
     * @see timing.TimingPackage#getDistribution_DefaultExp()
     * @model containment="true"
     * @generated
     */
	Expression getDefaultExp();

	/**
     * Sets the value of the '{@link timing.Distribution#getDefaultExp <em>Default Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Default Exp</em>' containment reference.
     * @see #getDefaultExp()
     * @generated
     */
	void setDefaultExp(Expression value);

} // Distribution
