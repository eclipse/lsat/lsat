/**
 */
package timing;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see timing.TimingPackage
 * @generated
 */
public interface TimingFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	TimingFactory eINSTANCE = timing.impl.TimingFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Fixed Value</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Fixed Value</em>'.
     * @generated
     */
	FixedValue createFixedValue();

	/**
     * Returns a new object of class '<em>Array</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Array</em>'.
     * @generated
     */
	Array createArray();

	/**
     * Returns a new object of class '<em>Triangular Distribution</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Triangular Distribution</em>'.
     * @generated
     */
	TriangularDistribution createTriangularDistribution();

	/**
     * Returns a new object of class '<em>Pert Distribution</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Pert Distribution</em>'.
     * @generated
     */
	PertDistribution createPertDistribution();

	/**
     * Returns a new object of class '<em>Normal Distribution</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Normal Distribution</em>'.
     * @generated
     */
	NormalDistribution createNormalDistribution();

	/**
     * Returns a new object of class '<em>Enumerated Distribution</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Enumerated Distribution</em>'.
     * @generated
     */
	EnumeratedDistribution createEnumeratedDistribution();

	/**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	TimingPackage getTimingPackage();

} //TimingFactory
