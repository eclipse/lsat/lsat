/**
 */
package timing;

import expressions.Expression;
import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pert Distribution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link timing.PertDistribution#getMode <em>Mode</em>}</li>
 *   <li>{@link timing.PertDistribution#getMin <em>Min</em>}</li>
 *   <li>{@link timing.PertDistribution#getMax <em>Max</em>}</li>
 *   <li>{@link timing.PertDistribution#getModeExp <em>Mode Exp</em>}</li>
 *   <li>{@link timing.PertDistribution#getMinExp <em>Min Exp</em>}</li>
 *   <li>{@link timing.PertDistribution#getMaxExp <em>Max Exp</em>}</li>
 *   <li>{@link timing.PertDistribution#getGamma <em>Gamma</em>}</li>
 *   <li>{@link timing.PertDistribution#getGammaExp <em>Gamma Exp</em>}</li>
 * </ul>
 *
 * @see timing.TimingPackage#getPertDistribution()
 * @model
 * @generated
 */
public interface PertDistribution extends Distribution {
	/**
     * Returns the value of the '<em><b>Mode</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mode</em>' attribute.
     * @see timing.TimingPackage#getPertDistribution_Mode()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMode();

	/**
     * Returns the value of the '<em><b>Min</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Min</em>' attribute.
     * @see timing.TimingPackage#getPertDistribution_Min()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMin();

	/**
     * Returns the value of the '<em><b>Max</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Max</em>' attribute.
     * @see timing.TimingPackage#getPertDistribution_Max()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getMax();

	/**
     * Returns the value of the '<em><b>Mode Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Mode Exp</em>' containment reference.
     * @see #setModeExp(Expression)
     * @see timing.TimingPackage#getPertDistribution_ModeExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getModeExp();

	/**
     * Sets the value of the '{@link timing.PertDistribution#getModeExp <em>Mode Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Mode Exp</em>' containment reference.
     * @see #getModeExp()
     * @generated
     */
	void setModeExp(Expression value);

	/**
     * Returns the value of the '<em><b>Min Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Min Exp</em>' containment reference.
     * @see #setMinExp(Expression)
     * @see timing.TimingPackage#getPertDistribution_MinExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getMinExp();

	/**
     * Sets the value of the '{@link timing.PertDistribution#getMinExp <em>Min Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Min Exp</em>' containment reference.
     * @see #getMinExp()
     * @generated
     */
	void setMinExp(Expression value);

	/**
     * Returns the value of the '<em><b>Max Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Max Exp</em>' containment reference.
     * @see #setMaxExp(Expression)
     * @see timing.TimingPackage#getPertDistribution_MaxExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getMaxExp();

	/**
     * Sets the value of the '{@link timing.PertDistribution#getMaxExp <em>Max Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Max Exp</em>' containment reference.
     * @see #getMaxExp()
     * @generated
     */
	void setMaxExp(Expression value);

	/**
     * Returns the value of the '<em><b>Gamma</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gamma</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Gamma</em>' attribute.
     * @see timing.TimingPackage#getPertDistribution_Gamma()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	BigDecimal getGamma();

	/**
     * Returns the value of the '<em><b>Gamma Exp</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Gamma Exp</em>' containment reference.
     * @see #setGammaExp(Expression)
     * @see timing.TimingPackage#getPertDistribution_GammaExp()
     * @model containment="true" required="true"
     * @generated
     */
	Expression getGammaExp();

	/**
     * Sets the value of the '{@link timing.PertDistribution#getGammaExp <em>Gamma Exp</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Gamma Exp</em>' containment reference.
     * @see #getGammaExp()
     * @generated
     */
	void setGammaExp(Expression value);

} // PertDistribution
