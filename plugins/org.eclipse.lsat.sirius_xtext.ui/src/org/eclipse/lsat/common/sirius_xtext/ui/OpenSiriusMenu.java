/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.sirius_xtext.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.sirius.business.api.dialect.DialectManager;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.ui.business.api.dialect.DialectUIManager;
import org.eclipse.sirius.ui.tools.internal.editor.MenuHelper;
import org.eclipse.sirius.viewpoint.DRepresentationDescriptor;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

public class OpenSiriusMenu extends CompoundContributionItem {
    private final AdapterFactory adapter_factory = DialectUIManager.INSTANCE.createAdapterFactory();

    @Override
    protected IContributionItem[] getContributionItems() {
        List<IContributionItem> menuContent = new ArrayList<>();
        EObject context = getContext();

        // If models are loaded in same ResourceSet we could use (but this is not the case, yet)
        // Session siriusSession = SessionManager.INSTANCE.getSession(context);
        // EObject semantic = context;

        // Instead, we have to use this...
        URI contextUri = context.eResource().getURI();
        URI representationsUri = contextUri.trimSegments(contextUri.segmentCount() - 2)
                .appendSegment("representations.aird");
        Session siriusSession = SessionManager.INSTANCE.getSession(representationsUri, new NullProgressMonitor());
        if (null != siriusSession) {
            URI semanticUri = contextUri.appendFragment(context.eResource().getURIFragment(context));
            EObject semantic = siriusSession.getTransactionalEditingDomain().getResourceSet().getEObject(semanticUri,
                    true);

            Collection<DRepresentationDescriptor> representations = DialectManager.INSTANCE
                    .getRepresentationDescriptors(semantic, siriusSession);
            for (DRepresentationDescriptor representation: representations) {
                menuContent.add(new ActionContributionItem(
                        MenuHelper.buildOpenRepresentationAction(siriusSession, representation, adapter_factory)));
            }
        }
        return menuContent.toArray(new IContributionItem[menuContent.size()]);
    }

    private EObject getContext() {
        XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
        ISelection selection = xtextEditor.getSelectionProvider().getSelection();
        if (selection instanceof ITextSelection) {
            final int offset = ((ITextSelection)selection).getOffset();
            return xtextEditor.getDocument().readOnly(new IUnitOfWork<EObject, XtextResource>() {
                @Override
                public EObject exec(XtextResource state) throws Exception {
                    return new EObjectAtOffsetHelper().resolveElementAt(state, offset);
                }
            });
        }
        return null;
    }
}
