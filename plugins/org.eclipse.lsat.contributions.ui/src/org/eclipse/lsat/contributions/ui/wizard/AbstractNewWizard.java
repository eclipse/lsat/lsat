/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.wizard;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.lsat.contributions.ui.Activator;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ISetSelectionTarget;

import machine.Import;
import machine.MachineFactory;

abstract class AbstractNewWizard<T extends EObject> extends Wizard implements INewWizard {
    private final String itsModelName;

    private NewModelCreationPage itsNewModelCreationPage;

    private IWorkbench itsWorkbench;

    private IStructuredSelection itsSelection;

    public AbstractNewWizard(String modelName) {
        itsModelName = modelName;
        setWindowTitle("New");
    }

    @Override
    public void init(IWorkbench workbench, IStructuredSelection selection) {
        itsWorkbench = workbench;
        itsSelection = selection;
        itsNewModelCreationPage = new NewModelCreationPage(getClass().getSimpleName() + "_NewModelCreationPage",
                selection);
    }

    protected String baseName(IResource resource) {
        String baseName = resource.getName();
        String extension = resource.getFileExtension();
        if (null != extension) {
            baseName = baseName.substring(0, baseName.length() - 1 - extension.length());
        }
        return baseName;
    }

    protected ResourceSet createResourceSet() {
        return new ResourceSetImpl();
    }

    protected abstract Collection<String> getFileExtensions();

    protected abstract T createModel(IFile modelFile, Collection<Import> imports);

    @Override
    public void addPages() {
        addPage(itsNewModelCreationPage);
    }

    @Override
    public boolean performFinish() {
        try {
            // Remember the file.
            final IFile modelFile = itsNewModelCreationPage.getModelFile();
            final URI modelURI = URI.createPlatformResourceURI(modelFile.getFullPath().toString(), true);

            // Create the imports, based on the user selection
            final ArrayList<Import> imports = new ArrayList<Import>(itsSelection.size());
            for (Object selected: itsSelection.toArray()) {
                if (selected instanceof IFile) {
                    Import _import = MachineFactory.eINSTANCE.createImport();
                    URI importUri = URI.createPlatformResourceURI(((IFile)selected).getFullPath().toString(), true);
                    _import.setImportURI(importUri.deresolve(modelURI).toString());
                    imports.add(_import);
                }
            }

            // Do the work within an operation.
            WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
                @Override
                protected void execute(IProgressMonitor progressMonitor) {
                    try {
                        ResourceSet resourceSet = createResourceSet();
                        Resource resource = resourceSet.createResource(modelURI);
                        EObject model = createModel(modelFile, imports);
                        resource.getContents().add(model);
                        resource.save(Collections.emptyMap());
                    } catch (Exception exception) {
                        Activator.getDefault().getLog().log(
                                new Status(Status.ERROR, Activator.PLUGIN_ID, "Failed to create model", exception));
                    } finally {
                        progressMonitor.done();
                    }
                }
            };

            getContainer().run(false, false, operation);

            // Select the new file resource in the current view.
            IWorkbenchWindow workbenchWindow = itsWorkbench.getActiveWorkbenchWindow();
            IWorkbenchPage page = workbenchWindow.getActivePage();
            final IWorkbenchPart activePart = page.getActivePart();
            if (activePart instanceof ISetSelectionTarget) {
                final ISelection targetSelection = new StructuredSelection(modelFile);
                getShell().getDisplay().asyncExec(new Runnable() {
                    public void run() {
                        ((ISetSelectionTarget)activePart).selectReveal(targetSelection);
                    }
                });
            }

            // Open an editor on the new file.
            try {
                page.openEditor(new FileEditorInput(modelFile),
                        itsWorkbench.getEditorRegistry().getDefaultEditor(modelFile.getFullPath().toString()).getId());
            } catch (PartInitException exception) {
                MessageDialog.openError(workbenchWindow.getShell(), "Failed to open editor", exception.getMessage());
                return false;
            }

            return true;
        } catch (Exception exception) {
            Activator.getDefault().getLog()
                    .log(new Status(Status.ERROR, Activator.PLUGIN_ID, "Failed to create model", exception));
            return false;
        }
    }

    protected class NewModelCreationPage extends WizardNewFileCreationPage {
        public NewModelCreationPage(String pageId, IStructuredSelection selection) {
            super(pageId, selection);
            setTitle(format("%s Model", itsModelName));
            setDescription(format("Create a new %s model", itsModelName));
            setFileName(getFileNameSuggestion(selection));
        }

        private String getFileNameSuggestion(IStructuredSelection selection) {
            String fileName = itsModelName.replace(' ', '_').toLowerCase();
            String fileExtension = getFileExtensions().iterator().next();
            String newPath = fileName + "." + fileExtension;

            if (!selection.isEmpty()) {
                IResource selected = (IResource)selection.getFirstElement();
                fileName = baseName(selected);

                newPath = fileName + "." + fileExtension;
                int counter = 1;
                while (null != selected.getParent().findMember(newPath)) {
                    newPath = fileName + "_" + (counter++) + "." + fileExtension;
                }
            }

            return newPath;
        }

        @Override
        protected boolean validatePage() {
            if (super.validatePage()) {
                String extension = new Path(getFileName()).getFileExtension();
                if (extension == null || !getFileExtensions().contains(extension)) {
                    setErrorMessage("Please specify a valid file extension for this type of model.");
                    return false;
                }
                return true;
            }
            return false;
        }

        public IFile getModelFile() {
            return ResourcesPlugin.getWorkspace().getRoot().getFile(getContainerFullPath().append(getFileName()));
        }
    }
}
