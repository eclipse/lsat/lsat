/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.wizard;

import java.util.Arrays;
import java.util.Collection;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResourceSet;

import activity.ActivityFactory;
import activity.ActivitySet;
import machine.Import;

public class WizardActivity extends AbstractNewWizard<ActivitySet> {
    public static final String ID = "org.eclipse.lsat.contributions.ui.WizardActivity";

    public WizardActivity() {
        super("Activities");
    }

    @Override
    protected ResourceSet createResourceSet() {
        return new XtextResourceSet();
    }

    @Override
    protected Collection<String> getFileExtensions() {
        return Arrays.asList("activity");
    }

    @Override
    protected ActivitySet createModel(IFile modelFile, Collection<Import> imports) {
        ActivitySet activitySet = ActivityFactory.eINSTANCE.createActivitySet();
        activitySet.getImports().addAll(imports);
        return activitySet;
    }
}
