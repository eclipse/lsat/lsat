/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.project;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.lsat.contributions.ui.Activator;
import org.eclipse.sirius.business.api.modelingproject.ModelingProject;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.ui.business.api.session.UserSession;
import org.eclipse.sirius.ui.tools.api.project.ModelingProjectManager;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.ui.XtextProjectHelper;

import machine.Machine;
import machine.MachineFactory;

public class WizardProject extends Wizard implements INewWizard {
    public static final String ID = "org.eclipse.lsat.contributions.ui.WizardProject";

    /**
     * This is a new project wizard page.
     */
    private WizardNewProjectCreationPage newProjectPage;

    public WizardProject() {
        super();
        setNeedsProgressMonitor(true);
    }

    @Override
    public boolean performFinish() {
        boolean finished = true;
        try {
            final String projectName = newProjectPage.getProjectName();
            final IPath locationPath = newProjectPage.getLocationPath();
            getContainer().run(false, false, new IRunnableWithProgress() {
                @Override
                public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
                    try {
                        IProject project = ModelingProjectManager.INSTANCE.createNewModelingProject(projectName,
                                locationPath, true, monitor);
                        LogisticsNatureHandler.addNatures(project, LogisticsProjectNature.NATURE_ID,
                                XtextProjectHelper.NATURE_ID);

                        // Replace illegal Xtext identifier characters with underscore when assigning the machine type.
                        final String machineType = projectName.replaceAll("\\W", "_");
                        Machine machine = MachineFactory.eINSTANCE.createMachine();
                        machine.setType(machineType);

                        IFile machineFile = project.getFile(projectName + ".machine");
                        URI machineURI = URI.createPlatformResourceURI(machineFile.getFullPath().toString(), true);
                        XtextResourceSet resourceSet = new XtextResourceSet();
                        Resource machineResource = resourceSet.createResource(machineURI);
                        machineResource.getContents().add(machine);
                        machineResource.save(Collections.emptyMap());
                        project.refreshLocal(IResource.DEPTH_ONE, monitor);

                        IFile representationsFile = project.getFile(ModelingProject.DEFAULT_REPRESENTATIONS_FILE_NAME);
                        URI representationsURI = URI
                                .createPlatformResourceURI(representationsFile.getFullPath().toString(), true);
                        Session session = SessionManager.INSTANCE.getSession(representationsURI, monitor);
                        UserSession userSession = UserSession.from(session);
                        userSession.selectViewpoint("SymbolicPosition");

                        // Re-opening the project will open the default editors
                        userSession.save(monitor);
                        userSession.close(monitor);
                        project.close(monitor);
                        project.open(monitor);
                    } catch (final CoreException | IOException e) {
                        throw new InvocationTargetException(e);
                    }
                }
            });
        } catch (InvocationTargetException e) {
            final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR, e.getMessage(), e);
            Activator.getDefault().getLog().log(status);
            finished = false;
        } catch (InterruptedException e) {
            final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, IStatus.ERROR, e.getMessage(), e);
            Activator.getDefault().getLog().log(status);
            finished = false;
        }
        return finished;
    }

    @Override
    public void init(IWorkbench workbench, IStructuredSelection selection) {
        setWindowTitle("New Logistics Project");
    }

    @Override
    public void addPages() {
        newProjectPage = new WizardNewProjectCreationPage("Create a Logistics Project");
        newProjectPage.setInitialProjectName("");
        newProjectPage.setTitle("Create a Logistics Project");
        newProjectPage.setDescription("Enter a project name");
        addPage(newProjectPage);
        super.addPages();
    }
}
