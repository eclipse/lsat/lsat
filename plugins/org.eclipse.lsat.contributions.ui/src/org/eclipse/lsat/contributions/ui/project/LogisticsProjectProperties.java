/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.project;

import java.util.Objects;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.lsat.contributions.ui.Activator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.dialogs.PropertyPage;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import setting.SettingActivator;

public class LogisticsProjectProperties extends PropertyPage implements IWorkbenchPropertyPage {
    private Text text;

    @Override
    protected Control createContents(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        GridLayout gl_container = new GridLayout();
        gl_container.numColumns = 3;
        container.setLayout(gl_container);

        Label lblPhysicalSettings = new Label(container, SWT.NONE);
        lblPhysicalSettings.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblPhysicalSettings.setText("Physical settings:");

        text = new Text(container, SWT.BORDER);
        text.setEditable(false);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        try {
            text.setText(SettingActivator.getDefault().getSettingIResource(getProject()).getProjectRelativePath()
                    .toString());
        } catch (Exception e) {
            // Ignore
        }

        Button btnBrowse = new Button(container, SWT.NONE);
        btnBrowse.setText("Browse...");
        btnBrowse.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                IFile settingIResource = selectSettingIResource();
                text.setText(null == settingIResource ? "" : settingIResource.getProjectRelativePath().toString());
            }
        });
        return container;
    }

    @Override
    public boolean performOk() {
        Preferences preferences = SettingActivator.getDefault().getProjectPreferences(getProject());
        IResource settingIResource = getProject().findMember(text.getText());
        if (null == settingIResource) {
            preferences.remove(SettingActivator.PREFERENCE_SETTINGS);
        } else {
            preferences.put(SettingActivator.PREFERENCE_SETTINGS, settingIResource.getProjectRelativePath().toString());
        }
        try {
            preferences.flush();
        } catch (BackingStoreException e) {
            Activator.getDefault().getLog().log(new Status(Status.ERROR, Activator.PLUGIN_ID, e.getMessage(), e));
        }
        return true;
    }

    private IFile selectSettingIResource() {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(getShell(), new WorkbenchLabelProvider(),
                new BaseWorkbenchContentProvider());
        dialog.setTitle("Select physical settings");
        dialog.setMessage("Select the active setting file for this project");
        dialog.setAllowMultiple(false);
        dialog.setInput(getProject());
        // filter for specific files and elements only
        dialog.addFilter(new ViewerFilter() {
            public boolean select(Viewer viewer, Object parentElement, Object element) {
                if (element instanceof IFile) {
                    return Objects.equals("setting", ((IFile)element).getFileExtension());
                }
                return element instanceof IContainer;
            }
        });
        dialog.setValidator(new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                if (selection.length <= 0) {
                    return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Please select a setting file!", null);
                }
                return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, "OK", null);
            }
        });
        IResource settingIResource = getProject().findMember(text.getText());
        if (null != settingIResource) {
            dialog.setInitialSelection(settingIResource);
        }
        ;
        return ElementTreeSelectionDialog.OK == dialog.open() ? (IFile)dialog.getFirstResult() : null;
    }

    private IProject getProject() {
        return IProject.class.cast(getElement().getAdapter(IProject.class));
    }
}
