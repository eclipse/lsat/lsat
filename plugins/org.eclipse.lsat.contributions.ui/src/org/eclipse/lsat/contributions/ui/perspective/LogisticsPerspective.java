/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.contributions.ui.perspective;

import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.lsat.contributions.ui.project.WizardProject;
import org.eclipse.lsat.contributions.ui.wizard.WizardActivity;
import org.eclipse.lsat.contributions.ui.wizard.WizardDispatching;
import org.eclipse.lsat.contributions.ui.wizard.WizardMachine;
import org.eclipse.lsat.contributions.ui.wizard.WizardSettings;
import org.eclipse.sirius.ui.tools.api.views.modelexplorerview.IModelExplorerView;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class LogisticsPerspective implements IPerspectiveFactory {
    public static final String ID = "org.eclipse.lsat.contributions.ui.LogisticsPerspective";

    protected static final String ID_MOTION_VIEW = "org.eclipse.lsat.timing.view.XYPlotView";

    protected static final String ID_ERROR_LOG = "org.eclipse.pde.runtime.LogView";

    protected static final String ID_LAYOUT_VIEW = "org.eclipse.elk.core.views.layout";

    @Override
    public void createInitialLayout(IPageLayout layout) {
        defineActions(layout);
        defineLayout(layout);
    }

    /**
     * add items and actions set to the window
     *
     * @param layout layout of the perspective
     */
    private void defineActions(final IPageLayout layout) {
        // Wizards
        layout.addNewWizardShortcut(WizardProject.ID);
        layout.addNewWizardShortcut(WizardDispatching.ID);
        layout.addNewWizardShortcut(WizardActivity.ID);
        layout.addNewWizardShortcut(WizardMachine.ID);
        layout.addNewWizardShortcut(WizardSettings.ID);

        // Show view shortcuts
        layout.addShowViewShortcut(IModelExplorerView.ID);
        layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
        layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
        layout.addShowViewShortcut(IPageLayout.ID_PROBLEM_VIEW);
        layout.addShowViewShortcut(ID_ERROR_LOG);
        layout.addShowViewShortcut(ID_MOTION_VIEW);

        // Show action sets
        layout.addActionSet(IDebugUIConstants.LAUNCH_ACTION_SET);
    }

    /**
     * add views to the layout
     *
     * @param layout layout of the perspective
     */
    private void defineLayout(final IPageLayout layout) {
        final String editorArea = layout.getEditorArea();
        layout.addView(IModelExplorerView.ID, IPageLayout.LEFT, (float)0.25, editorArea);
        layout.addView(IPageLayout.ID_OUTLINE, IPageLayout.BOTTOM, 0.50f, IModelExplorerView.ID);

        // Place problem, properties and advance views to bottom of editor area.
        final IFolderLayout bottom = layout.createFolder("bottom", IPageLayout.BOTTOM, (float)0.65, editorArea);
        bottom.addView(IPageLayout.ID_PROBLEM_VIEW);
        bottom.addView(IPageLayout.ID_PROP_SHEET);
        bottom.addView(ID_MOTION_VIEW);
        bottom.addView(ID_ERROR_LOG);
    }
}
