/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import expressions.Expression;
import machine.Axis;
import machine.IResource;
import machine.Peripheral;
import machine.Profile;
import machine.SymbolicPosition;
import setting.MotionProfileSettings;
import setting.MotionSettings;
import setting.PhysicalLocation;
import setting.PhysicalSettings;
import setting.SettingUtil;
import setting.Settings;
import setting.impl.MotionArgumentsMapEntryImpl;

public class SettingService {
    public MotionProfileSettings getMotionProfileSettings(MotionSettings settings, Profile profile) {
        return settings.getProfileSettings().get(profile);
    }

    public PhysicalLocation getPhysicalLocation(MotionSettings settings, SymbolicPosition symbolicPosition) {
        return settings.getLocationSettings().get(symbolicPosition.getPosition(settings.getEntry().getKey()));
    }

    public Collection<MotionArgumentsMapEntryImpl> getMotionParameters(Peripheral peripheral, Axis axis)
            throws IOException
    {
        Map<String, MotionArgumentsMapEntryImpl> uniqueMotionArguments = new LinkedHashMap<>();
        for (MotionProfileSettings settings: getMotionSettings(peripheral, axis).getProfileSettings().values()) {
            for (Map.Entry<String, Expression> entry: settings.getMotionArguments()) {
                uniqueMotionArguments.putIfAbsent(entry.getKey(), (MotionArgumentsMapEntryImpl)entry);
            }
        }
        return uniqueMotionArguments.values();
    }

    public String getHeaderLabel(MotionArgumentsMapEntryImpl entry) {
        return entry.getKey();
    }

    public BigDecimal getMotionArgumentValue(MotionArgumentsMapEntryImpl entry, Profile profile) {
        MotionSettings motionSettings = getMotionSettings(entry);
        MotionProfileSettings motionProfileSettings = motionSettings.getProfileSettings().get(profile);
        if (motionProfileSettings == null)
            return null;
        Expression exp = motionProfileSettings.getMotionArguments().get(entry.getKey());
        return exp == null ? null : exp.evaluate();
    }

    public Collection<Profile> getProfiles(MotionArgumentsMapEntryImpl entry) {
        return getMotionSettings(entry).getProfileSettings().keySet();
    }

    private MotionSettings getMotionSettings(MotionArgumentsMapEntryImpl entry) {
        return (MotionSettings)entry.eContainer().eContainer().eContainer();
    }

    public MotionSettings getMotionSettings(Peripheral peripheral, Axis axis) throws IOException {
        return getPhysicalSettings(peripheral).getMotionSettings().get(axis);
    }

    public PhysicalSettings getPhysicalSettings(Peripheral peripheral) throws IOException {
        Settings setting = SettingUtil.getSettings(peripheral.eResource());
        IResource resource = null; // @TODO resource must be supplied here
        return setting.getPhysicalSettings(resource, peripheral);
    }

    public BigDecimal toBigDecimal(String value) {
        try {
            return new BigDecimal(value);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
