/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import java.math.BigDecimal;
import machine.IResource;
import org.eclipse.xtend.lib.annotations.AccessorType;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;

@Accessors({ AccessorType.PUBLIC_GETTER, AccessorType.PACKAGE_SETTER })
@SuppressWarnings("all")
public class ResourceThroughputOutlineItem {
  private IResource resource;
  
  private int numberOfIterations;
  
  private BigDecimal totalMakespan;
  
  private Double iterationMakespan;
  
  private Double throughput;
  
  @Pure
  public IResource getResource() {
    return this.resource;
  }
  
  void setResource(final IResource resource) {
    this.resource = resource;
  }
  
  @Pure
  public int getNumberOfIterations() {
    return this.numberOfIterations;
  }
  
  void setNumberOfIterations(final int numberOfIterations) {
    this.numberOfIterations = numberOfIterations;
  }
  
  @Pure
  public BigDecimal getTotalMakespan() {
    return this.totalMakespan;
  }
  
  void setTotalMakespan(final BigDecimal totalMakespan) {
    this.totalMakespan = totalMakespan;
  }
  
  @Pure
  public Double getIterationMakespan() {
    return this.iterationMakespan;
  }
  
  void setIterationMakespan(final Double iterationMakespan) {
    this.iterationMakespan = iterationMakespan;
  }
  
  @Pure
  public Double getThroughput() {
    return this.throughput;
  }
  
  void setThroughput(final Double throughput) {
    this.throughput = throughput;
  }
}
