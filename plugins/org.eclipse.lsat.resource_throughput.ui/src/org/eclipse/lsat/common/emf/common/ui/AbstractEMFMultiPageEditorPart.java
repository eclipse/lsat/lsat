/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.ui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.WeakHashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.ui.MarkerHelper;
import org.eclipse.emf.common.ui.ViewerPane;
import org.eclipse.emf.common.ui.editor.ProblemEditorPart;
import org.eclipse.emf.common.ui.viewer.IViewerProvider;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.action.EditingDomainActionBarContributor;
import org.eclipse.emf.edit.ui.provider.UnwrappingSelectionProvider;
import org.eclipse.emf.edit.ui.util.EditUIMarkerHelper;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.lsat.common.emf.ecore.resource.ResourceSetUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

public abstract class AbstractEMFMultiPageEditorPart extends MultiPageEditorPart
        implements IEditingDomainProvider, IViewerProvider, IMenuListener
{
    /** This keeps track of the registered viewer pages. */
    private final Map<Control, ViewerPane> viewerMap = new WeakHashMap<>();

    /** This keeps track of the editing domain that is used to track all changes to the model. */
    protected EditingDomain editingDomain;

    /** This is the one adapter factory used for providing views of the model. */
    protected ComposedAdapterFactory adapterFactory;

    /** This is the content outline page. */
    protected IContentOutlinePage contentOutlinePage = null;

    /** Map to store the diagnostic associated with a resource. */
    protected Map<Resource, Diagnostic> resourceToDiagnosticMap = new LinkedHashMap<Resource, Diagnostic>();

    /**
     * The MarkerHelper is responsible for creating workspace resource markers presented in Eclipse's Problems View.
     */
    protected MarkerHelper markerHelper = new EditUIMarkerHelper();

    /**
     * Controls whether the problem indication should be updated.
     */
    protected boolean updateProblemIndication = true;

    /** This listens for when the outline becomes active */
    protected IPartListener partListener = new IPartListener() {
        @Override
        public void partActivated(IWorkbenchPart p) {
            if (p instanceof ContentOutline) {
                if (((ContentOutline)p).getCurrentPage() == contentOutlinePage) {
                    getActionBarContributor().setActiveEditor(AbstractEMFMultiPageEditorPart.this);
                }
            } else if (p == AbstractEMFMultiPageEditorPart.this) {
                handleActivate();
            }
        }

        @Override
        public void partBroughtToTop(IWorkbenchPart p) {
            // Ignore.
        }

        @Override
        public void partClosed(IWorkbenchPart p) {
            // Ignore.
        }

        @Override
        public void partDeactivated(IWorkbenchPart p) {
            // Ignore.
        }

        @Override
        public void partOpened(IWorkbenchPart p) {
            // Ignore.
        }
    };

    /** Resources that have been removed since last activation. */
    protected Collection<Resource> removedResources = new ArrayList<Resource>();

    /** Resources that have been changed since last activation. */
    protected Collection<Resource> changedResources = new ArrayList<Resource>();

    /** Resources that have been saved. */
    protected Collection<Resource> savedResources = new ArrayList<Resource>();

    /** Adapter used to update the problem indication when resources are demanded loaded. */
    protected EContentAdapter problemIndicationAdapter = new EContentAdapter() {
        @Override
        public void notifyChanged(Notification notification) {
            if (notification.getNotifier() instanceof Resource) {
                switch (notification.getFeatureID(Resource.class)) {
                    case Resource.RESOURCE__IS_LOADED:
                    case Resource.RESOURCE__ERRORS:
                    case Resource.RESOURCE__WARNINGS: {
                        Resource resource = (Resource)notification.getNotifier();
                        Diagnostic diagnostic = analyzeResourceProblems(resource, null);
                        if (diagnostic.getSeverity() != Diagnostic.OK) {
                            resourceToDiagnosticMap.put(resource, diagnostic);
                        } else {
                            resourceToDiagnosticMap.remove(resource);
                        }

                        if (updateProblemIndication) {
                            getSite().getShell().getDisplay().asyncExec(new Runnable() {
                                @Override
                                public void run() {
                                    updateProblemIndication();
                                }
                            });
                        }
                        break;
                    }
                }
            } else {
                super.notifyChanged(notification);
            }
        }

        @Override
        protected void setTarget(Resource target) {
            basicSetTarget(target);
        }

        @Override
        protected void unsetTarget(Resource target) {
            basicUnsetTarget(target);
            resourceToDiagnosticMap.remove(target);
            if (updateProblemIndication) {
                getSite().getShell().getDisplay().asyncExec(new Runnable() {
                    @Override
                    public void run() {
                        updateProblemIndication();
                    }
                });
            }
        }
    };

    /** This listens for workspace changes. */
    protected IResourceChangeListener resourceChangeListener = new IResourceChangeListener() {
        @Override
        public void resourceChanged(IResourceChangeEvent event) {
            IResourceDelta delta = event.getDelta();
            try {
                class ResourceDeltaVisitor implements IResourceDeltaVisitor {
                    protected ResourceSet resourceSet = editingDomain.getResourceSet();

                    protected Collection<Resource> changedResources = new ArrayList<Resource>();

                    protected Collection<Resource> removedResources = new ArrayList<Resource>();

                    @Override
                    public boolean visit(IResourceDelta delta) {
                        if (delta.getResource().getType() == IResource.FILE) {
                            if (delta.getKind() == IResourceDelta.REMOVED || delta.getKind() == IResourceDelta.CHANGED
                                    && delta.getFlags() != IResourceDelta.MARKERS)
                            {
                                Resource resource = resourceSet.getResource(
                                        URI.createPlatformResourceURI(delta.getFullPath().toString(), true), false);
                                if (resource != null) {
                                    if (delta.getKind() == IResourceDelta.REMOVED) {
                                        removedResources.add(resource);
                                    } else if (!savedResources.remove(resource)) {
                                        changedResources.add(resource);
                                    }
                                }
                            }
                            return false;
                        }

                        return true;
                    }

                    public Collection<Resource> getChangedResources() {
                        return changedResources;
                    }

                    public Collection<Resource> getRemovedResources() {
                        return removedResources;
                    }
                }

                final ResourceDeltaVisitor visitor = new ResourceDeltaVisitor();
                delta.accept(visitor);

                if (!visitor.getRemovedResources().isEmpty()) {
                    getSite().getShell().getDisplay().asyncExec(new Runnable() {
                        @Override
                        public void run() {
                            removedResources.addAll(visitor.getRemovedResources());
                            if (!isDirty()) {
                                getSite().getPage().closeEditor(AbstractEMFMultiPageEditorPart.this, false);
                            }
                        }
                    });
                }

                if (!visitor.getChangedResources().isEmpty()) {
                    getSite().getShell().getDisplay().asyncExec(new Runnable() {
                        @Override
                        public void run() {
                            changedResources.addAll(visitor.getChangedResources());
                            if (getSite().getPage().getActiveEditor() == AbstractEMFMultiPageEditorPart.this) {
                                handleActivate();
                            }
                        }
                    });
                }
            } catch (CoreException exception) {
                getPlugin().log(exception);
            }
        }
    };

    /** Handles activation of the editor or it's associated views. */
    protected void handleActivate() {
        if (!removedResources.isEmpty()) {
            if (handleDirtyConflict()) {
                getSite().getPage().closeEditor(AbstractEMFMultiPageEditorPart.this, false);
            } else {
                removedResources.clear();
                changedResources.clear();
                savedResources.clear();
            }
        } else if (!changedResources.isEmpty()) {
            changedResources.removeAll(savedResources);
            if (!changedResources.isEmpty() && (!isDirty() || handleDirtyConflict())) {
                handleReload();
            }
            changedResources.clear();
            savedResources.clear();
        }
    }

    protected void handleReload() {
        updateProblemIndication = false;

        ArrayList<Resource> resourcesToReload = new ArrayList<>(editingDomain.getResourceSet().getResources());
        for (Iterator<Resource> i = resourcesToReload.iterator(); i.hasNext();) {
            Resource resource = i.next();
            if (resource.isLoaded()) {
                resource.unload();
            } else {
                // No need to load as it wasn't loaded
                i.remove();
            }
        }

        for (Resource resource: resourcesToReload) {
            try {
                resource.load(editingDomain.getResourceSet().getLoadOptions());
            } catch (IOException exception) {
                if (!resourceToDiagnosticMap.containsKey(resource)) {
                    resourceToDiagnosticMap.put(resource, analyzeResourceProblems(resource, exception));
                }
            }
        }
        updateProblemIndication = true;
        updateProblemIndication();

        // Reloading obsoletes previous editing changes
        editingDomain.getCommandStack().flush();
    }

    /** Updates the problems indication with the information described in the specified diagnostic. */
    protected void updateProblemIndication() {
        if (updateProblemIndication) {
            BasicDiagnostic diagnostic = new BasicDiagnostic(Diagnostic.OK, getPlugin().getSymbolicName(), 0, null,
                    new Object[]
                    {editingDomain.getResourceSet()});
            for (Diagnostic childDiagnostic: resourceToDiagnosticMap.values()) {
                if (childDiagnostic.getSeverity() != Diagnostic.OK) {
                    diagnostic.add(childDiagnostic);
                }
            }

            int lastEditorPage = getPageCount() - 1;
            if (lastEditorPage >= 0 && getEditor(lastEditorPage) instanceof ProblemEditorPart) {
                ((ProblemEditorPart)getEditor(lastEditorPage)).setDiagnostic(diagnostic);
                if (diagnostic.getSeverity() != Diagnostic.OK) {
                    setActivePage(lastEditorPage);
                }
            } else if (diagnostic.getSeverity() != Diagnostic.OK) {
                ProblemEditorPart problemEditorPart = new ProblemEditorPart();
                problemEditorPart.setDiagnostic(diagnostic);
                problemEditorPart.setMarkerHelper(markerHelper);
                try {
                    addPage(++lastEditorPage, problemEditorPart, getEditorInput());
                    setPageText(lastEditorPage, problemEditorPart.getPartName());
                    setActivePage(lastEditorPage);
                    showTabs();
                } catch (PartInitException exception) {
                    getPlugin().log(exception);
                }
            }

            if (markerHelper.hasMarkers(editingDomain.getResourceSet())) {
                markerHelper.deleteMarkers(editingDomain.getResourceSet());
                if (diagnostic.getSeverity() != Diagnostic.OK) {
                    try {
                        markerHelper.createMarkers(diagnostic);
                    } catch (CoreException exception) {
                        getPlugin().log(exception);
                    }
                }
            }
        }
    }

    /** Shows a dialog that asks if conflicting changes should be discarded. */
    protected boolean handleDirtyConflict() {
        return MessageDialog.openQuestion(getSite().getShell(), "File Conflict",
                "There are unsaved changes that conflict with changes made outside the editor.  Do you wish to discard this editor's changes?");
    }

    public AbstractEMFMultiPageEditorPart() {
        initializeEditingDomain();
    }

    protected abstract EMFPlugin getPlugin();

    /**
     * This sets up the editing domain for the model editor.
     */
    protected void initializeEditingDomain() {
        // Create an adapter factory that yields item providers.
        adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

        adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
        adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());

        // Create the command stack that will notify this editor as commands are
        // executed.
        BasicCommandStack commandStack = new BasicCommandStack();

        commandStack.addCommandStackListener(new CommandStackListener() {
            @Override
            public void commandStackChanged(final EventObject event) {
                getContainer().getDisplay().asyncExec(new Runnable() {
                    @Override
                    public void run() {
                        firePropertyChange(IEditorPart.PROP_DIRTY);
                    }
                });
            }
        });

        // Create the editing domain with a special command stack.
        editingDomain = createEditingDomain(commandStack, new HashMap<Resource, Boolean>());
        editingDomain.getResourceSet().eAdapters().add(problemIndicationAdapter);
    }

    protected EditingDomain createEditingDomain(CommandStack commandStack,
            Map<Resource, Boolean> resourceToReadOnlyMap)
    {
        return new AdapterFactoryEditingDomain(adapterFactory, commandStack, resourceToReadOnlyMap);
    }

    /**
     * This returns the editing domain as required by the {@link IEditingDomainProvider} interface. This is important
     * for implementing the static methods of {@link AdapterFactoryEditingDomain} and for supporting
     * {@link org.eclipse.emf.edit.ui.action.CommandAction}.
     */
    @Override
    public EditingDomain getEditingDomain() {
        return editingDomain;
    }

    /**
     * This is the method called to load a resource into the editing domain's resource set based on the editor's input.
     */
    protected Resource createModel() {
        URI resourceURI = EditUIUtil.getURI(getEditorInput(), editingDomain.getResourceSet().getURIConverter());
        Exception exception = null;
        Resource resource = null;
        try {
            // Load the resource through the editing domain.
            resource = editingDomain.getResourceSet().getResource(resourceURI, true);
        } catch (Exception e) {
            exception = e;
            resource = editingDomain.getResourceSet().getResource(resourceURI, false);
        }

        Diagnostic diagnostic = analyzeResourceProblems(resource, exception);
        if (diagnostic.getSeverity() != Diagnostic.OK) {
            resourceToDiagnosticMap.put(resource, diagnostic);
        }
        return resource;
    }

    /**
     * Returns a diagnostic describing the errors and warnings listed in the resource and the specified exception (if
     * any).
     */
    public Diagnostic analyzeResourceProblems(Resource resource, Exception exception) {
        boolean hasErrors = !resource.getErrors().isEmpty();
        if (hasErrors || !resource.getWarnings().isEmpty()) {
            BasicDiagnostic basicDiagnostic = new BasicDiagnostic(hasErrors ? Diagnostic.ERROR : Diagnostic.WARNING,
                    getPlugin().getSymbolicName(), 0,
                    String.format("Problems encountered in file \"%s\"", resource.getURI()), new Object[]
                    {exception == null ? (Object)resource : exception});
            basicDiagnostic.merge(EcoreUtil.computeDiagnostic(resource, true));
            return basicDiagnostic;
        } else if (exception != null) {
            return new BasicDiagnostic(Diagnostic.ERROR, getPlugin().getSymbolicName(), 0,
                    String.format("Problems encountered in file \"%s\"", resource.getURI()), new Object[]
                    {exception});
        } else {
            return Diagnostic.OK_INSTANCE;
        }
    }

    /**
     * This looks up a string in the plugin's plugin.properties file.
     */
    protected String getString(String key, Object... substitutions) {
        return getPlugin().getString(key, substitutions);
    }

    /**
     * If there is more than one page in the multi-page editor part, this shows the tabs at the bottom.
     */
    protected void showTabs() {
        if (getPageCount() > 1) {
            if (getContainer() instanceof CTabFolder) {
                ((CTabFolder)getContainer()).setTabHeight(SWT.DEFAULT);
                Point point = getContainer().getSize();
                getContainer().setSize(point.x, point.y - 6);
            }
        }
    }

    /**
     * This is for implementing {@link IEditorPart} and simply tests the command stack. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     *
     * @generated
     */
    @Override
    public boolean isDirty() {
        return ((BasicCommandStack)editingDomain.getCommandStack()).isSaveNeeded();
    }

    /**
     * This is for implementing {@link IEditorPart} and simply saves the model file.
     */
    @Override
    public void doSave(IProgressMonitor progressMonitor) {
        // Save only resources that have actually changed.
        final Map<Object, Object> saveOptions = new HashMap<Object, Object>();
        saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
        saveOptions.put(Resource.OPTION_LINE_DELIMITER, Resource.OPTION_LINE_DELIMITER_UNSPECIFIED);

        // Do the work within an operation because this is a long running
        // activity that modifies the workbench.
        //
        WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
            // This is the method that gets invoked when the operation runs.
            @Override
            public void execute(IProgressMonitor monitor) {
                // Save the resources to the file system.
                for (Resource resource: ResourceSetUtil.getResourcesInSaveOrder(editingDomain.getResourceSet())) {
                    if (!editingDomain.isReadOnly(resource)) {
                        try {
                            long timeStamp = resource.getTimeStamp();
                            resource.save(saveOptions);
                            if (resource.getTimeStamp() != timeStamp) {
                                savedResources.add(resource);
                            }
                        } catch (Exception exception) {
                            resourceToDiagnosticMap.put(resource, analyzeResourceProblems(resource, exception));
                        }
                    }
                }
            }
        };

        updateProblemIndication = false;
        try {
            // This runs the options, and shows progress.
            new ProgressMonitorDialog(getSite().getShell()).run(true, false, operation);

            // Refresh the necessary state.
            ((BasicCommandStack)editingDomain.getCommandStack()).saveIsDone();
            firePropertyChange(IEditorPart.PROP_DIRTY);
        } catch (Exception exception) {
            // Something went wrong that shouldn't.
            getPlugin().log(exception);
        }
        updateProblemIndication = true;
        updateProblemIndication();
    }

    /**
     * This returns whether something has been persisted to the URI of the specified resource. The implementation uses
     * the URI converter from the editor's resource set to try to open an input stream.
     */
    protected boolean isPersisted(Resource resource) {
        boolean result = false;
        try {
            InputStream stream = editingDomain.getResourceSet().getURIConverter().createInputStream(resource.getURI());
            if (stream != null) {
                result = true;
                stream.close();
            }
        } catch (IOException e) {
            // Ignore
        }
        return result;
    }

    /**
     * This always returns true because it is not currently supported.
     */
    @Override
    public boolean isSaveAsAllowed() {
        return true;
    }

    /**
     * This also changes the editor's input.
     */
    @Override
    public void doSaveAs() {
        SaveAsDialog saveAsDialog = new SaveAsDialog(getSite().getShell());
        saveAsDialog.open();
        IPath path = saveAsDialog.getResult();
        if (path != null) {
            IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
            if (file != null) {
                doSaveAs(URI.createPlatformResourceURI(file.getFullPath().toString(), true), new FileEditorInput(file));
            }
        }
    }

    protected void doSaveAs(URI uri, IEditorInput editorInput) {
        (editingDomain.getResourceSet().getResources().get(0)).setURI(uri);
        setInputWithNotify(editorInput);
        setPartName(editorInput.getName());
        IProgressMonitor progressMonitor = getActionBars().getStatusLineManager() != null
                ? getActionBars().getStatusLineManager().getProgressMonitor() : new NullProgressMonitor();
        doSave(progressMonitor);
    }

    @Override
    public void init(IEditorSite site, IEditorInput editorInput) throws PartInitException {
        super.init(site, editorInput);
        setPartName(editorInput.getName());
        site.getPage().addPartListener(partListener);
        ResourcesPlugin.getWorkspace().addResourceChangeListener(resourceChangeListener,
                IResourceChangeEvent.POST_CHANGE);
    }

    public IActionBars getActionBars() {
        return getActionBarContributor().getActionBars();
    }

    public EditingDomainActionBarContributor getActionBarContributor() {
        return (EditingDomainActionBarContributor)getEditorSite().getActionBarContributor();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(Class<T> adapter) {
        if (adapter.equals(IContentOutlinePage.class)) {
            if (null == contentOutlinePage) {
                contentOutlinePage = createOutlinePage();
            }
            return (T)contentOutlinePage;
        }
        return super.getAdapter(adapter);
    }

    protected IContentOutlinePage createOutlinePage() {
        return null;
    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorPart#addPage(org.eclipse.swt.widgets.Control)
     * @deprecated use {@link #addPage(ViewerPane)}
     */
    @Override
    @Deprecated
    public final int addPage(Control control) {
        throw new UnsupportedOperationException("Please use addPage(ViewerPane viewerPane)");
    }

    /**
     * @see org.eclipse.ui.part.MultiPageEditorPart#addPage(int, org.eclipse.swt.widgets.Control)
     * @deprecated use {@link #addPage(int, ViewerPane)}
     */
    @Override
    @Deprecated
    public final void addPage(int index, Control control) {
        throw new UnsupportedOperationException("Please use addPage(int index, ViewerPane viewerPane)");
    }

    public final int addPage(ViewerPane viewerPane) {
        int index = getPageCount();
        addPage(index, viewerPane);
        return index;
    }

    public final void addPage(int index, ViewerPane viewerPane) {
        Control pageControl = viewerPane.getControl();
        viewerMap.put(pageControl, viewerPane);
        super.addPage(index, pageControl);
    }

    @Override
    public void removePage(int pageIndex) {
        viewerMap.remove(getControl(pageIndex));
        super.removePage(pageIndex);
    }

    @Override
    public Viewer getViewer() {
        return getViewer(getSelectedPage());
    }

    protected Viewer getViewer(Object page) {
        ViewerPane viewerPane = viewerMap.get(page);
        return null == viewerPane ? null : viewerPane.getViewer();
    }

    /**
     * This creates a context menu for the viewer and adds a listener as well registering the menu for extension.
     */
    protected void createContextMenuFor(StructuredViewer viewer) {
        MenuManager contextMenu = new MenuManager("#PopUp");
        contextMenu.add(new Separator("additions"));
        contextMenu.setRemoveAllWhenShown(true);
        contextMenu.addMenuListener(this);
        Menu menu = contextMenu.createContextMenu(viewer.getControl());
        viewer.getControl().setMenu(menu);
        getSite().registerContextMenu(contextMenu, new UnwrappingSelectionProvider(viewer));
    }

    /**
     * This implements {@link org.eclipse.jface.action.IMenuListener} to help fill the context menus with contributions
     * from the Edit menu.
     */
    @Override
    public void menuAboutToShow(IMenuManager menuManager) {
        IEditorActionBarContributor actionBarContributor = getEditorSite().getActionBarContributor();
        if (actionBarContributor instanceof IMenuListener) {
            ((IMenuListener)actionBarContributor).menuAboutToShow(menuManager);
        }
    }

    @Override
    public void dispose() {
        updateProblemIndication = false;

        ResourcesPlugin.getWorkspace().removeResourceChangeListener(resourceChangeListener);

        getSite().getPage().removePartListener(partListener);

        adapterFactory.dispose();

        if (getActionBarContributor().getActiveEditor() == this) {
            getActionBarContributor().setActiveEditor(null);
        }

        if (contentOutlinePage != null) {
            contentOutlinePage.dispose();
        }

        super.dispose();
    }
}
