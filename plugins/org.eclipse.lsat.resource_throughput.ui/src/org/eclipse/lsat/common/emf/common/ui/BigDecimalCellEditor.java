/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.emf.common.ui;

import java.math.BigDecimal;

import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

public class BigDecimalCellEditor extends TextCellEditor implements ICellEditorValidator {
    public BigDecimalCellEditor() {
        setValidator(this);
    }

    public BigDecimalCellEditor(Composite parent) {
        super(parent);
        setValidator(this);
    }

    public BigDecimalCellEditor(Composite parent, int style) {
        super(parent, style);
        setValidator(this);
    }

    @Override
    public String isValid(Object value) {
        try {
            new BigDecimal(String.valueOf(value));
            return null;
        } catch (NumberFormatException e) {
            return String.format("Not a valid number %s: %s", value, e.getMessage());
        }
    }

    @Override
    protected boolean isCorrect(Object value) {
        boolean corect = super.isCorrect(value);
        setValueValid(corect);
        return corect;
    }

    @Override
    protected void valueChanged(boolean oldValidState, boolean newValidState) {
        // TODO Auto-generated method stub
        super.valueChanged(oldValidState, newValidState);
    }

    @Override
    protected Object doGetValue() {
        Object value = super.doGetValue();
        try {
            if (value instanceof String) {
                value = new BigDecimal((String)value);
            }
        } catch (NumberFormatException e) {
            // Ignore
        }
        return value;
    }

    @Override
    protected void doSetValue(Object value) {
        super.doSetValue(null == value ? null : value.toString());
    }
}
