/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors.internal;

import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputEditingDomain;
import org.eclipse.lsat.timing.util.MoveHelper;
import org.eclipse.swt.widgets.Composite;

import com.google.common.base.Objects;

import activity.ActivityPackage;
import activity.Move;
import machine.Profile;

public class MotionProfileEditingSupport extends EditingSupport {
    private final ResourceThroughputEditingDomain editingDomain;

    private final ComboBoxViewerCellEditor cellEditor;

    public MotionProfileEditingSupport(ColumnViewer viewer, ResourceThroughputEditingDomain editingDomain) {
        super(viewer);
        this.editingDomain = editingDomain;
        cellEditor = new ComboBoxViewerCellEditor((Composite)viewer.getControl());
        cellEditor.setContentProvider(new ArrayContentProvider());
        cellEditor.setLabelProvider(new LabelProvider() {
            @Override
            public String getText(Object element) {
                return element instanceof Profile ? ((Profile)element).getName() : null;
            }
        });
    }

    @Override
    protected CellEditor getCellEditor(Object element) {
        return cellEditor;
    }

    @Override
    protected boolean canEdit(Object element) {
        return element instanceof Move;
    }

    @Override
    protected void initializeCellEditorValue(CellEditor cellEditor, ViewerCell cell) {
        Object element = cell.getElement();
        if (element instanceof Move) {
            Move move = (Move)element;
            this.cellEditor.setInput(MoveHelper.getAvailabeProfiles(move).toArray());
        }
        super.initializeCellEditorValue(cellEditor, cell);
    }

    @Override
    protected Object getValue(Object element) {
        if (element instanceof Move) {
            return ((Move)element).getProfile();
        }
        return null;
    }

    @Override
    protected void setValue(Object element, Object value) {
        if (element instanceof Move && value instanceof Profile && !Objects.equal(getValue(element), value)) {
            editingDomain.getCommandStack().execute(
                    new SetCommand(editingDomain, (Move)element, ActivityPackage.Literals.MOVE__PROFILE, value));
        }
    }
}
