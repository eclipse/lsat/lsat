/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors

import activity.Activity
import activity.Claim
import activity.PeripheralAction
import activity.Release
import activity.ResourceAction
import org.eclipse.jface.viewers.IStructuredContentProvider
import org.eclipse.lsat.common.graph.directed.editable.Node

import static extension org.eclipse.lsat.common.graph.directed.editable.EdgQueries.topologicalOrdering
import static extension org.eclipse.lsat.common.xtend.Queries.*

class ResourceThroughputTableContentProvider implements IStructuredContentProvider {
    val String resourceFqn
    
    new(String resourceFqn) {
        this.resourceFqn = resourceFqn
    }
    
    override Object[] getElements(Object editingDomain) {
        if (editingDomain instanceof ResourceThroughputEditingDomain) {
            return editingDomain.activities
                .getResourceActivities(resourceFqn)
                .collect[getResourceActions(resourceFqn)]
                .objectsOfKind(PeripheralAction)
        } else {
            throw new IllegalArgumentException()
        }
    }
    
    /**
     * Returns all Activities in which resource participates
     */
    static def Iterable<Activity> getResourceActivities(Iterable<Activity> activities, String resourceFqn) {
        return activities.select[nodes.objectsOfKind(ResourceAction).exists[resource.fqn == resourceFqn]]
    }
    
    /**
     * The nodes which are applicable are the ones that are both in the closure of
     * <ul><li>outgoing edges of the claim</li>
     * <li>incoming edges of the release</li></ul>
     */    
    static def Iterable<Node> getResourceActions(Activity activity, String resourceFqn) {
        val claim = activity.nodes.objectsOfKind(Claim).select[resource.fqn == resourceFqn]
        val claimOutgoingNodes = claim.closure[Node n | n.outgoingEdges.collectOne[targetNode]].toSet
        val release = activity.nodes.objectsOfKind(Release).select[resource.fqn == resourceFqn]
        val releaseIncomingNodes = release.closure[Node n | n.incomingEdges.collectOne[sourceNode]].toSet

        return activity.nodes.topologicalOrdering.select[claimOutgoingNodes.contains(it) && releaseIncomingNodes.contains(it)]
    }
}