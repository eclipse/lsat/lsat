/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors;

import static dispatching.util.DispatchingUtil.convertYield;
import static dispatching.util.DispatchingUtil.expand;
import static dispatching.util.DispatchingUtil.expandActivities;
import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.lsat.common.queries.QueryableIterable;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.lsat.timing.util.ITimingCalculator;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.lsat.timing.util.TimingCalculator;

import activity.Activity;
import dispatching.ActivityDispatching;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import setting.Settings;
import timing.CalculationMode;

public class ResourceThroughputEditingDomain extends AdapterFactoryEditingDomain {
    private TimingCalculator timingCalculator;

    private Collection<String> dispatchPhases;

    public ResourceThroughputEditingDomain(AdapterFactory adapterFactory, CommandStack commandStack) {
        super(adapterFactory, commandStack);
    }

    public void reCalculate() {
        if (null != timingCalculator) {
            timingCalculator.reset();
        }
    }

    private <T> QueryableIterable<T> getContents(Class<T> type) {
        return from(getResourceSet().getResources()).collect(Resource::getContents).objectsOfKind(type);
    }

    public ActivityDispatching getActivityDispatching() {
        ActivityDispatching activityDispatching = EcoreUtil.copy(getRawActivityDispatching());
        expandActivities(activityDispatching);
        return filterPhase(convertYield(activityDispatching));
    }

    public ActivityDispatching getFullyExpandedActivityDispatching() {
        ActivityDispatching activityDispatching = EcoreUtil.copy(getRawActivityDispatching());
        expand(activityDispatching);
        return filterPhase(activityDispatching);
    }

    public void setDispatchPhases(String[] phases) {
        if (phases == null || phases.length == 0) {
            dispatchPhases = null;
        }
        dispatchPhases = Arrays.asList(phases);
    }

    public String[] getSelectedDispatchPhases() {
        return (dispatchPhases != null) ? dispatchPhases.toArray(new String[dispatchPhases.size()])
                : getDispatchPhases();
    }

    public String[] getDispatchPhases() {
        return getRawActivityDispatching().getDispatchGroups().stream().map(DispatchGroup::getName).distinct()
                .toArray(String[]::new);
    }

    public QueryableIterable<Activity> getActivities() {
        return from(getActivityDispatching()).xcollect(ActivityDispatching::getDispatchGroups)
                .sortedBy(DispatchGroup::getOffset).xcollect(DispatchGroup::getDispatches)
                .xcollectOne(Dispatch::getActivity);
    }

    public Settings getSettings() throws SpecificationException {
        Settings settings = getContents(Settings.class).first();
        if (null == settings) {
            throw new SpecificationException("Settings not found, please see problems tab.");
        }
        return settings;
    }

    public ITimingCalculator getTimingCalculator() throws MotionException, SpecificationException {
        final Settings settings = getSettings();
        final MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();

        if (null == timingCalculator || settings != timingCalculator.getSettings()
                || motionCalculator != timingCalculator.getMotionCalculator())
        {
            timingCalculator = new TimingCalculator(settings, motionCalculator, CalculationMode.MEAN, false, true);
        }
        return timingCalculator;
    }

    private ActivityDispatching getRawActivityDispatching() {
        return getContents(ActivityDispatching.class).first();
    }

    private ActivityDispatching filterPhase(ActivityDispatching dispatching) {
        if (dispatchPhases == null)
            return dispatching;
        ActivityDispatching out = EcoreUtil.copy(dispatching);
        Iterator<DispatchGroup> iter = out.getDispatchGroups().iterator();
        while (iter.hasNext()) {
            if (!dispatchPhases.contains(iter.next().getName())) {
                iter.remove();
            }
        }
        return out;
    }
}
