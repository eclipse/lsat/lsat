/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors.internal;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.lsat.common.emf.common.ui.BigDecimalCellEditor;
import org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputEditingDomain;
import org.eclipse.lsat.timing.util.MoveHelper;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.swt.widgets.Composite;

import activity.Move;
import machine.Axis;
import machine.Peripheral;
import machine.impl.MachineQueries;

public class AxisTargetLocationEditingSupport extends EditingSupport {
    private final ResourceThroughputEditingDomain editingDomain;

    private final CellEditor cellEditor;

    private final String axisName;

    public AxisTargetLocationEditingSupport(ColumnViewer viewer, String axisName,
            ResourceThroughputEditingDomain editingDomain)
    {
        super(viewer);
        this.editingDomain = editingDomain;
        this.axisName = axisName;
        cellEditor = new BigDecimalCellEditor((Composite)viewer.getControl());
    }

    @Override
    protected CellEditor getCellEditor(Object element) {
        return cellEditor;
    }

    protected MoveHelper getEditorContext(Object element) {
        if (element instanceof Move) {
            Move move = (Move)element;
            Peripheral peripheral = move.getPeripheral();
            Axis axis = MachineQueries.getAxis(peripheral, axisName);
            try {
                return new MoveHelper(move, axis, editingDomain.getSettings());
            } catch (SpecificationException e) {
                // ignore
            }
        }
        return null;
    }

    @Override
    protected boolean canEdit(Object element) {
        return getEditorContext(element) != null;
    }

    @Override
    protected Object getValue(Object element) {
        MoveHelper helper = getEditorContext(element);
        return null == helper ? null : helper.getValue();
    }

    @Override
    protected void setValue(Object element, Object value) {
        MoveHelper helper = getEditorContext(element);
        if (helper != null) {
            helper.setValue(editingDomain, value);
        }
    }
}
