/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.resource_throughput.ui.editors.internal;

import java.math.BigDecimal;

import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.lsat.common.emf.common.ui.BigDecimalCellEditor;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.resource_throughput.ui.editors.ResourceThroughputEditingDomain;
import org.eclipse.lsat.timing.util.SpecificationException;
import org.eclipse.swt.widgets.Composite;

import activity.SimpleAction;
import machine.IResource;
import machine.Peripheral;
import setting.PhysicalSettings;
import timing.Distribution;
import timing.FixedValue;
import timing.Timing;
import timing.TimingPackage;

public class SimpleActionExecutionTimeEditingSupport extends EditingSupport {
    private final ResourceThroughputEditingDomain editingDomain;

    private final CellEditor cellEditor;

    public SimpleActionExecutionTimeEditingSupport(ColumnViewer viewer, ResourceThroughputEditingDomain editingDomain) {
        super(viewer);
        this.editingDomain = editingDomain;
        cellEditor = new BigDecimalCellEditor((Composite)viewer.getControl());
    }

    @Override
    protected CellEditor getCellEditor(Object element) {
        return cellEditor;
    }

    protected Timing getEditorContext(Object element) {
        Timing timing = null;
        if (element instanceof SimpleAction) {
            SimpleAction action = (SimpleAction)element;
            Peripheral peripheral = action.getPeripheral();
            try {
                IResource resource = action.getResource();
                PhysicalSettings settings = editingDomain.getSettings().getPhysicalSettings(resource, peripheral);
                if (null != settings) {
                    timing = settings.getTimingSettings().get(action.getType());
                }
            } catch (SpecificationException e) {
                // Ignore
            }
        }
        return timing;
    }

    @Override
    protected boolean canEdit(Object element) {
        return getValueInMillis(element) != null;
    }

    @Override
    protected final Object getValue(Object element) {
        return getValueInMillis(element);
    }

    protected BigDecimal getValueInMillis(Object element) {
        BigDecimal value = null;
        if (element instanceof SimpleAction) {
            try {
                value = editingDomain.getTimingCalculator().calculateDuration((SimpleAction)element);
            } catch (MotionException | SpecificationException e) {
                // Ignore
            }
        }
        return null == value ? null : value.movePointRight(3);
    }

    @Override
    protected void setValue(Object element, Object value) {
        BigDecimal millis = (BigDecimal)value;
        if (getValueInMillis(element).compareTo(millis) != 0) {
            BigDecimal seconds = millis.movePointLeft(3);
            Timing timing = getEditorContext(element);
            if (timing instanceof FixedValue) {
                editingDomain.getCommandStack().execute(
                        new SetCommand(editingDomain, timing, TimingPackage.Literals.FIXED_VALUE__VALUE, seconds));
            }
            if (timing instanceof Distribution) {
                editingDomain.getCommandStack().execute(
                        new SetCommand(editingDomain, timing, TimingPackage.Literals.DISTRIBUTION__DEFAULT, seconds));
            }
        }
    }
}
