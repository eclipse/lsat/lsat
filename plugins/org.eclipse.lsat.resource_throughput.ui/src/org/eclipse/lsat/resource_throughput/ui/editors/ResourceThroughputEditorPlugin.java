/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */

package org.eclipse.lsat.resource_throughput.ui.editors;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.ui.EclipseUIPlugin;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;

import machine.provider.MachineEditPlugin;

/**
 * This is the central singleton for the Dispatching editor plugin. <!-- begin-user-doc --> <!-- end-user-doc -->
 *
 * @generated
 */
public final class ResourceThroughputEditorPlugin extends EMFPlugin {
    /**
     * Keep track of the singleton. <!-- begin-user-doc --> <!-- end-user-doc -->
     *
     * @generated
     */
    public static final ResourceThroughputEditorPlugin INSTANCE = new ResourceThroughputEditorPlugin();

    public static final String IMAGE_ERROR = ResourceThroughputEditorPlugin.class.getName() + ".IMAGE_ERROR";

    public static final String IMAGE_CRITICAL = ResourceThroughputEditorPlugin.class.getName() + ".IMAGE_CRITICAL";

    public static final String IMAGE_UP = ResourceThroughputEditorPlugin.class.getName() + ".IMAGE_UP";

    public static final String IMAGE_DOWN = ResourceThroughputEditorPlugin.class.getName() + ".IMAGE_DOWN";

    public static final String IMAGE_RELOAD = ResourceThroughputEditorPlugin.class.getName() + ".IMAGE_RELOAD";

    public static final String IMAGE_EDIT = ResourceThroughputEditorPlugin.class.getName() + ".IMAGE_PENCIL";

    public static final String IMAGE_FILTER = ResourceThroughputEditorPlugin.class.getName() + ".IMAGE_FILTER";

    /**
     * Keep track of the singleton. <!-- begin-user-doc --> <!-- end-user-doc -->
     *
     * @generated
     */
    private static Implementation plugin;

    /**
     * Create the instance. <!-- begin-user-doc --> <!-- end-user-doc -->
     *
     * @generated
     */
    public ResourceThroughputEditorPlugin() {
        super(new ResourceLocator[] {MachineEditPlugin.INSTANCE});
    }

    /**
     * Returns the singleton instance of the Eclipse plugin. <!-- begin-user-doc --> <!-- end-user-doc -->
     *
     * @return the singleton instance.
     * @generated
     */
    @Override
    public ResourceLocator getPluginResourceLocator() {
        return plugin;
    }

    /**
     * Returns the singleton instance of the Eclipse plugin. <!-- begin-user-doc --> <!-- end-user-doc -->
     *
     * @return the singleton instance.
     * @generated
     */
    public static Implementation getPlugin() {
        return plugin;
    }

    /**
     * The actual implementation of the Eclipse <b>Plugin</b>. <!-- begin-user-doc --> <!-- end-user-doc -->
     *
     * @generated
     */
    public static class Implementation extends EclipseUIPlugin {
        /**
         * Creates an instance. <!-- begin-user-doc --> <!-- end-user-doc -->
         *
         * @generated
         */
        public Implementation() {
            super();

            // Remember the static instance.
            //
            plugin = this;
        }

        @Override
        protected void initializeImageRegistry(ImageRegistry reg) {
            super.initializeImageRegistry(reg);

            reg.put(IMAGE_ERROR,
                    ImageDescriptor.createFromURL(FileLocator.find(getBundle(), new Path("icons/error.png"), null)));

            reg.put(IMAGE_CRITICAL,
                    ImageDescriptor.createFromURL(FileLocator.find(getBundle(), new Path("icons/critical.gif"), null)));

            reg.put(IMAGE_UP,
                    ImageDescriptor.createFromURL(FileLocator.find(getBundle(), new Path("icons/ArrowUp.gif"), null)));

            reg.put(IMAGE_DOWN, ImageDescriptor
                    .createFromURL(FileLocator.find(getBundle(), new Path("icons/ArrowDown.gif"), null)));

            reg.put(IMAGE_RELOAD,
                    ImageDescriptor.createFromURL(FileLocator.find(getBundle(), new Path("icons/reload.gif"), null)));

            reg.put(IMAGE_EDIT, ImageDescriptor
                    .createFromURL(FileLocator.find(getBundle(), new Path("icons/IntegerFieldEditor.gif"), null)));

            reg.put(IMAGE_FILTER, ImageDescriptor
                    .createFromURL(FileLocator.find(getBundle(), new Path("icons/filter_ps.png"), null)));
        }
    }
}
