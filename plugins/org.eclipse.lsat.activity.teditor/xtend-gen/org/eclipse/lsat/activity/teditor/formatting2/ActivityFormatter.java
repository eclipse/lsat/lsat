/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor.formatting2;

import activity.Activity;
import activity.ActivityPackage;
import activity.ActivitySet;
import activity.Claim;
import activity.Event;
import activity.LocationPrerequisite;
import activity.Move;
import activity.RaiseEvent;
import activity.Release;
import activity.RequireEvent;
import activity.SimpleAction;
import com.google.inject.Inject;
import java.util.Arrays;
import machine.Import;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.activity.teditor.services.ActivityGrammarAccess;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.EdgeTarget;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.graph.directed.editable.SourceReference;
import org.eclipse.lsat.common.graph.directed.editable.TargetReference;
import org.eclipse.xtext.formatting2.AbstractFormatter2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.formatting2.IHiddenRegionFormatter;
import org.eclipse.xtext.formatting2.regionaccess.ISemanticRegion;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class ActivityFormatter extends AbstractFormatter2 {
  @Inject
  @Extension
  private ActivityGrammarAccess _activityGrammarAccess;
  
  protected void _format(final ActivitySet activityset, @Extension final IFormattableDocument document) {
    EList<Import> _imports = activityset.getImports();
    for (final Import imports : _imports) {
      final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
        it.noSpace();
      };
      document.<Import>prepend(imports, _function);
    }
    EList<Activity> _activities = activityset.getActivities();
    for (final Activity activities : _activities) {
      if (activities!=null) {
        document.<Activity>format(activities);
      }
    }
    EList<Event> _events = activityset.getEvents();
    for (final Event event : _events) {
      if (event!=null) {
        document.<Event>format(event);
      }
    }
  }
  
  protected void _format(final LocationPrerequisite prerequisite, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.<LocationPrerequisite>prepend(prerequisite, _function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(prerequisite).keyword(this._activityGrammarAccess.getLocationPrerequisiteAccess().getAtKeyword_3()), _function_1);
  }
  
  protected void _format(final Activity activity, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.setNewLines(2);
    };
    document.<Activity>prepend(activity, _function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getActivityKeyword_0()), _function_1);
    final ISemanticRegion openActivity = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getLeftCurlyBracketKeyword_2());
    final ISemanticRegion closeActivity = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getRightCurlyBracketKeyword_12());
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(openActivity, _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(openActivity, closeActivity, _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.prepend(this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getPrerequisitesKeyword_3_0()), _function_4);
    final ISemanticRegion openPrerequisites = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getLeftCurlyBracketKeyword_3_1());
    final ISemanticRegion closePrerequisites = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getRightCurlyBracketKeyword_3_3());
    final Procedure1<IHiddenRegionFormatter> _function_5 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_6 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(document.prepend(openPrerequisites, _function_5), _function_6);
    final Procedure1<IHiddenRegionFormatter> _function_7 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_8 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(document.prepend(closePrerequisites, _function_7), _function_8);
    final Procedure1<IHiddenRegionFormatter> _function_9 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(openPrerequisites, closePrerequisites, _function_9);
    EList<LocationPrerequisite> _prerequisites = activity.getPrerequisites();
    for (final LocationPrerequisite prerequisite : _prerequisites) {
      if (prerequisite!=null) {
        document.<LocationPrerequisite>format(prerequisite);
      }
    }
    final Procedure1<IHiddenRegionFormatter> _function_10 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.prepend(this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getActionsKeyword_4()), _function_10);
    final ISemanticRegion openActions = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getLeftCurlyBracketKeyword_5());
    final ISemanticRegion closeActions = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getRightCurlyBracketKeyword_7());
    final Procedure1<IHiddenRegionFormatter> _function_11 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_12 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(document.prepend(openActions, _function_11), _function_12);
    final Procedure1<IHiddenRegionFormatter> _function_13 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_14 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(document.prepend(closeActions, _function_13), _function_14);
    final Procedure1<IHiddenRegionFormatter> _function_15 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(openActions, closeActions, _function_15);
    final ISemanticRegion openActionFlow = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getLeftCurlyBracketKeyword_9());
    final ISemanticRegion closeActionFlow = this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getRightCurlyBracketKeyword_11());
    final Procedure1<IHiddenRegionFormatter> _function_16 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_17 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(document.prepend(openActionFlow, _function_16), _function_17);
    final Procedure1<IHiddenRegionFormatter> _function_18 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    final Procedure1<IHiddenRegionFormatter> _function_19 = (IHiddenRegionFormatter it) -> {
      it.newLine();
    };
    document.append(document.prepend(closeActionFlow, _function_18), _function_19);
    final Procedure1<IHiddenRegionFormatter> _function_20 = (IHiddenRegionFormatter it) -> {
      it.indent();
    };
    document.<ISemanticRegion, ISemanticRegion>interior(openActionFlow, closeActionFlow, _function_20);
    final Procedure1<IHiddenRegionFormatter> _function_21 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(this.textRegionExtensions.regionFor(activity).keyword(this._activityGrammarAccess.getActivityAccess().getActionsKeyword_4()), _function_21);
    EList<Node> _nodes = activity.getNodes();
    for (final Node node : _nodes) {
      {
        final Procedure1<IHiddenRegionFormatter> _function_22 = (IHiddenRegionFormatter it) -> {
          it.newLine();
        };
        document.<Node>prepend(node, _function_22);
        if (node!=null) {
          document.<Node>format(node);
        }
      }
    }
    EList<Edge> _edges = activity.getEdges();
    for (final Edge edge : _edges) {
      {
        final Procedure1<IHiddenRegionFormatter> _function_22 = (IHiddenRegionFormatter it) -> {
          it.newLine();
        };
        document.<Edge>prepend(edge, _function_22);
        if (edge!=null) {
          document.<Edge>format(edge);
        }
      }
    }
  }
  
  protected void _format(final Claim claim, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(claim).keyword(this._activityGrammarAccess.getClaimAccess().getColonKeyword_2()), _function), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(claim).keyword(this._activityGrammarAccess.getClaimAccess().getClaimKeyword_5()), _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(claim).keyword(this._activityGrammarAccess.getClaimAccess().getPassivePassiveKeyword_4_0()), _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(claim).assignment(this._activityGrammarAccess.getClaimAccess().getResourceAssignment_6()), _function_4);
  }
  
  protected void _format(final Release release, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(release).keyword(this._activityGrammarAccess.getReleaseAccess().getColonKeyword_2()), _function), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(release).keyword(this._activityGrammarAccess.getReleaseAccess().getReleaseKeyword_4()), _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(release).assignment(this._activityGrammarAccess.getReleaseAccess().getResourceAssignment_5()), _function_3);
  }
  
  protected void _format(final RequireEvent require, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(require).keyword(this._activityGrammarAccess.getRequireEventAccess().getColonKeyword_2()), _function), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(require).keyword(this._activityGrammarAccess.getRequireEventAccess().getRequireKeyword_3()), _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(require).assignment(this._activityGrammarAccess.getRequireEventAccess().getResourceAssignment_4()), _function_3);
  }
  
  protected void _format(final RaiseEvent raise, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(raise).keyword(this._activityGrammarAccess.getRaiseEventAccess().getColonKeyword_2()), _function), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(raise).keyword(this._activityGrammarAccess.getRaiseEventAccess().getRaiseKeyword_3()), _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(raise).assignment(this._activityGrammarAccess.getRaiseEventAccess().getResourceAssignment_4()), _function_3);
  }
  
  protected void _format(final Event event, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.setNewLines(2);
    };
    document.<Event>prepend(event, _function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(event).keyword(this._activityGrammarAccess.getEventAccess().getEventKeyword_0()), _function_1), _function_2);
  }
  
  protected void _format(final Move move, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(move).keyword(this._activityGrammarAccess.getMoveAccess().getColonKeyword_2()), _function), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).keyword(this._activityGrammarAccess.getMoveAccess().getMoveKeyword_4()), _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).keyword(this._activityGrammarAccess.getMoveAccess().getPassingPassingKeyword_8_0_0_0_0()), _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).keyword(this._activityGrammarAccess.getMoveAccess().getContinuingContinuingKeyword_8_1_0_0_0()), _function_4);
    final Procedure1<IHiddenRegionFormatter> _function_5 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).keyword(this._activityGrammarAccess.getMoveAccess().getToKeyword_8_0_0_1()), _function_5);
    final Procedure1<IHiddenRegionFormatter> _function_6 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).keyword(this._activityGrammarAccess.getMoveAccess().getForKeyword_8_1_0_1()), _function_6);
    final Procedure1<IHiddenRegionFormatter> _function_7 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).keyword(this._activityGrammarAccess.getMoveAccess().getWithSpeedProfileKeyword_9_0()), _function_7);
    final Procedure1<IHiddenRegionFormatter> _function_8 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).ruleCall(this._activityGrammarAccess.getMoveAccess().getSchedulingTypeSchedulingEnumRuleCall_10_0()), _function_8);
    final Procedure1<IHiddenRegionFormatter> _function_9 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(move).feature(ActivityPackage.Literals.MOVE__PROFILE), _function_9);
  }
  
  protected void _format(final SimpleAction simpleaction, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(simpleaction).keyword(this._activityGrammarAccess.getSimpleActionAccess().getColonKeyword_2()), _function), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.append(this.textRegionExtensions.regionFor(simpleaction).feature(ActivityPackage.Literals.SIMPLE_ACTION__TYPE), _function_2);
    final Procedure1<IHiddenRegionFormatter> _function_3 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(simpleaction).keyword(this._activityGrammarAccess.getSimpleActionAccess().getFullStopKeyword_5_0()), _function_3);
    final Procedure1<IHiddenRegionFormatter> _function_4 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.surround(this.textRegionExtensions.regionFor(simpleaction).keyword(this._activityGrammarAccess.getSimpleActionAccess().getFullStopKeyword_5_2()), _function_4);
  }
  
  protected void _format(final Edge edge, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(this.textRegionExtensions.regionFor(edge).keyword(this._activityGrammarAccess.getEdgeAccess().getHyphenMinusGreaterThanSignKeyword_2()), _function);
    SourceReference _source = edge.getSource();
    if (_source!=null) {
      document.<SourceReference>format(_source);
    }
    EdgeTarget _target = edge.getTarget();
    if (_target!=null) {
      document.<EdgeTarget>format(_target);
    }
  }
  
  protected void _format(final SourceReference sourceRef, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(sourceRef).keyword(this._activityGrammarAccess.getSourceReferenceAccess().getVerticalLineKeyword_1_1_0()), _function), _function_1);
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(this.textRegionExtensions.regionFor(sourceRef).ruleCall(this._activityGrammarAccess.getSourceReferenceAccess().getNodeActionIIDParserRuleCall_1_0_0_1()), _function_2);
  }
  
  protected void _format(final TargetReference targetRef, @Extension final IFormattableDocument document) {
    final Procedure1<IHiddenRegionFormatter> _function = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    document.prepend(this.textRegionExtensions.regionFor(targetRef).ruleCall(this._activityGrammarAccess.getTargetReferenceAccess().getNodeActionIIDParserRuleCall_1_0_0_1()), _function);
    final Procedure1<IHiddenRegionFormatter> _function_1 = (IHiddenRegionFormatter it) -> {
      it.oneSpace();
    };
    final Procedure1<IHiddenRegionFormatter> _function_2 = (IHiddenRegionFormatter it) -> {
      it.noSpace();
    };
    document.append(document.prepend(this.textRegionExtensions.regionFor(targetRef).keyword(this._activityGrammarAccess.getTargetReferenceAccess().getVerticalLineKeyword_1_1_0()), _function_1), _function_2);
  }
  
  public void format(final Object move, final IFormattableDocument document) {
    if (move instanceof Move) {
      _format((Move)move, document);
      return;
    } else if (move instanceof RaiseEvent) {
      _format((RaiseEvent)move, document);
      return;
    } else if (move instanceof RequireEvent) {
      _format((RequireEvent)move, document);
      return;
    } else if (move instanceof SimpleAction) {
      _format((SimpleAction)move, document);
      return;
    } else if (move instanceof Claim) {
      _format((Claim)move, document);
      return;
    } else if (move instanceof Release) {
      _format((Release)move, document);
      return;
    } else if (move instanceof Event) {
      _format((Event)move, document);
      return;
    } else if (move instanceof XtextResource) {
      _format((XtextResource)move, document);
      return;
    } else if (move instanceof Activity) {
      _format((Activity)move, document);
      return;
    } else if (move instanceof ActivitySet) {
      _format((ActivitySet)move, document);
      return;
    } else if (move instanceof LocationPrerequisite) {
      _format((LocationPrerequisite)move, document);
      return;
    } else if (move instanceof Edge) {
      _format((Edge)move, document);
      return;
    } else if (move instanceof TargetReference) {
      _format((TargetReference)move, document);
      return;
    } else if (move instanceof SourceReference) {
      _format((SourceReference)move, document);
      return;
    } else if (move instanceof EObject) {
      _format((EObject)move, document);
      return;
    } else if (move == null) {
      _format((Void)null, document);
      return;
    } else if (move != null) {
      _format(move, document);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(move, document).toString());
    }
  }
}
