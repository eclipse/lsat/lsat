/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor.validation;

import activity.Action;
import activity.Activity;
import activity.ActivityPackage;
import activity.ActivitySet;
import activity.Claim;
import activity.Event;
import activity.EventAction;
import activity.LocationPrerequisite;
import activity.Move;
import activity.PeripheralAction;
import activity.RaiseEvent;
import activity.Release;
import activity.RequireEvent;
import activity.ResourceAction;
import activity.SchedulingType;
import activity.SimpleAction;
import activity.SyncBar;
import activity.impl.ActivityQueries;
import activity.util.ActivityUtil;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import machine.Axis;
import machine.Distance;
import machine.IResource;
import machine.Import;
import machine.Machine;
import machine.MachinePackage;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.Resource;
import machine.ResourceItem;
import machine.ResourceType;
import machine.SymbolicPosition;
import machine.impl.MachineQueries;
import machine.util.ResourcePeripheralKey;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.activity.teditor.validation.AbstractActivityValidator;
import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;
import org.eclipse.lsat.common.graph.directed.editable.EdgQueries;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.queries.QueryableIterable;
import org.eclipse.lsat.common.util.BranchIterable;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.CollectionExtensions;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class ActivityValidator extends AbstractActivityValidator {
  public static final String ALAP_SCENARIO_1 = "invalidAlapScenario1";
  
  public static final String ALAP_SCENARIO_2 = "invalidAlapScenario2";
  
  public static final String ALAP_SCENARIO_3 = "invalidAlapScenario3";
  
  public static final String ALAP_SCENARIO_4 = "invalidAlapScenario4";
  
  public static final String INVALID_CLAIM = "invalidClaim";
  
  public static final String INVALID_PASSIVE_CLAIM = "invalidPassiveClaim";
  
  public static final String SUGGEST_PASSIVE_CLAIM = "suggestPassiveClaim";
  
  public static final String INVALID_RELEASE = "invalidRelease";
  
  public static final String PASSIVE_CLAIM_INCOMING_EDGES = "passiveClaimIncomingEdges";
  
  public static final String PASSIVE_RELEASE_OUTGOING_EDGES = "passiveReleaseOutgoingEdges";
  
  public static final String INVALID_SYNC_BAR = "invalidSyncBar";
  
  public static final String REMOVE_SYNC_BAR = "removeSyncBar";
  
  public static final String NO_INCOMING_EDGES = "noIncomingEdges";
  
  public static final String NO_OUTGOING_EDGES = "noOutgoingEdges";
  
  public static final String DEAD_ACTION = "deadAction";
  
  public static final String DEAD_EVENT = "deadEvent";
  
  public static final String MORE_THAN_ONE_INCOMING_EDGE = "moreThanOneIncomingEdge";
  
  public static final String MORE_THAN_ONE_OUTGOING_EDGE = "moreThanOneOutgoingEdge";
  
  public static final String ONE_CLAIM_PER_RESOURCE_PER_ACT = "oneClaimPerResourcePerActivity";
  
  public static final String ONE_EVENT_WITH_SAME_NAME_PER_ACT = "oneEventWithSameNamePerActivity";
  
  public static final String ONE_RELEASE_PER_RESOURCE_PER_ACT = "oneReleasePerResourcePerActivity";
  
  public static final String NO_CLAIM_DEFINED_FOR_RESOURCE = "noClaimDefinedForResource";
  
  public static final String RESOURCE_NOT_PROPERLY_CLAIMED = "resourceNotProperlyClaimed";
  
  public static final String NO_RELEASE_DEFINED_FOR_RESOURCE = "noReleaseDefinedForResource";
  
  public static final String RESOURCE_NOT_PROPERLY_RELEASED = "resourceNotProperlyReleased";
  
  public static final String CYCLE_DETECTED = "cycleDetected";
  
  public static final String ACTION_IN_PARALLEL_FOR_SAME_PERIPHERAL = "actionInParallelForSamePeripheral";
  
  public static final String NO_LOC_PREREQUISTIE_FOR_PERIPHERAL_FOR_MOVE = "noLocPrereqForPeripheralForMove";
  
  public static final String DUPLICATE_ACTIVITY_NAME = "duplicateActivityName";
  
  public static final String MORE_THAN_ONE_LOCATION_PREREQ_FOR_PERIPHERAL = "moreThanOneLocationPrereqForPeripheral";
  
  public static final String SAME_SOURCE_TARGET_FOR_PERIPHERAL = "sameSourceTargetForPeripheral";
  
  public static final String NO_PATH_FOUND_FOR_PROFILE = "noPathFoundForProfile";
  
  public static final String SETTLING_ALREADY_DEFINED_FOR_PATH = "settlingAlreadyDefinedForPath";
  
  public static final String SETTLING_ALREADY_DEFINED_FOR_MOVE = "settlingAlreadyDefinedForMove";
  
  public static final String NO_CONCAT_FOUND_FOR_MOVE = "noConcatFoundForMove";
  
  public static final String INVALID_IMPORT = "invalidImport";
  
  public static final String INVALID_RESOURCE = "invalidResource";
  
  public static final String DUPLICATE_EVENT_RESOURCE_NAME = "duplicateEventResourceName";
  
  @Check
  public void checkUniqueEventAndResourceNames(final Event event) {
    EObject _eContainer = event.eContainer();
    final EList<Import> imports = ((ActivitySet) _eContainer).getImports();
    final Function1<Import, Boolean> _function = (Import it) -> {
      final Function1<Machine, EList<Resource>> _function_1 = (Machine it_1) -> {
        return it_1.getResources();
      };
      final Function1<Resource, Boolean> _function_2 = (Resource it_1) -> {
        String _name = it_1.getName();
        String _name_1 = event.getName();
        return Boolean.valueOf(Objects.equal(_name, _name_1));
      };
      return Boolean.valueOf(IterableExtensions.<Resource>exists(IterableExtensions.<Machine, Resource>flatMap(Iterables.<Machine>filter(it.load(), Machine.class), _function_1), _function_2));
    };
    final Iterable<Import> conflictingImports = IterableExtensions.<Import>filter(imports, _function);
    for (final Import conflictingImport : conflictingImports) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Import ");
      String _importURI = conflictingImport.getImportURI();
      _builder.append(_importURI);
      _builder.append(" already defines a resource with name ");
      String _name = event.getName();
      _builder.append(_name);
      _builder.append(". Event and resource names should be unique.");
      this.error(_builder.toString(), 
        MachinePackage.Literals.IRESOURCE__NAME, ActivityValidator.DUPLICATE_EVENT_RESOURCE_NAME);
    }
  }
  
  @Check
  public void checkImportIsValid(final Import imp) {
    try {
      final boolean isImportUriValid = EcoreUtil2.isValidUri(imp, URI.createURI(imp.getImportURI()));
      if ((!isImportUriValid)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("The import ");
        String _importURI = imp.getImportURI();
        _builder.append(_importURI);
        _builder.append(" cannot be resolved. Make sure that the name is spelled correctly.");
        this.error(_builder.toString(), imp, MachinePackage.Literals.IMPORT__IMPORT_URI, ActivityValidator.INVALID_IMPORT);
      }
      final boolean isUnderstood = imp.getImportURI().matches(".*\\.(machine|activity)");
      if ((!isUnderstood)) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Importing ");
        String _importURI_1 = imp.getImportURI();
        _builder_1.append(_importURI_1);
        _builder_1.append(" is not allowed. Only \'machine\' files are allowed");
        this.error(_builder_1.toString(), imp, 
          MachinePackage.Literals.IMPORT__IMPORT_URI, ActivityValidator.INVALID_IMPORT);
      }
    } catch (final Throwable _t) {
      if (_t instanceof IllegalArgumentException) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("The import ");
        String _importURI_2 = imp.getImportURI();
        _builder_2.append(_importURI_2);
        _builder_2.append(" is not a valid URI.");
        this.error(_builder_2.toString(), imp, 
          MachinePackage.Literals.IMPORT__IMPORT_URI, ActivityValidator.INVALID_IMPORT);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  /**
   * If ASAP required, guarantees that it will be ASAP
   */
  public boolean concatenatesASAP(final Action action) {
    boolean _switchResult = false;
    boolean _matched = false;
    if (action instanceof PeripheralAction) {
      _matched=true;
      SchedulingType _schedulingType = ((PeripheralAction)action).getSchedulingType();
      _switchResult = Objects.equal(_schedulingType, SchedulingType.ASAP);
    }
    if (!_matched) {
      if (action instanceof Release) {
        _matched=true;
        _switchResult = true;
      }
    }
    if (!_matched) {
      _switchResult = false;
    }
    final boolean willDoASAP = _switchResult;
    return (willDoASAP && (IterableExtensions.size(EdgQueries.<Action>nearestPredecessors(action, Action.class)) <= 1));
  }
  
  /**
   * If ALAP required, guarantees that it will be ALAP
   */
  public boolean concatenatesALAP(final Action action) {
    boolean _switchResult = false;
    boolean _matched = false;
    if (action instanceof PeripheralAction) {
      _matched=true;
      SchedulingType _schedulingType = ((PeripheralAction)action).getSchedulingType();
      _switchResult = Objects.equal(_schedulingType, SchedulingType.ALAP);
    }
    if (!_matched) {
      if (action instanceof Claim) {
        _matched=true;
        _switchResult = true;
      }
    }
    if (!_matched) {
      _switchResult = false;
    }
    final boolean willDoALAP = _switchResult;
    return (willDoALAP && (IterableExtensions.size(EdgQueries.<Action>nearestSuccessors(action, Action.class)) <= 1));
  }
  
  @Check
  public void checkActionIsALAPWithSuccessorIsASAP(final PeripheralAction action) {
    if ((Objects.equal(action.getSchedulingType(), SchedulingType.ALAP) && IterableExtensions.<Action>exists(EdgQueries.<Action>nearestSuccessors(action, Action.class), ((Function1<Action, Boolean>) (Action it) -> {
      return Boolean.valueOf(this.concatenatesASAP(it));
    })))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("The ALAP keyword does not have any affect for action ");
      String _name = action.getName();
      _builder.append(_name);
      _builder.append(".");
      this.warning(_builder.toString(), 
        ActivityPackage.Literals.PERIPHERAL_ACTION__SCHEDULING_TYPE, ActivityValidator.ALAP_SCENARIO_1);
    }
  }
  
  @Check
  public void checkConcatenatedMove(final Move move) {
    final Move successorMove = move.getSuccessorMove();
    boolean _isStopAtTarget = move.isStopAtTarget();
    final boolean isContinuing = (!_isStopAtTarget);
    if (isContinuing) {
      final Set<PeripheralAction> successorPeripheralActions = IterableExtensions.<PeripheralAction>toSet(EdgQueries.<PeripheralAction>nearestSuccessors(move, PeripheralAction.class));
      if ((successorMove == null)) {
        StringConcatenation _builder = new StringConcatenation();
        String _name = move.getName();
        _builder.append(_name);
        _builder.append(" should be concatenated with another move. ");
        this.error(_builder.toString(), move, 
          EdgPackage.Literals.NODE__NAME, ActivityValidator.NO_CONCAT_FOUND_FOR_MOVE);
        return;
      } else {
        boolean _contains = successorPeripheralActions.contains(successorMove);
        boolean _not = (!_contains);
        if (_not) {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("Failed to concatenate ");
          String _name_1 = move.getName();
          _builder_1.append(_name_1);
          _builder_1.append(" and ");
          String _name_2 = successorMove.getName();
          _builder_1.append(_name_2);
          _builder_1.append(", only sync bars, claims, releases or events are allowed in between.");
          this.error(_builder_1.toString(), move, EdgPackage.Literals.NODE__NAME, ActivityValidator.NO_CONCAT_FOUND_FOR_MOVE);
          return;
        }
      }
    }
    final Move predecessorMove = move.getPredecessorMove();
    final boolean isContinuated = ((predecessorMove != null) && (!predecessorMove.isStopAtTarget()));
    final Function1<Action, Iterable<? extends Action>> _function = (Action it) -> {
      return EdgQueries.<Action>nearestSuccessors(it, Action.class);
    };
    final Function1<Action, Boolean> _function_1 = (Action it) -> {
      return Boolean.valueOf(Objects.equal(it, move));
    };
    final BranchIterable<Action> actionsUntilPredecessorMove = Queries.<Action>until(Queries.<Action>closure(Collections.<Action>singleton(((Action) predecessorMove)), true, _function), _function_1);
    final Function1<Action, Iterable<? extends Action>> _function_2 = (Action it) -> {
      return EdgQueries.<Action>nearestPredecessors(it, Action.class);
    };
    final Function1<Action, Boolean> _function_3 = (Action it) -> {
      return Boolean.valueOf(Objects.equal(it, move));
    };
    final BranchIterable<Action> actionsUntilSuccessorMove = Queries.<Action>until(Queries.<Action>closure(Collections.<Action>singleton(((Action) successorMove)), true, _function_2), _function_3);
    final boolean requiresASAP = (isContinuated && IterableExtensions.<Action>exists(actionsUntilPredecessorMove, ((Function1<Action, Boolean>) (Action it) -> {
      boolean _concatenatesALAP = this.concatenatesALAP(it);
      return Boolean.valueOf((!_concatenatesALAP));
    })));
    final boolean requiresALAP = ((isContinuing && IterableExtensions.<Action>exists(actionsUntilSuccessorMove, ((Function1<Action, Boolean>) (Action it) -> {
      boolean _concatenatesASAP = this.concatenatesASAP(it);
      return Boolean.valueOf((!_concatenatesASAP));
    }))) && (!IterableExtensions.<Action>exists(EdgQueries.<Action>nearestSuccessors(move, Action.class), ((Function1<Action, Boolean>) (Action it) -> {
      return Boolean.valueOf(this.concatenatesASAP(it));
    }))));
    if ((requiresASAP && requiresALAP)) {
      final Set<Node> predecessors = IterableExtensions.<Node>toSet(EdgQueries.allPredecessors(successorMove));
      predecessors.remove(move);
      CollectionExtensions.<Node>removeAll(predecessors, EdgQueries.allPredecessors(move));
      Iterable<Node> _reject = IterableExtensions.<Node>reject(IterableExtensions.<Node>reject(predecessors, Release.class), SyncBar.class);
      for (final Node pred : _reject) {
        {
          final Activity activity = EcoreUtil2.<Activity>getContainerOfType(pred, Activity.class);
          final int predIndex = activity.getNodes().indexOf(pred);
          StringConcatenation _builder_2 = new StringConcatenation();
          String _name_3 = pred.getName();
          _builder_2.append(_name_3);
          _builder_2.append(" might interrupt concatenated move [");
          String _name_4 = move.getName();
          _builder_2.append(_name_4);
          _builder_2.append("->");
          String _name_5 = successorMove.getName();
          _builder_2.append(_name_5);
          _builder_2.append("]");
          this.warning(_builder_2.toString(), activity, EdgPackage.Literals.EDITABLE_DIRECTED_GRAPH__NODES, predIndex, ActivityValidator.ALAP_SCENARIO_4);
        }
      }
    } else {
      if ((requiresASAP && Objects.equal(move.getSchedulingType(), SchedulingType.ALAP))) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("Move ");
        String _name_3 = move.getName();
        _builder_2.append(_name_3);
        _builder_2.append(" must be set to ASAP to guarantee concatenation with predecessor move ");
        String _name_4 = predecessorMove.getName();
        _builder_2.append(_name_4);
        _builder_2.append(".");
        this.error(_builder_2.toString(), 
          ActivityPackage.Literals.PERIPHERAL_ACTION__SCHEDULING_TYPE, ActivityValidator.ALAP_SCENARIO_3);
        return;
      } else {
        if ((requiresALAP && Objects.equal(move.getSchedulingType(), SchedulingType.ASAP))) {
          StringConcatenation _builder_3 = new StringConcatenation();
          _builder_3.append("Move ");
          String _name_5 = move.getName();
          _builder_3.append(_name_5);
          _builder_3.append(" must be set to ALAP to guarantee concatenation with successor move ");
          String _name_6 = successorMove.getName();
          _builder_3.append(_name_6);
          _builder_3.append(".");
          this.error(_builder_3.toString(), 
            ActivityPackage.Literals.PERIPHERAL_ACTION__SCHEDULING_TYPE, ActivityValidator.ALAP_SCENARIO_2);
          return;
        }
      }
    }
  }
  
  /**
   * This method makes sure that a Claim has outgoing edges.
   */
  @Check
  public void checkIfUsedInActivityFlow(final Claim claim) {
    boolean _isEmpty = claim.getOutgoingEdges().isEmpty();
    if (_isEmpty) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Claim ");
      String _name = claim.getName();
      _builder.append(_name);
      _builder.append(" has no outgoing edges.");
      this.error(_builder.toString(), claim, 
        EdgPackage.Literals.NODE__OUTGOING_EDGES, ActivityValidator.INVALID_CLAIM);
    }
  }
  
  /**
   * This event action pointing to EventResource
   */
  @Check
  public void checkEvent(final EventAction event) {
    ResourceType _resourceType = event.getResource().getResource().getResourceType();
    boolean _tripleNotEquals = (_resourceType != ResourceType.EVENT);
    if (_tripleNotEquals) {
      String _xifexpression = null;
      if ((event instanceof RequireEvent)) {
        _xifexpression = "Require";
      } else {
        _xifexpression = "Raise";
      }
      final String name = _xifexpression;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(name);
      _builder.append(" ");
      String _fqn = event.getResource().fqn();
      _builder.append(_fqn);
      _builder.append(" is not an Event");
      this.error(_builder.toString(), event, 
        ActivityPackage.Literals.RESOURCE_ACTION__RESOURCE, ActivityValidator.INVALID_RESOURCE);
    }
  }
  
  @Check
  public void checkResource(final ResourceAction action) {
    if ((action instanceof EventAction)) {
      return;
    }
    ResourceType _resourceType = action.getResource().getResource().getResourceType();
    boolean _tripleNotEquals = (_resourceType != ResourceType.REGULAR);
    if (_tripleNotEquals) {
      final String name = action.getClass().getSimpleName();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(name);
      _builder.append(" ");
      String _fqn = action.getResource().fqn();
      _builder.append(_fqn);
      _builder.append(" is not a machine resource.");
      this.error(_builder.toString(), action, 
        ActivityPackage.Literals.RESOURCE_ACTION__RESOURCE, ActivityValidator.INVALID_RESOURCE);
    }
  }
  
  /**
   * This method checks passive claims in relation to actions on the resource.
   */
  @Check
  public void checkPassiveClaim(final Claim claim) {
    ResourceType _resourceType = claim.getResource().getResource().getResourceType();
    boolean _tripleNotEquals = (_resourceType != ResourceType.REGULAR);
    if (_tripleNotEquals) {
      return;
    }
    EditableDirectedGraph _graph = claim.getGraph();
    final Activity activity = ((Activity) _graph);
    final Function1<PeripheralAction, Boolean> _function = (PeripheralAction it) -> {
      IResource _resource = it.getResource();
      IResource _resource_1 = claim.getResource();
      return Boolean.valueOf((_resource == _resource_1));
    };
    boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<PeripheralAction>filter(Iterables.<PeripheralAction>filter(activity.getNodes(), PeripheralAction.class), _function));
    final boolean hasActions = (!_isEmpty);
    final boolean isCollisionArea = claim.getResource().getResource().getPeripherals().isEmpty();
    boolean _isPassive = claim.isPassive();
    if (_isPassive) {
      if (hasActions) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Claim ");
        String _name = claim.getName();
        _builder.append(_name);
        _builder.append(" cannot be passive. There are actions for resource ");
        String _fqn = claim.getResource().fqn();
        _builder.append(_fqn);
        _builder.append(".");
        this.error(_builder.toString(), claim, 
          ActivityPackage.Literals.CLAIM__PASSIVE, ActivityValidator.INVALID_PASSIVE_CLAIM);
        return;
      }
      boolean _isEmpty_1 = claim.getIncomingEdges().isEmpty();
      boolean _not = (!_isEmpty_1);
      if (_not) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Passive claim ");
        String _name_1 = claim.getName();
        _builder_1.append(_name_1);
        _builder_1.append(" should not have incoming edges as they may possibly block other activities.");
        this.warning(_builder_1.toString(), claim, 
          EdgPackage.Literals.NODE__INCOMING_EDGES, ActivityValidator.PASSIVE_CLAIM_INCOMING_EDGES);
      }
      final Function1<Release, Boolean> _function_1 = (Release it) -> {
        return Boolean.valueOf(it.getOutgoingEdges().isEmpty());
      };
      Iterable<Release> _reject = IterableExtensions.<Release>reject(ActivityUtil.getReleases(activity, claim.getResource()), _function_1);
      for (final Release release : _reject) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("Passive release ");
        String _name_2 = release.getName();
        _builder_2.append(_name_2);
        _builder_2.append(" should not have outgoing edges as they may possibly block other activities.");
        this.warning(_builder_2.toString(), release, 
          EdgPackage.Literals.NODE__OUTGOING_EDGES, ActivityValidator.PASSIVE_RELEASE_OUTGOING_EDGES);
      }
    } else {
      if (((!isCollisionArea) && (!hasActions))) {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append("Claim ");
        String _name_3 = claim.getName();
        _builder_3.append(_name_3);
        _builder_3.append(" can be made passive. There are no actions for resource ");
        String _fqn_1 = claim.getResource().fqn();
        _builder_3.append(_fqn_1);
        _builder_3.append(".");
        this.info(_builder_3.toString(), claim, 
          ActivityPackage.Literals.CLAIM__PASSIVE, ActivityValidator.SUGGEST_PASSIVE_CLAIM);
      }
    }
  }
  
  /**
   * This method makes sure that a Release has incoming edges.
   */
  @Check
  public void checkIfUsedInActivityFlow(final Release release) {
    boolean _isEmpty = release.getIncomingEdges().isEmpty();
    if (_isEmpty) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Release ");
      String _name = release.getName();
      _builder.append(_name);
      _builder.append(" has no incoming edges.");
      this.error(_builder.toString(), release, 
        EdgPackage.Literals.NODE__INCOMING_EDGES, ActivityValidator.INVALID_RELEASE);
    }
  }
  
  /**
   * This method makes sure that a SyncBar has incoming edges.
   */
  @Check
  public void checkHasEdges(final SyncBar syncBar) {
    if (((syncBar.getOutgoingEdges().size() > 0) && syncBar.getIncomingEdges().isEmpty())) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("SyncBar ");
      String _name = syncBar.getName();
      _builder.append(_name);
      _builder.append(" has no incoming edges.");
      this.error(_builder.toString(), syncBar.getOutgoingEdges().get(0), 
        EdgPackage.Literals.EDGE__SOURCE_NODE, ActivityValidator.INVALID_SYNC_BAR);
    }
    if (((syncBar.getIncomingEdges().size() == 1) && (syncBar.getOutgoingEdges().size() == 1))) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("SyncBar ");
      String _name_1 = syncBar.getName();
      _builder_1.append(_name_1);
      _builder_1.append(" with only 1 incoming and 1 outgoing edge can be removed.");
      this.warning(_builder_1.toString(), 
        syncBar.getOutgoingEdges().get(0), EdgPackage.Literals.EDGE__SOURCE_NODE, ActivityValidator.REMOVE_SYNC_BAR);
    }
  }
  
  /**
   * There are not many restriction on events
   * Only check if they are used.
   */
  @Check
  public void checkIfHasPredecessorAction(final RaiseEvent event) {
    boolean _isEmpty = IterableExtensions.isEmpty(EdgQueries.<Action>nearestPredecessors(event, Action.class));
    if (_isEmpty) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Event \'");
      String _name = event.getResource().getName();
      _builder.append(_name);
      _builder.append("\' should be preceded with an action.");
      this.error(_builder.toString(), event, 
        EdgPackage.Literals.NODE__NAME, ActivityValidator.DEAD_EVENT);
    }
  }
  
  /**
   * There are not many restriction on events
   * Only check if they are used.
   */
  @Check
  public void checkIfHasSuccessorAction(final RequireEvent event) {
    boolean _isEmpty = IterableExtensions.isEmpty(EdgQueries.<Action>nearestSuccessors(event, Action.class));
    if (_isEmpty) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Event ");
      String _name = event.getResource().getName();
      _builder.append(_name);
      _builder.append(" should be succeeded by an action.");
      this.error(_builder.toString(), event, 
        EdgPackage.Literals.NODE__NAME, ActivityValidator.DEAD_EVENT);
    }
  }
  
  /**
   * Check if event.resource.fqn is unique within activity.
   */
  @Check
  public void checkEventNameUnique(final Activity activity) {
    final Function1<EventAction, String> _function = (EventAction it) -> {
      return it.getResource().fqn();
    };
    final Function1<List<EventAction>, Boolean> _function_1 = (List<EventAction> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 2));
    };
    final Consumer<EventAction> _function_2 = (EventAction it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("One require and/or raise event with name \'");
      String _fqn = it.getResource().fqn();
      _builder.append(_fqn);
      _builder.append("\' per activity are allowed.");
      this.error(_builder.toString(), it, 
        ActivityPackage.Literals.RESOURCE_ACTION__RESOURCE, ActivityValidator.ONE_EVENT_WITH_SAME_NAME_PER_ACT);
    };
    Iterables.<EventAction>concat(IterableExtensions.<List<EventAction>>filter(IterableExtensions.<String, EventAction>groupBy(Iterables.<EventAction>filter(activity.getNodes(), EventAction.class), _function).values(), _function_1)).forEach(_function_2);
  }
  
  /**
   * Check if event.resource.fqn is unique within activity.
   */
  @Check
  public void checkRequireRaise(final Activity activity) {
    final Function1<EventAction, String> _function = (EventAction it) -> {
      return it.getResource().fqn();
    };
    final Function1<List<EventAction>, Boolean> _function_1 = (List<EventAction> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size == 2));
    };
    final Function1<List<EventAction>, Boolean> _function_2 = (List<EventAction> it) -> {
      return Boolean.valueOf((!((it.get(0) instanceof RequireEvent) && (it.get(1) instanceof RaiseEvent))));
    };
    final Consumer<List<EventAction>> _function_3 = (List<EventAction> seq) -> {
      final Consumer<EventAction> _function_4 = (EventAction it) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Expecting \'require\' followed by \'raise\' for event \'");
        String _fqn = it.getResource().fqn();
        _builder.append(_fqn);
        _builder.append("\', not \'");
        String _type = this.type(seq.get(0));
        _builder.append(_type);
        _builder.append("\' followed by \'");
        String _type_1 = this.type(seq.get(1));
        _builder.append(_type_1);
        _builder.append("\' ");
        this.error(_builder.toString(), it, ActivityPackage.Literals.RESOURCE_ACTION__RESOURCE, ActivityValidator.ONE_EVENT_WITH_SAME_NAME_PER_ACT);
      };
      seq.forEach(_function_4);
    };
    IterableExtensions.<List<EventAction>>filter(IterableExtensions.<List<EventAction>>filter(IterableExtensions.<String, EventAction>groupBy(Iterables.<EventAction>filter(activity.allNodesInTopologicalOrder(), EventAction.class), _function).values(), _function_1), _function_2).forEach(_function_3);
  }
  
  public String type(final EventAction event) {
    String _xifexpression = null;
    if ((event instanceof RequireEvent)) {
      _xifexpression = "require";
    } else {
      _xifexpression = "raise";
    }
    return _xifexpression;
  }
  
  /**
   * This methods makes sure that a PeripheralAction is not a dead end by verifying
   * that it has incoming and outgoing edges.
   */
  @Check
  public void checkIfUsedInActivityFlow(final PeripheralAction action) {
    if ((action.getIncomingEdges().isEmpty() && action.getOutgoingEdges().isEmpty())) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = action.getName();
      _builder.append(_name);
      _builder.append(" is a dead action. It has no incoming and no outgoing edges.");
      this.error(_builder.toString(), action, 
        EdgPackage.Literals.NODE__NAME, ActivityValidator.DEAD_ACTION);
    } else {
      boolean _isEmpty = action.getIncomingEdges().isEmpty();
      if (_isEmpty) {
        StringConcatenation _builder_1 = new StringConcatenation();
        String _name_1 = action.getName();
        _builder_1.append(_name_1);
        _builder_1.append(" is a dead action. It has no incoming edges.");
        this.error(_builder_1.toString(), action, 
          EdgPackage.Literals.NODE__INCOMING_EDGES, ActivityValidator.NO_INCOMING_EDGES);
      } else {
        boolean _isEmpty_1 = action.getOutgoingEdges().isEmpty();
        if (_isEmpty_1) {
          StringConcatenation _builder_2 = new StringConcatenation();
          String _name_2 = action.getName();
          _builder_2.append(_name_2);
          _builder_2.append(" is a dead action. It has no outgoing edges.");
          this.error(_builder_2.toString(), action, 
            EdgPackage.Literals.NODE__OUTGOING_EDGES, ActivityValidator.NO_OUTGOING_EDGES);
        }
      }
    }
  }
  
  /**
   * This methods makes sure that an Action has only one incoming and one outgoing edge.
   */
  @Check
  public void checkActionHasOneIncomingAndOneOutgoingEdge(final Node node) {
    if ((node instanceof SyncBar)) {
      return;
    }
    int _size = node.getIncomingEdges().size();
    boolean _greaterThan = (_size > 1);
    if (_greaterThan) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = node.getName();
      _builder.append(_name);
      _builder.append(" can only have one incoming dependency. Use sync bars instead.");
      this.warning(_builder.toString(), node, 
        EdgPackage.Literals.NODE__INCOMING_EDGES, ActivityValidator.MORE_THAN_ONE_INCOMING_EDGE);
    } else {
      int _size_1 = node.getOutgoingEdges().size();
      boolean _greaterThan_1 = (_size_1 > 1);
      if (_greaterThan_1) {
        StringConcatenation _builder_1 = new StringConcatenation();
        String _name_1 = node.getName();
        _builder_1.append(_name_1);
        _builder_1.append(" can only have one outgoing dependency. Use sync bars instead.");
        this.warning(_builder_1.toString(), node, 
          EdgPackage.Literals.NODE__OUTGOING_EDGES, ActivityValidator.MORE_THAN_ONE_OUTGOING_EDGE);
      }
    }
  }
  
  /**
   * This method makes sure that only one claim per resource in an activity is used.
   * The method collects all of the claims from an activity in a hashmap, with a resource as a key.
   * If a claim already exists for that resource, then we raise an error.
   */
  @Check
  public void checkOneClaimPerResourcePerActivity(final Activity activity) {
    final Function1<Claim, IResource> _function = (Claim it) -> {
      return it.getResource();
    };
    final Function1<List<Claim>, Boolean> _function_1 = (List<Claim> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<Claim> _function_2 = (Claim duplicateClaim) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Only one claim per resource ");
      String _name = duplicateClaim.getResource().getName();
      _builder.append(_name);
      _builder.append(" per activity is allowed.");
      this.error(_builder.toString(), duplicateClaim, EdgPackage.Literals.NODE__NAME, ActivityValidator.ONE_CLAIM_PER_RESOURCE_PER_ACT);
    };
    Iterables.<Claim>concat(IterableExtensions.<List<Claim>>filter(IterableExtensions.<IResource, Claim>groupBy(Iterables.<Claim>filter(activity.getNodes(), Claim.class), _function).values(), _function_1)).forEach(_function_2);
  }
  
  /**
   * This method makes sure that only one Release per resource in an activity is used.
   * The method collects all of the Release from an activity in a hashmap, with a resource as a key.
   * If a Release already exists for that resource, then we raise an error.
   */
  @Check
  public void checkOneReleasePerResourcePerActivity(final Activity activity) {
    final Function1<Release, IResource> _function = (Release it) -> {
      return it.getResource();
    };
    final Function1<List<Release>, Boolean> _function_1 = (List<Release> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<Release> _function_2 = (Release duplicateRelease) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Only one release per resource ");
      String _name = duplicateRelease.getResource().getName();
      _builder.append(_name);
      _builder.append(" per activity is allowed.");
      this.error(_builder.toString(), duplicateRelease, EdgPackage.Literals.NODE__NAME, ActivityValidator.ONE_RELEASE_PER_RESOURCE_PER_ACT);
    };
    Iterables.<Release>concat(IterableExtensions.<List<Release>>filter(IterableExtensions.<IResource, Release>groupBy(Iterables.<Release>filter(activity.getNodes(), Release.class), _function).values(), _function_1)).forEach(_function_2);
  }
  
  /**
   * This methods checks two things:
   * 1) If a claim is defined for a resource on the current PeripheralAction.
   * 2) If a claim exists for the PeripheralAction.
   */
  @Check
  public void checkActionHasAValidClaimAndResource(final PeripheralAction action) {
    final Function1<Claim, IResource> _function = (Claim it) -> {
      return it.getResource();
    };
    Claim claim = IterableExtensions.<IResource, Claim>toMap(Queries.<Claim>objectsOfKind(action.getGraph().getNodes(), Claim.class), _function).get(action.getResource());
    if ((null == claim)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("No claim defined for resource ");
      String _name = action.getResource().getName();
      _builder.append(_name);
      _builder.append(". It is required before ");
      String _name_1 = action.getName();
      _builder.append(_name_1);
      _builder.append(" can be used.");
      this.error(_builder.toString(), claim, EdgPackage.Literals.NODE__NAME, ActivityValidator.NO_CLAIM_DEFINED_FOR_RESOURCE);
    } else {
      boolean _contains = IterableExtensions.contains(EdgQueries.allPredecessors(action), claim);
      boolean _not = (!_contains);
      if (_not) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Resource ");
        String _name_2 = action.getResource().getName();
        _builder_1.append(_name_2);
        _builder_1.append(" is not properly claimed (by ");
        String _name_3 = claim.getName();
        _builder_1.append(_name_3);
        _builder_1.append(") before ");
        String _name_4 = action.getName();
        _builder_1.append(_name_4);
        _builder_1.append(" is used.");
        this.warning(_builder_1.toString(), claim, EdgPackage.Literals.NODE__NAME, ActivityValidator.RESOURCE_NOT_PROPERLY_CLAIMED);
      }
    }
  }
  
  /**
   * This methods checks two things:
   * 1) If a Release is defined for a resource on the current PeripheralAction.
   * 2) If a Release exists for the PeripheralAction.
   */
  @Check
  public void checkActionHasAValidReleaseAndResource(final PeripheralAction action) {
    final Function1<Release, IResource> _function = (Release it) -> {
      return it.getResource();
    };
    Release release = IterableExtensions.<IResource, Release>toMap(Queries.<Release>objectsOfKind(action.getGraph().getNodes(), Release.class), _function).get(action.getResource());
    if ((null == release)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("No release defined for resource ");
      String _name = action.getResource().getName();
      _builder.append(_name);
      _builder.append(". It is required before ");
      String _name_1 = action.getName();
      _builder.append(_name_1);
      _builder.append(" can be used.");
      this.error(_builder.toString(), action, EdgPackage.Literals.NODE__NAME, ActivityValidator.NO_RELEASE_DEFINED_FOR_RESOURCE);
    } else {
      boolean _contains = IterableExtensions.contains(EdgQueries.allSuccessors(action), release);
      boolean _not = (!_contains);
      if (_not) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Resource ");
        String _name_2 = action.getResource().getName();
        _builder_1.append(_name_2);
        _builder_1.append(" is not properly released (by ");
        String _name_3 = release.getName();
        _builder_1.append(_name_3);
        _builder_1.append(") before ");
        String _name_4 = action.getName();
        _builder_1.append(_name_4);
        _builder_1.append(" is used.");
        this.warning(_builder_1.toString(), action, EdgPackage.Literals.NODE__NAME, ActivityValidator.RESOURCE_NOT_PROPERLY_RELEASED);
      }
    }
  }
  
  /**
   * This method makes sure that a PeripheralAction has not created a cycle.
   * It works by making sure there is no intersection between the predecessors and successors
   * of the PeripheralAction. It does, however, show multiple errors as it is per action.
   */
  @Check
  public void checkForCycles(final PeripheralAction action) {
    Set<Node> cycle = IterableExtensions.<Node>toSet(EdgQueries.allSuccessors(action));
    cycle.retainAll(IterableExtensions.<Node>toSet(EdgQueries.allPredecessors(action)));
    boolean _isEmpty = cycle.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Actions ");
      final Function1<Node, String> _function = (Node it) -> {
        return it.getName();
      };
      String _join = IterableExtensions.join(IterableExtensions.<Node, String>map(cycle, _function), ",");
      _builder.append(_join);
      _builder.append(" have a cyclic dependency.");
      this.error(_builder.toString(), action, 
        EdgPackage.Literals.NODE__NAME, ActivityValidator.CYCLE_DETECTED);
    }
  }
  
  /**
   * This method makes sure that (expanded) activities do not have the same name
   */
  @Check
  public void checkForPotentiallyDuplicateActivityNames(final ActivitySet activitySet) {
    final Function1<Activity, Set<Map.Entry<String, Activity>>> _function = (Activity it) -> {
      return this.expandedNames(it);
    };
    final Function1<Map.Entry<String, Activity>, String> _function_1 = (Map.Entry<String, Activity> it) -> {
      return it.getKey();
    };
    final Function1<List<Map.Entry<String, Activity>>, Boolean> _function_2 = (List<Map.Entry<String, Activity>> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<Map.Entry<String, Activity>> _function_3 = (Map.Entry<String, Activity> da) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Activity \'");
      String _name = da.getValue().getName();
      _builder.append(_name);
      _builder.append("\' conflicts with other activities. Please remove all conflicting instances.");
      this.error(_builder.toString(), 
        da.getValue(), EdgPackage.Literals.EDITABLE_DIRECTED_GRAPH__NAME, ActivityValidator.DUPLICATE_ACTIVITY_NAME);
    };
    Iterables.<Map.Entry<String, Activity>>concat(IterableExtensions.<List<Map.Entry<String, Activity>>>filter(IterableExtensions.<String, Map.Entry<String, Activity>>groupBy(Queries.<Activity, Map.Entry<String, Activity>>collect(activitySet.getActivities(), _function), _function_1).values(), _function_2)).forEach(_function_3);
  }
  
  private Set<Map.Entry<String, Activity>> expandedNames(final Activity activity) {
    final HashMap<String, Activity> result = CollectionLiterals.<String, Activity>newHashMap();
    result.put(activity.getName(), activity);
    final Function1<ResourceAction, IResource> _function = (ResourceAction it) -> {
      return it.getResource();
    };
    final Function1<Resource, Boolean> _function_1 = (Resource it) -> {
      return Boolean.valueOf(it.getItems().isEmpty());
    };
    final Function1<Resource, Set<ResourceItem>> _function_2 = (Resource it) -> {
      return IterableExtensions.<ResourceItem>toSet(it.getItems());
    };
    final Function1<List<ResourceItem>, String> _function_3 = (List<ResourceItem> it) -> {
      return ActivityUtil.expandName(activity, it);
    };
    final Consumer<String> _function_4 = (String it) -> {
      result.put(it, activity);
    };
    IterableExtensions.<List<ResourceItem>, String>map(this.cartesianProduct(IterableExtensions.<Set<ResourceItem>>toList(IterableExtensions.<Resource, Set<ResourceItem>>map(Queries.<Resource>unique(IterableExtensions.<Resource>reject(Iterables.<Resource>filter(IterableExtensions.<ResourceAction, IResource>map(Iterables.<ResourceAction>filter(activity.getNodes(), ResourceAction.class), _function), Resource.class), _function_1)), _function_2))), _function_3).forEach(_function_4);
    return result.entrySet();
  }
  
  private Set<List<ResourceItem>> cartesianProduct(final List<Set<ResourceItem>> r) {
    return Sets.<ResourceItem>cartesianProduct(r);
  }
  
  /**
   * This method checks if a PeripheralAction is done in parallel with another action on the same Peripheral.
   * It works by calculating the actions for the current peripheral action's peripheral and then checking if any of them are a predecessor
   * or successor of the current action.
   */
  @Check
  public void checkActionInParallelForSamePeripheral(final PeripheralAction pAction) {
    final EList<Node> allNodesInTheGraph = pAction.getGraph().getNodes();
    final int indexOfpAction = allNodesInTheGraph.indexOf(pAction);
    QueryableIterable<PeripheralAction> _actionsFor = ActivityQueries.<PeripheralAction>getActionsFor(pAction.getResource(), pAction.getPeripheral(), PeripheralAction.class, 
      allNodesInTheGraph.subList((indexOfpAction + 1), allNodesInTheGraph.size()));
    for (final PeripheralAction otherAction : _actionsFor) {
      boolean _not = (!(IterableExtensions.contains(EdgQueries.allSuccessors(pAction), otherAction) || IterableExtensions.contains(EdgQueries.allPredecessors(pAction), otherAction)));
      if (_not) {
        StringConcatenation _builder = new StringConcatenation();
        String _name = pAction.getName();
        _builder.append(_name);
        _builder.append(" cannot be done in parallel with ");
        String _name_1 = otherAction.getName();
        _builder.append(_name_1);
        _builder.append(", as they share the same peripheral.");
        this.error(_builder.toString(), pAction, EdgPackage.Literals.NODE__NAME, ActivityValidator.ACTION_IN_PARALLEL_FOR_SAME_PERIPHERAL);
      }
    }
  }
  
  /**
   * This method makes sure оnly one location prerequisite per peripheral is allowed.
   */
  @Check
  public HashMap<ResourcePeripheralKey, SymbolicPosition> indexPrerequisites(final Activity activity) {
    boolean _isNullOrEmpty = IterableExtensions.isNullOrEmpty(activity.getPrerequisites());
    if (_isNullOrEmpty) {
      return new HashMap<ResourcePeripheralKey, SymbolicPosition>();
    }
    int _size = activity.getPrerequisites().size();
    HashMap<ResourcePeripheralKey, SymbolicPosition> result = new HashMap<ResourcePeripheralKey, SymbolicPosition>(_size);
    EList<LocationPrerequisite> _prerequisites = activity.getPrerequisites();
    for (final LocationPrerequisite prerequisite : _prerequisites) {
      {
        final ResourcePeripheralKey key = ResourcePeripheralKey.createKey(prerequisite);
        boolean _containsKey = result.containsKey(key);
        if (_containsKey) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("Only one location prerequisite for peripheral ");
          String _fqn = key.fqn();
          _builder.append(_fqn);
          _builder.append(" is allowed.");
          this.error(_builder.toString(), prerequisite, 
            ActivityPackage.Literals.LOCATION_PREREQUISITE__PERIPHERAL, 
            ActivityValidator.MORE_THAN_ONE_LOCATION_PREREQ_FOR_PERIPHERAL);
        }
        result.put(key, prerequisite.getPosition());
      }
    }
    return result;
  }
  
  /**
   * Multiple validations for Move cramped into this method:
   * 1) Check for location prerequisite for peripheral for move.
   * 2) Check that source and target position are different.
   * 3) Check that the path for a move is complete.
   * 4) Check if settling is already defined for a path.
   */
  @Check
  public void checkMoveHasLocationPrerequisiteForPeripheral(final Move move) {
    boolean _isPositionMove = move.isPositionMove();
    boolean _not = (!_isPositionMove);
    if (_not) {
      return;
    }
    final Activity moveActivity = EcoreUtil2.<Activity>getContainerOfType(move, Activity.class);
    HashMap<ResourcePeripheralKey, SymbolicPosition> prerequisites = this.indexPrerequisites(moveActivity);
    final Move predecessorMove = move.getPredecessorMove();
    final ResourcePeripheralKey key = ResourcePeripheralKey.createKey(move);
    if (((null == predecessorMove) && (!prerequisites.containsKey(key)))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("This movement requires a location prerequisite for peripheral ");
      String _fqn = ActivityValidator.fqn(move.getPeripheral());
      _builder.append(_fqn);
      _builder.append(".");
      this.error(_builder.toString(), 
        ActivityPackage.Literals.PERIPHERAL_ACTION__PERIPHERAL, ActivityValidator.NO_LOC_PREREQUISTIE_FOR_PERIPHERAL_FOR_MOVE);
      return;
    }
    SymbolicPosition _xifexpression = null;
    if ((null == predecessorMove)) {
      _xifexpression = prerequisites.get(key);
    } else {
      _xifexpression = predecessorMove.getTargetPosition();
    }
    SymbolicPosition sourcePosition = _xifexpression;
    SymbolicPosition targetPosition = move.getTargetPosition();
    boolean _equals = Objects.equal(sourcePosition, targetPosition);
    if (_equals) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Peripheral ");
      String _fqn_1 = move.fqn();
      _builder_1.append(_fqn_1);
      _builder_1.append(" already at ");
      String _name = sourcePosition.getName();
      _builder_1.append(_name);
      _builder_1.append(".");
      this.warning(_builder_1.toString(), move, 
        ActivityPackage.Literals.PERIPHERAL_ACTION__PERIPHERAL, ActivityValidator.SAME_SOURCE_TARGET_FOR_PERIPHERAL);
    }
    PathTargetReference pathTargetRef = MachineQueries.findPath(sourcePosition, targetPosition, move.getProfile());
    if ((null == pathTargetRef)) {
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("Don\'t know how to move peripheral ");
      String _fqn_2 = ActivityValidator.fqn(move.getPeripheral());
      _builder_2.append(_fqn_2);
      _builder_2.append(" from ");
      String _name_1 = sourcePosition.getName();
      _builder_2.append(_name_1);
      _builder_2.append(" to ");
      String _name_2 = targetPosition.getName();
      _builder_2.append(_name_2);
      _builder_2.append(" using speed profile ");
      String _name_3 = move.getProfile().getName();
      _builder_2.append(_name_3);
      _builder_2.append(".");
      this.error(_builder_2.toString(), move, ActivityPackage.Literals.MOVE__PROFILE, ActivityValidator.NO_PATH_FOUND_FOR_PROFILE);
    }
    boolean _isPassing = move.isPassing();
    if (_isPassing) {
      if (((null != pathTargetRef) && (!pathTargetRef.getSettling().isEmpty()))) {
        StringConcatenation _builder_3 = new StringConcatenation();
        String _name_4 = move.getName();
        _builder_3.append(_name_4);
        _builder_3.append(" cannot pass symbolic position ");
        String _name_5 = targetPosition.getName();
        _builder_3.append(_name_5);
        _builder_3.append(", as settling is defined for its path.");
        this.error(_builder_3.toString(), move, ActivityPackage.Literals.MOVE__STOP_AT_TARGET, ActivityValidator.SETTLING_ALREADY_DEFINED_FOR_PATH);
      }
    }
  }
  
  /**
   * Check if settling is already defined for a distance move.
   */
  @Check
  public void checkSettling(final Move move) {
    boolean _isContinuing = move.isContinuing();
    if (_isContinuing) {
      Distance _distance = move.getDistance();
      EList<Axis> _settling = null;
      if (_distance!=null) {
        _settling=_distance.getSettling();
      }
      final EList<Axis> settling = _settling;
      boolean _xifexpression = false;
      if ((settling == null)) {
        _xifexpression = false;
      } else {
        boolean _isEmpty = settling.isEmpty();
        _xifexpression = (!_isEmpty);
      }
      final boolean isSettling = _xifexpression;
      if (isSettling) {
        StringConcatenation _builder = new StringConcatenation();
        String _name = move.getName();
        _builder.append(_name);
        _builder.append(" cannot pass ");
        String _name_1 = move.getDistance().getName();
        _builder.append(_name_1);
        _builder.append(", as settling has been defined for axes : ");
        String _join = IterableExtensions.join(settling, ",");
        _builder.append(_join);
        this.error(_builder.toString(), move, ActivityPackage.Literals.MOVE__STOP_AT_TARGET, ActivityValidator.SETTLING_ALREADY_DEFINED_FOR_MOVE);
      }
    }
  }
  
  private static String fqn(final Peripheral peripheral) {
    String _xifexpression = null;
    Resource _resource = peripheral.getResource();
    boolean _tripleEquals = (_resource == null);
    if (_tripleEquals) {
      _xifexpression = "<<unknown>>";
    } else {
      String _name = peripheral.getResource().getName();
      String _plus = (_name + ".");
      String _name_1 = peripheral.getName();
      _xifexpression = (_plus + _name_1);
    }
    return _xifexpression;
  }
  
  public static String names(final Iterable<? extends Node> nodes) {
    final Iterator<? extends Node> iNodes = nodes.iterator();
    boolean _hasNext = iNodes.hasNext();
    boolean _not = (!_hasNext);
    if (_not) {
      return "";
    }
    Node _next = iNodes.next();
    String _name = null;
    if (_next!=null) {
      _name=_next.getName();
    }
    final StringBuilder result = new StringBuilder(_name);
    while (iNodes.hasNext()) {
      {
        Node _next_1 = iNodes.next();
        String _name_1 = null;
        if (_next_1!=null) {
          _name_1=_next_1.getName();
        }
        final String nextName = _name_1;
        String _xifexpression = null;
        boolean _hasNext_1 = iNodes.hasNext();
        if (_hasNext_1) {
          _xifexpression = ", ";
        } else {
          _xifexpression = " and ";
        }
        result.append(_xifexpression).append(nextName);
      }
    }
    return result.toString();
  }
  
  public static String id(final Node node) {
    return ActivityValidator.id(node, false);
  }
  
  public static String id(final Node node, final boolean capitalize) {
    if ((node == null)) {
      return null;
    }
    String _switchResult = null;
    final Node it = node;
    boolean _matched = false;
    if (it instanceof Claim) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("claim ");
      String _name = ((Claim)it).getName();
      _builder.append(_name);
      _switchResult = _builder.toString();
    }
    if (!_matched) {
      if (it instanceof Release) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("release ");
        String _name = ((Release)it).getName();
        _builder.append(_name);
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (it instanceof SimpleAction) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("action ");
        String _name = ((SimpleAction)it).getName();
        _builder.append(_name);
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (it instanceof Move) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        {
          boolean _isStopAtTarget = ((Move)it).isStopAtTarget();
          boolean _not = (!_isStopAtTarget);
          if (_not) {
            {
              boolean _isPositionMove = ((Move)it).isPositionMove();
              if (_isPositionMove) {
                _builder.append("passing ");
              } else {
                _builder.append("continuing ");
              }
            }
          }
        }
        _builder.append("move ");
        String _name = ((Move)it).getName();
        _builder.append(_name);
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (it instanceof SyncBar) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("sync-bar ");
        String _name = ((SyncBar)it).getName();
        _builder.append(_name);
        _switchResult = _builder.toString();
      }
    }
    String id = _switchResult;
    String _xifexpression = null;
    if (capitalize) {
      _xifexpression = StringExtensions.toFirstUpper(id);
    } else {
      _xifexpression = id;
    }
    return _xifexpression;
  }
}
