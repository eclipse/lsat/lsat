/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor;

import com.google.inject.Inject;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.xtext.validation.CancelableDiagnostician;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class ItemLabelDiagnostician extends CancelableDiagnostician {
  @Extension
  private final AdapterFactory adapterFactory = new ReflectiveItemProviderAdapterFactory();
  
  @Inject
  public ItemLabelDiagnostician(final EValidator.Registry registry) {
    super(registry);
  }
  
  @Override
  public String getObjectLabel(final EObject eObject) {
    final Adapter labelProvider = this.adapterFactory.adapt(eObject, IItemLabelProvider.class);
    String _xifexpression = null;
    if ((labelProvider instanceof IItemLabelProvider)) {
      _xifexpression = ((IItemLabelProvider)labelProvider).getText(eObject);
    } else {
      _xifexpression = super.getObjectLabel(eObject);
    }
    return _xifexpression;
  }
}
