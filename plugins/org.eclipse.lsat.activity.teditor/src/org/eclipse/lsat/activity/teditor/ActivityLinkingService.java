/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor;

import static java.lang.String.format;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.lsat.activity.teditor.services.ActivityGrammarAccess;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.xtext.linking.impl.DefaultLinkingService;
import org.eclipse.xtext.linking.impl.IllegalNodeException;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.lib.Extension;

import com.google.inject.Inject;

import activity.ActivityFactory;
import activity.SyncBar;

public class ActivityLinkingService extends DefaultLinkingService {
    @Inject
    @Extension
    private ActivityGrammarAccess activityGrammarAccess;

    @Override
    public List<EObject> getLinkedObjects(EObject context, EReference ref, INode inode) throws IllegalNodeException {
        List<EObject> result = super.getLinkedObjects(context, ref, inode);

        final EObject singleResult = (null == result || result.isEmpty()) ? null : result.get(0);
        if (inode.getGrammarElement()
                .equals(activityGrammarAccess.getSourceReferenceAccess().getNodeSyncBarCrossReference_1_1_1_0())
                || inode.getGrammarElement().equals(
                        activityGrammarAccess.getTargetReferenceAccess().getNodeSyncBarCrossReference_1_1_1_0()))
        {
            if (null == singleResult) {
                // Only sync bars can be implicitly declared
                EObject syncBar = createSyncBar((Edge)context.eContainer(), inode.getText().trim());
                result = Collections.singletonList(syncBar);
            } else if (!(singleResult instanceof SyncBar)) {
                throw new IllegalNodeException(inode, format(
                        "Cannot create sync bar, an action with name %s already exitsts", inode.getText().trim()));
            }
        } else if (inode.getGrammarElement()
                .equals(activityGrammarAccess.getSourceReferenceAccess().getNodeActionCrossReference_1_0_0())
                || inode.getGrammarElement()
                        .equals(activityGrammarAccess.getTargetReferenceAccess().getNodeActionCrossReference_1_0_0()))
        {
            if (singleResult instanceof SyncBar) {
                throw new IllegalNodeException(inode, format(
                        "Reference to sync bar %s should be preceded with the '|' sign", inode.getText().trim()));
            }
        }
        return result;
    }

    private EObject createSyncBar(Edge context, String name) {
        EditableDirectedGraph graph = context.getGraph();
        if (null == graph) {
            return null;
        }
        SyncBar syncBar = ActivityFactory.eINSTANCE.createSyncBar();
        syncBar.setName(name);
        graph.getNodes().add(syncBar);
        return syncBar;
    }
}
