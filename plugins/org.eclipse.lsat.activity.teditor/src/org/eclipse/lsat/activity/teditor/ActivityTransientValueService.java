/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;
import org.eclipse.xtext.parsetree.reconstr.impl.DefaultTransientValueService;

import activity.SyncBar;

/**
 * This class prevents sync bars to be explicitly stored in the syntax
 */
public class ActivityTransientValueService extends DefaultTransientValueService {
    @Override
    public boolean isCheckElementsIndividually(EObject owner, EStructuralFeature feature) {
        if (EdgPackage.Literals.EDITABLE_DIRECTED_GRAPH__NODES == feature) {
            return true;
        }
        return super.isCheckElementsIndividually(owner, feature);
    }

    @Override
    public boolean isTransient(EObject owner, EStructuralFeature feature, int index) {
        boolean result = super.isTransient(owner, feature, index);
        if (!result && EdgPackage.Literals.EDITABLE_DIRECTED_GRAPH__NODES == feature) {
            result = ((List<?>)owner.eGet(feature)).get(index) instanceof SyncBar;
        }
        return result;
    }
}
