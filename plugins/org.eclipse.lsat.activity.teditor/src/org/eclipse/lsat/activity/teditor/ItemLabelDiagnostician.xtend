/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor

import org.eclipse.emf.common.notify.AdapterFactory
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory
import org.eclipse.xtext.validation.CancelableDiagnostician
import org.eclipse.emf.edit.provider.IItemLabelProvider
import org.eclipse.emf.ecore.EValidator.Registry
import com.google.inject.Inject

class ItemLabelDiagnostician extends CancelableDiagnostician {

    extension val AdapterFactory adapterFactory = new ReflectiveItemProviderAdapterFactory

    @Inject
    new(Registry registry) {
        super(registry)
    }

    override getObjectLabel(EObject eObject) {
        val labelProvider = eObject.adapt(IItemLabelProvider)
        return if (labelProvider instanceof IItemLabelProvider) {
            labelProvider.getText(eObject);
        } else {
            super.getObjectLabel(eObject)
        }
    }

}
