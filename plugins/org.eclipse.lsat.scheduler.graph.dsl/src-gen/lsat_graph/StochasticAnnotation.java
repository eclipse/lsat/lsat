/**
 */
package lsat_graph;

import org.eclipse.lsat.common.graph.directed.Aspect;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stochastic Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.StochasticAnnotation#getWeight <em>Weight</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getStochasticAnnotation()
 * @model
 * @generated
 */
public interface StochasticAnnotation extends Aspect<ScheduledTask<Task>, ScheduledDependency> {
	/**
     * Returns the value of the '<em><b>Weight</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Weight</em>' attribute.
     * @see #setWeight(BigDecimal)
     * @see lsat_graph.lsat_graphPackage#getStochasticAnnotation_Weight()
     * @model required="true"
     * @generated
     */
	BigDecimal getWeight();

	/**
     * Sets the value of the '{@link lsat_graph.StochasticAnnotation#getWeight <em>Weight</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Weight</em>' attribute.
     * @see #getWeight()
     * @generated
     */
	void setWeight(BigDecimal value);

} // StochasticAnnotation
