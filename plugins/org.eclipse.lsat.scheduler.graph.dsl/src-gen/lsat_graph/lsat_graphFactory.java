/**
 */
package lsat_graph;

import org.eclipse.emf.ecore.EFactory;
import org.eclipse.lsat.common.scheduler.graph.Task;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see lsat_graph.lsat_graphPackage
 * @generated
 */
public interface lsat_graphFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	lsat_graphFactory eINSTANCE = lsat_graph.impl.lsat_graphFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Peripheral Action Task</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Peripheral Action Task</em>'.
     * @generated
     */
	PeripheralActionTask createPeripheralActionTask();

	/**
     * Returns a new object of class '<em>Claim Task</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Claim Task</em>'.
     * @generated
     */
	ClaimTask createClaimTask();

	/**
     * Returns a new object of class '<em>Release Task</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Release Task</em>'.
     * @generated
     */
	ReleaseTask createReleaseTask();

	/**
     * Returns a new object of class '<em>Claim Release Resource</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Claim Release Resource</em>'.
     * @generated
     */
	ClaimReleaseResource createClaimReleaseResource();

	/**
     * Returns a new object of class '<em>Dispatch Group Task</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Dispatch Group Task</em>'.
     * @generated
     */
	DispatchGroupTask createDispatchGroupTask();

	/**
     * Returns a new object of class '<em>Dispatch Group Resource</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Dispatch Group Resource</em>'.
     * @generated
     */
	DispatchGroupResource createDispatchGroupResource();

	/**
     * Returns a new object of class '<em>Peripheral Resource</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Peripheral Resource</em>'.
     * @generated
     */
	PeripheralResource createPeripheralResource();

	/**
     * Returns a new object of class '<em>Dispatch Graph</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Dispatch Graph</em>'.
     * @generated
     */
	DispatchGraph createDispatchGraph();

	/**
     * Returns a new object of class '<em>Claimed By Scheduled Task</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Claimed By Scheduled Task</em>'.
     * @generated
     */
	ClaimedByScheduledTask createClaimedByScheduledTask();

	/**
     * Returns a new object of class '<em>Stochastic Annotation</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Stochastic Annotation</em>'.
     * @generated
     */
	StochasticAnnotation createStochasticAnnotation();

	/**
     * Returns a new object of class '<em>Event Status Task</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Event Status Task</em>'.
     * @generated
     */
	EventStatusTask createEventStatusTask();

	/**
     * Returns a new object of class '<em>Event Annotation</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Event Annotation</em>'.
     * @generated
     */
    <T extends Task> EventAnnotation<T> createEventAnnotation();

    /**
     * Returns a new object of class '<em>Require Task</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Require Task</em>'.
     * @generated
     */
    RequireTask createRequireTask();

    /**
     * Returns a new object of class '<em>Raise Task</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Raise Task</em>'.
     * @generated
     */
    RaiseTask createRaiseTask();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	lsat_graphPackage getlsat_graphPackage();

} //lsat_graphFactory
