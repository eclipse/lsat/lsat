/**
 */
package lsat_graph;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;

import org.eclipse.lsat.common.scheduler.graph.GraphPackage;

import org.eclipse.lsat.common.scheduler.resources.ResourcesPackage;

import org.eclipse.lsat.common.scheduler.schedule.SchedulePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see lsat_graph.lsat_graphFactory
 * @model kind="package"
 * @generated
 */
public interface lsat_graphPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "lsat_graph";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.eclipse.org/lsat/lsat_graph";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "lsat_graph";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	lsat_graphPackage eINSTANCE = lsat_graph.impl.lsat_graphPackageImpl.init();

	/**
     * The meta object id for the '{@link lsat_graph.impl.ActionTaskImpl <em>Action Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.ActionTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getActionTask()
     * @generated
     */
	int ACTION_TASK = 9;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__NAME = GraphPackage.TASK__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__OUTGOING_EDGES = GraphPackage.TASK__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__INCOMING_EDGES = GraphPackage.TASK__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__GRAPH = GraphPackage.TASK__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__ASPECTS = GraphPackage.TASK__ASPECTS;

	/**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__EXECUTION_TIME = GraphPackage.TASK__EXECUTION_TIME;

	/**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__RESOURCES = GraphPackage.TASK__RESOURCES;

	/**
     * The feature id for the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK__ACTION = GraphPackage.TASK_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Action Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TASK_FEATURE_COUNT = GraphPackage.TASK_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link lsat_graph.impl.PeripheralActionTaskImpl <em>Peripheral Action Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.PeripheralActionTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getPeripheralActionTask()
     * @generated
     */
	int PERIPHERAL_ACTION_TASK = 0;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__NAME = ACTION_TASK__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__OUTGOING_EDGES = ACTION_TASK__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__INCOMING_EDGES = ACTION_TASK__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__GRAPH = ACTION_TASK__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__ASPECTS = ACTION_TASK__ASPECTS;

	/**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__EXECUTION_TIME = ACTION_TASK__EXECUTION_TIME;

	/**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__RESOURCES = ACTION_TASK__RESOURCES;

	/**
     * The feature id for the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK__ACTION = ACTION_TASK__ACTION;

	/**
     * The number of structural features of the '<em>Peripheral Action Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_ACTION_TASK_FEATURE_COUNT = ACTION_TASK_FEATURE_COUNT + 0;

	/**
     * The meta object id for the '{@link lsat_graph.impl.ClaimTaskImpl <em>Claim Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.ClaimTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getClaimTask()
     * @generated
     */
	int CLAIM_TASK = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__NAME = ACTION_TASK__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__OUTGOING_EDGES = ACTION_TASK__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__INCOMING_EDGES = ACTION_TASK__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__GRAPH = ACTION_TASK__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__ASPECTS = ACTION_TASK__ASPECTS;

	/**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__EXECUTION_TIME = ACTION_TASK__EXECUTION_TIME;

	/**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__RESOURCES = ACTION_TASK__RESOURCES;

	/**
     * The feature id for the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK__ACTION = ACTION_TASK__ACTION;

	/**
     * The number of structural features of the '<em>Claim Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_TASK_FEATURE_COUNT = ACTION_TASK_FEATURE_COUNT + 0;

	/**
     * The meta object id for the '{@link lsat_graph.impl.ReleaseTaskImpl <em>Release Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.ReleaseTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getReleaseTask()
     * @generated
     */
	int RELEASE_TASK = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__NAME = ACTION_TASK__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__OUTGOING_EDGES = ACTION_TASK__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__INCOMING_EDGES = ACTION_TASK__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__GRAPH = ACTION_TASK__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__ASPECTS = ACTION_TASK__ASPECTS;

	/**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__EXECUTION_TIME = ACTION_TASK__EXECUTION_TIME;

	/**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__RESOURCES = ACTION_TASK__RESOURCES;

	/**
     * The feature id for the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK__ACTION = ACTION_TASK__ACTION;

	/**
     * The number of structural features of the '<em>Release Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RELEASE_TASK_FEATURE_COUNT = ACTION_TASK_FEATURE_COUNT + 0;

	/**
     * The meta object id for the '{@link lsat_graph.impl.ClaimReleaseResourceImpl <em>Claim Release Resource</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.ClaimReleaseResourceImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getClaimReleaseResource()
     * @generated
     */
	int CLAIM_RELEASE_RESOURCE = 3;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_RELEASE_RESOURCE__NAME = ResourcesPackage.RESOURCE__NAME;

	/**
     * The feature id for the '<em><b>Container</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_RELEASE_RESOURCE__CONTAINER = ResourcesPackage.RESOURCE__CONTAINER;

	/**
     * The feature id for the '<em><b>Start</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_RELEASE_RESOURCE__START = ResourcesPackage.RESOURCE__START;

	/**
     * The number of structural features of the '<em>Claim Release Resource</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIM_RELEASE_RESOURCE_FEATURE_COUNT = ResourcesPackage.RESOURCE_FEATURE_COUNT + 0;

	/**
     * The meta object id for the '{@link lsat_graph.impl.DispatchGroupTaskImpl <em>Dispatch Group Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.DispatchGroupTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getDispatchGroupTask()
     * @generated
     */
	int DISPATCH_GROUP_TASK = 4;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__NAME = GraphPackage.TASK__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__OUTGOING_EDGES = GraphPackage.TASK__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__INCOMING_EDGES = GraphPackage.TASK__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__GRAPH = GraphPackage.TASK__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__ASPECTS = GraphPackage.TASK__ASPECTS;

	/**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__EXECUTION_TIME = GraphPackage.TASK__EXECUTION_TIME;

	/**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__RESOURCES = GraphPackage.TASK__RESOURCES;

	/**
     * The feature id for the '<em><b>Dispatch Group</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK__DISPATCH_GROUP = GraphPackage.TASK_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Dispatch Group Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_TASK_FEATURE_COUNT = GraphPackage.TASK_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link lsat_graph.impl.DispatchGroupResourceImpl <em>Dispatch Group Resource</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.DispatchGroupResourceImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getDispatchGroupResource()
     * @generated
     */
	int DISPATCH_GROUP_RESOURCE = 5;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_RESOURCE__NAME = ResourcesPackage.RESOURCE__NAME;

	/**
     * The feature id for the '<em><b>Container</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_RESOURCE__CONTAINER = ResourcesPackage.RESOURCE__CONTAINER;

	/**
     * The feature id for the '<em><b>Start</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_RESOURCE__START = ResourcesPackage.RESOURCE__START;

	/**
     * The number of structural features of the '<em>Dispatch Group Resource</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GROUP_RESOURCE_FEATURE_COUNT = ResourcesPackage.RESOURCE_FEATURE_COUNT + 0;

	/**
     * The meta object id for the '{@link lsat_graph.impl.PeripheralResourceImpl <em>Peripheral Resource</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.PeripheralResourceImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getPeripheralResource()
     * @generated
     */
	int PERIPHERAL_RESOURCE = 6;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_RESOURCE__NAME = ResourcesPackage.RESOURCE__NAME;

	/**
     * The feature id for the '<em><b>Container</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_RESOURCE__CONTAINER = ResourcesPackage.RESOURCE__CONTAINER;

	/**
     * The feature id for the '<em><b>Start</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_RESOURCE__START = ResourcesPackage.RESOURCE__START;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_RESOURCE__PERIPHERAL = ResourcesPackage.RESOURCE_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Peripheral Resource</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_RESOURCE_FEATURE_COUNT = ResourcesPackage.RESOURCE_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link lsat_graph.impl.DispatchGraphImpl <em>Dispatch Graph</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.DispatchGraphImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getDispatchGraph()
     * @generated
     */
	int DISPATCH_GRAPH = 7;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__NAME = GraphPackage.TASK_DEPENDENCY_GRAPH__NAME;

	/**
     * The feature id for the '<em><b>Sub Graphs</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__SUB_GRAPHS = GraphPackage.TASK_DEPENDENCY_GRAPH__SUB_GRAPHS;

	/**
     * The feature id for the '<em><b>Parent Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__PARENT_GRAPH = GraphPackage.TASK_DEPENDENCY_GRAPH__PARENT_GRAPH;

	/**
     * The feature id for the '<em><b>Edges</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__EDGES = GraphPackage.TASK_DEPENDENCY_GRAPH__EDGES;

	/**
     * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__NODES = GraphPackage.TASK_DEPENDENCY_GRAPH__NODES;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__ASPECTS = GraphPackage.TASK_DEPENDENCY_GRAPH__ASPECTS;

	/**
     * The feature id for the '<em><b>Resource Model</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__RESOURCE_MODEL = GraphPackage.TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL;

	/**
     * The feature id for the '<em><b>Dispatch</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH__DISPATCH = GraphPackage.TASK_DEPENDENCY_GRAPH_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Dispatch Graph</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISPATCH_GRAPH_FEATURE_COUNT = GraphPackage.TASK_DEPENDENCY_GRAPH_FEATURE_COUNT + 1;

	/**
     * The meta object id for the '{@link lsat_graph.impl.ClaimedByScheduledTaskImpl <em>Claimed By Scheduled Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.ClaimedByScheduledTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getClaimedByScheduledTask()
     * @generated
     */
	int CLAIMED_BY_SCHEDULED_TASK = 8;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__NAME = SchedulePackage.SCHEDULED_TASK__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__OUTGOING_EDGES = SchedulePackage.SCHEDULED_TASK__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__INCOMING_EDGES = SchedulePackage.SCHEDULED_TASK__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__GRAPH = SchedulePackage.SCHEDULED_TASK__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__ASPECTS = SchedulePackage.SCHEDULED_TASK__ASPECTS;

	/**
     * The feature id for the '<em><b>Start Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__START_TIME = SchedulePackage.SCHEDULED_TASK__START_TIME;

	/**
     * The feature id for the '<em><b>End Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__END_TIME = SchedulePackage.SCHEDULED_TASK__END_TIME;

	/**
     * The feature id for the '<em><b>Task</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__TASK = SchedulePackage.SCHEDULED_TASK__TASK;

	/**
     * The feature id for the '<em><b>Sequence</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__SEQUENCE = SchedulePackage.SCHEDULED_TASK__SEQUENCE;

	/**
     * The feature id for the '<em><b>Duration</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK__DURATION = SchedulePackage.SCHEDULED_TASK__DURATION;

	/**
     * The feature id for the '<em><b>Releases</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CLAIMED_BY_SCHEDULED_TASK__RELEASES = SchedulePackage.SCHEDULED_TASK_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Claims</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int CLAIMED_BY_SCHEDULED_TASK__CLAIMS = SchedulePackage.SCHEDULED_TASK_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Claimed By Scheduled Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int CLAIMED_BY_SCHEDULED_TASK_FEATURE_COUNT = SchedulePackage.SCHEDULED_TASK_FEATURE_COUNT + 2;

	/**
     * The meta object id for the '{@link lsat_graph.impl.StochasticAnnotationImpl <em>Stochastic Annotation</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.StochasticAnnotationImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getStochasticAnnotation()
     * @generated
     */
	int STOCHASTIC_ANNOTATION = 10;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int STOCHASTIC_ANNOTATION__NAME = DirectedGraphPackage.ASPECT__NAME;

	/**
     * The feature id for the '<em><b>Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int STOCHASTIC_ANNOTATION__EDGES = DirectedGraphPackage.ASPECT__EDGES;

	/**
     * The feature id for the '<em><b>Nodes</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int STOCHASTIC_ANNOTATION__NODES = DirectedGraphPackage.ASPECT__NODES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int STOCHASTIC_ANNOTATION__GRAPH = DirectedGraphPackage.ASPECT__GRAPH;

	/**
     * The feature id for the '<em><b>Weight</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int STOCHASTIC_ANNOTATION__WEIGHT = DirectedGraphPackage.ASPECT_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Stochastic Annotation</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int STOCHASTIC_ANNOTATION_FEATURE_COUNT = DirectedGraphPackage.ASPECT_FEATURE_COUNT + 1;


	/**
     * The meta object id for the '{@link lsat_graph.impl.EventStatusTaskImpl <em>Event Status Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see lsat_graph.impl.EventStatusTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getEventStatusTask()
     * @generated
     */
	int EVENT_STATUS_TASK = 11;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__NAME = SchedulePackage.SCHEDULED_TASK__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__OUTGOING_EDGES = SchedulePackage.SCHEDULED_TASK__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__INCOMING_EDGES = SchedulePackage.SCHEDULED_TASK__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__GRAPH = SchedulePackage.SCHEDULED_TASK__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__ASPECTS = SchedulePackage.SCHEDULED_TASK__ASPECTS;

	/**
     * The feature id for the '<em><b>Start Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__START_TIME = SchedulePackage.SCHEDULED_TASK__START_TIME;

	/**
     * The feature id for the '<em><b>End Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__END_TIME = SchedulePackage.SCHEDULED_TASK__END_TIME;

	/**
     * The feature id for the '<em><b>Task</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__TASK = SchedulePackage.SCHEDULED_TASK__TASK;

	/**
     * The feature id for the '<em><b>Sequence</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__SEQUENCE = SchedulePackage.SCHEDULED_TASK__SEQUENCE;

	/**
     * The feature id for the '<em><b>Duration</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK__DURATION = SchedulePackage.SCHEDULED_TASK__DURATION;

	/**
     * The number of structural features of the '<em>Event Status Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int EVENT_STATUS_TASK_FEATURE_COUNT = SchedulePackage.SCHEDULED_TASK_FEATURE_COUNT + 0;


	/**
     * The meta object id for the '{@link lsat_graph.impl.EventAnnotationImpl <em>Event Annotation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see lsat_graph.impl.EventAnnotationImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getEventAnnotation()
     * @generated
     */
    int EVENT_ANNOTATION = 12;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ANNOTATION__NAME = DirectedGraphPackage.ASPECT__NAME;

    /**
     * The feature id for the '<em><b>Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ANNOTATION__EDGES = DirectedGraphPackage.ASPECT__EDGES;

    /**
     * The feature id for the '<em><b>Nodes</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ANNOTATION__NODES = DirectedGraphPackage.ASPECT__NODES;

    /**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ANNOTATION__GRAPH = DirectedGraphPackage.ASPECT__GRAPH;

    /**
     * The feature id for the '<em><b>Require Event</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ANNOTATION__REQUIRE_EVENT = DirectedGraphPackage.ASPECT_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Event Annotation</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_ANNOTATION_FEATURE_COUNT = DirectedGraphPackage.ASPECT_FEATURE_COUNT + 1;


    /**
     * The meta object id for the '{@link lsat_graph.impl.EventTaskImpl <em>Event Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see lsat_graph.impl.EventTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getEventTask()
     * @generated
     */
    int EVENT_TASK = 13;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__NAME = GraphPackage.TASK__NAME;

    /**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__OUTGOING_EDGES = GraphPackage.TASK__OUTGOING_EDGES;

    /**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__INCOMING_EDGES = GraphPackage.TASK__INCOMING_EDGES;

    /**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__GRAPH = GraphPackage.TASK__GRAPH;

    /**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__ASPECTS = GraphPackage.TASK__ASPECTS;

    /**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__EXECUTION_TIME = GraphPackage.TASK__EXECUTION_TIME;

    /**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__RESOURCES = GraphPackage.TASK__RESOURCES;

    /**
     * The feature id for the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK__ACTION = GraphPackage.TASK_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Event Task</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int EVENT_TASK_FEATURE_COUNT = GraphPackage.TASK_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link lsat_graph.impl.RequireTaskImpl <em>Require Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see lsat_graph.impl.RequireTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getRequireTask()
     * @generated
     */
    int REQUIRE_TASK = 14;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__NAME = EVENT_TASK__NAME;

    /**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__OUTGOING_EDGES = EVENT_TASK__OUTGOING_EDGES;

    /**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__INCOMING_EDGES = EVENT_TASK__INCOMING_EDGES;

    /**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__GRAPH = EVENT_TASK__GRAPH;

    /**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__ASPECTS = EVENT_TASK__ASPECTS;

    /**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__EXECUTION_TIME = EVENT_TASK__EXECUTION_TIME;

    /**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__RESOURCES = EVENT_TASK__RESOURCES;

    /**
     * The feature id for the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK__ACTION = EVENT_TASK__ACTION;

    /**
     * The number of structural features of the '<em>Require Task</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REQUIRE_TASK_FEATURE_COUNT = EVENT_TASK_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link lsat_graph.impl.RaiseTaskImpl <em>Raise Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see lsat_graph.impl.RaiseTaskImpl
     * @see lsat_graph.impl.lsat_graphPackageImpl#getRaiseTask()
     * @generated
     */
    int RAISE_TASK = 15;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__NAME = EVENT_TASK__NAME;

    /**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__OUTGOING_EDGES = EVENT_TASK__OUTGOING_EDGES;

    /**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__INCOMING_EDGES = EVENT_TASK__INCOMING_EDGES;

    /**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__GRAPH = EVENT_TASK__GRAPH;

    /**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__ASPECTS = EVENT_TASK__ASPECTS;

    /**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__EXECUTION_TIME = EVENT_TASK__EXECUTION_TIME;

    /**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__RESOURCES = EVENT_TASK__RESOURCES;

    /**
     * The feature id for the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK__ACTION = EVENT_TASK__ACTION;

    /**
     * The number of structural features of the '<em>Raise Task</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RAISE_TASK_FEATURE_COUNT = EVENT_TASK_FEATURE_COUNT + 0;


    /**
     * Returns the meta object for class '{@link lsat_graph.PeripheralActionTask <em>Peripheral Action Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Peripheral Action Task</em>'.
     * @see lsat_graph.PeripheralActionTask
     * @generated
     */
	EClass getPeripheralActionTask();

	/**
     * Returns the meta object for class '{@link lsat_graph.ClaimTask <em>Claim Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Claim Task</em>'.
     * @see lsat_graph.ClaimTask
     * @generated
     */
	EClass getClaimTask();

	/**
     * Returns the meta object for class '{@link lsat_graph.ReleaseTask <em>Release Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Release Task</em>'.
     * @see lsat_graph.ReleaseTask
     * @generated
     */
	EClass getReleaseTask();

	/**
     * Returns the meta object for class '{@link lsat_graph.ClaimReleaseResource <em>Claim Release Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Claim Release Resource</em>'.
     * @see lsat_graph.ClaimReleaseResource
     * @generated
     */
	EClass getClaimReleaseResource();

	/**
     * Returns the meta object for class '{@link lsat_graph.DispatchGroupTask <em>Dispatch Group Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Dispatch Group Task</em>'.
     * @see lsat_graph.DispatchGroupTask
     * @generated
     */
	EClass getDispatchGroupTask();

	/**
     * Returns the meta object for the reference '{@link lsat_graph.DispatchGroupTask#getDispatchGroup <em>Dispatch Group</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Dispatch Group</em>'.
     * @see lsat_graph.DispatchGroupTask#getDispatchGroup()
     * @see #getDispatchGroupTask()
     * @generated
     */
	EReference getDispatchGroupTask_DispatchGroup();

	/**
     * Returns the meta object for class '{@link lsat_graph.DispatchGroupResource <em>Dispatch Group Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Dispatch Group Resource</em>'.
     * @see lsat_graph.DispatchGroupResource
     * @generated
     */
	EClass getDispatchGroupResource();

	/**
     * Returns the meta object for class '{@link lsat_graph.PeripheralResource <em>Peripheral Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Peripheral Resource</em>'.
     * @see lsat_graph.PeripheralResource
     * @generated
     */
	EClass getPeripheralResource();

	/**
     * Returns the meta object for the reference '{@link lsat_graph.PeripheralResource#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Peripheral</em>'.
     * @see lsat_graph.PeripheralResource#getPeripheral()
     * @see #getPeripheralResource()
     * @generated
     */
	EReference getPeripheralResource_Peripheral();

	/**
     * Returns the meta object for class '{@link lsat_graph.DispatchGraph <em>Dispatch Graph</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Dispatch Graph</em>'.
     * @see lsat_graph.DispatchGraph
     * @generated
     */
	EClass getDispatchGraph();

	/**
     * Returns the meta object for the reference '{@link lsat_graph.DispatchGraph#getDispatch <em>Dispatch</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Dispatch</em>'.
     * @see lsat_graph.DispatchGraph#getDispatch()
     * @see #getDispatchGraph()
     * @generated
     */
	EReference getDispatchGraph_Dispatch();

	/**
     * Returns the meta object for class '{@link lsat_graph.ClaimedByScheduledTask <em>Claimed By Scheduled Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Claimed By Scheduled Task</em>'.
     * @see lsat_graph.ClaimedByScheduledTask
     * @generated
     */
	EClass getClaimedByScheduledTask();

	/**
     * Returns the meta object for the reference list '{@link lsat_graph.ClaimedByScheduledTask#getReleases <em>Releases</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Releases</em>'.
     * @see lsat_graph.ClaimedByScheduledTask#getReleases()
     * @see #getClaimedByScheduledTask()
     * @generated
     */
    EReference getClaimedByScheduledTask_Releases();

    /**
     * Returns the meta object for the reference list '{@link lsat_graph.ClaimedByScheduledTask#getClaims <em>Claims</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Claims</em>'.
     * @see lsat_graph.ClaimedByScheduledTask#getClaims()
     * @see #getClaimedByScheduledTask()
     * @generated
     */
    EReference getClaimedByScheduledTask_Claims();

    /**
     * Returns the meta object for class '{@link lsat_graph.ActionTask <em>Action Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Action Task</em>'.
     * @see lsat_graph.ActionTask
     * @generated
     */
	EClass getActionTask();

	/**
     * Returns the meta object for the reference '{@link lsat_graph.ActionTask#getAction <em>Action</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Action</em>'.
     * @see lsat_graph.ActionTask#getAction()
     * @see #getActionTask()
     * @generated
     */
	EReference getActionTask_Action();

	/**
     * Returns the meta object for class '{@link lsat_graph.StochasticAnnotation <em>Stochastic Annotation</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Stochastic Annotation</em>'.
     * @see lsat_graph.StochasticAnnotation
     * @generated
     */
	EClass getStochasticAnnotation();

	/**
     * Returns the meta object for the attribute '{@link lsat_graph.StochasticAnnotation#getWeight <em>Weight</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Weight</em>'.
     * @see lsat_graph.StochasticAnnotation#getWeight()
     * @see #getStochasticAnnotation()
     * @generated
     */
	EAttribute getStochasticAnnotation_Weight();

	/**
     * Returns the meta object for class '{@link lsat_graph.EventStatusTask <em>Event Status Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Event Status Task</em>'.
     * @see lsat_graph.EventStatusTask
     * @generated
     */
	EClass getEventStatusTask();

	/**
     * Returns the meta object for class '{@link lsat_graph.EventAnnotation <em>Event Annotation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Event Annotation</em>'.
     * @see lsat_graph.EventAnnotation
     * @generated
     */
    EClass getEventAnnotation();

    /**
     * Returns the meta object for the attribute '{@link lsat_graph.EventAnnotation#isRequireEvent <em>Require Event</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Require Event</em>'.
     * @see lsat_graph.EventAnnotation#isRequireEvent()
     * @see #getEventAnnotation()
     * @generated
     */
    EAttribute getEventAnnotation_RequireEvent();

    /**
     * Returns the meta object for class '{@link lsat_graph.EventTask <em>Event Task</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Event Task</em>'.
     * @see lsat_graph.EventTask
     * @generated
     */
    EClass getEventTask();

    /**
     * Returns the meta object for the reference '{@link lsat_graph.EventTask#getAction <em>Action</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Action</em>'.
     * @see lsat_graph.EventTask#getAction()
     * @see #getEventTask()
     * @generated
     */
    EReference getEventTask_Action();

    /**
     * Returns the meta object for class '{@link lsat_graph.RequireTask <em>Require Task</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Require Task</em>'.
     * @see lsat_graph.RequireTask
     * @generated
     */
    EClass getRequireTask();

    /**
     * Returns the meta object for class '{@link lsat_graph.RaiseTask <em>Raise Task</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Raise Task</em>'.
     * @see lsat_graph.RaiseTask
     * @generated
     */
    EClass getRaiseTask();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	lsat_graphFactory getlsat_graphFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link lsat_graph.impl.PeripheralActionTaskImpl <em>Peripheral Action Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.PeripheralActionTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getPeripheralActionTask()
         * @generated
         */
		EClass PERIPHERAL_ACTION_TASK = eINSTANCE.getPeripheralActionTask();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.ClaimTaskImpl <em>Claim Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.ClaimTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getClaimTask()
         * @generated
         */
		EClass CLAIM_TASK = eINSTANCE.getClaimTask();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.ReleaseTaskImpl <em>Release Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.ReleaseTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getReleaseTask()
         * @generated
         */
		EClass RELEASE_TASK = eINSTANCE.getReleaseTask();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.ClaimReleaseResourceImpl <em>Claim Release Resource</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.ClaimReleaseResourceImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getClaimReleaseResource()
         * @generated
         */
		EClass CLAIM_RELEASE_RESOURCE = eINSTANCE.getClaimReleaseResource();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.DispatchGroupTaskImpl <em>Dispatch Group Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.DispatchGroupTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getDispatchGroupTask()
         * @generated
         */
		EClass DISPATCH_GROUP_TASK = eINSTANCE.getDispatchGroupTask();

		/**
         * The meta object literal for the '<em><b>Dispatch Group</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DISPATCH_GROUP_TASK__DISPATCH_GROUP = eINSTANCE.getDispatchGroupTask_DispatchGroup();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.DispatchGroupResourceImpl <em>Dispatch Group Resource</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.DispatchGroupResourceImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getDispatchGroupResource()
         * @generated
         */
		EClass DISPATCH_GROUP_RESOURCE = eINSTANCE.getDispatchGroupResource();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.PeripheralResourceImpl <em>Peripheral Resource</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.PeripheralResourceImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getPeripheralResource()
         * @generated
         */
		EClass PERIPHERAL_RESOURCE = eINSTANCE.getPeripheralResource();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL_RESOURCE__PERIPHERAL = eINSTANCE.getPeripheralResource_Peripheral();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.DispatchGraphImpl <em>Dispatch Graph</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.DispatchGraphImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getDispatchGraph()
         * @generated
         */
		EClass DISPATCH_GRAPH = eINSTANCE.getDispatchGraph();

		/**
         * The meta object literal for the '<em><b>Dispatch</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DISPATCH_GRAPH__DISPATCH = eINSTANCE.getDispatchGraph_Dispatch();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.ClaimedByScheduledTaskImpl <em>Claimed By Scheduled Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.ClaimedByScheduledTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getClaimedByScheduledTask()
         * @generated
         */
		EClass CLAIMED_BY_SCHEDULED_TASK = eINSTANCE.getClaimedByScheduledTask();

		/**
         * The meta object literal for the '<em><b>Releases</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CLAIMED_BY_SCHEDULED_TASK__RELEASES = eINSTANCE.getClaimedByScheduledTask_Releases();

        /**
         * The meta object literal for the '<em><b>Claims</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference CLAIMED_BY_SCHEDULED_TASK__CLAIMS = eINSTANCE.getClaimedByScheduledTask_Claims();

        /**
         * The meta object literal for the '{@link lsat_graph.impl.ActionTaskImpl <em>Action Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.ActionTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getActionTask()
         * @generated
         */
		EClass ACTION_TASK = eINSTANCE.getActionTask();

		/**
         * The meta object literal for the '<em><b>Action</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference ACTION_TASK__ACTION = eINSTANCE.getActionTask_Action();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.StochasticAnnotationImpl <em>Stochastic Annotation</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.StochasticAnnotationImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getStochasticAnnotation()
         * @generated
         */
		EClass STOCHASTIC_ANNOTATION = eINSTANCE.getStochasticAnnotation();

		/**
         * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute STOCHASTIC_ANNOTATION__WEIGHT = eINSTANCE.getStochasticAnnotation_Weight();

		/**
         * The meta object literal for the '{@link lsat_graph.impl.EventStatusTaskImpl <em>Event Status Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see lsat_graph.impl.EventStatusTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getEventStatusTask()
         * @generated
         */
		EClass EVENT_STATUS_TASK = eINSTANCE.getEventStatusTask();

        /**
         * The meta object literal for the '{@link lsat_graph.impl.EventAnnotationImpl <em>Event Annotation</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see lsat_graph.impl.EventAnnotationImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getEventAnnotation()
         * @generated
         */
        EClass EVENT_ANNOTATION = eINSTANCE.getEventAnnotation();

        /**
         * The meta object literal for the '<em><b>Require Event</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute EVENT_ANNOTATION__REQUIRE_EVENT = eINSTANCE.getEventAnnotation_RequireEvent();

        /**
         * The meta object literal for the '{@link lsat_graph.impl.EventTaskImpl <em>Event Task</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see lsat_graph.impl.EventTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getEventTask()
         * @generated
         */
        EClass EVENT_TASK = eINSTANCE.getEventTask();

        /**
         * The meta object literal for the '<em><b>Action</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference EVENT_TASK__ACTION = eINSTANCE.getEventTask_Action();

        /**
         * The meta object literal for the '{@link lsat_graph.impl.RequireTaskImpl <em>Require Task</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see lsat_graph.impl.RequireTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getRequireTask()
         * @generated
         */
        EClass REQUIRE_TASK = eINSTANCE.getRequireTask();

        /**
         * The meta object literal for the '{@link lsat_graph.impl.RaiseTaskImpl <em>Raise Task</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see lsat_graph.impl.RaiseTaskImpl
         * @see lsat_graph.impl.lsat_graphPackageImpl#getRaiseTask()
         * @generated
         */
        EClass RAISE_TASK = eINSTANCE.getRaiseTask();

	}

} //lsat_graphPackage
