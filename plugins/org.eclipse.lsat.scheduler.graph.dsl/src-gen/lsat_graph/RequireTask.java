/**
 */
package lsat_graph;

import activity.RequireEvent;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Require Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getRequireTask()
 * @model
 * @generated
 */
public interface RequireTask extends EventTask<RequireEvent>
{
} // RequireTask
