/**
 */
package lsat_graph;

import activity.Release;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Release Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getReleaseTask()
 * @model
 * @generated
 */
public interface ReleaseTask extends ActionTask<Release> {
} // ReleaseTask
