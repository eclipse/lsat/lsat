/**
 */
package lsat_graph;

import org.eclipse.lsat.common.graph.directed.Aspect;

import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.Task;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EventAction Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.EventAnnotation#isRequireEvent <em>Require Event</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getEventAnnotation()
 * @model
 * @generated
 */
public interface EventAnnotation<T extends Task> extends Aspect<T, Dependency>
{
    /**
     * Returns the value of the '<em><b>Require Event</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Require Event</em>' attribute.
     * @see #setRequireEvent(boolean)
     * @see lsat_graph.lsat_graphPackage#getEventAnnotation_RequireEvent()
     * @model required="true"
     * @generated
     */
    boolean isRequireEvent();

    /**
     * Sets the value of the '{@link lsat_graph.EventAnnotation#isRequireEvent <em>Require Event</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Require Event</em>' attribute.
     * @see #isRequireEvent()
     * @generated
     */
    void setRequireEvent(boolean value);

} // EventAnnotation
