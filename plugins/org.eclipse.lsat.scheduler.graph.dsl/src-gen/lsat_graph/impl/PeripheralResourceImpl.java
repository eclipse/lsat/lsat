/**
 */
package lsat_graph.impl;

import org.eclipse.lsat.common.scheduler.resources.impl.ResourceImpl;

import machine.Peripheral;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import lsat_graph.PeripheralResource;
import lsat_graph.lsat_graphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Peripheral Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.impl.PeripheralResourceImpl#getPeripheral <em>Peripheral</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PeripheralResourceImpl extends ResourceImpl implements PeripheralResource {
	/**
     * The cached value of the '{@link #getPeripheral() <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPeripheral()
     * @generated
     * @ordered
     */
	protected Peripheral peripheral;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PeripheralResourceImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.PERIPHERAL_RESOURCE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Peripheral getPeripheral() {
        if (peripheral != null && peripheral.eIsProxy())
        {
            InternalEObject oldPeripheral = (InternalEObject)peripheral;
            peripheral = (Peripheral)eResolveProxy(oldPeripheral);
            if (peripheral != oldPeripheral)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, lsat_graphPackage.PERIPHERAL_RESOURCE__PERIPHERAL, oldPeripheral, peripheral));
            }
        }
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Peripheral basicGetPeripheral() {
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPeripheral(Peripheral newPeripheral) {
        Peripheral oldPeripheral = peripheral;
        peripheral = newPeripheral;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, lsat_graphPackage.PERIPHERAL_RESOURCE__PERIPHERAL, oldPeripheral, peripheral));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case lsat_graphPackage.PERIPHERAL_RESOURCE__PERIPHERAL:
                if (resolve) return getPeripheral();
                return basicGetPeripheral();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case lsat_graphPackage.PERIPHERAL_RESOURCE__PERIPHERAL:
                setPeripheral((Peripheral)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.PERIPHERAL_RESOURCE__PERIPHERAL:
                setPeripheral((Peripheral)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case lsat_graphPackage.PERIPHERAL_RESOURCE__PERIPHERAL:
                return peripheral != null;
        }
        return super.eIsSet(featureID);
    }

} //PeripheralResourceImpl
