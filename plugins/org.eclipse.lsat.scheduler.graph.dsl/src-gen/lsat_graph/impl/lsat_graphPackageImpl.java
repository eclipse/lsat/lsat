/**
 */
package lsat_graph.impl;

import activity.ActivityPackage;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;

import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;
import org.eclipse.lsat.common.scheduler.graph.GraphPackage;

import org.eclipse.lsat.common.scheduler.resources.ResourcesPackage;

import org.eclipse.lsat.common.scheduler.schedule.SchedulePackage;

import dispatching.DispatchingPackage;
import lsat_graph.ActionTask;
import lsat_graph.ClaimReleaseResource;
import lsat_graph.ClaimTask;
import lsat_graph.ClaimedByScheduledTask;
import lsat_graph.DispatchGraph;
import lsat_graph.DispatchGroupResource;
import lsat_graph.DispatchGroupTask;
import lsat_graph.EventAnnotation;
import lsat_graph.EventStatusTask;
import lsat_graph.EventTask;
import lsat_graph.PeripheralActionTask;
import lsat_graph.PeripheralResource;
import lsat_graph.RaiseTask;
import lsat_graph.ReleaseTask;
import lsat_graph.RequireTask;
import lsat_graph.StochasticAnnotation;
import lsat_graph.lsat_graphFactory;
import lsat_graph.lsat_graphPackage;
import machine.MachinePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class lsat_graphPackageImpl extends EPackageImpl implements lsat_graphPackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass peripheralActionTaskEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass claimTaskEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass releaseTaskEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass claimReleaseResourceEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass dispatchGroupTaskEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass dispatchGroupResourceEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass peripheralResourceEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass dispatchGraphEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass claimedByScheduledTaskEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass actionTaskEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass stochasticAnnotationEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass eventStatusTaskEClass = null;

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass eventAnnotationEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass eventTaskEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass requireTaskEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass raiseTaskEClass = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see lsat_graph.lsat_graphPackage#eNS_URI
     * @see #init()
     * @generated
     */
	private lsat_graphPackageImpl() {
        super(eNS_URI, lsat_graphFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link lsat_graphPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static lsat_graphPackage init() {
        if (isInited) return (lsat_graphPackage)EPackage.Registry.INSTANCE.getEPackage(lsat_graphPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredlsat_graphPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        lsat_graphPackageImpl thelsat_graphPackage = registeredlsat_graphPackage instanceof lsat_graphPackageImpl ? (lsat_graphPackageImpl)registeredlsat_graphPackage : new lsat_graphPackageImpl();

        isInited = true;

        // Initialize simple dependencies
        ActivityPackage.eINSTANCE.eClass();
        DirectedGraphPackage.eINSTANCE.eClass();
        DispatchingPackage.eINSTANCE.eClass();
        EdgPackage.eINSTANCE.eClass();
        GraphPackage.eINSTANCE.eClass();
        MachinePackage.eINSTANCE.eClass();
        ResourcesPackage.eINSTANCE.eClass();
        SchedulePackage.eINSTANCE.eClass();

        // Create package meta-data objects
        thelsat_graphPackage.createPackageContents();

        // Initialize created meta-data
        thelsat_graphPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        thelsat_graphPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(lsat_graphPackage.eNS_URI, thelsat_graphPackage);
        return thelsat_graphPackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPeripheralActionTask() {
        return peripheralActionTaskEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getClaimTask() {
        return claimTaskEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getReleaseTask() {
        return releaseTaskEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getClaimReleaseResource() {
        return claimReleaseResourceEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDispatchGroupTask() {
        return dispatchGroupTaskEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDispatchGroupTask_DispatchGroup() {
        return (EReference)dispatchGroupTaskEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDispatchGroupResource() {
        return dispatchGroupResourceEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPeripheralResource() {
        return peripheralResourceEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheralResource_Peripheral() {
        return (EReference)peripheralResourceEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDispatchGraph() {
        return dispatchGraphEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDispatchGraph_Dispatch() {
        return (EReference)dispatchGraphEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getClaimedByScheduledTask() {
        return claimedByScheduledTaskEClass;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getClaimedByScheduledTask_Releases()
    {
        return (EReference)claimedByScheduledTaskEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getClaimedByScheduledTask_Claims()
    {
        return (EReference)claimedByScheduledTaskEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getActionTask() {
        return actionTaskEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getActionTask_Action() {
        return (EReference)actionTaskEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getStochasticAnnotation() {
        return stochasticAnnotationEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getStochasticAnnotation_Weight() {
        return (EAttribute)stochasticAnnotationEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getEventStatusTask() {
        return eventStatusTaskEClass;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEventAnnotation()
    {
        return eventAnnotationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEventAnnotation_RequireEvent()
    {
        return (EAttribute)eventAnnotationEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEventTask()
    {
        return eventTaskEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEventTask_Action()
    {
        return (EReference)eventTaskEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRequireTask()
    {
        return requireTaskEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRaiseTask()
    {
        return raiseTaskEClass;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public lsat_graphFactory getlsat_graphFactory() {
        return (lsat_graphFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        peripheralActionTaskEClass = createEClass(PERIPHERAL_ACTION_TASK);

        claimTaskEClass = createEClass(CLAIM_TASK);

        releaseTaskEClass = createEClass(RELEASE_TASK);

        claimReleaseResourceEClass = createEClass(CLAIM_RELEASE_RESOURCE);

        dispatchGroupTaskEClass = createEClass(DISPATCH_GROUP_TASK);
        createEReference(dispatchGroupTaskEClass, DISPATCH_GROUP_TASK__DISPATCH_GROUP);

        dispatchGroupResourceEClass = createEClass(DISPATCH_GROUP_RESOURCE);

        peripheralResourceEClass = createEClass(PERIPHERAL_RESOURCE);
        createEReference(peripheralResourceEClass, PERIPHERAL_RESOURCE__PERIPHERAL);

        dispatchGraphEClass = createEClass(DISPATCH_GRAPH);
        createEReference(dispatchGraphEClass, DISPATCH_GRAPH__DISPATCH);

        claimedByScheduledTaskEClass = createEClass(CLAIMED_BY_SCHEDULED_TASK);
        createEReference(claimedByScheduledTaskEClass, CLAIMED_BY_SCHEDULED_TASK__RELEASES);
        createEReference(claimedByScheduledTaskEClass, CLAIMED_BY_SCHEDULED_TASK__CLAIMS);

        actionTaskEClass = createEClass(ACTION_TASK);
        createEReference(actionTaskEClass, ACTION_TASK__ACTION);

        stochasticAnnotationEClass = createEClass(STOCHASTIC_ANNOTATION);
        createEAttribute(stochasticAnnotationEClass, STOCHASTIC_ANNOTATION__WEIGHT);

        eventStatusTaskEClass = createEClass(EVENT_STATUS_TASK);

        eventAnnotationEClass = createEClass(EVENT_ANNOTATION);
        createEAttribute(eventAnnotationEClass, EVENT_ANNOTATION__REQUIRE_EVENT);

        eventTaskEClass = createEClass(EVENT_TASK);
        createEReference(eventTaskEClass, EVENT_TASK__ACTION);

        requireTaskEClass = createEClass(REQUIRE_TASK);

        raiseTaskEClass = createEClass(RAISE_TASK);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        ActivityPackage theActivityPackage = (ActivityPackage)EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI);
        ResourcesPackage theResourcesPackage = (ResourcesPackage)EPackage.Registry.INSTANCE.getEPackage(ResourcesPackage.eNS_URI);
        GraphPackage theGraphPackage = (GraphPackage)EPackage.Registry.INSTANCE.getEPackage(GraphPackage.eNS_URI);
        DispatchingPackage theDispatchingPackage = (DispatchingPackage)EPackage.Registry.INSTANCE.getEPackage(DispatchingPackage.eNS_URI);
        MachinePackage theMachinePackage = (MachinePackage)EPackage.Registry.INSTANCE.getEPackage(MachinePackage.eNS_URI);
        SchedulePackage theSchedulePackage = (SchedulePackage)EPackage.Registry.INSTANCE.getEPackage(SchedulePackage.eNS_URI);
        DirectedGraphPackage theDirectedGraphPackage = (DirectedGraphPackage)EPackage.Registry.INSTANCE.getEPackage(DirectedGraphPackage.eNS_URI);

        // Create type parameters
        ETypeParameter actionTaskEClass_T = addETypeParameter(actionTaskEClass, "T");
        ETypeParameter eventAnnotationEClass_T = addETypeParameter(eventAnnotationEClass, "T");
        ETypeParameter eventTaskEClass_T = addETypeParameter(eventTaskEClass, "T");

        // Set bounds for type parameters
        EGenericType g1 = createEGenericType(theActivityPackage.getResourceAction());
        actionTaskEClass_T.getEBounds().add(g1);
        g1 = createEGenericType(theGraphPackage.getTask());
        eventAnnotationEClass_T.getEBounds().add(g1);
        g1 = createEGenericType(theActivityPackage.getEventAction());
        eventTaskEClass_T.getEBounds().add(g1);

        // Add supertypes to classes
        g1 = createEGenericType(this.getActionTask());
        EGenericType g2 = createEGenericType(theActivityPackage.getPeripheralAction());
        g1.getETypeArguments().add(g2);
        peripheralActionTaskEClass.getEGenericSuperTypes().add(g1);
        g1 = createEGenericType(this.getActionTask());
        g2 = createEGenericType(theActivityPackage.getClaim());
        g1.getETypeArguments().add(g2);
        claimTaskEClass.getEGenericSuperTypes().add(g1);
        g1 = createEGenericType(this.getActionTask());
        g2 = createEGenericType(theActivityPackage.getRelease());
        g1.getETypeArguments().add(g2);
        releaseTaskEClass.getEGenericSuperTypes().add(g1);
        claimReleaseResourceEClass.getESuperTypes().add(theResourcesPackage.getResource());
        dispatchGroupTaskEClass.getESuperTypes().add(theGraphPackage.getTask());
        dispatchGroupResourceEClass.getESuperTypes().add(theResourcesPackage.getResource());
        peripheralResourceEClass.getESuperTypes().add(theResourcesPackage.getResource());
        g1 = createEGenericType(theGraphPackage.getTaskDependencyGraph());
        g2 = createEGenericType(theGraphPackage.getTask());
        g1.getETypeArguments().add(g2);
        dispatchGraphEClass.getEGenericSuperTypes().add(g1);
        g1 = createEGenericType(theSchedulePackage.getScheduledTask());
        g2 = createEGenericType(theGraphPackage.getTask());
        g1.getETypeArguments().add(g2);
        claimedByScheduledTaskEClass.getEGenericSuperTypes().add(g1);
        actionTaskEClass.getESuperTypes().add(theGraphPackage.getTask());
        g1 = createEGenericType(theDirectedGraphPackage.getAspect());
        g2 = createEGenericType(theSchedulePackage.getScheduledTask());
        g1.getETypeArguments().add(g2);
        EGenericType g3 = createEGenericType(theGraphPackage.getTask());
        g2.getETypeArguments().add(g3);
        g2 = createEGenericType(theSchedulePackage.getScheduledDependency());
        g1.getETypeArguments().add(g2);
        stochasticAnnotationEClass.getEGenericSuperTypes().add(g1);
        g1 = createEGenericType(theSchedulePackage.getScheduledTask());
        g2 = createEGenericType(theGraphPackage.getTask());
        g1.getETypeArguments().add(g2);
        eventStatusTaskEClass.getEGenericSuperTypes().add(g1);
        g1 = createEGenericType(theDirectedGraphPackage.getAspect());
        g2 = createEGenericType(eventAnnotationEClass_T);
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(theGraphPackage.getDependency());
        g1.getETypeArguments().add(g2);
        eventAnnotationEClass.getEGenericSuperTypes().add(g1);
        eventTaskEClass.getESuperTypes().add(theGraphPackage.getTask());
        g1 = createEGenericType(this.getEventTask());
        g2 = createEGenericType(theActivityPackage.getRequireEvent());
        g1.getETypeArguments().add(g2);
        requireTaskEClass.getEGenericSuperTypes().add(g1);
        g1 = createEGenericType(this.getEventTask());
        g2 = createEGenericType(theActivityPackage.getRaiseEvent());
        g1.getETypeArguments().add(g2);
        raiseTaskEClass.getEGenericSuperTypes().add(g1);

        // Initialize classes and features; add operations and parameters
        initEClass(peripheralActionTaskEClass, PeripheralActionTask.class, "PeripheralActionTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(claimTaskEClass, ClaimTask.class, "ClaimTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(releaseTaskEClass, ReleaseTask.class, "ReleaseTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(claimReleaseResourceEClass, ClaimReleaseResource.class, "ClaimReleaseResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(dispatchGroupTaskEClass, DispatchGroupTask.class, "DispatchGroupTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getDispatchGroupTask_DispatchGroup(), theDispatchingPackage.getDispatchGroup(), null, "dispatchGroup", null, 1, 1, DispatchGroupTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(dispatchGroupResourceEClass, DispatchGroupResource.class, "DispatchGroupResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(peripheralResourceEClass, PeripheralResource.class, "PeripheralResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getPeripheralResource_Peripheral(), theMachinePackage.getPeripheral(), null, "peripheral", null, 1, 1, PeripheralResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(dispatchGraphEClass, DispatchGraph.class, "DispatchGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getDispatchGraph_Dispatch(), theDispatchingPackage.getDispatch(), null, "dispatch", null, 1, 1, DispatchGraph.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(claimedByScheduledTaskEClass, ClaimedByScheduledTask.class, "ClaimedByScheduledTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        g1 = createEGenericType(theSchedulePackage.getScheduledTask());
        g2 = createEGenericType(theGraphPackage.getTask());
        g1.getETypeArguments().add(g2);
        initEReference(getClaimedByScheduledTask_Releases(), g1, null, "releases", null, 0, -1, ClaimedByScheduledTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        g1 = createEGenericType(theSchedulePackage.getScheduledTask());
        g2 = createEGenericType(theGraphPackage.getTask());
        g1.getETypeArguments().add(g2);
        initEReference(getClaimedByScheduledTask_Claims(), g1, null, "claims", null, 1, -1, ClaimedByScheduledTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(actionTaskEClass, ActionTask.class, "ActionTask", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        g1 = createEGenericType(actionTaskEClass_T);
        initEReference(getActionTask_Action(), g1, null, "action", null, 1, 1, ActionTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(stochasticAnnotationEClass, StochasticAnnotation.class, "StochasticAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getStochasticAnnotation_Weight(), ecorePackage.getEBigDecimal(), "weight", null, 1, 1, StochasticAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(eventStatusTaskEClass, EventStatusTask.class, "EventStatusTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(eventAnnotationEClass, EventAnnotation.class, "EventAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getEventAnnotation_RequireEvent(), ecorePackage.getEBoolean(), "requireEvent", null, 1, 1, EventAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(eventTaskEClass, EventTask.class, "EventTask", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        g1 = createEGenericType(eventTaskEClass_T);
        initEReference(getEventTask_Action(), g1, null, "action", null, 1, 1, EventTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(requireTaskEClass, RequireTask.class, "RequireTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(raiseTaskEClass, RaiseTask.class, "RaiseTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        // Create resource
        createResource(eNS_URI);
    }

} //lsat_graphPackageImpl
