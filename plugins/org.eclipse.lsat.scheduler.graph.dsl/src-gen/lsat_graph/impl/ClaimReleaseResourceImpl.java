/**
 */
package lsat_graph.impl;

import org.eclipse.lsat.common.scheduler.resources.impl.ResourceImpl;

import org.eclipse.emf.ecore.EClass;

import lsat_graph.ClaimReleaseResource;
import lsat_graph.lsat_graphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Claim Release Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClaimReleaseResourceImpl extends ResourceImpl implements ClaimReleaseResource {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ClaimReleaseResourceImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.CLAIM_RELEASE_RESOURCE;
    }

} //ClaimReleaseResourceImpl
