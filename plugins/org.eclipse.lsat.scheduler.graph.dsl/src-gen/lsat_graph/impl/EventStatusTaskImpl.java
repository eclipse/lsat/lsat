/**
 */
package lsat_graph.impl;

import lsat_graph.EventStatusTask;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.lsat.common.scheduler.graph.Task;

import org.eclipse.lsat.common.scheduler.schedule.impl.ScheduledTaskImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EventAction Status Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EventStatusTaskImpl extends ScheduledTaskImpl<Task> implements EventStatusTask {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected EventStatusTaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.EVENT_STATUS_TASK;
    }

} //EventStatusTaskImpl
