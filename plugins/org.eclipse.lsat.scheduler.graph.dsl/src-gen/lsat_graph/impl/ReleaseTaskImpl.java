/**
 */
package lsat_graph.impl;

import activity.Release;
import lsat_graph.ReleaseTask;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Release Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReleaseTaskImpl extends ActionTaskImpl<Release> implements ReleaseTask {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ReleaseTaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.RELEASE_TASK;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific type known in this context.
     * @generated
     */
	@Override
	public void setAction(Release newAction) {
        super.setAction(newAction);
    }

} //ReleaseTaskImpl
