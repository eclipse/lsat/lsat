/**
 */
package lsat_graph.impl;

import org.eclipse.lsat.common.scheduler.resources.impl.ResourceImpl;

import org.eclipse.emf.ecore.EClass;

import lsat_graph.DispatchGroupResource;
import lsat_graph.lsat_graphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dispatch Group Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DispatchGroupResourceImpl extends ResourceImpl implements DispatchGroupResource {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected DispatchGroupResourceImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.DISPATCH_GROUP_RESOURCE;
    }

} //DispatchGroupResourceImpl
