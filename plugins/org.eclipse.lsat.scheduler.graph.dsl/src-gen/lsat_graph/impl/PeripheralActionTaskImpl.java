/**
 */
package lsat_graph.impl;

import activity.PeripheralAction;
import lsat_graph.PeripheralActionTask;
import lsat_graph.lsat_graphPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Peripheral Action Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PeripheralActionTaskImpl extends ActionTaskImpl<PeripheralAction> implements PeripheralActionTask {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PeripheralActionTaskImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return lsat_graphPackage.Literals.PERIPHERAL_ACTION_TASK;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific type known in this context.
     * @generated
     */
	@Override
	public void setAction(PeripheralAction newAction) {
        super.setAction(newAction);
    }

} //PeripheralActionTaskImpl
