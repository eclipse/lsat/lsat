/**
 */
package lsat_graph;

import org.eclipse.lsat.common.scheduler.resources.Resource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Claim Release Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lsat_graph.lsat_graphPackage#getClaimReleaseResource()
 * @model
 * @generated
 */
public interface ClaimReleaseResource extends Resource {
} // ClaimReleaseResource
