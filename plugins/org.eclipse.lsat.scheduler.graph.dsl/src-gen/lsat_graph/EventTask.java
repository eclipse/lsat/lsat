/**
 */
package lsat_graph;

import activity.EventAction;
import org.eclipse.lsat.common.scheduler.graph.Task;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EventAction Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lsat_graph.EventTask#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @see lsat_graph.lsat_graphPackage#getEventTask()
 * @model abstract="true"
 * @generated
 */
public interface EventTask<T extends EventAction> extends Task
{
    /**
     * Returns the value of the '<em><b>Action</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Action</em>' reference.
     * @see #setAction(EventAction)
     * @see lsat_graph.lsat_graphPackage#getEventTask_Action()
     * @model required="true"
     * @generated
     */
    T getAction();

    /**
     * Sets the value of the '{@link lsat_graph.EventTask#getAction <em>Action</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Action</em>' reference.
     * @see #getAction()
     * @generated
     */
    void setAction(T value);

} // EventTask
