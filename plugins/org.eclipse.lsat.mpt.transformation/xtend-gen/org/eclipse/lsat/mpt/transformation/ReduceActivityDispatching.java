/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.transformation;

import activity.Activity;
import activity.ActivitySet;
import activity.util.ActivityUtil;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import dispatching.ActivityDispatching;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Consumer;
import machine.Import;
import machine.MachineFactory;
import machine.Resource;
import machine.ResourceItem;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.mpt.transformation.ExpandActivities;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * Add a named Activity for all combinations of ResourceItems.
 * The original (unusable) activity is removed.
 */
@SuppressWarnings("all")
public final class ReduceActivityDispatching {
  private ReduceActivityDispatching() {
  }
  
  /**
   * Reduces expanded activities back to their original format stored on disk.
   * Activities refer to the supplied activitySet
   */
  public static void reduce(final ActivityDispatching activityDispatching, final ActivitySet activitySet) {
    final HashSet<Activity> allActivities = CollectionLiterals.<Activity>newHashSet();
    EList<Activity> _activities = activitySet.getActivities();
    Iterables.<Activity>addAll(allActivities, _activities);
    final Function1<ActivitySet, EList<Activity>> _function = (ActivitySet it) -> {
      return it.getActivities();
    };
    Iterable<Activity> _flatMap = IterableExtensions.<ActivitySet, Activity>flatMap(Iterables.<ActivitySet>filter(activitySet.loadAll(), ActivitySet.class), _function);
    Iterables.<Activity>addAll(allActivities, _flatMap);
    final LinkedHashMap<String, Activity> expandMap = ReduceActivityDispatching.expandNames(IterableExtensions.<Activity>toList(allActivities));
    final Import importActivitySet = MachineFactory.eINSTANCE.createImport();
    importActivitySet.setImportURI(activitySet.eResource().getURI().toString());
    EList<Import> _imports = activityDispatching.getImports();
    _imports.add(importActivitySet);
    final Function1<DispatchGroup, EList<Dispatch>> _function_1 = (DispatchGroup it) -> {
      return it.getDispatches();
    };
    final Consumer<Dispatch> _function_2 = (Dispatch dis) -> {
      final Activity activity = expandMap.get(dis.getActivity().getName());
      final Collection<String> itemNames = ActivityUtil.getItemNames(activity.getName(), dis.getActivity().getName());
      final EList<Resource> resourceNeedingItem = activity.getResourcesNeedingItem();
      int _size = itemNames.size();
      int _size_1 = resourceNeedingItem.size();
      boolean _notEquals = (_size != _size_1);
      if (_notEquals) {
        throw new RuntimeException("Cannot convert expanded activity");
      }
      final Iterator<Resource> iter = resourceNeedingItem.iterator();
      for (final String itemName : itemNames) {
        {
          final Function1<ResourceItem, Boolean> _function_3 = (ResourceItem it) -> {
            String _name = it.getName();
            return Boolean.valueOf(Objects.equal(_name, itemName));
          };
          final ResourceItem resourceItem = IterableExtensions.<ResourceItem>findFirst(iter.next().getItems(), _function_3);
          if ((resourceItem == null)) {
            throw new RuntimeException(("Cannot find item with name " + itemName));
          }
          EList<ResourceItem> _resourceItems = dis.getResourceItems();
          _resourceItems.add(resourceItem);
        }
      }
      dis.setActivity(activity);
    };
    IterableExtensions.<DispatchGroup, Dispatch>flatMap(activityDispatching.getDispatchGroups(), _function_1).forEach(_function_2);
    EList<Import> _imports_1 = activityDispatching.getImports();
    _imports_1.remove(importActivitySet);
    final Function1<Import, Boolean> _function_3 = (Import it) -> {
      return Boolean.valueOf(it.getImportURI().contains(".activity"));
    };
    Import _findFirst = IterableExtensions.<Import>findFirst(activityDispatching.getImports(), _function_3);
    _findFirst.setImportURI(activitySet.eResource().getURI().toString());
  }
  
  /**
   * returns a map with key all possible activity names and activity as values
   */
  private static LinkedHashMap<String, Activity> expandNames(final List<Activity> activities) {
    return ExpandActivities.expandNames(activities);
  }
}
