/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.transformation;

import activity.Activity;
import activity.ActivitySet;
import activity.util.ActivityUtil;
import com.google.common.collect.Sets;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import machine.Resource;
import machine.ResourceItem;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

/**
 * Add a named Activity for all combinations of ResourceItems.
 * The original (unusable) activity is removed.
 */
@SuppressWarnings("all")
public final class ExpandActivities {
  private ExpandActivities() {
  }
  
  public static void expand(final ActivitySet activitySet) {
    final LinkedHashMap<String, Activity> expandMap = ExpandActivities.expandNames(activitySet.getActivities());
    int _size = expandMap.size();
    int _size_1 = activitySet.getActivities().size();
    boolean _equals = (_size == _size_1);
    if (_equals) {
      return;
    }
    final Function1<Activity, Boolean> _function = (Activity it) -> {
      boolean _isEmpty = it.getResourcesNeedingItem().isEmpty();
      return Boolean.valueOf((!_isEmpty));
    };
    final List<Activity> eligibleActivities = IterableExtensions.<Activity>toList(IterableExtensions.<Activity>filter(activitySet.getActivities(), _function));
    final Consumer<Activity> _function_1 = (Activity it) -> {
      ExpandActivities.expand(it);
    };
    eligibleActivities.forEach(_function_1);
  }
  
  private static void expand(final Activity activity) {
    final Function1<Resource, Set<ResourceItem>> _function = (Resource it) -> {
      return IterableExtensions.<ResourceItem>toSet(it.getItems());
    };
    final Consumer<List<ResourceItem>> _function_1 = (List<ResourceItem> it) -> {
      ActivityUtil.queryCreateExpandedActivity(activity, it);
    };
    ExpandActivities.cartesianProduct(IterableExtensions.<Set<ResourceItem>>toList(ListExtensions.<Resource, Set<ResourceItem>>map(activity.getResourcesNeedingItem(), _function))).forEach(_function_1);
    EObject _eContainer = activity.eContainer();
    final ActivitySet c = ((ActivitySet) _eContainer);
    EList<Activity> _activities = c.getActivities();
    _activities.remove(activity);
  }
  
  /**
   * returns a map with all possible activity names and activity as values
   */
  public static LinkedHashMap<String, Activity> expandNames(final List<Activity> activities) {
    final LinkedHashMap<String, Activity> result = CollectionLiterals.<String, Activity>newLinkedHashMap();
    final Consumer<Activity> _function = (Activity activity) -> {
      result.put(activity.getName(), activity);
      final Function1<Resource, Set<ResourceItem>> _function_1 = (Resource it) -> {
        return IterableExtensions.<ResourceItem>toSet(it.getItems());
      };
      final Function1<List<ResourceItem>, String> _function_2 = (List<ResourceItem> it) -> {
        return ActivityUtil.expandName(activity, it);
      };
      final Consumer<String> _function_3 = (String it) -> {
        result.put(it, activity);
      };
      IterableExtensions.<List<ResourceItem>, String>map(ExpandActivities.cartesianProduct(IterableExtensions.<Set<ResourceItem>>toList(ListExtensions.<Resource, Set<ResourceItem>>map(activity.getResourcesNeedingItem(), _function_1))), _function_2).forEach(_function_3);
    };
    activities.forEach(_function);
    return result;
  }
  
  private static Set<List<ResourceItem>> cartesianProduct(final List<Set<ResourceItem>> r) {
    return Sets.<ResourceItem>cartesianProduct(r);
  }
}
