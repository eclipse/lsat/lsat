/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.transformation;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

import activity.ActivitySet;

public class Activity2Graph extends AbstractModelTransformer<ActivitySet, ActivitySet> {
    @Override
    protected String getDefaultTransformation() {
        return "/transforms/activities2graph.qvto";
    }

    @Override
    protected ActivitySet doTransformModel(ActivitySet input) throws QvtoTransformationException {
        // Input/Output
        BasicModelExtent inoutActivity = new BasicModelExtent();
        inoutActivity.add(input);

        execute(inoutActivity);

        return validateOneAndOnlyOne(ActivitySet.class, inoutActivity);
    }
}
