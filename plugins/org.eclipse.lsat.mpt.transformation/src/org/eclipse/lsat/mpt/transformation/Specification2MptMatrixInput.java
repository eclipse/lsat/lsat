/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.transformation;

import org.eclipse.escet.cif.metamodel.cif.Specification;

import activity.ActivitySet;
import setting.Settings;

public class Specification2MptMatrixInput {
    private final ActivitySet activitySet;

    private final Settings settings;

    private final Specification cifSpecification;

    public Specification2MptMatrixInput(ActivitySet activitySet, Settings settings, Specification cifSpecification) {
        this.activitySet = activitySet;
        this.settings = settings;
        this.cifSpecification = cifSpecification;
    }

    public ActivitySet getActivitySet() {
        return activitySet;
    }

    public Settings getSettings() {
        return settings;
    }

    public Specification getCifSpecification() {
        return cifSpecification;
    }
}
