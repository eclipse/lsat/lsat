/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.transformation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.escet.cif.common.CifCollectUtils;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.lsat.common.mpt.MaxPlusSpecification;
import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

public class Specification2MptMatrix
        extends AbstractModelTransformer<Specification2MptMatrixInput, MaxPlusSpecification>
{
    @Override
    protected String getDefaultTransformation() {
        return "/transforms/activities2matricesTransformation.qvto";
    }

    @Override
    protected MaxPlusSpecification doTransformModel(Specification2MptMatrixInput input)
            throws QvtoTransformationException
    {
        // Input
        BasicModelExtent inActivity = new BasicModelExtent();
        inActivity.add(input.getActivitySet());

        BasicModelExtent inSetting = new BasicModelExtent();
        inSetting.add(input.getSettings());

        BasicModelExtent inCifSpecification = new BasicModelExtent();

        // FIXME For a strange reason, compiler requires to have explicit
        // casting to EObject, even though cif.Specification extends EObject
        // (not directly).

        inCifSpecification.add((EObject)input.getCifSpecification());

        // TODO find a more elegant way to incorporate the events in the transformation
        // Aggregate events from CIF specification.
        List<Event> events = new ArrayList<>();
        CifCollectUtils.collectEvents(input.getCifSpecification(), events);
        BasicModelExtent inEvents = new BasicModelExtent(events);

        // Output
        BasicModelExtent outMatrix = new BasicModelExtent();

        execute(inActivity, inSetting, inCifSpecification, inEvents, outMatrix);

        return validateOneAndOnlyOne(MaxPlusSpecification.class, outMatrix);
    }
}
