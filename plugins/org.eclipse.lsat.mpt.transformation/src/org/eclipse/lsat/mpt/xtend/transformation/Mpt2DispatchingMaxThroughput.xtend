/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.mpt.xtend.transformation

import activity.ActivitySet
import org.eclipse.lsat.common.mpt.api.MaximumThroughputResult

import static extension org.eclipse.lsat.mpt.xtend.transformation.Mpt2Dispatching.createSequence
import static extension org.eclipse.lsat.mpt.xtend.transformation.Mpt2Dispatching.round

class Mpt2DispatchingMaxThroughput {
    
    def transformModel(ActivitySet inActivity, MaximumThroughputResult throughputResult) '''
    // Maximum throughput analysis result
    import "«inActivity.eResource.URI.toString»"
    
    «throughputResult.getTransientStateActivities.createSequence('Startup phase')»
    
    // Repeatable activity sequence, achieving the maximum throughput of «throughputResult.throughput.round»:
    «throughputResult.getSteadyStateActivities.createSequence('Steady phase')»
    '''

}
