/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package machine.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import machine.Import;
import machine.ImportContainer;
import machine.MachineFactory;

public class ImportsFlattener {
    private ImportsFlattener() {
    }

    /**
     * Returns a flattened model of all referred {@link ImportContainer}s. <br>
     * There will only be one {@link ImportContainer} per type after flattening.
     *
     * @return a ResourceSet containing all resources
     */
    public static ResourceSet flatten(URI baseURI, ImportContainer... importContainers) {
        Flattener flattener = new Flattener(baseURI);
        for (ImportContainer importContainer: importContainers) {
            // make sure the roots are returned as last so put a placeholder at the top as
            // the result is eventually reversed.
            flattener.result.put(importContainer.eClass(), null);
            String rootExtension = EcoreUtil.getURI(importContainer).fileExtension();
            flattener.extensions.put(importContainer.eClass(), rootExtension);
        }
        flattener.flatten(importContainers);
        return flattener.resourceSet;
    }

    private static final class Flattener {
        private final Copier copier = new Copier();

        private final Map<EClass, ImportContainer> result = new LinkedHashMap<>();

        private final Map<EClass, String> extensions = new LinkedHashMap<>();

        private final Collection<ImportContainer> seen = new LinkedHashSet<>();

        private final ResourceSet resourceSet = createResourceSet();

        private final URI baseURI;

        public Flattener(URI baseURI) {
            this.baseURI = baseURI;
        }

        /**
         * Flattens a hierarchy of ImportContainers to a single container per {@link EClass} The result is returned as a
         * map with the {@link EClass} as key and the import
         *
         * @param root
         *
         * @param <T>
         * @return a map with the {@link EClass} as key and the newly created ImportContainer as value
         */
        private void flatten(ImportContainer... importContainers) {
            for (ImportContainer importContainer: importContainers) {
                flatten(importContainer);
            }
            copier.copyReferences();
            result.values().forEach(ic -> replaceImports(ic));
        }

        @SuppressWarnings({"rawtypes", "unchecked"})
        private void flatten(ImportContainer importContainer) {
            if (!seen.add(importContainer)) {
                return;
            }

            EClass eClass = importContainer.eClass();
            EObject copy = copier.copy(importContainer);

            ImportContainer container = result.get(eClass);
            if (container == null) {
                container = (ImportContainer)copy;
                // make sure the new resource can be found in the resource set.
                Resource childResource = resourceSet
                        .createResource(createReference(extensions.get(container.eClass())));
                childResource.getContents().add(container);
                result.put(eClass, container);
            } else {
                for (int i = 0, size = eClass.getFeatureCount(); i < size; ++i) {
                    EStructuralFeature eStructuralFeature = eClass.getEStructuralFeature(i);
                    Object v = copy.eGet(eStructuralFeature);
                    if (v instanceof Collection) {
                        ((Collection)container.eGet(eStructuralFeature)).addAll((Collection)v);
                    } else {
                        container.eSet(eStructuralFeature, v);
                    }
                }
            }
            for (Import imp: importContainer.getImports()) {
                flatten(findChild(imp));
            }
            ;
        }

        private ImportContainer findChild(Import imp) {
            ImportContainer result = (ImportContainer)imp.load().get(0);
            extensions.put(result.eClass(), extension(imp));
            return result;
        }

        private void replaceImports(ImportContainer container) {
            if (baseURI == null || container == null) {
                return;
            }
            String myExtension = extensions.get(container.eClass());
            URI myURI = container.eResource().getURI();
            List<URI> uris = container.getImports().stream().map(ImportsFlattener::extension)
                    .filter(e -> !e.equals(myExtension)).map(extension -> createReference(extension).deresolve(myURI))
                    .distinct().collect(Collectors.toList());

            container.getImports().clear();

            uris.forEach(uri -> {
                Import imp = MachineFactory.eINSTANCE.createImport();
                imp.setImportURI(uri.toString());
                container.getImports().add(imp);
            });
        }

        private URI createReference(String extension) {
            return baseURI.appendFileExtension(extension);
        }
    }

    private static String extension(Import imp) {
        URI uri = URI.createURI(imp.getImportURI());
        return uri.fileExtension();
    }

    private static ResourceSet createResourceSet() {
        ResourceSetImpl resourceSet = new ResourceSetImpl();
        resourceSet.setURIResourceMap(new HashMap<URI, Resource>());
        return resourceSet;
    }
}
