/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package machine.impl;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;
import static org.eclipse.lsat.common.util.IterableUtil.contains;

import java.util.Objects;
import java.util.Set;

import org.eclipse.emf.common.util.EList;

//import org.eclipse.lsat.common.graph.directed.editable.Edge;
//import org.eclipse.lsat.common.graph.directed.editable.Node;

import machine.Axis;
import machine.Path;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.Profile;
import machine.SetPoint;
import machine.SymbolicPosition;

public final class MachineQueries {
    private MachineQueries() {
        /* Empty */
    }

    /**
     * Returns the {@link PathTargetReference} of the target position for the path (if found).<br>
     * The path of the {@link PathTargetReference} can be retrieved by casting the
     * {@link PathTargetReference#eContainer()} to a {@link Path}.
     *
     * @return The path from source to target with the specified profile, or null if it doesn't exist
     */
    public static final PathTargetReference findPath(SymbolicPosition source, SymbolicPosition target,
            Profile profile)
    {
        return from(source.getOutgoingPaths()).select(p -> p.getProfiles().contains(profile)).collect(Path::getTargets)
                .any(t -> t.getPosition() == target);
    }

    public static final Set<Profile> getAvailableProfiles(SymbolicPosition source, SymbolicPosition target) {
        return from(source.getOutgoingPaths())
                .select(p -> contains(from(p.getTargets()).collectOne(PathTargetReference::getPosition), target))
                .collect(Path::getProfiles).asOrderedSet();
    }

    public static Axis getAxis(Peripheral peripheral, String axisName) {
        return from(peripheral.getType().getAxes()).any(x -> Objects.equals(axisName, x.getName()));
    }

    public static SetPoint getSetPoint(Peripheral peripheral, String setPointName) {
        return from(peripheral.getType().getSetPoints()).any(x -> Objects.equals(setPointName, x.getName()));
    }

    public static final Set<Path> findTargetReference(SymbolicPosition source) {
        EList<Path> p1 = source.getPeripheral().getPaths();
        // List<Node> listSourcesFirstNode =
        // from(node.getIncomingEdges()).xcollectOne(Edge::getSourceNode)
        // .asList();

        return from(p1).select(p -> source.getTargetReferences().containsAll(p.getTargets())).asOrderedSet();
    }
}
