/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.Machine#getType <em>Type</em>}</li>
 *   <li>{@link machine.Machine#getPathAnnotations <em>Path Annotations</em>}</li>
 *   <li>{@link machine.Machine#getResources <em>Resources</em>}</li>
 *   <li>{@link machine.Machine#getPeripheralTypes <em>Peripheral Types</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getMachine()
 * @model
 * @generated
 */
public interface Machine extends ImportContainer {
	/**
     * Returns the value of the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Type</em>' attribute.
     * @see #setType(String)
     * @see machine.MachinePackage#getMachine_Type()
     * @model
     * @generated
     */
	String getType();

	/**
     * Sets the value of the '{@link machine.Machine#getType <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Type</em>' attribute.
     * @see #getType()
     * @generated
     */
	void setType(String value);

	/**
     * Returns the value of the '<em><b>Path Annotations</b></em>' containment reference list.
     * The list contents are of type {@link machine.PathAnnotation}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Path Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Path Annotations</em>' containment reference list.
     * @see machine.MachinePackage#getMachine_PathAnnotations()
     * @model containment="true" keys="name"
     * @generated
     */
	EList<PathAnnotation> getPathAnnotations();

	/**
     * Returns the value of the '<em><b>Resources</b></em>' containment reference list.
     * The list contents are of type {@link machine.Resource}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resources</em>' containment reference list.
     * @see machine.MachinePackage#getMachine_Resources()
     * @model containment="true"
     * @generated
     */
	EList<Resource> getResources();

	/**
     * Returns the value of the '<em><b>Peripheral Types</b></em>' containment reference list.
     * The list contents are of type {@link machine.PeripheralType}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripheral Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral Types</em>' containment reference list.
     * @see machine.MachinePackage#getMachine_PeripheralTypes()
     * @model containment="true"
     * @generated
     */
	EList<PeripheralType> getPeripheralTypes();

} // Machine
