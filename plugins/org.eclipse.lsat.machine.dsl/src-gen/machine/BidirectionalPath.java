/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bidirectional Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.BidirectionalPath#getEndPoints <em>End Points</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getBidirectionalPath()
 * @model
 * @generated
 */
public interface BidirectionalPath extends Path {
	/**
     * Returns the value of the '<em><b>End Points</b></em>' containment reference list.
     * The list contents are of type {@link machine.PathTargetReference}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Points</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>End Points</em>' containment reference list.
     * @see machine.MachinePackage#getBidirectionalPath_EndPoints()
     * @model containment="true" lower="2" upper="2"
     * @generated
     */
	EList<PathTargetReference> getEndPoints();

} // BidirectionalPath
