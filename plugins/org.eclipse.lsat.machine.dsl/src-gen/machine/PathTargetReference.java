/**
 */
package machine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Target Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.PathTargetReference#getName <em>Name</em>}</li>
 *   <li>{@link machine.PathTargetReference#getPosition <em>Position</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getPathTargetReference()
 * @model
 * @generated
 */
public interface PathTargetReference extends HasSettling {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see machine.MachinePackage#getPathTargetReference_Name()
     * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	String getName();

	/**
     * Returns the value of the '<em><b>Position</b></em>' reference.
     * It is bidirectional and its opposite is '{@link machine.SymbolicPosition#getTargetReferences <em>Target References</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Position</em>' reference.
     * @see #setPosition(SymbolicPosition)
     * @see machine.MachinePackage#getPathTargetReference_Position()
     * @see machine.SymbolicPosition#getTargetReferences
     * @model opposite="targetReferences" resolveProxies="false" required="true"
     * @generated
     */
	SymbolicPosition getPosition();

	/**
     * Sets the value of the '{@link machine.PathTargetReference#getPosition <em>Position</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Position</em>' reference.
     * @see #getPosition()
     * @generated
     */
	void setPosition(SymbolicPosition value);

} // PathTargetReference
