/**
 */
package machine;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Profile</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.Profile#getName <em>Name</em>}</li>
 *   <li>{@link machine.Profile#getPeripheral <em>Peripheral</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getProfile()
 * @model
 * @generated
 */
public interface Profile extends EObject {
	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getProfile_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.Profile#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link machine.Peripheral#getProfiles <em>Profiles</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripheral</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' container reference.
     * @see #setPeripheral(Peripheral)
     * @see machine.MachinePackage#getProfile_Peripheral()
     * @see machine.Peripheral#getProfiles
     * @model opposite="profiles" required="true" transient="false"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link machine.Profile#getPeripheral <em>Peripheral</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' container reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

} // Profile
