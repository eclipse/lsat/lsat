/**
 */
package machine;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see machine.MachineFactory
 * @model kind="package"
 * @generated
 */
public interface MachinePackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "machine";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.eclipse.org/lsat/machine";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "machine";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	MachinePackage eINSTANCE = machine.impl.MachinePackageImpl.init();

	/**
     * The meta object id for the '{@link machine.impl.PeripheralTypeImpl <em>Peripheral Type</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.PeripheralTypeImpl
     * @see machine.impl.MachinePackageImpl#getPeripheralType()
     * @generated
     */
	int PERIPHERAL_TYPE = 0;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_TYPE__NAME = 0;

	/**
     * The feature id for the '<em><b>Conversion</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_TYPE__CONVERSION = 1;

	/**
     * The feature id for the '<em><b>Axes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_TYPE__AXES = 2;

	/**
     * The feature id for the '<em><b>Set Points</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_TYPE__SET_POINTS = 3;

	/**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_TYPE__ACTIONS = 4;

	/**
     * The number of structural features of the '<em>Peripheral Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_TYPE_FEATURE_COUNT = 5;

	/**
     * The number of operations of the '<em>Peripheral Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_TYPE_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.PathImpl <em>Path</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.PathImpl
     * @see machine.impl.MachinePackageImpl#getPath()
     * @generated
     */
	int PATH = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH__NAME = 0;

	/**
     * The feature id for the '<em><b>Profiles</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH__PROFILES = 1;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH__PERIPHERAL = 2;

	/**
     * The feature id for the '<em><b>Annotations</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH__ANNOTATIONS = 3;

	/**
     * The number of structural features of the '<em>Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_FEATURE_COUNT = 4;

	/**
     * The operation id for the '<em>Get Sources</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH___GET_SOURCES = 0;

	/**
     * The operation id for the '<em>Get Targets</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH___GET_TARGETS = 1;

	/**
     * The number of operations of the '<em>Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_OPERATION_COUNT = 2;

	/**
     * The meta object id for the '{@link machine.impl.PositionImpl <em>Position</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.PositionImpl
     * @see machine.impl.MachinePackageImpl#getPosition()
     * @generated
     */
	int POSITION = 11;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int POSITION__NAME = 0;

	/**
     * The number of structural features of the '<em>Position</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int POSITION_FEATURE_COUNT = 1;

	/**
     * The number of operations of the '<em>Position</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int POSITION_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.SymbolicPositionImpl <em>Symbolic Position</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.SymbolicPositionImpl
     * @see machine.impl.MachinePackageImpl#getSymbolicPosition()
     * @generated
     */
	int SYMBOLIC_POSITION = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION__NAME = POSITION__NAME;

	/**
     * The feature id for the '<em><b>Axis Position</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION__AXIS_POSITION = POSITION_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION__PERIPHERAL = POSITION_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Target References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION__TARGET_REFERENCES = POSITION_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Source References</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION__SOURCE_REFERENCES = POSITION_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>Symbolic Position</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION_FEATURE_COUNT = POSITION_FEATURE_COUNT + 4;

	/**
     * The operation id for the '<em>Get Position</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION___GET_POSITION__AXIS = POSITION_OPERATION_COUNT + 0;

	/**
     * The operation id for the '<em>Get Outgoing Paths</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION___GET_OUTGOING_PATHS = POSITION_OPERATION_COUNT + 1;

	/**
     * The number of operations of the '<em>Symbolic Position</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SYMBOLIC_POSITION_OPERATION_COUNT = POSITION_OPERATION_COUNT + 2;

	/**
     * The meta object id for the '{@link machine.IResource <em>IResource</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.IResource
     * @see machine.impl.MachinePackageImpl#getIResource()
     * @generated
     */
	int IRESOURCE = 21;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IRESOURCE__NAME = 0;

	/**
     * The number of structural features of the '<em>IResource</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IRESOURCE_FEATURE_COUNT = 1;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IRESOURCE___GET_RESOURCE = 0;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IRESOURCE___FQN = 1;

	/**
     * The number of operations of the '<em>IResource</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IRESOURCE_OPERATION_COUNT = 2;

	/**
     * The meta object id for the '{@link machine.impl.ResourceImpl <em>Resource</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.ResourceImpl
     * @see machine.impl.MachinePackageImpl#getResource()
     * @generated
     */
	int RESOURCE = 3;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE__NAME = IRESOURCE__NAME;

	/**
     * The feature id for the '<em><b>Peripherals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE__PERIPHERALS = IRESOURCE_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Items</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE__ITEMS = IRESOURCE_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Resource Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int RESOURCE__RESOURCE_TYPE = IRESOURCE_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Resource</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_FEATURE_COUNT = IRESOURCE_FEATURE_COUNT + 3;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE___GET_RESOURCE = IRESOURCE_OPERATION_COUNT + 0;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE___FQN = IRESOURCE_OPERATION_COUNT + 1;

	/**
     * The number of operations of the '<em>Resource</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_OPERATION_COUNT = IRESOURCE_OPERATION_COUNT + 2;

	/**
     * The meta object id for the '{@link machine.impl.ActionTypeImpl <em>Action Type</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.ActionTypeImpl
     * @see machine.impl.MachinePackageImpl#getActionType()
     * @generated
     */
	int ACTION_TYPE = 4;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TYPE__NAME = 0;

	/**
     * The number of structural features of the '<em>Action Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TYPE_FEATURE_COUNT = 1;

	/**
     * The number of operations of the '<em>Action Type</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int ACTION_TYPE_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.PeripheralImpl <em>Peripheral</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.PeripheralImpl
     * @see machine.impl.MachinePackageImpl#getPeripheral()
     * @generated
     */
	int PERIPHERAL = 5;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__NAME = 0;

	/**
     * The feature id for the '<em><b>Type</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__TYPE = 1;

	/**
     * The feature id for the '<em><b>Axis Positions</b></em>' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__AXIS_POSITIONS = 2;

	/**
     * The feature id for the '<em><b>Positions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__POSITIONS = 3;

	/**
     * The feature id for the '<em><b>Resource</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__RESOURCE = 4;

	/**
     * The feature id for the '<em><b>Paths</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__PATHS = 5;

	/**
     * The feature id for the '<em><b>Profiles</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__PROFILES = 6;

	/**
     * The feature id for the '<em><b>Distances</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL__DISTANCES = 7;

	/**
     * The number of structural features of the '<em>Peripheral</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_FEATURE_COUNT = 8;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL___FQN = 0;

	/**
     * The number of operations of the '<em>Peripheral</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PERIPHERAL_OPERATION_COUNT = 1;

	/**
     * The meta object id for the '{@link machine.impl.ImportContainerImpl <em>Import Container</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.ImportContainerImpl
     * @see machine.impl.MachinePackageImpl#getImportContainer()
     * @generated
     */
	int IMPORT_CONTAINER = 7;

	/**
     * The feature id for the '<em><b>Imports</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT_CONTAINER__IMPORTS = 0;

	/**
     * The number of structural features of the '<em>Import Container</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT_CONTAINER_FEATURE_COUNT = 1;

	/**
     * The operation id for the '<em>Load All</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT_CONTAINER___LOAD_ALL = 0;

	/**
     * The number of operations of the '<em>Import Container</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT_CONTAINER_OPERATION_COUNT = 1;

	/**
     * The meta object id for the '{@link machine.impl.MachineImpl <em>Machine</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.MachineImpl
     * @see machine.impl.MachinePackageImpl#getMachine()
     * @generated
     */
	int MACHINE = 6;

	/**
     * The feature id for the '<em><b>Imports</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE__IMPORTS = IMPORT_CONTAINER__IMPORTS;

	/**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE__TYPE = IMPORT_CONTAINER_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Path Annotations</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE__PATH_ANNOTATIONS = IMPORT_CONTAINER_FEATURE_COUNT + 1;

	/**
     * The feature id for the '<em><b>Resources</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE__RESOURCES = IMPORT_CONTAINER_FEATURE_COUNT + 2;

	/**
     * The feature id for the '<em><b>Peripheral Types</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE__PERIPHERAL_TYPES = IMPORT_CONTAINER_FEATURE_COUNT + 3;

	/**
     * The number of structural features of the '<em>Machine</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE_FEATURE_COUNT = IMPORT_CONTAINER_FEATURE_COUNT + 4;

	/**
     * The operation id for the '<em>Load All</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE___LOAD_ALL = IMPORT_CONTAINER___LOAD_ALL;

	/**
     * The number of operations of the '<em>Machine</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int MACHINE_OPERATION_COUNT = IMPORT_CONTAINER_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link machine.impl.ImportImpl <em>Import</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.ImportImpl
     * @see machine.impl.MachinePackageImpl#getImport()
     * @generated
     */
	int IMPORT = 8;

	/**
     * The feature id for the '<em><b>Import URI</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT__IMPORT_URI = 0;

	/**
     * The number of structural features of the '<em>Import</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT_FEATURE_COUNT = 1;

	/**
     * The operation id for the '<em>Load</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT___LOAD = 0;

	/**
     * The number of operations of the '<em>Import</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int IMPORT_OPERATION_COUNT = 1;

	/**
     * The meta object id for the '{@link machine.impl.ProfileImpl <em>Profile</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.ProfileImpl
     * @see machine.impl.MachinePackageImpl#getProfile()
     * @generated
     */
	int PROFILE = 9;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE__NAME = 0;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE__PERIPHERAL = 1;

	/**
     * The number of structural features of the '<em>Profile</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Profile</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PROFILE_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.AxisImpl <em>Axis</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.AxisImpl
     * @see machine.impl.MachinePackageImpl#getAxis()
     * @generated
     */
	int AXIS = 10;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS__NAME = 0;

	/**
     * The feature id for the '<em><b>Set Points</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS__SET_POINTS = 1;

	/**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS__UNIT = 2;

	/**
     * The number of structural features of the '<em>Axis</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_FEATURE_COUNT = 3;

	/**
     * The number of operations of the '<em>Axis</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.SetPointImpl <em>Set Point</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.SetPointImpl
     * @see machine.impl.MachinePackageImpl#getSetPoint()
     * @generated
     */
	int SET_POINT = 12;

	/**
     * The feature id for the '<em><b>Axes</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SET_POINT__AXES = 0;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SET_POINT__NAME = 1;

	/**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SET_POINT__UNIT = 2;

	/**
     * The number of structural features of the '<em>Set Point</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SET_POINT_FEATURE_COUNT = 3;

	/**
     * The number of operations of the '<em>Set Point</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int SET_POINT_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.AxisPositionMapEntryImpl <em>Axis Position Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.AxisPositionMapEntryImpl
     * @see machine.impl.MachinePackageImpl#getAxisPositionMapEntry()
     * @generated
     */
	int AXIS_POSITION_MAP_ENTRY = 13;

	/**
     * The feature id for the '<em><b>Value</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITION_MAP_ENTRY__VALUE = 0;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITION_MAP_ENTRY__KEY = 1;

	/**
     * The number of structural features of the '<em>Axis Position Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Axis Position Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITION_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.AxisPositionsMapEntryImpl <em>Axis Positions Map Entry</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.AxisPositionsMapEntryImpl
     * @see machine.impl.MachinePackageImpl#getAxisPositionsMapEntry()
     * @generated
     */
	int AXIS_POSITIONS_MAP_ENTRY = 14;

	/**
     * The feature id for the '<em><b>Key</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITIONS_MAP_ENTRY__KEY = 0;

	/**
     * The feature id for the '<em><b>Value</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITIONS_MAP_ENTRY__VALUE = 1;

	/**
     * The number of structural features of the '<em>Axis Positions Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITIONS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Axis Positions Map Entry</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int AXIS_POSITIONS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.HasSettlingImpl <em>Has Settling</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.HasSettlingImpl
     * @see machine.impl.MachinePackageImpl#getHasSettling()
     * @generated
     */
	int HAS_SETTLING = 24;

	/**
     * The feature id for the '<em><b>Settling</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_SETTLING__SETTLING = 0;

	/**
     * The number of structural features of the '<em>Has Settling</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_SETTLING_FEATURE_COUNT = 1;

	/**
     * The number of operations of the '<em>Has Settling</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_SETTLING_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.PathTargetReferenceImpl <em>Path Target Reference</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.PathTargetReferenceImpl
     * @see machine.impl.MachinePackageImpl#getPathTargetReference()
     * @generated
     */
	int PATH_TARGET_REFERENCE = 15;

	/**
     * The feature id for the '<em><b>Settling</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_TARGET_REFERENCE__SETTLING = HAS_SETTLING__SETTLING;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_TARGET_REFERENCE__NAME = HAS_SETTLING_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Position</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_TARGET_REFERENCE__POSITION = HAS_SETTLING_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Path Target Reference</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_TARGET_REFERENCE_FEATURE_COUNT = HAS_SETTLING_FEATURE_COUNT + 2;

	/**
     * The number of operations of the '<em>Path Target Reference</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_TARGET_REFERENCE_OPERATION_COUNT = HAS_SETTLING_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link machine.impl.UnidirectionalPathImpl <em>Unidirectional Path</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.UnidirectionalPathImpl
     * @see machine.impl.MachinePackageImpl#getUnidirectionalPath()
     * @generated
     */
	int UNIDIRECTIONAL_PATH = 16;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH__NAME = PATH__NAME;

	/**
     * The feature id for the '<em><b>Profiles</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH__PROFILES = PATH__PROFILES;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH__PERIPHERAL = PATH__PERIPHERAL;

	/**
     * The feature id for the '<em><b>Annotations</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH__ANNOTATIONS = PATH__ANNOTATIONS;

	/**
     * The feature id for the '<em><b>Source</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH__SOURCE = PATH_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Target</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH__TARGET = PATH_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Unidirectional Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH_FEATURE_COUNT = PATH_FEATURE_COUNT + 2;

	/**
     * The operation id for the '<em>Get Sources</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH___GET_SOURCES = PATH___GET_SOURCES;

	/**
     * The operation id for the '<em>Get Targets</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH___GET_TARGETS = PATH___GET_TARGETS;

	/**
     * The number of operations of the '<em>Unidirectional Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int UNIDIRECTIONAL_PATH_OPERATION_COUNT = PATH_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link machine.impl.BidirectionalPathImpl <em>Bidirectional Path</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.BidirectionalPathImpl
     * @see machine.impl.MachinePackageImpl#getBidirectionalPath()
     * @generated
     */
	int BIDIRECTIONAL_PATH = 17;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH__NAME = PATH__NAME;

	/**
     * The feature id for the '<em><b>Profiles</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH__PROFILES = PATH__PROFILES;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH__PERIPHERAL = PATH__PERIPHERAL;

	/**
     * The feature id for the '<em><b>Annotations</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH__ANNOTATIONS = PATH__ANNOTATIONS;

	/**
     * The feature id for the '<em><b>End Points</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH__END_POINTS = PATH_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Bidirectional Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH_FEATURE_COUNT = PATH_FEATURE_COUNT + 1;

	/**
     * The operation id for the '<em>Get Sources</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH___GET_SOURCES = PATH___GET_SOURCES;

	/**
     * The operation id for the '<em>Get Targets</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH___GET_TARGETS = PATH___GET_TARGETS;

	/**
     * The number of operations of the '<em>Bidirectional Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int BIDIRECTIONAL_PATH_OPERATION_COUNT = PATH_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link machine.impl.FullMeshPathImpl <em>Full Mesh Path</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.FullMeshPathImpl
     * @see machine.impl.MachinePackageImpl#getFullMeshPath()
     * @generated
     */
	int FULL_MESH_PATH = 18;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH__NAME = PATH__NAME;

	/**
     * The feature id for the '<em><b>Profiles</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH__PROFILES = PATH__PROFILES;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH__PERIPHERAL = PATH__PERIPHERAL;

	/**
     * The feature id for the '<em><b>Annotations</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH__ANNOTATIONS = PATH__ANNOTATIONS;

	/**
     * The feature id for the '<em><b>End Points</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH__END_POINTS = PATH_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Full Mesh Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH_FEATURE_COUNT = PATH_FEATURE_COUNT + 1;

	/**
     * The operation id for the '<em>Get Sources</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH___GET_SOURCES = PATH___GET_SOURCES;

	/**
     * The operation id for the '<em>Get Targets</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH___GET_TARGETS = PATH___GET_TARGETS;

	/**
     * The number of operations of the '<em>Full Mesh Path</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int FULL_MESH_PATH_OPERATION_COUNT = PATH_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link machine.impl.PathAnnotationImpl <em>Path Annotation</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.PathAnnotationImpl
     * @see machine.impl.MachinePackageImpl#getPathAnnotation()
     * @generated
     */
	int PATH_ANNOTATION = 19;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_ANNOTATION__NAME = 0;

	/**
     * The feature id for the '<em><b>Paths</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_ANNOTATION__PATHS = 1;

	/**
     * The number of structural features of the '<em>Path Annotation</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_ANNOTATION_FEATURE_COUNT = 2;

	/**
     * The number of operations of the '<em>Path Annotation</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int PATH_ANNOTATION_OPERATION_COUNT = 0;

	/**
     * The meta object id for the '{@link machine.impl.ResourceItemImpl <em>Resource Item</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.ResourceItemImpl
     * @see machine.impl.MachinePackageImpl#getResourceItem()
     * @generated
     */
	int RESOURCE_ITEM = 20;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_ITEM__NAME = IRESOURCE__NAME;

	/**
     * The feature id for the '<em><b>Resource</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_ITEM__RESOURCE = IRESOURCE_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Resource Item</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_ITEM_FEATURE_COUNT = IRESOURCE_FEATURE_COUNT + 1;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_ITEM___GET_RESOURCE = IRESOURCE___GET_RESOURCE;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_ITEM___FQN = IRESOURCE_OPERATION_COUNT + 0;

	/**
     * The number of operations of the '<em>Resource Item</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int RESOURCE_ITEM_OPERATION_COUNT = IRESOURCE_OPERATION_COUNT + 1;

	/**
     * The meta object id for the '{@link machine.impl.DistanceImpl <em>Distance</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.impl.DistanceImpl
     * @see machine.impl.MachinePackageImpl#getDistance()
     * @generated
     */
	int DISTANCE = 22;

	/**
     * The feature id for the '<em><b>Settling</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE__SETTLING = HAS_SETTLING__SETTLING;

	/**
     * The feature id for the '<em><b>Peripheral</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE__PERIPHERAL = HAS_SETTLING_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE__NAME = HAS_SETTLING_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Distance</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_FEATURE_COUNT = HAS_SETTLING_FEATURE_COUNT + 2;

	/**
     * The number of operations of the '<em>Distance</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DISTANCE_OPERATION_COUNT = HAS_SETTLING_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link machine.HasResourcePeripheral <em>Has Resource Peripheral</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see machine.HasResourcePeripheral
     * @see machine.impl.MachinePackageImpl#getHasResourcePeripheral()
     * @generated
     */
	int HAS_RESOURCE_PERIPHERAL = 23;

	/**
     * The number of structural features of the '<em>Has Resource Peripheral</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_RESOURCE_PERIPHERAL_FEATURE_COUNT = 0;

	/**
     * The operation id for the '<em>Fqn</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_RESOURCE_PERIPHERAL___FQN = 0;

	/**
     * The operation id for the '<em>Get Resource</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_RESOURCE_PERIPHERAL___GET_RESOURCE = 1;

	/**
     * The operation id for the '<em>Get Peripheral</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_RESOURCE_PERIPHERAL___GET_PERIPHERAL = 2;

	/**
     * The operation id for the '<em>Rp Equals</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_RESOURCE_PERIPHERAL___RP_EQUALS__HASRESOURCEPERIPHERAL = 3;

	/**
     * The number of operations of the '<em>Has Resource Peripheral</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int HAS_RESOURCE_PERIPHERAL_OPERATION_COUNT = 4;

	/**
     * The meta object id for the '{@link machine.ResourceType <em>Resource Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see machine.ResourceType
     * @see machine.impl.MachinePackageImpl#getResourceType()
     * @generated
     */
    int RESOURCE_TYPE = 25;

    /**
     * Returns the meta object for class '{@link machine.PeripheralType <em>Peripheral Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Peripheral Type</em>'.
     * @see machine.PeripheralType
     * @generated
     */
	EClass getPeripheralType();

	/**
     * Returns the meta object for the attribute '{@link machine.PeripheralType#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.PeripheralType#getName()
     * @see #getPeripheralType()
     * @generated
     */
	EAttribute getPeripheralType_Name();

	/**
     * Returns the meta object for the attribute '{@link machine.PeripheralType#getConversion <em>Conversion</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Conversion</em>'.
     * @see machine.PeripheralType#getConversion()
     * @see #getPeripheralType()
     * @generated
     */
	EAttribute getPeripheralType_Conversion();

	/**
     * Returns the meta object for the containment reference list '{@link machine.PeripheralType#getAxes <em>Axes</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Axes</em>'.
     * @see machine.PeripheralType#getAxes()
     * @see #getPeripheralType()
     * @generated
     */
	EReference getPeripheralType_Axes();

	/**
     * Returns the meta object for the containment reference list '{@link machine.PeripheralType#getSetPoints <em>Set Points</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Set Points</em>'.
     * @see machine.PeripheralType#getSetPoints()
     * @see #getPeripheralType()
     * @generated
     */
	EReference getPeripheralType_SetPoints();

	/**
     * Returns the meta object for the containment reference list '{@link machine.PeripheralType#getActions <em>Actions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Actions</em>'.
     * @see machine.PeripheralType#getActions()
     * @see #getPeripheralType()
     * @generated
     */
	EReference getPeripheralType_Actions();

	/**
     * Returns the meta object for class '{@link machine.Path <em>Path</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Path</em>'.
     * @see machine.Path
     * @generated
     */
	EClass getPath();

	/**
     * Returns the meta object for the attribute '{@link machine.Path#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.Path#getName()
     * @see #getPath()
     * @generated
     */
	EAttribute getPath_Name();

	/**
     * Returns the meta object for the reference list '{@link machine.Path#getProfiles <em>Profiles</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Profiles</em>'.
     * @see machine.Path#getProfiles()
     * @see #getPath()
     * @generated
     */
	EReference getPath_Profiles();

	/**
     * Returns the meta object for the container reference '{@link machine.Path#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Peripheral</em>'.
     * @see machine.Path#getPeripheral()
     * @see #getPath()
     * @generated
     */
	EReference getPath_Peripheral();

	/**
     * Returns the meta object for the reference list '{@link machine.Path#getAnnotations <em>Annotations</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Annotations</em>'.
     * @see machine.Path#getAnnotations()
     * @see #getPath()
     * @generated
     */
	EReference getPath_Annotations();

	/**
     * Returns the meta object for the '{@link machine.Path#getSources() <em>Get Sources</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Sources</em>' operation.
     * @see machine.Path#getSources()
     * @generated
     */
	EOperation getPath__GetSources();

	/**
     * Returns the meta object for the '{@link machine.Path#getTargets() <em>Get Targets</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Targets</em>' operation.
     * @see machine.Path#getTargets()
     * @generated
     */
	EOperation getPath__GetTargets();

	/**
     * Returns the meta object for class '{@link machine.SymbolicPosition <em>Symbolic Position</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Symbolic Position</em>'.
     * @see machine.SymbolicPosition
     * @generated
     */
	EClass getSymbolicPosition();

	/**
     * Returns the meta object for the map '{@link machine.SymbolicPosition#getAxisPosition <em>Axis Position</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Axis Position</em>'.
     * @see machine.SymbolicPosition#getAxisPosition()
     * @see #getSymbolicPosition()
     * @generated
     */
	EReference getSymbolicPosition_AxisPosition();

	/**
     * Returns the meta object for the container reference '{@link machine.SymbolicPosition#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Peripheral</em>'.
     * @see machine.SymbolicPosition#getPeripheral()
     * @see #getSymbolicPosition()
     * @generated
     */
	EReference getSymbolicPosition_Peripheral();

	/**
     * Returns the meta object for the reference list '{@link machine.SymbolicPosition#getTargetReferences <em>Target References</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Target References</em>'.
     * @see machine.SymbolicPosition#getTargetReferences()
     * @see #getSymbolicPosition()
     * @generated
     */
	EReference getSymbolicPosition_TargetReferences();

	/**
     * Returns the meta object for the reference list '{@link machine.SymbolicPosition#getSourceReferences <em>Source References</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Source References</em>'.
     * @see machine.SymbolicPosition#getSourceReferences()
     * @see #getSymbolicPosition()
     * @generated
     */
	EReference getSymbolicPosition_SourceReferences();

	/**
     * Returns the meta object for the '{@link machine.SymbolicPosition#getPosition(machine.Axis) <em>Get Position</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Position</em>' operation.
     * @see machine.SymbolicPosition#getPosition(machine.Axis)
     * @generated
     */
	EOperation getSymbolicPosition__GetPosition__Axis();

	/**
     * Returns the meta object for the '{@link machine.SymbolicPosition#getOutgoingPaths() <em>Get Outgoing Paths</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Outgoing Paths</em>' operation.
     * @see machine.SymbolicPosition#getOutgoingPaths()
     * @generated
     */
	EOperation getSymbolicPosition__GetOutgoingPaths();

	/**
     * Returns the meta object for class '{@link machine.Resource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Resource</em>'.
     * @see machine.Resource
     * @generated
     */
	EClass getResource();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Resource#getPeripherals <em>Peripherals</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Peripherals</em>'.
     * @see machine.Resource#getPeripherals()
     * @see #getResource()
     * @generated
     */
	EReference getResource_Peripherals();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Resource#getItems <em>Items</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Items</em>'.
     * @see machine.Resource#getItems()
     * @see #getResource()
     * @generated
     */
	EReference getResource_Items();

	/**
     * Returns the meta object for the attribute '{@link machine.Resource#getResourceType <em>Resource Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Resource Type</em>'.
     * @see machine.Resource#getResourceType()
     * @see #getResource()
     * @generated
     */
    EAttribute getResource_ResourceType();

    /**
     * Returns the meta object for the '{@link machine.Resource#getResource() <em>Get Resource</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Resource</em>' operation.
     * @see machine.Resource#getResource()
     * @generated
     */
	EOperation getResource__GetResource();

	/**
     * Returns the meta object for the '{@link machine.Resource#fqn() <em>Fqn</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Fqn</em>' operation.
     * @see machine.Resource#fqn()
     * @generated
     */
	EOperation getResource__Fqn();

	/**
     * Returns the meta object for class '{@link machine.ActionType <em>Action Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Action Type</em>'.
     * @see machine.ActionType
     * @generated
     */
	EClass getActionType();

	/**
     * Returns the meta object for the attribute '{@link machine.ActionType#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.ActionType#getName()
     * @see #getActionType()
     * @generated
     */
	EAttribute getActionType_Name();

	/**
     * Returns the meta object for class '{@link machine.Peripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Peripheral</em>'.
     * @see machine.Peripheral
     * @generated
     */
	EClass getPeripheral();

	/**
     * Returns the meta object for the attribute '{@link machine.Peripheral#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.Peripheral#getName()
     * @see #getPeripheral()
     * @generated
     */
	EAttribute getPeripheral_Name();

	/**
     * Returns the meta object for the reference '{@link machine.Peripheral#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Type</em>'.
     * @see machine.Peripheral#getType()
     * @see #getPeripheral()
     * @generated
     */
	EReference getPeripheral_Type();

	/**
     * Returns the meta object for the map '{@link machine.Peripheral#getAxisPositions <em>Axis Positions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the map '<em>Axis Positions</em>'.
     * @see machine.Peripheral#getAxisPositions()
     * @see #getPeripheral()
     * @generated
     */
	EReference getPeripheral_AxisPositions();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Peripheral#getPositions <em>Positions</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Positions</em>'.
     * @see machine.Peripheral#getPositions()
     * @see #getPeripheral()
     * @generated
     */
	EReference getPeripheral_Positions();

	/**
     * Returns the meta object for the container reference '{@link machine.Peripheral#getResource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Resource</em>'.
     * @see machine.Peripheral#getResource()
     * @see #getPeripheral()
     * @generated
     */
	EReference getPeripheral_Resource();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Peripheral#getPaths <em>Paths</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Paths</em>'.
     * @see machine.Peripheral#getPaths()
     * @see #getPeripheral()
     * @generated
     */
	EReference getPeripheral_Paths();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Peripheral#getProfiles <em>Profiles</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Profiles</em>'.
     * @see machine.Peripheral#getProfiles()
     * @see #getPeripheral()
     * @generated
     */
	EReference getPeripheral_Profiles();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Peripheral#getDistances <em>Distances</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Distances</em>'.
     * @see machine.Peripheral#getDistances()
     * @see #getPeripheral()
     * @generated
     */
	EReference getPeripheral_Distances();

	/**
     * Returns the meta object for the '{@link machine.Peripheral#fqn() <em>Fqn</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Fqn</em>' operation.
     * @see machine.Peripheral#fqn()
     * @generated
     */
	EOperation getPeripheral__Fqn();

	/**
     * Returns the meta object for class '{@link machine.Machine <em>Machine</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Machine</em>'.
     * @see machine.Machine
     * @generated
     */
	EClass getMachine();

	/**
     * Returns the meta object for the attribute '{@link machine.Machine#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see machine.Machine#getType()
     * @see #getMachine()
     * @generated
     */
	EAttribute getMachine_Type();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Machine#getPathAnnotations <em>Path Annotations</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Path Annotations</em>'.
     * @see machine.Machine#getPathAnnotations()
     * @see #getMachine()
     * @generated
     */
	EReference getMachine_PathAnnotations();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Machine#getResources <em>Resources</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Resources</em>'.
     * @see machine.Machine#getResources()
     * @see #getMachine()
     * @generated
     */
	EReference getMachine_Resources();

	/**
     * Returns the meta object for the containment reference list '{@link machine.Machine#getPeripheralTypes <em>Peripheral Types</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Peripheral Types</em>'.
     * @see machine.Machine#getPeripheralTypes()
     * @see #getMachine()
     * @generated
     */
	EReference getMachine_PeripheralTypes();

	/**
     * Returns the meta object for class '{@link machine.ImportContainer <em>Import Container</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Import Container</em>'.
     * @see machine.ImportContainer
     * @generated
     */
	EClass getImportContainer();

	/**
     * Returns the meta object for the containment reference list '{@link machine.ImportContainer#getImports <em>Imports</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Imports</em>'.
     * @see machine.ImportContainer#getImports()
     * @see #getImportContainer()
     * @generated
     */
	EReference getImportContainer_Imports();

	/**
     * Returns the meta object for the '{@link machine.ImportContainer#loadAll() <em>Load All</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Load All</em>' operation.
     * @see machine.ImportContainer#loadAll()
     * @generated
     */
	EOperation getImportContainer__LoadAll();

	/**
     * Returns the meta object for class '{@link machine.Import <em>Import</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Import</em>'.
     * @see machine.Import
     * @generated
     */
	EClass getImport();

	/**
     * Returns the meta object for the attribute '{@link machine.Import#getImportURI <em>Import URI</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Import URI</em>'.
     * @see machine.Import#getImportURI()
     * @see #getImport()
     * @generated
     */
	EAttribute getImport_ImportURI();

	/**
     * Returns the meta object for the '{@link machine.Import#load() <em>Load</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Load</em>' operation.
     * @see machine.Import#load()
     * @generated
     */
	EOperation getImport__Load();

	/**
     * Returns the meta object for class '{@link machine.Profile <em>Profile</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Profile</em>'.
     * @see machine.Profile
     * @generated
     */
	EClass getProfile();

	/**
     * Returns the meta object for the attribute '{@link machine.Profile#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.Profile#getName()
     * @see #getProfile()
     * @generated
     */
	EAttribute getProfile_Name();

	/**
     * Returns the meta object for the container reference '{@link machine.Profile#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Peripheral</em>'.
     * @see machine.Profile#getPeripheral()
     * @see #getProfile()
     * @generated
     */
	EReference getProfile_Peripheral();

	/**
     * Returns the meta object for class '{@link machine.Axis <em>Axis</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Axis</em>'.
     * @see machine.Axis
     * @generated
     */
	EClass getAxis();

	/**
     * Returns the meta object for the attribute '{@link machine.Axis#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.Axis#getName()
     * @see #getAxis()
     * @generated
     */
	EAttribute getAxis_Name();

	/**
     * Returns the meta object for the reference list '{@link machine.Axis#getSetPoints <em>Set Points</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Set Points</em>'.
     * @see machine.Axis#getSetPoints()
     * @see #getAxis()
     * @generated
     */
	EReference getAxis_SetPoints();

	/**
     * Returns the meta object for the attribute '{@link machine.Axis#getUnit <em>Unit</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Unit</em>'.
     * @see machine.Axis#getUnit()
     * @see #getAxis()
     * @generated
     */
	EAttribute getAxis_Unit();

	/**
     * Returns the meta object for class '{@link machine.Position <em>Position</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Position</em>'.
     * @see machine.Position
     * @generated
     */
	EClass getPosition();

	/**
     * Returns the meta object for the attribute '{@link machine.Position#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.Position#getName()
     * @see #getPosition()
     * @generated
     */
	EAttribute getPosition_Name();

	/**
     * Returns the meta object for class '{@link machine.SetPoint <em>Set Point</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Set Point</em>'.
     * @see machine.SetPoint
     * @generated
     */
	EClass getSetPoint();

	/**
     * Returns the meta object for the reference list '{@link machine.SetPoint#getAxes <em>Axes</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Axes</em>'.
     * @see machine.SetPoint#getAxes()
     * @see #getSetPoint()
     * @generated
     */
	EReference getSetPoint_Axes();

	/**
     * Returns the meta object for the attribute '{@link machine.SetPoint#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.SetPoint#getName()
     * @see #getSetPoint()
     * @generated
     */
	EAttribute getSetPoint_Name();

	/**
     * Returns the meta object for the attribute '{@link machine.SetPoint#getUnit <em>Unit</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Unit</em>'.
     * @see machine.SetPoint#getUnit()
     * @see #getSetPoint()
     * @generated
     */
	EAttribute getSetPoint_Unit();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Axis Position Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Axis Position Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model features="value key" 
     *        valueType="machine.Position" valueRequired="true"
     *        keyType="machine.Axis" keyRequired="true"
     * @generated
     */
	EClass getAxisPositionMapEntry();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getAxisPositionMapEntry()
     * @generated
     */
	EReference getAxisPositionMapEntry_Value();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getAxisPositionMapEntry()
     * @generated
     */
	EReference getAxisPositionMapEntry_Key();

	/**
     * Returns the meta object for class '{@link java.util.Map.Entry <em>Axis Positions Map Entry</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Axis Positions Map Entry</em>'.
     * @see java.util.Map.Entry
     * @model keyType="machine.Axis" keyRequired="true"
     *        valueType="machine.Position" valueContainment="true" valueMany="true"
     * @generated
     */
	EClass getAxisPositionsMapEntry();

	/**
     * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Key</em>'.
     * @see java.util.Map.Entry
     * @see #getAxisPositionsMapEntry()
     * @generated
     */
	EReference getAxisPositionsMapEntry_Key();

	/**
     * Returns the meta object for the containment reference list '{@link java.util.Map.Entry <em>Value</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Value</em>'.
     * @see java.util.Map.Entry
     * @see #getAxisPositionsMapEntry()
     * @generated
     */
	EReference getAxisPositionsMapEntry_Value();

	/**
     * Returns the meta object for class '{@link machine.PathTargetReference <em>Path Target Reference</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Path Target Reference</em>'.
     * @see machine.PathTargetReference
     * @generated
     */
	EClass getPathTargetReference();

	/**
     * Returns the meta object for the attribute '{@link machine.PathTargetReference#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.PathTargetReference#getName()
     * @see #getPathTargetReference()
     * @generated
     */
	EAttribute getPathTargetReference_Name();

	/**
     * Returns the meta object for the reference '{@link machine.PathTargetReference#getPosition <em>Position</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Position</em>'.
     * @see machine.PathTargetReference#getPosition()
     * @see #getPathTargetReference()
     * @generated
     */
	EReference getPathTargetReference_Position();

	/**
     * Returns the meta object for class '{@link machine.UnidirectionalPath <em>Unidirectional Path</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Unidirectional Path</em>'.
     * @see machine.UnidirectionalPath
     * @generated
     */
	EClass getUnidirectionalPath();

	/**
     * Returns the meta object for the reference '{@link machine.UnidirectionalPath#getSource <em>Source</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Source</em>'.
     * @see machine.UnidirectionalPath#getSource()
     * @see #getUnidirectionalPath()
     * @generated
     */
	EReference getUnidirectionalPath_Source();

	/**
     * Returns the meta object for the containment reference '{@link machine.UnidirectionalPath#getTarget <em>Target</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference '<em>Target</em>'.
     * @see machine.UnidirectionalPath#getTarget()
     * @see #getUnidirectionalPath()
     * @generated
     */
	EReference getUnidirectionalPath_Target();

	/**
     * Returns the meta object for class '{@link machine.BidirectionalPath <em>Bidirectional Path</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Bidirectional Path</em>'.
     * @see machine.BidirectionalPath
     * @generated
     */
	EClass getBidirectionalPath();

	/**
     * Returns the meta object for the containment reference list '{@link machine.BidirectionalPath#getEndPoints <em>End Points</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>End Points</em>'.
     * @see machine.BidirectionalPath#getEndPoints()
     * @see #getBidirectionalPath()
     * @generated
     */
	EReference getBidirectionalPath_EndPoints();

	/**
     * Returns the meta object for class '{@link machine.FullMeshPath <em>Full Mesh Path</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Full Mesh Path</em>'.
     * @see machine.FullMeshPath
     * @generated
     */
	EClass getFullMeshPath();

	/**
     * Returns the meta object for the containment reference list '{@link machine.FullMeshPath#getEndPoints <em>End Points</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>End Points</em>'.
     * @see machine.FullMeshPath#getEndPoints()
     * @see #getFullMeshPath()
     * @generated
     */
	EReference getFullMeshPath_EndPoints();

	/**
     * Returns the meta object for class '{@link machine.PathAnnotation <em>Path Annotation</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Path Annotation</em>'.
     * @see machine.PathAnnotation
     * @generated
     */
	EClass getPathAnnotation();

	/**
     * Returns the meta object for the attribute '{@link machine.PathAnnotation#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.PathAnnotation#getName()
     * @see #getPathAnnotation()
     * @generated
     */
	EAttribute getPathAnnotation_Name();

	/**
     * Returns the meta object for the reference list '{@link machine.PathAnnotation#getPaths <em>Paths</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Paths</em>'.
     * @see machine.PathAnnotation#getPaths()
     * @see #getPathAnnotation()
     * @generated
     */
	EReference getPathAnnotation_Paths();

	/**
     * Returns the meta object for class '{@link machine.ResourceItem <em>Resource Item</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Resource Item</em>'.
     * @see machine.ResourceItem
     * @generated
     */
	EClass getResourceItem();

	/**
     * Returns the meta object for the container reference '{@link machine.ResourceItem#getResource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Resource</em>'.
     * @see machine.ResourceItem#getResource()
     * @see #getResourceItem()
     * @generated
     */
	EReference getResourceItem_Resource();

	/**
     * Returns the meta object for the '{@link machine.ResourceItem#fqn() <em>Fqn</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Fqn</em>' operation.
     * @see machine.ResourceItem#fqn()
     * @generated
     */
	EOperation getResourceItem__Fqn();

	/**
     * Returns the meta object for class '{@link machine.IResource <em>IResource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>IResource</em>'.
     * @see machine.IResource
     * @generated
     */
	EClass getIResource();

	/**
     * Returns the meta object for the attribute '{@link machine.IResource#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.IResource#getName()
     * @see #getIResource()
     * @generated
     */
	EAttribute getIResource_Name();

	/**
     * Returns the meta object for the '{@link machine.IResource#getResource() <em>Get Resource</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Resource</em>' operation.
     * @see machine.IResource#getResource()
     * @generated
     */
	EOperation getIResource__GetResource();

	/**
     * Returns the meta object for the '{@link machine.IResource#fqn() <em>Fqn</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Fqn</em>' operation.
     * @see machine.IResource#fqn()
     * @generated
     */
	EOperation getIResource__Fqn();

	/**
     * Returns the meta object for class '{@link machine.Distance <em>Distance</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Distance</em>'.
     * @see machine.Distance
     * @generated
     */
	EClass getDistance();

	/**
     * Returns the meta object for the container reference '{@link machine.Distance#getPeripheral <em>Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the container reference '<em>Peripheral</em>'.
     * @see machine.Distance#getPeripheral()
     * @see #getDistance()
     * @generated
     */
	EReference getDistance_Peripheral();

	/**
     * Returns the meta object for the attribute '{@link machine.Distance#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see machine.Distance#getName()
     * @see #getDistance()
     * @generated
     */
	EAttribute getDistance_Name();

	/**
     * Returns the meta object for class '{@link machine.HasResourcePeripheral <em>Has Resource Peripheral</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Has Resource Peripheral</em>'.
     * @see machine.HasResourcePeripheral
     * @generated
     */
	EClass getHasResourcePeripheral();

	/**
     * Returns the meta object for the '{@link machine.HasResourcePeripheral#fqn() <em>Fqn</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Fqn</em>' operation.
     * @see machine.HasResourcePeripheral#fqn()
     * @generated
     */
	EOperation getHasResourcePeripheral__Fqn();

	/**
     * Returns the meta object for the '{@link machine.HasResourcePeripheral#getResource() <em>Get Resource</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Resource</em>' operation.
     * @see machine.HasResourcePeripheral#getResource()
     * @generated
     */
	EOperation getHasResourcePeripheral__GetResource();

	/**
     * Returns the meta object for the '{@link machine.HasResourcePeripheral#getPeripheral() <em>Get Peripheral</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Peripheral</em>' operation.
     * @see machine.HasResourcePeripheral#getPeripheral()
     * @generated
     */
	EOperation getHasResourcePeripheral__GetPeripheral();

	/**
     * Returns the meta object for the '{@link machine.HasResourcePeripheral#rpEquals(machine.HasResourcePeripheral) <em>Rp Equals</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Rp Equals</em>' operation.
     * @see machine.HasResourcePeripheral#rpEquals(machine.HasResourcePeripheral)
     * @generated
     */
	EOperation getHasResourcePeripheral__RpEquals__HasResourcePeripheral();

	/**
     * Returns the meta object for class '{@link machine.HasSettling <em>Has Settling</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Has Settling</em>'.
     * @see machine.HasSettling
     * @generated
     */
	EClass getHasSettling();

	/**
     * Returns the meta object for the reference list '{@link machine.HasSettling#getSettling <em>Settling</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Settling</em>'.
     * @see machine.HasSettling#getSettling()
     * @see #getHasSettling()
     * @generated
     */
	EReference getHasSettling_Settling();

	/**
     * Returns the meta object for enum '{@link machine.ResourceType <em>Resource Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for enum '<em>Resource Type</em>'.
     * @see machine.ResourceType
     * @generated
     */
    EEnum getResourceType();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	MachineFactory getMachineFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link machine.impl.PeripheralTypeImpl <em>Peripheral Type</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.PeripheralTypeImpl
         * @see machine.impl.MachinePackageImpl#getPeripheralType()
         * @generated
         */
		EClass PERIPHERAL_TYPE = eINSTANCE.getPeripheralType();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERIPHERAL_TYPE__NAME = eINSTANCE.getPeripheralType_Name();

		/**
         * The meta object literal for the '<em><b>Conversion</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERIPHERAL_TYPE__CONVERSION = eINSTANCE.getPeripheralType_Conversion();

		/**
         * The meta object literal for the '<em><b>Axes</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL_TYPE__AXES = eINSTANCE.getPeripheralType_Axes();

		/**
         * The meta object literal for the '<em><b>Set Points</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL_TYPE__SET_POINTS = eINSTANCE.getPeripheralType_SetPoints();

		/**
         * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL_TYPE__ACTIONS = eINSTANCE.getPeripheralType_Actions();

		/**
         * The meta object literal for the '{@link machine.impl.PathImpl <em>Path</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.PathImpl
         * @see machine.impl.MachinePackageImpl#getPath()
         * @generated
         */
		EClass PATH = eINSTANCE.getPath();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PATH__NAME = eINSTANCE.getPath_Name();

		/**
         * The meta object literal for the '<em><b>Profiles</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PATH__PROFILES = eINSTANCE.getPath_Profiles();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PATH__PERIPHERAL = eINSTANCE.getPath_Peripheral();

		/**
         * The meta object literal for the '<em><b>Annotations</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PATH__ANNOTATIONS = eINSTANCE.getPath_Annotations();

		/**
         * The meta object literal for the '<em><b>Get Sources</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation PATH___GET_SOURCES = eINSTANCE.getPath__GetSources();

		/**
         * The meta object literal for the '<em><b>Get Targets</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation PATH___GET_TARGETS = eINSTANCE.getPath__GetTargets();

		/**
         * The meta object literal for the '{@link machine.impl.SymbolicPositionImpl <em>Symbolic Position</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.SymbolicPositionImpl
         * @see machine.impl.MachinePackageImpl#getSymbolicPosition()
         * @generated
         */
		EClass SYMBOLIC_POSITION = eINSTANCE.getSymbolicPosition();

		/**
         * The meta object literal for the '<em><b>Axis Position</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SYMBOLIC_POSITION__AXIS_POSITION = eINSTANCE.getSymbolicPosition_AxisPosition();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SYMBOLIC_POSITION__PERIPHERAL = eINSTANCE.getSymbolicPosition_Peripheral();

		/**
         * The meta object literal for the '<em><b>Target References</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SYMBOLIC_POSITION__TARGET_REFERENCES = eINSTANCE.getSymbolicPosition_TargetReferences();

		/**
         * The meta object literal for the '<em><b>Source References</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SYMBOLIC_POSITION__SOURCE_REFERENCES = eINSTANCE.getSymbolicPosition_SourceReferences();

		/**
         * The meta object literal for the '<em><b>Get Position</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation SYMBOLIC_POSITION___GET_POSITION__AXIS = eINSTANCE.getSymbolicPosition__GetPosition__Axis();

		/**
         * The meta object literal for the '<em><b>Get Outgoing Paths</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation SYMBOLIC_POSITION___GET_OUTGOING_PATHS = eINSTANCE.getSymbolicPosition__GetOutgoingPaths();

		/**
         * The meta object literal for the '{@link machine.impl.ResourceImpl <em>Resource</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.ResourceImpl
         * @see machine.impl.MachinePackageImpl#getResource()
         * @generated
         */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
         * The meta object literal for the '<em><b>Peripherals</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference RESOURCE__PERIPHERALS = eINSTANCE.getResource_Peripherals();

		/**
         * The meta object literal for the '<em><b>Items</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference RESOURCE__ITEMS = eINSTANCE.getResource_Items();

		/**
         * The meta object literal for the '<em><b>Resource Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute RESOURCE__RESOURCE_TYPE = eINSTANCE.getResource_ResourceType();

        /**
         * The meta object literal for the '<em><b>Get Resource</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation RESOURCE___GET_RESOURCE = eINSTANCE.getResource__GetResource();

		/**
         * The meta object literal for the '<em><b>Fqn</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation RESOURCE___FQN = eINSTANCE.getResource__Fqn();

		/**
         * The meta object literal for the '{@link machine.impl.ActionTypeImpl <em>Action Type</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.ActionTypeImpl
         * @see machine.impl.MachinePackageImpl#getActionType()
         * @generated
         */
		EClass ACTION_TYPE = eINSTANCE.getActionType();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute ACTION_TYPE__NAME = eINSTANCE.getActionType_Name();

		/**
         * The meta object literal for the '{@link machine.impl.PeripheralImpl <em>Peripheral</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.PeripheralImpl
         * @see machine.impl.MachinePackageImpl#getPeripheral()
         * @generated
         */
		EClass PERIPHERAL = eINSTANCE.getPeripheral();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PERIPHERAL__NAME = eINSTANCE.getPeripheral_Name();

		/**
         * The meta object literal for the '<em><b>Type</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL__TYPE = eINSTANCE.getPeripheral_Type();

		/**
         * The meta object literal for the '<em><b>Axis Positions</b></em>' map feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL__AXIS_POSITIONS = eINSTANCE.getPeripheral_AxisPositions();

		/**
         * The meta object literal for the '<em><b>Positions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL__POSITIONS = eINSTANCE.getPeripheral_Positions();

		/**
         * The meta object literal for the '<em><b>Resource</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL__RESOURCE = eINSTANCE.getPeripheral_Resource();

		/**
         * The meta object literal for the '<em><b>Paths</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL__PATHS = eINSTANCE.getPeripheral_Paths();

		/**
         * The meta object literal for the '<em><b>Profiles</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL__PROFILES = eINSTANCE.getPeripheral_Profiles();

		/**
         * The meta object literal for the '<em><b>Distances</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PERIPHERAL__DISTANCES = eINSTANCE.getPeripheral_Distances();

		/**
         * The meta object literal for the '<em><b>Fqn</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation PERIPHERAL___FQN = eINSTANCE.getPeripheral__Fqn();

		/**
         * The meta object literal for the '{@link machine.impl.MachineImpl <em>Machine</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.MachineImpl
         * @see machine.impl.MachinePackageImpl#getMachine()
         * @generated
         */
		EClass MACHINE = eINSTANCE.getMachine();

		/**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute MACHINE__TYPE = eINSTANCE.getMachine_Type();

		/**
         * The meta object literal for the '<em><b>Path Annotations</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MACHINE__PATH_ANNOTATIONS = eINSTANCE.getMachine_PathAnnotations();

		/**
         * The meta object literal for the '<em><b>Resources</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MACHINE__RESOURCES = eINSTANCE.getMachine_Resources();

		/**
         * The meta object literal for the '<em><b>Peripheral Types</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference MACHINE__PERIPHERAL_TYPES = eINSTANCE.getMachine_PeripheralTypes();

		/**
         * The meta object literal for the '{@link machine.impl.ImportContainerImpl <em>Import Container</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.ImportContainerImpl
         * @see machine.impl.MachinePackageImpl#getImportContainer()
         * @generated
         */
		EClass IMPORT_CONTAINER = eINSTANCE.getImportContainer();

		/**
         * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference IMPORT_CONTAINER__IMPORTS = eINSTANCE.getImportContainer_Imports();

		/**
         * The meta object literal for the '<em><b>Load All</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation IMPORT_CONTAINER___LOAD_ALL = eINSTANCE.getImportContainer__LoadAll();

		/**
         * The meta object literal for the '{@link machine.impl.ImportImpl <em>Import</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.ImportImpl
         * @see machine.impl.MachinePackageImpl#getImport()
         * @generated
         */
		EClass IMPORT = eINSTANCE.getImport();

		/**
         * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

		/**
         * The meta object literal for the '<em><b>Load</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation IMPORT___LOAD = eINSTANCE.getImport__Load();

		/**
         * The meta object literal for the '{@link machine.impl.ProfileImpl <em>Profile</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.ProfileImpl
         * @see machine.impl.MachinePackageImpl#getProfile()
         * @generated
         */
		EClass PROFILE = eINSTANCE.getProfile();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PROFILE__NAME = eINSTANCE.getProfile_Name();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PROFILE__PERIPHERAL = eINSTANCE.getProfile_Peripheral();

		/**
         * The meta object literal for the '{@link machine.impl.AxisImpl <em>Axis</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.AxisImpl
         * @see machine.impl.MachinePackageImpl#getAxis()
         * @generated
         */
		EClass AXIS = eINSTANCE.getAxis();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute AXIS__NAME = eINSTANCE.getAxis_Name();

		/**
         * The meta object literal for the '<em><b>Set Points</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference AXIS__SET_POINTS = eINSTANCE.getAxis_SetPoints();

		/**
         * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute AXIS__UNIT = eINSTANCE.getAxis_Unit();

		/**
         * The meta object literal for the '{@link machine.impl.PositionImpl <em>Position</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.PositionImpl
         * @see machine.impl.MachinePackageImpl#getPosition()
         * @generated
         */
		EClass POSITION = eINSTANCE.getPosition();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute POSITION__NAME = eINSTANCE.getPosition_Name();

		/**
         * The meta object literal for the '{@link machine.impl.SetPointImpl <em>Set Point</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.SetPointImpl
         * @see machine.impl.MachinePackageImpl#getSetPoint()
         * @generated
         */
		EClass SET_POINT = eINSTANCE.getSetPoint();

		/**
         * The meta object literal for the '<em><b>Axes</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference SET_POINT__AXES = eINSTANCE.getSetPoint_Axes();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute SET_POINT__NAME = eINSTANCE.getSetPoint_Name();

		/**
         * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute SET_POINT__UNIT = eINSTANCE.getSetPoint_Unit();

		/**
         * The meta object literal for the '{@link machine.impl.AxisPositionMapEntryImpl <em>Axis Position Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.AxisPositionMapEntryImpl
         * @see machine.impl.MachinePackageImpl#getAxisPositionMapEntry()
         * @generated
         */
		EClass AXIS_POSITION_MAP_ENTRY = eINSTANCE.getAxisPositionMapEntry();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference AXIS_POSITION_MAP_ENTRY__VALUE = eINSTANCE.getAxisPositionMapEntry_Value();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference AXIS_POSITION_MAP_ENTRY__KEY = eINSTANCE.getAxisPositionMapEntry_Key();

		/**
         * The meta object literal for the '{@link machine.impl.AxisPositionsMapEntryImpl <em>Axis Positions Map Entry</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.AxisPositionsMapEntryImpl
         * @see machine.impl.MachinePackageImpl#getAxisPositionsMapEntry()
         * @generated
         */
		EClass AXIS_POSITIONS_MAP_ENTRY = eINSTANCE.getAxisPositionsMapEntry();

		/**
         * The meta object literal for the '<em><b>Key</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference AXIS_POSITIONS_MAP_ENTRY__KEY = eINSTANCE.getAxisPositionsMapEntry_Key();

		/**
         * The meta object literal for the '<em><b>Value</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference AXIS_POSITIONS_MAP_ENTRY__VALUE = eINSTANCE.getAxisPositionsMapEntry_Value();

		/**
         * The meta object literal for the '{@link machine.impl.PathTargetReferenceImpl <em>Path Target Reference</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.PathTargetReferenceImpl
         * @see machine.impl.MachinePackageImpl#getPathTargetReference()
         * @generated
         */
		EClass PATH_TARGET_REFERENCE = eINSTANCE.getPathTargetReference();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PATH_TARGET_REFERENCE__NAME = eINSTANCE.getPathTargetReference_Name();

		/**
         * The meta object literal for the '<em><b>Position</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PATH_TARGET_REFERENCE__POSITION = eINSTANCE.getPathTargetReference_Position();

		/**
         * The meta object literal for the '{@link machine.impl.UnidirectionalPathImpl <em>Unidirectional Path</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.UnidirectionalPathImpl
         * @see machine.impl.MachinePackageImpl#getUnidirectionalPath()
         * @generated
         */
		EClass UNIDIRECTIONAL_PATH = eINSTANCE.getUnidirectionalPath();

		/**
         * The meta object literal for the '<em><b>Source</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference UNIDIRECTIONAL_PATH__SOURCE = eINSTANCE.getUnidirectionalPath_Source();

		/**
         * The meta object literal for the '<em><b>Target</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference UNIDIRECTIONAL_PATH__TARGET = eINSTANCE.getUnidirectionalPath_Target();

		/**
         * The meta object literal for the '{@link machine.impl.BidirectionalPathImpl <em>Bidirectional Path</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.BidirectionalPathImpl
         * @see machine.impl.MachinePackageImpl#getBidirectionalPath()
         * @generated
         */
		EClass BIDIRECTIONAL_PATH = eINSTANCE.getBidirectionalPath();

		/**
         * The meta object literal for the '<em><b>End Points</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference BIDIRECTIONAL_PATH__END_POINTS = eINSTANCE.getBidirectionalPath_EndPoints();

		/**
         * The meta object literal for the '{@link machine.impl.FullMeshPathImpl <em>Full Mesh Path</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.FullMeshPathImpl
         * @see machine.impl.MachinePackageImpl#getFullMeshPath()
         * @generated
         */
		EClass FULL_MESH_PATH = eINSTANCE.getFullMeshPath();

		/**
         * The meta object literal for the '<em><b>End Points</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference FULL_MESH_PATH__END_POINTS = eINSTANCE.getFullMeshPath_EndPoints();

		/**
         * The meta object literal for the '{@link machine.impl.PathAnnotationImpl <em>Path Annotation</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.PathAnnotationImpl
         * @see machine.impl.MachinePackageImpl#getPathAnnotation()
         * @generated
         */
		EClass PATH_ANNOTATION = eINSTANCE.getPathAnnotation();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute PATH_ANNOTATION__NAME = eINSTANCE.getPathAnnotation_Name();

		/**
         * The meta object literal for the '<em><b>Paths</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference PATH_ANNOTATION__PATHS = eINSTANCE.getPathAnnotation_Paths();

		/**
         * The meta object literal for the '{@link machine.impl.ResourceItemImpl <em>Resource Item</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.ResourceItemImpl
         * @see machine.impl.MachinePackageImpl#getResourceItem()
         * @generated
         */
		EClass RESOURCE_ITEM = eINSTANCE.getResourceItem();

		/**
         * The meta object literal for the '<em><b>Resource</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference RESOURCE_ITEM__RESOURCE = eINSTANCE.getResourceItem_Resource();

		/**
         * The meta object literal for the '<em><b>Fqn</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation RESOURCE_ITEM___FQN = eINSTANCE.getResourceItem__Fqn();

		/**
         * The meta object literal for the '{@link machine.IResource <em>IResource</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.IResource
         * @see machine.impl.MachinePackageImpl#getIResource()
         * @generated
         */
		EClass IRESOURCE = eINSTANCE.getIResource();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute IRESOURCE__NAME = eINSTANCE.getIResource_Name();

		/**
         * The meta object literal for the '<em><b>Get Resource</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation IRESOURCE___GET_RESOURCE = eINSTANCE.getIResource__GetResource();

		/**
         * The meta object literal for the '<em><b>Fqn</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation IRESOURCE___FQN = eINSTANCE.getIResource__Fqn();

		/**
         * The meta object literal for the '{@link machine.impl.DistanceImpl <em>Distance</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.DistanceImpl
         * @see machine.impl.MachinePackageImpl#getDistance()
         * @generated
         */
		EClass DISTANCE = eINSTANCE.getDistance();

		/**
         * The meta object literal for the '<em><b>Peripheral</b></em>' container reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference DISTANCE__PERIPHERAL = eINSTANCE.getDistance_Peripheral();

		/**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute DISTANCE__NAME = eINSTANCE.getDistance_Name();

		/**
         * The meta object literal for the '{@link machine.HasResourcePeripheral <em>Has Resource Peripheral</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.HasResourcePeripheral
         * @see machine.impl.MachinePackageImpl#getHasResourcePeripheral()
         * @generated
         */
		EClass HAS_RESOURCE_PERIPHERAL = eINSTANCE.getHasResourcePeripheral();

		/**
         * The meta object literal for the '<em><b>Fqn</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation HAS_RESOURCE_PERIPHERAL___FQN = eINSTANCE.getHasResourcePeripheral__Fqn();

		/**
         * The meta object literal for the '<em><b>Get Resource</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation HAS_RESOURCE_PERIPHERAL___GET_RESOURCE = eINSTANCE.getHasResourcePeripheral__GetResource();

		/**
         * The meta object literal for the '<em><b>Get Peripheral</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation HAS_RESOURCE_PERIPHERAL___GET_PERIPHERAL = eINSTANCE.getHasResourcePeripheral__GetPeripheral();

		/**
         * The meta object literal for the '<em><b>Rp Equals</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation HAS_RESOURCE_PERIPHERAL___RP_EQUALS__HASRESOURCEPERIPHERAL = eINSTANCE.getHasResourcePeripheral__RpEquals__HasResourcePeripheral();

		/**
         * The meta object literal for the '{@link machine.impl.HasSettlingImpl <em>Has Settling</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see machine.impl.HasSettlingImpl
         * @see machine.impl.MachinePackageImpl#getHasSettling()
         * @generated
         */
		EClass HAS_SETTLING = eINSTANCE.getHasSettling();

		/**
         * The meta object literal for the '<em><b>Settling</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference HAS_SETTLING__SETTLING = eINSTANCE.getHasSettling_Settling();

        /**
         * The meta object literal for the '{@link machine.ResourceType <em>Resource Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see machine.ResourceType
         * @see machine.impl.MachinePackageImpl#getResourceType()
         * @generated
         */
        EEnum RESOURCE_TYPE = eINSTANCE.getResourceType();

	}

} //MachinePackage
