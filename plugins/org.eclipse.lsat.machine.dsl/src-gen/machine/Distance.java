/**
 */
package machine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Distance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.Distance#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link machine.Distance#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getDistance()
 * @model
 * @generated
 */
public interface Distance extends HasSettling {
	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link machine.Peripheral#getDistances <em>Distances</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' container reference.
     * @see #setPeripheral(Peripheral)
     * @see machine.MachinePackage#getDistance_Peripheral()
     * @see machine.Peripheral#getDistances
     * @model opposite="distances" required="true" transient="false"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link machine.Distance#getPeripheral <em>Peripheral</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' container reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

	/**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see machine.MachinePackage#getDistance_Name()
     * @model required="true"
     * @generated
     */
	String getName();

	/**
     * Sets the value of the '{@link machine.Distance#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
	void setName(String value);

} // Distance
