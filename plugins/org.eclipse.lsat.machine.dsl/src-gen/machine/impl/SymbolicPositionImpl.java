/**
 */
package machine.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import machine.Axis;
import machine.MachinePackage;
import machine.Path;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.Position;
import machine.SymbolicPosition;
import machine.UnidirectionalPath;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Symbolic Position</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.SymbolicPositionImpl#getAxisPosition <em>Axis Position</em>}</li>
 *   <li>{@link machine.impl.SymbolicPositionImpl#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link machine.impl.SymbolicPositionImpl#getTargetReferences <em>Target References</em>}</li>
 *   <li>{@link machine.impl.SymbolicPositionImpl#getSourceReferences <em>Source References</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SymbolicPositionImpl extends PositionImpl implements SymbolicPosition {
	/**
     * The cached value of the '{@link #getAxisPosition() <em>Axis Position</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAxisPosition()
     * @generated
     * @ordered
     */
	protected EMap<Axis, Position> axisPosition;

	/**
     * The cached value of the '{@link #getTargetReferences() <em>Target References</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTargetReferences()
     * @generated
     * @ordered
     */
	protected EList<PathTargetReference> targetReferences;

	/**
     * The cached value of the '{@link #getSourceReferences() <em>Source References</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSourceReferences()
     * @generated
     * @ordered
     */
	protected EList<UnidirectionalPath> sourceReferences;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected SymbolicPositionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.SYMBOLIC_POSITION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<Axis, Position> getAxisPosition() {
        if (axisPosition == null)
        {
            axisPosition = new EcoreEMap<Axis,Position>(MachinePackage.Literals.AXIS_POSITION_MAP_ENTRY, AxisPositionMapEntryImpl.class, this, MachinePackage.SYMBOLIC_POSITION__AXIS_POSITION);
        }
        return axisPosition;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Peripheral getPeripheral() {
        if (eContainerFeatureID() != MachinePackage.SYMBOLIC_POSITION__PERIPHERAL) return null;
        return (Peripheral)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetPeripheral(Peripheral newPeripheral, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newPeripheral, MachinePackage.SYMBOLIC_POSITION__PERIPHERAL, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPeripheral(Peripheral newPeripheral) {
        if (newPeripheral != eInternalContainer() || (eContainerFeatureID() != MachinePackage.SYMBOLIC_POSITION__PERIPHERAL && newPeripheral != null))
        {
            if (EcoreUtil.isAncestor(this, newPeripheral))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newPeripheral != null)
                msgs = ((InternalEObject)newPeripheral).eInverseAdd(this, MachinePackage.PERIPHERAL__POSITIONS, Peripheral.class, msgs);
            msgs = basicSetPeripheral(newPeripheral, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.SYMBOLIC_POSITION__PERIPHERAL, newPeripheral, newPeripheral));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<PathTargetReference> getTargetReferences() {
        if (targetReferences == null)
        {
            targetReferences = new EObjectWithInverseEList<PathTargetReference>(PathTargetReference.class, this, MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES, MachinePackage.PATH_TARGET_REFERENCE__POSITION);
        }
        return targetReferences;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<UnidirectionalPath> getSourceReferences() {
        if (sourceReferences == null)
        {
            sourceReferences = new EObjectWithInverseEList<UnidirectionalPath>(UnidirectionalPath.class, this, MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES, MachinePackage.UNIDIRECTIONAL_PATH__SOURCE);
        }
        return sourceReferences;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Position getPosition(final Axis axis) {
        return getAxisPosition().containsKey(axis) ? getAxisPosition().get(axis) : this;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Path> getOutgoingPaths() {
        org.eclipse.emf.common.util.BasicEList<Path> outgoingPaths = 
            new org.eclipse.emf.common.util.BasicEList<Path>(getSourceReferences().size() + getTargetReferences().size());
        outgoingPaths.addAll(sourceReferences);
        for (PathTargetReference ref : targetReferences) {
            org.eclipse.emf.ecore.EStructuralFeature feature = ref.eContainingFeature();
            if (feature == machine.MachinePackage.Literals.BIDIRECTIONAL_PATH__END_POINTS
                    || feature == machine.MachinePackage.Literals.FULL_MESH_PATH__END_POINTS) {
                outgoingPaths.add((Path)ref.eContainer());
            }
        }
        return org.eclipse.emf.common.util.ECollections.unmodifiableEList(outgoingPaths);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.SYMBOLIC_POSITION__PERIPHERAL:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetPeripheral((Peripheral)otherEnd, msgs);
            case MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getTargetReferences()).basicAdd(otherEnd, msgs);
            case MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getSourceReferences()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.SYMBOLIC_POSITION__AXIS_POSITION:
                return ((InternalEList<?>)getAxisPosition()).basicRemove(otherEnd, msgs);
            case MachinePackage.SYMBOLIC_POSITION__PERIPHERAL:
                return basicSetPeripheral(null, msgs);
            case MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES:
                return ((InternalEList<?>)getTargetReferences()).basicRemove(otherEnd, msgs);
            case MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES:
                return ((InternalEList<?>)getSourceReferences()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID())
        {
            case MachinePackage.SYMBOLIC_POSITION__PERIPHERAL:
                return eInternalContainer().eInverseRemove(this, MachinePackage.PERIPHERAL__POSITIONS, Peripheral.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.SYMBOLIC_POSITION__AXIS_POSITION:
                if (coreType) return getAxisPosition();
                else return getAxisPosition().map();
            case MachinePackage.SYMBOLIC_POSITION__PERIPHERAL:
                return getPeripheral();
            case MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES:
                return getTargetReferences();
            case MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES:
                return getSourceReferences();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.SYMBOLIC_POSITION__AXIS_POSITION:
                ((EStructuralFeature.Setting)getAxisPosition()).set(newValue);
                return;
            case MachinePackage.SYMBOLIC_POSITION__PERIPHERAL:
                setPeripheral((Peripheral)newValue);
                return;
            case MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES:
                getTargetReferences().clear();
                getTargetReferences().addAll((Collection<? extends PathTargetReference>)newValue);
                return;
            case MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES:
                getSourceReferences().clear();
                getSourceReferences().addAll((Collection<? extends UnidirectionalPath>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.SYMBOLIC_POSITION__AXIS_POSITION:
                getAxisPosition().clear();
                return;
            case MachinePackage.SYMBOLIC_POSITION__PERIPHERAL:
                setPeripheral((Peripheral)null);
                return;
            case MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES:
                getTargetReferences().clear();
                return;
            case MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES:
                getSourceReferences().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.SYMBOLIC_POSITION__AXIS_POSITION:
                return axisPosition != null && !axisPosition.isEmpty();
            case MachinePackage.SYMBOLIC_POSITION__PERIPHERAL:
                return getPeripheral() != null;
            case MachinePackage.SYMBOLIC_POSITION__TARGET_REFERENCES:
                return targetReferences != null && !targetReferences.isEmpty();
            case MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES:
                return sourceReferences != null && !sourceReferences.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case MachinePackage.SYMBOLIC_POSITION___GET_POSITION__AXIS:
                return getPosition((Axis)arguments.get(0));
            case MachinePackage.SYMBOLIC_POSITION___GET_OUTGOING_PATHS:
                return getOutgoingPaths();
        }
        return super.eInvoke(operationID, arguments);
    }

} //SymbolicPositionImpl
