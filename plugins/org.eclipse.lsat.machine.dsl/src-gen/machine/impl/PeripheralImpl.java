/**
 */
package machine.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import machine.Axis;
import machine.Distance;
import machine.MachinePackage;
import machine.Path;
import machine.Peripheral;
import machine.PeripheralType;
import machine.Position;
import machine.Profile;
import machine.Resource;
import machine.SymbolicPosition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Peripheral</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.PeripheralImpl#getName <em>Name</em>}</li>
 *   <li>{@link machine.impl.PeripheralImpl#getType <em>Type</em>}</li>
 *   <li>{@link machine.impl.PeripheralImpl#getAxisPositions <em>Axis Positions</em>}</li>
 *   <li>{@link machine.impl.PeripheralImpl#getPositions <em>Positions</em>}</li>
 *   <li>{@link machine.impl.PeripheralImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link machine.impl.PeripheralImpl#getPaths <em>Paths</em>}</li>
 *   <li>{@link machine.impl.PeripheralImpl#getProfiles <em>Profiles</em>}</li>
 *   <li>{@link machine.impl.PeripheralImpl#getDistances <em>Distances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PeripheralImpl extends MinimalEObjectImpl.Container implements Peripheral {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected String name = NAME_EDEFAULT;

	/**
     * The cached value of the '{@link #getType() <em>Type</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getType()
     * @generated
     * @ordered
     */
	protected PeripheralType type;

	/**
     * The cached value of the '{@link #getAxisPositions() <em>Axis Positions</em>}' map.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAxisPositions()
     * @generated
     * @ordered
     */
	protected EMap<Axis, EList<Position>> axisPositions;

	/**
     * The cached value of the '{@link #getPositions() <em>Positions</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPositions()
     * @generated
     * @ordered
     */
	protected EList<SymbolicPosition> positions;

	/**
     * The cached value of the '{@link #getPaths() <em>Paths</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPaths()
     * @generated
     * @ordered
     */
	protected EList<Path> paths;

	/**
     * The cached value of the '{@link #getProfiles() <em>Profiles</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getProfiles()
     * @generated
     * @ordered
     */
	protected EList<Profile> profiles;

	/**
     * The cached value of the '{@link #getDistances() <em>Distances</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getDistances()
     * @generated
     * @ordered
     */
	protected EList<Distance> distances;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PeripheralImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.PERIPHERAL;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getName() {
        return name;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.PERIPHERAL__NAME, oldName, name));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PeripheralType getType() {
        if (type != null && type.eIsProxy())
        {
            InternalEObject oldType = (InternalEObject)type;
            type = (PeripheralType)eResolveProxy(oldType);
            if (type != oldType)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, MachinePackage.PERIPHERAL__TYPE, oldType, type));
            }
        }
        return type;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public PeripheralType basicGetType() {
        return type;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setType(PeripheralType newType) {
        PeripheralType oldType = type;
        type = newType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.PERIPHERAL__TYPE, oldType, type));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EMap<Axis, EList<Position>> getAxisPositions() {
        if (axisPositions == null)
        {
            axisPositions = new EcoreEMap<Axis,EList<Position>>(MachinePackage.Literals.AXIS_POSITIONS_MAP_ENTRY, AxisPositionsMapEntryImpl.class, this, MachinePackage.PERIPHERAL__AXIS_POSITIONS);
        }
        return axisPositions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<SymbolicPosition> getPositions() {
        if (positions == null)
        {
            positions = new EObjectContainmentWithInverseEList<SymbolicPosition>(SymbolicPosition.class, this, MachinePackage.PERIPHERAL__POSITIONS, MachinePackage.SYMBOLIC_POSITION__PERIPHERAL);
        }
        return positions;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Resource getResource() {
        if (eContainerFeatureID() != MachinePackage.PERIPHERAL__RESOURCE) return null;
        return (Resource)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetResource(Resource newResource, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newResource, MachinePackage.PERIPHERAL__RESOURCE, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setResource(Resource newResource) {
        if (newResource != eInternalContainer() || (eContainerFeatureID() != MachinePackage.PERIPHERAL__RESOURCE && newResource != null))
        {
            if (EcoreUtil.isAncestor(this, newResource))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newResource != null)
                msgs = ((InternalEObject)newResource).eInverseAdd(this, MachinePackage.RESOURCE__PERIPHERALS, Resource.class, msgs);
            msgs = basicSetResource(newResource, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.PERIPHERAL__RESOURCE, newResource, newResource));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Path> getPaths() {
        if (paths == null)
        {
            paths = new EObjectContainmentWithInverseEList<Path>(Path.class, this, MachinePackage.PERIPHERAL__PATHS, MachinePackage.PATH__PERIPHERAL);
        }
        return paths;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Profile> getProfiles() {
        if (profiles == null)
        {
            profiles = new EObjectContainmentWithInverseEList<Profile>(Profile.class, this, MachinePackage.PERIPHERAL__PROFILES, MachinePackage.PROFILE__PERIPHERAL);
        }
        return profiles;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Distance> getDistances() {
        if (distances == null)
        {
            distances = new EObjectContainmentWithInverseEList<Distance>(Distance.class, this, MachinePackage.PERIPHERAL__DISTANCES, MachinePackage.DISTANCE__PERIPHERAL);
        }
        return distances;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String fqn() {
        StringBuffer fqn = new StringBuffer();
        fqn.append(null == getResource() ? null : getResource().getName());
        fqn.append('.');
        fqn.append(name);
        return fqn.toString();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL__POSITIONS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getPositions()).basicAdd(otherEnd, msgs);
            case MachinePackage.PERIPHERAL__RESOURCE:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetResource((Resource)otherEnd, msgs);
            case MachinePackage.PERIPHERAL__PATHS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getPaths()).basicAdd(otherEnd, msgs);
            case MachinePackage.PERIPHERAL__PROFILES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getProfiles()).basicAdd(otherEnd, msgs);
            case MachinePackage.PERIPHERAL__DISTANCES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getDistances()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL__AXIS_POSITIONS:
                return ((InternalEList<?>)getAxisPositions()).basicRemove(otherEnd, msgs);
            case MachinePackage.PERIPHERAL__POSITIONS:
                return ((InternalEList<?>)getPositions()).basicRemove(otherEnd, msgs);
            case MachinePackage.PERIPHERAL__RESOURCE:
                return basicSetResource(null, msgs);
            case MachinePackage.PERIPHERAL__PATHS:
                return ((InternalEList<?>)getPaths()).basicRemove(otherEnd, msgs);
            case MachinePackage.PERIPHERAL__PROFILES:
                return ((InternalEList<?>)getProfiles()).basicRemove(otherEnd, msgs);
            case MachinePackage.PERIPHERAL__DISTANCES:
                return ((InternalEList<?>)getDistances()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID())
        {
            case MachinePackage.PERIPHERAL__RESOURCE:
                return eInternalContainer().eInverseRemove(this, MachinePackage.RESOURCE__PERIPHERALS, Resource.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL__NAME:
                return getName();
            case MachinePackage.PERIPHERAL__TYPE:
                if (resolve) return getType();
                return basicGetType();
            case MachinePackage.PERIPHERAL__AXIS_POSITIONS:
                if (coreType) return getAxisPositions();
                else return getAxisPositions().map();
            case MachinePackage.PERIPHERAL__POSITIONS:
                return getPositions();
            case MachinePackage.PERIPHERAL__RESOURCE:
                return getResource();
            case MachinePackage.PERIPHERAL__PATHS:
                return getPaths();
            case MachinePackage.PERIPHERAL__PROFILES:
                return getProfiles();
            case MachinePackage.PERIPHERAL__DISTANCES:
                return getDistances();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL__NAME:
                setName((String)newValue);
                return;
            case MachinePackage.PERIPHERAL__TYPE:
                setType((PeripheralType)newValue);
                return;
            case MachinePackage.PERIPHERAL__AXIS_POSITIONS:
                ((EStructuralFeature.Setting)getAxisPositions()).set(newValue);
                return;
            case MachinePackage.PERIPHERAL__POSITIONS:
                getPositions().clear();
                getPositions().addAll((Collection<? extends SymbolicPosition>)newValue);
                return;
            case MachinePackage.PERIPHERAL__RESOURCE:
                setResource((Resource)newValue);
                return;
            case MachinePackage.PERIPHERAL__PATHS:
                getPaths().clear();
                getPaths().addAll((Collection<? extends Path>)newValue);
                return;
            case MachinePackage.PERIPHERAL__PROFILES:
                getProfiles().clear();
                getProfiles().addAll((Collection<? extends Profile>)newValue);
                return;
            case MachinePackage.PERIPHERAL__DISTANCES:
                getDistances().clear();
                getDistances().addAll((Collection<? extends Distance>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL__NAME:
                setName(NAME_EDEFAULT);
                return;
            case MachinePackage.PERIPHERAL__TYPE:
                setType((PeripheralType)null);
                return;
            case MachinePackage.PERIPHERAL__AXIS_POSITIONS:
                getAxisPositions().clear();
                return;
            case MachinePackage.PERIPHERAL__POSITIONS:
                getPositions().clear();
                return;
            case MachinePackage.PERIPHERAL__RESOURCE:
                setResource((Resource)null);
                return;
            case MachinePackage.PERIPHERAL__PATHS:
                getPaths().clear();
                return;
            case MachinePackage.PERIPHERAL__PROFILES:
                getProfiles().clear();
                return;
            case MachinePackage.PERIPHERAL__DISTANCES:
                getDistances().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PERIPHERAL__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case MachinePackage.PERIPHERAL__TYPE:
                return type != null;
            case MachinePackage.PERIPHERAL__AXIS_POSITIONS:
                return axisPositions != null && !axisPositions.isEmpty();
            case MachinePackage.PERIPHERAL__POSITIONS:
                return positions != null && !positions.isEmpty();
            case MachinePackage.PERIPHERAL__RESOURCE:
                return getResource() != null;
            case MachinePackage.PERIPHERAL__PATHS:
                return paths != null && !paths.isEmpty();
            case MachinePackage.PERIPHERAL__PROFILES:
                return profiles != null && !profiles.isEmpty();
            case MachinePackage.PERIPHERAL__DISTANCES:
                return distances != null && !distances.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case MachinePackage.PERIPHERAL___FQN:
                return fqn();
        }
        return super.eInvoke(operationID, arguments);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(')');
        return result.toString();
    }

} //PeripheralImpl
