/**
 */
package machine.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import machine.MachinePackage;
import machine.Path;
import machine.PathAnnotation;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.Profile;
import machine.SymbolicPosition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.PathImpl#getName <em>Name</em>}</li>
 *   <li>{@link machine.impl.PathImpl#getProfiles <em>Profiles</em>}</li>
 *   <li>{@link machine.impl.PathImpl#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link machine.impl.PathImpl#getAnnotations <em>Annotations</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PathImpl extends MinimalEObjectImpl.Container implements Path {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
	protected static final String NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getProfiles() <em>Profiles</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getProfiles()
     * @generated
     * @ordered
     */
	protected EList<Profile> profiles;

	/**
     * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getAnnotations()
     * @generated
     * @ordered
     */
	protected EList<PathAnnotation> annotations;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PathImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.PATH;
    }

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract String getName();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Profile> getProfiles() {
        if (profiles == null)
        {
            profiles = new EObjectEList<Profile>(Profile.class, this, MachinePackage.PATH__PROFILES);
        }
        return profiles;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Peripheral getPeripheral() {
        if (eContainerFeatureID() != MachinePackage.PATH__PERIPHERAL) return null;
        return (Peripheral)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetPeripheral(Peripheral newPeripheral, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newPeripheral, MachinePackage.PATH__PERIPHERAL, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPeripheral(Peripheral newPeripheral) {
        if (newPeripheral != eInternalContainer() || (eContainerFeatureID() != MachinePackage.PATH__PERIPHERAL && newPeripheral != null))
        {
            if (EcoreUtil.isAncestor(this, newPeripheral))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newPeripheral != null)
                msgs = ((InternalEObject)newPeripheral).eInverseAdd(this, MachinePackage.PERIPHERAL__PATHS, Peripheral.class, msgs);
            msgs = basicSetPeripheral(newPeripheral, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.PATH__PERIPHERAL, newPeripheral, newPeripheral));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<PathAnnotation> getAnnotations() {
        if (annotations == null)
        {
            annotations = new EObjectWithInverseEList.ManyInverse<PathAnnotation>(PathAnnotation.class, this, MachinePackage.PATH__ANNOTATIONS, MachinePackage.PATH_ANNOTATION__PATHS);
        }
        return annotations;
    }

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract EList<SymbolicPosition> getSources();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract EList<PathTargetReference> getTargets();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.PATH__PERIPHERAL:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetPeripheral((Peripheral)otherEnd, msgs);
            case MachinePackage.PATH__ANNOTATIONS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getAnnotations()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.PATH__PERIPHERAL:
                return basicSetPeripheral(null, msgs);
            case MachinePackage.PATH__ANNOTATIONS:
                return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID())
        {
            case MachinePackage.PATH__PERIPHERAL:
                return eInternalContainer().eInverseRemove(this, MachinePackage.PERIPHERAL__PATHS, Peripheral.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.PATH__NAME:
                return getName();
            case MachinePackage.PATH__PROFILES:
                return getProfiles();
            case MachinePackage.PATH__PERIPHERAL:
                return getPeripheral();
            case MachinePackage.PATH__ANNOTATIONS:
                return getAnnotations();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.PATH__PROFILES:
                getProfiles().clear();
                getProfiles().addAll((Collection<? extends Profile>)newValue);
                return;
            case MachinePackage.PATH__PERIPHERAL:
                setPeripheral((Peripheral)newValue);
                return;
            case MachinePackage.PATH__ANNOTATIONS:
                getAnnotations().clear();
                getAnnotations().addAll((Collection<? extends PathAnnotation>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PATH__PROFILES:
                getProfiles().clear();
                return;
            case MachinePackage.PATH__PERIPHERAL:
                setPeripheral((Peripheral)null);
                return;
            case MachinePackage.PATH__ANNOTATIONS:
                getAnnotations().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.PATH__NAME:
                return NAME_EDEFAULT == null ? getName() != null : !NAME_EDEFAULT.equals(getName());
            case MachinePackage.PATH__PROFILES:
                return profiles != null && !profiles.isEmpty();
            case MachinePackage.PATH__PERIPHERAL:
                return getPeripheral() != null;
            case MachinePackage.PATH__ANNOTATIONS:
                return annotations != null && !annotations.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case MachinePackage.PATH___GET_SOURCES:
                return getSources();
            case MachinePackage.PATH___GET_TARGETS:
                return getTargets();
        }
        return super.eInvoke(operationID, arguments);
    }

} //PathImpl
