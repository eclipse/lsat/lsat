/**
 */
package machine.impl;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.Collection;

import machine.FullMeshPath;
import machine.MachinePackage;
import machine.PathTargetReference;
import machine.Profile;
import machine.SymbolicPosition;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.lsat.common.util.CollectionUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Full Mesh Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.FullMeshPathImpl#getEndPoints <em>End Points</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FullMeshPathImpl extends PathImpl implements FullMeshPath {
	/**
     * The cached value of the '{@link #getEndPoints() <em>End Points</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getEndPoints()
     * @generated
     * @ordered
     */
	protected EList<PathTargetReference> endPoints;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected FullMeshPathImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.FULL_MESH_PATH;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<PathTargetReference> getEndPoints() {
        if (endPoints == null)
        {
            endPoints = new EObjectContainmentEList<PathTargetReference>(PathTargetReference.class, this, MachinePackage.FULL_MESH_PATH__END_POINTS);
        }
        return endPoints;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.FULL_MESH_PATH__END_POINTS:
                return ((InternalEList<?>)getEndPoints()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.FULL_MESH_PATH__END_POINTS:
                return getEndPoints();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.FULL_MESH_PATH__END_POINTS:
                getEndPoints().clear();
                getEndPoints().addAll((Collection<? extends PathTargetReference>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.FULL_MESH_PATH__END_POINTS:
                getEndPoints().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.FULL_MESH_PATH__END_POINTS:
                return endPoints != null && !endPoints.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
	 * @generated NOT
	 * @see machine.impl.PathImpl#getName()
	 */
	@Override
	public String getName() {
		StringBuffer name = new StringBuffer();
		name.append("FullMesh { profiles ");
		name.append(null == profiles || profiles.isEmpty() ? null : from(profiles).collectOne(Profile::getName).joinfields(", ", "", ""));
		if (null != endPoints) {
			for (PathTargetReference endPoint : endPoints) {
				name.append(' ').append(endPoint.getName());
			}
		}
		name.append('}');
		return name.toString();
	}

	/**
	 * @generated NOT
	 * @see machine.impl.PathImpl#getSources()
	 */
	@Override
	public EList<SymbolicPosition> getSources() {
		EList<SymbolicPosition> sources = new BasicEList<SymbolicPosition>(getEndPoints().size());
		CollectionUtil.addAll(sources, from(getEndPoints()).xcollectOne(e -> e.getPosition()));
		return ECollections.unmodifiableEList(sources);
	}

	/**
	 * @generated NOT
	 * @see machine.impl.PathImpl#getTargets()
	 */
	@Override
	public EList<PathTargetReference> getTargets() {
		return ECollections.unmodifiableEList(getEndPoints());
	}
} //FullMeshPathImpl
