/**
 */
package machine.impl;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.Arrays;

import machine.MachinePackage;
import machine.PathTargetReference;
import machine.Profile;
import machine.SymbolicPosition;
import machine.UnidirectionalPath;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unidirectional Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.UnidirectionalPathImpl#getSource <em>Source</em>}</li>
 *   <li>{@link machine.impl.UnidirectionalPathImpl#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnidirectionalPathImpl extends PathImpl implements UnidirectionalPath {
	/**
     * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSource()
     * @generated
     * @ordered
     */
	protected SymbolicPosition source;

	/**
     * The cached value of the '{@link #getTarget() <em>Target</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getTarget()
     * @generated
     * @ordered
     */
	protected PathTargetReference target;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected UnidirectionalPathImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.UNIDIRECTIONAL_PATH;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SymbolicPosition getSource() {
        return source;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetSource(SymbolicPosition newSource, NotificationChain msgs) {
        SymbolicPosition oldSource = source;
        source = newSource;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MachinePackage.UNIDIRECTIONAL_PATH__SOURCE, oldSource, newSource);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setSource(SymbolicPosition newSource) {
        if (newSource != source)
        {
            NotificationChain msgs = null;
            if (source != null)
                msgs = ((InternalEObject)source).eInverseRemove(this, MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES, SymbolicPosition.class, msgs);
            if (newSource != null)
                msgs = ((InternalEObject)newSource).eInverseAdd(this, MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES, SymbolicPosition.class, msgs);
            msgs = basicSetSource(newSource, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.UNIDIRECTIONAL_PATH__SOURCE, newSource, newSource));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public PathTargetReference getTarget() {
        return target;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetTarget(PathTargetReference newTarget, NotificationChain msgs) {
        PathTargetReference oldTarget = target;
        target = newTarget;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MachinePackage.UNIDIRECTIONAL_PATH__TARGET, oldTarget, newTarget);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setTarget(PathTargetReference newTarget) {
        if (newTarget != target)
        {
            NotificationChain msgs = null;
            if (target != null)
                msgs = ((InternalEObject)target).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MachinePackage.UNIDIRECTIONAL_PATH__TARGET, null, msgs);
            if (newTarget != null)
                msgs = ((InternalEObject)newTarget).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MachinePackage.UNIDIRECTIONAL_PATH__TARGET, null, msgs);
            msgs = basicSetTarget(newTarget, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, MachinePackage.UNIDIRECTIONAL_PATH__TARGET, newTarget, newTarget));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.UNIDIRECTIONAL_PATH__SOURCE:
                if (source != null)
                    msgs = ((InternalEObject)source).eInverseRemove(this, MachinePackage.SYMBOLIC_POSITION__SOURCE_REFERENCES, SymbolicPosition.class, msgs);
                return basicSetSource((SymbolicPosition)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.UNIDIRECTIONAL_PATH__SOURCE:
                return basicSetSource(null, msgs);
            case MachinePackage.UNIDIRECTIONAL_PATH__TARGET:
                return basicSetTarget(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.UNIDIRECTIONAL_PATH__SOURCE:
                return getSource();
            case MachinePackage.UNIDIRECTIONAL_PATH__TARGET:
                return getTarget();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.UNIDIRECTIONAL_PATH__SOURCE:
                setSource((SymbolicPosition)newValue);
                return;
            case MachinePackage.UNIDIRECTIONAL_PATH__TARGET:
                setTarget((PathTargetReference)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.UNIDIRECTIONAL_PATH__SOURCE:
                setSource((SymbolicPosition)null);
                return;
            case MachinePackage.UNIDIRECTIONAL_PATH__TARGET:
                setTarget((PathTargetReference)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.UNIDIRECTIONAL_PATH__SOURCE:
                return source != null;
            case MachinePackage.UNIDIRECTIONAL_PATH__TARGET:
                return target != null;
        }
        return super.eIsSet(featureID);
    }
	
	/**
	 * @generated NOT
	 * @see machine.impl.PathImpl#getName()
	 */
	@Override
	public String getName() {
		StringBuffer name = new StringBuffer();
		name.append(null == source ? null : source.getName());
		name.append(" --> ");
		name.append(null == target ? null : target.getName());
		name.append(" profiles ");
		name.append(null == profiles || profiles.isEmpty() ? null : from(profiles).collectOne(Profile::getName).joinfields(", ", "", ""));
		return name.toString();
	}
	
	/**
	 * @generated NOT
	 * @see machine.impl.PathImpl#getSources()
	 */
	@Override
	public EList<SymbolicPosition> getSources() {
		return ECollections.unmodifiableEList(new BasicEList<SymbolicPosition>(Arrays.asList(source)));
	}

	/**
	 * @generated NOT
	 * @see machine.impl.PathImpl#getTargets()
	 */
	@Override
	public EList<PathTargetReference> getTargets() {
		if (null == target) {
			return ECollections.emptyEList();
		}
		return ECollections.unmodifiableEList(new BasicEList<PathTargetReference>(Arrays.asList(target)));
	}
} //UnidirectionalPathImpl
