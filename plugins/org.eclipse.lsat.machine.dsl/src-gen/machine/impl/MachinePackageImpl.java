/**
 */
package machine.impl;

import java.util.Map;

import machine.ActionType;
import machine.Axis;
import machine.BidirectionalPath;
import machine.Distance;
import machine.FullMeshPath;
import machine.HasResourcePeripheral;
import machine.HasSettling;
import machine.IResource;
import machine.Import;
import machine.ImportContainer;
import machine.Machine;
import machine.MachineFactory;
import machine.MachinePackage;
import machine.Path;
import machine.PathAnnotation;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.PeripheralType;
import machine.Position;
import machine.Profile;
import machine.Resource;
import machine.ResourceItem;
import machine.ResourceType;
import machine.SetPoint;
import machine.SymbolicPosition;
import machine.UnidirectionalPath;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MachinePackageImpl extends EPackageImpl implements MachinePackage {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass peripheralTypeEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass pathEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass symbolicPositionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass resourceEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass actionTypeEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass peripheralEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass machineEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass importContainerEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass importEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass profileEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass axisEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass positionEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass setPointEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass axisPositionMapEntryEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass axisPositionsMapEntryEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass pathTargetReferenceEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass unidirectionalPathEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass bidirectionalPathEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass fullMeshPathEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass pathAnnotationEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass resourceItemEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass iResourceEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass distanceEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass hasResourcePeripheralEClass = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass hasSettlingEClass = null;

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum resourceTypeEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see machine.MachinePackage#eNS_URI
     * @see #init()
     * @generated
     */
	private MachinePackageImpl() {
        super(eNS_URI, MachineFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link MachinePackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static MachinePackage init() {
        if (isInited) return (MachinePackage)EPackage.Registry.INSTANCE.getEPackage(MachinePackage.eNS_URI);

        // Obtain or create and register package
        Object registeredMachinePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        MachinePackageImpl theMachinePackage = registeredMachinePackage instanceof MachinePackageImpl ? (MachinePackageImpl)registeredMachinePackage : new MachinePackageImpl();

        isInited = true;

        // Create package meta-data objects
        theMachinePackage.createPackageContents();

        // Initialize created meta-data
        theMachinePackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theMachinePackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(MachinePackage.eNS_URI, theMachinePackage);
        return theMachinePackage;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPeripheralType() {
        return peripheralTypeEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPeripheralType_Name() {
        return (EAttribute)peripheralTypeEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPeripheralType_Conversion() {
        return (EAttribute)peripheralTypeEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheralType_Axes() {
        return (EReference)peripheralTypeEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheralType_SetPoints() {
        return (EReference)peripheralTypeEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheralType_Actions() {
        return (EReference)peripheralTypeEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPath() {
        return pathEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPath_Name() {
        return (EAttribute)pathEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPath_Profiles() {
        return (EReference)pathEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPath_Peripheral() {
        return (EReference)pathEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPath_Annotations() {
        return (EReference)pathEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getPath__GetSources() {
        return pathEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getPath__GetTargets() {
        return pathEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getSymbolicPosition() {
        return symbolicPositionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSymbolicPosition_AxisPosition() {
        return (EReference)symbolicPositionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSymbolicPosition_Peripheral() {
        return (EReference)symbolicPositionEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSymbolicPosition_TargetReferences() {
        return (EReference)symbolicPositionEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSymbolicPosition_SourceReferences() {
        return (EReference)symbolicPositionEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getSymbolicPosition__GetPosition__Axis() {
        return symbolicPositionEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getSymbolicPosition__GetOutgoingPaths() {
        return symbolicPositionEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getResource() {
        return resourceEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getResource_Peripherals() {
        return (EReference)resourceEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getResource_Items() {
        return (EReference)resourceEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getResource_ResourceType()
    {
        return (EAttribute)resourceEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getResource__GetResource() {
        return resourceEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getResource__Fqn() {
        return resourceEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getActionType() {
        return actionTypeEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getActionType_Name() {
        return (EAttribute)actionTypeEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPeripheral() {
        return peripheralEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPeripheral_Name() {
        return (EAttribute)peripheralEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheral_Type() {
        return (EReference)peripheralEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheral_AxisPositions() {
        return (EReference)peripheralEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheral_Positions() {
        return (EReference)peripheralEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheral_Resource() {
        return (EReference)peripheralEClass.getEStructuralFeatures().get(4);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheral_Paths() {
        return (EReference)peripheralEClass.getEStructuralFeatures().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheral_Profiles() {
        return (EReference)peripheralEClass.getEStructuralFeatures().get(6);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPeripheral_Distances() {
        return (EReference)peripheralEClass.getEStructuralFeatures().get(7);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getPeripheral__Fqn() {
        return peripheralEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getMachine() {
        return machineEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getMachine_Type() {
        return (EAttribute)machineEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMachine_PathAnnotations() {
        return (EReference)machineEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMachine_Resources() {
        return (EReference)machineEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getMachine_PeripheralTypes() {
        return (EReference)machineEClass.getEStructuralFeatures().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getImportContainer() {
        return importContainerEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getImportContainer_Imports() {
        return (EReference)importContainerEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getImportContainer__LoadAll() {
        return importContainerEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getImport() {
        return importEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getImport_ImportURI() {
        return (EAttribute)importEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getImport__Load() {
        return importEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getProfile() {
        return profileEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getProfile_Name() {
        return (EAttribute)profileEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getProfile_Peripheral() {
        return (EReference)profileEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getAxis() {
        return axisEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getAxis_Name() {
        return (EAttribute)axisEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAxis_SetPoints() {
        return (EReference)axisEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getAxis_Unit() {
        return (EAttribute)axisEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPosition() {
        return positionEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPosition_Name() {
        return (EAttribute)positionEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getSetPoint() {
        return setPointEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getSetPoint_Axes() {
        return (EReference)setPointEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getSetPoint_Name() {
        return (EAttribute)setPointEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getSetPoint_Unit() {
        return (EAttribute)setPointEClass.getEStructuralFeatures().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getAxisPositionMapEntry() {
        return axisPositionMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAxisPositionMapEntry_Value() {
        return (EReference)axisPositionMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAxisPositionMapEntry_Key() {
        return (EReference)axisPositionMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getAxisPositionsMapEntry() {
        return axisPositionsMapEntryEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAxisPositionsMapEntry_Key() {
        return (EReference)axisPositionsMapEntryEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getAxisPositionsMapEntry_Value() {
        return (EReference)axisPositionsMapEntryEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPathTargetReference() {
        return pathTargetReferenceEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPathTargetReference_Name() {
        return (EAttribute)pathTargetReferenceEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPathTargetReference_Position() {
        return (EReference)pathTargetReferenceEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getUnidirectionalPath() {
        return unidirectionalPathEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getUnidirectionalPath_Source() {
        return (EReference)unidirectionalPathEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getUnidirectionalPath_Target() {
        return (EReference)unidirectionalPathEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getBidirectionalPath() {
        return bidirectionalPathEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getBidirectionalPath_EndPoints() {
        return (EReference)bidirectionalPathEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getFullMeshPath() {
        return fullMeshPathEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getFullMeshPath_EndPoints() {
        return (EReference)fullMeshPathEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getPathAnnotation() {
        return pathAnnotationEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getPathAnnotation_Name() {
        return (EAttribute)pathAnnotationEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getPathAnnotation_Paths() {
        return (EReference)pathAnnotationEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getResourceItem() {
        return resourceItemEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getResourceItem_Resource() {
        return (EReference)resourceItemEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getResourceItem__Fqn() {
        return resourceItemEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getIResource() {
        return iResourceEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getIResource_Name() {
        return (EAttribute)iResourceEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getIResource__GetResource() {
        return iResourceEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getIResource__Fqn() {
        return iResourceEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getDistance() {
        return distanceEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getDistance_Peripheral() {
        return (EReference)distanceEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EAttribute getDistance_Name() {
        return (EAttribute)distanceEClass.getEStructuralFeatures().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getHasResourcePeripheral() {
        return hasResourcePeripheralEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getHasResourcePeripheral__Fqn() {
        return hasResourcePeripheralEClass.getEOperations().get(0);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getHasResourcePeripheral__GetResource() {
        return hasResourcePeripheralEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getHasResourcePeripheral__GetPeripheral() {
        return hasResourcePeripheralEClass.getEOperations().get(2);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EOperation getHasResourcePeripheral__RpEquals__HasResourcePeripheral() {
        return hasResourcePeripheralEClass.getEOperations().get(3);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EClass getHasSettling() {
        return hasSettlingEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EReference getHasSettling_Settling() {
        return (EReference)hasSettlingEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getResourceType()
    {
        return resourceTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public MachineFactory getMachineFactory() {
        return (MachineFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        peripheralTypeEClass = createEClass(PERIPHERAL_TYPE);
        createEAttribute(peripheralTypeEClass, PERIPHERAL_TYPE__NAME);
        createEAttribute(peripheralTypeEClass, PERIPHERAL_TYPE__CONVERSION);
        createEReference(peripheralTypeEClass, PERIPHERAL_TYPE__AXES);
        createEReference(peripheralTypeEClass, PERIPHERAL_TYPE__SET_POINTS);
        createEReference(peripheralTypeEClass, PERIPHERAL_TYPE__ACTIONS);

        pathEClass = createEClass(PATH);
        createEAttribute(pathEClass, PATH__NAME);
        createEReference(pathEClass, PATH__PROFILES);
        createEReference(pathEClass, PATH__PERIPHERAL);
        createEReference(pathEClass, PATH__ANNOTATIONS);
        createEOperation(pathEClass, PATH___GET_SOURCES);
        createEOperation(pathEClass, PATH___GET_TARGETS);

        symbolicPositionEClass = createEClass(SYMBOLIC_POSITION);
        createEReference(symbolicPositionEClass, SYMBOLIC_POSITION__AXIS_POSITION);
        createEReference(symbolicPositionEClass, SYMBOLIC_POSITION__PERIPHERAL);
        createEReference(symbolicPositionEClass, SYMBOLIC_POSITION__TARGET_REFERENCES);
        createEReference(symbolicPositionEClass, SYMBOLIC_POSITION__SOURCE_REFERENCES);
        createEOperation(symbolicPositionEClass, SYMBOLIC_POSITION___GET_POSITION__AXIS);
        createEOperation(symbolicPositionEClass, SYMBOLIC_POSITION___GET_OUTGOING_PATHS);

        resourceEClass = createEClass(RESOURCE);
        createEReference(resourceEClass, RESOURCE__PERIPHERALS);
        createEReference(resourceEClass, RESOURCE__ITEMS);
        createEAttribute(resourceEClass, RESOURCE__RESOURCE_TYPE);
        createEOperation(resourceEClass, RESOURCE___GET_RESOURCE);
        createEOperation(resourceEClass, RESOURCE___FQN);

        actionTypeEClass = createEClass(ACTION_TYPE);
        createEAttribute(actionTypeEClass, ACTION_TYPE__NAME);

        peripheralEClass = createEClass(PERIPHERAL);
        createEAttribute(peripheralEClass, PERIPHERAL__NAME);
        createEReference(peripheralEClass, PERIPHERAL__TYPE);
        createEReference(peripheralEClass, PERIPHERAL__AXIS_POSITIONS);
        createEReference(peripheralEClass, PERIPHERAL__POSITIONS);
        createEReference(peripheralEClass, PERIPHERAL__RESOURCE);
        createEReference(peripheralEClass, PERIPHERAL__PATHS);
        createEReference(peripheralEClass, PERIPHERAL__PROFILES);
        createEReference(peripheralEClass, PERIPHERAL__DISTANCES);
        createEOperation(peripheralEClass, PERIPHERAL___FQN);

        machineEClass = createEClass(MACHINE);
        createEAttribute(machineEClass, MACHINE__TYPE);
        createEReference(machineEClass, MACHINE__PATH_ANNOTATIONS);
        createEReference(machineEClass, MACHINE__RESOURCES);
        createEReference(machineEClass, MACHINE__PERIPHERAL_TYPES);

        importContainerEClass = createEClass(IMPORT_CONTAINER);
        createEReference(importContainerEClass, IMPORT_CONTAINER__IMPORTS);
        createEOperation(importContainerEClass, IMPORT_CONTAINER___LOAD_ALL);

        importEClass = createEClass(IMPORT);
        createEAttribute(importEClass, IMPORT__IMPORT_URI);
        createEOperation(importEClass, IMPORT___LOAD);

        profileEClass = createEClass(PROFILE);
        createEAttribute(profileEClass, PROFILE__NAME);
        createEReference(profileEClass, PROFILE__PERIPHERAL);

        axisEClass = createEClass(AXIS);
        createEAttribute(axisEClass, AXIS__NAME);
        createEReference(axisEClass, AXIS__SET_POINTS);
        createEAttribute(axisEClass, AXIS__UNIT);

        positionEClass = createEClass(POSITION);
        createEAttribute(positionEClass, POSITION__NAME);

        setPointEClass = createEClass(SET_POINT);
        createEReference(setPointEClass, SET_POINT__AXES);
        createEAttribute(setPointEClass, SET_POINT__NAME);
        createEAttribute(setPointEClass, SET_POINT__UNIT);

        axisPositionMapEntryEClass = createEClass(AXIS_POSITION_MAP_ENTRY);
        createEReference(axisPositionMapEntryEClass, AXIS_POSITION_MAP_ENTRY__VALUE);
        createEReference(axisPositionMapEntryEClass, AXIS_POSITION_MAP_ENTRY__KEY);

        axisPositionsMapEntryEClass = createEClass(AXIS_POSITIONS_MAP_ENTRY);
        createEReference(axisPositionsMapEntryEClass, AXIS_POSITIONS_MAP_ENTRY__KEY);
        createEReference(axisPositionsMapEntryEClass, AXIS_POSITIONS_MAP_ENTRY__VALUE);

        pathTargetReferenceEClass = createEClass(PATH_TARGET_REFERENCE);
        createEAttribute(pathTargetReferenceEClass, PATH_TARGET_REFERENCE__NAME);
        createEReference(pathTargetReferenceEClass, PATH_TARGET_REFERENCE__POSITION);

        unidirectionalPathEClass = createEClass(UNIDIRECTIONAL_PATH);
        createEReference(unidirectionalPathEClass, UNIDIRECTIONAL_PATH__SOURCE);
        createEReference(unidirectionalPathEClass, UNIDIRECTIONAL_PATH__TARGET);

        bidirectionalPathEClass = createEClass(BIDIRECTIONAL_PATH);
        createEReference(bidirectionalPathEClass, BIDIRECTIONAL_PATH__END_POINTS);

        fullMeshPathEClass = createEClass(FULL_MESH_PATH);
        createEReference(fullMeshPathEClass, FULL_MESH_PATH__END_POINTS);

        pathAnnotationEClass = createEClass(PATH_ANNOTATION);
        createEAttribute(pathAnnotationEClass, PATH_ANNOTATION__NAME);
        createEReference(pathAnnotationEClass, PATH_ANNOTATION__PATHS);

        resourceItemEClass = createEClass(RESOURCE_ITEM);
        createEReference(resourceItemEClass, RESOURCE_ITEM__RESOURCE);
        createEOperation(resourceItemEClass, RESOURCE_ITEM___FQN);

        iResourceEClass = createEClass(IRESOURCE);
        createEAttribute(iResourceEClass, IRESOURCE__NAME);
        createEOperation(iResourceEClass, IRESOURCE___GET_RESOURCE);
        createEOperation(iResourceEClass, IRESOURCE___FQN);

        distanceEClass = createEClass(DISTANCE);
        createEReference(distanceEClass, DISTANCE__PERIPHERAL);
        createEAttribute(distanceEClass, DISTANCE__NAME);

        hasResourcePeripheralEClass = createEClass(HAS_RESOURCE_PERIPHERAL);
        createEOperation(hasResourcePeripheralEClass, HAS_RESOURCE_PERIPHERAL___FQN);
        createEOperation(hasResourcePeripheralEClass, HAS_RESOURCE_PERIPHERAL___GET_RESOURCE);
        createEOperation(hasResourcePeripheralEClass, HAS_RESOURCE_PERIPHERAL___GET_PERIPHERAL);
        createEOperation(hasResourcePeripheralEClass, HAS_RESOURCE_PERIPHERAL___RP_EQUALS__HASRESOURCEPERIPHERAL);

        hasSettlingEClass = createEClass(HAS_SETTLING);
        createEReference(hasSettlingEClass, HAS_SETTLING__SETTLING);

        // Create enums
        resourceTypeEEnum = createEEnum(RESOURCE_TYPE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        symbolicPositionEClass.getESuperTypes().add(this.getPosition());
        resourceEClass.getESuperTypes().add(this.getIResource());
        machineEClass.getESuperTypes().add(this.getImportContainer());
        pathTargetReferenceEClass.getESuperTypes().add(this.getHasSettling());
        unidirectionalPathEClass.getESuperTypes().add(this.getPath());
        bidirectionalPathEClass.getESuperTypes().add(this.getPath());
        fullMeshPathEClass.getESuperTypes().add(this.getPath());
        resourceItemEClass.getESuperTypes().add(this.getIResource());
        distanceEClass.getESuperTypes().add(this.getHasSettling());

        // Initialize classes, features, and operations; add parameters
        initEClass(peripheralTypeEClass, PeripheralType.class, "PeripheralType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPeripheralType_Name(), ecorePackage.getEString(), "name", null, 1, 1, PeripheralType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPeripheralType_Conversion(), ecorePackage.getEString(), "conversion", null, 0, 1, PeripheralType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheralType_Axes(), this.getAxis(), null, "axes", null, 0, -1, PeripheralType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheralType_SetPoints(), this.getSetPoint(), null, "setPoints", null, 0, -1, PeripheralType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheralType_Actions(), this.getActionType(), null, "actions", null, 0, -1, PeripheralType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(pathEClass, Path.class, "Path", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPath_Name(), ecorePackage.getEString(), "name", null, 1, 1, Path.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getPath_Profiles(), this.getProfile(), null, "profiles", null, 1, -1, Path.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPath_Peripheral(), this.getPeripheral(), this.getPeripheral_Paths(), "peripheral", null, 1, 1, Path.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPath_Annotations(), this.getPathAnnotation(), this.getPathAnnotation_Paths(), "annotations", null, 0, -1, Path.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getPath__GetSources(), this.getSymbolicPosition(), "getSources", 1, -1, IS_UNIQUE, IS_ORDERED);

        initEOperation(getPath__GetTargets(), this.getPathTargetReference(), "getTargets", 1, -1, IS_UNIQUE, IS_ORDERED);

        initEClass(symbolicPositionEClass, SymbolicPosition.class, "SymbolicPosition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSymbolicPosition_AxisPosition(), this.getAxisPositionMapEntry(), null, "axisPosition", null, 0, -1, SymbolicPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getSymbolicPosition_Peripheral(), this.getPeripheral(), this.getPeripheral_Positions(), "peripheral", null, 1, 1, SymbolicPosition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getSymbolicPosition_TargetReferences(), this.getPathTargetReference(), this.getPathTargetReference_Position(), "targetReferences", null, 0, -1, SymbolicPosition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getSymbolicPosition_SourceReferences(), this.getUnidirectionalPath(), this.getUnidirectionalPath_Source(), "sourceReferences", null, 0, -1, SymbolicPosition.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        EOperation op = initEOperation(getSymbolicPosition__GetPosition__Axis(), this.getPosition(), "getPosition", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, this.getAxis(), "axis", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEOperation(getSymbolicPosition__GetOutgoingPaths(), this.getPath(), "getOutgoingPaths", 0, -1, IS_UNIQUE, IS_ORDERED);

        initEClass(resourceEClass, Resource.class, "Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getResource_Peripherals(), this.getPeripheral(), this.getPeripheral_Resource(), "peripherals", null, 0, -1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        getResource_Peripherals().getEKeys().add(this.getPeripheral_Name());
        initEReference(getResource_Items(), this.getResourceItem(), this.getResourceItem_Resource(), "items", null, 0, -1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getResource_ResourceType(), this.getResourceType(), "resourceType", "REGULAR", 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getResource__GetResource(), this.getResource(), "getResource", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEOperation(getResource__Fqn(), ecorePackage.getEString(), "fqn", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(actionTypeEClass, ActionType.class, "ActionType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getActionType_Name(), ecorePackage.getEString(), "name", null, 1, 1, ActionType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(peripheralEClass, Peripheral.class, "Peripheral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPeripheral_Name(), ecorePackage.getEString(), "name", null, 1, 1, Peripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheral_Type(), this.getPeripheralType(), null, "type", null, 1, 1, Peripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheral_AxisPositions(), this.getAxisPositionsMapEntry(), null, "axisPositions", null, 0, -1, Peripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheral_Positions(), this.getSymbolicPosition(), this.getSymbolicPosition_Peripheral(), "positions", null, 0, -1, Peripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        getPeripheral_Positions().getEKeys().add(this.getPosition_Name());
        initEReference(getPeripheral_Resource(), this.getResource(), this.getResource_Peripherals(), "resource", null, 1, 1, Peripheral.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheral_Paths(), this.getPath(), this.getPath_Peripheral(), "paths", null, 0, -1, Peripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPeripheral_Profiles(), this.getProfile(), this.getProfile_Peripheral(), "profiles", null, 0, -1, Peripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        getPeripheral_Profiles().getEKeys().add(this.getProfile_Name());
        initEReference(getPeripheral_Distances(), this.getDistance(), this.getDistance_Peripheral(), "distances", null, 0, -1, Peripheral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getPeripheral__Fqn(), ecorePackage.getEString(), "fqn", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(machineEClass, Machine.class, "Machine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getMachine_Type(), ecorePackage.getEString(), "type", null, 0, 1, Machine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMachine_PathAnnotations(), this.getPathAnnotation(), null, "pathAnnotations", null, 0, -1, Machine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        getMachine_PathAnnotations().getEKeys().add(this.getPathAnnotation_Name());
        initEReference(getMachine_Resources(), this.getResource(), null, "resources", null, 0, -1, Machine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getMachine_PeripheralTypes(), this.getPeripheralType(), null, "peripheralTypes", null, 0, -1, Machine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(importContainerEClass, ImportContainer.class, "ImportContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getImportContainer_Imports(), this.getImport(), null, "imports", null, 0, -1, ImportContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getImportContainer__LoadAll(), ecorePackage.getEObject(), "loadAll", 0, -1, IS_UNIQUE, IS_ORDERED);

        initEClass(importEClass, Import.class, "Import", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getImport_ImportURI(), ecorePackage.getEString(), "importURI", null, 1, 1, Import.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getImport__Load(), ecorePackage.getEObject(), "load", 0, -1, IS_UNIQUE, IS_ORDERED);

        initEClass(profileEClass, Profile.class, "Profile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getProfile_Name(), ecorePackage.getEString(), "name", null, 1, 1, Profile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getProfile_Peripheral(), this.getPeripheral(), this.getPeripheral_Profiles(), "peripheral", null, 1, 1, Profile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(axisEClass, Axis.class, "Axis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAxis_Name(), ecorePackage.getEString(), "name", null, 1, 1, Axis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAxis_SetPoints(), this.getSetPoint(), this.getSetPoint_Axes(), "setPoints", null, 1, -1, Axis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAxis_Unit(), ecorePackage.getEString(), "unit", "m", 1, 1, Axis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(positionEClass, Position.class, "Position", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPosition_Name(), ecorePackage.getEString(), "name", null, 1, 1, Position.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(setPointEClass, SetPoint.class, "SetPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSetPoint_Axes(), this.getAxis(), this.getAxis_SetPoints(), "axes", null, 1, -1, SetPoint.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSetPoint_Name(), ecorePackage.getEString(), "name", null, 1, 1, SetPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSetPoint_Unit(), ecorePackage.getEString(), "unit", "m", 1, 1, SetPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(axisPositionMapEntryEClass, Map.Entry.class, "AxisPositionMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getAxisPositionMapEntry_Value(), this.getPosition(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAxisPositionMapEntry_Key(), this.getAxis(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(axisPositionsMapEntryEClass, Map.Entry.class, "AxisPositionsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
        initEReference(getAxisPositionsMapEntry_Key(), this.getAxis(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAxisPositionsMapEntry_Value(), this.getPosition(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(pathTargetReferenceEClass, PathTargetReference.class, "PathTargetReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPathTargetReference_Name(), ecorePackage.getEString(), "name", null, 1, 1, PathTargetReference.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getPathTargetReference_Position(), this.getSymbolicPosition(), this.getSymbolicPosition_TargetReferences(), "position", null, 1, 1, PathTargetReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(unidirectionalPathEClass, UnidirectionalPath.class, "UnidirectionalPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getUnidirectionalPath_Source(), this.getSymbolicPosition(), this.getSymbolicPosition_SourceReferences(), "source", null, 1, 1, UnidirectionalPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getUnidirectionalPath_Target(), this.getPathTargetReference(), null, "target", null, 1, 1, UnidirectionalPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(bidirectionalPathEClass, BidirectionalPath.class, "BidirectionalPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getBidirectionalPath_EndPoints(), this.getPathTargetReference(), null, "endPoints", null, 2, 2, BidirectionalPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(fullMeshPathEClass, FullMeshPath.class, "FullMeshPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getFullMeshPath_EndPoints(), this.getPathTargetReference(), null, "endPoints", null, 3, -1, FullMeshPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(pathAnnotationEClass, PathAnnotation.class, "PathAnnotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPathAnnotation_Name(), ecorePackage.getEString(), "name", null, 1, 1, PathAnnotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getPathAnnotation_Paths(), this.getPath(), this.getPath_Annotations(), "paths", null, 0, -1, PathAnnotation.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(resourceItemEClass, ResourceItem.class, "ResourceItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getResourceItem_Resource(), this.getResource(), this.getResource_Items(), "resource", null, 1, 1, ResourceItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getResourceItem__Fqn(), ecorePackage.getEString(), "fqn", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(iResourceEClass, IResource.class, "IResource", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getIResource_Name(), ecorePackage.getEString(), "name", null, 1, 1, IResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEOperation(getIResource__GetResource(), this.getResource(), "getResource", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEOperation(getIResource__Fqn(), ecorePackage.getEString(), "fqn", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(distanceEClass, Distance.class, "Distance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getDistance_Peripheral(), this.getPeripheral(), this.getPeripheral_Distances(), "peripheral", null, 1, 1, Distance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDistance_Name(), ecorePackage.getEString(), "name", null, 1, 1, Distance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(hasResourcePeripheralEClass, HasResourcePeripheral.class, "HasResourcePeripheral", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEOperation(getHasResourcePeripheral__Fqn(), ecorePackage.getEString(), "fqn", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEOperation(getHasResourcePeripheral__GetResource(), this.getIResource(), "getResource", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEOperation(getHasResourcePeripheral__GetPeripheral(), this.getPeripheral(), "getPeripheral", 1, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getHasResourcePeripheral__RpEquals__HasResourcePeripheral(), ecorePackage.getEBoolean(), "rpEquals", 1, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, this.getHasResourcePeripheral(), "cmp", 1, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(hasSettlingEClass, HasSettling.class, "HasSettling", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getHasSettling_Settling(), this.getAxis(), null, "settling", null, 0, -1, HasSettling.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(resourceTypeEEnum, ResourceType.class, "ResourceType");
        addEEnumLiteral(resourceTypeEEnum, ResourceType.REGULAR);
        addEEnumLiteral(resourceTypeEEnum, ResourceType.EVENT);
        addEEnumLiteral(resourceTypeEEnum, ResourceType.PASSIVE);

        // Create resource
        createResource(eNS_URI);
    }

} //MachinePackageImpl
