package machine.impl;


import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import machine.Import;
import machine.ImportContainer;
import machine.MachinePackage;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Import Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link machine.impl.ImportContainerImpl#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ImportContainerImpl extends MinimalEObjectImpl.Container implements ImportContainer {
	/**
     * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getImports()
     * @generated
     * @ordered
     */
	protected EList<Import> imports;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ImportContainerImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return MachinePackage.Literals.IMPORT_CONTAINER;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Import> getImports() {
        if (imports == null)
        {
            imports = new EObjectContainmentEList<Import>(Import.class, this, MachinePackage.IMPORT_CONTAINER__IMPORTS);
        }
        return imports;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<EObject> loadAll() {
        java.util.Collection<EObject> result = new java.util.LinkedHashSet<>();
        java.util.function.Function<ImportContainer, Void> loadAll = new java.util.function.Function<ImportContainer, Void>() {
            java.util.Collection<org.eclipse.emf.common.util.URI> seen = new java.util.LinkedHashSet<>();
        
            @Override
            public Void apply(ImportContainer container) {
                java.util.List<ImportContainer> childContainers = new java.util.ArrayList<>();
                org.eclipse.emf.common.util.URI containerURI = container.eResource().getURI();
                for (Import imp : container.getImports()) {
                    org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(imp.getImportURI());
                    if(!containerURI.isRelative()){
                        uri = uri.resolve(containerURI);
                    }
                    if (uri != null && seen.add(uri)) {
                        for (EObject eObject : imp.load()) {
                            if (result.add(eObject)) {
                                if (eObject instanceof ImportContainer) {
                                    childContainers.add((ImportContainer) eObject);
                                }
                            }
                        }
                    }
                }
        
                // post evaluate all imports of imports
                for (ImportContainer ic : childContainers) {
                    apply(ic);
                }
                return null;
            }
        
        };
        loadAll.apply(this);
        return new org.eclipse.emf.common.util.BasicEList<>(result);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case MachinePackage.IMPORT_CONTAINER__IMPORTS:
                return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case MachinePackage.IMPORT_CONTAINER__IMPORTS:
                return getImports();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case MachinePackage.IMPORT_CONTAINER__IMPORTS:
                getImports().clear();
                getImports().addAll((Collection<? extends Import>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case MachinePackage.IMPORT_CONTAINER__IMPORTS:
                getImports().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case MachinePackage.IMPORT_CONTAINER__IMPORTS:
                return imports != null && !imports.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case MachinePackage.IMPORT_CONTAINER___LOAD_ALL:
                return loadAll();
        }
        return super.eInvoke(operationID, arguments);
    }

} //ImportContainerImpl