/**
 */
package machine;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Resource Peripheral</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see machine.MachinePackage#getHasResourcePeripheral()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface HasResourcePeripheral extends EObject {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
	IResource getResource();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true" cmpRequired="true"
     * @generated
     */
	boolean rpEquals(HasResourcePeripheral cmp);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	String fqn();

} // HasResourcePeripheral
