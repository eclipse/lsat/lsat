/**
 */
package machine;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link machine.Resource#getPeripherals <em>Peripherals</em>}</li>
 *   <li>{@link machine.Resource#getItems <em>Items</em>}</li>
 *   <li>{@link machine.Resource#getResourceType <em>Resource Type</em>}</li>
 * </ul>
 *
 * @see machine.MachinePackage#getResource()
 * @model
 * @generated
 */
public interface Resource extends IResource {
	/**
     * Returns the value of the '<em><b>Peripherals</b></em>' containment reference list.
     * The list contents are of type {@link machine.Peripheral}.
     * It is bidirectional and its opposite is '{@link machine.Peripheral#getResource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripherals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripherals</em>' containment reference list.
     * @see machine.MachinePackage#getResource_Peripherals()
     * @see machine.Peripheral#getResource
     * @model opposite="resource" containment="true" keys="name"
     * @generated
     */
	EList<Peripheral> getPeripherals();

	/**
     * Returns the value of the '<em><b>Items</b></em>' containment reference list.
     * The list contents are of type {@link machine.ResourceItem}.
     * It is bidirectional and its opposite is '{@link machine.ResourceItem#getResource <em>Resource</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Items</em>' containment reference list.
     * @see machine.MachinePackage#getResource_Items()
     * @see machine.ResourceItem#getResource
     * @model opposite="resource" containment="true"
     * @generated
     */
	EList<ResourceItem> getItems();

	/**
     * Returns the value of the '<em><b>Resource Type</b></em>' attribute.
     * The default value is <code>"REGULAR"</code>.
     * The literals are from the enumeration {@link machine.ResourceType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Resource Type</em>' attribute.
     * @see machine.ResourceType
     * @see #setResourceType(ResourceType)
     * @see machine.MachinePackage#getResource_ResourceType()
     * @model default="REGULAR"
     * @generated
     */
    ResourceType getResourceType();

    /**
     * Sets the value of the '{@link machine.Resource#getResourceType <em>Resource Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource Type</em>' attribute.
     * @see machine.ResourceType
     * @see #getResourceType()
     * @generated
     */
    void setResourceType(ResourceType value);

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model kind="operation" required="true"
     * @generated
     */
	Resource getResource();

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @model required="true"
     * @generated
     */
	String fqn();

} // Resource
