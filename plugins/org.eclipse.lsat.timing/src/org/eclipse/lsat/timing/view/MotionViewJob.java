/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.view;

import java.awt.Paint;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.lsat.activity.teditor.validation.ActivityValidator;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.PositionInfo;
import org.eclipse.lsat.motioncalculator.util.PositionInfoUtilities;
import org.eclipse.lsat.timing.Activator;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.lsat.timing.util.MotionCalculatorHelper;
import org.eclipse.lsat.timing.util.MoveHelper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import activity.Move;
import setting.SettingUtil;
import setting.Settings;

public class MotionViewJob extends Job {
    public static final String SHOW_DEBUG_INFO = "org.eclipse.lsat.timing.view.debug";

    private final Move move;

    public MotionViewJob(Move move) {
        super("Plot " + ActivityValidator.id(move));
        this.move = move;
    }

    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            monitor.beginTask("Calculating plot info", IProgressMonitor.UNKNOWN);

            Settings settings = SettingUtil.getSettings(move.eResource());
            MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
            MotionCalculatorHelper util = new MotionCalculatorHelper(settings, motionCalculator);
            List<Move> concatenatedMove = util.getConcatenatedMove(move);
            List<MotionSegment> motionSegments = util.createMotionSegments(concatenatedMove);
            List<Double> motionTimes = motionCalculator.calculateTimes(motionSegments);
            Collection<PositionInfo> plotInfo = motionCalculator.getPositionInfo(motionSegments);
            Collection<String> setPoints = PositionInfoUtilities.getSetPointIds(plotInfo);

            if (null == plotInfo || plotInfo.size() == 0 || monitor.isCanceled()) {
                return Status.CANCEL_STATUS;
            }

            monitor.subTask("Rendering plot");

            JFreeChart jfreechart = null;
            Iterator<String> iter = setPoints.iterator();
            for (int i = 0; iter.hasNext(); i++) {
                String setPoint = iter.next();
                XYSeries setPointSeries = new XYSeries("Position " + setPoint);
                PositionInfo posInfo = plotInfo.stream().filter(p -> setPoint.equals(p.getSetPointId())).findFirst()
                        .get();
                posInfo.getTimePositions().forEach(e -> setPointSeries.add(e.getTime(), e.getPosition()));
                XYSeriesCollection dataSet = new XYSeriesCollection(setPointSeries);
                if (i == 0) {
                    jfreechart = ChartFactory.createXYLineChart(getTitle(concatenatedMove), "Time", setPoint, dataSet,
                            PlotOrientation.VERTICAL, true, true, false);
                    jfreechart.addSubtitle(new TextTitle(getSubTitle(concatenatedMove)));
                    updateRenderer(jfreechart.getXYPlot(), i);
                } else {
                    XYPlot xyplot = jfreechart.getXYPlot();
                    NumberAxis numberaxis = new NumberAxis(setPoint);
                    numberaxis.setAutoRangeIncludesZero(false);
                    xyplot.setRangeAxis(i, numberaxis);
                    xyplot.setDataset(i, dataSet);
                    xyplot.mapDatasetToRangeAxis(i, i);
                    updateRenderer(xyplot, i);
                }
            }

            XYPlot xyplot = jfreechart.getXYPlot();
            xyplot.setDomainPannable(true);
            xyplot.setRangePannable(true);

            for (int i = 0; i < motionTimes.size(); i++) {
                // Assuming that a move arrives at it destination when all its setpoints arrived at their location.
                // Thus time of the move is the maximum time it takes for its setpoint moves
                ValueMarker marker = new ValueMarker(motionTimes.get(i));
                Move move = concatenatedMove.get(i);
                String text;
                if (move.isPositionMove()) {
                    text = String.format("%s %s at %.4f", i < motionTimes.size() - 1 ? "Passing" : "At",
                            move.getTargetPosition().getName(), motionTimes.get(i));
                } else {
                    text = String.format("Completed %s at %.4f", move.getDistance().getName(), motionTimes.get(i));
                }
                marker.setLabel(text);
                marker.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
                xyplot.addDomainMarker(marker);
            }

            XYPlotView.showJFreeChart(jfreechart);

            return Status.OK_STATUS;
        } catch (Exception e) {
            return new Status(Status.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
        } finally {
            monitor.done();
        }
    }

    private XYItemRenderer updateRenderer(XYPlot xyplot, int index) {
        StandardXYItemRenderer standardxyitemrenderer = new StandardXYItemRenderer();
        xyplot.setRenderer(index, standardxyitemrenderer);
        final Paint paint = standardxyitemrenderer.lookupSeriesPaint(0);
        xyplot.getRangeAxis(index).setLabelPaint(paint);
        xyplot.getRangeAxis(index).setTickLabelPaint(paint);
        return standardxyitemrenderer;
    }

    private String getTitle(List<Move> concatenatedMove) {
        if (concatenatedMove.size() == 1) {
            return ActivityValidator.id(concatenatedMove.get(0));
        }
        return "Concatenated move: " + ActivityValidator.names(concatenatedMove);
    }

    private String getSubTitle(List<Move> concatenatedMove) {
        if (concatenatedMove.isEmpty())
            return "";
        if (isPositionMove(concatenatedMove)) {
            return getPositionsSubTitle(concatenatedMove);
        } else {
            return getDistancesSubTitle(concatenatedMove);
        }
    }

    private boolean isPositionMove(List<Move> concatenatedMove) {
        return concatenatedMove.stream().anyMatch(e -> e.isPositionMove());
    }

    private String getDistancesSubTitle(List<Move> concatenatedMove) {
        StringBuffer subTitle = new StringBuffer();
        String currentName = MoveHelper.getName(concatenatedMove.get(0));
        int count = 1;
        for (int i = 1; i < concatenatedMove.size(); i++) {
            String name = MoveHelper.getName(concatenatedMove.get(i));
            if (name.equals(currentName)) {
                count++;
            } else {
                appendDistances(subTitle, currentName, count);
                currentName = name;
                count = 1;
            }
        }
        appendDistances(subTitle, currentName, count);
        return subTitle.toString();
    }

    private StringBuffer appendDistances(StringBuffer subTitle, String name, int count) {
        if (subTitle.length() > 0) {
            subTitle.append(" ► ");
        }
        subTitle.append(name);
        if (count > 1) {
            subTitle.append(" (").append(count).append("x)");
        }
        return subTitle;
    }

    private String getPositionsSubTitle(List<Move> concatenatedMove) {
        StringBuffer subTitle = new StringBuffer();
        for (int i = 0; i < concatenatedMove.size(); i++) {
            if (concatenatedMove.get(i).isPositionMove()) {
                Move move = concatenatedMove.get(i);
                if (i == 0) {
                    subTitle.append(move.getSourcePosition().getName());
                }
                subTitle.append(" ► ");
                subTitle.append(move.getTargetPosition().getName());
            }
        }
        return subTitle.toString();
    }
}
