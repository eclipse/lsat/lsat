/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.view;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import activity.Move;
import timing.Distribution;

public class XYPlotViewXtextHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        final EObject context = getContext();
        if (!(context instanceof Move || context instanceof Distribution)) {
            return null;
        }
        if (!PlatformUI.getWorkbench().saveAllEditors(true)) {
            return null;
        }

        final Job job;
        if (context instanceof Move) {
            job = new MotionViewJob((Move)context);
        } else {
            job = new DistributionViewJob((Distribution)context);
        }
        job.setUser(true);
        job.schedule();
        return null;
    }

    private EObject getContext() {
        XtextEditor xtextEditor = EditorUtils.getActiveXtextEditor();
        ISelection selection = xtextEditor.getSelectionProvider().getSelection();
        if (selection instanceof ITextSelection) {
            final int offset = ((ITextSelection)selection).getOffset();
            return xtextEditor.getDocument().readOnly(new IUnitOfWork<EObject, XtextResource>() {
                @Override
                public EObject exec(XtextResource state) throws Exception {
                    return new EObjectAtOffsetHelper().resolveElementAt(state, offset);
                }
            });
        }
        return null;
    }
}
