/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.lsat.timing.Activator;
import org.eclipse.xtext.EcoreUtil2;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.Range;
import org.jfree.data.xy.XYBarDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import setting.PhysicalSettings;
import setting.impl.TimingSettingsMapEntryImpl;
import timing.Distribution;
import timing.DistributionsFactory;
import timing.distribution.ModeDistribution;
import timing.distribution.ModeNotSupportedException;

public class DistributionViewJob extends Job {
    private final Distribution distribution;

    public DistributionViewJob(Distribution distribution) {
        super("Plot " + getName(distribution));
        this.distribution = distribution;
    }

    private static String getName(Distribution distribution) {
        TimingSettingsMapEntryImpl tsmei = (TimingSettingsMapEntryImpl)distribution.eContainer();
        PhysicalSettings psmei = EcoreUtil2.getContainerOfType(tsmei, PhysicalSettings.class);
        return psmei.fqn() + "." + tsmei.getKey().getName();
    }

    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            String name = getName(distribution);
            monitor.beginTask("Rendering plot " + name, IProgressMonitor.UNKNOWN);

            ModeDistribution modeDistribution = DistributionsFactory.createRealDistribution(distribution);
            double mean = modeDistribution.getNumericalMean();
            Range range = getDistributionRange(modeDistribution);
            double step = Math.max(range.getLength() / 1000, 1e-6);

            XYSeries series = new XYSeries(name);
            for (double x = range.getLowerBound(); x < range.getUpperBound() + step; x += step) {
                series.add(x, modeDistribution.density(x));
            }
            XYSeriesCollection dataset = new XYSeriesCollection(series);

            JFreeChart jfreechart = ChartFactory.createXYLineChart("Probability Density Function", "Time", "Density",
                    dataset, PlotOrientation.VERTICAL, true, false, false);

            ValueMarker meanMarker = new ValueMarker(mean, Color.BLACK, new BasicStroke(1f));
            meanMarker.setLabel(String.format("mean = %.6f", mean));
            meanMarker.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
            meanMarker.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);
            jfreechart.getXYPlot().addDomainMarker(meanMarker);

            // TODO: Remove this?
            // YBLA: I only added this for myself to gain some insight in statistics
            final boolean showDebugInfo = Platform.getPreferencesService().getBoolean(Activator.PLUGIN_ID,
                    MotionViewJob.SHOW_DEBUG_INFO, false, null);
            if (showDebugInfo) {
                addSampledDataset(jfreechart, modeDistribution, 1);

                try {
                    ValueMarker modeMarker = new ValueMarker(modeDistribution.getMode(), Color.GRAY,
                            new BasicStroke(1f));
                    modeMarker.setLabel(String.format("mode = %.6f", modeDistribution.getMode()));
                    modeMarker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
                    modeMarker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
                    jfreechart.getXYPlot().addDomainMarker(modeMarker);
                } catch (ModeNotSupportedException e) {
                    // Distribution doesn't have mode, skip marker
                }
            }
            // END: Remove this

            XYPlotView.showJFreeChart(jfreechart);
            return Status.OK_STATUS;
        } catch (Exception e) {
            return new Status(Status.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
        } finally {
            monitor.done();
        }
    }

    private Range getDistributionRange(RealDistribution distribution) {
        double lower = distribution.getSupportLowerBound();
        double upper = distribution.getSupportUpperBound();

        if (distribution instanceof NormalDistribution) {
            double mean = distribution.getNumericalMean();
            double sd = ((NormalDistribution)distribution).getStandardDeviation();
            lower = mean - 4 * sd;
            upper = mean + 4 * sd;
        }

        // Assume execution times not less than 0 and exceeding 1 minute
        return new Range(Math.max(lower, 0), Math.min(upper, 60));
    }

    private void addSampledDataset(JFreeChart jfreechart, RealDistribution realDistribution, int index) {
        double[] samples = realDistribution.sample(1000);
        double sampleSize = getDistributionRange(realDistribution).getLength() / 15;
        Map<Double, Integer> data = new HashMap<>();
        for (int i = 0; i < samples.length; i++) {
            double sample = Math.floor(samples[i] / sampleSize);
            int count = data.containsKey(sample) ? data.get(sample) : 0;
            data.put(sample, ++count);
        }
        XYSeries series = new XYSeries("sampled (" + samples.length + "x)");
        for (Map.Entry<Double, Integer> entry: data.entrySet()) {
            series.add((entry.getKey() * sampleSize) + (sampleSize / 2), entry.getValue());
        }

        XYPlot xyplot = jfreechart.getXYPlot();
        NumberAxis numberaxis = new NumberAxis("number of samples");
        xyplot.setRangeAxis(index, numberaxis);
        xyplot.setDataset(index, new XYBarDataset(new XYSeriesCollection(series), sampleSize));
        xyplot.mapDatasetToRangeAxis(index, index);
        xyplot.setRenderer(index, new XYBarRenderer());
    }
}
