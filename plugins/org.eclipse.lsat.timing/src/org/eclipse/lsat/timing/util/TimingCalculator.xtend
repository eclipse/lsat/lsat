/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util

import activity.Move
import activity.SimpleAction
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.util.Collection
import java.util.IdentityHashMap
import java.util.Map
import machine.ActionType
import machine.IResource
import machine.Peripheral
import machine.SetPoint
import org.apache.commons.math3.random.JDKRandomGenerator
import org.apache.commons.math3.random.RandomGenerator
import org.eclipse.lsat.common.graph.directed.editable.Node
import org.eclipse.lsat.common.xtend.annotations.IntermediateProperty
import org.eclipse.lsat.motioncalculator.MotionException
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension
import setting.PhysicalSettings
import setting.Settings
import timing.Array
import timing.CalculationMode
import timing.Distribution
import timing.DistributionsFactory
import timing.FixedValue
import timing.distribution.ModeDistribution

import static extension org.eclipse.lsat.common.util.IterableUtil.*

class TimingCalculator implements ITimingCalculator {
    static val MICRO_SECONDS = new MathContext(6 , RoundingMode.HALF_UP)
    
    @IntermediateProperty(Array)
    val Integer pointer = -1

    val RandomGenerator distributionRandom = new JDKRandomGenerator(1618033989)
    val Map<Distribution, ModeDistribution> distributionSession = new IdentityHashMap()
    
    val MotionCalculatorHelper motionCalculatorHelper;
    val CalculationMode mode
    val boolean synchronizeAxes
    val Map<Node, BigDecimal> nodeTimes
    val Map<Move, Map<SetPoint, BigDecimal>> motionTimes

    new(Settings settings, MotionCalculatorExtension motionCalculator) {
        this(settings, motionCalculator, CalculationMode.MEAN, true, true)
    }
    
    new(Settings settings, MotionCalculatorExtension motionCalculator, CalculationMode mode) {
        this(settings, motionCalculator, mode, true, true)
    }
    
    new(Settings settings, MotionCalculatorExtension motionCalculator, CalculationMode mode, boolean synchronizeAxes, boolean useCache) {
        this.motionCalculatorHelper = new MotionCalculatorHelper(settings, motionCalculator);
        this.mode = mode
        this.synchronizeAxes = synchronizeAxes
        
        nodeTimes = if (useCache && mode == CalculationMode.MEAN) new IdentityHashMap()
        // Motion does not support distribution/linear yet and calculations are 'expensive', 
        // thus cache is independent of calculation mode
        motionTimes = if (useCache) new IdentityHashMap()
    }
    
    def Settings getSettings() {
        return motionCalculatorHelper.settings
    }
    
    def MotionCalculatorExtension getMotionCalculator() {
        return motionCalculatorHelper.motionCalculator
    }
    
    override void reset() {
        distributionSession.clear
        _IntermediateProperty_pointer.clear
        if (nodeTimes !== null) nodeTimes.clear
        if (motionTimes !== null) motionTimes.clear
    }
    
    override BigDecimal calculateDuration(Node node) throws MotionException {
        if (null === nodeTimes)
            node.doCalculateDuration
        else
            nodeTimes.computeIfAbsent(node)[doCalculateDuration];
    }
    
    protected def dispatch BigDecimal doCalculateDuration(Node node) {
        BigDecimal.ZERO
    }

    protected def dispatch BigDecimal doCalculateDuration(SimpleAction action) {
       action.type.doCalculateValue(action.resource, action.peripheral)
    }
    
    protected def BigDecimal doCalculateValue(ActionType actionType, IResource resource, Peripheral peripheral) {
        getPhysicalSettings(resource,peripheral).timingSettings.get(actionType).doCalculateValue
    }

    protected def dispatch BigDecimal doCalculateValue(FixedValue fixedValue) {
        fixedValue.value
    }
    
    protected def dispatch BigDecimal doCalculateValue(Array array) {
        switch (mode) {
            case LINEAIR : {
                array.pointer = (array.pointer + 1) % array.values.size
                array.values.get(array.pointer)
            }
            default : array.values.average
        }
    }

    protected def dispatch BigDecimal doCalculateValue(Distribution distribution) {
        val realDistribution = distributionSession.computeIfAbsent(distribution) [
            DistributionsFactory::createRealDistribution(it, distributionRandom)
        ]
        switch (mode) {
            case DISTRIBUTED : realDistribution.sample.normalize
            default : realDistribution.^default.normalize
        }
    }

    protected def dispatch BigDecimal doCalculateDuration(Move move) {
        move.calculateMotionTime.values.max(BigDecimal.ZERO)
    }
    
    override Map<SetPoint, BigDecimal> calculateMotionTime(Move move) throws MotionException {
        if (null === motionTimes) return move.doCalculateMotionTimes.get(move)
        
        if (!motionTimes.containsKey(move)) {
            motionTimes.putAll(move.doCalculateMotionTimes)
        }
        return motionTimes.get(move);
    }
    
    protected def Map<Move, ? extends Map<SetPoint, BigDecimal>> doCalculateMotionTimes(Move move) {
        val axesAndSetPoints = if (synchronizeAxes) {
            newHashMap(move.peripheral.type.axes -> move.peripheral.type.setPoints)
        } else {
            move.peripheral.type.setPoints.groupBy[axes];
        }
        
        val concatenatedMove = motionCalculatorHelper.getConcatenatedMove(move)
        val result = new IdentityHashMap(concatenatedMove.size)
        for (e : axesAndSetPoints.entrySet) {
            val motionSegments = motionCalculatorHelper.createMotionSegments(concatenatedMove, e.key)
            val motionTimes = motionCalculatorHelper.motionCalculator.calculateTimes(motionSegments)
            var startTime = BigDecimal.ZERO
            for (var i = 0; i < motionTimes.size(); i++) {
                val endTime = motionTimes.get(i).normalize
                result.computeIfAbsent(concatenatedMove.get(i)) [
                    new IdentityHashMap(move.peripheral.type.setPoints.size)
                ].fill(e.value, endTime.subtract(startTime))
                startTime = endTime
            }
        }
        return result
    }
    
    protected def PhysicalSettings getPhysicalSettings(IResource resource, Peripheral peripheral) throws SpecificationException {
        val physicalSettings = settings.getPhysicalSettings(resource, peripheral)
        if (null === physicalSettings) {
            throw new SpecificationException('''Physical settings not specified for peripheral: «peripheral.fqn»''',
                settings)
        }
        return physicalSettings
    }
    
    /**
     * Fills the map with the specified value for all specified keys.
     */
    static def <K,V> fill(Map<K,V> map, Collection<? extends K> keys, V value) {
        keys.forEach[map.put(it, value)]
    }
    
    static def BigDecimal getAverage(Collection<BigDecimal> values) {
        var sum = values.head
        for (value : values.tail) {
            sum += value
        }
        return sum.divide(new BigDecimal(values.size), MICRO_SECONDS)
    } 
    
    static def BigDecimal normalize(Number number) {
        val result = BigDecimal.valueOf(number.doubleValue)
        return result.max(BigDecimal.ZERO).round(MICRO_SECONDS)
    }
}
