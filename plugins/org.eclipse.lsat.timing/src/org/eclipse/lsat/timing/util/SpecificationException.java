/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util;

import org.eclipse.emf.ecore.EObject;

public class SpecificationException extends Exception {
    private static final long serialVersionUID = 1L;

    private final EObject[] eObjects;

    public SpecificationException(String message, EObject... eObjects) {
        super(message);
        this.eObjects = eObjects;
    }

    public SpecificationException(String message, Throwable cause, EObject... eObjects) {
        super(message, cause);
        this.eObjects = eObjects;
    }

    public EObject[] getEObjects() {
        return eObjects;
    }
}
