/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.util;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import machine.Axis;
import machine.Distance;
import machine.Peripheral;
import machine.PeripheralType;

public final class AxesDistance extends HashMap<Axis, BigDecimal> {
    private static final long serialVersionUID = 6227801704225324973L;

    private final Distance distance;

    AxesDistance(Distance distance) {
        super(safeGetAxes(distance).size());
        this.distance = distance;
    }

    public Distance getDistance() {
        return distance;
    }

    public PeripheralType getPeripheralType() {
        return distance.getPeripheral().getType();
    }

    public Set<Axis> getMovingAxes() {
        Set<Axis> movingAxes = new LinkedHashSet<Axis>(getPeripheralType().getAxes().size());
        for (Map.Entry<Axis, BigDecimal> entry: entrySet()) {
            if (!BigDecimal.ZERO.equals(entry.getValue())) {
                movingAxes.add(entry.getKey());
            }
        }
        return movingAxes;
    }

    static final List<Axis> safeGetAxes(Distance distance) {
        if (null == distance)
            return Collections.emptyList();
        Peripheral peripheral = distance.getPeripheral();
        if (null == peripheral)
            return Collections.emptyList();
        PeripheralType peripheralType = peripheral.getType();
        if (null == peripheralType)
            return Collections.emptyList();
        return peripheralType.getAxes();
    }
}
