/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.calculator;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.lang.String.format;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.motioncalculator.MotionCalculator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionSetPoint;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.PositionInfo;
import org.eclipse.lsat.motioncalculator.util.MotionSegmentUtilities;
import org.eclipse.lsat.motioncalculator.util.MotionSetPointUtilities;

public class PointToPointMotionCalculator implements MotionCalculator {
    public static final String ID = "org.eclipse.lsat.timing.calculator.PointToPointMotionCalculator";

    protected static final String THIRD_ORDER_POINT_TO_POINT_MOTION_PROFILE_KEY = "ThirdOrderP2P";

    protected static final String VELOCITY_PARAMETER_KEY = "V";

    protected static final String ACCELERATION_PARAMETER_KEY = "A";

    protected static final String JERK_PARAMETER_KEY = "J";

    protected static final String SETTLING_PARAMETER_KEY = "S";

    private static final double DEFAULT_SAMPLE_FREQUENCY = 5000;

    private static final double DZERO = ZERO.doubleValue();

    @Override
    public void validate(List<MotionSegment> segments) throws MotionValidationException {
        if (segments.size() == 0) {
            throw new MotionValidationException(
                    "Programming error: expected at least 1 motion segment. Please contact LSAT support.");
        }

        for (String setPoint: MotionSegmentUtilities.getSetPointIds(segments)) {
            List<MotionSetPoint> motionSetPoints = MotionSegmentUtilities.getMotionSetPoints(segments, setPoint);
            if (motionSetPoints.stream().anyMatch(MotionSetPoint::doesMove)
                    && !motionSetPoints.stream().allMatch(MotionSetPoint::doesMove))
            {
                List<MotionSegment> eroneousSegments = segments.stream()
                        .filter(s -> !s.getMotionSetpoint(setPoint).doesMove()).collect(Collectors.toList());
                throw new MotionValidationException(
                        String.format("Setpoint %s does not move during this part of the concatenated %s", setPoint,
                                String.join(", ",
                                        segments.stream().map(Object::toString).collect(Collectors.toList()))),
                        eroneousSegments);
            }
            Set<MotionProfile> motionProfiles = MotionSetPointUtilities.getMotionProfiles(motionSetPoints);
            if (motionProfiles.size() > 1) {
                throw new MotionValidationException(
                        "A concatenated point-to-point move should use the same motion profile", segments);
            }
            Map<String, BigDecimal> motionProfileArguments = motionSetPoints.get(0).getMotionProfileArguments();
            if (!motionSetPoints.stream().allMatch(s -> s.getMotionProfileArguments().equals(motionProfileArguments))) {
                throw new MotionValidationException(
                        "A concatenated point-to-point move should define the same motion profile arguments", segments);
            }
        }
    }

    @Override
    public Collection<PositionInfo> getPositionInfo(List<MotionSegment> segments) throws MotionException {
        final Set<String> setPointIds = MotionSegmentUtilities.getSetPointIds(segments);
        if (setPointIds.isEmpty()) {
            return Collections.emptySet();
        }

        Double pointToPointTime = calculatePointToPointTime(segments);

        List<PositionInfo> result = new ArrayList<>();

        for (String setPointId: setPointIds) {
            List<MotionSetPoint> setPoints = MotionSegmentUtilities.getMotionSetPoints(segments, setPointId);
            double[][] samples = sampleMotionProfile(setPoints, pointToPointTime);
            PositionInfo positionInfo = new PositionInfo(setPointId);
            for (double[] sample: samples) {
                positionInfo.addTimePosition(sample[0], sample[1]);
            }
            result.add(positionInfo);
        }
        return Collections.unmodifiableCollection(result);
    }

    @Override
    public List<Double> calculateTimes(List<MotionSegment> segments) throws MotionException {
        if (segments.get(0).getMotionSetpoints().isEmpty()) {
            return Collections.emptyList();
        }

        final Double[] motionTimes = new Double[segments.size()];
        Double pointToPointTime = calculatePointToPointTime(segments);
        motionTimes[motionTimes.length - 1] = pointToPointTime;

        if (segments.size() == 1) {
            // No event-on-the fly, then we only need to calculate the point-to-point time and we're done.
            return Arrays.asList(motionTimes);
        }

        for (String setPointId: MotionSegmentUtilities.getSetPointIds(segments)) {
            List<MotionSetPoint> setPoints = MotionSegmentUtilities.getMotionSetPoints(segments, setPointId);
            double[][] samples = sampleMotionProfile(setPoints, pointToPointTime);
            for (int index = 0; index < segments.size() - 1; index++) {
                BigDecimal to = MotionSetPointUtilities.getTo(setPoints.subList(0, index + 1));
                Double setPointTime = determineTimeforLocation(to, samples);
                if (null == setPointTime) {
                    BigDecimal startPos = MotionSetPointUtilities.getFrom(setPoints);
                    BigDecimal endPos = MotionSetPointUtilities.getTo(setPoints);
                    throw new MotionException(format("Cannot find location for setpoint %s: %f not between %f and %f",
                            setPointId, to, startPos, endPos));
                }
                // Take the max of all set-point times for the current set-point
                double currentTime = motionTimes[index] == null ? 0 : motionTimes[index];
                motionTimes[index] = Math.max(currentTime, Math.min(setPointTime, pointToPointTime));
            }
        }
        return Arrays.asList(motionTimes);
    }

    private Double calculatePointToPointTime(List<MotionSegment> segments) throws MotionException {
        final Map<String, Double> pointToPointTime = new HashMap<>();
        for (String setPointId: MotionSegmentUtilities.getSetPointIds(segments)) {
            List<MotionSetPoint> setPoints = MotionSegmentUtilities.getMotionSetPoints(segments, setPointId);
            Double spTime = calculateTime(setPoints);
            pointToPointTime.put(setPointId, spTime);
        }
        // Stretching the moves of all set-points to the most time consuming one
        return Collections.max(pointToPointTime.values());
    }

    private Double calculateTime(List<MotionSetPoint> setPoints) throws MotionException {
        final MotionSetPoint lastSp = setPoints.get(setPoints.size() - 1);
        final double vMax = toPositiveDouble(lastSp.getMotionProfileArgument(VELOCITY_PARAMETER_KEY));
        final double aMax = toPositiveDouble(lastSp.getMotionProfileArgument(ACCELERATION_PARAMETER_KEY));
        final double jMax = toPositiveDouble(lastSp.getMotionProfileArgument(JERK_PARAMETER_KEY));
        final double tSettle = toPositiveDouble(
                lastSp.isSettling() ? lastSp.getMotionProfileArgument(SETTLING_PARAMETER_KEY, ZERO) : ZERO);

        final double sAbs = MotionSetPointUtilities.getDistance(setPoints).abs().doubleValue();
        final double[] t_vaj = calculateTvTaTj(sAbs, vMax, aMax, jMax);
        return (t_vaj[0] + 2.0 * t_vaj[1] + 4.0 * t_vaj[2]) + tSettle;
    }

    @SuppressWarnings("unused")
    private double[][] sampleMotionProfile(List<MotionSetPoint> setPoints, Double stretchToTime)
            throws MotionException
    {
        final MotionSetPoint lastSp = setPoints.get(setPoints.size() - 1);
        final double vMax = toPositiveDouble(lastSp.getMotionProfileArgument(VELOCITY_PARAMETER_KEY));
        final double aMax = toPositiveDouble(lastSp.getMotionProfileArgument(ACCELERATION_PARAMETER_KEY));
        double jMax = toPositiveDouble(lastSp.getMotionProfileArgument(JERK_PARAMETER_KEY));
        final double tSettle = toPositiveDouble(
                lastSp.isSettling() ? lastSp.getMotionProfileArgument(SETTLING_PARAMETER_KEY, ZERO) : ZERO);

        final BigDecimal distance = MotionSetPointUtilities.getDistance(setPoints);
        final BigDecimal from = MotionSetPointUtilities.getFrom(setPoints);
        final BigDecimal to = MotionSetPointUtilities.getTo(setPoints);

        if (0 == from.compareTo(to)) {
            return null == stretchToTime
                    ? new double[][]
                    {{DZERO, from.doubleValue(), DZERO, DZERO, DZERO}, {tSettle, to.doubleValue(), DZERO, DZERO, DZERO}}
                    : new double[][]
                    {{DZERO, from.doubleValue()}, {stretchToTime, to.doubleValue()}};
        }

        final double sFrom = from.doubleValue();
        final double sTo = to.doubleValue();
        final double sAbs = distance.abs().doubleValue();
        final double[] t_vaj = calculateTvTaTj(sAbs, vMax, aMax, jMax);
        final double tv = t_vaj[0];
        final double ta = t_vaj[1];
        final double tj = t_vaj[2];

        double t1 = tj;
        double t2 = t1 + ta;
        double t3 = t2 + tj;
        double t4 = t3 + tv;
        double t5 = t4 + tj;
        double t6 = t5 + ta;
        double t7 = t6 + tj;

        // If we want to calculate a 'negative' move, we only need to invert the jerk as all other params are derived
        jMax = sTo < sFrom ? -jMax : jMax;

        double a1 = jMax * tj;
        double v1 = 0.5 * jMax * pow(tj, 2);
        double p1 = sFrom + (jMax * pow(tj, 3)) / 6.0;
        double a2 = a1;
        double v2 = v1 + a1 * ta;
        double p2 = p1 + v1 * ta + 0.5 * a1 * pow(ta, 2);
        double a3 = 0;
        double v3 = v2 + a2 * tj - 0.5 * jMax * pow(tj, 2);
        double p3 = p2 + v2 * tj + 0.5 * a2 * pow(tj, 2) - (jMax * pow(tj, 3) / 6.0);
        double a4 = 0;
        double v4 = v3;
        double p4 = p3 + v3 * tv;
        double a5 = -jMax * tj;
        double v5 = v4 - 0.5 * jMax * pow(tj, 2);
        double p5 = p4 + v4 * tj - (jMax * pow(tj, 3) / 6.0);
        double a6 = a5;
        double v6 = v5 + a5 * ta;
        double p6 = p5 + v5 * ta + 0.5 * a5 * pow(ta, 2);
        double a7 = 0;
        double v7 = 0;
        double p7 = sTo;

        // We implement a very simple algorithm to stretch a move to a certain time, by really stretching it.
        // In practice this means that all maximum values are lower then max (vMax, aMax, jMax).
        // Therefore these columns will not be exported anymore as this calculation would not be valid.
        final double point2pointTime = t7 + tSettle;
        final boolean stretch = null != stretchToTime && Double.compare(stretchToTime, point2pointTime) != 0;
        final double stretchFactor = stretch ? stretchToTime / point2pointTime : 1.0;
        // Use an additional sample
        final int tpvaj_size = (int)Math.ceil(point2pointTime * DEFAULT_SAMPLE_FREQUENCY) + 1;
        final double[][] tpvaj = new double[tpvaj_size][stretch ? 2 : 5];
        int index = 0;
        double t = 0;
        // t0..t1
        if (index < tpvaj_size) {
            do {
                tpvaj[index][0] = t * stretchFactor;
                tpvaj[index][1] = sFrom + (jMax * pow(t, 3)) / 6.0;
                if (!stretch) {
                    tpvaj[index][2] = 0.5 * jMax * pow(t, 2);
                    tpvaj[index][3] = jMax * t;
                    tpvaj[index][4] = jMax;
                }
                t = ++index / DEFAULT_SAMPLE_FREQUENCY;
            } while (t < t1);
        }
        // t1..t2
        if (index < tpvaj_size) {
            do {
                tpvaj[index][0] = t * stretchFactor;
                tpvaj[index][1] = p1 + v1 * (t - t1) + 0.5 * a1 * pow((t - t1), 2);
                if (!stretch) {
                    tpvaj[index][2] = v1 + a1 * (t - t1);
                    tpvaj[index][3] = a1;
                    tpvaj[index][4] = 0;
                }
                t = ++index / DEFAULT_SAMPLE_FREQUENCY;
            } while (t < t2);
        }
        // t2..t3
        if (index < tpvaj_size) {
            do {
                tpvaj[index][0] = t * stretchFactor;
                tpvaj[index][1] = p2 + v2 * (t - t2) + 0.5 * a2 * pow(t - t2, 2) - (jMax * pow(t - t2, 3) / 6.0);
                if (!stretch) {
                    tpvaj[index][2] = v2 + a2 * (t - t2) - 0.5 * jMax * pow(t - t2, 2);
                    tpvaj[index][3] = a2 - jMax * (t - t2);
                    tpvaj[index][4] = -jMax;
                }
                t = ++index / DEFAULT_SAMPLE_FREQUENCY;
            } while (t < t3);
        }
        // t3..t4
        if (index < tpvaj_size) {
            do {
                tpvaj[index][0] = t * stretchFactor;
                tpvaj[index][1] = p3 + v3 * (t - t3);
                if (!stretch) {
                    tpvaj[index][2] = v3;
                    tpvaj[index][3] = 0;
                    tpvaj[index][4] = 0;
                }
                t = ++index / DEFAULT_SAMPLE_FREQUENCY;
            } while (t < t4);
        }
        // t4..t5
        if (index < tpvaj_size) {
            do {
                tpvaj[index][0] = t * stretchFactor;
                tpvaj[index][1] = p4 + v4 * (t - t4) - (jMax * pow(t - t4, 3) / 6.0);
                if (!stretch) {
                    tpvaj[index][2] = v4 - 0.5 * jMax * pow(t - t4, 2);
                    tpvaj[index][3] = -jMax * (t - t4);
                    tpvaj[index][4] = -jMax;
                }
                t = ++index / DEFAULT_SAMPLE_FREQUENCY;
            } while (t < t5);
        }
        // t5..t6
        if (index < tpvaj_size) {
            do {
                tpvaj[index][0] = t * stretchFactor;
                tpvaj[index][1] = p5 + v5 * (t - t5) + 0.5 * a5 * pow(t - t5, 2);
                if (!stretch) {
                    tpvaj[index][2] = v5 + a5 * (t - t5);
                    tpvaj[index][3] = a5;
                    tpvaj[index][4] = 0;
                }
                t = ++index / DEFAULT_SAMPLE_FREQUENCY;
            } while (t < t6);
        }
        // t6..t7
        if (index < tpvaj_size) {
            do {
                tpvaj[index][0] = t * stretchFactor;
                tpvaj[index][1] = p6 + v6 * (t - t6) + 0.5 * a6 * pow(t - t6, 2) + (jMax * pow(t - t6, 3) / 6.0);
                if (!stretch) {
                    tpvaj[index][2] = v6 + a6 * (t - t6) + 0.5 * jMax * pow(t - t6, 2);
                    tpvaj[index][3] = a6 + jMax * (t - t6);
                    tpvaj[index][4] = jMax;
                }
                t = ++index / DEFAULT_SAMPLE_FREQUENCY;
            } while (t < t7);
        }
        // t7..
        while (index < tpvaj.length) {
            tpvaj[index][0] = t * stretchFactor;
            tpvaj[index][1] = p7;
            if (!stretch) {
                tpvaj[index][2] = 0;
                tpvaj[index][3] = 0;
                tpvaj[index][4] = 0;
            }
            t = ++index / DEFAULT_SAMPLE_FREQUENCY;
        }
        return tpvaj;
    }

    /**
     * Calculates the time when a sample segment crosses the specified location.<br>
     * Uses the tpvaj as lookup table, but threats samples as linear line segments.
     *
     * @param location requested lookup location
     * @param tpvaj lookup table
     * @return the time when location is passed or null if location is not within move range
     */
    private Double determineTimeforLocation(BigDecimal location, double[][] tpvaj) {
        final double locationD = location.doubleValue();
        final int operator = Double.compare(locationD, tpvaj[0][1]);
        if (0 == operator) {
            // First sample is exact match, return start time
            return tpvaj[0][0];
        }
        int index = 0;
        while (index < tpvaj.length && Double.compare(locationD, tpvaj[index][1]) == operator) {
            index++;
        }
        if (index < 1 || index >= tpvaj.length) {
            // Didn't find right index for calculation
            return null;
        }
        double timFrom = tpvaj[index - 1][0];
        double locFrom = tpvaj[index - 1][1];
        double timTo = tpvaj[index][0];
        double locTo = tpvaj[index][1];
        double factor = (locationD - locFrom) / (locTo - locFrom);
        double time = timFrom + factor * (timTo - timFrom);
        return time;
    }

    private double[] calculateTvTaTj(double sAbs, double vMax, double aMax, double jMax) {
        final double[] t_vaj = new double[3];

        if (aMax < sqrt(vMax * jMax)) {
            if (sAbs > ((pow(vMax, 2.0) / aMax) + ((vMax * aMax) / jMax))) {
                t_vaj[2] = aMax / jMax;
                t_vaj[1] = vMax / aMax - aMax / jMax;
                t_vaj[0] = sAbs / vMax - vMax / aMax - aMax / jMax;
            } else if (sAbs > (2.0 * pow(aMax, 3.0)) / pow(jMax, 2.0)) {
                t_vaj[2] = aMax / jMax;
                t_vaj[1] = (-(3.0 * aMax) / (2.0 * jMax)) + sqrt(pow(aMax, 2.0) / (4.0 * pow(jMax, 2.0)) + sAbs / aMax);
                t_vaj[0] = 0.0;
            } else {
                t_vaj[2] = pow(sAbs / (2.0 * jMax), 1.0 / 3.0);
                t_vaj[1] = 0.0;
                t_vaj[0] = 0.0;
            }
        } else if (aMax >= sqrt(vMax * jMax)) {
            if (sAbs < 2.0 * sqrt(pow(vMax, 3.0) / jMax)) {
                t_vaj[2] = pow(sAbs / (2.0 * jMax), 1.0 / 3.0);
                t_vaj[1] = 0.0;
                t_vaj[0] = 0.0;
            } else {
                t_vaj[2] = sqrt(vMax / jMax);
                t_vaj[1] = 0.0;
                t_vaj[0] = (sAbs / vMax) - (2.0 * sqrt(vMax / jMax));
            }
        }

        return t_vaj;
    }

    /**
     * Converts a {@link BigDecimal} to a double.
     *
     * @throws MotionException
     */
    private double toPositiveDouble(BigDecimal bd) throws MotionException {
        if (null == bd) {
            throw new MotionException("Value cannot be null");
        } else if (bd.compareTo(ZERO) < 0) {
            throw new MotionException("Value should be greater then zero: " + bd);
        }
        return bd.doubleValue();
    }
}
