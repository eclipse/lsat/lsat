/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.validation;

import org.eclipse.lsat.common.emf.validation.EValidatorUtil;
import org.eclipse.ui.IStartup;

import activity.ActivityPackage;
import machine.MachinePackage;

public class Startup implements IStartup {
    @Override
    public void earlyStartup() {
        EValidatorUtil.registerValidations(MachinePackage.eINSTANCE, MachineEValidator.INSTANCE);
        EValidatorUtil.registerValidations(ActivityPackage.eINSTANCE, ActivityEValidator.INSTANCE);
    }
}
