/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.timing.validation;

import static java.lang.String.format;
import static org.eclipse.emf.common.util.Diagnostic.WARNING;
import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.lsat.common.emf.common.util.BufferedDiagnosticChain;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.timing.calculator.MotionCalculatorExtension;
import org.eclipse.lsat.timing.util.AxesLocation;
import org.eclipse.lsat.timing.util.MotionCalculatorHelper;
import org.eclipse.lsat.timing.util.SpecificationException;

import machine.Activator;
import machine.Axis;
import machine.BidirectionalPath;
import machine.FullMeshPath;
import machine.IResource;
import machine.Machine;
import machine.MachinePackage;
import machine.Path;
import machine.PathTargetReference;
import machine.Peripheral;
import machine.SymbolicPosition;
import machine.UnidirectionalPath;
import setting.SettingUtil;
import setting.Settings;

/**
 * This validator validates whether:
 * <ul>
 * <li>Peripheral contains incompatible paths, see {@link #getIncompatibleTargetReferences(Path, Path)}</li>
 * </ul>
 */
public class MachineEValidator implements EValidator {
    public static final MachineEValidator INSTANCE = new MachineEValidator();

    @Override
    public boolean validate(EDataType eDataType, Object value, DiagnosticChain diagnostics,
            Map<Object, Object> context)
    {
        return true;
    }

    @Override
    public boolean validate(EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return null == eObject ? true : validate(eObject.eClass(), eObject, diagnostics, context);
    }

    @Override
    public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        BufferedDiagnosticChain bufferedDiagnostics = new BufferedDiagnosticChain(diagnostics);
        switch (eClass.getClassifierID()) {
            case MachinePackage.MACHINE:
                validateMachine((Machine)eObject, bufferedDiagnostics);
                break;
        }
        return bufferedDiagnostics.getMaxSeverity() < WARNING;
    }

    private void validateMachine(Machine machine, DiagnosticChain diagnostics) {
        try {
            Settings settings = SettingUtil.getSettings(machine.eResource());
            MotionCalculatorExtension motionCalculator = MotionCalculatorExtension.getSelectedMotionCalculator();
            MotionCalculatorHelper helper = new MotionCalculatorHelper(settings, motionCalculator);
            for (Path path: from(machine.getResources()).xcollect(r -> r.getPeripherals())
                    .xcollect(p -> p.getPaths()))
            {
                for (IResource resource: getUsedResources(path, settings)) {
                    switch (path.eClass().getClassifierID()) {
                        case MachinePackage.UNIDIRECTIONAL_PATH:
                            validateUnidirectionalPath((UnidirectionalPath)path, resource, helper, diagnostics);
                            break;
                        case MachinePackage.BIDIRECTIONAL_PATH:
                            validateBidirectionalPath((BidirectionalPath)path, resource, helper, diagnostics);
                            break;
                        case MachinePackage.FULL_MESH_PATH:
                            validateFullMeshPath((FullMeshPath)path, resource, helper, diagnostics);
                            break;
                    }
                }
            }
        } catch (IOException | MotionException e) {
            writeDiagnostic(diagnostics, Diagnostic.INFO, "Timing validation disabled: " + e.getMessage(), machine);
        }
    }

    private Collection<IResource> getUsedResources(Path path, Settings settings) {
        return getUsedResources(path.getPeripheral(), settings);
    }

    private Collection<IResource> getUsedResources(Peripheral peripheral, Settings settings) {
        return settings.getTransitivePhysicalSettings().stream().filter(e -> peripheral.equals(e.getPeripheral()))
                .map(e -> e.getResource()).collect(Collectors.toSet());
    }

    private void validateUnidirectionalPath(UnidirectionalPath path, IResource resource, MotionCalculatorHelper helper,
            DiagnosticChain diagnostics)
    {
        try {
            // Validate multi-axes
            AxesLocation source = helper.getAxesLocation(resource, path.getSource());
            AxesLocation target = helper.getAxesLocation(resource, safeGetPosition(path.getTarget()));

            Set<Axis> movingAxes = source.getMovingAxes(target);
            if (movingAxes.size() > 1) {
                writeDiagnostic(diagnostics, WARNING,
                        format("Axes %s are moving simultaneously when moving from %s to %s",
                                from(movingAxes).xcollectOne(a -> a.getName()), source.getPosition().getName(),
                                target.getPosition().getName()),
                        path);
            }
        } catch (SpecificationException e) {
            writeDiagnostic(diagnostics, Diagnostic.INFO, "Physical settings validation disabled: " + e.getMessage(),
                    path);
        }
    }

    private void validateBidirectionalPath(BidirectionalPath path, IResource resource, MotionCalculatorHelper helper,
            DiagnosticChain diagnostics)
    {
        try {
            // Validate multi-axes
            AxesLocation source = helper.getAxesLocation(resource,
                    safeGetPosition(safeGetIndex(path.getEndPoints(), 0)));
            AxesLocation target = helper.getAxesLocation(resource,
                    safeGetPosition(safeGetIndex(path.getEndPoints(), 1)));

            Set<Axis> movingAxes = source.getMovingAxes(target);
            if (movingAxes.size() > 1) {
                writeDiagnostic(diagnostics, WARNING,
                        format("Axes %s are moving simultaneously when moving between %s and %s",
                                from(movingAxes).xcollectOne(a -> a.getName()), source.getPosition().getName(),
                                target.getPosition().getName()),
                        path);
            }
        } catch (SpecificationException e) {
            writeDiagnostic(diagnostics, Diagnostic.INFO, "Physical settings validation disabled: " + e.getMessage(),
                    path);
        }
    }

    private void validateFullMeshPath(FullMeshPath path, IResource resource, MotionCalculatorHelper helper,
            DiagnosticChain diagnostics)
    {
        try {
            final List<PathTargetReference> endPoints = path.getEndPoints();

            // Validate multi-axes
            for (int i = 0; i < endPoints.size(); i++) {
                AxesLocation source = helper.getAxesLocation(resource, safeGetPosition(endPoints.get(i)));
                for (int j = i + 1; j < endPoints.size(); j++) {
                    AxesLocation target = helper.getAxesLocation(resource, safeGetPosition(endPoints.get(j)));

                    Set<Axis> movingAxes = source.getMovingAxes(target);
                    if (movingAxes.size() > 1) {
                        writeDiagnostic(diagnostics, WARNING,
                                format("Axes %s are moving simultaneously when moving between %s and %s",
                                        from(movingAxes).xcollectOne(a -> a.getName()), source.getPosition().getName(),
                                        target.getPosition().getName()),
                                path);
                    }
                }
            }
        } catch (SpecificationException e) {
            writeDiagnostic(diagnostics, Diagnostic.INFO, "Physical settings validation disabled: " + e.getMessage(),
                    path);
        }
    }

    private SymbolicPosition safeGetPosition(PathTargetReference pathTargetReference) {
        return null != pathTargetReference ? pathTargetReference.getPosition() : null;
    }

    private <T> T safeGetIndex(List<T> coll, int index) {
        return index < coll.size() ? coll.get(index) : null;
    }

    private void writeDiagnostic(DiagnosticChain diagnostics, int severity, String message, EObject... data) {
        diagnostics.add(new BasicDiagnostic(severity, Activator.BUNDLE_NAME, 0, message, data));
    }
}
