/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package activity.impl;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.queries.QueryableIterable;

import activity.PeripheralAction;
import activity.ResourceAction;
import machine.IResource;
import machine.Peripheral;

public final class ActivityQueries {
    private ActivityQueries() {
        /* Empty */
    }

    public static <T extends ResourceAction> QueryableIterable<T> getActionsFor(final IResource resource,
            final Class<T> clazz, Iterable<Node> nodes)
    {
        return from(nodes).objectsOfKind(clazz).select(n -> n.getResource() == resource);
    }

    public static <T extends PeripheralAction> QueryableIterable<T> getActionsFor(final IResource resource,
            final Peripheral peripheral, final Class<T> clazz, Iterable<Node> nodes)
    {
        return from(nodes).objectsOfKind(clazz)
                .select(n -> n.getPeripheral() == peripheral && n.getResource() == resource);
    }
}
