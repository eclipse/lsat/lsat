/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
/**
 */

package activity.util;

import static activity.util.ActivityUtil.addClaim;
import static activity.util.ActivityUtil.addEdge;
import static activity.util.ActivityUtil.addRelease;
import static activity.util.ActivityUtil.getClaims;
import static activity.util.ActivityUtil.getPassiveName;
import static activity.util.ActivityUtil.getReleases;
import static activity.util.ActivityUtil.process;
import static org.eclipse.lsat.common.util.IterableUtil.first;

import activity.Activity;
import activity.ActivitySet;
import activity.Claim;
import activity.Release;
import activity.ResourceAction;
import machine.IResource;
import machine.Machine;
import machine.MachineFactory;
import machine.Resource;
import machine.ResourceType;

/**
 * Converts an activity set with passive claims into activities with an extra resource for the passive claims.
 * <br>
 * The resulting activities are organised such that activities with passive claims may execute in parallel but not with
 * activities that have claimed the resource non passive.
 * <br>
 * Conversion rules are described in the transform method.
 */
public class PassiveClaimTransformer {

    private PassiveClaimTransformer() {
        // Empty
    }

    /**
     * Adapt dispatching to handle passive claims.
     * <br>
     * Given a resource A: <br>
     * If A contains passive claims in any dispatched activity then create resource PA. <br>
     * <b>For an activity with actions performed on resource A:</b>
     * <ul>
     * <li>1.1 Claim PA just before the Claim A</li>
     * <li>1.2 Release PA just after Release A</li>
     * </ul>
     * <b>For an activity that passively claims resource A:</b>
     * <ul>
     * <li>2.1 Release A immediately after claiming A</li>
     * <li>2.2 For release A replace resource A with resource PA</li>
     * <li>2.3 Claim PA immediately before releasing it</li>
     * </ul>
     */
    public static void transform(ActivitySet activities) {
        //!!don't change the order passive resources are created in processRelease
        process(activities, Release.class, PassiveClaimTransformer::processRelease);
        process(activities, Claim.class, PassiveClaimTransformer::processClaim);
        process(activities, ResourceAction.class, ActivityUtil::insertSyncBars);
        // set all passive claims to active because after this transformation there are no active claims anymore.
        process(activities, Claim.class, (it) -> it.setPassive(false));
    }

    private static void processRelease(Release release) {
        Activity activity = (Activity)release.getGraph();
        Claim claim = first(getClaims(activity, release.getResource()));
         if (claim.isPassive()) {
             IResource resource = release.getResource();
             Resource passiveResource = getPassiveResource(resource, true);
             //for passive release replace resource with resource passive resource
             release.setResource(passiveResource);
             //claim PA immediately before releasing it
             Claim pClaim = addPassiveClaim(getPassiveName(resource), activity, passiveResource);
             // also  mark this claim as passive
             pClaim.setPassive(true);
             addEdge(activity, pClaim, release);
             //release original passive claims immediately after claim
             Release iRelease = addPassiveRelease(getPassiveName(claim.getResource()), activity, claim.getResource());
             addEdge(activity, claim, iRelease);
        }
    }

    private static void processClaim(Claim claim) {
        if (!claim.isPassive()) {
            processActiveClaim(claim);
        }
    }

    private static void processActiveClaim(Claim claim) {
        IResource resource = claim.getResource();
        Resource passiveResource = getPassiveResource(resource, false);
        if (passiveResource == null) {
            return;
        }
        Activity activity = (Activity)claim.getGraph();
        //get corresponding release
        Release release = first(getReleases(activity, resource));

        Claim pClaim = addPassiveClaim(getPassiveName(resource), activity, passiveResource);
        Release pRelease = addPassiveRelease(getPassiveName(resource), activity, passiveResource);
        addEdge(activity, pClaim, claim);
        addEdge(activity, release, pRelease);
    }

     private static Resource getPassiveResource(IResource resource, boolean create) {
        Machine machine = (Machine)resource.getResource().eContainer();
        for (Resource pResource: machine.getResources()) {
            if (pResource.getName().equals(getPassiveName(resource))) {
                return pResource;
            }
        }
        if (create) {
            return createPassiveResource(resource);
        }
        return null;
    }

    private static Resource createPassiveResource(IResource resource) {
        Machine machine = (Machine)resource.getResource().eContainer();
        Resource pResource = MachineFactory.eINSTANCE.createResource();
        pResource.setName(getPassiveName(resource));
        pResource.setResourceType(ResourceType.PASSIVE);
        machine.getResources().add(pResource);
        return pResource;
    }

    private static Claim addPassiveClaim(String name, Activity activity, IResource passiveResource) {
        Claim claim = addClaim(activity, passiveResource);
        claim.setName("C_" + name);
        return claim;
    }

    private static Release addPassiveRelease(String name, Activity activity, IResource passiveResource) {
        Release release = addRelease(activity, passiveResource);
        release.setName("R_" + name);
        return release;
    }

}
