/**
 */
package activity.impl;

import activity.ActivityPackage;
import activity.RequireEvent;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Require EventAction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequireEventImpl extends EventActionImpl implements RequireEvent {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected RequireEventImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.REQUIRE_EVENT;
    }

} //RequireEventImpl
