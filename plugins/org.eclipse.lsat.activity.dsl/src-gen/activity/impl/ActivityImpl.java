/**
 */
package activity.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.lsat.common.graph.directed.editable.impl.EditableDirectedGraphImpl;

import activity.Activity;
import activity.ActivityPackage;
import activity.LocationPrerequisite;
import activity.ResourceAction;
import machine.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link activity.impl.ActivityImpl#getPrerequisites <em>Prerequisites</em>}</li>
 *   <li>{@link activity.impl.ActivityImpl#getOriginalName <em>Original Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActivityImpl extends EditableDirectedGraphImpl implements Activity {
	/**
     * The cached value of the '{@link #getPrerequisites() <em>Prerequisites</em>}' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPrerequisites()
     * @generated
     * @ordered
     */
	protected EList<LocationPrerequisite> prerequisites;

	/**
     * The default value of the '{@link #getOriginalName() <em>Original Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getOriginalName()
     * @generated
     * @ordered
     */
	protected static final String ORIGINAL_NAME_EDEFAULT = null;

	/**
     * The cached value of the '{@link #getOriginalName() <em>Original Name</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getOriginalName()
     * @generated
     * @ordered
     */
	protected String originalName = ORIGINAL_NAME_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActivityImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.ACTIVITY;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<LocationPrerequisite> getPrerequisites() {
        if (prerequisites == null)
        {
            prerequisites = new EObjectContainmentWithInverseEList<LocationPrerequisite>(LocationPrerequisite.class, this, ActivityPackage.ACTIVITY__PREREQUISITES, ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY);
        }
        return prerequisites;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String getOriginalName() {
        return originalName;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setOriginalName(String newOriginalName) {
        String oldOriginalName = originalName;
        originalName = newOriginalName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTIVITY__ORIGINAL_NAME, oldOriginalName, originalName));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean isExpanded() {
        if (getOriginalName()==null) return false; 
        return  !java.util.Objects.equals(getName(),getOriginalName());
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public EList<Resource> getResourcesNeedingItem() {
        return new org.eclipse.emf.common.util.BasicEList<Resource>(
            getNodes().stream().filter(ResourceAction.class::isInstance).map(ResourceAction.class::cast)
            .map(ResourceAction::getResource).filter(machine.Resource.class::isInstance)
            .map(machine.Resource.class::cast).filter(r -> !r.getItems().isEmpty())
            .distinct()
            .sorted( (r1,r2) ->  r1.getName().compareTo(r2.getName()))
            .collect(java.util.stream.Collectors.toCollection(java.util.LinkedHashSet::new)));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY__PREREQUISITES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getPrerequisites()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY__PREREQUISITES:
                return ((InternalEList<?>)getPrerequisites()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY__PREREQUISITES:
                return getPrerequisites();
            case ActivityPackage.ACTIVITY__ORIGINAL_NAME:
                return getOriginalName();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY__PREREQUISITES:
                getPrerequisites().clear();
                getPrerequisites().addAll((Collection<? extends LocationPrerequisite>)newValue);
                return;
            case ActivityPackage.ACTIVITY__ORIGINAL_NAME:
                setOriginalName((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY__PREREQUISITES:
                getPrerequisites().clear();
                return;
            case ActivityPackage.ACTIVITY__ORIGINAL_NAME:
                setOriginalName(ORIGINAL_NAME_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.ACTIVITY__PREREQUISITES:
                return prerequisites != null && !prerequisites.isEmpty();
            case ActivityPackage.ACTIVITY__ORIGINAL_NAME:
                return ORIGINAL_NAME_EDEFAULT == null ? originalName != null : !ORIGINAL_NAME_EDEFAULT.equals(originalName);
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case ActivityPackage.ACTIVITY___IS_EXPANDED:
                return isExpanded();
            case ActivityPackage.ACTIVITY___GET_RESOURCES_NEEDING_ITEM:
                return getResourcesNeedingItem();
        }
        return super.eInvoke(operationID, arguments);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (originalName: ");
        result.append(originalName);
        result.append(')');
        return result.toString();
    }

} //ActivityImpl
