/**
 */
package activity.impl;

import activity.Activity;
import activity.ActivityPackage;
import activity.LocationPrerequisite;
import java.lang.reflect.InvocationTargetException;
import machine.HasResourcePeripheral;
import machine.IResource;
import machine.Peripheral;
import machine.SymbolicPosition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Location Prerequisite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link activity.impl.LocationPrerequisiteImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link activity.impl.LocationPrerequisiteImpl#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link activity.impl.LocationPrerequisiteImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link activity.impl.LocationPrerequisiteImpl#getActivity <em>Activity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationPrerequisiteImpl extends MinimalEObjectImpl.Container implements LocationPrerequisite {
	/**
     * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getResource()
     * @generated
     * @ordered
     */
	protected IResource resource;

	/**
     * The cached value of the '{@link #getPeripheral() <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPeripheral()
     * @generated
     * @ordered
     */
	protected Peripheral peripheral;

	/**
     * The cached value of the '{@link #getPosition() <em>Position</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPosition()
     * @generated
     * @ordered
     */
	protected SymbolicPosition position;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected LocationPrerequisiteImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.LOCATION_PREREQUISITE;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public IResource getResource() {
        if (resource != null && resource.eIsProxy())
        {
            InternalEObject oldResource = (InternalEObject)resource;
            resource = (IResource)eResolveProxy(oldResource);
            if (resource != oldResource)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.LOCATION_PREREQUISITE__RESOURCE, oldResource, resource));
            }
        }
        return resource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public IResource basicGetResource() {
        return resource;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setResource(IResource newResource) {
        IResource oldResource = resource;
        resource = newResource;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.LOCATION_PREREQUISITE__RESOURCE, oldResource, resource));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Peripheral getPeripheral() {
        if (peripheral != null && peripheral.eIsProxy())
        {
            InternalEObject oldPeripheral = (InternalEObject)peripheral;
            peripheral = (Peripheral)eResolveProxy(oldPeripheral);
            if (peripheral != oldPeripheral)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.LOCATION_PREREQUISITE__PERIPHERAL, oldPeripheral, peripheral));
            }
        }
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Peripheral basicGetPeripheral() {
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPeripheral(Peripheral newPeripheral) {
        Peripheral oldPeripheral = peripheral;
        peripheral = newPeripheral;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.LOCATION_PREREQUISITE__PERIPHERAL, oldPeripheral, peripheral));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SymbolicPosition getPosition() {
        if (position != null && position.eIsProxy())
        {
            InternalEObject oldPosition = (InternalEObject)position;
            position = (SymbolicPosition)eResolveProxy(oldPosition);
            if (position != oldPosition)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.LOCATION_PREREQUISITE__POSITION, oldPosition, position));
            }
        }
        return position;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public SymbolicPosition basicGetPosition() {
        return position;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPosition(SymbolicPosition newPosition) {
        SymbolicPosition oldPosition = position;
        position = newPosition;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.LOCATION_PREREQUISITE__POSITION, oldPosition, position));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Activity getActivity() {
        if (eContainerFeatureID() != ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY) return null;
        return (Activity)eInternalContainer();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetActivity(Activity newActivity, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject)newActivity, ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY, msgs);
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setActivity(Activity newActivity) {
        if (newActivity != eInternalContainer() || (eContainerFeatureID() != ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY && newActivity != null))
        {
            if (EcoreUtil.isAncestor(this, newActivity))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newActivity != null)
                msgs = ((InternalEObject)newActivity).eInverseAdd(this, ActivityPackage.ACTIVITY__PREREQUISITES, Activity.class, msgs);
            msgs = basicSetActivity(newActivity, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY, newActivity, newActivity));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String fqn() {
        return (getResource()!=null && getPeripheral() != null) ? getResource().fqn()+"."+getPeripheral().getName():null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean rpEquals(final HasResourcePeripheral cmp) {
        if (cmp ==null) return false; 
        return org.eclipse.emf.ecore.util.EcoreUtil.equals(getResource(),cmp.getResource()) && org.eclipse.emf.ecore.util.EcoreUtil.equals(getPeripheral(),cmp.getPeripheral());
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetActivity((Activity)otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY:
                return basicSetActivity(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID())
        {
            case ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY:
                return eInternalContainer().eInverseRemove(this, ActivityPackage.ACTIVITY__PREREQUISITES, Activity.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case ActivityPackage.LOCATION_PREREQUISITE__RESOURCE:
                if (resolve) return getResource();
                return basicGetResource();
            case ActivityPackage.LOCATION_PREREQUISITE__PERIPHERAL:
                if (resolve) return getPeripheral();
                return basicGetPeripheral();
            case ActivityPackage.LOCATION_PREREQUISITE__POSITION:
                if (resolve) return getPosition();
                return basicGetPosition();
            case ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY:
                return getActivity();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case ActivityPackage.LOCATION_PREREQUISITE__RESOURCE:
                setResource((IResource)newValue);
                return;
            case ActivityPackage.LOCATION_PREREQUISITE__PERIPHERAL:
                setPeripheral((Peripheral)newValue);
                return;
            case ActivityPackage.LOCATION_PREREQUISITE__POSITION:
                setPosition((SymbolicPosition)newValue);
                return;
            case ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY:
                setActivity((Activity)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.LOCATION_PREREQUISITE__RESOURCE:
                setResource((IResource)null);
                return;
            case ActivityPackage.LOCATION_PREREQUISITE__PERIPHERAL:
                setPeripheral((Peripheral)null);
                return;
            case ActivityPackage.LOCATION_PREREQUISITE__POSITION:
                setPosition((SymbolicPosition)null);
                return;
            case ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY:
                setActivity((Activity)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.LOCATION_PREREQUISITE__RESOURCE:
                return resource != null;
            case ActivityPackage.LOCATION_PREREQUISITE__PERIPHERAL:
                return peripheral != null;
            case ActivityPackage.LOCATION_PREREQUISITE__POSITION:
                return position != null;
            case ActivityPackage.LOCATION_PREREQUISITE__ACTIVITY:
                return getActivity() != null;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case ActivityPackage.LOCATION_PREREQUISITE___FQN:
                return fqn();
            case ActivityPackage.LOCATION_PREREQUISITE___RP_EQUALS__HASRESOURCEPERIPHERAL:
                return rpEquals((HasResourcePeripheral)arguments.get(0));
        }
        return super.eInvoke(operationID, arguments);
    }

} //LocationPrerequisiteImpl
