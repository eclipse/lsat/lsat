/**
 */
package activity.impl;

import activity.Action;
import activity.ActivityPackage;
import activity.PeripheralAction;
import activity.ResourceAction;
import activity.SchedulingType;

import activity.TracePoint;
import java.lang.reflect.InvocationTargetException;
import machine.HasResourcePeripheral;
import machine.IResource;
import machine.Peripheral;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.graph.directed.editable.SourceReference;
import org.eclipse.lsat.common.graph.directed.editable.TargetReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Peripheral Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link activity.impl.PeripheralActionImpl#getName <em>Name</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getGraph <em>Graph</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getIncomingEdges <em>Incoming Edges</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getOutgoingEdges <em>Outgoing Edges</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getTargetReferences <em>Target References</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getSourceReferences <em>Source References</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getOuterEntry <em>Outer Entry</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getEntry <em>Entry</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getExit <em>Exit</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getOuterExit <em>Outer Exit</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link activity.impl.PeripheralActionImpl#getSchedulingType <em>Scheduling Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PeripheralActionImpl extends MinimalEObjectImpl.Container implements PeripheralAction {
	/**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getTargetReferences() <em>Target References</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getTargetReferences()
     * @generated
     * @ordered
     */
    protected EList<TargetReference> targetReferences;

    /**
     * The cached value of the '{@link #getSourceReferences() <em>Source References</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getSourceReferences()
     * @generated
     * @ordered
     */
    protected EList<SourceReference> sourceReferences;

    /**
     * The cached value of the '{@link #getOuterEntry() <em>Outer Entry</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getOuterEntry()
     * @generated
     * @ordered
     */
    protected TracePoint outerEntry;

    /**
     * The cached value of the '{@link #getEntry() <em>Entry</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getEntry()
     * @generated
     * @ordered
     */
    protected TracePoint entry;

    /**
     * The cached value of the '{@link #getExit() <em>Exit</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getExit()
     * @generated
     * @ordered
     */
    protected TracePoint exit;

    /**
     * The cached value of the '{@link #getOuterExit() <em>Outer Exit</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getOuterExit()
     * @generated
     * @ordered
     */
    protected TracePoint outerExit;

    /**
     * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getResource()
     * @generated
     * @ordered
     */
    protected IResource resource;

    /**
     * The cached value of the '{@link #getPeripheral() <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getPeripheral()
     * @generated
     * @ordered
     */
	protected Peripheral peripheral;

	/**
     * The default value of the '{@link #getSchedulingType() <em>Scheduling Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSchedulingType()
     * @generated
     * @ordered
     */
	protected static final SchedulingType SCHEDULING_TYPE_EDEFAULT = SchedulingType.ASAP;

	/**
     * The cached value of the '{@link #getSchedulingType() <em>Scheduling Type</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getSchedulingType()
     * @generated
     * @ordered
     */
	protected SchedulingType schedulingType = SCHEDULING_TYPE_EDEFAULT;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected PeripheralActionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.PERIPHERAL_ACTION;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getName()
    {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setName(String newName)
    {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EditableDirectedGraph getGraph()
    {
        if (eContainerFeatureID() != ActivityPackage.PERIPHERAL_ACTION__GRAPH) return null;
        return (EditableDirectedGraph)eInternalContainer();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetGraph(EditableDirectedGraph newGraph, NotificationChain msgs)
    {
        msgs = eBasicSetContainer((InternalEObject)newGraph, ActivityPackage.PERIPHERAL_ACTION__GRAPH, msgs);
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setGraph(EditableDirectedGraph newGraph)
    {
        if (newGraph != eInternalContainer() || (eContainerFeatureID() != ActivityPackage.PERIPHERAL_ACTION__GRAPH && newGraph != null))
        {
            if (EcoreUtil.isAncestor(this, newGraph))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newGraph != null)
                msgs = ((InternalEObject)newGraph).eInverseAdd(this, EdgPackage.EDITABLE_DIRECTED_GRAPH__NODES, EditableDirectedGraph.class, msgs);
            msgs = basicSetGraph(newGraph, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__GRAPH, newGraph, newGraph));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Edge> getIncomingEdges()
    {
        EList<Edge> result = new  org.eclipse.emf.common.util.BasicEList<Edge>(
                getSourceReferences().size() + getTargetReferences().size());
        org.eclipse.lsat.common.util.CollectionUtil.addAll(result, org.eclipse.lsat.common.graph.directed.editable.EdgQueries.getIncomingEdges(this));
        return new org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList<Edge>(
                this, EdgPackage.Literals.NODE__INCOMING_EDGES, result.size(), result.toArray());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<Edge> getOutgoingEdges()
    {
        EList<Edge> result = new  org.eclipse.emf.common.util.BasicEList<Edge>(getSourceReferences().size());
        org.eclipse.lsat.common.util.CollectionUtil.addAll(result, org.eclipse.lsat.common.graph.directed.editable.EdgQueries.getOutgoingEdges(this));
        return new org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList<Edge>(
                this, EdgPackage.Literals.NODE__OUTGOING_EDGES, result.size(), result.toArray());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<TargetReference> getTargetReferences()
    {
        if (targetReferences == null)
        {
            targetReferences = new EObjectWithInverseEList<TargetReference>(TargetReference.class, this, ActivityPackage.PERIPHERAL_ACTION__TARGET_REFERENCES, EdgPackage.TARGET_REFERENCE__NODE);
        }
        return targetReferences;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<SourceReference> getSourceReferences()
    {
        if (sourceReferences == null)
        {
            sourceReferences = new EObjectWithInverseEList<SourceReference>(SourceReference.class, this, ActivityPackage.PERIPHERAL_ACTION__SOURCE_REFERENCES, EdgPackage.SOURCE_REFERENCE__NODE);
        }
        return sourceReferences;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TracePoint getOuterEntry()
    {
        return outerEntry;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetOuterEntry(TracePoint newOuterEntry, NotificationChain msgs)
    {
        TracePoint oldOuterEntry = outerEntry;
        outerEntry = newOuterEntry;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY, oldOuterEntry, newOuterEntry);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setOuterEntry(TracePoint newOuterEntry)
    {
        if (newOuterEntry != outerEntry)
        {
            NotificationChain msgs = null;
            if (outerEntry != null)
                msgs = ((InternalEObject)outerEntry).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY, null, msgs);
            if (newOuterEntry != null)
                msgs = ((InternalEObject)newOuterEntry).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY, null, msgs);
            msgs = basicSetOuterEntry(newOuterEntry, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY, newOuterEntry, newOuterEntry));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TracePoint getEntry()
    {
        return entry;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetEntry(TracePoint newEntry, NotificationChain msgs)
    {
        TracePoint oldEntry = entry;
        entry = newEntry;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__ENTRY, oldEntry, newEntry);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setEntry(TracePoint newEntry)
    {
        if (newEntry != entry)
        {
            NotificationChain msgs = null;
            if (entry != null)
                msgs = ((InternalEObject)entry).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__ENTRY, null, msgs);
            if (newEntry != null)
                msgs = ((InternalEObject)newEntry).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__ENTRY, null, msgs);
            msgs = basicSetEntry(newEntry, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__ENTRY, newEntry, newEntry));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TracePoint getExit()
    {
        return exit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetExit(TracePoint newExit, NotificationChain msgs)
    {
        TracePoint oldExit = exit;
        exit = newExit;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__EXIT, oldExit, newExit);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setExit(TracePoint newExit)
    {
        if (newExit != exit)
        {
            NotificationChain msgs = null;
            if (exit != null)
                msgs = ((InternalEObject)exit).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__EXIT, null, msgs);
            if (newExit != null)
                msgs = ((InternalEObject)newExit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__EXIT, null, msgs);
            msgs = basicSetExit(newExit, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__EXIT, newExit, newExit));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TracePoint getOuterExit()
    {
        return outerExit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetOuterExit(TracePoint newOuterExit, NotificationChain msgs)
    {
        TracePoint oldOuterExit = outerExit;
        outerExit = newOuterExit;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT, oldOuterExit, newOuterExit);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setOuterExit(TracePoint newOuterExit)
    {
        if (newOuterExit != outerExit)
        {
            NotificationChain msgs = null;
            if (outerExit != null)
                msgs = ((InternalEObject)outerExit).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT, null, msgs);
            if (newOuterExit != null)
                msgs = ((InternalEObject)newOuterExit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT, null, msgs);
            msgs = basicSetOuterExit(newOuterExit, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT, newOuterExit, newOuterExit));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IResource getResource()
    {
        if (resource != null && resource.eIsProxy())
        {
            InternalEObject oldResource = (InternalEObject)resource;
            resource = (IResource)eResolveProxy(oldResource);
            if (resource != oldResource)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.PERIPHERAL_ACTION__RESOURCE, oldResource, resource));
            }
        }
        return resource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public IResource basicGetResource()
    {
        return resource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setResource(IResource newResource)
    {
        IResource oldResource = resource;
        resource = newResource;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__RESOURCE, oldResource, resource));
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Peripheral getPeripheral() {
        if (peripheral != null && peripheral.eIsProxy())
        {
            InternalEObject oldPeripheral = (InternalEObject)peripheral;
            peripheral = (Peripheral)eResolveProxy(oldPeripheral);
            if (peripheral != oldPeripheral)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActivityPackage.PERIPHERAL_ACTION__PERIPHERAL, oldPeripheral, peripheral));
            }
        }
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public Peripheral basicGetPeripheral() {
        return peripheral;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setPeripheral(Peripheral newPeripheral) {
        Peripheral oldPeripheral = peripheral;
        peripheral = newPeripheral;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__PERIPHERAL, oldPeripheral, peripheral));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public SchedulingType getSchedulingType() {
        return schedulingType;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setSchedulingType(SchedulingType newSchedulingType) {
        SchedulingType oldSchedulingType = schedulingType;
        schedulingType = newSchedulingType == null ? SCHEDULING_TYPE_EDEFAULT : newSchedulingType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.PERIPHERAL_ACTION__SCHEDULING_TYPE, oldSchedulingType, schedulingType));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String fqn() {
        return (getResource()!=null && getPeripheral() != null) ? getResource().fqn()+"."+getPeripheral().getName():null;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean rpEquals(final HasResourcePeripheral cmp) {
        if (cmp ==null) return false; 
        return org.eclipse.emf.ecore.util.EcoreUtil.equals(getResource(),cmp.getResource()) && org.eclipse.emf.ecore.util.EcoreUtil.equals(getPeripheral(),cmp.getPeripheral());
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
    {
        switch (featureID)
        {
            case ActivityPackage.PERIPHERAL_ACTION__GRAPH:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetGraph((EditableDirectedGraph)otherEnd, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__TARGET_REFERENCES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getTargetReferences()).basicAdd(otherEnd, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__SOURCE_REFERENCES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getSourceReferences()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
    {
        switch (featureID)
        {
            case ActivityPackage.PERIPHERAL_ACTION__GRAPH:
                return basicSetGraph(null, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__TARGET_REFERENCES:
                return ((InternalEList<?>)getTargetReferences()).basicRemove(otherEnd, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__SOURCE_REFERENCES:
                return ((InternalEList<?>)getSourceReferences()).basicRemove(otherEnd, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY:
                return basicSetOuterEntry(null, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__ENTRY:
                return basicSetEntry(null, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__EXIT:
                return basicSetExit(null, msgs);
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT:
                return basicSetOuterExit(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
    {
        switch (eContainerFeatureID())
        {
            case ActivityPackage.PERIPHERAL_ACTION__GRAPH:
                return eInternalContainer().eInverseRemove(this, EdgPackage.EDITABLE_DIRECTED_GRAPH__NODES, EditableDirectedGraph.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case ActivityPackage.PERIPHERAL_ACTION__NAME:
                return getName();
            case ActivityPackage.PERIPHERAL_ACTION__GRAPH:
                return getGraph();
            case ActivityPackage.PERIPHERAL_ACTION__INCOMING_EDGES:
                return getIncomingEdges();
            case ActivityPackage.PERIPHERAL_ACTION__OUTGOING_EDGES:
                return getOutgoingEdges();
            case ActivityPackage.PERIPHERAL_ACTION__TARGET_REFERENCES:
                return getTargetReferences();
            case ActivityPackage.PERIPHERAL_ACTION__SOURCE_REFERENCES:
                return getSourceReferences();
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY:
                return getOuterEntry();
            case ActivityPackage.PERIPHERAL_ACTION__ENTRY:
                return getEntry();
            case ActivityPackage.PERIPHERAL_ACTION__EXIT:
                return getExit();
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT:
                return getOuterExit();
            case ActivityPackage.PERIPHERAL_ACTION__RESOURCE:
                if (resolve) return getResource();
                return basicGetResource();
            case ActivityPackage.PERIPHERAL_ACTION__PERIPHERAL:
                if (resolve) return getPeripheral();
                return basicGetPeripheral();
            case ActivityPackage.PERIPHERAL_ACTION__SCHEDULING_TYPE:
                return getSchedulingType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case ActivityPackage.PERIPHERAL_ACTION__NAME:
                setName((String)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__GRAPH:
                setGraph((EditableDirectedGraph)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY:
                setOuterEntry((TracePoint)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__ENTRY:
                setEntry((TracePoint)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__EXIT:
                setExit((TracePoint)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT:
                setOuterExit((TracePoint)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__RESOURCE:
                setResource((IResource)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__PERIPHERAL:
                setPeripheral((Peripheral)newValue);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__SCHEDULING_TYPE:
                setSchedulingType((SchedulingType)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.PERIPHERAL_ACTION__NAME:
                setName(NAME_EDEFAULT);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__GRAPH:
                setGraph((EditableDirectedGraph)null);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY:
                setOuterEntry((TracePoint)null);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__ENTRY:
                setEntry((TracePoint)null);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__EXIT:
                setExit((TracePoint)null);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT:
                setOuterExit((TracePoint)null);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__RESOURCE:
                setResource((IResource)null);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__PERIPHERAL:
                setPeripheral((Peripheral)null);
                return;
            case ActivityPackage.PERIPHERAL_ACTION__SCHEDULING_TYPE:
                setSchedulingType(SCHEDULING_TYPE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.PERIPHERAL_ACTION__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case ActivityPackage.PERIPHERAL_ACTION__GRAPH:
                return getGraph() != null;
            case ActivityPackage.PERIPHERAL_ACTION__INCOMING_EDGES:
                return !getIncomingEdges().isEmpty();
            case ActivityPackage.PERIPHERAL_ACTION__OUTGOING_EDGES:
                return !getOutgoingEdges().isEmpty();
            case ActivityPackage.PERIPHERAL_ACTION__TARGET_REFERENCES:
                return targetReferences != null && !targetReferences.isEmpty();
            case ActivityPackage.PERIPHERAL_ACTION__SOURCE_REFERENCES:
                return sourceReferences != null && !sourceReferences.isEmpty();
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY:
                return outerEntry != null;
            case ActivityPackage.PERIPHERAL_ACTION__ENTRY:
                return entry != null;
            case ActivityPackage.PERIPHERAL_ACTION__EXIT:
                return exit != null;
            case ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT:
                return outerExit != null;
            case ActivityPackage.PERIPHERAL_ACTION__RESOURCE:
                return resource != null;
            case ActivityPackage.PERIPHERAL_ACTION__PERIPHERAL:
                return peripheral != null;
            case ActivityPackage.PERIPHERAL_ACTION__SCHEDULING_TYPE:
                return schedulingType != SCHEDULING_TYPE_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass)
    {
        if (baseClass == Node.class)
        {
            switch (derivedFeatureID)
            {
                case ActivityPackage.PERIPHERAL_ACTION__NAME: return EdgPackage.NODE__NAME;
                case ActivityPackage.PERIPHERAL_ACTION__GRAPH: return EdgPackage.NODE__GRAPH;
                case ActivityPackage.PERIPHERAL_ACTION__INCOMING_EDGES: return EdgPackage.NODE__INCOMING_EDGES;
                case ActivityPackage.PERIPHERAL_ACTION__OUTGOING_EDGES: return EdgPackage.NODE__OUTGOING_EDGES;
                case ActivityPackage.PERIPHERAL_ACTION__TARGET_REFERENCES: return EdgPackage.NODE__TARGET_REFERENCES;
                case ActivityPackage.PERIPHERAL_ACTION__SOURCE_REFERENCES: return EdgPackage.NODE__SOURCE_REFERENCES;
                default: return -1;
            }
        }
        if (baseClass == Action.class)
        {
            switch (derivedFeatureID)
            {
                case ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY: return ActivityPackage.ACTION__OUTER_ENTRY;
                case ActivityPackage.PERIPHERAL_ACTION__ENTRY: return ActivityPackage.ACTION__ENTRY;
                case ActivityPackage.PERIPHERAL_ACTION__EXIT: return ActivityPackage.ACTION__EXIT;
                case ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT: return ActivityPackage.ACTION__OUTER_EXIT;
                default: return -1;
            }
        }
        if (baseClass == ResourceAction.class)
        {
            switch (derivedFeatureID)
            {
                case ActivityPackage.PERIPHERAL_ACTION__RESOURCE: return ActivityPackage.RESOURCE_ACTION__RESOURCE;
                default: return -1;
            }
        }
        return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass)
    {
        if (baseClass == Node.class)
        {
            switch (baseFeatureID)
            {
                case EdgPackage.NODE__NAME: return ActivityPackage.PERIPHERAL_ACTION__NAME;
                case EdgPackage.NODE__GRAPH: return ActivityPackage.PERIPHERAL_ACTION__GRAPH;
                case EdgPackage.NODE__INCOMING_EDGES: return ActivityPackage.PERIPHERAL_ACTION__INCOMING_EDGES;
                case EdgPackage.NODE__OUTGOING_EDGES: return ActivityPackage.PERIPHERAL_ACTION__OUTGOING_EDGES;
                case EdgPackage.NODE__TARGET_REFERENCES: return ActivityPackage.PERIPHERAL_ACTION__TARGET_REFERENCES;
                case EdgPackage.NODE__SOURCE_REFERENCES: return ActivityPackage.PERIPHERAL_ACTION__SOURCE_REFERENCES;
                default: return -1;
            }
        }
        if (baseClass == Action.class)
        {
            switch (baseFeatureID)
            {
                case ActivityPackage.ACTION__OUTER_ENTRY: return ActivityPackage.PERIPHERAL_ACTION__OUTER_ENTRY;
                case ActivityPackage.ACTION__ENTRY: return ActivityPackage.PERIPHERAL_ACTION__ENTRY;
                case ActivityPackage.ACTION__EXIT: return ActivityPackage.PERIPHERAL_ACTION__EXIT;
                case ActivityPackage.ACTION__OUTER_EXIT: return ActivityPackage.PERIPHERAL_ACTION__OUTER_EXIT;
                default: return -1;
            }
        }
        if (baseClass == ResourceAction.class)
        {
            switch (baseFeatureID)
            {
                case ActivityPackage.RESOURCE_ACTION__RESOURCE: return ActivityPackage.PERIPHERAL_ACTION__RESOURCE;
                default: return -1;
            }
        }
        return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID)
        {
            case ActivityPackage.PERIPHERAL_ACTION___FQN:
                return fqn();
            case ActivityPackage.PERIPHERAL_ACTION___RP_EQUALS__HASRESOURCEPERIPHERAL:
                return rpEquals((HasResourcePeripheral)arguments.get(0));
        }
        return super.eInvoke(operationID, arguments);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", schedulingType: ");
        result.append(schedulingType);
        result.append(')');
        return result.toString();
    }

} //PeripheralActionImpl
