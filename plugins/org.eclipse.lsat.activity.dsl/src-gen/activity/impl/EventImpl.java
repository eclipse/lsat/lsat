/**
 */
package activity.impl;

import activity.ActivityPackage;
import activity.Event;

import java.lang.reflect.InvocationTargetException;
import machine.ResourceType;
import machine.impl.ResourceImpl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EventImpl extends ResourceImpl implements Event
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected EventImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return ActivityPackage.Literals.EVENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ResourceType getResourceType()
    {
        return machine.ResourceType.EVENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException
    {
        switch (operationID)
        {
            case ActivityPackage.EVENT___GET_RESOURCE_TYPE:
                return getResourceType();
        }
        return super.eInvoke(operationID, arguments);
    }

} //EventImpl
