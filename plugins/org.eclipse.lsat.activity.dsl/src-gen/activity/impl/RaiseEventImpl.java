/**
 */
package activity.impl;

import activity.ActivityPackage;
import activity.RaiseEvent;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raise EventAction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RaiseEventImpl extends EventActionImpl implements RaiseEvent {
	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected RaiseEventImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.RAISE_EVENT;
    }

} //RaiseEventImpl
