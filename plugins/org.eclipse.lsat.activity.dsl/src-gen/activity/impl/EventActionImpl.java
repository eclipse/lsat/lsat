/**
 */
package activity.impl;

import activity.ActivityPackage;
import activity.EventAction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class EventActionImpl extends ResourceActionImpl implements EventAction
{
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected EventActionImpl()
    {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass()
    {
        return ActivityPackage.Literals.EVENT_ACTION;
    }

} //EventActionImpl
