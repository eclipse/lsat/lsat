/**
 */
package activity.impl;

import activity.Action;
import activity.ActivityPackage;
import activity.TracePoint;
import org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link activity.impl.ActionImpl#getOuterEntry <em>Outer Entry</em>}</li>
 *   <li>{@link activity.impl.ActionImpl#getEntry <em>Entry</em>}</li>
 *   <li>{@link activity.impl.ActionImpl#getExit <em>Exit</em>}</li>
 *   <li>{@link activity.impl.ActionImpl#getOuterExit <em>Outer Exit</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ActionImpl extends NodeImpl implements Action {
	/**
     * The cached value of the '{@link #getOuterEntry() <em>Outer Entry</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getOuterEntry()
     * @generated
     * @ordered
     */
	protected TracePoint outerEntry;

	/**
     * The cached value of the '{@link #getEntry() <em>Entry</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getEntry()
     * @generated
     * @ordered
     */
	protected TracePoint entry;

	/**
     * The cached value of the '{@link #getExit() <em>Exit</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getExit()
     * @generated
     * @ordered
     */
	protected TracePoint exit;

	/**
     * The cached value of the '{@link #getOuterExit() <em>Outer Exit</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getOuterExit()
     * @generated
     * @ordered
     */
	protected TracePoint outerExit;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected ActionImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return ActivityPackage.Literals.ACTION;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TracePoint getOuterEntry() {
        return outerEntry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetOuterEntry(TracePoint newOuterEntry, NotificationChain msgs) {
        TracePoint oldOuterEntry = outerEntry;
        outerEntry = newOuterEntry;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__OUTER_ENTRY, oldOuterEntry, newOuterEntry);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setOuterEntry(TracePoint newOuterEntry) {
        if (newOuterEntry != outerEntry)
        {
            NotificationChain msgs = null;
            if (outerEntry != null)
                msgs = ((InternalEObject)outerEntry).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__OUTER_ENTRY, null, msgs);
            if (newOuterEntry != null)
                msgs = ((InternalEObject)newOuterEntry).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__OUTER_ENTRY, null, msgs);
            msgs = basicSetOuterEntry(newOuterEntry, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__OUTER_ENTRY, newOuterEntry, newOuterEntry));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TracePoint getEntry() {
        return entry;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetEntry(TracePoint newEntry, NotificationChain msgs) {
        TracePoint oldEntry = entry;
        entry = newEntry;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__ENTRY, oldEntry, newEntry);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setEntry(TracePoint newEntry) {
        if (newEntry != entry)
        {
            NotificationChain msgs = null;
            if (entry != null)
                msgs = ((InternalEObject)entry).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__ENTRY, null, msgs);
            if (newEntry != null)
                msgs = ((InternalEObject)newEntry).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__ENTRY, null, msgs);
            msgs = basicSetEntry(newEntry, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__ENTRY, newEntry, newEntry));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TracePoint getExit() {
        return exit;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetExit(TracePoint newExit, NotificationChain msgs) {
        TracePoint oldExit = exit;
        exit = newExit;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__EXIT, oldExit, newExit);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setExit(TracePoint newExit) {
        if (newExit != exit)
        {
            NotificationChain msgs = null;
            if (exit != null)
                msgs = ((InternalEObject)exit).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__EXIT, null, msgs);
            if (newExit != null)
                msgs = ((InternalEObject)newExit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__EXIT, null, msgs);
            msgs = basicSetExit(newExit, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__EXIT, newExit, newExit));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public TracePoint getOuterExit() {
        return outerExit;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public NotificationChain basicSetOuterExit(TracePoint newOuterExit, NotificationChain msgs) {
        TracePoint oldOuterExit = outerExit;
        outerExit = newOuterExit;
        if (eNotificationRequired())
        {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__OUTER_EXIT, oldOuterExit, newOuterExit);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setOuterExit(TracePoint newOuterExit) {
        if (newOuterExit != outerExit)
        {
            NotificationChain msgs = null;
            if (outerExit != null)
                msgs = ((InternalEObject)outerExit).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__OUTER_EXIT, null, msgs);
            if (newOuterExit != null)
                msgs = ((InternalEObject)newOuterExit).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ActivityPackage.ACTION__OUTER_EXIT, null, msgs);
            msgs = basicSetOuterExit(newOuterExit, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ActivityPackage.ACTION__OUTER_EXIT, newOuterExit, newOuterExit));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID)
        {
            case ActivityPackage.ACTION__OUTER_ENTRY:
                return basicSetOuterEntry(null, msgs);
            case ActivityPackage.ACTION__ENTRY:
                return basicSetEntry(null, msgs);
            case ActivityPackage.ACTION__EXIT:
                return basicSetExit(null, msgs);
            case ActivityPackage.ACTION__OUTER_EXIT:
                return basicSetOuterExit(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case ActivityPackage.ACTION__OUTER_ENTRY:
                return getOuterEntry();
            case ActivityPackage.ACTION__ENTRY:
                return getEntry();
            case ActivityPackage.ACTION__EXIT:
                return getExit();
            case ActivityPackage.ACTION__OUTER_EXIT:
                return getOuterExit();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case ActivityPackage.ACTION__OUTER_ENTRY:
                setOuterEntry((TracePoint)newValue);
                return;
            case ActivityPackage.ACTION__ENTRY:
                setEntry((TracePoint)newValue);
                return;
            case ActivityPackage.ACTION__EXIT:
                setExit((TracePoint)newValue);
                return;
            case ActivityPackage.ACTION__OUTER_EXIT:
                setOuterExit((TracePoint)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.ACTION__OUTER_ENTRY:
                setOuterEntry((TracePoint)null);
                return;
            case ActivityPackage.ACTION__ENTRY:
                setEntry((TracePoint)null);
                return;
            case ActivityPackage.ACTION__EXIT:
                setExit((TracePoint)null);
                return;
            case ActivityPackage.ACTION__OUTER_EXIT:
                setOuterExit((TracePoint)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case ActivityPackage.ACTION__OUTER_ENTRY:
                return outerEntry != null;
            case ActivityPackage.ACTION__ENTRY:
                return entry != null;
            case ActivityPackage.ACTION__EXIT:
                return exit != null;
            case ActivityPackage.ACTION__OUTER_EXIT:
                return outerExit != null;
        }
        return super.eIsSet(featureID);
    }

} //ActionImpl
