/**
 */
package activity;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EventAction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activity.ActivityPackage#getEventAction()
 * @model abstract="true"
 * @generated
 */
public interface EventAction extends ResourceAction {
} // EventAction
