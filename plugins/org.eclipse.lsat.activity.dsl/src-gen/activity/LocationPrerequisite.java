/**
 */
package activity;

import machine.HasResourcePeripheral;
import machine.IResource;
import machine.Peripheral;
import machine.SymbolicPosition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Location Prerequisite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.LocationPrerequisite#getResource <em>Resource</em>}</li>
 *   <li>{@link activity.LocationPrerequisite#getPeripheral <em>Peripheral</em>}</li>
 *   <li>{@link activity.LocationPrerequisite#getPosition <em>Position</em>}</li>
 *   <li>{@link activity.LocationPrerequisite#getActivity <em>Activity</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getLocationPrerequisite()
 * @model
 * @generated
 */
public interface LocationPrerequisite extends HasResourcePeripheral {
	/**
     * Returns the value of the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resource</em>' reference.
     * @see #setResource(IResource)
     * @see activity.ActivityPackage#getLocationPrerequisite_Resource()
     * @model required="true"
     * @generated
     */
	IResource getResource();

	/**
     * Sets the value of the '{@link activity.LocationPrerequisite#getResource <em>Resource</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource</em>' reference.
     * @see #getResource()
     * @generated
     */
	void setResource(IResource value);

	/**
     * Returns the value of the '<em><b>Peripheral</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Peripheral</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Peripheral</em>' reference.
     * @see #setPeripheral(Peripheral)
     * @see activity.ActivityPackage#getLocationPrerequisite_Peripheral()
     * @model required="true"
     * @generated
     */
	Peripheral getPeripheral();

	/**
     * Sets the value of the '{@link activity.LocationPrerequisite#getPeripheral <em>Peripheral</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Peripheral</em>' reference.
     * @see #getPeripheral()
     * @generated
     */
	void setPeripheral(Peripheral value);

	/**
     * Returns the value of the '<em><b>Position</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Position</em>' reference.
     * @see #setPosition(SymbolicPosition)
     * @see activity.ActivityPackage#getLocationPrerequisite_Position()
     * @model required="true"
     * @generated
     */
	SymbolicPosition getPosition();

	/**
     * Sets the value of the '{@link activity.LocationPrerequisite#getPosition <em>Position</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Position</em>' reference.
     * @see #getPosition()
     * @generated
     */
	void setPosition(SymbolicPosition value);

	/**
     * Returns the value of the '<em><b>Activity</b></em>' container reference.
     * It is bidirectional and its opposite is '{@link activity.Activity#getPrerequisites <em>Prerequisites</em>}'.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Activity</em>' container reference.
     * @see #setActivity(Activity)
     * @see activity.ActivityPackage#getLocationPrerequisite_Activity()
     * @see activity.Activity#getPrerequisites
     * @model opposite="prerequisites" required="true" transient="false"
     * @generated
     */
	Activity getActivity();

	/**
     * Sets the value of the '{@link activity.LocationPrerequisite#getActivity <em>Activity</em>}' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Activity</em>' container reference.
     * @see #getActivity()
     * @generated
     */
	void setActivity(Activity value);

} // LocationPrerequisite
