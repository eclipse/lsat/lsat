/**
 */
package activity;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Require EventAction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see activity.ActivityPackage#getRequireEvent()
 * @model
 * @generated
 */
public interface RequireEvent extends EventAction {

} // RequireEvent
