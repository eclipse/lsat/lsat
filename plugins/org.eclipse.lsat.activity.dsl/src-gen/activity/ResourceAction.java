/**
 */
package activity;

import machine.IResource;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.ResourceAction#getResource <em>Resource</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getResourceAction()
 * @model abstract="true"
 * @generated
 */
public interface ResourceAction extends Action
{
    /**
     * Returns the value of the '<em><b>Resource</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Resource</em>' reference.
     * @see #setResource(IResource)
     * @see activity.ActivityPackage#getResourceAction_Resource()
     * @model required="true"
     * @generated
     */
    IResource getResource();

    /**
     * Sets the value of the '{@link activity.ResourceAction#getResource <em>Resource</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource</em>' reference.
     * @see #getResource()
     * @generated
     */
    void setResource(IResource value);

} // ResourceAction
