/**
 */
package activity;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trace Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.TracePoint#getValue <em>Value</em>}</li>
 *   <li>{@link activity.TracePoint#isRegex <em>Regex</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getTracePoint()
 * @model
 * @generated
 */
public interface TracePoint extends EObject {
	/**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(String)
     * @see activity.ActivityPackage#getTracePoint_Value()
     * @model required="true"
     * @generated
     */
	String getValue();

	/**
     * Sets the value of the '{@link activity.TracePoint#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
	void setValue(String value);

	/**
     * Returns the value of the '<em><b>Regex</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Regex</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Regex</em>' attribute.
     * @see #setRegex(boolean)
     * @see activity.ActivityPackage#getTracePoint_Regex()
     * @model required="true"
     * @generated
     */
	boolean isRegex();

	/**
     * Sets the value of the '{@link activity.TracePoint#isRegex <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Regex</em>' attribute.
     * @see #isRegex()
     * @generated
     */
	void setRegex(boolean value);

} // TracePoint
