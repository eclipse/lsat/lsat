/**
 */
package activity;

import machine.ImportContainer;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.ActivitySet#getActivities <em>Activities</em>}</li>
 *   <li>{@link activity.ActivitySet#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getActivitySet()
 * @model
 * @generated
 */
public interface ActivitySet extends ImportContainer {
	/**
     * Returns the value of the '<em><b>Activities</b></em>' containment reference list.
     * The list contents are of type {@link activity.Activity}.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Activities</em>' containment reference list.
     * @see activity.ActivityPackage#getActivitySet_Activities()
     * @model containment="true" keys="name"
     * @generated
     */
	EList<Activity> getActivities();

    /**
     * Returns the value of the '<em><b>Events</b></em>' containment reference list.
     * The list contents are of type {@link activity.Event}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Events</em>' containment reference list.
     * @see activity.ActivityPackage#getActivitySet_Events()
     * @model containment="true" keys="name"
     * @generated
     */
    EList<Event> getEvents();

} // ActivitySet
