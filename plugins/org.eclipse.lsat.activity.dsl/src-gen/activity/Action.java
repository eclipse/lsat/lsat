/**
 */
package activity;

import org.eclipse.lsat.common.graph.directed.editable.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.Action#getOuterEntry <em>Outer Entry</em>}</li>
 *   <li>{@link activity.Action#getEntry <em>Entry</em>}</li>
 *   <li>{@link activity.Action#getExit <em>Exit</em>}</li>
 *   <li>{@link activity.Action#getOuterExit <em>Outer Exit</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends Node {
	/**
     * Returns the value of the '<em><b>Outer Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outer Entry</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Outer Entry</em>' containment reference.
     * @see #setOuterEntry(TracePoint)
     * @see activity.ActivityPackage#getAction_OuterEntry()
     * @model containment="true"
     * @generated
     */
	TracePoint getOuterEntry();

	/**
     * Sets the value of the '{@link activity.Action#getOuterEntry <em>Outer Entry</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Outer Entry</em>' containment reference.
     * @see #getOuterEntry()
     * @generated
     */
	void setOuterEntry(TracePoint value);

	/**
     * Returns the value of the '<em><b>Entry</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Entry</em>' containment reference.
     * @see #setEntry(TracePoint)
     * @see activity.ActivityPackage#getAction_Entry()
     * @model containment="true"
     * @generated
     */
	TracePoint getEntry();

	/**
     * Sets the value of the '{@link activity.Action#getEntry <em>Entry</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Entry</em>' containment reference.
     * @see #getEntry()
     * @generated
     */
	void setEntry(TracePoint value);

	/**
     * Returns the value of the '<em><b>Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Exit</em>' containment reference.
     * @see #setExit(TracePoint)
     * @see activity.ActivityPackage#getAction_Exit()
     * @model containment="true"
     * @generated
     */
	TracePoint getExit();

	/**
     * Sets the value of the '{@link activity.Action#getExit <em>Exit</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Exit</em>' containment reference.
     * @see #getExit()
     * @generated
     */
	void setExit(TracePoint value);

	/**
     * Returns the value of the '<em><b>Outer Exit</b></em>' containment reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outer Exit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Outer Exit</em>' containment reference.
     * @see #setOuterExit(TracePoint)
     * @see activity.ActivityPackage#getAction_OuterExit()
     * @model containment="true"
     * @generated
     */
	TracePoint getOuterExit();

	/**
     * Sets the value of the '{@link activity.Action#getOuterExit <em>Outer Exit</em>}' containment reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Outer Exit</em>' containment reference.
     * @see #getOuterExit()
     * @generated
     */
	void setOuterExit(TracePoint value);

} // Action
