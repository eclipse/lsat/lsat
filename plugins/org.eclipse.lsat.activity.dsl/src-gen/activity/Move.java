/**
 */
package activity;

import machine.Distance;
import machine.Profile;
import machine.SymbolicPosition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Move</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link activity.Move#getProfile <em>Profile</em>}</li>
 *   <li>{@link activity.Move#getSuccessorMove <em>Successor Move</em>}</li>
 *   <li>{@link activity.Move#getPredecessorMove <em>Predecessor Move</em>}</li>
 *   <li>{@link activity.Move#isStopAtTarget <em>Stop At Target</em>}</li>
 *   <li>{@link activity.Move#isPositionMove <em>Position Move</em>}</li>
 *   <li>{@link activity.Move#getTargetPosition <em>Target Position</em>}</li>
 *   <li>{@link activity.Move#getSourcePosition <em>Source Position</em>}</li>
 *   <li>{@link activity.Move#getDistance <em>Distance</em>}</li>
 *   <li>{@link activity.Move#isPassing <em>Passing</em>}</li>
 *   <li>{@link activity.Move#isContinuing <em>Continuing</em>}</li>
 * </ul>
 *
 * @see activity.ActivityPackage#getMove()
 * @model
 * @generated
 */
public interface Move extends PeripheralAction {
	/**
     * Returns the value of the '<em><b>Profile</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Profile</em>' reference.
     * @see #setProfile(Profile)
     * @see activity.ActivityPackage#getMove_Profile()
     * @model required="true"
     * @generated
     */
	Profile getProfile();

	/**
     * Sets the value of the '{@link activity.Move#getProfile <em>Profile</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Profile</em>' reference.
     * @see #getProfile()
     * @generated
     */
	void setProfile(Profile value);

	/**
     * Returns the value of the '<em><b>Successor Move</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Returns the consecutive move for this peripheral (if any). A consecutive move is a move that is immediately executed after this move (not delayed by any other action), this means that there's either a direct dependency from this move to the consecutive move, or there's a syncbar in between with 1 input dependency from this move only.<br>
     * <b>NOTE:</b> If the model would contain two or more consecutive moves (=invalid model), this method rturns <tt>null</tt>
     * 
     * <!-- end-model-doc -->
     * @return the value of the '<em>Successor Move</em>' reference.
     * @see activity.ActivityPackage#getMove_SuccessorMove()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	Move getSuccessorMove();

	/**
     * Returns the value of the '<em><b>Predecessor Move</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Returns the previous move for this peripheral (if any).
     * <!-- end-model-doc -->
     * @return the value of the '<em>Predecessor Move</em>' reference.
     * @see activity.ActivityPackage#getMove_PredecessorMove()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	Move getPredecessorMove();

	/**
     * Returns the value of the '<em><b>Stop At Target</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stop At Target</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Stop At Target</em>' attribute.
     * @see #setStopAtTarget(boolean)
     * @see activity.ActivityPackage#getMove_StopAtTarget()
     * @model required="true" transient="true" volatile="true"
     * @generated
     */
	boolean isStopAtTarget();

	/**
     * Sets the value of the '{@link activity.Move#isStopAtTarget <em>Stop At Target</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Stop At Target</em>' attribute.
     * @see #isStopAtTarget()
     * @generated
     */
	void setStopAtTarget(boolean value);

	/**
     * Returns the value of the '<em><b>Position Move</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Position Move</em>' attribute.
     * @see activity.ActivityPackage#getMove_PositionMove()
     * @model required="true" transient="true" changeable="false" volatile="true"
     * @generated
     */
	boolean isPositionMove();

	/**
     * Returns the value of the '<em><b>Target Position</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Target Position</em>' reference.
     * @see #setTargetPosition(SymbolicPosition)
     * @see activity.ActivityPackage#getMove_TargetPosition()
     * @model
     * @generated
     */
	SymbolicPosition getTargetPosition();

	/**
     * Sets the value of the '{@link activity.Move#getTargetPosition <em>Target Position</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Target Position</em>' reference.
     * @see #getTargetPosition()
     * @generated
     */
	void setTargetPosition(SymbolicPosition value);

	/**
     * Returns the value of the '<em><b>Source Position</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Source Position</em>' reference.
     * @see activity.ActivityPackage#getMove_SourcePosition()
     * @model transient="true" changeable="false" volatile="true" derived="true"
     * @generated
     */
	SymbolicPosition getSourcePosition();

	/**
     * Returns the value of the '<em><b>Distance</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Distance</em>' reference.
     * @see #setDistance(Distance)
     * @see activity.ActivityPackage#getMove_Distance()
     * @model
     * @generated
     */
	Distance getDistance();

	/**
     * Sets the value of the '{@link activity.Move#getDistance <em>Distance</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Distance</em>' reference.
     * @see #getDistance()
     * @generated
     */
	void setDistance(Distance value);

	/**
     * Returns the value of the '<em><b>Passing</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Passing</em>' attribute.
     * @see #setPassing(boolean)
     * @see activity.ActivityPackage#getMove_Passing()
     * @model required="true"
     * @generated
     */
	boolean isPassing();

	/**
     * Sets the value of the '{@link activity.Move#isPassing <em>Passing</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Passing</em>' attribute.
     * @see #isPassing()
     * @generated
     */
	void setPassing(boolean value);

	/**
     * Returns the value of the '<em><b>Continuing</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Continuing</em>' attribute.
     * @see #setContinuing(boolean)
     * @see activity.ActivityPackage#getMove_Continuing()
     * @model required="true"
     * @generated
     */
	boolean isContinuing();

	/**
     * Sets the value of the '{@link activity.Move#isContinuing <em>Continuing</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Continuing</em>' attribute.
     * @see #isContinuing()
     * @generated
     */
	void setContinuing(boolean value);

} // Move
