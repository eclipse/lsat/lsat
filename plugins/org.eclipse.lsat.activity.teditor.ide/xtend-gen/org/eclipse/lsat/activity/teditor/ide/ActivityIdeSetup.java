/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.activity.teditor.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.lsat.activity.teditor.ActivityRuntimeModule;
import org.eclipse.lsat.activity.teditor.ActivityStandaloneSetup;
import org.eclipse.lsat.activity.teditor.ide.ActivityIdeModule;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
@SuppressWarnings("all")
public class ActivityIdeSetup extends ActivityStandaloneSetup {
  @Override
  public Injector createInjector() {
    ActivityRuntimeModule _activityRuntimeModule = new ActivityRuntimeModule();
    ActivityIdeModule _activityIdeModule = new ActivityIdeModule();
    return Guice.createInjector(Modules2.mixin(_activityRuntimeModule, _activityIdeModule));
  }
}
