/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.teditor.scoping;

import machine.Peripheral;
import machine.PeripheralType;
import machine.SymbolicPosition;
import machine.impl.AxisPositionMapEntryImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider;

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it
 */
@SuppressWarnings("all")
public class MachineScopeProvider extends AbstractDeclarativeScopeProvider {
  public IScope scope_SetPoint(final PeripheralType peripheralType, final EReference ref) {
    return Scopes.scopeFor(peripheralType.getSetPoints());
  }
  
  public IScope scope_Profile(final Peripheral peripheral, final EReference ref) {
    return Scopes.scopeFor(peripheral.getProfiles());
  }
  
  public IScope scope_SymbolicPosition(final Peripheral peripheral, final EReference ref) {
    return Scopes.scopeFor(peripheral.getPositions());
  }
  
  public IScope scope_Axis(final Peripheral peripheral, final EReference ref) {
    PeripheralType _type = peripheral.getType();
    boolean _tripleEquals = (null == _type);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(peripheral.getType().getAxes());
  }
  
  public IScope scope_AxisPositionMapEntry_value(final AxisPositionMapEntryImpl entry, final EReference ref) {
    if ((((null == entry.getKey()) || (!(entry.eContainer() instanceof SymbolicPosition))) || (!(entry.eContainer().eContainer() instanceof Peripheral)))) {
      return IScope.NULLSCOPE;
    }
    EObject _eContainer = entry.eContainer().eContainer();
    final Peripheral peripheral = ((Peripheral) _eContainer);
    boolean _containsKey = peripheral.getAxisPositions().containsKey(entry.getKey());
    boolean _not = (!_containsKey);
    if (_not) {
      return IScope.NULLSCOPE;
    }
    return Scopes.scopeFor(peripheral.getAxisPositions().get(entry.getKey()));
  }
}
