/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.teditor.formatting;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.lsat.machine.teditor.services.MachineGrammarAccess;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;
import org.eclipse.xtext.util.Pair;
import org.eclipse.xtext.xbase.lib.Extension;

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
@SuppressWarnings("all")
public class MachineFormatter extends AbstractDeclarativeFormatter {
  @Inject
  @Extension
  private MachineGrammarAccess _machineGrammarAccess;
  
  @Override
  protected void configureFormatting(final FormattingConfig c) {
    c.setAutoLinewrap(120);
    List<Pair<Keyword, Keyword>> _findKeywordPairs = this._machineGrammarAccess.findKeywordPairs("{", "}");
    for (final Pair<Keyword, Keyword> pair : _findKeywordPairs) {
      {
        c.setIndentation(pair.getFirst(), pair.getSecond());
        c.setLinewrap(1).after(pair.getFirst());
        c.setLinewrap(1).before(pair.getSecond());
        c.setLinewrap(1).after(pair.getSecond());
      }
    }
    List<Pair<Keyword, Keyword>> _findKeywordPairs_1 = this._machineGrammarAccess.findKeywordPairs("(", ")");
    for (final Pair<Keyword, Keyword> pair_1 : _findKeywordPairs_1) {
      {
        c.setIndentation(pair_1.getFirst(), pair_1.getSecond());
        c.setNoSpace().after(pair_1.getFirst());
        c.setNoSpace().before(pair_1.getSecond());
      }
    }
    List<Pair<Keyword, Keyword>> _findKeywordPairs_2 = this._machineGrammarAccess.findKeywordPairs("[", "]");
    for (final Pair<Keyword, Keyword> pair_2 : _findKeywordPairs_2) {
      {
        c.setNoSpace().after(pair_2.getFirst());
        c.setNoSpace().before(pair_2.getSecond());
      }
    }
    List<Keyword> _findKeywords = this._machineGrammarAccess.findKeywords(".");
    for (final Keyword dot : _findKeywords) {
      {
        c.setNoLinewrap().around(dot);
        c.setNoSpace().around(dot);
      }
    }
    List<Keyword> _findKeywords_1 = this._machineGrammarAccess.findKeywords(",");
    for (final Keyword comma : _findKeywords_1) {
      {
        c.setNoLinewrap().before(comma);
        c.setNoSpace().before(comma);
      }
    }
    List<Keyword> _findKeywords_2 = this._machineGrammarAccess.findKeywords(":");
    for (final Keyword doublecolon : _findKeywords_2) {
      {
        c.setNoLinewrap().before(doublecolon);
        c.setNoSpace().before(doublecolon);
      }
    }
    List<Keyword> _findKeywords_3 = this._machineGrammarAccess.findKeywords("->");
    for (final Keyword arrow : _findKeywords_3) {
      {
        c.setNoLinewrap().before(arrow);
        c.setNoSpace().before(arrow);
      }
    }
    List<Keyword> _findKeywords_4 = this._machineGrammarAccess.findKeywords("@");
    for (final Keyword at : _findKeywords_4) {
      c.setNoSpace().after(at);
    }
    c.setLinewrap(1).after(this._machineGrammarAccess.getImportRule());
    c.setLinewrap(2, 2, 2).before(this._machineGrammarAccess.getMachineAccess().getPathAnnotationsKeyword_3_0());
    c.setLinewrap(2, 2, 2).before(this._machineGrammarAccess.getPeripheralTypeRule());
    c.setLinewrap(2, 2, 2).after(this._machineGrammarAccess.getPeripheralTypeRule());
    c.setLinewrap(2, 2, 2).before(this._machineGrammarAccess.getResourceRule());
    c.setLinewrap(2, 2, 2).after(this._machineGrammarAccess.getResourceRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getPathAnnotationRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getActionTypeRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getSetPointRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getAxisRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getPeripheralRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getAxisPositionsMapEntryRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getSymbolicPositionRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getPeripheralAccess().getPathsKeyword_3_1_8_0());
    c.setLinewrap(1).before(this._machineGrammarAccess.getPathRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getFullMeshPathAccess().getEndPointsAssignment_5());
    c.setLinewrap(1).before(this._machineGrammarAccess.getDistanceRule());
    c.setLinewrap(1).before(this._machineGrammarAccess.getPeripheralAccess().getDistancesKeyword_3_1_7_0());
    c.setLinewrap(0, 1, 2).before(this._machineGrammarAccess.getSL_COMMENTRule());
    c.setLinewrap(0, 1, 2).before(this._machineGrammarAccess.getML_COMMENTRule());
    c.setLinewrap(0, 1, 1).after(this._machineGrammarAccess.getML_COMMENTRule());
  }
}
