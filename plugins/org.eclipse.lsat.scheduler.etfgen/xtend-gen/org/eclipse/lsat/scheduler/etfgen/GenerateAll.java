/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.etfgen;

import activity.Activity;
import activity.TracePoint;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import dispatching.Attribute;
import dispatching.Dispatch;
import dispatching.HasUserAttributes;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import lsat_graph.ActionTask;
import lsat_graph.ClaimReleaseResource;
import lsat_graph.ClaimTask;
import lsat_graph.ClaimedByScheduledTask;
import lsat_graph.DispatchGraph;
import lsat_graph.DispatchGroupResource;
import lsat_graph.DispatchGroupTask;
import lsat_graph.EventAnnotation;
import lsat_graph.PeripheralActionTask;
import lsat_graph.PeripheralResource;
import lsat_graph.ReleaseTask;
import lsat_graph.StochasticAnnotation;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.resources.Resource;
import org.eclipse.lsat.common.scheduler.resources.ResourceContainer;
import org.eclipse.lsat.common.scheduler.schedule.DependencyBoundary;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;
import org.eclipse.lsat.scheduler.etfgen.GanttColor;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function0;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pair;

@SuppressWarnings("all")
public class GenerateAll {
  private static final String PASSING_MOVE_TIMING_GAP = "PassingMoveTimingGap";
  
  private final Schedule<Task> schedule;
  
  private final IContainer targetIContainer;
  
  private final String[] arguments;
  
  private boolean erroneousPassingMoveColorScheme;
  
  private boolean criticalPathColorScheme;
  
  private final boolean stochasticImpactColorScheme;
  
  /**
   * Constructor.
   * 
   * @param modelURI
   *            is the URI of the model.
   * @param targetFolder
   *            is the output folder
   * @param arguments
   *            are the other arguments
   * @throws IOException
   *             Thrown when the output cannot be saved.
   * @generated NOT
   */
  public GenerateAll(final Schedule<Task> schedule, final IContainer targetIContainer, final boolean criticalPathColorScheme, final boolean stochasticImpactColorScheme, final String... arguments) {
    this.schedule = schedule;
    this.targetIContainer = targetIContainer;
    this.arguments = arguments;
    final Function1<Aspect<ScheduledTask<Task>, ScheduledDependency>, Boolean> _function = (Aspect<ScheduledTask<Task>, ScheduledDependency> it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, GenerateAll.PASSING_MOVE_TIMING_GAP));
    };
    boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<Aspect<ScheduledTask<Task>, ScheduledDependency>>filter(schedule.getAspects(), _function));
    boolean _not = (!_isEmpty);
    this.erroneousPassingMoveColorScheme = _not;
    this.criticalPathColorScheme = ((!this.erroneousPassingMoveColorScheme) && criticalPathColorScheme);
    this.stochasticImpactColorScheme = ((!this.erroneousPassingMoveColorScheme) && stochasticImpactColorScheme);
  }
  
  /**
   * Launches the generation.
   * 
   * @param monitor
   *            This will be used to display progress information to the user.
   * @throws IOException
   *             Thrown when the output cannot be saved.
   * @generated NOT
   */
  public IFile generate(final IProgressMonitor monitor) throws CoreException, IOException {
    StringConcatenation _builder = new StringConcatenation();
    String _elvis = null;
    String _head = null;
    if (((Iterable<String>)Conversions.doWrapArray(this.arguments))!=null) {
      _head=IterableExtensions.<String>head(((Iterable<String>)Conversions.doWrapArray(this.arguments)));
    }
    if (_head != null) {
      _elvis = _head;
    } else {
      String _name = this.schedule.getName();
      _elvis = _name;
    }
    _builder.append(_elvis);
    _builder.append(".etf");
    final String etfFileString = _builder.toString();
    Path _path = new Path(etfFileString);
    final IFile etfFile = this.targetIContainer.getFile(_path);
    try (final ByteArrayInputStream contents = new Function0<ByteArrayInputStream>() {
      @Override
      public ByteArrayInputStream apply() {
        try {
          byte[] _bytes = GenerateAll.this.generateETF(GenerateAll.this.schedule).getBytes(etfFile.getCharset(true));
          return new ByteArrayInputStream(_bytes);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
    }.apply()) {
      boolean _exists = etfFile.exists();
      if (_exists) {
        etfFile.setContents(contents, true, true, monitor);
      } else {
        etfFile.create(contents, true, monitor);
      }
    }
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append(".");
    _builder_1.append(etfFileString);
    _builder_1.append(".view");
    Path _path_1 = new Path(_builder_1.toString());
    final IFile confFile = this.targetIContainer.getFile(_path_1);
    try (final ByteArrayInputStream contents = new Function0<ByteArrayInputStream>() {
      @Override
      public ByteArrayInputStream apply() {
        try {
          byte[] _bytes = GenerateAll.this.generateTraceViewConfiguration().getBytes(confFile.getCharset(true));
          return new ByteArrayInputStream(_bytes);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
    }.apply()) {
      boolean _exists = confFile.exists();
      boolean _not = (!_exists);
      if (_not) {
        confFile.create(contents, true, monitor);
      }
    }
    return etfFile;
  }
  
  public String generateETF(final Schedule<? extends Task> schedule) {
    final Function1<Sequence<? extends Task>, Boolean> _function = (Sequence<? extends Task> it) -> {
      boolean _isEvent = this.isEvent(it);
      return Boolean.valueOf((!_isEvent));
    };
    final List<? extends Sequence<? extends Task>> sequences = IterableExtensions.toList(IterableExtensions.filter(schedule.getSequences(), _function));
    final Function1<Sequence<? extends Task>, Boolean> _function_1 = (Sequence<? extends Task> it) -> {
      return Boolean.valueOf(this.isEvent(it));
    };
    final Iterable<? extends Sequence<? extends Task>> eventSequences = IterableExtensions.filter(schedule.getSequences(), _function_1);
    final Function1<Pair<Integer, Sequence<? extends Task>>, Sequence<? extends Task>> _function_2 = (Pair<Integer, Sequence<? extends Task>> it) -> {
      return it.getValue();
    };
    final Function1<Pair<Integer, Sequence<? extends Task>>, Integer> _function_3 = (Pair<Integer, Sequence<? extends Task>> it) -> {
      return it.getKey();
    };
    final Map<Sequence<? extends Task>, Integer> sequenceIds = IterableExtensions.<Pair<Integer, Sequence<? extends Task>>, Sequence<? extends Task>, Integer>toMap(IterableExtensions.<Sequence<? extends Task>>indexed(sequences), _function_2, _function_3);
    final Function1<Sequence<? extends Task>, List<ScheduledTask<?>>> _function_4 = (Sequence<? extends Task> it) -> {
      EList<? extends ScheduledTask<? extends Task>> _scheduledTasks = it.getScheduledTasks();
      return ((List<ScheduledTask<?>>) _scheduledTasks);
    };
    final Iterable<ScheduledTask<?>> tasks = IterableExtensions.flatMap(schedule.getSequences(), _function_4);
    final Function1<Pair<Integer, ScheduledTask<?>>, ScheduledTask<?>> _function_5 = (Pair<Integer, ScheduledTask<?>> it) -> {
      return it.getValue();
    };
    final Function1<Pair<Integer, ScheduledTask<?>>, Integer> _function_6 = (Pair<Integer, ScheduledTask<?>> it) -> {
      return it.getKey();
    };
    final Map<ScheduledTask<?>, Integer> taskIds = IterableExtensions.<Pair<Integer, ScheduledTask<?>>, ScheduledTask<?>, Integer>toMap(IterableExtensions.<ScheduledTask<?>>indexed(tasks), _function_5, _function_6);
    final Function1<ScheduledTask<?>, Boolean> _function_7 = (ScheduledTask<?> it) -> {
      return Boolean.valueOf(this.isEvent(it));
    };
    final Function1<ScheduledTask<?>, String> _function_8 = (ScheduledTask<?> it) -> {
      return this.getEventName(it);
    };
    final List<String> uniqueEvents = IterableExtensions.<String>toList(IterableExtensions.<String>sort(IterableExtensions.<String>toSet(IterableExtensions.<ScheduledTask<?>, String>map(IterableExtensions.<ScheduledTask<?>>filter(tasks, _function_7), _function_8))));
    final Function1<ScheduledTask<?>, List<Edge>> _function_9 = (ScheduledTask<?> it) -> {
      final Function1<Edge, Integer> _function_10 = (Edge it_1) -> {
        return taskIds.get(it_1.getTargetNode());
      };
      return IterableExtensions.<Edge, Integer>sortBy(it.getOutgoingEdges(), _function_10);
    };
    final Function1<ScheduledDependency, Boolean> _function_10 = (ScheduledDependency it) -> {
      Dependency _dependency = it.getDependency();
      return Boolean.valueOf((_dependency == null));
    };
    final Iterable<ScheduledDependency> dependencies = IterableExtensions.<ScheduledDependency>reject(Iterables.<ScheduledDependency>filter(IterableExtensions.<ScheduledTask<?>, Edge>flatMap(tasks, _function_9), ScheduledDependency.class), _function_10);
    final Function1<Pair<Integer, ScheduledDependency>, ScheduledDependency> _function_11 = (Pair<Integer, ScheduledDependency> it) -> {
      return it.getValue();
    };
    final Function1<Pair<Integer, ScheduledDependency>, Integer> _function_12 = (Pair<Integer, ScheduledDependency> it) -> {
      return it.getKey();
    };
    final Map<ScheduledDependency, Integer> dependencyIds = IterableExtensions.<Pair<Integer, ScheduledDependency>, ScheduledDependency, Integer>toMap(IterableExtensions.<ScheduledDependency>indexed(dependencies), _function_11, _function_12);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("TU SECONDS");
    _builder.newLine();
    _builder.append("O 0");
    _builder.newLine();
    _builder.append("T");
    _builder.newLine();
    {
      for(final Sequence<? extends Task> sequence : sequences) {
        _builder.append("R ");
        Integer _get = sequenceIds.get(sequence);
        _builder.append(_get);
        _builder.append(" 100.0 true;");
        String _attributes = this.getAttributes(sequence);
        _builder.append(_attributes);
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _isEmpty = IterableExtensions.isEmpty(eventSequences);
      boolean _not = (!_isEmpty);
      if (_not) {
        _builder.append("R ");
        int _size = sequences.size();
        _builder.append(_size);
        _builder.append(" 100.0 true;name=->Events,type=Event");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final ScheduledTask<?> task : tasks) {
        _builder.append("C ");
        Integer _get_1 = taskIds.get(task);
        _builder.append(_get_1);
        _builder.append(" ");
        String _plainString = task.getStartTime().toPlainString();
        _builder.append(_plainString);
        _builder.append(" ");
        String _plainString_1 = task.getEndTime().toPlainString();
        _builder.append(_plainString_1);
        _builder.append(" ");
        Integer _sequenceId = this.getSequenceId(task, sequenceIds);
        _builder.append(_sequenceId);
        _builder.append(" ");
        String _offsetAndAmount = this.getOffsetAndAmount(task, uniqueEvents);
        _builder.append(_offsetAndAmount);
        _builder.append(";");
        String _attributes_1 = this.getAttributes(task);
        _builder.append(_attributes_1);
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final ScheduledDependency dependency : dependencies) {
        _builder.append("D ");
        Integer _get_2 = dependencyIds.get(dependency);
        _builder.append(_get_2);
        _builder.append(" ");
        int _etfType = this.getEtfType(dependency);
        _builder.append(_etfType);
        _builder.append(" ");
        Integer _get_3 = taskIds.get(dependency.getSourceNode());
        _builder.append(_get_3);
        _builder.append(" ");
        Integer _get_4 = taskIds.get(dependency.getTargetNode());
        _builder.append(_get_4);
        _builder.append(";");
        String _attributes_2 = this.getAttributes(dependency);
        _builder.append(_attributes_2);
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder.toString();
  }
  
  public String generateTraceViewConfiguration() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("activityView : false");
    _builder.newLine();
    _builder.append("showClaimLabels : true");
    _builder.newLine();
    _builder.append("claimColoring : color");
    _builder.newLine();
    _builder.append("claimDescribing: name");
    _builder.newLine();
    _builder.append("claimGrouping : description");
    _builder.newLine();
    _builder.append("dependencyColoring : color");
    _builder.newLine();
    _builder.append("dependencyFiltering :  { boundary = Between peripherals within 1 resource , Between peripherals in different resources } ");
    _builder.newLine();
    _builder.append("resourceDescribing: name");
    _builder.newLine();
    _builder.append("resourceFiltering :  { type = Peripheral , Resource, Event }");
    _builder.newLine();
    return _builder.toString();
  }
  
  private String getAttributes(final Sequence<?> sequence) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("name=");
    String _etfName = this.getEtfName(sequence.getResource());
    _builder.append(_etfName);
    _builder.append(",type=");
    String _type = this.getType(sequence.getResource());
    _builder.append(_type);
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  private String _getEtfName(final Resource resource) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = resource.getName();
    _builder.append(_name);
    return _builder.toString();
  }
  
  private String _getEtfName(final PeripheralResource resource) {
    StringConcatenation _builder = new StringConcatenation();
    ResourceContainer _container = resource.getContainer();
    String _name = null;
    if (_container!=null) {
      _name=_container.getName();
    }
    _builder.append(_name);
    _builder.append(".");
    String _name_1 = resource.getName();
    _builder.append(_name_1);
    return _builder.toString();
  }
  
  private String getType(final Resource resource) {
    String _switchResult = null;
    boolean _matched = false;
    if (resource instanceof DispatchGroupResource) {
      _matched=true;
      _switchResult = "Dispatch offset";
    }
    if (!_matched) {
      if (resource instanceof PeripheralResource) {
        _matched=true;
        _switchResult = "Peripheral";
      }
    }
    if (!_matched) {
      if (resource instanceof ClaimReleaseResource) {
        _matched=true;
        _switchResult = "Resource";
      }
    }
    if (!_matched) {
      Class<? extends Resource> _class = resource.getClass();
      String _plus = ("Unsupported type: " + _class);
      throw new IllegalArgumentException(_plus);
    }
    return _switchResult;
  }
  
  private Integer getSequenceId(final ScheduledTask<?> task, final Map<Sequence<? extends Task>, Integer> sequenceIds) {
    boolean _isEvent = this.isEvent(task);
    if (_isEvent) {
      return Integer.valueOf(sequenceIds.size());
    }
    return sequenceIds.get(task.getSequence());
  }
  
  private String getAttributes(final ScheduledTask<?> task) {
    final LinkedHashMap<String, String> map = CollectionLiterals.<String, String>newLinkedHashMap();
    String _switchResult = null;
    final ScheduledTask<?> it = task;
    boolean _matched = false;
    if (it instanceof ClaimedByScheduledTask) {
      if (this.stochasticImpactColorScheme) {
        _matched=true;
        _switchResult = "NA";
      }
    }
    if (!_matched) {
      if (it instanceof ClaimedByScheduledTask) {
        if (((this.criticalPathColorScheme && this.<Task>isCritical(((ClaimedByScheduledTask)it).getClaims())) && this.<Task>isCritical(((ClaimedByScheduledTask)it).getReleases()))) {
          _matched=true;
          _switchResult = "claim and release";
        }
      }
    }
    if (!_matched) {
      if (it instanceof ClaimedByScheduledTask) {
        if ((this.criticalPathColorScheme && this.<Task>isCritical(((ClaimedByScheduledTask)it).getClaims()))) {
          _matched=true;
          _switchResult = "claim only";
        }
      }
    }
    if (!_matched) {
      if (it instanceof ClaimedByScheduledTask) {
        if ((this.criticalPathColorScheme && this.<Task>isCritical(((ClaimedByScheduledTask)it).getReleases()))) {
          _matched=true;
          _switchResult = "release only";
        }
      }
    }
    if (!_matched) {
      if (it instanceof ClaimedByScheduledTask) {
        if (this.criticalPathColorScheme) {
          _matched=true;
          _switchResult = "none";
        }
      }
    }
    if (!_matched) {
      if (this.stochasticImpactColorScheme) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        BigDecimal _stochasticWeight = this.getStochasticWeight(it);
        BigDecimal _multiply = _stochasticWeight.multiply(BigDecimal.valueOf(100L));
        _builder.append(_multiply);
        _builder.append("%");
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (this.criticalPathColorScheme) {
        _matched=true;
        _switchResult = String.valueOf(this.isCritical(it));
      }
    }
    final String critical = _switchResult;
    this.putConditional(map, "name", this.getDisplayName(task));
    map.put("description", this.getDescription(task));
    this.putConditional(map, "type", this.getType(task));
    this.putConditional(map, "color", this.getColor(task));
    this.putConditional(map, "critical", critical);
    this.putConditional(map, "activity", this.getActivityName(task));
    this.putConditional(map, "baseActivity", this.getBaseActivityName(task));
    final Task tasktask = task.getTask();
    if ((tasktask instanceof ActionTask)) {
      TracePoint _outerEntry = ((ActionTask)tasktask).getAction().getOuterEntry();
      String _value = null;
      if (_outerEntry!=null) {
        _value=_outerEntry.getValue();
      }
      this.putConditional(map, "outerEntry", _value);
      TracePoint _outerExit = ((ActionTask)tasktask).getAction().getOuterExit();
      String _value_1 = null;
      if (_outerExit!=null) {
        _value_1=_outerExit.getValue();
      }
      this.putConditional(map, "outerExit", _value_1);
      TracePoint _entry = ((ActionTask)tasktask).getAction().getEntry();
      String _value_2 = null;
      if (_entry!=null) {
        _value_2=_entry.getValue();
      }
      this.putConditional(map, "entry", _value_2);
      TracePoint _exit = ((ActionTask)tasktask).getAction().getExit();
      String _value_3 = null;
      if (_exit!=null) {
        _value_3=_exit.getValue();
      }
      this.putConditional(map, "exit", _value_3);
    }
    boolean _isErroneousPassingMove = this.isErroneousPassingMove(task);
    if (_isErroneousPassingMove) {
      map.put("error", "Concatenated move is interrupted");
    }
    this.addUserAttributes(task, map);
    final Function1<Map.Entry<String, String>, String> _function = (Map.Entry<String, String> it_1) -> {
      String _trim = it_1.getKey().trim();
      String _plus = (_trim + "=");
      String _escape = this.escape(it_1.getValue());
      return (_plus + _escape);
    };
    return IterableExtensions.join(IterableExtensions.<Map.Entry<String, String>, String>map(map.entrySet(), _function), ",");
  }
  
  private Map<String, String> putConditional(final Map<String, String> map, final String key, final String value) {
    if (((value != null) && (!value.isEmpty()))) {
      map.put(key, value);
    }
    return map;
  }
  
  private String escape(final String string) {
    String _replaceAll = null;
    if (string!=null) {
      _replaceAll=string.replaceAll("([=,])", "\\\\$1");
    }
    return _replaceAll.trim();
  }
  
  private String getDisplayName(final ScheduledTask<?> task) {
    return task.getName();
  }
  
  /**
   * Claim, Release and ClaimedBy tasks are visualized as 'low' bars and all other tasks as 'high' bars.
   */
  private String getOffsetAndAmount(final ScheduledTask<?> task, final List<String> eventNames) {
    boolean _isEvent = this.isEvent(task);
    if (_isEvent) {
      final int index = eventNames.indexOf(this.getEventName(task));
      int _size = eventNames.size();
      int _divide = (100 / _size);
      final int size = Math.min(30, _divide);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append((size * index));
      _builder.append(" ");
      _builder.append(size);
      return _builder.toString();
    }
    String _switchResult = null;
    Task _task = task.getTask();
    boolean _matched = false;
    if (_task instanceof ClaimTask) {
      _matched=true;
    }
    if (!_matched) {
      if (_task instanceof ReleaseTask) {
        _matched=true;
      }
    }
    if (_matched) {
      _switchResult = "0.0 20.0";
    }
    if (!_matched) {
      _switchResult = "20.0 80.0";
    }
    return _switchResult;
  }
  
  private Dispatch getDispatch(final ScheduledTask<?> task) {
    return this.getDispatch(task.getTask());
  }
  
  private Dispatch getDispatch(final Task task) {
    final DirectedGraph<?, ?> graph = task.getGraph();
    Dispatch _xifexpression = null;
    if ((graph instanceof DispatchGraph)) {
      _xifexpression = ((DispatchGraph)graph).getDispatch();
    }
    return _xifexpression;
  }
  
  private String _getActivityName(final ScheduledTask<?> task) {
    Dispatch _dispatch = this.getDispatch(task);
    Activity _activity = null;
    if (_dispatch!=null) {
      _activity=_dispatch.getActivity();
    }
    String _name = null;
    if (_activity!=null) {
      _name=_activity.getName();
    }
    return _name;
  }
  
  /**
   * return the claim activity name or if there are more then 1 peripheral action
   * the disjunct activities of these actions are returned
   */
  private String _getActivityName(final ClaimedByScheduledTask task) {
    final Function1<ScheduledTask<Task>, String> _function = (ScheduledTask<Task> it) -> {
      Dispatch _dispatch = this.getDispatch(it);
      Activity _activity = null;
      if (_dispatch!=null) {
        _activity=_dispatch.getActivity();
      }
      String _name = null;
      if (_activity!=null) {
        _name=_activity.getName();
      }
      return _name;
    };
    return IterableExtensions.join(ListExtensions.<ScheduledTask<Task>, String>map(task.getClaims(), _function), ", ");
  }
  
  private String getBaseActivityName(final ScheduledTask<?> task) {
    Dispatch _dispatch = this.getDispatch(task);
    Activity _activity = null;
    if (_dispatch!=null) {
      _activity=_dispatch.getActivity();
    }
    String _originalName = null;
    if (_activity!=null) {
      _originalName=_activity.getOriginalName();
    }
    return _originalName;
  }
  
  private String getDescription(final ScheduledTask<?> task) {
    String _xifexpression = null;
    Dispatch _dispatch = this.getDispatch(task);
    boolean _tripleNotEquals = (_dispatch != null);
    if (_tripleNotEquals) {
      String _elvis = null;
      String _elvis_1 = null;
      String _description = this.getDispatch(task).getDescription();
      if (_description != null) {
        _elvis_1 = _description;
      } else {
        String _descriptionFromUserAttributes = this.getDescriptionFromUserAttributes(this.getDispatch(task));
        _elvis_1 = _descriptionFromUserAttributes;
      }
      if (_elvis_1 != null) {
        _elvis = _elvis_1;
      } else {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Activity: ");
        String _activityName = this.getActivityName(task);
        _builder.append(_activityName);
        _elvis = _builder.toString();
      }
      _xifexpression = _elvis;
    } else {
      _xifexpression = "";
    }
    return _xifexpression;
  }
  
  private String getDescriptionFromUserAttributes(final EObject object) {
    if ((object instanceof HasUserAttributes)) {
      final Function1<Map.Entry<Attribute, String>, Boolean> _function = (Map.Entry<Attribute, String> it) -> {
        String _name = it.getKey().getName();
        return Boolean.valueOf(Objects.equal(_name, "description"));
      };
      final Map.Entry<Attribute, String> entry = IterableExtensions.<Map.Entry<Attribute, String>>findFirst(((HasUserAttributes)object).getUserAttributes(), _function);
      if ((entry != null)) {
        return entry.getValue();
      }
    }
    if ((object != null)) {
      return this.getDescriptionFromUserAttributes(object.eContainer());
    }
    return null;
  }
  
  private void addUserAttributes(final EObject object, final Map<String, String> map) {
    if ((object != null)) {
      this.addUserAttributes(object.eContainer(), map);
    }
    if ((object instanceof HasUserAttributes)) {
      final Function1<Map.Entry<Attribute, String>, Boolean> _function = (Map.Entry<Attribute, String> it) -> {
        String _name = it.getKey().getName();
        return Boolean.valueOf((!Objects.equal("description", _name)));
      };
      final Consumer<Map.Entry<Attribute, String>> _function_1 = (Map.Entry<Attribute, String> it) -> {
        this.putConditional(map, it.getKey().getName(), it.getValue());
      };
      IterableExtensions.<Map.Entry<Attribute, String>>filter(((HasUserAttributes)object).getUserAttributes(), _function).forEach(_function_1);
    }
  }
  
  private void addUserAttributes(final ScheduledTask<?> task, final Map<String, String> map) {
    Dispatch _dispatch = this.getDispatch(task);
    EMap<Attribute, String> _userAttributes = null;
    if (_dispatch!=null) {
      _userAttributes=_dispatch.getUserAttributes();
    }
    boolean _tripleNotEquals = (_userAttributes != null);
    if (_tripleNotEquals) {
      this.addUserAttributes(this.getDispatch(task), map);
    }
  }
  
  private String getType(final ScheduledTask<?> task) {
    String _xifexpression = null;
    boolean _isEvent = this.isEvent(task);
    if (_isEvent) {
      _xifexpression = "Event";
    } else {
      String _xifexpression_1 = null;
      boolean _isClaimOrRelease = this.isClaimOrRelease(task);
      if (_isClaimOrRelease) {
        _xifexpression_1 = "Claim or release";
      } else {
        String _xifexpression_2 = null;
        Task _task = task.getTask();
        if ((_task instanceof DispatchGroupTask)) {
          _xifexpression_2 = "Dispatch offset";
        } else {
          String _xifexpression_3 = null;
          Task _task_1 = task.getTask();
          if ((_task_1 instanceof PeripheralActionTask)) {
            _xifexpression_3 = "Peripheral task";
          } else {
            throw new IllegalArgumentException(("Unsupported task type: " + task));
          }
          _xifexpression_2 = _xifexpression_3;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  private String getColor(final ScheduledTask<?> task) {
    String _xifexpression = null;
    if (this.erroneousPassingMoveColorScheme) {
      String _xifexpression_1 = null;
      boolean _isErroneousPassingMove = this.isErroneousPassingMove(task);
      if (_isErroneousPassingMove) {
        _xifexpression_1 = "orange";
      } else {
        _xifexpression_1 = "light_gray";
      }
      _xifexpression = _xifexpression_1;
    } else {
      String _xifexpression_2 = null;
      if (this.stochasticImpactColorScheme) {
        String _switchResult = null;
        boolean _matched = false;
        if (task instanceof ClaimedByScheduledTask) {
          _matched=true;
          _switchResult = GanttColor.ClaimReleaseNotCritical.getColor();
        }
        if (!_matched) {
          _switchResult = GanttColor.getStochasticColor(this.getStochasticWeight(task)).getColor();
        }
        _xifexpression_2 = _switchResult;
      } else {
        String _xifexpression_3 = null;
        if (this.criticalPathColorScheme) {
          String _xifexpression_4 = null;
          if ((task instanceof ClaimedByScheduledTask)) {
            String _switchResult_1 = null;
            boolean _matched_1 = false;
            if ((this.<Task>isCritical(((ClaimedByScheduledTask)task).getClaims()) && this.<Task>isCritical(((ClaimedByScheduledTask)task).getReleases()))) {
              _matched_1=true;
              _switchResult_1 = GanttColor.ClaimAndReleaseCritical.getColor();
            }
            if (!_matched_1) {
              if ((this.<Task>isCritical(((ClaimedByScheduledTask)task).getClaims()) || this.<Task>isCritical(((ClaimedByScheduledTask)task).getReleases()))) {
                _matched_1=true;
                _switchResult_1 = GanttColor.ClaimOrReleaseCritical.getColor();
              }
            }
            if (!_matched_1) {
              _switchResult_1 = GanttColor.ClaimReleaseNotCritical.getColor();
            }
            _xifexpression_4 = _switchResult_1;
          } else {
            String _xifexpression_5 = null;
            boolean _isCritical = this.isCritical(task);
            if (_isCritical) {
              _xifexpression_5 = GanttColor.TaskCritical.getColor();
            } else {
              _xifexpression_5 = GanttColor.TaskNotCritical.getColor();
            }
            _xifexpression_4 = _xifexpression_5;
          }
          _xifexpression_3 = _xifexpression_4;
        } else {
          _xifexpression_3 = this.getColorKey(task);
        }
        _xifexpression_2 = _xifexpression_3;
      }
      _xifexpression = _xifexpression_2;
    }
    return _xifexpression;
  }
  
  private String getColorKey(final ScheduledTask<?> task) {
    String _xifexpression = null;
    boolean _isEvent = this.isEvent(task);
    if (_isEvent) {
      _xifexpression = this.getEventName(task);
    } else {
      String _xifexpression_1 = null;
      boolean _isClaimOrRelease = this.isClaimOrRelease(task);
      if (_isClaimOrRelease) {
        _xifexpression_1 = this.getDescription(task);
      } else {
        String _xifexpression_2 = null;
        Task _task = task.getTask();
        if ((_task instanceof DispatchGroupTask)) {
          _xifexpression_2 = "Dispatch offset";
        } else {
          String _xifexpression_3 = null;
          Task _task_1 = task.getTask();
          if ((_task_1 instanceof PeripheralActionTask)) {
            Task _task_2 = task.getTask();
            _xifexpression_3 = ((PeripheralActionTask) _task_2).getAction().getResource().fqn();
          } else {
            throw new IllegalArgumentException(("Unsupported task type: " + task));
          }
          _xifexpression_2 = _xifexpression_3;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  private String getAttributes(final ScheduledDependency dependency) {
    LinkedHashMap<String, String> attrs = CollectionLiterals.<String, String>newLinkedHashMap();
    attrs.put("boundary", this.getEtfBoundary(dependency));
    attrs.put("color", this.getColor(dependency));
    if (this.criticalPathColorScheme) {
      attrs.put("critical", Boolean.valueOf(this.isCritical(dependency)).toString());
    }
    boolean _isEvent = this.isEvent(dependency);
    if (_isEvent) {
      attrs.put("name", this.getEventName(dependency));
    }
    final Function1<Map.Entry<String, String>, String> _function = (Map.Entry<String, String> it) -> {
      String _trim = it.getKey().trim();
      String _plus = (_trim + "=");
      String _escape = this.escape(it.getValue());
      return (_plus + _escape);
    };
    return IterableExtensions.join(IterableExtensions.<Map.Entry<String, String>, String>map(attrs.entrySet(), _function), ",");
  }
  
  private int getEtfType(final ScheduledDependency dependency) {
    int _switchResult = (int) 0;
    ScheduledDependencyType _type = dependency.getType();
    if (_type != null) {
      switch (_type) {
        case SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS:
          _switchResult = 0;
          break;
        case SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS:
          _switchResult = 1;
          break;
        case SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS:
          _switchResult = 2;
          break;
        case SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS:
          _switchResult = 3;
          break;
        default:
          break;
      }
    }
    return _switchResult;
  }
  
  private String getEtfBoundary(final ScheduledDependency dependency) {
    boolean _isEvent = this.isEvent(dependency);
    if (_isEvent) {
      return "Between events";
    }
    String _switchResult = null;
    DependencyBoundary _boundary = dependency.getBoundary();
    if (_boundary != null) {
      switch (_boundary) {
        case CROSS_RESOURCE:
          _switchResult = "Between peripherals within 1 resource";
          break;
        case CROSS_RESOURCE_CONTAINER:
          _switchResult = "Between peripherals in different resources";
          break;
        case IN_RESOURCE:
          _switchResult = "Within 1 peripheral";
          break;
        default:
          break;
      }
    }
    return _switchResult;
  }
  
  private String getColor(final ScheduledDependency dependency) {
    String _xifexpression = null;
    if (this.erroneousPassingMoveColorScheme) {
      String _xifexpression_1 = null;
      boolean _isEvent = this.isEvent(dependency);
      if (_isEvent) {
        _xifexpression_1 = GanttColor.DependencyEvent.getColor();
      } else {
        _xifexpression_1 = GanttColor.DependencyNormal.getColor();
      }
      _xifexpression = _xifexpression_1;
    } else {
      String _xifexpression_2 = null;
      if (this.criticalPathColorScheme) {
        String _xifexpression_3 = null;
        boolean _isCritical = this.isCritical(dependency);
        if (_isCritical) {
          _xifexpression_3 = GanttColor.DependencyCritical.getColor();
        } else {
          String _xifexpression_4 = null;
          boolean _isEvent_1 = this.isEvent(dependency);
          if (_isEvent_1) {
            _xifexpression_4 = GanttColor.DependencyEvent.getColor();
          } else {
            _xifexpression_4 = GanttColor.DependencyNotCritical.getColor();
          }
          _xifexpression_3 = _xifexpression_4;
        }
        _xifexpression_2 = _xifexpression_3;
      } else {
        String _xifexpression_5 = null;
        boolean _isEvent_2 = this.isEvent(dependency);
        if (_isEvent_2) {
          _xifexpression_5 = GanttColor.DependencyEvent.getColor();
        } else {
          String _xifexpression_6 = null;
          if ((this.isClaimOrRelease(((ScheduledTask<Task>) dependency.getSourceNode())) || this.isClaimOrRelease(((ScheduledTask<Task>) dependency.getTargetNode())))) {
            _xifexpression_6 = GanttColor.DependencyClaimRelease.getColor();
          } else {
            _xifexpression_6 = GanttColor.DependencyNormal.getColor();
          }
          _xifexpression_5 = _xifexpression_6;
        }
        _xifexpression_2 = _xifexpression_5;
      }
      _xifexpression = _xifexpression_2;
    }
    return _xifexpression;
  }
  
  private BigDecimal getStochasticWeight(final ScheduledTask<?> task) {
    BigDecimal _elvis = null;
    StochasticAnnotation _last = IterableExtensions.<StochasticAnnotation>last(Iterables.<StochasticAnnotation>filter(task.getAspects(), StochasticAnnotation.class));
    BigDecimal _weight = null;
    if (_last!=null) {
      _weight=_last.getWeight();
    }
    if (_weight != null) {
      _elvis = _weight;
    } else {
      _elvis = BigDecimal.ZERO;
    }
    return _elvis;
  }
  
  private <T extends Task> boolean isCritical(final List<ScheduledTask<T>> tasks) {
    final Function1<ScheduledTask<T>, Boolean> _function = (ScheduledTask<T> it) -> {
      return Boolean.valueOf(this.isCritical(it));
    };
    return IterableExtensions.<ScheduledTask<T>>exists(tasks, _function);
  }
  
  private boolean isCritical(final ScheduledTask<?> task) {
    return ((task != null) && (!IterableExtensions.isEmpty(IterableExtensions.<Aspect<?, ?>>filter(task.getAspects(), ((Function1<Aspect<?, ?>, Boolean>) (Aspect<?, ?> it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "Critical"));
    })))));
  }
  
  private boolean isErroneousPassingMove(final ScheduledTask<?> task) {
    return ((task != null) && (!IterableExtensions.isEmpty(IterableExtensions.<Aspect<?, ?>>filter(task.getAspects(), ((Function1<Aspect<?, ?>, Boolean>) (Aspect<?, ?> it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "PassingMoveTimingGap"));
    })))));
  }
  
  private boolean isCritical(final ScheduledDependency dependency) {
    final Function1<Aspect<?, ?>, Boolean> _function = (Aspect<?, ?> it) -> {
      String _name = it.getName();
      return Boolean.valueOf(Objects.equal(_name, "Critical"));
    };
    boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<Aspect<?, ?>>filter(dependency.getAspects(), _function));
    return (!_isEmpty);
  }
  
  private boolean isEvent(final ScheduledDependency dependency) {
    return (this.isEvent(((ScheduledTask<?>) dependency.getSourceNode())) || this.isEvent(((ScheduledTask<?>) dependency.getTargetNode())));
  }
  
  private boolean isEvent(final ScheduledTask<?> task) {
    boolean _isEmpty = IterableExtensions.isEmpty(Iterables.<EventAnnotation>filter(task.getTask().getAspects(), EventAnnotation.class));
    return (!_isEmpty);
  }
  
  private <T extends Task> boolean isEvent(final Sequence<T> sequence) {
    final Function1<ScheduledTask<T>, Boolean> _function = (ScheduledTask<T> it) -> {
      return Boolean.valueOf(this.isEvent(it));
    };
    return IterableExtensions.<ScheduledTask<T>>exists(sequence.getScheduledTasks(), _function);
  }
  
  private String getEventName(final ScheduledTask<?> task) {
    EventAnnotation _head = IterableExtensions.<EventAnnotation>head(Iterables.<EventAnnotation>filter(task.getTask().getAspects(), EventAnnotation.class));
    String _name = null;
    if (_head!=null) {
      _name=_head.getName();
    }
    return _name;
  }
  
  private String getEventName(final ScheduledDependency dependency) {
    String _xifexpression = null;
    Node _sourceNode = dependency.getSourceNode();
    boolean _isEvent = this.isEvent(((ScheduledTask<?>) _sourceNode));
    if (_isEvent) {
      Node _sourceNode_1 = dependency.getSourceNode();
      _xifexpression = this.getEventName(((ScheduledTask<?>) _sourceNode_1));
    } else {
      String _xifexpression_1 = null;
      Node _targetNode = dependency.getTargetNode();
      boolean _isEvent_1 = this.isEvent(((ScheduledTask<?>) _targetNode));
      if (_isEvent_1) {
        Node _targetNode_1 = dependency.getTargetNode();
        _xifexpression_1 = this.getEventName(((ScheduledTask<?>) _targetNode_1));
      } else {
        _xifexpression_1 = "";
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  private boolean isClaimOrRelease(final ScheduledTask<?> task) {
    return (((task instanceof ClaimedByScheduledTask) || (task.getTask() instanceof ClaimTask)) || (task.getTask() instanceof ReleaseTask));
  }
  
  private String getEtfName(final Resource resource) {
    if (resource instanceof PeripheralResource) {
      return _getEtfName((PeripheralResource)resource);
    } else if (resource != null) {
      return _getEtfName(resource);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(resource).toString());
    }
  }
  
  private String getActivityName(final ScheduledTask<?> task) {
    if (task instanceof ClaimedByScheduledTask) {
      return _getActivityName((ClaimedByScheduledTask)task);
    } else if (task != null) {
      return _getActivityName(task);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(task).toString());
    }
  }
}
