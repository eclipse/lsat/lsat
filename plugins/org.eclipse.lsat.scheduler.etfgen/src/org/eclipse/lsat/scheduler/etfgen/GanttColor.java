/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.etfgen;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;

public enum GanttColor {
    // TODO: Commented out preferred colors, as custom colors are not supported yet by TRACE
    DependencyNormal("black"), DependencyEvent("dark_green"), DependencyClaimRelease("gray"),
    DependencyNotCritical("gray"), DependencyCritical("very_dark_red"), // #660600
    TaskNotCritical("gray"), // #999999
    TaskCritical("dark_red"), // #990900
    ClaimReleaseNotCritical("light_gray"), // #BBBBBB
    ClaimOrReleaseCritical("very_light_red"), // #AA7A77
    ClaimAndReleaseCritical("red"), // #993933

    Stochastic00("red_shade_00"), Stochastic01("red_shade_01"), Stochastic02("red_shade_02"),
    Stochastic03("red_shade_03"), Stochastic04("red_shade_04"), Stochastic05("red_shade_05"),
    Stochastic06("red_shade_06"), Stochastic07("red_shade_07"), Stochastic08("red_shade_08"),
    Stochastic09("red_shade_09"), Stochastic10("red_shade_10");

    /** 57 special colors that are bright and easy to distinguish */
    public static final String[] COLORS = {"#FFAAAA", "#FF55F6", "#1B00FF", "#AAF2FF", "#55FF78", "#BBFF00", "#FFC5AA",
            "#FF55C0", "#6C00FF", "#AAD7FF", "#55FFAE", "#6CFF00", "#FFE0AA", "#FF558B", "#BB00FF", "#AABCFF",
            "#55FFE4", "#1BFF00", "#FFFBAA", "#FF5555", "#FF00F2", "#B3AAFF", "#55E4FF", "#00FF35", "#E8FFAA",
            "#FF8B55", "#FF00A1", "#CEAAFF", "#55AEFF", "#00FF86", "#CEFFAA", "#FFC055", "#FF0051", "#E8AAFF",
            "#5578FF", "#00FFD7", "#B3FFAA", "#FFF655", "#FF0000", "#FFAAFB", "#6755FF", "#00D7FF", "#AAFFBC",
            "#D2FF55", "#FF5100", "#FFAAE0", "#9D55FF", "#0086FF", "#AAFFD7", "#9DFF55", "#FFA100", "#FFAAC5",
            "#D255FF", "#0035FF", "#AAFFF2", "#67FF55", "#FFF200"};

    private final String color;

    private GanttColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    /**
     * Returns a shade of red of which its intensity depends on a {@code weight} in the range of (0.0 - 1.0).
     *
     * @param weight a value in the range of (0.0 - 1.0)
     * @return one of {@link GanttColor#Stochastic00} to {@link GanttColor#Stochastic10}
     */
    public static GanttColor getStochasticColor(BigDecimal weight) {
        int offset = Math.round(weight.max(ZERO).min(ONE).multiply(TEN).floatValue());
        return values()[Stochastic00.ordinal() + offset];
    }

    public static String getColor(int index) {
        return COLORS[Math.abs(index) % COLORS.length];
    }
}
