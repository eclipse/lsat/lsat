/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.etfgen;

import dispatching.HasUserAttributes
import java.io.ByteArrayInputStream
import java.io.IOException
import java.math.BigDecimal
import java.util.List
import java.util.Map
import lsat_graph.ActionTask
import lsat_graph.ClaimReleaseResource
import lsat_graph.ClaimTask
import lsat_graph.ClaimedByScheduledTask
import lsat_graph.DispatchGraph
import lsat_graph.DispatchGroupResource
import lsat_graph.DispatchGroupTask
import lsat_graph.EventAnnotation
import lsat_graph.PeripheralActionTask
import lsat_graph.PeripheralResource
import lsat_graph.ReleaseTask
import lsat_graph.StochasticAnnotation
import org.eclipse.core.resources.IContainer
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Path
import org.eclipse.emf.ecore.EObject
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.resources.Resource
import org.eclipse.lsat.common.scheduler.schedule.Schedule
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask
import org.eclipse.lsat.common.scheduler.schedule.Sequence

class GenerateAll {
    
    static val PASSING_MOVE_TIMING_GAP ='PassingMoveTimingGap'
    
    val Schedule<Task> schedule
    val IContainer targetIContainer
    val String[] arguments
    var boolean erroneousPassingMoveColorScheme
    var boolean criticalPathColorScheme
    val boolean stochasticImpactColorScheme
    // TODO: Commented out preferred colors, as custom colors are not supported yet by TRACE 
    // val Map<String, String> colorMap = newHashMap
    
    /**
     * Constructor.
     * 
     * @param modelURI
     *            is the URI of the model.
     * @param targetFolder
     *            is the output folder
     * @param arguments
     *            are the other arguments
     * @throws IOException
     *             Thrown when the output cannot be saved.
     * @generated NOT
     */
    new(
        Schedule<Task> schedule,
        IContainer targetIContainer,
        boolean criticalPathColorScheme,
        boolean stochasticImpactColorScheme,
        String... arguments
    ) {
        this.schedule = schedule
        this.targetIContainer = targetIContainer
        this.arguments = arguments
        this.erroneousPassingMoveColorScheme = !schedule.aspects.filter[name==PASSING_MOVE_TIMING_GAP].empty
        this.criticalPathColorScheme = !this.erroneousPassingMoveColorScheme && criticalPathColorScheme
        this.stochasticImpactColorScheme = !this.erroneousPassingMoveColorScheme && stochasticImpactColorScheme
    }

    /**
     * Launches the generation.
     * 
     * @param monitor
     *            This will be used to display progress information to the user.
     * @throws IOException
     *             Thrown when the output cannot be saved.
     * @generated NOT
     */
    def IFile generate(IProgressMonitor monitor) throws CoreException, IOException {
        val etfFileString = '''«arguments?.head ?: schedule.name».etf'''

        val etfFile = targetIContainer.getFile(new Path(etfFileString))
        try(val contents = new ByteArrayInputStream(schedule.generateETF.getBytes(etfFile.getCharset(true)))) {
            if (etfFile.exists) {
                etfFile.setContents(contents, true, true, monitor)
            } else {
                etfFile.create(contents, true, monitor)
            }
        }

        // Generate a default config, if it doesn't exist yet
        val confFile = targetIContainer.getFile(new Path('''.«etfFileString».view'''))
        try(val contents = new ByteArrayInputStream(generateTraceViewConfiguration.getBytes(confFile.getCharset(true)))) {
            if (!confFile.exists) {
                confFile.create(contents, true, monitor)
            }
        }

        return etfFile;
    }

    def String generateETF(Schedule<? extends Task> schedule) {
        // Getting the required iterables in a deterministic order and
        // build IDs based on their position in the list, they are used as references
        // NOTE: There seems to be some non-determinism in the model, hence we need some sorting
        val sequences = schedule.sequences.filter[!event].toList
        val eventSequences = schedule.sequences.filter[event] // add event sequences 
        val sequenceIds = sequences.indexed.toMap([value], [key])
        val tasks = schedule.sequences.flatMap[scheduledTasks as List<ScheduledTask<?>>]
        val taskIds = tasks.indexed.toMap([value], [key])
        val uniqueEvents = tasks.filter[event].map[eventName].toSet.sort.toList
        val dependencies = tasks.flatMap[outgoingEdges.sortBy[taskIds.get(targetNode)]].filter(ScheduledDependency).reject[dependency === null]
        val dependencyIds = dependencies.indexed.toMap([value], [key])

        return '''
            TU SECONDS
            O 0
            T
            «FOR sequence : sequences»
                R «sequenceIds.get(sequence)» 100.0 true;«sequence.attributes»
            «ENDFOR»
            «IF !eventSequences.empty»
                R «sequences.size» 100.0 true;name=->Events,type=Event
            «ENDIF»
            «FOR task : tasks»
                C «taskIds.get(task)» «task.startTime.toPlainString» «task.endTime.toPlainString» «task.getSequenceId(sequenceIds)» «task.getOffsetAndAmount(uniqueEvents)»;«task.attributes»
            «ENDFOR»
            «FOR dependency : dependencies»
                D «dependencyIds.get(dependency)» «dependency.etfType» «taskIds.get(dependency.sourceNode)» «taskIds.get(dependency.targetNode)»;«dependency.attributes»
            «ENDFOR»
        '''
    }
    
    def String generateTraceViewConfiguration() '''
        activityView : false
        showClaimLabels : true
        claimColoring : color
        claimDescribing: name
        claimGrouping : description
        dependencyColoring : color
        dependencyFiltering :  { boundary = Between peripherals within 1 resource , Between peripherals in different resources } 
        resourceDescribing: name
        resourceFiltering :  { type = Peripheral , Resource, Event }
    '''

    // Resource helpers
    private def String getAttributes(Sequence<?> sequence) '''
        name=«sequence.resource.etfName»,type=«sequence.resource.type»
    '''
    
    private dispatch def String getEtfName(Resource resource) '''«resource.name»'''

    private dispatch def String getEtfName(PeripheralResource resource) '''«resource.container?.name».«resource.name»'''
    
    private def String getType(Resource resource) {
        return switch (resource) {
        	DispatchGroupResource: 'Dispatch offset'
            PeripheralResource: 'Peripheral'
            ClaimReleaseResource: 'Resource'
        	default: throw new IllegalArgumentException('Unsupported type: ' + resource.class)
        }
    }

   // events are grouped to one lane and don't have a sequence id hence it will be the size of sequence
   private def getSequenceId(ScheduledTask<?> task, Map<Sequence<? extends Task>, Integer> sequenceIds) {
       if(task.event){
           return sequenceIds.size
       }
       return sequenceIds.get(task.sequence)
   }

    // Claim Helpers
    private def String getAttributes(ScheduledTask<?> task) {
        val map = newLinkedHashMap
        val critical = switch it: task {
            ClaimedByScheduledTask case stochasticImpactColorScheme: 'NA'
        	ClaimedByScheduledTask case criticalPathColorScheme && claims.critical && releases.critical: 'claim and release'
            ClaimedByScheduledTask case criticalPathColorScheme && claims.critical: 'claim only'
            ClaimedByScheduledTask case criticalPathColorScheme && releases.critical: 'release only'
            ClaimedByScheduledTask case criticalPathColorScheme: 'none'
            case stochasticImpactColorScheme: '''«stochasticWeight*100bd»%'''
            case criticalPathColorScheme: String::valueOf(critical)
        }
        map.putConditional("name", task.displayName)
        map.put("description", task.description) // add always
        map.putConditional("type", task.type)
        map.putConditional("color", task.color)
        map.putConditional("critical",critical)
        map.putConditional("activity", task.activityName)
        map.putConditional("baseActivity", task.baseActivityName)
        
        val tasktask = task.task
        if (tasktask instanceof ActionTask) {
            map.putConditional("outerEntry", tasktask.action.outerEntry?.value)
            map.putConditional("outerExit", tasktask.action.outerExit?.value)
            map.putConditional("entry", tasktask.action.entry?.value)
            map.putConditional("exit", tasktask.action.exit?.value)
        }
        if(task.isErroneousPassingMove){
            map.put("error","Concatenated move is interrupted")
        }
        task.addUserAttributes(map)
        return map.entrySet.map[key.trim+'='+value.escape].join(",")
    }
    
    private def putConditional(Map<String,String> map, String key, String value){
        if((value!==null && !value.empty)){
            map.put(key,value)
        }
        return map;
    }
    
    private def escape(String string){
        string?.replaceAll("([=,])","\\\\$1").trim
    }

  private def String getDisplayName(ScheduledTask<?> task) {
      return task.name
  }
 
    /** Claim, Release and ClaimedBy tasks are visualized as 'low' bars and all other tasks as 'high' bars. */
    private def String getOffsetAndAmount(ScheduledTask<?> task, List<String> eventNames) {
        if( task.event){
            val index = eventNames.indexOf(task.eventName)
            val size = Math.min(30,100/eventNames.size); 
            return '''«size*index» «size»'''
        }
        return switch (task.task) {
            ClaimTask,
            ReleaseTask: '0.0 20.0'
            default: '20.0 80.0'
        }
    }
    
    private def getDispatch(ScheduledTask<?> task){
       task.task.dispatch
    }

    private def getDispatch(Task task){
        val graph = task.graph;
        return if (graph instanceof DispatchGraph) {
            graph.dispatch
        }  
    }
    
    private def dispatch getActivityName(ScheduledTask<?> task) {
        task.dispatch?.activity?.name
    }

    /**
     * return the claim activity name or if there are more then 1 peripheral action 
     * the disjunct activities of these actions are returned
     */
    private def dispatch getActivityName(ClaimedByScheduledTask task) {
        task.claims.map[dispatch?.activity?.name].join(", ")
    }

    private def getBaseActivityName(ScheduledTask<?> task) {
        task.dispatch?.activity?.originalName
    }

    private def String getDescription(ScheduledTask<?> task) {
        return if(task.dispatch!==null) {
            task.dispatch.description?: task.dispatch.descriptionFromUserAttributes?:'''Activity: «task.activityName»'''
        } else ''
    }

    private def String getDescriptionFromUserAttributes(EObject object) {
        if(object instanceof HasUserAttributes) {
            val entry = object.userAttributes.findFirst[key.name=="description"]
            if(  entry !==null){
                return entry.value;
            }
        } 
        if(object !==null){
            return getDescriptionFromUserAttributes(object.eContainer)
        }
        return null;
    }
    
    private def void addUserAttributes(EObject object, Map<String, String> map) {
        if(object !== null){
            addUserAttributes(object.eContainer,map)
        }
        if(object instanceof HasUserAttributes){
            object.userAttributes.filter['description' != key.name].forEach[map.putConditional(key.name, value)]
        }
    }
    
    private def addUserAttributes(ScheduledTask<?> task, Map<String, String> map) {
        if (task.dispatch?.userAttributes !== null) {
            addUserAttributes(task.dispatch,map)
        }
    }
    
    private def String getType(ScheduledTask<?> task) {
        return if(task.isEvent) {
            'Event'
        } else if(task.isClaimOrRelease) {
            'Claim or release'
        } else if (task.task instanceof DispatchGroupTask) {
            'Dispatch offset'
        } else if (task.task instanceof PeripheralActionTask) {
        	'Peripheral task'
        } else {
            throw new IllegalArgumentException('Unsupported task type: ' + task)
        }
    }
    
    private def String getColor(ScheduledTask<?> task) {
        return 
        if (erroneousPassingMoveColorScheme) {
             task.erroneousPassingMove ? 'orange': 'light_gray'
        } else if (stochasticImpactColorScheme) {
            switch (task) {
                ClaimedByScheduledTask: GanttColor::ClaimReleaseNotCritical.getColor
                default: GanttColor::getStochasticColor(task.stochasticWeight).getColor
            }
        } else if (criticalPathColorScheme) {
            if (task instanceof ClaimedByScheduledTask) {
                switch (task) {
                    case (task.claims.critical && task.releases.critical):
                        GanttColor::ClaimAndReleaseCritical.getColor
                    case (task.claims.critical || task.releases.critical):
                        GanttColor::ClaimOrReleaseCritical.getColor
                    default:
                        GanttColor::ClaimReleaseNotCritical.getColor
                }
            } else if (task.critical) {
                GanttColor::TaskCritical.getColor
            } else {
                GanttColor::TaskNotCritical.getColor
            }
        } else {
            // TODO: Commented out preferred colors, as custom colors are not supported yet by TRACE 
            // colorMap.computeIfAbsent(task.colorKey)[GanttColor::getColor(colorMap.size)]
            task.colorKey
        }
    }
    
    private def String getColorKey(ScheduledTask<?> task) {
        return if (task.isEvent) {
            // Use a color per dispatch description or activity name
            task.eventName
         } else if (task.isClaimOrRelease) {
            // Use a color per dispatch description or activity name
            task.description
        } else if (task.task instanceof DispatchGroupTask) {
            // All dispatches get the same color
            'Dispatch offset'
        } else if (task.task instanceof PeripheralActionTask) {
            // Use a color per LSAT resource
            (task.task as PeripheralActionTask).action.resource.fqn
        } else {
            throw new IllegalArgumentException('Unsupported task type: ' + task)
        }
    }

    // Dependency helpers
    private def String getAttributes(ScheduledDependency dependency){
        var attrs = newLinkedHashMap
        attrs.put('boundary',dependency.etfBoundary)
        attrs.put('color',dependency.color)
        if (criticalPathColorScheme) attrs.put('critical',dependency.critical.toString)
        if (dependency.event) attrs.put('name',dependency.eventName)
        return attrs.entrySet.map[key.trim+'='+value.escape].join(",")
    }
    
    private def int getEtfType(ScheduledDependency dependency) {
        return switch (dependency.type) {
            case SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS: 0
            case SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS: 1
            case SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS: 2
            case SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS: 3
        }
    }
    
    private def String getEtfBoundary(ScheduledDependency dependency) {
        if(dependency.event){
            return 'Between events'
        }
        return switch (dependency.boundary) {
            case CROSS_RESOURCE: 'Between peripherals within 1 resource'
            case CROSS_RESOURCE_CONTAINER: 'Between peripherals in different resources'
            case IN_RESOURCE: 'Within 1 peripheral'
        }
    }

    private def String getColor(ScheduledDependency dependency) {
        return 
        if (erroneousPassingMoveColorScheme) {
            dependency.event ? GanttColor::DependencyEvent.color: GanttColor::DependencyNormal.color
        } else if (criticalPathColorScheme) {
            dependency.critical ? GanttColor::DependencyCritical.color : 
                dependency.event?  GanttColor::DependencyEvent.color : GanttColor::DependencyNotCritical.color
        } else if (dependency.event) { 
            GanttColor::DependencyEvent.color
        } else if (isClaimOrRelease(dependency.sourceNode as ScheduledTask<Task>) 
                || isClaimOrRelease(dependency.targetNode as ScheduledTask<Task>)) {
            GanttColor::DependencyClaimRelease.color
        } else {
            GanttColor::DependencyNormal.color
        }
    }

    // Model queries
    private def BigDecimal getStochasticWeight(ScheduledTask<?> task) {
        return task.aspects.filter(StochasticAnnotation).last?.weight ?: 0bd
    }

    private def <T extends Task> boolean isCritical(List<ScheduledTask<T>> tasks) {
        return tasks.exists[critical]
    }

    private def boolean isCritical(ScheduledTask<?> task) {
        return task!==null &&  !task.aspects.filter[name=='Critical'].empty
    }

    private def boolean isErroneousPassingMove(ScheduledTask<?> task) {
        return task!==null && !task.aspects.filter[name=='PassingMoveTimingGap'].empty
    }

    private def boolean isCritical(ScheduledDependency dependency) {
        return !dependency.aspects.filter[name=='Critical'].empty
    }
    
    private def boolean isEvent(ScheduledDependency dependency) {
        (dependency.sourceNode as ScheduledTask<?>).event || (dependency.targetNode as ScheduledTask<?>).event
    }
    
    private def boolean isEvent(ScheduledTask<?> task) {
        !task.task.aspects.filter(EventAnnotation).empty
    }

    private def <T extends Task> boolean isEvent(Sequence<T> sequence) {
        sequence.scheduledTasks.exists[event]
    }
    
    private def getEventName(ScheduledTask<?> task) {
        task.task.aspects.filter(EventAnnotation).head?.name
    }

    private def getEventName(ScheduledDependency dependency) {
        return
        if ((dependency.sourceNode as ScheduledTask<?>).event) {
            (dependency.sourceNode as ScheduledTask<?>).eventName
        }
        else if ((dependency.targetNode as ScheduledTask<?>).event){
          (dependency.targetNode as ScheduledTask<?>).eventName  
        } 
        else ''
    }

    private def isClaimOrRelease(ScheduledTask<?> task) {
        return task instanceof ClaimedByScheduledTask || task.task instanceof ClaimTask || task.task instanceof ReleaseTask
    }
}
