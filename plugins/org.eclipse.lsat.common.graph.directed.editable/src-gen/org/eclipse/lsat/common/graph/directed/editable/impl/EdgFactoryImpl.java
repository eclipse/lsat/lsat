/**
 */
package org.eclipse.lsat.common.graph.directed.editable.impl;

import org.eclipse.lsat.common.graph.directed.editable.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EdgFactoryImpl extends EFactoryImpl implements EdgFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EdgFactory init() {
		try {
			EdgFactory theEdgFactory = (EdgFactory)EPackage.Registry.INSTANCE.getEFactory(EdgPackage.eNS_URI);
			if (theEdgFactory != null) {
				return theEdgFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EdgFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EdgFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case EdgPackage.EDITABLE_DIRECTED_GRAPH: return createEditableDirectedGraph();
			case EdgPackage.NODE: return createNode();
			case EdgPackage.EDGE: return createEdge();
			case EdgPackage.SOURCE_REFERENCE: return createSourceReference();
			case EdgPackage.TARGET_REFERENCE: return createTargetReference();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EditableDirectedGraph createEditableDirectedGraph() {
		EditableDirectedGraphImpl editableDirectedGraph = new EditableDirectedGraphImpl();
		return editableDirectedGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node createNode() {
		NodeImpl node = new NodeImpl();
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Edge createEdge() {
		EdgeImpl edge = new EdgeImpl();
		return edge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SourceReference createSourceReference() {
		SourceReferenceImpl sourceReference = new SourceReferenceImpl();
		return sourceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TargetReference createTargetReference() {
		TargetReferenceImpl targetReference = new TargetReferenceImpl();
		return targetReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EdgPackage getEdgPackage() {
		return (EdgPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EdgPackage getPackage() {
		return EdgPackage.eINSTANCE;
	}

} //EdgFactoryImpl
