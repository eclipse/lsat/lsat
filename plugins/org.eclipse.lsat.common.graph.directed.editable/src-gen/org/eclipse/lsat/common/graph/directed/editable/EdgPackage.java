/**
 */
package org.eclipse.lsat.common.graph.directed.editable;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.graph.directed.editable.EdgFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface EdgPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "editable";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/lsat/edg";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "edg";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EdgPackage eINSTANCE = org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.EditableDirectedGraphImpl <em>Editable Directed Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EditableDirectedGraphImpl
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getEditableDirectedGraph()
	 * @generated
	 */
	int EDITABLE_DIRECTED_GRAPH = 0;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITABLE_DIRECTED_GRAPH__NODES = 0;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITABLE_DIRECTED_GRAPH__EDGES = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITABLE_DIRECTED_GRAPH__NAME = 2;

	/**
	 * The number of structural features of the '<em>Editable Directed Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITABLE_DIRECTED_GRAPH_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>All Nodes In Topological Order</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITABLE_DIRECTED_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER = 0;

	/**
	 * The number of operations of the '<em>Editable Directed Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITABLE_DIRECTED_GRAPH_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__GRAPH = 1;

	/**
	 * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__INCOMING_EDGES = 2;

	/**
	 * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__OUTGOING_EDGES = 3;

	/**
	 * The feature id for the '<em><b>Target References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__TARGET_REFERENCES = 4;

	/**
	 * The feature id for the '<em><b>Source References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__SOURCE_REFERENCES = 5;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget <em>Edge Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgeTarget
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getEdgeTarget()
	 * @generated
	 */
	int EDGE_TARGET = 3;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_TARGET__EDGE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_TARGET__NAME = 1;

	/**
	 * The number of structural features of the '<em>Edge Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_TARGET_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Edge Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_TARGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl <em>Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getEdge()
	 * @generated
	 */
	int EDGE = 2;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__EDGE = EDGE_TARGET__EDGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__NAME = EDGE_TARGET__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__SOURCE = EDGE_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__SOURCE_NODE = EDGE_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TARGET = EDGE_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__TARGET_NODE = EDGE_TARGET_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__GRAPH = EDGE_TARGET_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_FEATURE_COUNT = EDGE_TARGET_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_OPERATION_COUNT = EDGE_TARGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.SourceReferenceImpl <em>Source Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.SourceReferenceImpl
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getSourceReference()
	 * @generated
	 */
	int SOURCE_REFERENCE = 4;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE__NODE = 0;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE__EDGE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE__NAME = 2;

	/**
	 * The number of structural features of the '<em>Source Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Source Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.TargetReferenceImpl <em>Target Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.TargetReferenceImpl
	 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getTargetReference()
	 * @generated
	 */
	int TARGET_REFERENCE = 5;

	/**
	 * The feature id for the '<em><b>Edge</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_REFERENCE__EDGE = EDGE_TARGET__EDGE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_REFERENCE__NAME = EDGE_TARGET__NAME;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_REFERENCE__NODE = EDGE_TARGET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Target Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_REFERENCE_FEATURE_COUNT = EDGE_TARGET_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Target Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_REFERENCE_OPERATION_COUNT = EDGE_TARGET_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph <em>Editable Directed Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Editable Directed Graph</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph
	 * @generated
	 */
	EClass getEditableDirectedGraph();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getNodes()
	 * @see #getEditableDirectedGraph()
	 * @generated
	 */
	EReference getEditableDirectedGraph_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getEdges <em>Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Edges</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getEdges()
	 * @see #getEditableDirectedGraph()
	 * @generated
	 */
	EReference getEditableDirectedGraph_Edges();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getName()
	 * @see #getEditableDirectedGraph()
	 * @generated
	 */
	EAttribute getEditableDirectedGraph_Name();

	/**
	 * Returns the meta object for the '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#allNodesInTopologicalOrder() <em>All Nodes In Topological Order</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Nodes In Topological Order</em>' operation.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#allNodesInTopologicalOrder()
	 * @generated
	 */
	EOperation getEditableDirectedGraph__AllNodesInTopologicalOrder();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.graph.directed.editable.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getName()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Name();

	/**
	 * Returns the meta object for the container reference '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Graph</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getGraph()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_Graph();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getIncomingEdges <em>Incoming Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Edges</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getIncomingEdges()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_IncomingEdges();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getOutgoingEdges <em>Outgoing Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing Edges</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getOutgoingEdges()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_OutgoingEdges();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getTargetReferences <em>Target References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target References</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getTargetReferences()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_TargetReferences();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getSourceReferences <em>Source References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Source References</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Node#getSourceReferences()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_SourceReferences();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.graph.directed.editable.Edge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge
	 * @generated
	 */
	EClass getEdge();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getSource()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Source();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getSourceNode <em>Source Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Node</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getSourceNode()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_SourceNode();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getTarget()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Target();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getTargetNode <em>Target Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Node</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getTargetNode()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_TargetNode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Graph</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getGraph()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_Graph();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget <em>Edge Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge Target</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgeTarget
	 * @generated
	 */
	EClass getEdgeTarget();

	/**
	 * Returns the meta object for the container reference '{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Edge</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getEdge()
	 * @see #getEdgeTarget()
	 * @generated
	 */
	EReference getEdgeTarget_Edge();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getName()
	 * @see #getEdgeTarget()
	 * @generated
	 */
	EAttribute getEdgeTarget_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference <em>Source Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Reference</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.SourceReference
	 * @generated
	 */
	EClass getSourceReference();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Node</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.SourceReference#getNode()
	 * @see #getSourceReference()
	 * @generated
	 */
	EReference getSourceReference_Node();

	/**
	 * Returns the meta object for the container reference '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getEdge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Edge</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.SourceReference#getEdge()
	 * @see #getSourceReference()
	 * @generated
	 */
	EReference getSourceReference_Edge();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.SourceReference#getName()
	 * @see #getSourceReference()
	 * @generated
	 */
	EAttribute getSourceReference_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.common.graph.directed.editable.TargetReference <em>Target Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Reference</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.TargetReference
	 * @generated
	 */
	EClass getTargetReference();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.common.graph.directed.editable.TargetReference#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Node</em>'.
	 * @see org.eclipse.lsat.common.graph.directed.editable.TargetReference#getNode()
	 * @see #getTargetReference()
	 * @generated
	 */
	EReference getTargetReference_Node();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EdgFactory getEdgFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.EditableDirectedGraphImpl <em>Editable Directed Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EditableDirectedGraphImpl
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getEditableDirectedGraph()
		 * @generated
		 */
		EClass EDITABLE_DIRECTED_GRAPH = eINSTANCE.getEditableDirectedGraph();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDITABLE_DIRECTED_GRAPH__NODES = eINSTANCE.getEditableDirectedGraph_Nodes();

		/**
		 * The meta object literal for the '<em><b>Edges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDITABLE_DIRECTED_GRAPH__EDGES = eINSTANCE.getEditableDirectedGraph_Edges();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDITABLE_DIRECTED_GRAPH__NAME = eINSTANCE.getEditableDirectedGraph_Name();

		/**
		 * The meta object literal for the '<em><b>All Nodes In Topological Order</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EDITABLE_DIRECTED_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER = eINSTANCE.getEditableDirectedGraph__AllNodesInTopologicalOrder();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__NAME = eINSTANCE.getNode_Name();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__GRAPH = eINSTANCE.getNode_Graph();

		/**
		 * The meta object literal for the '<em><b>Incoming Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__INCOMING_EDGES = eINSTANCE.getNode_IncomingEdges();

		/**
		 * The meta object literal for the '<em><b>Outgoing Edges</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__OUTGOING_EDGES = eINSTANCE.getNode_OutgoingEdges();

		/**
		 * The meta object literal for the '<em><b>Target References</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__TARGET_REFERENCES = eINSTANCE.getNode_TargetReferences();

		/**
		 * The meta object literal for the '<em><b>Source References</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__SOURCE_REFERENCES = eINSTANCE.getNode_SourceReferences();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl <em>Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgeImpl
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getEdge()
		 * @generated
		 */
		EClass EDGE = eINSTANCE.getEdge();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__SOURCE = eINSTANCE.getEdge_Source();

		/**
		 * The meta object literal for the '<em><b>Source Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__SOURCE_NODE = eINSTANCE.getEdge_SourceNode();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__TARGET = eINSTANCE.getEdge_Target();

		/**
		 * The meta object literal for the '<em><b>Target Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__TARGET_NODE = eINSTANCE.getEdge_TargetNode();

		/**
		 * The meta object literal for the '<em><b>Graph</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__GRAPH = eINSTANCE.getEdge_Graph();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget <em>Edge Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.graph.directed.editable.EdgeTarget
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getEdgeTarget()
		 * @generated
		 */
		EClass EDGE_TARGET = eINSTANCE.getEdgeTarget();

		/**
		 * The meta object literal for the '<em><b>Edge</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE_TARGET__EDGE = eINSTANCE.getEdgeTarget_Edge();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE_TARGET__NAME = eINSTANCE.getEdgeTarget_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.SourceReferenceImpl <em>Source Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.SourceReferenceImpl
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getSourceReference()
		 * @generated
		 */
		EClass SOURCE_REFERENCE = eINSTANCE.getSourceReference();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOURCE_REFERENCE__NODE = eINSTANCE.getSourceReference_Node();

		/**
		 * The meta object literal for the '<em><b>Edge</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOURCE_REFERENCE__EDGE = eINSTANCE.getSourceReference_Edge();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOURCE_REFERENCE__NAME = eINSTANCE.getSourceReference_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.common.graph.directed.editable.impl.TargetReferenceImpl <em>Target Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.TargetReferenceImpl
		 * @see org.eclipse.lsat.common.graph.directed.editable.impl.EdgPackageImpl#getTargetReference()
		 * @generated
		 */
		EClass TARGET_REFERENCE = eINSTANCE.getTargetReference();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGET_REFERENCE__NODE = eINSTANCE.getTargetReference_Node();

	}

} //EdgPackage
