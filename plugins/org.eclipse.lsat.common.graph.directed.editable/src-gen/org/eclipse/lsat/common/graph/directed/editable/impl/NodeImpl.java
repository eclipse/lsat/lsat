/**
 */
package org.eclipse.lsat.common.graph.directed.editable.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;
import org.eclipse.lsat.common.graph.directed.editable.Edge;
import org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.graph.directed.editable.SourceReference;
import org.eclipse.lsat.common.graph.directed.editable.TargetReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl#getGraph <em>Graph</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl#getIncomingEdges <em>Incoming Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl#getOutgoingEdges <em>Outgoing Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl#getTargetReferences <em>Target References</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.impl.NodeImpl#getSourceReferences <em>Source References</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NodeImpl extends MinimalEObjectImpl.Container implements Node {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTargetReferences() <em>Target References</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<TargetReference> targetReferences;

	/**
	 * The cached value of the '{@link #getSourceReferences() <em>Source References</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<SourceReference> sourceReferences;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EdgPackage.Literals.NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired()) {
            eNotify(new ENotificationImpl(this, Notification.SET, EdgPackage.NODE__NAME, oldName, name));
        }
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EditableDirectedGraph getGraph() {
		if (eContainerFeatureID() != EdgPackage.NODE__GRAPH) {
            return null;
        }
		return (EditableDirectedGraph)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraph(EditableDirectedGraph newGraph, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newGraph, EdgPackage.NODE__GRAPH, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGraph(EditableDirectedGraph newGraph) {
		if (newGraph != eInternalContainer() || (eContainerFeatureID() != EdgPackage.NODE__GRAPH && newGraph != null)) {
			if (EcoreUtil.isAncestor(this, newGraph)) {
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            }
			NotificationChain msgs = null;
			if (eInternalContainer() != null) {
                msgs = eBasicRemoveFromContainer(msgs);
            }
			if (newGraph != null) {
                msgs = ((InternalEObject)newGraph).eInverseAdd(this, EdgPackage.EDITABLE_DIRECTED_GRAPH__NODES, EditableDirectedGraph.class, msgs);
            }
			msgs = basicSetGraph(newGraph, msgs);
			if (msgs != null) {
                msgs.dispatch();
            }
		}
		else if (eNotificationRequired()) {
            eNotify(new ENotificationImpl(this, Notification.SET, EdgPackage.NODE__GRAPH, newGraph, newGraph));
        }
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Edge> getIncomingEdges() {
		EList<Edge> result = new  org.eclipse.emf.common.util.BasicEList<Edge>(
				getSourceReferences().size() + getTargetReferences().size());
		org.eclipse.lsat.common.util.CollectionUtil.addAll(result, org.eclipse.lsat.common.graph.directed.editable.EdgQueries.getIncomingEdges(this));
		return new org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList<Edge>(
				this, EdgPackage.Literals.NODE__INCOMING_EDGES, result.size(), result.toArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Edge> getOutgoingEdges() {
		EList<Edge> result = new  org.eclipse.emf.common.util.BasicEList<Edge>(getSourceReferences().size());
		org.eclipse.lsat.common.util.CollectionUtil.addAll(result, org.eclipse.lsat.common.graph.directed.editable.EdgQueries.getOutgoingEdges(this));
		return new org.eclipse.emf.ecore.util.EcoreEList.UnmodifiableEList<Edge>(
				this, EdgPackage.Literals.NODE__OUTGOING_EDGES, result.size(), result.toArray());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TargetReference> getTargetReferences() {
		if (targetReferences == null) {
			targetReferences = new EObjectWithInverseEList<TargetReference>(TargetReference.class, this, EdgPackage.NODE__TARGET_REFERENCES, EdgPackage.TARGET_REFERENCE__NODE);
		}
		return targetReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SourceReference> getSourceReferences() {
		if (sourceReferences == null) {
			sourceReferences = new EObjectWithInverseEList<SourceReference>(SourceReference.class, this, EdgPackage.NODE__SOURCE_REFERENCES, EdgPackage.SOURCE_REFERENCE__NODE);
		}
		return sourceReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EdgPackage.NODE__GRAPH:
				if (eInternalContainer() != null) {
                    msgs = eBasicRemoveFromContainer(msgs);
                }
				return basicSetGraph((EditableDirectedGraph)otherEnd, msgs);
			case EdgPackage.NODE__TARGET_REFERENCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTargetReferences()).basicAdd(otherEnd, msgs);
			case EdgPackage.NODE__SOURCE_REFERENCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSourceReferences()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EdgPackage.NODE__GRAPH:
				return basicSetGraph(null, msgs);
			case EdgPackage.NODE__TARGET_REFERENCES:
				return ((InternalEList<?>)getTargetReferences()).basicRemove(otherEnd, msgs);
			case EdgPackage.NODE__SOURCE_REFERENCES:
				return ((InternalEList<?>)getSourceReferences()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case EdgPackage.NODE__GRAPH:
				return eInternalContainer().eInverseRemove(this, EdgPackage.EDITABLE_DIRECTED_GRAPH__NODES, EditableDirectedGraph.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EdgPackage.NODE__NAME:
				return getName();
			case EdgPackage.NODE__GRAPH:
				return getGraph();
			case EdgPackage.NODE__INCOMING_EDGES:
				return getIncomingEdges();
			case EdgPackage.NODE__OUTGOING_EDGES:
				return getOutgoingEdges();
			case EdgPackage.NODE__TARGET_REFERENCES:
				return getTargetReferences();
			case EdgPackage.NODE__SOURCE_REFERENCES:
				return getSourceReferences();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EdgPackage.NODE__NAME:
				setName((String)newValue);
				return;
			case EdgPackage.NODE__GRAPH:
				setGraph((EditableDirectedGraph)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EdgPackage.NODE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case EdgPackage.NODE__GRAPH:
				setGraph((EditableDirectedGraph)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EdgPackage.NODE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case EdgPackage.NODE__GRAPH:
				return getGraph() != null;
			case EdgPackage.NODE__INCOMING_EDGES:
				return !getIncomingEdges().isEmpty();
			case EdgPackage.NODE__OUTGOING_EDGES:
				return !getOutgoingEdges().isEmpty();
			case EdgPackage.NODE__TARGET_REFERENCES:
				return targetReferences != null && !targetReferences.isEmpty();
			case EdgPackage.NODE__SOURCE_REFERENCES:
				return sourceReferences != null && !sourceReferences.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
            return super.toString();
        }

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //NodeImpl
