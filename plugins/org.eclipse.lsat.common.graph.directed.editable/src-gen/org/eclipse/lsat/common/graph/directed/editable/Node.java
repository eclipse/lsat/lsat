/**
 */
package org.eclipse.lsat.common.graph.directed.editable;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Node#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Node#getGraph <em>Graph</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Node#getIncomingEdges <em>Incoming Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Node#getOutgoingEdges <em>Outgoing Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Node#getTargetReferences <em>Target References</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.Node#getSourceReferences <em>Source References</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getNode()
 * @model
 * @generated
 */
public interface Node extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getNode_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(EditableDirectedGraph)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getNode_Graph()
	 * @see org.eclipse.lsat.common.graph.directed.editable.EditableDirectedGraph#getNodes
	 * @model opposite="nodes" resolveProxies="false" required="true" transient="false"
	 * @generated
	 */
	EditableDirectedGraph getGraph();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.Node#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(EditableDirectedGraph value);

	/**
	 * Returns the value of the '<em><b>Incoming Edges</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.editable.Edge}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getTargetNode <em>Target Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Edges</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getNode_IncomingEdges()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getTargetNode
	 * @model opposite="targetNode" resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Edge> getIncomingEdges();

	/**
	 * Returns the value of the '<em><b>Outgoing Edges</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.editable.Edge}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getSourceNode <em>Source Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Edges</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getNode_OutgoingEdges()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getSourceNode
	 * @model opposite="sourceNode" resolveProxies="false" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Edge> getOutgoingEdges();

	/**
	 * Returns the value of the '<em><b>Target References</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.editable.TargetReference}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.TargetReference#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target References</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target References</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getNode_TargetReferences()
	 * @see org.eclipse.lsat.common.graph.directed.editable.TargetReference#getNode
	 * @model opposite="node" resolveProxies="false" transient="true" changeable="false"
	 * @generated
	 */
	EList<TargetReference> getTargetReferences();

	/**
	 * Returns the value of the '<em><b>Source References</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.editable.SourceReference}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.SourceReference#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source References</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source References</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getNode_SourceReferences()
	 * @see org.eclipse.lsat.common.graph.directed.editable.SourceReference#getNode
	 * @model opposite="node" resolveProxies="false" transient="true" changeable="false"
	 * @generated
	 */
	EList<SourceReference> getSourceReferences();

} // Node
