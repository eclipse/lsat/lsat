/**
 */
package org.eclipse.lsat.common.graph.directed.editable;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge Target</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getEdge <em>Edge</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdgeTarget()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface EdgeTarget extends EObject {
	/**
	 * Returns the value of the '<em><b>Edge</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.editable.Edge#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edge</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edge</em>' container reference.
	 * @see #setEdge(Edge)
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdgeTarget_Edge()
	 * @see org.eclipse.lsat.common.graph.directed.editable.Edge#getTarget
	 * @model opposite="target" resolveProxies="false" transient="false"
	 * @generated
	 */
	Edge getEdge();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.editable.EdgeTarget#getEdge <em>Edge</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edge</em>' container reference.
	 * @see #getEdge()
	 * @generated
	 */
	void setEdge(Edge value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see org.eclipse.lsat.common.graph.directed.editable.EdgPackage#getEdgeTarget_Name()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getName();

} // EdgeTarget
