/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.graph.directed.editable;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.function.Predicate;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.queries.IterableQueries;
import org.eclipse.lsat.common.queries.QueryableIterable;

public class EdgQueries {
    private EdgQueries() {
        // Empty
    }

    public static final QueryableIterable<Edge> getIncomingEdges(Node node) {
        // OK, first part is simple. The incoming edges are at least
        // the set of edges of which the TargetReference refers to
        // this Node
        return from(node.getTargetReferences()).xcollectOne(TargetReference::getEdge).union(
                // Now this part is a bit more tricky. This Node its incoming
                // edges are also the edges of which the target is the
                // edge of which its source node refers to this node.
                from(node.getSourceReferences()).xcollectOne(SourceReference::getEdge).xcollectOne(Edge::getEdge));
    }

    public static final QueryableIterable<Edge> getOutgoingEdges(Node node) {
        return from(node.getSourceReferences()).xcollectOne(SourceReference::getEdge);
    }

    public static final QueryableIterable<Node> directPredecessors(Node node) {
        return from(node).xcollect(EdgQueries::getIncomingEdges).xcollectOne(Edge::getSourceNode);
    }

    public static final <T extends Node> Iterable<T> nearestPredecessors(Node node, Class<T> type) {
        return IterableQueries.findNearest(
                IterableQueries.closure(Collections.singleton(node), EdgQueries::directPredecessors), type);
    }

    public static final Iterable<Node> nearestPredecessors(Node node, Predicate<? super Node> predicate) {
        return IterableQueries.findNearest(
                IterableQueries.closure(Collections.singleton(node), EdgQueries::directPredecessors), predicate);
    }

    public static final QueryableIterable<Node> allPredecessors(Node node) {
        return from(node).closure(EdgQueries::directPredecessors);
    }

    public static final QueryableIterable<Node> directSuccessors(Node node) {
        return from(node).xcollect(EdgQueries::getOutgoingEdges).xcollectOne(Edge::getTargetNode);
    }

    public static final <T extends Node> Iterable<T> nearestSuccessors(Node node, Class<T> type) {
        return IterableQueries.findNearest(
                IterableQueries.closure(Collections.singleton(node), EdgQueries::directSuccessors), type);
    }

    public static final Iterable<Node> nearestSuccessors(Node node, Predicate<? super Node> predicate) {
        return IterableQueries.findNearest(
                IterableQueries.closure(Collections.singleton(node), EdgQueries::directSuccessors), predicate);
    }

    public static final QueryableIterable<Node> allSuccessors(Node node) {
        return from(node).closure(EdgQueries::directSuccessors);
    }

    public static final <N extends Node> EList<N> topologicalOrdering(Iterable<N> nodes) {
        return topologicalOrdering(nodes, new LinkedList<N>());
    }

    public static final <N extends Node> EList<N> topologicalOrdering(Iterable<N> nodes,
            Comparator<? super N> preference)
    {
        return topologicalOrdering(nodes, new PriorityQueue<N>(11, preference));
    }

    @SuppressWarnings("unchecked")
    private static final <N extends Node> EList<N> topologicalOrdering(Iterable<N> nodes,
            Queue<N> nodesWithoutIncomingEdges)
    {
        assert nodesWithoutIncomingEdges.isEmpty() : "Expected an empty queue";
        final Map<N, Integer> notVisitedIncomingEdges = new HashMap<N, Integer>();
        for (N node: nodes) {
            int incomingEdgesSize = node.getIncomingEdges().size();
            notVisitedIncomingEdges.put(node, incomingEdgesSize);
            if (0 == incomingEdgesSize) {
                nodesWithoutIncomingEdges.add(node);
            }
        }

        final EList<N> topologicalOrder = new BasicEList<N>(notVisitedIncomingEdges.size());
        while (!nodesWithoutIncomingEdges.isEmpty()) {
            // Take a node from the queue
            N node = nodesWithoutIncomingEdges.remove();
            // Add to ordering
            topologicalOrder.add(node);

            // For all targets, remove this task from the incoming edges count
            for (Edge outgoingEdge: node.getOutgoingEdges()) {
                Node sucessor = outgoingEdge.getTargetNode();
                Integer notVisitedIncomingEdgesSize = notVisitedIncomingEdges.get(sucessor);
                if (null == notVisitedIncomingEdgesSize) {
                    // Strange, but might happen while typing in the XText editor, just skip
                    continue;
                }
                notVisitedIncomingEdgesSize -= 1;
                // As successor was found in the notVisitedIncomingEdgesSize
                // it should be of type N, so we can safely cast here
                notVisitedIncomingEdges.put((N)sucessor, notVisitedIncomingEdgesSize);
                if (0 == notVisitedIncomingEdgesSize) {
                    nodesWithoutIncomingEdges.add((N)sucessor);
                }
            }
        }

        if (topologicalOrder.size() != notVisitedIncomingEdges.size()) {
            // "Cycle exists in graph!!"
            return null;
        }

        return topologicalOrder;
    }

    /**
     * Find all paths between n1 and n2. If n1==n2 then all cyclic paths are returned
     *
     * @param <N>
     * @param n1
     * @param n2
     * @return
     */
    public static final <N extends Node> List<List<Node>> pathsBetweenNodes(Node n1, Node n2) {
        List<List<Node>> result = new ArrayList<>();
        addPaths(n1, n2, result);
        return result;
    }

    private static void addPaths(Node from, Node to, List<List<Node>> result) {
        List<Edge> visited = new ArrayList<>();
        List<Node> path = new ArrayList<>();

        // path starts with from
        path.add(from);

        // call recursively to find all paths
        findAllPaths(from, to, visited, path, result);
    }

    /**
     * Find all paths from <b>from</b> till <b>to</b>
     *
     * @param visited is used for tracking already visited edges
     * @param path contains the found path up to from
     * @param result is used to store found paths
     *
     */
    private static void findAllPaths(Node from, Node to, List<Edge> visited, List<Node> path, List<List<Node>> result) {
        // if start and 'from' and 'to' are equal then search for all paths the eventually lead from 'from' till 'to'
        boolean start = path.size() == 1;
        if (!start && from.equals(to)) {
            result.add(new ArrayList<>(path));
            // if match found then no need to traverse more till depth
            return;
        }

        // Recur for all locations succeeding to the current location
        for (Edge edge: from.getOutgoingEdges()) {
            if (!visited.contains(edge)) {
                visited.add(edge);
                Node successor = edge.getTargetNode();
                // store found location in path
                path.add(successor);
                // and recursively continue with the successor
                findAllPaths(successor, to, visited, path, result);
                // remove current (last) node and edge. using index! as path may contain node twice!
                path.remove(path.size() - 1);
                visited.remove(visited.size() - 1);
            }
        }
    }
}
