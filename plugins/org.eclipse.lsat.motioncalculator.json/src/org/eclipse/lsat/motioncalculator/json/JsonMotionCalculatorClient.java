/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.motioncalculator.MotionCalculator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileProvider;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.PositionInfo;

public class JsonMotionCalculatorClient implements MotionCalculator, MotionProfileProvider {
    private final JsonServer server;

    public JsonMotionCalculatorClient(JsonServer server) {
        this.server = server;
    }

    @Override
    public void validate(List<MotionSegment> segments) throws MotionValidationException {
        try {
            request(JsonRequestType.Validate, segments);
        } catch (MotionValidationException e) {
            throw e;
        } catch (MotionException e) {
            new MotionValidationException(e.getMessage(), e);
        }
    }

    @Override
    public List<Double> calculateTimes(List<MotionSegment> segments) throws MotionException {
        List<Double> times = request(JsonRequestType.CalculateTimes, segments).getTimes();
        if (times == null) {
            return Collections.emptyList();
        }
        return times;
    }

    @Override
    public Collection<PositionInfo> getPositionInfo(List<MotionSegment> segments) throws MotionException {
        Collection<PositionInfo> positions = request(JsonRequestType.PositionInfo, segments).getPositions();
        if (positions == null) {
            return Collections.emptySet();
        }
        return positions;
    }

    @Override
    public Set<MotionProfile> getSupportedProfiles() throws MotionException {
        Set<MotionProfile> motionProfiles = request(JsonRequestType.SupportedProfiles, null).getMotionProfiles();
        if (motionProfiles == null) {
            return Collections.emptySet();
        }
        return motionProfiles;
    }

    public JsonResponse request(JsonRequestType type, List<MotionSegment> segments) throws MotionException {
        JsonRequest request = new JsonRequest(type, segments);
        String json = JsonSerializer.toJson(request);
        try {
            return handleResponse(request, server.request(json));
        } catch (MotionValidationException e) {
            throw e;
        } catch (MotionException e) {
            throw e;
        } catch (IOException e) {
            throw new MotionValidationException(e.getMessage());
        }
    }

    private JsonResponse handleResponse(JsonRequest request, String responseText) throws MotionException {
        JsonResponse response = JsonSerializer.createResponse(responseText);
        if (response.getErrorSegments() != null) {
            throw new MotionValidationException(response.getErrorMessage(), response.getErrorSegments().stream()
                    .map(id -> getSegment(request.getSegments(), id)).collect(Collectors.toList()));
        }
        if (response.getErrorMessage() != null) {
            throw new MotionException(response.getErrorMessage());
        }
        return response;
    }

    private MotionSegment getSegment(List<MotionSegment> segments, String id) {
        return segments.stream().filter(s -> s.getId().equals(id)).findFirst().get();
    }
}
