/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.PositionInfo;

public class JsonResponse {
    private JsonRequestType requestType;

    private String errorMessage;

    private Set<String> errorSegments;

    private List<Double> times;

    private Set<MotionProfile> motionProfiles;

    private Collection<PositionInfo> positionInfo;

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setMotionProfiles(Set<MotionProfile> motionProfiles) {
        this.motionProfiles = motionProfiles;
    }

    public void setErrorSegments(Set<String> errorSegments) {
        this.errorSegments = errorSegments;
    }

    public void setPositions(Collection<PositionInfo> positions) {
        this.positionInfo = positions;
    }

    public void setTimes(List<Double> times) {
        this.times = times;
    }

    public void setRequestType(JsonRequestType type) {
        this.requestType = type;
    }

    public JsonRequestType getRequestType() {
        return requestType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Set<String> getErrorSegments() {
        return errorSegments;
    }

    public Collection<PositionInfo> getPositions() {
        return positionInfo;
    }

    public List<Double> getTimes() {
        return times;
    }

    public Set<MotionProfile> getMotionProfiles() {
        return motionProfiles;
    }
}
