/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class JsonSerializer {
    private static final Gson GSON;

    private static final int SCALE = 16;

    private static final TypeAdapter<BigDecimal> BIGDECIMAL_ADAPTER = new TypeAdapter<BigDecimal>() {
        @Override
        public void write(JsonWriter out, BigDecimal value) throws IOException {
            out.value(scale(value));
        }

        @Override
        public BigDecimal read(JsonReader in) throws IOException {
            return scale(new BigDecimal(in.nextString()));
        }
    };

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(BigDecimal.class, BIGDECIMAL_ADAPTER);
        GSON = builder.setPrettyPrinting().create();
    }

    private JsonSerializer() {
        // Empty
    }

    public static String toJson(JsonRequest o) {
        return GSON.toJson(o);
    }

    public static String toJson(JsonResponse o) {
        return GSON.toJson(o);
    }

    public static JsonRequest createRequest(String json) {
        return GSON.fromJson(json, JsonRequest.class);
    }

    public static JsonResponse createResponse(String json) {
        return GSON.fromJson(json, JsonResponse.class);
    }

    private static BigDecimal scale(BigDecimal value) {
        if (value != null && value.scale() > SCALE) {
            value = value.setScale(SCALE, RoundingMode.HALF_UP);
            for (int s = SCALE - 1; s >= 1; s--) {
                BigDecimal newValue = value.setScale(s, RoundingMode.HALF_UP);
                if (newValue.doubleValue() != value.doubleValue()) {
                    return value;
                }
                value = newValue;
            }
        }
        return value;
    }
}
