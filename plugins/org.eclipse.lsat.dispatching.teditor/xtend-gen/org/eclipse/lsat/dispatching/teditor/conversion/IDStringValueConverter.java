/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.conversion;

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.lib.Exceptions;

@FinalFieldsConstructor
@SuppressWarnings("all")
public class IDStringValueConverter implements IValueConverter<String> {
  private final IValueConverter<String> idValueConverter;
  
  private final IValueConverter<String> stringValueConverter;
  
  /**
   * Prefer ID with fallback to STRING
   */
  @Override
  public String toString(final String value) throws ValueConverterException {
    try {
      return this.idValueConverter.toString(value);
    } catch (final Throwable _t) {
      if (_t instanceof ValueConverterException) {
        return this.stringValueConverter.toString(value);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  /**
   * Try STRING with fallback to ID
   */
  @Override
  public String toValue(final String string, final INode node) throws ValueConverterException {
    String _xifexpression = null;
    if (((string != null) && (string.startsWith("\"") || string.startsWith("\'")))) {
      _xifexpression = this.stringValueConverter.toValue(string, node);
    } else {
      _xifexpression = this.idValueConverter.toValue(string, node);
    }
    return _xifexpression;
  }
  
  public IDStringValueConverter(final IValueConverter<String> idValueConverter, final IValueConverter<String> stringValueConverter) {
    super();
    this.idValueConverter = idValueConverter;
    this.stringValueConverter = stringValueConverter;
  }
}
