/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor;

import org.eclipse.lsat.dispatching.teditor.AbstractDispatchingRuntimeModule;
import org.eclipse.lsat.dispatching.teditor.conversion.DispatchingXtextValueConverterService;
import org.eclipse.lsat.dispatching.teditor.formatting.DispatchingFormatter;
import org.eclipse.lsat.machine.teditor.ImportResourceDescriptionStrategy;
import org.eclipse.lsat.machine.teditor.scoping.ImportScopeProvider;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.formatting.IFormatter;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.scoping.IGlobalScopeProvider;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
@SuppressWarnings("all")
public class DispatchingRuntimeModule extends AbstractDispatchingRuntimeModule {
  @Override
  public Class<? extends IFormatter> bindIFormatter() {
    return DispatchingFormatter.class;
  }
  
  @Override
  public Class<? extends IGlobalScopeProvider> bindIGlobalScopeProvider() {
    return ImportScopeProvider.class;
  }
  
  public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
    return ImportResourceDescriptionStrategy.class;
  }
  
  @Override
  public Class<? extends IValueConverterService> bindIValueConverterService() {
    return DispatchingXtextValueConverterService.class;
  }
}
