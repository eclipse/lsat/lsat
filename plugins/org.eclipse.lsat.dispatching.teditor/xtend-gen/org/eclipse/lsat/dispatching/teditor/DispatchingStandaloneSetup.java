/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor;

import org.eclipse.lsat.dispatching.teditor.DispatchingStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages
 * without equinox extension registry
 */
@SuppressWarnings("all")
public class DispatchingStandaloneSetup extends DispatchingStandaloneSetupGenerated {
  public static void doSetup() {
    new DispatchingStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
}
