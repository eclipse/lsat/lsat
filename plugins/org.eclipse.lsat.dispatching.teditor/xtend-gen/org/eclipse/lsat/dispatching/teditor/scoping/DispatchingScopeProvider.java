/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.scoping;

import activity.Activity;
import activity.ResourceAction;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import java.util.List;
import java.util.Set;
import machine.IResource;
import machine.Resource;
import machine.ResourceItem;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider;
import org.eclipse.xtext.scoping.impl.FilteringScope;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it
 */
@SuppressWarnings("all")
public class DispatchingScopeProvider extends AbstractDeclarativeScopeProvider {
  /**
   * Allow for shortName (@link ResourceItem name)
   */
  public IScope scope_ResourceItem(final Dispatch dis, final EReference ref) {
    Activity _activity = dis.getActivity();
    boolean _tripleEquals = (_activity == null);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    final Function1<Resource, EList<ResourceItem>> _function = (Resource it) -> {
      return it.getItems();
    };
    final Set<ResourceItem> eligibleItems = IterableExtensions.<ResourceItem>toSet(IterableExtensions.<Resource, ResourceItem>flatMap(dis.getActivity().getResourcesNeedingItem(), _function));
    final Function1<ResourceItem, String> _function_1 = (ResourceItem it) -> {
      return it.getName();
    };
    final Function1<List<ResourceItem>, Boolean> _function_2 = (List<ResourceItem> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size == 1));
    };
    final Set<ResourceItem> validNameItems = IterableExtensions.<ResourceItem>toSet(Iterables.<ResourceItem>concat(IterableExtensions.<List<ResourceItem>>filter(IterableExtensions.<String, ResourceItem>groupBy(eligibleItems, _function_1).values(), _function_2)));
    final Function1<ResourceItem, String> _function_3 = (ResourceItem it) -> {
      return it.fqn();
    };
    final Set<String> fqns = IterableExtensions.<String>toSet(IterableExtensions.<ResourceItem, String>map(eligibleItems, _function_3));
    IScope _delegateGetScope = this.delegateGetScope(dis, ref);
    final Predicate<IEObjectDescription> _function_4 = (IEObjectDescription it) -> {
      return fqns.contains(it.getQualifiedName().toString());
    };
    FilteringScope _filteringScope = new FilteringScope(_delegateGetScope, _function_4);
    return Scopes.scopeFor(validNameItems, _filteringScope);
  }
  
  public IScope ResourceYieldMapEntry(final DispatchGroup dg, final EReference ref) {
    EList<Dispatch> _dispatches = dg.getDispatches();
    boolean _tripleEquals = (_dispatches == null);
    if (_tripleEquals) {
      return IScope.NULLSCOPE;
    }
    final Function1<Dispatch, EList<Node>> _function = (Dispatch it) -> {
      return it.getActivity().getNodes();
    };
    final Function1<ResourceAction, IResource> _function_1 = (ResourceAction it) -> {
      return it.getResource();
    };
    return Scopes.scopeFor(IterableExtensions.<IResource>toSet(IterableExtensions.<ResourceAction, IResource>map(Iterables.<ResourceAction>filter(IterableExtensions.<Dispatch, Node>flatMap(dg.getDispatches(), _function), ResourceAction.class), _function_1)));
  }
}
