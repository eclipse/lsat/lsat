/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.formatting;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.lsat.dispatching.teditor.services.DispatchingGrammarAccess;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;
import org.eclipse.xtext.util.Pair;
import org.eclipse.xtext.xbase.lib.Extension;

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
@SuppressWarnings("all")
public class DispatchingFormatter extends AbstractDeclarativeFormatter {
  @Inject
  @Extension
  private DispatchingGrammarAccess _dispatchingGrammarAccess;
  
  @Override
  protected void configureFormatting(final FormattingConfig c) {
    c.setAutoLinewrap(120);
    List<Pair<Keyword, Keyword>> _findKeywordPairs = this._dispatchingGrammarAccess.findKeywordPairs("{", "}");
    for (final Pair<Keyword, Keyword> pair : _findKeywordPairs) {
      {
        c.setIndentation(pair.getFirst(), pair.getSecond());
        c.setLinewrap(1).after(pair.getFirst());
        c.setLinewrap(1).before(pair.getSecond());
        c.setLinewrap(1).after(pair.getSecond());
      }
    }
    List<Pair<Keyword, Keyword>> _findKeywordPairs_1 = this._dispatchingGrammarAccess.findKeywordPairs("[", "]");
    for (final Pair<Keyword, Keyword> pair_1 : _findKeywordPairs_1) {
      {
        c.setNoSpace().before(pair_1.getSecond());
        c.setNoSpace().after(pair_1.getFirst());
        c.setNoSpace().before(pair_1.getFirst());
      }
    }
    List<Pair<Keyword, Keyword>> _findKeywordPairs_2 = this._dispatchingGrammarAccess.findKeywordPairs("(", ")");
    for (final Pair<Keyword, Keyword> pair_2 : _findKeywordPairs_2) {
      {
        c.setNoSpace().before(pair_2.getSecond());
        c.setNoSpace().after(pair_2.getFirst());
        c.setNoSpace().before(pair_2.getFirst());
      }
    }
    List<Keyword> _findKeywords = this._dispatchingGrammarAccess.findKeywords(":");
    for (final Keyword doublecolon : _findKeywords) {
      {
        c.setNoLinewrap().before(doublecolon);
        c.setNoSpace().before(doublecolon);
      }
    }
    List<Keyword> _findKeywords_1 = this._dispatchingGrammarAccess.findKeywords("=", ".");
    for (final Keyword delim : _findKeywords_1) {
      {
        c.setNoLinewrap().before(delim);
        c.setNoLinewrap().after(delim);
        c.setNoSpace().before(delim);
        c.setNoSpace().after(delim);
      }
    }
    List<Keyword> _findKeywords_2 = this._dispatchingGrammarAccess.findKeywords(",");
    for (final Keyword comma : _findKeywords_2) {
      {
        c.setNoLinewrap().before(comma);
        c.setNoLinewrap().after(comma);
        c.setNoSpace().before(comma);
      }
    }
    c.setLinewrap(2, 2, 2).before(this._dispatchingGrammarAccess.getDispatchGroupRule());
    c.setLinewrap(2, 2, 2).after(this._dispatchingGrammarAccess.getDispatchGroupRule());
    c.setLinewrap(1).after(this._dispatchingGrammarAccess.getImportRule());
    c.setLinewrap(1).before(this._dispatchingGrammarAccess.getActivityDispatchingAccess().getThroughputKeyword_2_0());
    c.setLinewrap(1).before(this._dispatchingGrammarAccess.getDispatchRule());
    c.setLinewrap(1).after(this._dispatchingGrammarAccess.getDispatchRule());
    c.setLinewrap(1).before(this._dispatchingGrammarAccess.getDispatchGroupAccess().getYieldKeyword_6_0());
    c.setLinewrap(1).before(this._dispatchingGrammarAccess.getDispatchGroupAccess().getOffsetKeyword_8_0());
    c.setLinewrap(1).before(this._dispatchingGrammarAccess.getResourceYieldMapEntryRule());
    c.setLinewrap(0, 1, 2).before(this._dispatchingGrammarAccess.getSL_COMMENTRule());
    c.setLinewrap(0, 1, 2).before(this._dispatchingGrammarAccess.getML_COMMENTRule());
    c.setLinewrap(0, 1, 1).after(this._dispatchingGrammarAccess.getML_COMMENTRule());
  }
}
