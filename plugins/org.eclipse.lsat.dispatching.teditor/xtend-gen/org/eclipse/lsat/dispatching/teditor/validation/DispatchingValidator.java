/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.validation;

import activity.Activity;
import activity.Claim;
import activity.LocationPrerequisite;
import activity.Move;
import activity.util.EventSequenceValidator;
import com.google.common.collect.Iterables;
import dispatching.ActivityDispatching;
import dispatching.Attribute;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import dispatching.DispatchingPackage;
import dispatching.impl.AttributesMapEntryImpl;
import dispatching.util.DispatchingUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import machine.IResource;
import machine.Import;
import machine.MachinePackage;
import machine.Peripheral;
import machine.Resource;
import machine.ResourceItem;
import machine.SymbolicPosition;
import machine.util.ResourcePeripheralKey;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.graph.directed.editable.EdgQueries;
import org.eclipse.lsat.common.graph.directed.editable.Node;
import org.eclipse.lsat.common.queries.QueryableIterable;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.dispatching.teditor.validation.AbstractDispatchingValidator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IntegerRange;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class DispatchingValidator extends AbstractDispatchingValidator {
  private static final class PositionInfo {
    private final SymbolicPosition position;
    
    private final Activity activity;
    
    PositionInfo(final SymbolicPosition position, final Activity activity) {
      this.position = position;
      this.activity = activity;
    }
  }
  
  public static final String GREATER_OFFSET = "greaterOffset";
  
  public static final String INVALID_IMPORT = "invalidImport";
  
  public static final String REMOVE_ITEM = "removeItem";
  
  public static final String SELECT_ITEM_FOR_RESOURCE = "selectItemForResource";
  
  public static final String REPLACE_ITEM_FOR_RESOURCE = "replaceItemForResource";
  
  public static final String CONVERT_YIELD = "convertYield";
  
  @Check
  public void checkObsoleteThroughput(final ActivityDispatching ad) {
    boolean _eIsSet = ad.eIsSet(DispatchingPackage.Literals.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS);
    if (_eIsSet) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Througput is obsolete. Specify yield below in activities");
      this.warning(_builder.toString(), ad, 
        DispatchingPackage.Literals.ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS, DispatchingValidator.CONVERT_YIELD);
    }
    for (int i = 0; (i < ad.getResourceIterations().size()); i++) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Througput is obsolete. Specify yield below in activities");
      this.warning(_builder_1.toString(), ad, 
        DispatchingPackage.Literals.ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS, i, DispatchingValidator.CONVERT_YIELD);
    }
  }
  
  @Check
  public void checkDuplicateDispatchGroup(final ActivityDispatching ad) {
    final Function1<DispatchGroup, Boolean> _function = (DispatchGroup it) -> {
      return Boolean.valueOf(it.eIsSet(DispatchingPackage.Literals.DISPATCH_GROUP__NAME));
    };
    final Function1<DispatchGroup, String> _function_1 = (DispatchGroup it) -> {
      return it.getName();
    };
    final Function1<List<DispatchGroup>, Boolean> _function_2 = (List<DispatchGroup> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<DispatchGroup> _function_3 = (DispatchGroup it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Duplicate dispatch group name  \'");
      String _name = it.getName();
      _builder.append(_name);
      _builder.append("\' Rename one");
      this.error(_builder.toString(), ad, 
        DispatchingPackage.Literals.ACTIVITY_DISPATCHING__DISPATCH_GROUPS, ad.getDispatchGroups().indexOf(it));
    };
    Iterables.<DispatchGroup>concat(IterableExtensions.<List<DispatchGroup>>filter(IterableExtensions.<String, DispatchGroup>groupBy(IterableExtensions.<DispatchGroup>filter(ad.getDispatchGroups(), _function), _function_1).values(), _function_2)).forEach(_function_3);
  }
  
  @Check
  public void checkImportIsValid(final Import imp) {
    try {
      final boolean isImportUriValid = EcoreUtil2.isValidUri(imp, URI.createURI(imp.getImportURI()));
      if ((!isImportUriValid)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("The import ");
        String _importURI = imp.getImportURI();
        _builder.append(_importURI);
        _builder.append(" cannot be resolved. Make sure that the name is spelled correctly.");
        this.error(_builder.toString(), imp, MachinePackage.Literals.IMPORT__IMPORT_URI, DispatchingValidator.INVALID_IMPORT);
      }
      final boolean isUnderstood = imp.getImportURI().matches(".*\\.(activity)");
      if ((!isUnderstood)) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("Importing ");
        String _importURI_1 = imp.getImportURI();
        _builder_1.append(_importURI_1);
        _builder_1.append(" is not allowed. Only \'activity\' files are allowed");
        this.error(_builder_1.toString(), imp, MachinePackage.Literals.IMPORT__IMPORT_URI, DispatchingValidator.INVALID_IMPORT);
      }
    } catch (final Throwable _t) {
      if (_t instanceof IllegalArgumentException) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("The import ");
        String _importURI_2 = imp.getImportURI();
        _builder_2.append(_importURI_2);
        _builder_2.append(" is not a valid URI.");
        this.error(_builder_2.toString(), imp, 
          MachinePackage.Literals.IMPORT__IMPORT_URI, DispatchingValidator.INVALID_IMPORT);
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
  }
  
  @Check
  public void checkOffsetShouldBeGreaterThanPrevious(final ActivityDispatching activityDispatching) {
    EList<DispatchGroup> dispatchGroups = activityDispatching.getDispatchGroups();
    for (int i = 1; (i < dispatchGroups.size()); i++) {
      BigDecimal _offset = dispatchGroups.get(i).getOffset();
      BigDecimal _offset_1 = dispatchGroups.get((i - 1)).getOffset();
      boolean _lessThan = (_offset.compareTo(_offset_1) < 0);
      if (_lessThan) {
        this.error("Offset should be greater then previously defined offset", dispatchGroups.get(i), 
          DispatchingPackage.Literals.DISPATCH_GROUP__OFFSET, DispatchingValidator.GREATER_OFFSET);
      }
    }
  }
  
  @Check
  public void checkResourceItemsDuplicate(final Dispatch dis) {
    final Function1<ResourceItem, Resource> _function = (ResourceItem it) -> {
      return it.getResource();
    };
    final Function1<List<ResourceItem>, Boolean> _function_1 = (List<ResourceItem> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<ResourceItem> _function_2 = (ResourceItem it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Duplicate item specified for Resource \'");
      String _fqn = it.getResource().fqn();
      _builder.append(_fqn);
      _builder.append("\' Remove one");
      this.error(_builder.toString(), dis, 
        DispatchingPackage.Literals.DISPATCH__RESOURCE_ITEMS, dis.getResourceItems().indexOf(it), DispatchingValidator.REMOVE_ITEM);
    };
    Iterables.<ResourceItem>concat(IterableExtensions.<List<ResourceItem>>filter(IterableExtensions.<Resource, ResourceItem>groupBy(dis.getResourceItems(), _function).values(), _function_1)).forEach(_function_2);
  }
  
  @Check
  public void checkMissingResourceItem(final Dispatch dis) {
    final EList<Resource> resourcesNeedingItem = this.getResourcesNeedingItem(dis);
    EcoreUtil.resolveAll(dis.eContainer());
    final Function1<ResourceItem, Resource> _function = (ResourceItem it) -> {
      return it.getResource();
    };
    resourcesNeedingItem.removeAll(ListExtensions.<ResourceItem, Resource>map(dis.getResourceItems(), _function));
    final Consumer<Resource> _function_1 = (Resource res) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("No item specified for Resource \'");
      String _fqn = res.fqn();
      _builder.append(_fqn);
      _builder.append("\' Press <ctrl> spacebar");
      this.error(_builder.toString(), dis, 
        DispatchingPackage.Literals.DISPATCH__RESOURCE_ITEMS);
    };
    resourcesNeedingItem.forEach(_function_1);
  }
  
  @Check
  public void checkResourceItemNeedsResource(final Dispatch dis) {
    final Procedure2<ResourceItem, Integer> _function = (ResourceItem item, Integer index) -> {
      int _size = this.getResourcesNeedingItem(dis).size();
      boolean _greaterEqualsThan = ((index).intValue() >= _size);
      if (_greaterEqualsThan) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Unnecessary item specified  \'");
        String _fqn = item.fqn();
        _builder.append(_fqn);
        _builder.append("\' ");
        this.error(_builder.toString(), dis, 
          DispatchingPackage.Literals.DISPATCH__RESOURCE_ITEMS, (index).intValue(), DispatchingValidator.REMOVE_ITEM);
      }
    };
    IterableExtensions.<ResourceItem>forEach(dis.getResourceItems(), _function);
  }
  
  @Check
  public void validateAttributesDuplicate(final Dispatch dis) {
    final Function1<Map.Entry<Attribute, String>, String> _function = (Map.Entry<Attribute, String> it) -> {
      return it.getKey().getName();
    };
    final Function1<List<Map.Entry<Attribute, String>>, Boolean> _function_1 = (List<Map.Entry<Attribute, String>> it) -> {
      int _size = it.size();
      return Boolean.valueOf((_size > 1));
    };
    final Consumer<Map.Entry<Attribute, String>> _function_2 = (Map.Entry<Attribute, String> it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Duplicate item specified for Attribute \'");
      String _name = it.getKey().getName();
      _builder.append(_name);
      _builder.append("\' Remove one");
      this.error(_builder.toString(), dis, 
        DispatchingPackage.Literals.HAS_USER_ATTRIBUTES__USER_ATTRIBUTES, dis.getUserAttributes().indexOf(it), 
        DispatchingValidator.REMOVE_ITEM);
    };
    Iterables.<Map.Entry<Attribute, String>>concat(IterableExtensions.<List<Map.Entry<Attribute, String>>>filter(IterableExtensions.<String, Map.Entry<Attribute, String>>groupBy(dis.getUserAttributes(), _function).values(), _function_1)).forEach(_function_2);
  }
  
  @Check
  public void validateAttributeValue(final AttributesMapEntryImpl entry) {
    String _value = entry.getValue();
    boolean _tripleEquals = (_value == null);
    if (_tripleEquals) {
      return;
    }
    final String illegalChars = entry.getValue().replaceAll("[\\w\\.\\-\\s]", "");
    boolean _isEmpty = illegalChars.isEmpty();
    boolean _not = (!_isEmpty);
    if (_not) {
      final Dispatch parent = EcoreUtil2.<Dispatch>getContainerOfType(entry, Dispatch.class);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Attribute value may not contain  \'");
      _builder.append(illegalChars);
      _builder.append("\'. only \'a-zA-Z0-9-_.<space><tab>\' are allowed ");
      this.error(_builder.toString(), parent, DispatchingPackage.Literals.HAS_USER_ATTRIBUTES__USER_ATTRIBUTES, 
        parent.getUserAttributes().indexOf(entry), DispatchingValidator.REMOVE_ITEM);
    }
  }
  
  @Check
  public void validatePosition(final ActivityDispatching activityDispatching) {
    final LinkedHashMap<ResourcePeripheralKey, DispatchingValidator.PositionInfo> positionState = CollectionLiterals.<ResourcePeripheralKey, DispatchingValidator.PositionInfo>newLinkedHashMap();
    final Consumer<DispatchGroup> _function = (DispatchGroup dispatchGroup) -> {
      final Procedure2<Dispatch, Integer> _function_1 = (Dispatch disp, Integer index) -> {
        Activity activity = disp.getActivity();
        LinkedHashMap<ResourcePeripheralKey, SymbolicPosition> locationPrerequisites = this.indexPrerequisites(disp, (index).intValue());
        Set<Map.Entry<ResourcePeripheralKey, SymbolicPosition>> _entrySet = locationPrerequisites.entrySet();
        for (final Map.Entry<ResourcePeripheralKey, SymbolicPosition> prerequisite : _entrySet) {
          {
            ResourcePeripheralKey _key = prerequisite.getKey();
            SymbolicPosition _value = prerequisite.getValue();
            DispatchingValidator.PositionInfo _positionInfo = new DispatchingValidator.PositionInfo(_value, activity);
            DispatchingValidator.PositionInfo positionInfo = positionState.put(_key, _positionInfo);
            if (((null != positionInfo) && (!positionInfo.position.equals(prerequisite.getValue())))) {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("Activity ");
              String _name = activity.getName();
              _builder.append(_name);
              _builder.append(" requires peripheral ");
              String _fqn = prerequisite.getKey().fqn();
              _builder.append(_fqn);
              _builder.append(" at position ");
              String _name_1 = prerequisite.getValue().getName();
              _builder.append(_name_1);
              _builder.append(", but activity ");
              String _name_2 = positionInfo.activity.getName();
              _builder.append(_name_2);
              _builder.append(" leaves it at position ");
              String _name_3 = positionInfo.position.getName();
              _builder.append(_name_3);
              this.warning(_builder.toString(), disp, 
                DispatchingPackage.Literals.DISPATCH__ACTIVITY);
            }
          }
        }
        EList<Node> topologicalOrder = EdgQueries.<Node>topologicalOrdering(activity.getNodes());
        if ((null == topologicalOrder)) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("Location prerequisites cannot be validated for the next activity as activity ");
          String _name = activity.getName();
          _builder.append(_name);
          _builder.append(" contains a cycle");
          this.error(_builder.toString(), disp, 
            DispatchingPackage.Literals.DISPATCH__ACTIVITY);
          positionState.clear();
        } else {
          final Function1<Move, Boolean> _function_2 = (Move it) -> {
            return Boolean.valueOf(it.isPositionMove());
          };
          Iterable<Move> _filter = IterableExtensions.<Move>filter(QueryableIterable.<Node>from(topologicalOrder).<Move>objectsOfKind(Move.class), _function_2);
          for (final Move move : _filter) {
            IResource _actualResource = DispatchingValidator.getActualResource(disp, move.getResource());
            Peripheral _peripheral = move.getPeripheral();
            ResourcePeripheralKey _resourcePeripheralKey = new ResourcePeripheralKey(_actualResource, _peripheral);
            SymbolicPosition _targetPosition = move.getTargetPosition();
            DispatchingValidator.PositionInfo _positionInfo = new DispatchingValidator.PositionInfo(_targetPosition, activity);
            positionState.put(_resourcePeripheralKey, _positionInfo);
          }
        }
      };
      IterableExtensions.<Dispatch>forEach(dispatchGroup.getDispatches(), _function_1);
    };
    activityDispatching.getDispatchGroups().forEach(_function);
  }
  
  /**
   * Checks the number if events raised and required match test rules:
   * <ul>
   *  <li>#receives <= #sends at any point in the sequence</li>
   *  <li>0 <= #sends - #receives <= 1, such that at most one event can be outstanding</li>
   *  <li>the number of sends and receives matches over the full sequence for the event</li>
   * </ul>
   */
  @Check
  public void checkUnfolded(final ActivityDispatching activityDispatching) {
    final List<Dispatch> unfoldedDispatches = DispatchingValidator.unfoldedDispatches(activityDispatching);
    final EventSequenceValidator.ErrorRaiser errorRaiser = new EventSequenceValidator.ErrorRaiser() {
      @Override
      public void raise(final int listIndex, final String msg) {
        if ((listIndex < 0)) {
          DispatchingValidator.this.error(msg, activityDispatching, DispatchingPackage.Literals.ACTIVITY_DISPATCHING__DISPATCH_GROUPS);
        } else {
          final Dispatch d = unfoldedDispatches.get(listIndex);
          EObject _eContainer = d.eContainer();
          DispatchGroup dg = ((DispatchGroup) _eContainer);
          DispatchingValidator.this.error(msg, dg, DispatchingPackage.Literals.DISPATCH_GROUP__DISPATCHES, dg.getDispatches().indexOf(d));
        }
      }
    };
    final Function1<Dispatch, Activity> _function = (Dispatch it) -> {
      return it.getActivity();
    };
    EventSequenceValidator.validate(IterableExtensions.<Activity>toList(ListExtensions.<Dispatch, Activity>map(unfoldedDispatches, _function)), errorRaiser);
    final Function1<Dispatch, Iterable<Pair<Claim, Dispatch>>> _function_1 = (Dispatch d) -> {
      final Function1<Claim, Pair<Claim, Dispatch>> _function_2 = (Claim c) -> {
        return Pair.<Claim, Dispatch>of(c, d);
      };
      return IterableExtensions.<Claim, Pair<Claim, Dispatch>>map(Iterables.<Claim>filter(d.getActivity().getNodes(), Claim.class), _function_2);
    };
    final Iterable<Pair<Claim, Dispatch>> claimsWithDispatches = IterableExtensions.<Dispatch, Pair<Claim, Dispatch>>flatMap(unfoldedDispatches, _function_1);
    final Function1<Pair<Claim, Dispatch>, IResource> _function_2 = (Pair<Claim, Dispatch> it) -> {
      return it.getKey().getResource();
    };
    final Function1<List<Pair<Claim, Dispatch>>, Iterable<Dispatch>> _function_3 = (List<Pair<Claim, Dispatch>> claims) -> {
      final Function2<Pair<Claim, Dispatch>, Pair<Claim, Dispatch>, Boolean> _function_4 = (Pair<Claim, Dispatch> p1, Pair<Claim, Dispatch> p2) -> {
        boolean _isPassive = p1.getKey().isPassive();
        boolean _isPassive_1 = p2.getKey().isPassive();
        return Boolean.valueOf((_isPassive == _isPassive_1));
      };
      final Function1<List<Pair<Claim, Dispatch>>, Boolean> _function_5 = (List<Pair<Claim, Dispatch>> it) -> {
        return Boolean.valueOf(IterableExtensions.<Pair<Claim, Dispatch>>head(it).getKey().isPassive());
      };
      final Iterable<List<Pair<Claim, Dispatch>>> passiveClaimsGroups = IterableExtensions.<List<Pair<Claim, Dispatch>>>filter(Queries.<Pair<Claim, Dispatch>>segment(claims, _function_4), _function_5);
      final Function1<List<Pair<Claim, Dispatch>>, Iterable<Dispatch>> _function_6 = (List<Pair<Claim, Dispatch>> passiveClaims) -> {
        final Function1<Pair<Claim, Dispatch>, Activity> _function_7 = (Pair<Claim, Dispatch> it) -> {
          return it.getValue().getActivity();
        };
        final Function1<List<Pair<Claim, Dispatch>>, Boolean> _function_8 = (List<Pair<Claim, Dispatch>> it) -> {
          int _size = it.size();
          return Boolean.valueOf((_size > 1));
        };
        final Iterable<Pair<Claim, Dispatch>> duplicatePassiveClaims = Iterables.<Pair<Claim, Dispatch>>concat(IterableExtensions.<List<Pair<Claim, Dispatch>>>filter(IterableExtensions.<Activity, Pair<Claim, Dispatch>>groupBy(passiveClaims, _function_7).values(), _function_8));
        final Function1<Pair<Claim, Dispatch>, Dispatch> _function_9 = (Pair<Claim, Dispatch> it) -> {
          return it.getValue();
        };
        return IterableExtensions.<Pair<Claim, Dispatch>, Dispatch>map(duplicatePassiveClaims, _function_9);
      };
      return IterableExtensions.<List<Pair<Claim, Dispatch>>, Dispatch>flatMap(passiveClaimsGroups, _function_6);
    };
    final Iterable<Dispatch> duplicatePassiveClaimDispatches = IterableExtensions.<List<Pair<Claim, Dispatch>>, Dispatch>flatMap(IterableExtensions.<IResource, Pair<Claim, Dispatch>>groupBy(claimsWithDispatches, _function_2).values(), _function_3);
    final Consumer<Dispatch> _function_4 = (Dispatch dispatch) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Activity ");
      String _name = dispatch.getActivity().getName();
      _builder.append(_name);
      _builder.append(" cannot be repeated within a passive claim sequence.");
      this.error(_builder.toString(), dispatch, 
        DispatchingPackage.Literals.DISPATCH__ACTIVITY);
    };
    Queries.<Dispatch>unique(duplicatePassiveClaimDispatches).forEach(_function_4);
  }
  
  private static List<Dispatch> unfoldedDispatches(final ActivityDispatching activityDispatching) {
    final ArrayList<Dispatch> unfoldedDispatches = CollectionLiterals.<Dispatch>newArrayList();
    final Consumer<DispatchGroup> _function = (DispatchGroup dg) -> {
      int _numberRepeats = DispatchingUtil.getNumberRepeats(dg);
      IntegerRange _upTo = new IntegerRange(1, _numberRepeats);
      for (final Integer i : _upTo) {
        unfoldedDispatches.addAll(dg.getDispatches());
      }
    };
    activityDispatching.getDispatchGroups().forEach(_function);
    return unfoldedDispatches;
  }
  
  @Check
  public void validateRepeatAndOffset(final DispatchGroup group) {
    if (((BigDecimal.ZERO.compareTo(group.getOffset()) != 0) && (DispatchingUtil.getNumberRepeats(group) > 1))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("offset must be ommited or zero when repeats are specified");
      final String msg = _builder.toString();
      this.error(msg, group, DispatchingPackage.Literals.DISPATCH_GROUP__OFFSET);
      this.error(msg, group, DispatchingPackage.Literals.DISPATCH_GROUP__REPEATS, 0);
    }
  }
  
  @Check
  public void validatePhase(final AttributesMapEntryImpl entry) {
    final DispatchGroup dispatchGroup = EcoreUtil2.<DispatchGroup>getContainerOfType(entry, DispatchGroup.class);
    if ((dispatchGroup == null)) {
      return;
    }
    boolean _and = false;
    Attribute _key = entry.getKey();
    String _name = null;
    if (_key!=null) {
      _name=_key.getName();
    }
    boolean _equalsIgnoreCase = "phase".equalsIgnoreCase(_name);
    if (!_equalsIgnoreCase) {
      _and = false;
    } else {
      boolean _equalsIgnoreCase_1 = dispatchGroup.getName().equalsIgnoreCase(entry.getValue());
      boolean _not = (!_equalsIgnoreCase_1);
      _and = _not;
    }
    if (_and) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("phase is a reserved attribute and may not be used");
      final String msg = _builder.toString();
      this.error(msg, entry, DispatchingPackage.Literals.ATTRIBUTES_MAP_ENTRY__KEY);
    }
  }
  
  @Check
  public void validateIteratorName(final AttributesMapEntryImpl entry) {
    final DispatchGroup dispatchGroup = EcoreUtil2.<DispatchGroup>getContainerOfType(entry, DispatchGroup.class);
    if ((dispatchGroup == null)) {
      return;
    }
    boolean _and = false;
    String _iteratorName = dispatchGroup.getIteratorName();
    Attribute _key = entry.getKey();
    String _name = null;
    if (_key!=null) {
      _name=_key.getName();
    }
    boolean _equalsIgnoreCase = _iteratorName.equalsIgnoreCase(_name);
    if (!_equalsIgnoreCase) {
      _and = false;
    } else {
      int _numberRepeats = DispatchingUtil.getNumberRepeats(dispatchGroup);
      boolean _greaterThan = (_numberRepeats > 1);
      _and = _greaterThan;
    }
    if (_and) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("attribute key may not be equal to activites iterator name. Change key or iterator name ");
      final String msg = _builder.toString();
      this.error(msg, entry, DispatchingPackage.Literals.ATTRIBUTES_MAP_ENTRY__KEY);
      boolean _eIsSet = dispatchGroup.eIsSet(DispatchingPackage.Literals.DISPATCH_GROUP__ITERATOR_NAME);
      if (_eIsSet) {
        this.error(msg, dispatchGroup, DispatchingPackage.Literals.DISPATCH_GROUP__ITERATOR_NAME);
      }
    }
  }
  
  public EList<Resource> getResourcesNeedingItem(final Dispatch dis) {
    return dis.getActivity().getResourcesNeedingItem();
  }
  
  private LinkedHashMap<ResourcePeripheralKey, SymbolicPosition> indexPrerequisites(final Dispatch disp, final int index) {
    EList<LocationPrerequisite> prerequisites = disp.getActivity().getPrerequisites();
    final LinkedHashMap<ResourcePeripheralKey, SymbolicPosition> result = CollectionLiterals.<ResourcePeripheralKey, SymbolicPosition>newLinkedHashMap();
    for (final LocationPrerequisite prerequisite : prerequisites) {
      {
        IResource _actualResource = DispatchingValidator.getActualResource(disp, prerequisite.getResource());
        Peripheral _peripheral = prerequisite.getPeripheral();
        ResourcePeripheralKey key = new ResourcePeripheralKey(_actualResource, _peripheral);
        boolean _containsKey = result.containsKey(key);
        if (_containsKey) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("Only one location prerequisite per peripheral is allowed");
          this.error(_builder.toString(), disp, 
            DispatchingPackage.Literals.DISPATCH__ACTIVITY);
        }
        result.put(key, prerequisite.getPosition());
      }
    }
    return result;
  }
  
  private static IResource getActualResource(final Dispatch dispatch, final IResource resource) {
    final Function1<IResource, Boolean> _function = (IResource it) -> {
      Resource _resource = it.getResource();
      return Boolean.valueOf((_resource == resource));
    };
    final IResource result = IterableExtensions.<IResource>findFirst(Iterables.<IResource>filter(dispatch.getResourceItems(), IResource.class), _function);
    IResource _xifexpression = null;
    if ((result == null)) {
      _xifexpression = resource;
    } else {
      _xifexpression = result;
    }
    return _xifexpression;
  }
}
