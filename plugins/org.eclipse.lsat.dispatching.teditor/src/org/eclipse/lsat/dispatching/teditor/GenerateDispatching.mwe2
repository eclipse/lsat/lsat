/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
module org.eclipse.lsat.dispatching.teditor.GenerateDispatching

import org.eclipse.xtext.xtext.generator.*
import org.eclipse.xtext.xtext.generator.model.project.*
import org.eclipse.emf.mwe.utils.*

var rootPath = ".."
var projectName = "org.eclipse.lsat.dispatching.teditor"
var runtimeProject = "../${projectName}"
//@formatter:off
var fileHeaderText =
"/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Do not edit: This file is generated by Xtext
 */"
//@formatter:on
Workflow {

    bean = StandaloneSetup {
        scanClassPath = true
        platformUri = "${runtimeProject}/.."
        registerGeneratedEPackage = "dispatching.DispatchingPackage"
        registerGenModelFile = "platform:/resource/org.eclipse.lsat.dispatching.dsl/model/dispatching.genmodel"
    }

    component = XtextGenerator {

        configuration = {
            project = StandardProjectConfig {
                baseName = "${projectName}"
                rootPath = rootPath
                eclipsePlugin = {
                    enabled = true
                }
                createEclipseMetaData = true
            }
            code = {
                encoding = "UTF-8"
                lineDelimiter = "\n"
                fileHeader = "${fileHeaderText}"
            }
        }
        language = StandardLanguage {
            name = "${projectName}.Dispatching"
            fileExtensions = "dispatching"
            referencedResource = "platform:/resource/org.eclipse.lsat.dispatching.dsl/model/dispatching.genmodel"
            fragment = ecore2xtext.Ecore2XtextValueConverterServiceFragment2 auto-inject {}
            
            generator = {
                generateStub = false
            }

            // Patch for issue related to use of EMap: https://github.com/eclipse/xtext/issues/1991
            serializer = org.eclipse.lsat.common.xtext.generator.PatchedSerializerFragment2 {
                generateStub = true
            }
            validator = {
            // composedCheck = "org.eclipse.xtext.validation.NamesAreUniqueValidator"
            // Generates checks for @Deprecated grammar annotations, an IssueProvider and a corresponding PropertyPage
                generateDeprecationValidation = true
            }
            junitSupport = {
                junitVersion = "5"
            }
        }
    }
}
