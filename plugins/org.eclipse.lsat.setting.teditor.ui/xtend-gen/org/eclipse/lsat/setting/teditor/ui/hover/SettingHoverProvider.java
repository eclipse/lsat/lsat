/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.setting.teditor.ui.hover;

import expressions.Declaration;
import expressions.Expression;
import java.math.BigDecimal;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import setting.PhysicalLocation;
import setting.impl.LocationSettingsMapEntryImpl;
import setting.impl.MotionArgumentsMapEntryImpl;
import setting.impl.MotionSettingsMapEntryImpl;
import timing.Array;
import timing.EnumeratedDistribution;
import timing.FixedValue;
import timing.NormalDistribution;
import timing.PertDistribution;
import timing.TriangularDistribution;

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
@SuppressWarnings("all")
public class SettingHoverProvider extends DefaultEObjectHoverProvider {
  @Override
  public String getFirstLine(final EObject o) {
    final EObject parent = o.eContainer();
    String _switchResult = null;
    boolean _matched = false;
    if (parent instanceof LocationSettingsMapEntryImpl) {
      _matched=true;
      _switchResult = ((LocationSettingsMapEntryImpl)parent).getKey().getName();
    }
    if (!_matched) {
      if (parent instanceof MotionSettingsMapEntryImpl) {
        _matched=true;
        _switchResult = ((MotionSettingsMapEntryImpl)parent).getKey().getName();
      }
    }
    if (!_matched) {
      if (parent instanceof MotionArgumentsMapEntryImpl) {
        _matched=true;
        _switchResult = ((MotionArgumentsMapEntryImpl)parent).getKey();
      }
    }
    if (!_matched) {
      _switchResult = super.getFirstLine(o);
    }
    final String def = _switchResult;
    final Object value = this.toString(o);
    if ((value != null)) {
      return ((def + " = ") + value);
    }
    return def;
  }
  
  @Override
  public boolean hasHover(final EObject o) {
    return ((this.toString(o) != null) || super.hasHover(o));
  }
  
  private Object toString(final EObject o) {
    if ((o instanceof Declaration)) {
      Expression _expression = ((Declaration)o).getExpression();
      BigDecimal _evaluate = null;
      if (_expression!=null) {
        _evaluate=_expression.evaluate();
      }
      return _evaluate;
    }
    if ((o instanceof PhysicalLocation)) {
      return ((PhysicalLocation)o).getDefault();
    }
    if ((o instanceof FixedValue)) {
      return ((FixedValue)o).getValue();
    }
    if ((o instanceof Array)) {
      return IterableExtensions.join(((Array)o).getValues(), (("[" + ",") + "]"));
    }
    if ((o instanceof TriangularDistribution)) {
      return ((TriangularDistribution)o).getDefault();
    }
    if ((o instanceof PertDistribution)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("{min=");
      BigDecimal _min = ((PertDistribution)o).getMin();
      _builder.append(_min);
      _builder.append(", max=");
      BigDecimal _max = ((PertDistribution)o).getMax();
      _builder.append(_max);
      _builder.append(", mode=");
      BigDecimal _mode = ((PertDistribution)o).getMode();
      _builder.append(_mode);
      _builder.append(", gamma=");
      BigDecimal _gamma = ((PertDistribution)o).getGamma();
      _builder.append(_gamma);
      _builder.append(", default=");
      BigDecimal _default = ((PertDistribution)o).getDefault();
      _builder.append(_default);
      _builder.append("}");
      return _builder.toString();
    }
    if ((o instanceof NormalDistribution)) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("{mean=");
      BigDecimal _mean = ((NormalDistribution)o).getMean();
      _builder_1.append(_mean);
      _builder_1.append(", sd=");
      BigDecimal _sd = ((NormalDistribution)o).getSd();
      _builder_1.append(_sd);
      _builder_1.append(", default=");
      BigDecimal _default_1 = ((NormalDistribution)o).getDefault();
      _builder_1.append(_default_1);
      _builder_1.append("}");
      return _builder_1.toString();
    }
    if ((o instanceof EnumeratedDistribution)) {
      return IterableExtensions.join(((EnumeratedDistribution)o).getValues(), (("[" + ",") + "]"));
    }
    if ((o instanceof Expression)) {
      return ((Expression)o).evaluate();
    }
    return null;
  }
}
