/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.dispatching.teditor.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.lsat.dispatching.teditor.DispatchingRuntimeModule;
import org.eclipse.lsat.dispatching.teditor.DispatchingStandaloneSetup;
import org.eclipse.lsat.dispatching.teditor.ide.DispatchingIdeModule;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
@SuppressWarnings("all")
public class DispatchingIdeSetup extends DispatchingStandaloneSetup {
  @Override
  public Injector createInjector() {
    DispatchingRuntimeModule _dispatchingRuntimeModule = new DispatchingRuntimeModule();
    DispatchingIdeModule _dispatchingIdeModule = new DispatchingIdeModule();
    return Guice.createInjector(Modules2.mixin(_dispatchingRuntimeModule, _dispatchingIdeModule));
  }
}
