/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.conformance;

import org.eclipse.lsat.common.qvto.util.AbstractModelTransformer;
import org.eclipse.lsat.common.qvto.util.QvtoTransformationException;
import org.eclipse.lsat.conformance.ConformanceCheckInput.Level;
import org.eclipse.lsat.petri_net.PetriNet;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;

/**
 * @deprecated use {@link ConformanceCheckJava}
 */
public class ConformanceCheck extends AbstractModelTransformer<ConformanceCheckInput, PetriNet> {
    public ConformanceCheck(Level aLevel) {
        setConfigProperty("CONFORMANCE_LEVEL", aLevel.ordinal());
    }

    @Override
    protected String getDefaultTransformation() {
        return "/transforms/conformanceCheck.qvto";
    }

    @Override
    protected PetriNet doTransformModel(ConformanceCheckInput input) throws QvtoTransformationException {
        // Input/Output
        BasicModelExtent inoutPetriNet = new BasicModelExtent();
        inoutPetriNet.add(input.getPetriNet());

        // Input
        BasicModelExtent inTraces = new BasicModelExtent();
        inTraces.setContents(input.getTraces());

        execute(inoutPetriNet, inTraces);

        return validateOneAndOnlyOne(PetriNet.class, inoutPetriNet);
    }
}
