/**
 */
package dispatching;

import machine.IResource;
import machine.ImportContainer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Dispatching</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dispatching.ActivityDispatching#getDispatchGroups <em>Dispatch Groups</em>}</li>
 *   <li>{@link dispatching.ActivityDispatching#getNumberOfIterations <em>Number Of Iterations</em>}</li>
 *   <li>{@link dispatching.ActivityDispatching#getResourceIterations <em>Resource Iterations</em>}</li>
 * </ul>
 *
 * @see dispatching.DispatchingPackage#getActivityDispatching()
 * @model
 * @generated
 */
public interface ActivityDispatching extends ImportContainer {
	/**
	 * Returns the value of the '<em><b>Dispatch Groups</b></em>' containment reference list.
	 * The list contents are of type {@link dispatching.DispatchGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatch Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatch Groups</em>' containment reference list.
	 * @see dispatching.DispatchingPackage#getActivityDispatching_DispatchGroups()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<DispatchGroup> getDispatchGroups();

	/**
	 * Returns the value of the '<em><b>Number Of Iterations</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Iterations</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Iterations</em>' attribute.
	 * @see #setNumberOfIterations(int)
	 * @see dispatching.DispatchingPackage#getActivityDispatching_NumberOfIterations()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getNumberOfIterations();

	/**
	 * Sets the value of the '{@link dispatching.ActivityDispatching#getNumberOfIterations <em>Number Of Iterations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Iterations</em>' attribute.
	 * @see #getNumberOfIterations()
	 * @generated
	 */
	void setNumberOfIterations(int value);

	/**
	 * Returns the value of the '<em><b>Resource Iterations</b></em>' map.
	 * The key is of type {@link machine.IResource},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Iterations</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Iterations</em>' map.
	 * @see dispatching.DispatchingPackage#getActivityDispatching_ResourceIterations()
	 * @model mapType="dispatching.ResourceIterationsMapEntry&lt;machine.IResource, org.eclipse.emf.ecore.EIntegerObject&gt;"
	 * @generated
	 */
	EMap<IResource, Integer> getResourceIterations();

} // ActivityDispatching
