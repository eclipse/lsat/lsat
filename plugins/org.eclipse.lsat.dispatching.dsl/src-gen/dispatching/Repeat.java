/**
 */
package dispatching;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dispatching.Repeat#getStart <em>Start</em>}</li>
 *   <li>{@link dispatching.Repeat#getCount <em>Count</em>}</li>
 *   <li>{@link dispatching.Repeat#getEnd <em>End</em>}</li>
 *   <li>{@link dispatching.Repeat#getNumRepeats <em>Num Repeats</em>}</li>
 * </ul>
 *
 * @see dispatching.DispatchingPackage#getRepeat()
 * @model
 * @generated
 */
public interface Repeat extends EObject {
	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(int)
	 * @see dispatching.DispatchingPackage#getRepeat_Start()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getStart();

	/**
	 * Sets the value of the '{@link dispatching.Repeat#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(int value);

	/**
	 * Returns the value of the '<em><b>Count</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' attribute.
	 * @see dispatching.DispatchingPackage#getRepeat_Count()
	 * @model default="1" required="true" transient="true" changeable="false" volatile="true"
	 * @generated
	 */
	int getCount();

	/**
	 * Returns the value of the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' attribute.
	 * @see #setEnd(int)
	 * @see dispatching.DispatchingPackage#getRepeat_End()
	 * @model
	 * @generated
	 */
	int getEnd();

	/**
	 * Sets the value of the '{@link dispatching.Repeat#getEnd <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' attribute.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(int value);

	/**
	 * Returns the value of the '<em><b>Num Repeats</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Repeats</em>' attribute.
	 * @see #setNumRepeats(int)
	 * @see dispatching.DispatchingPackage#getRepeat_NumRepeats()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getNumRepeats();

	/**
	 * Sets the value of the '{@link dispatching.Repeat#getNumRepeats <em>Num Repeats</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Repeats</em>' attribute.
	 * @see #getNumRepeats()
	 * @generated
	 */
	void setNumRepeats(int value);

} // Range
