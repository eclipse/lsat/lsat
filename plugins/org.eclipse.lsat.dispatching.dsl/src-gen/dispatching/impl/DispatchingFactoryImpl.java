/**
 */
package dispatching.impl;

import dispatching.*;

import java.util.Map;

import machine.IResource;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DispatchingFactoryImpl extends EFactoryImpl implements DispatchingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DispatchingFactory init() {
		try {
			DispatchingFactory theDispatchingFactory = (DispatchingFactory)EPackage.Registry.INSTANCE.getEFactory(DispatchingPackage.eNS_URI);
			if (theDispatchingFactory != null) {
				return theDispatchingFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DispatchingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DispatchingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DispatchingPackage.ACTIVITY_DISPATCHING: return createActivityDispatching();
			case DispatchingPackage.DISPATCH: return createDispatch();
			case DispatchingPackage.DISPATCH_GROUP: return createDispatchGroup();
			case DispatchingPackage.RESOURCE_ITERATIONS_MAP_ENTRY: return (EObject)createResourceIterationsMapEntry();
			case DispatchingPackage.ATTRIBUTES_MAP_ENTRY: return (EObject)createAttributesMapEntry();
			case DispatchingPackage.ATTRIBUTE: return createAttribute();
			case DispatchingPackage.REPEAT: return createRepeat();
			case DispatchingPackage.RESOURCE_YIELD_MAP_ENTRY: return (EObject)createResourceYieldMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActivityDispatching createActivityDispatching() {
		ActivityDispatchingImpl activityDispatching = new ActivityDispatchingImpl();
		return activityDispatching;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Dispatch createDispatch() {
		DispatchImpl dispatch = new DispatchImpl();
		return dispatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DispatchGroup createDispatchGroup() {
		DispatchGroupImpl dispatchGroup = new DispatchGroupImpl();
		return dispatchGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<IResource, Integer> createResourceIterationsMapEntry() {
		ResourceIterationsMapEntryImpl resourceIterationsMapEntry = new ResourceIterationsMapEntryImpl();
		return resourceIterationsMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<Attribute, String> createAttributesMapEntry() {
		AttributesMapEntryImpl attributesMapEntry = new AttributesMapEntryImpl();
		return attributesMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Attribute createAttribute() {
		AttributeImpl attribute = new AttributeImpl();
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Repeat createRepeat() {
		RepeatImpl repeat = new RepeatImpl();
		return repeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<IResource, Integer> createResourceYieldMapEntry() {
		ResourceYieldMapEntryImpl resourceYieldMapEntry = new ResourceYieldMapEntryImpl();
		return resourceYieldMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DispatchingPackage getDispatchingPackage() {
		return (DispatchingPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DispatchingPackage getPackage() {
		return DispatchingPackage.eINSTANCE;
	}

} //DispatchingFactoryImpl
