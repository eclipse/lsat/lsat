/**
 */
package dispatching.impl;

import activity.ActivityPackage;

import dispatching.ActivityDispatching;
import dispatching.Attribute;
import dispatching.Dispatch;
import dispatching.DispatchGroup;
import dispatching.DispatchingFactory;
import dispatching.DispatchingPackage;

import dispatching.HasUserAttributes;
import dispatching.Repeat;
import java.util.Map;

import machine.MachinePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.lsat.common.graph.directed.editable.EdgPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DispatchingPackageImpl extends EPackageImpl implements DispatchingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityDispatchingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dispatchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dispatchGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceIterationsMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributesMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hasUserAttributesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass repeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceYieldMapEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dispatching.DispatchingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DispatchingPackageImpl() {
		super(eNS_URI, DispatchingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DispatchingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DispatchingPackage init() {
		if (isInited) return (DispatchingPackage)EPackage.Registry.INSTANCE.getEPackage(DispatchingPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDispatchingPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DispatchingPackageImpl theDispatchingPackage = registeredDispatchingPackage instanceof DispatchingPackageImpl ? (DispatchingPackageImpl)registeredDispatchingPackage : new DispatchingPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		ActivityPackage.eINSTANCE.eClass();
		MachinePackage.eINSTANCE.eClass();
		EdgPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDispatchingPackage.createPackageContents();

		// Initialize created meta-data
		theDispatchingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDispatchingPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DispatchingPackage.eNS_URI, theDispatchingPackage);
		return theDispatchingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getActivityDispatching() {
		return activityDispatchingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getActivityDispatching_DispatchGroups() {
		return (EReference)activityDispatchingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getActivityDispatching_NumberOfIterations() {
		return (EAttribute)activityDispatchingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getActivityDispatching_ResourceIterations() {
		return (EReference)activityDispatchingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDispatch() {
		return dispatchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDispatch_Activity() {
		return (EReference)dispatchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDispatch_Description() {
		return (EAttribute)dispatchEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDispatch_ResourceItems() {
		return (EReference)dispatchEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDispatchGroup() {
		return dispatchGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDispatchGroup_Dispatches() {
		return (EReference)dispatchGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDispatchGroup_Offset() {
		return (EAttribute)dispatchGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDispatchGroup_Name() {
		return (EAttribute)dispatchGroupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDispatchGroup_IteratorName() {
		return (EAttribute)dispatchGroupEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDispatchGroup_Repeats() {
		return (EReference)dispatchGroupEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDispatchGroup_Yield() {
		return (EAttribute)dispatchGroupEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDispatchGroup_ResourceYield() {
		return (EReference)dispatchGroupEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getResourceIterationsMapEntry() {
		return resourceIterationsMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResourceIterationsMapEntry_Key() {
		return (EReference)resourceIterationsMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getResourceIterationsMapEntry_Value() {
		return (EAttribute)resourceIterationsMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAttributesMapEntry() {
		return attributesMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAttributesMapEntry_Key() {
		return (EReference)attributesMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAttributesMapEntry_Value() {
		return (EAttribute)attributesMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAttribute() {
		return attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAttribute_Name() {
		return (EAttribute)attributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHasUserAttributes() {
		return hasUserAttributesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHasUserAttributes_UserAttributes() {
		return (EReference)hasUserAttributesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRepeat() {
		return repeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepeat_Start() {
		return (EAttribute)repeatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepeat_Count() {
		return (EAttribute)repeatEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepeat_End() {
		return (EAttribute)repeatEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRepeat_NumRepeats() {
		return (EAttribute)repeatEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getResourceYieldMapEntry() {
		return resourceYieldMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResourceYieldMapEntry_Key() {
		return (EReference)resourceYieldMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getResourceYieldMapEntry_Value() {
		return (EAttribute)resourceYieldMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DispatchingFactory getDispatchingFactory() {
		return (DispatchingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		activityDispatchingEClass = createEClass(ACTIVITY_DISPATCHING);
		createEReference(activityDispatchingEClass, ACTIVITY_DISPATCHING__DISPATCH_GROUPS);
		createEAttribute(activityDispatchingEClass, ACTIVITY_DISPATCHING__NUMBER_OF_ITERATIONS);
		createEReference(activityDispatchingEClass, ACTIVITY_DISPATCHING__RESOURCE_ITERATIONS);

		dispatchEClass = createEClass(DISPATCH);
		createEReference(dispatchEClass, DISPATCH__ACTIVITY);
		createEAttribute(dispatchEClass, DISPATCH__DESCRIPTION);
		createEReference(dispatchEClass, DISPATCH__RESOURCE_ITEMS);

		dispatchGroupEClass = createEClass(DISPATCH_GROUP);
		createEReference(dispatchGroupEClass, DISPATCH_GROUP__DISPATCHES);
		createEAttribute(dispatchGroupEClass, DISPATCH_GROUP__OFFSET);
		createEAttribute(dispatchGroupEClass, DISPATCH_GROUP__NAME);
		createEAttribute(dispatchGroupEClass, DISPATCH_GROUP__ITERATOR_NAME);
		createEReference(dispatchGroupEClass, DISPATCH_GROUP__REPEATS);
		createEAttribute(dispatchGroupEClass, DISPATCH_GROUP__YIELD);
		createEReference(dispatchGroupEClass, DISPATCH_GROUP__RESOURCE_YIELD);

		resourceIterationsMapEntryEClass = createEClass(RESOURCE_ITERATIONS_MAP_ENTRY);
		createEReference(resourceIterationsMapEntryEClass, RESOURCE_ITERATIONS_MAP_ENTRY__KEY);
		createEAttribute(resourceIterationsMapEntryEClass, RESOURCE_ITERATIONS_MAP_ENTRY__VALUE);

		attributesMapEntryEClass = createEClass(ATTRIBUTES_MAP_ENTRY);
		createEReference(attributesMapEntryEClass, ATTRIBUTES_MAP_ENTRY__KEY);
		createEAttribute(attributesMapEntryEClass, ATTRIBUTES_MAP_ENTRY__VALUE);

		attributeEClass = createEClass(ATTRIBUTE);
		createEAttribute(attributeEClass, ATTRIBUTE__NAME);

		hasUserAttributesEClass = createEClass(HAS_USER_ATTRIBUTES);
		createEReference(hasUserAttributesEClass, HAS_USER_ATTRIBUTES__USER_ATTRIBUTES);

		repeatEClass = createEClass(REPEAT);
		createEAttribute(repeatEClass, REPEAT__START);
		createEAttribute(repeatEClass, REPEAT__COUNT);
		createEAttribute(repeatEClass, REPEAT__END);
		createEAttribute(repeatEClass, REPEAT__NUM_REPEATS);

		resourceYieldMapEntryEClass = createEClass(RESOURCE_YIELD_MAP_ENTRY);
		createEReference(resourceYieldMapEntryEClass, RESOURCE_YIELD_MAP_ENTRY__KEY);
		createEAttribute(resourceYieldMapEntryEClass, RESOURCE_YIELD_MAP_ENTRY__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MachinePackage theMachinePackage = (MachinePackage)EPackage.Registry.INSTANCE.getEPackage(MachinePackage.eNS_URI);
		ActivityPackage theActivityPackage = (ActivityPackage)EPackage.Registry.INSTANCE.getEPackage(ActivityPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		activityDispatchingEClass.getESuperTypes().add(theMachinePackage.getImportContainer());
		dispatchEClass.getESuperTypes().add(this.getHasUserAttributes());
		dispatchGroupEClass.getESuperTypes().add(this.getHasUserAttributes());

		// Initialize classes, features, and operations; add parameters
		initEClass(activityDispatchingEClass, ActivityDispatching.class, "ActivityDispatching", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivityDispatching_DispatchGroups(), this.getDispatchGroup(), null, "dispatchGroups", null, 1, -1, ActivityDispatching.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivityDispatching_NumberOfIterations(), ecorePackage.getEInt(), "numberOfIterations", "1", 1, 1, ActivityDispatching.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getActivityDispatching_ResourceIterations(), this.getResourceIterationsMapEntry(), null, "resourceIterations", null, 0, -1, ActivityDispatching.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dispatchEClass, Dispatch.class, "Dispatch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDispatch_Activity(), theActivityPackage.getActivity(), null, "activity", null, 1, 1, Dispatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDispatch_Description(), ecorePackage.getEString(), "description", null, 0, 1, Dispatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDispatch_ResourceItems(), theMachinePackage.getResourceItem(), null, "resourceItems", null, 0, -1, Dispatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dispatchGroupEClass, DispatchGroup.class, "DispatchGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDispatchGroup_Dispatches(), this.getDispatch(), null, "dispatches", null, 1, -1, DispatchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDispatchGroup_Offset(), ecorePackage.getEBigDecimal(), "offset", "0.0", 1, 1, DispatchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDispatchGroup_Name(), ecorePackage.getEString(), "name", "default", 1, 1, DispatchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDispatchGroup_IteratorName(), ecorePackage.getEString(), "iteratorName", "iteration", 1, 1, DispatchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDispatchGroup_Repeats(), this.getRepeat(), null, "repeats", null, 0, -1, DispatchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDispatchGroup_Yield(), ecorePackage.getEInt(), "yield", "1", 1, 1, DispatchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDispatchGroup_ResourceYield(), this.getResourceYieldMapEntry(), null, "resourceYield", null, 0, -1, DispatchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceIterationsMapEntryEClass, Map.Entry.class, "ResourceIterationsMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceIterationsMapEntry_Key(), theMachinePackage.getIResource(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourceIterationsMapEntry_Value(), ecorePackage.getEIntegerObject(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributesMapEntryEClass, Map.Entry.class, "AttributesMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributesMapEntry_Key(), this.getAttribute(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttributesMapEntry_Value(), ecorePackage.getEString(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeEClass, Attribute.class, "Attribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttribute_Name(), ecorePackage.getEString(), "name", null, 1, 1, Attribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hasUserAttributesEClass, HasUserAttributes.class, "HasUserAttributes", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHasUserAttributes_UserAttributes(), this.getAttributesMapEntry(), null, "userAttributes", null, 0, -1, HasUserAttributes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(repeatEClass, Repeat.class, "Repeat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRepeat_Start(), ecorePackage.getEInt(), "start", "1", 1, 1, Repeat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepeat_Count(), ecorePackage.getEInt(), "count", "1", 1, 1, Repeat.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepeat_End(), ecorePackage.getEInt(), "end", null, 0, 1, Repeat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepeat_NumRepeats(), ecorePackage.getEInt(), "numRepeats", "1", 1, 1, Repeat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceYieldMapEntryEClass, Map.Entry.class, "ResourceYieldMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResourceYieldMapEntry_Key(), theMachinePackage.getIResource(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResourceYieldMapEntry_Value(), ecorePackage.getEIntegerObject(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DispatchingPackageImpl
