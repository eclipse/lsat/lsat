/**
 */
package dispatching;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has User Attributes</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dispatching.HasUserAttributes#getUserAttributes <em>User Attributes</em>}</li>
 * </ul>
 *
 * @see dispatching.DispatchingPackage#getHasUserAttributes()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface HasUserAttributes extends EObject {
	/**
	 * Returns the value of the '<em><b>User Attributes</b></em>' map.
	 * The key is of type {@link dispatching.Attribute},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Attributes</em>' map.
	 * @see dispatching.DispatchingPackage#getHasUserAttributes_UserAttributes()
	 * @model mapType="dispatching.AttributesMapEntry&lt;dispatching.Attribute, org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	EMap<Attribute, String> getUserAttributes();

} // HasUserAttributes
