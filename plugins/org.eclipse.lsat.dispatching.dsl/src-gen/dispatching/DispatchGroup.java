/**
 */
package dispatching;

import java.math.BigDecimal;
import machine.IResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dispatch Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dispatching.DispatchGroup#getDispatches <em>Dispatches</em>}</li>
 *   <li>{@link dispatching.DispatchGroup#getOffset <em>Offset</em>}</li>
 *   <li>{@link dispatching.DispatchGroup#getName <em>Name</em>}</li>
 *   <li>{@link dispatching.DispatchGroup#getIteratorName <em>Iterator Name</em>}</li>
 *   <li>{@link dispatching.DispatchGroup#getRepeats <em>Repeats</em>}</li>
 *   <li>{@link dispatching.DispatchGroup#getYield <em>Yield</em>}</li>
 *   <li>{@link dispatching.DispatchGroup#getResourceYield <em>Resource Yield</em>}</li>
 * </ul>
 *
 * @see dispatching.DispatchingPackage#getDispatchGroup()
 * @model
 * @generated
 */
public interface DispatchGroup extends HasUserAttributes {
	/**
	 * Returns the value of the '<em><b>Dispatches</b></em>' containment reference list.
	 * The list contents are of type {@link dispatching.Dispatch}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatches</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatches</em>' containment reference list.
	 * @see dispatching.DispatchingPackage#getDispatchGroup_Dispatches()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Dispatch> getDispatches();

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(BigDecimal)
	 * @see dispatching.DispatchingPackage#getDispatchGroup_Offset()
	 * @model default="0.0" required="true"
	 * @generated
	 */
	BigDecimal getOffset();

	/**
	 * Sets the value of the '{@link dispatching.DispatchGroup#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(BigDecimal value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>"default"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dispatching.DispatchingPackage#getDispatchGroup_Name()
	 * @model default="default" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dispatching.DispatchGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Iterator Name</b></em>' attribute.
	 * The default value is <code>"iteration"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator Name</em>' attribute.
	 * @see #setIteratorName(String)
	 * @see dispatching.DispatchingPackage#getDispatchGroup_IteratorName()
	 * @model default="iteration" required="true"
	 * @generated
	 */
	String getIteratorName();

	/**
	 * Sets the value of the '{@link dispatching.DispatchGroup#getIteratorName <em>Iterator Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator Name</em>' attribute.
	 * @see #getIteratorName()
	 * @generated
	 */
	void setIteratorName(String value);

	/**
	 * Returns the value of the '<em><b>Repeats</b></em>' containment reference list.
	 * The list contents are of type {@link dispatching.Repeat}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repeats</em>' containment reference list.
	 * @see dispatching.DispatchingPackage#getDispatchGroup_Repeats()
	 * @model containment="true"
	 * @generated
	 */
	EList<Repeat> getRepeats();

	/**
	 * Returns the value of the '<em><b>Yield</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Yield</em>' attribute.
	 * @see #setYield(int)
	 * @see dispatching.DispatchingPackage#getDispatchGroup_Yield()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getYield();

	/**
	 * Sets the value of the '{@link dispatching.DispatchGroup#getYield <em>Yield</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Yield</em>' attribute.
	 * @see #getYield()
	 * @generated
	 */
	void setYield(int value);

	/**
	 * Returns the value of the '<em><b>Resource Yield</b></em>' map.
	 * The key is of type {@link machine.IResource},
	 * and the value is of type {@link java.lang.Integer},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Yield</em>' map.
	 * @see dispatching.DispatchingPackage#getDispatchGroup_ResourceYield()
	 * @model mapType="dispatching.ResourceYieldMapEntry&lt;machine.IResource, org.eclipse.emf.ecore.EIntegerObject&gt;"
	 * @generated
	 */
	EMap<IResource, Integer> getResourceYield();

} // DispatchGroup
