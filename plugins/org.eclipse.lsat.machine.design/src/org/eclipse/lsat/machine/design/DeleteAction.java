/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.design;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.sirius.business.api.action.AbstractExternalJavaAction;

import machine.FullMeshPath;
import machine.Path;
import machine.PathTargetReference;
import machine.SymbolicPosition;
import machine.impl.MachineQueries;

public class DeleteAction extends AbstractExternalJavaAction {
    @Override
    public boolean canExecute(Collection<? extends EObject> selections) {
        return null != selections && selections.size() == 1;
    }

    @Override
    public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
        for (EObject selection: selections) {
            if (selection instanceof SymbolicPosition) {
                delete((SymbolicPosition)selection);
            } else if (selection instanceof Path) {
                delete((Path)selection);
            }
        }
    }

    private static void delete(SymbolicPosition symPos) {
        for (Path edge: MachineQueries.findTargetReference(symPos)) {
            delete(edge);
        }
        for (Path edge: symPos.getOutgoingPaths()) {
            if (edge instanceof FullMeshPath) {
                deleteMeshTarget(symPos, edge);
            } else {
                delete(edge);
            }
        }
        EcoreUtil.delete(symPos);
    }

    private static void delete(Path edge) {
        for (PathTargetReference pathTargetRef: edge.getTargets()) {
            EcoreUtil.delete(pathTargetRef);
        }
        EcoreUtil.delete(edge);
    }

    private static void deleteMeshTarget(SymbolicPosition symbPos, Path edge) {
        List<PathTargetReference> toRemove = new ArrayList<>();
        for (PathTargetReference pathTargetRef: edge.getTargets()) {
            if (symbPos.getTargetReferences().contains(pathTargetRef)) {
                toRemove.add(pathTargetRef);
            }
        }
        for (PathTargetReference pathTargetRef: toRemove) {
            EcoreUtil.delete(pathTargetRef);
        }
    }
}
