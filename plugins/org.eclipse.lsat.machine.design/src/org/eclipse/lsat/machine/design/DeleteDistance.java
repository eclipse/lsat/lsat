/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.design;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.business.api.action.AbstractExternalJavaAction;
import org.eclipse.xtext.EcoreUtil2;

import machine.Distance;
import machine.Peripheral;

public class DeleteDistance extends AbstractExternalJavaAction {
    @Override
    public boolean canExecute(Collection<? extends EObject> selections) {
        return null != selections && selections.size() == 1;
    }

    @Override
    public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
        // Deleting Distance
        for (EObject selection: selections) {
            if (selection instanceof Distance) {
                delete((Distance)selection);
            }
        }
    }

    private static void delete(Distance distance) {
        Peripheral peripheral = EcoreUtil2.getContainerOfType(distance, Peripheral.class);
        if (peripheral != null) {
            peripheral.getDistances().remove(distance);
        }
    }
}
