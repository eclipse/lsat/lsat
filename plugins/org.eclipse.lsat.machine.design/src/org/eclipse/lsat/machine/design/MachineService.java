/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.machine.design;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.ECollections;

import machine.Path;
import machine.Profile;

public class MachineService {
    public String getProfileNames(Path path) {
        return from(path.getProfiles()).collectOne(Profile::getName).joinfields(", ", "", "");
    }

    public Path updateProfilesBasedOnDirectEdit(Path path, String profilesString) {
        Map<String, Profile> profilesAvailableFromTheModel =
                from(path.getPeripheral().getProfiles()).toMap(p -> p.getName().trim());
        List<Profile> profilesSpecifiedinTheDiagram =
                from(profilesString.trim().split("\\s*,\\s*")).xcollectOne(profilesAvailableFromTheModel::get).asList();
        ECollections.setEList(path.getProfiles(), profilesSpecifiedinTheDiagram);
        return path;
    }
}
