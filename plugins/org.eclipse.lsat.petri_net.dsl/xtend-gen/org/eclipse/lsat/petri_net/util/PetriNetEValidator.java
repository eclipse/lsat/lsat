/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.petri_net.util;

import activity.Activator;
import com.google.common.base.Objects;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.petri_net.PetriNet;
import org.eclipse.lsat.petri_net.PetriNetPackage;
import org.eclipse.lsat.petri_net.Place;
import org.eclipse.lsat.petri_net.Transition;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class PetriNetEValidator implements EValidator {
  public static final PetriNetEValidator INSTANCE = new PetriNetEValidator();
  
  @Override
  public boolean validate(final EDataType eDataType, final Object value, final DiagnosticChain diagnostics, final Map<Object, Object> context) {
    return true;
  }
  
  public boolean validatePetriNet(final PetriNet petriNet, final DiagnosticChain diagnostics) {
    boolean result = true;
    final Function1<DirectedGraph<?, ?>, Iterable<? extends DirectedGraph<?, ?>>> _function = (DirectedGraph<?, ?> it) -> {
      return it.getSubGraphs();
    };
    final Function1<DirectedGraph<?, ?>, EList<? extends Node>> _function_1 = (DirectedGraph<?, ?> it) -> {
      return it.getNodes();
    };
    final Function1<Node, Boolean> _function_2 = (Node it) -> {
      return Boolean.valueOf(it.getIncomingEdges().isEmpty());
    };
    final Set<Node> calculatedInitial = IterableExtensions.<Node>toSet(Queries.<Node>select(Queries.<DirectedGraph<?, ?>, Node>xcollect(Queries.<DirectedGraph<?, ?>>closure(Collections.<DirectedGraph<?, ?>>singleton(((DirectedGraph<?, ?>) petriNet)), true, _function), _function_1), _function_2));
    Set<Place> _set = IterableExtensions.<Place>toSet(petriNet.getInitialPlaces());
    boolean _notEquals = (!Objects.equal(calculatedInitial, _set));
    if (_notEquals) {
      return this.validationFailed(Diagnostic.ERROR, "Initial nodes are not aligned with reality", diagnostics, petriNet);
    }
    return result;
  }
  
  public boolean validateTransitionIncoming(final Transition transition, final DiagnosticChain diagnostics) {
    boolean result = true;
    final Function1<Edge, Boolean> _function = (Edge it) -> {
      Node _sourceNode = it.getSourceNode();
      return Boolean.valueOf((_sourceNode instanceof Place));
    };
    final boolean conditionIsMet = IterableExtensions.<Edge>forall(transition.getIncomingEdges(), _function);
    if ((!conditionIsMet)) {
      return this.validationFailed(Diagnostic.ERROR, "Transition should connect places", diagnostics, transition);
    }
    return result;
  }
  
  public boolean validateTransitionOutgoing(final Transition transition, final DiagnosticChain diagnostics) {
    boolean result = true;
    final Function1<Edge, Boolean> _function = (Edge it) -> {
      Node _targetNode = it.getTargetNode();
      return Boolean.valueOf((_targetNode instanceof Place));
    };
    final boolean conditionIsMet = IterableExtensions.<Edge>forall(transition.getOutgoingEdges(), _function);
    if ((!conditionIsMet)) {
      return this.validationFailed(Diagnostic.ERROR, "Transition should connect places", diagnostics, transition);
    }
    return result;
  }
  
  public boolean validationFailed(final int severity, final String message, final DiagnosticChain diagnostics, final EObject... data) {
    BasicDiagnostic _basicDiagnostic = new BasicDiagnostic(severity, Activator.BUNDLE_NAME, 0, message, data);
    diagnostics.add(_basicDiagnostic);
    return false;
  }
  
  @Override
  public boolean validate(final EObject eObject, final DiagnosticChain diagnostics, final Map<Object, Object> context) {
    return this.validate(eObject.eClass(), eObject, diagnostics, context);
  }
  
  @Override
  public boolean validate(final EClass eClass, final EObject eObject, final DiagnosticChain diagnostics, final Map<Object, Object> context) {
    boolean _switchResult = false;
    int _classifierID = eClass.getClassifierID();
    switch (_classifierID) {
      case PetriNetPackage.PETRI_NET:
        _switchResult = this.validatePetriNet(((PetriNet) eObject), diagnostics);
        break;
      case PetriNetPackage.TRANSITION:
        _switchResult = (this.validateTransitionIncoming(((Transition) eObject), diagnostics) && 
          this.validateTransitionOutgoing(((Transition) eObject), diagnostics));
        break;
      case PetriNetPackage.PLACE:
        _switchResult = (this.validateIncomingTransitionsPlace(((Place) eObject), diagnostics) && 
          this.validateOutgoingTransitionsPlace(((Place) eObject), diagnostics));
        break;
      default:
        return true;
    }
    return _switchResult;
  }
  
  public boolean validateOutgoingTransitionsPlace(final Place place, final DiagnosticChain diagnostics) {
    boolean result = true;
    final Function1<Edge, Boolean> _function = (Edge it) -> {
      Node _targetNode = it.getTargetNode();
      return Boolean.valueOf((_targetNode instanceof Transition));
    };
    final boolean conditionIsMet = IterableExtensions.<Edge>forall(place.getOutgoingEdges(), _function);
    if ((!conditionIsMet)) {
      return this.validationFailed(Diagnostic.ERROR, "Place should be encapsulated by transitions", diagnostics, place);
    }
    return result;
  }
  
  public boolean validateIncomingTransitionsPlace(final Place place, final DiagnosticChain diagnostics) {
    boolean result = true;
    final Function1<Edge, Boolean> _function = (Edge it) -> {
      Node _sourceNode = it.getSourceNode();
      return Boolean.valueOf((_sourceNode instanceof Transition));
    };
    final boolean conditionIsMet = IterableExtensions.<Edge>forall(place.getIncomingEdges(), _function);
    if ((!conditionIsMet)) {
      return this.validationFailed(Diagnostic.ERROR, "Place should be encapsulated by transitions", diagnostics, place);
    }
    return result;
  }
}
