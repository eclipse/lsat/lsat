/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.petri_net.util

import activity.Activator
import java.util.Collections
import java.util.Map
import org.eclipse.lsat.common.graph.directed.DirectedGraph
import org.eclipse.lsat.petri_net.PetriNet
import org.eclipse.lsat.petri_net.PetriNetPackage
import org.eclipse.lsat.petri_net.Place
import org.eclipse.lsat.petri_net.Transition
import org.eclipse.emf.common.util.BasicDiagnostic
import org.eclipse.emf.common.util.DiagnosticChain
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EValidator

import static org.eclipse.emf.common.util.Diagnostic.*

import static extension org.eclipse.lsat.common.xtend.Queries.*

class PetriNetEValidator implements EValidator {

    public static final PetriNetEValidator INSTANCE = new PetriNetEValidator();

    override boolean validate(EDataType eDataType, Object value, DiagnosticChain diagnostics,
        Map<Object, Object> context) {
        return true;
    }

    def boolean validatePetriNet(PetriNet petriNet, DiagnosticChain diagnostics) {
        var result = true;
        val calculatedInitial = Collections::singleton(petriNet as DirectedGraph<?, ?>).closure(true, [subGraphs]).
            xcollect[nodes].select[incomingEdges.isEmpty].toSet
        if (calculatedInitial != (petriNet.initialPlaces.toSet)) {
            return validationFailed(ERROR, 'Initial nodes are not aligned with reality', diagnostics, petriNet);
        }
        return result;
    }

    def boolean validateTransitionIncoming(Transition transition, DiagnosticChain diagnostics) {
        var result = true;

        val conditionIsMet = transition.incomingEdges.forall[sourceNode instanceof Place]
        if (!conditionIsMet) {
            return validationFailed(ERROR, 'Transition should connect places', diagnostics, transition);
        }

        return result;
    }

    def boolean validateTransitionOutgoing(Transition transition, DiagnosticChain diagnostics) {
        var result = true;

        val conditionIsMet = transition.outgoingEdges.forall[targetNode instanceof Place]
        if (!conditionIsMet) {
            return validationFailed(ERROR, 'Transition should connect places', diagnostics, transition);
        }

        return result;
    }

    def boolean validationFailed(int severity, String message, DiagnosticChain diagnostics, EObject... data) {
        diagnostics.add(new BasicDiagnostic(severity, Activator.BUNDLE_NAME, 0, message, data));
        return false;
    }

    override validate(EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        validate(eObject.eClass, eObject, diagnostics, context);
    }

    override validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        switch (eClass.getClassifierID()) {
            case PetriNetPackage.PETRI_NET:
                validatePetriNet(eObject as PetriNet, diagnostics)
            case PetriNetPackage.TRANSITION:
                validateTransitionIncoming(eObject as Transition, diagnostics) &&
                    validateTransitionOutgoing(eObject as Transition, diagnostics)
            case PetriNetPackage.PLACE:
                validateIncomingTransitionsPlace(eObject as Place, diagnostics) &&
                    validateOutgoingTransitionsPlace(eObject as Place, diagnostics)
            default:
                return true
        }
    }

    def boolean validateOutgoingTransitionsPlace(Place place, DiagnosticChain diagnostics) {
        var result = true;

        val conditionIsMet = place.outgoingEdges.forall[targetNode instanceof Transition]
        if (!conditionIsMet) {
            return validationFailed(ERROR, 'Place should be encapsulated by transitions', diagnostics, place);
        }
        
        return result;
    }

    def boolean validateIncomingTransitionsPlace(Place place, DiagnosticChain diagnostics) {
        var result = true;

        val conditionIsMet = place.incomingEdges.forall[sourceNode instanceof Transition]
        if (!conditionIsMet) {
            return validationFailed(ERROR, 'Place should be encapsulated by transitions', diagnostics, place);
        }
        
        return result;
    }

}
