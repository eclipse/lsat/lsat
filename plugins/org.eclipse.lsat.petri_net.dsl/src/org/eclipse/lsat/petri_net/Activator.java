/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.petri_net;

import org.eclipse.lsat.common.emf.validation.EValidatorUtil;
import org.eclipse.lsat.petri_net.util.PetriNetEValidator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    @Override
    public void start(BundleContext context) throws Exception {
        EValidatorUtil.registerValidations(PetriNetPackage.eINSTANCE, PetriNetEValidator.INSTANCE);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
    }
}
