/**
 */
package org.eclipse.lsat.petri_net.impl;

import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl;

import org.eclipse.lsat.petri_net.PetriNet;
import org.eclipse.lsat.petri_net.PetriNetPackage;
import org.eclipse.lsat.petri_net.Place;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Petri Net</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.petri_net.impl.PetriNetImpl#getInitialPlaces <em>Initial Places</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.impl.PetriNetImpl#getFinalPlaces <em>Final Places</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PetriNetImpl extends DirectedGraphImpl<Node, Edge> implements PetriNet {
	/**
	 * The cached value of the '{@link #getInitialPlaces() <em>Initial Places</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialPlaces()
	 * @generated
	 * @ordered
	 */
	protected EList<Place> initialPlaces;

	/**
	 * The cached value of the '{@link #getFinalPlaces() <em>Final Places</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalPlaces()
	 * @generated
	 * @ordered
	 */
	protected EList<Place> finalPlaces;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PetriNetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PetriNetPackage.Literals.PETRI_NET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Place> getInitialPlaces() {
		if (initialPlaces == null) {
			initialPlaces = new EObjectWithInverseResolvingEList<Place>(Place.class, this, PetriNetPackage.PETRI_NET__INITIAL_PLACES, PetriNetPackage.PLACE__INITIAL);
		}
		return initialPlaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Place> getFinalPlaces() {
		if (finalPlaces == null) {
			finalPlaces = new EObjectWithInverseResolvingEList<Place>(Place.class, this, PetriNetPackage.PETRI_NET__FINAL_PLACES, PetriNetPackage.PLACE__FINAL);
		}
		return finalPlaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.PETRI_NET__INITIAL_PLACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInitialPlaces()).basicAdd(otherEnd, msgs);
			case PetriNetPackage.PETRI_NET__FINAL_PLACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFinalPlaces()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PetriNetPackage.PETRI_NET__INITIAL_PLACES:
				return ((InternalEList<?>)getInitialPlaces()).basicRemove(otherEnd, msgs);
			case PetriNetPackage.PETRI_NET__FINAL_PLACES:
				return ((InternalEList<?>)getFinalPlaces()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PetriNetPackage.PETRI_NET__INITIAL_PLACES:
				return getInitialPlaces();
			case PetriNetPackage.PETRI_NET__FINAL_PLACES:
				return getFinalPlaces();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PetriNetPackage.PETRI_NET__INITIAL_PLACES:
				getInitialPlaces().clear();
				getInitialPlaces().addAll((Collection<? extends Place>)newValue);
				return;
			case PetriNetPackage.PETRI_NET__FINAL_PLACES:
				getFinalPlaces().clear();
				getFinalPlaces().addAll((Collection<? extends Place>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PetriNetPackage.PETRI_NET__INITIAL_PLACES:
				getInitialPlaces().clear();
				return;
			case PetriNetPackage.PETRI_NET__FINAL_PLACES:
				getFinalPlaces().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PetriNetPackage.PETRI_NET__INITIAL_PLACES:
				return initialPlaces != null && !initialPlaces.isEmpty();
			case PetriNetPackage.PETRI_NET__FINAL_PLACES:
				return finalPlaces != null && !finalPlaces.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PetriNetImpl
