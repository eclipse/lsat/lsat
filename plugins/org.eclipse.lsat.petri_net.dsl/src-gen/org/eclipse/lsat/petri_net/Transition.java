/**
 */
package org.eclipse.lsat.petri_net;

import activity.SyncBar;
import activity.TracePoint;

import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.trace.TraceLine;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.petri_net.Transition#getTracePoint <em>Trace Point</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.Transition#getTraceLines <em>Trace Lines</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.Transition#getSyncBar <em>Sync Bar</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends Node {
	/**
	 * Returns the value of the '<em><b>Trace Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trace Point</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trace Point</em>' reference.
	 * @see #setTracePoint(TracePoint)
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getTransition_TracePoint()
	 * @model
	 * @generated
	 */
	TracePoint getTracePoint();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.petri_net.Transition#getTracePoint <em>Trace Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trace Point</em>' reference.
	 * @see #getTracePoint()
	 * @generated
	 */
	void setTracePoint(TracePoint value);

	/**
	 * Returns the value of the '<em><b>Trace Lines</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.lsat.trace.TraceLine}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trace Lines</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trace Lines</em>' reference list.
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getTransition_TraceLines()
	 * @model
	 * @generated
	 */
	EList<TraceLine> getTraceLines();

	/**
	 * Returns the value of the '<em><b>Sync Bar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sync Bar</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Bar</em>' reference.
	 * @see #setSyncBar(SyncBar)
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getTransition_SyncBar()
	 * @model
	 * @generated
	 */
	SyncBar getSyncBar();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.petri_net.Transition#getSyncBar <em>Sync Bar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Bar</em>' reference.
	 * @see #getSyncBar()
	 * @generated
	 */
	void setSyncBar(SyncBar value);

} // Transition
