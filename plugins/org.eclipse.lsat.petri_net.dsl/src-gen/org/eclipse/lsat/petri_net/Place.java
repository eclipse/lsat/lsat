/**
 */
package org.eclipse.lsat.petri_net;

import activity.Action;

import org.eclipse.lsat.common.graph.directed.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.petri_net.Place#getAction <em>Action</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.Place#isToken <em>Token</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.Place#getInitial <em>Initial</em>}</li>
 *   <li>{@link org.eclipse.lsat.petri_net.Place#getFinal <em>Final</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends Node {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' reference.
	 * @see #setAction(Action)
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPlace_Action()
	 * @model
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.petri_net.Place#getAction <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

	/**
	 * Returns the value of the '<em><b>Token</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token</em>' attribute.
	 * @see #setToken(boolean)
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPlace_Token()
	 * @model required="true"
	 * @generated
	 */
	boolean isToken();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.petri_net.Place#isToken <em>Token</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token</em>' attribute.
	 * @see #isToken()
	 * @generated
	 */
	void setToken(boolean value);

	/**
	 * Returns the value of the '<em><b>Initial</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.petri_net.PetriNet#getInitialPlaces <em>Initial Places</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' reference.
	 * @see #setInitial(PetriNet)
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPlace_Initial()
	 * @see org.eclipse.lsat.petri_net.PetriNet#getInitialPlaces
	 * @model opposite="initialPlaces"
	 * @generated
	 */
	PetriNet getInitial();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.petri_net.Place#getInitial <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial</em>' reference.
	 * @see #getInitial()
	 * @generated
	 */
	void setInitial(PetriNet value);

	/**
	 * Returns the value of the '<em><b>Final</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.petri_net.PetriNet#getFinalPlaces <em>Final Places</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final</em>' reference.
	 * @see #setFinal(PetriNet)
	 * @see org.eclipse.lsat.petri_net.PetriNetPackage#getPlace_Final()
	 * @see org.eclipse.lsat.petri_net.PetriNet#getFinalPlaces
	 * @model opposite="finalPlaces"
	 * @generated
	 */
	PetriNet getFinal();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.petri_net.Place#getFinal <em>Final</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final</em>' reference.
	 * @see #getFinal()
	 * @generated
	 */
	void setFinal(PetriNet value);

} // Place
