/**
 */
package org.eclipse.lsat.petri_net;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.petri_net.PetriNetFactory
 * @model kind="package"
 * @generated
 */
public interface PetriNetPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "petri_net";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/lsat/petri_net";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "petri_net";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PetriNetPackage eINSTANCE = org.eclipse.lsat.petri_net.impl.PetriNetPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.petri_net.impl.PetriNetImpl <em>Petri Net</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.petri_net.impl.PetriNetImpl
	 * @see org.eclipse.lsat.petri_net.impl.PetriNetPackageImpl#getPetriNet()
	 * @generated
	 */
	int PETRI_NET = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__NAME = DirectedGraphPackage.DIRECTED_GRAPH__NAME;

	/**
	 * The feature id for the '<em><b>Sub Graphs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__SUB_GRAPHS = DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS;

	/**
	 * The feature id for the '<em><b>Parent Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__PARENT_GRAPH = DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__EDGES = DirectedGraphPackage.DIRECTED_GRAPH__EDGES;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__NODES = DirectedGraphPackage.DIRECTED_GRAPH__NODES;

	/**
	 * The feature id for the '<em><b>Aspects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__ASPECTS = DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS;

	/**
	 * The feature id for the '<em><b>Initial Places</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__INITIAL_PLACES = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Final Places</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET__FINAL_PLACES = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Petri Net</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET_FEATURE_COUNT = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>All Nodes In Topological Order</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET___ALL_NODES_IN_TOPOLOGICAL_ORDER = DirectedGraphPackage.DIRECTED_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER;

	/**
	 * The number of operations of the '<em>Petri Net</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PETRI_NET_OPERATION_COUNT = DirectedGraphPackage.DIRECTED_GRAPH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.petri_net.impl.PlaceImpl <em>Place</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.petri_net.impl.PlaceImpl
	 * @see org.eclipse.lsat.petri_net.impl.PetriNetPackageImpl#getPlace()
	 * @generated
	 */
	int PLACE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__NAME = DirectedGraphPackage.NODE__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__OUTGOING_EDGES = DirectedGraphPackage.NODE__OUTGOING_EDGES;

	/**
	 * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__INCOMING_EDGES = DirectedGraphPackage.NODE__INCOMING_EDGES;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__GRAPH = DirectedGraphPackage.NODE__GRAPH;

	/**
	 * The feature id for the '<em><b>Aspects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__ASPECTS = DirectedGraphPackage.NODE__ASPECTS;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__ACTION = DirectedGraphPackage.NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Token</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__TOKEN = DirectedGraphPackage.NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initial</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__INITIAL = DirectedGraphPackage.NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Final</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE__FINAL = DirectedGraphPackage.NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Place</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE_FEATURE_COUNT = DirectedGraphPackage.NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Place</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLACE_OPERATION_COUNT = DirectedGraphPackage.NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.lsat.petri_net.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.lsat.petri_net.impl.TransitionImpl
	 * @see org.eclipse.lsat.petri_net.impl.PetriNetPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = DirectedGraphPackage.NODE__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__OUTGOING_EDGES = DirectedGraphPackage.NODE__OUTGOING_EDGES;

	/**
	 * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__INCOMING_EDGES = DirectedGraphPackage.NODE__INCOMING_EDGES;

	/**
	 * The feature id for the '<em><b>Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__GRAPH = DirectedGraphPackage.NODE__GRAPH;

	/**
	 * The feature id for the '<em><b>Aspects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ASPECTS = DirectedGraphPackage.NODE__ASPECTS;

	/**
	 * The feature id for the '<em><b>Trace Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRACE_POINT = DirectedGraphPackage.NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Trace Lines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRACE_LINES = DirectedGraphPackage.NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sync Bar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SYNC_BAR = DirectedGraphPackage.NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = DirectedGraphPackage.NODE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = DirectedGraphPackage.NODE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.petri_net.PetriNet <em>Petri Net</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Petri Net</em>'.
	 * @see org.eclipse.lsat.petri_net.PetriNet
	 * @generated
	 */
	EClass getPetriNet();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.petri_net.PetriNet#getInitialPlaces <em>Initial Places</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Initial Places</em>'.
	 * @see org.eclipse.lsat.petri_net.PetriNet#getInitialPlaces()
	 * @see #getPetriNet()
	 * @generated
	 */
	EReference getPetriNet_InitialPlaces();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.petri_net.PetriNet#getFinalPlaces <em>Final Places</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Final Places</em>'.
	 * @see org.eclipse.lsat.petri_net.PetriNet#getFinalPlaces()
	 * @see #getPetriNet()
	 * @generated
	 */
	EReference getPetriNet_FinalPlaces();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.petri_net.Place <em>Place</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Place</em>'.
	 * @see org.eclipse.lsat.petri_net.Place
	 * @generated
	 */
	EClass getPlace();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.petri_net.Place#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see org.eclipse.lsat.petri_net.Place#getAction()
	 * @see #getPlace()
	 * @generated
	 */
	EReference getPlace_Action();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.lsat.petri_net.Place#isToken <em>Token</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Token</em>'.
	 * @see org.eclipse.lsat.petri_net.Place#isToken()
	 * @see #getPlace()
	 * @generated
	 */
	EAttribute getPlace_Token();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.petri_net.Place#getInitial <em>Initial</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial</em>'.
	 * @see org.eclipse.lsat.petri_net.Place#getInitial()
	 * @see #getPlace()
	 * @generated
	 */
	EReference getPlace_Initial();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.petri_net.Place#getFinal <em>Final</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Final</em>'.
	 * @see org.eclipse.lsat.petri_net.Place#getFinal()
	 * @see #getPlace()
	 * @generated
	 */
	EReference getPlace_Final();

	/**
	 * Returns the meta object for class '{@link org.eclipse.lsat.petri_net.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see org.eclipse.lsat.petri_net.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.petri_net.Transition#getTracePoint <em>Trace Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Trace Point</em>'.
	 * @see org.eclipse.lsat.petri_net.Transition#getTracePoint()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_TracePoint();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.lsat.petri_net.Transition#getTraceLines <em>Trace Lines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Trace Lines</em>'.
	 * @see org.eclipse.lsat.petri_net.Transition#getTraceLines()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_TraceLines();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.lsat.petri_net.Transition#getSyncBar <em>Sync Bar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sync Bar</em>'.
	 * @see org.eclipse.lsat.petri_net.Transition#getSyncBar()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_SyncBar();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PetriNetFactory getPetriNetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.petri_net.impl.PetriNetImpl <em>Petri Net</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.petri_net.impl.PetriNetImpl
		 * @see org.eclipse.lsat.petri_net.impl.PetriNetPackageImpl#getPetriNet()
		 * @generated
		 */
		EClass PETRI_NET = eINSTANCE.getPetriNet();

		/**
		 * The meta object literal for the '<em><b>Initial Places</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PETRI_NET__INITIAL_PLACES = eINSTANCE.getPetriNet_InitialPlaces();

		/**
		 * The meta object literal for the '<em><b>Final Places</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PETRI_NET__FINAL_PLACES = eINSTANCE.getPetriNet_FinalPlaces();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.petri_net.impl.PlaceImpl <em>Place</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.petri_net.impl.PlaceImpl
		 * @see org.eclipse.lsat.petri_net.impl.PetriNetPackageImpl#getPlace()
		 * @generated
		 */
		EClass PLACE = eINSTANCE.getPlace();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACE__ACTION = eINSTANCE.getPlace_Action();

		/**
		 * The meta object literal for the '<em><b>Token</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLACE__TOKEN = eINSTANCE.getPlace_Token();

		/**
		 * The meta object literal for the '<em><b>Initial</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACE__INITIAL = eINSTANCE.getPlace_Initial();

		/**
		 * The meta object literal for the '<em><b>Final</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLACE__FINAL = eINSTANCE.getPlace_Final();

		/**
		 * The meta object literal for the '{@link org.eclipse.lsat.petri_net.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.lsat.petri_net.impl.TransitionImpl
		 * @see org.eclipse.lsat.petri_net.impl.PetriNetPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Trace Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TRACE_POINT = eINSTANCE.getTransition_TracePoint();

		/**
		 * The meta object literal for the '<em><b>Trace Lines</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TRACE_LINES = eINSTANCE.getTransition_TraceLines();

		/**
		 * The meta object literal for the '<em><b>Sync Bar</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SYNC_BAR = eINSTANCE.getTransition_SyncBar();

	}

} //PetriNetPackage
