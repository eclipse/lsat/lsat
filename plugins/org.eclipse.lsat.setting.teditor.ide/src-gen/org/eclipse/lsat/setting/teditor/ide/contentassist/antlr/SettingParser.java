/**
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Do not edit: This file is generated by Xtext
 */
package org.eclipse.lsat.setting.teditor.ide.contentassist.antlr;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Map;
import org.eclipse.lsat.setting.teditor.ide.contentassist.antlr.internal.InternalSettingParser;
import org.eclipse.lsat.setting.teditor.services.SettingGrammarAccess;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class SettingParser extends AbstractContentAssistParser {

	@Singleton
	public static final class NameMappings {
		
		private final Map<AbstractElement, String> mappings;
		
		@Inject
		public NameMappings(SettingGrammarAccess grammarAccess) {
			ImmutableMap.Builder<AbstractElement, String> builder = ImmutableMap.builder();
			init(builder, grammarAccess);
			this.mappings = builder.build();
		}
		
		public String getRuleName(AbstractElement element) {
			return mappings.get(element);
		}
		
		private static void init(ImmutableMap.Builder<AbstractElement, String> builder, SettingGrammarAccess grammarAccess) {
			builder.put(grammarAccess.getPhysicalLocationAccess().getAlternatives(), "rule__PhysicalLocation__Alternatives");
			builder.put(grammarAccess.getTimingAccess().getAlternatives(), "rule__Timing__Alternatives");
			builder.put(grammarAccess.getPlusOrMinusAccess().getAlternatives_1_0(), "rule__PlusOrMinus__Alternatives_1_0");
			builder.put(grammarAccess.getMulOrDivAccess().getAlternatives_1_0(), "rule__MulOrDiv__Alternatives_1_0");
			builder.put(grammarAccess.getPrimaryAccess().getAlternatives(), "rule__Primary__Alternatives");
			builder.put(grammarAccess.getAtomicAccess().getAlternatives(), "rule__Atomic__Alternatives");
			builder.put(grammarAccess.getEBigDecimalAccess().getAlternatives_3_0(), "rule__EBigDecimal__Alternatives_3_0");
			builder.put(grammarAccess.getIIDAccess().getAlternatives(), "rule__IID__Alternatives");
			builder.put(grammarAccess.getSettingsAccess().getGroup(), "rule__Settings__Group__0");
			builder.put(grammarAccess.getImportAccess().getGroup(), "rule__Import__Group__0");
			builder.put(grammarAccess.getPhysicalSettingsAccess().getGroup(), "rule__PhysicalSettings__Group__0");
			builder.put(grammarAccess.getPhysicalSettingsAccess().getGroup_5(), "rule__PhysicalSettings__Group_5__0");
			builder.put(grammarAccess.getMotionSettingsMapEntryAccess().getGroup(), "rule__MotionSettingsMapEntry__Group__0");
			builder.put(grammarAccess.getMotionSettingsAccess().getGroup(), "rule__MotionSettings__Group__0");
			builder.put(grammarAccess.getMotionSettingsAccess().getGroup_5(), "rule__MotionSettings__Group_5__0");
			builder.put(grammarAccess.getMotionSettingsAccess().getGroup_6(), "rule__MotionSettings__Group_6__0");
			builder.put(grammarAccess.getProfileSettingsMapEntryAccess().getGroup(), "rule__ProfileSettingsMapEntry__Group__0");
			builder.put(grammarAccess.getLocationSettingsMapEntryAccess().getGroup(), "rule__LocationSettingsMapEntry__Group__0");
			builder.put(grammarAccess.getDistanceSettingsMapEntryAccess().getGroup(), "rule__DistanceSettingsMapEntry__Group__0");
			builder.put(grammarAccess.getTimingSettingsMapEntryAccess().getGroup(), "rule__TimingSettingsMapEntry__Group__0");
			builder.put(grammarAccess.getMotionProfileSettingsAccess().getGroup(), "rule__MotionProfileSettings__Group__0");
			builder.put(grammarAccess.getMotionProfileSettingsAccess().getGroup_3(), "rule__MotionProfileSettings__Group_3__0");
			builder.put(grammarAccess.getMotionProfileSettingsAccess().getGroup_3_1(), "rule__MotionProfileSettings__Group_3_1__0");
			builder.put(grammarAccess.getMotionArgumentsMapEntryAccess().getGroup(), "rule__MotionArgumentsMapEntry__Group__0");
			builder.put(grammarAccess.getPhysicalLocationAccess().getGroup_0(), "rule__PhysicalLocation__Group_0__0");
			builder.put(grammarAccess.getPhysicalLocationAccess().getGroup_1(), "rule__PhysicalLocation__Group_1__0");
			builder.put(grammarAccess.getPhysicalLocationAccess().getGroup_1_1(), "rule__PhysicalLocation__Group_1_1__0");
			builder.put(grammarAccess.getPhysicalLocationAccess().getGroup_1_2(), "rule__PhysicalLocation__Group_1_2__0");
			builder.put(grammarAccess.getArrayAccess().getGroup(), "rule__Array__Group__0");
			builder.put(grammarAccess.getArrayAccess().getGroup_3(), "rule__Array__Group_3__0");
			builder.put(grammarAccess.getTriangularDistributionAccess().getGroup(), "rule__TriangularDistribution__Group__0");
			builder.put(grammarAccess.getTriangularDistributionAccess().getGroup_13(), "rule__TriangularDistribution__Group_13__0");
			builder.put(grammarAccess.getPertDistributionAccess().getGroup(), "rule__PertDistribution__Group__0");
			builder.put(grammarAccess.getPertDistributionAccess().getGroup_17(), "rule__PertDistribution__Group_17__0");
			builder.put(grammarAccess.getNormalDistributionAccess().getGroup(), "rule__NormalDistribution__Group__0");
			builder.put(grammarAccess.getNormalDistributionAccess().getGroup_9(), "rule__NormalDistribution__Group_9__0");
			builder.put(grammarAccess.getEnumeratedDistributionAccess().getGroup(), "rule__EnumeratedDistribution__Group__0");
			builder.put(grammarAccess.getEnumeratedDistributionAccess().getGroup_3(), "rule__EnumeratedDistribution__Group_3__0");
			builder.put(grammarAccess.getEnumeratedDistributionAccess().getGroup_4(), "rule__EnumeratedDistribution__Group_4__0");
			builder.put(grammarAccess.getDeclarationAccess().getGroup(), "rule__Declaration__Group__0");
			builder.put(grammarAccess.getPlusOrMinusAccess().getGroup(), "rule__PlusOrMinus__Group__0");
			builder.put(grammarAccess.getPlusOrMinusAccess().getGroup_1(), "rule__PlusOrMinus__Group_1__0");
			builder.put(grammarAccess.getPlusOrMinusAccess().getGroup_1_0_0(), "rule__PlusOrMinus__Group_1_0_0__0");
			builder.put(grammarAccess.getPlusOrMinusAccess().getGroup_1_0_1(), "rule__PlusOrMinus__Group_1_0_1__0");
			builder.put(grammarAccess.getMulOrDivAccess().getGroup(), "rule__MulOrDiv__Group__0");
			builder.put(grammarAccess.getMulOrDivAccess().getGroup_1(), "rule__MulOrDiv__Group_1__0");
			builder.put(grammarAccess.getMulOrDivAccess().getGroup_1_0_0(), "rule__MulOrDiv__Group_1_0_0__0");
			builder.put(grammarAccess.getMulOrDivAccess().getGroup_1_0_1(), "rule__MulOrDiv__Group_1_0_1__0");
			builder.put(grammarAccess.getPrimaryAccess().getGroup_0(), "rule__Primary__Group_0__0");
			builder.put(grammarAccess.getAtomicAccess().getGroup_0(), "rule__Atomic__Group_0__0");
			builder.put(grammarAccess.getAtomicAccess().getGroup_1(), "rule__Atomic__Group_1__0");
			builder.put(grammarAccess.getEBigDecimalAccess().getGroup(), "rule__EBigDecimal__Group__0");
			builder.put(grammarAccess.getEBigDecimalAccess().getGroup_2(), "rule__EBigDecimal__Group_2__0");
			builder.put(grammarAccess.getEBigDecimalAccess().getGroup_3(), "rule__EBigDecimal__Group_3__0");
			builder.put(grammarAccess.getResourceQualifiedNameAccess().getGroup(), "rule__ResourceQualifiedName__Group__0");
			builder.put(grammarAccess.getResourceQualifiedNameAccess().getGroup_1(), "rule__ResourceQualifiedName__Group_1__0");
			builder.put(grammarAccess.getSettingsAccess().getImportsAssignment_1(), "rule__Settings__ImportsAssignment_1");
			builder.put(grammarAccess.getSettingsAccess().getDeclarationsAssignment_2(), "rule__Settings__DeclarationsAssignment_2");
			builder.put(grammarAccess.getSettingsAccess().getPhysicalSettingsAssignment_3(), "rule__Settings__PhysicalSettingsAssignment_3");
			builder.put(grammarAccess.getImportAccess().getImportURIAssignment_1(), "rule__Import__ImportURIAssignment_1");
			builder.put(grammarAccess.getPhysicalSettingsAccess().getResourceAssignment_1(), "rule__PhysicalSettings__ResourceAssignment_1");
			builder.put(grammarAccess.getPhysicalSettingsAccess().getPeripheralAssignment_3(), "rule__PhysicalSettings__PeripheralAssignment_3");
			builder.put(grammarAccess.getPhysicalSettingsAccess().getTimingSettingsAssignment_5_2(), "rule__PhysicalSettings__TimingSettingsAssignment_5_2");
			builder.put(grammarAccess.getPhysicalSettingsAccess().getMotionSettingsAssignment_6(), "rule__PhysicalSettings__MotionSettingsAssignment_6");
			builder.put(grammarAccess.getMotionSettingsMapEntryAccess().getKeyAssignment_2(), "rule__MotionSettingsMapEntry__KeyAssignment_2");
			builder.put(grammarAccess.getMotionSettingsMapEntryAccess().getValueAssignment_4(), "rule__MotionSettingsMapEntry__ValueAssignment_4");
			builder.put(grammarAccess.getMotionSettingsAccess().getProfileSettingsAssignment_3(), "rule__MotionSettings__ProfileSettingsAssignment_3");
			builder.put(grammarAccess.getMotionSettingsAccess().getLocationSettingsAssignment_5_2(), "rule__MotionSettings__LocationSettingsAssignment_5_2");
			builder.put(grammarAccess.getMotionSettingsAccess().getDistanceSettingsAssignment_6_2(), "rule__MotionSettings__DistanceSettingsAssignment_6_2");
			builder.put(grammarAccess.getProfileSettingsMapEntryAccess().getKeyAssignment_0(), "rule__ProfileSettingsMapEntry__KeyAssignment_0");
			builder.put(grammarAccess.getProfileSettingsMapEntryAccess().getValueAssignment_1(), "rule__ProfileSettingsMapEntry__ValueAssignment_1");
			builder.put(grammarAccess.getLocationSettingsMapEntryAccess().getKeyAssignment_0(), "rule__LocationSettingsMapEntry__KeyAssignment_0");
			builder.put(grammarAccess.getLocationSettingsMapEntryAccess().getValueAssignment_1(), "rule__LocationSettingsMapEntry__ValueAssignment_1");
			builder.put(grammarAccess.getDistanceSettingsMapEntryAccess().getKeyAssignment_0(), "rule__DistanceSettingsMapEntry__KeyAssignment_0");
			builder.put(grammarAccess.getDistanceSettingsMapEntryAccess().getValueAssignment_2(), "rule__DistanceSettingsMapEntry__ValueAssignment_2");
			builder.put(grammarAccess.getTimingSettingsMapEntryAccess().getKeyAssignment_0(), "rule__TimingSettingsMapEntry__KeyAssignment_0");
			builder.put(grammarAccess.getTimingSettingsMapEntryAccess().getValueAssignment_2(), "rule__TimingSettingsMapEntry__ValueAssignment_2");
			builder.put(grammarAccess.getMotionProfileSettingsAccess().getMotionProfileAssignment_1(), "rule__MotionProfileSettings__MotionProfileAssignment_1");
			builder.put(grammarAccess.getMotionProfileSettingsAccess().getMotionArgumentsAssignment_3_0(), "rule__MotionProfileSettings__MotionArgumentsAssignment_3_0");
			builder.put(grammarAccess.getMotionProfileSettingsAccess().getMotionArgumentsAssignment_3_1_1(), "rule__MotionProfileSettings__MotionArgumentsAssignment_3_1_1");
			builder.put(grammarAccess.getMotionArgumentsMapEntryAccess().getKeyAssignment_0(), "rule__MotionArgumentsMapEntry__KeyAssignment_0");
			builder.put(grammarAccess.getMotionArgumentsMapEntryAccess().getValueAssignment_2(), "rule__MotionArgumentsMapEntry__ValueAssignment_2");
			builder.put(grammarAccess.getPhysicalLocationAccess().getDefaultExpAssignment_0_1(), "rule__PhysicalLocation__DefaultExpAssignment_0_1");
			builder.put(grammarAccess.getPhysicalLocationAccess().getMinExpAssignment_1_1_2(), "rule__PhysicalLocation__MinExpAssignment_1_1_2");
			builder.put(grammarAccess.getPhysicalLocationAccess().getMaxExpAssignment_1_2_2(), "rule__PhysicalLocation__MaxExpAssignment_1_2_2");
			builder.put(grammarAccess.getPhysicalLocationAccess().getDefaultExpAssignment_1_5(), "rule__PhysicalLocation__DefaultExpAssignment_1_5");
			builder.put(grammarAccess.getFixedValueAccess().getValueExpAssignment(), "rule__FixedValue__ValueExpAssignment");
			builder.put(grammarAccess.getArrayAccess().getValuesExpAssignment_2(), "rule__Array__ValuesExpAssignment_2");
			builder.put(grammarAccess.getArrayAccess().getValuesExpAssignment_3_1(), "rule__Array__ValuesExpAssignment_3_1");
			builder.put(grammarAccess.getTriangularDistributionAccess().getMinExpAssignment_4(), "rule__TriangularDistribution__MinExpAssignment_4");
			builder.put(grammarAccess.getTriangularDistributionAccess().getMaxExpAssignment_8(), "rule__TriangularDistribution__MaxExpAssignment_8");
			builder.put(grammarAccess.getTriangularDistributionAccess().getModeExpAssignment_12(), "rule__TriangularDistribution__ModeExpAssignment_12");
			builder.put(grammarAccess.getTriangularDistributionAccess().getDefaultExpAssignment_13_3(), "rule__TriangularDistribution__DefaultExpAssignment_13_3");
			builder.put(grammarAccess.getPertDistributionAccess().getMinExpAssignment_4(), "rule__PertDistribution__MinExpAssignment_4");
			builder.put(grammarAccess.getPertDistributionAccess().getMaxExpAssignment_8(), "rule__PertDistribution__MaxExpAssignment_8");
			builder.put(grammarAccess.getPertDistributionAccess().getModeExpAssignment_12(), "rule__PertDistribution__ModeExpAssignment_12");
			builder.put(grammarAccess.getPertDistributionAccess().getGammaExpAssignment_16(), "rule__PertDistribution__GammaExpAssignment_16");
			builder.put(grammarAccess.getPertDistributionAccess().getDefaultExpAssignment_17_3(), "rule__PertDistribution__DefaultExpAssignment_17_3");
			builder.put(grammarAccess.getNormalDistributionAccess().getMeanExpAssignment_4(), "rule__NormalDistribution__MeanExpAssignment_4");
			builder.put(grammarAccess.getNormalDistributionAccess().getSdExpAssignment_8(), "rule__NormalDistribution__SdExpAssignment_8");
			builder.put(grammarAccess.getNormalDistributionAccess().getDefaultExpAssignment_9_3(), "rule__NormalDistribution__DefaultExpAssignment_9_3");
			builder.put(grammarAccess.getEnumeratedDistributionAccess().getValuesExpAssignment_2(), "rule__EnumeratedDistribution__ValuesExpAssignment_2");
			builder.put(grammarAccess.getEnumeratedDistributionAccess().getValuesExpAssignment_3_1(), "rule__EnumeratedDistribution__ValuesExpAssignment_3_1");
			builder.put(grammarAccess.getEnumeratedDistributionAccess().getDefaultExpAssignment_4_3(), "rule__EnumeratedDistribution__DefaultExpAssignment_4_3");
			builder.put(grammarAccess.getDeclarationAccess().getNameAssignment_2(), "rule__Declaration__NameAssignment_2");
			builder.put(grammarAccess.getDeclarationAccess().getExpressionAssignment_4(), "rule__Declaration__ExpressionAssignment_4");
			builder.put(grammarAccess.getPlusOrMinusAccess().getRightAssignment_1_1(), "rule__PlusOrMinus__RightAssignment_1_1");
			builder.put(grammarAccess.getMulOrDivAccess().getRightAssignment_1_1(), "rule__MulOrDiv__RightAssignment_1_1");
			builder.put(grammarAccess.getAtomicAccess().getValueAssignment_0_1(), "rule__Atomic__ValueAssignment_0_1");
			builder.put(grammarAccess.getAtomicAccess().getDeclarationAssignment_1_1(), "rule__Atomic__DeclarationAssignment_1_1");
		}
	}
	
	@Inject
	private NameMappings nameMappings;

	@Inject
	private SettingGrammarAccess grammarAccess;

	@Override
	protected InternalSettingParser createParser() {
		InternalSettingParser result = new InternalSettingParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		return nameMappings.getRuleName(element);
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public SettingGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(SettingGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
	public NameMappings getNameMappings() {
		return nameMappings;
	}
	
	public void setNameMappings(NameMappings nameMappings) {
		this.nameMappings = nameMappings;
	}
}
