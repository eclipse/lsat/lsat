/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.main;

import activity.Move;
import activity.PeripheralAction;
import activity.SimpleAction;
import com.google.common.collect.Iterables;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import lsat_graph.PeripheralActionTask;
import machine.Axis;
import machine.IResource;
import machine.Machine;
import machine.Peripheral;
import machine.Resource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;
import org.eclipse.lsat.common.xtend.Queries;
import org.eclipse.lsat.scheduler.simulator.common.Common;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Pair;

@SuppressWarnings("all")
public class GenerateScheduleTemplate {
  public static CharSequence generateScheduleHtml(final Machine machine) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<!DOCTYPE html>");
    _builder.newLine();
    _builder.append("<html>");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("<head>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<meta charset=\"UTF-8\">");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<title>");
    String _type = machine.getType();
    _builder.append(_type, "    ");
    _builder.append("</title>");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<!-- Designed against Paper.js v0.12.15, which can be downloaded from: http://paperjs.org/download/ -->");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<script type=\"text/javascript\" src=\"http://paperjs.org/assets/js/paper.js\"></script>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<script type=\"text/javascript\">");
    _builder.newLine();
    _builder.append("      ");
    _builder.append("paper.install(window);");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("</script>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<script src=\"settings.js\" type=\"text/javascript\"></script>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<script src=\"machine.js\" \"text/javascript\"></script>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<script src=\"schedule.js\" type=\"text/javascript\"></script>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<script>");
    _builder.newLine();
    _builder.append("      ");
    _builder.append("window.onload = function() {");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("paper.setup(\'mcanvas\');");
    _builder.newLine();
    {
      final Function1<Resource, Collection<? extends IResource>> _function = (Resource it) -> {
        return Common.getItemsOrResource(it);
      };
      final Function1<IResource, String> _function_1 = (IResource it) -> {
        return it.fqn();
      };
      List<IResource> _sortBy = IterableExtensions.<IResource, String>sortBy(Queries.<Resource, IResource>xcollect(machine.getResources(), _function), _function_1);
      for(final IResource resource : _sortBy) {
        {
          final Function1<Peripheral, String> _function_2 = (Peripheral it) -> {
            return it.getName();
          };
          List<Peripheral> _sortBy_1 = IterableExtensions.<Peripheral, String>sortBy(resource.getResource().getPeripherals(), _function_2);
          for(final Peripheral peripheral : _sortBy_1) {
            _builder.append("        ");
            String _iD = Common.getID(resource, peripheral);
            _builder.append(_iD, "        ");
            _builder.append(".init(");
            {
              boolean _isEmpty = peripheral.getPositions().isEmpty();
              boolean _not = (!_isEmpty);
              if (_not) {
                _builder.append("/*TODO: position*/");
              }
            }
            _builder.append(");");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.newLine();
    _builder.append("        ");
    _builder.append("var message;");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("var count = 0; // The total number frames");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("view.onFrame = function(event) {");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("count++;");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("// Default frame rate approximately 60fps, reducing it to 20fps");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("if (count % 3 !== 0) {");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("return;");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("          ");
    _builder.append("if (typeof schedule == \'function\') {");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("schedule(event.time);");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("if (typeof setAnimationTime == \'function\') {");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("setAnimationTime(event.time);");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("} else if (message == undefined) {");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("message = new PointText({");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("point : new Point(10, 50),");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("content : \'Please generate a schedule to start the animation\',");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("fillColor : \'Black\',");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("strokeColor: \'White\',");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("strokeWidth: 1,");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("fontFamily : \'Courier New\',");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("fontWeight : \'bold\',");
    _builder.newLine();
    _builder.append("              ");
    _builder.append("fontSize : 25");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("});");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("      ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("</script>");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("</head>");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("<body>");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("<canvas id=\"mcanvas\" width=\"800\" height=\"600\" hidpi=\"off\" keepalive=\"true\" style=\"background:white\"></canvas>");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("</body>");
    _builder.newLine();
    _builder.append("</html>");
    _builder.newLine();
    return _builder;
  }
  
  public static <T extends Task> String generateScheduleJavaScript(final Schedule<T> schedule) {
    final Function1<Sequence<T>, EList<ScheduledTask<T>>> _function = (Sequence<T> it) -> {
      return it.getScheduledTasks();
    };
    final Function1<ScheduledTask<T>, Boolean> _function_1 = (ScheduledTask<T> it) -> {
      T _task = it.getTask();
      return Boolean.valueOf((_task instanceof PeripheralActionTask));
    };
    final Function1<ScheduledTask<T>, ScheduledTask<PeripheralActionTask>> _function_2 = (ScheduledTask<T> it) -> {
      return ((ScheduledTask<PeripheralActionTask>) it);
    };
    final List<ScheduledTask<PeripheralActionTask>> paTasks = IterableExtensions.<ScheduledTask<PeripheralActionTask>>toList(IterableExtensions.<ScheduledTask<T>, ScheduledTask<PeripheralActionTask>>map(Queries.<ScheduledTask<T>>select(IterableExtensions.<Sequence<T>, ScheduledTask<T>>flatMap(schedule.getSequences(), _function), _function_1), _function_2));
    final Function1<ScheduledTask<PeripheralActionTask>, PeripheralAction> _function_3 = (ScheduledTask<PeripheralActionTask> it) -> {
      return it.getTask().getAction();
    };
    final Function1<Move, Boolean> _function_4 = (Move it) -> {
      return Boolean.valueOf(it.isPositionMove());
    };
    final Function1<Move, Pair<IResource, Peripheral>> _function_5 = (Move it) -> {
      IResource _resource = it.getResource();
      Peripheral _peripheral = it.getPeripheral();
      return Pair.<IResource, Peripheral>of(_resource, _peripheral);
    };
    final Iterable<Pair<IResource, Peripheral>> distancePeripherals = Queries.<Pair<IResource, Peripheral>>unique(IterableExtensions.<Move, Pair<IResource, Peripheral>>map(IterableExtensions.<Move>reject(Iterables.<Move>filter(ListExtensions.<ScheduledTask<PeripheralActionTask>, PeripheralAction>map(paTasks, _function_3), Move.class), _function_4), _function_5));
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Auto generated, do not modify!");
    _builder.newLine();
    _builder.append("function schedule(time) {");
    _builder.newLine();
    {
      boolean _hasElements = false;
      for(final Pair<IResource, Peripheral> rp : distancePeripherals) {
        if (!_hasElements) {
          _hasElements = true;
        }
        _builder.append("  ");
        _builder.append("var ");
        String _positionID = Common.getPositionID(rp.getKey(), rp.getValue(), Common.CURRENT_POSITION);
        _builder.append(_positionID);
        _builder.append(" = {");
        {
          EList<Axis> _axes = rp.getValue().getType().getAxes();
          boolean _hasElements_1 = false;
          for(final Axis axis : _axes) {
            if (!_hasElements_1) {
              _hasElements_1 = true;
            } else {
              _builder.appendImmediate(", ", "");
            }
            String _name = axis.getName();
            _builder.append(_name);
            _builder.append(":");
            String _positionID_1 = Common.getPositionID(rp.getKey(), rp.getValue(), Common.INITIAL_POSITION);
            _builder.append(_positionID_1);
            _builder.append("_");
            String _name_1 = axis.getName();
            _builder.append(_name_1);
          }
        }
        _builder.append("};");
        _builder.newLineIfNotEmpty();
      }
      if (_hasElements) {
        String _lineSeparator = System.lineSeparator();
        _builder.append(_lineSeparator);
      }
    }
    {
      for(final ScheduledTask<PeripheralActionTask> task : paTasks) {
        _builder.append("  ");
        CharSequence _generateTask = GenerateScheduleTemplate.generateTask(task);
        _builder.append(_generateTask);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  private static CharSequence generateTask(final ScheduledTask<PeripheralActionTask> scheduledTask) {
    return GenerateScheduleTemplate.generateTask(scheduledTask, scheduledTask.getTask().getAction());
  }
  
  private static CharSequence _generateTask(final ScheduledTask<PeripheralActionTask> task, final SimpleAction action) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = action.getType().getName();
    _builder.append(_name);
    _builder.append("(");
    String _iD = Common.getID(action);
    _builder.append(_iD);
    _builder.append(", ");
    BigDecimal _roundToOneDecimal = GenerateScheduleTemplate.roundToOneDecimal(task.getStartTime());
    _builder.append(_roundToOneDecimal);
    _builder.append(", ");
    BigDecimal _roundToOneDecimal_1 = GenerateScheduleTemplate.roundToOneDecimal(task.getEndTime());
    _builder.append(_roundToOneDecimal_1);
    _builder.append(", time);");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  private static CharSequence _generateTask(final ScheduledTask<PeripheralActionTask> task, final Move move) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _isPositionMove = move.isPositionMove();
      if (_isPositionMove) {
        _builder.append("move(");
        String _iD = Common.getID(move);
        _builder.append(_iD);
        _builder.append(", ");
        String _positionID = Common.getPositionID(move, move.getSourcePosition());
        _builder.append(_positionID);
        _builder.append(", ");
        String _positionID_1 = Common.getPositionID(move, move.getTargetPosition());
        _builder.append(_positionID_1);
        _builder.append(", ");
        BigDecimal _roundToOneDecimal = GenerateScheduleTemplate.roundToOneDecimal(task.getStartTime());
        _builder.append(_roundToOneDecimal);
        _builder.append(", ");
        BigDecimal _roundToOneDecimal_1 = GenerateScheduleTemplate.roundToOneDecimal(task.getEndTime());
        _builder.append(_roundToOneDecimal_1);
        _builder.append(", time);");
        _builder.newLineIfNotEmpty();
      } else {
        _builder.append("move(");
        String _iD_1 = Common.getID(move);
        _builder.append(_iD_1);
        _builder.append(", clone(");
        String _positionID_2 = Common.getPositionID(move, Common.CURRENT_POSITION);
        _builder.append(_positionID_2);
        _builder.append("), plusIs(");
        String _positionID_3 = Common.getPositionID(move, Common.CURRENT_POSITION);
        _builder.append(_positionID_3);
        _builder.append(", ");
        String _distanceID = Common.getDistanceID(move, move.getDistance());
        _builder.append(_distanceID);
        _builder.append("), ");
        BigDecimal _roundToOneDecimal_2 = GenerateScheduleTemplate.roundToOneDecimal(task.getStartTime());
        _builder.append(_roundToOneDecimal_2);
        _builder.append(", ");
        BigDecimal _roundToOneDecimal_3 = GenerateScheduleTemplate.roundToOneDecimal(task.getEndTime());
        _builder.append(_roundToOneDecimal_3);
        _builder.append(", time);");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }
  
  private static BigDecimal roundToOneDecimal(final BigDecimal input) {
    return input.setScale(1, RoundingMode.HALF_UP);
  }
  
  private static CharSequence generateTask(final ScheduledTask<PeripheralActionTask> task, final PeripheralAction move) {
    if (move instanceof Move) {
      return _generateTask(task, (Move)move);
    } else if (move instanceof SimpleAction) {
      return _generateTask(task, (SimpleAction)move);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(task, move).toString());
    }
  }
}
