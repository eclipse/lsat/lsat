/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.main;

import expressions.Expression;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import machine.Axis;
import machine.Distance;
import machine.IResource;
import machine.Position;
import machine.SymbolicPosition;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.scheduler.simulator.common.Common;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import setting.MotionSettings;
import setting.PhysicalLocation;
import setting.PhysicalSettings;
import setting.Settings;

@SuppressWarnings("all")
public class GenerateSettingsTemplate {
  public static CharSequence generateSettingsJavaScript(final Settings model) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// TODO: Adapt these locations to match the screen resolution");
    _builder.newLine();
    {
      final Function1<PhysicalSettings, String> _function = (PhysicalSettings it) -> {
        return it.fqn();
      };
      List<PhysicalSettings> _sortBy = IterableExtensions.<PhysicalSettings, String>sortBy(model.getTransitivePhysicalSettings(), _function);
      for(final PhysicalSettings physicalSettings : _sortBy) {
        {
          final Function1<Map.Entry<Axis, MotionSettings>, String> _function_1 = (Map.Entry<Axis, MotionSettings> it) -> {
            return it.getKey().getName();
          };
          List<Map.Entry<Axis, MotionSettings>> _sortBy_1 = IterableExtensions.<Map.Entry<Axis, MotionSettings>, String>sortBy(physicalSettings.getMotionSettings(), _function_1);
          for(final Map.Entry<Axis, MotionSettings> motionSettings : _sortBy_1) {
            {
              final Function1<Map.Entry<Position, PhysicalLocation>, String> _function_2 = (Map.Entry<Position, PhysicalLocation> it) -> {
                return it.getKey().getName();
              };
              List<Map.Entry<Position, PhysicalLocation>> _sortBy_2 = IterableExtensions.<Map.Entry<Position, PhysicalLocation>, String>sortBy(motionSettings.getValue().getLocationSettings(), _function_2);
              for(final Map.Entry<Position, PhysicalLocation> locationSettings : _sortBy_2) {
                _builder.append("var ");
                String _positionID = Common.getPositionID(physicalSettings, locationSettings.getKey());
                _builder.append(_positionID);
                _builder.append("_");
                String _name = motionSettings.getKey().getName();
                _builder.append(_name);
                _builder.append(" = ");
                BigDecimal _default = locationSettings.getValue().getDefault();
                _builder.append(_default);
                _builder.append(";");
                _builder.newLineIfNotEmpty();
              }
            }
            {
              boolean _isEmpty = physicalSettings.getPeripheral().getDistances().isEmpty();
              boolean _not = (!_isEmpty);
              if (_not) {
                _builder.append("var ");
                String _positionID_1 = Common.getPositionID(physicalSettings, Common.INITIAL_POSITION);
                _builder.append(_positionID_1);
                _builder.append("_");
                String _name_1 = motionSettings.getKey().getName();
                _builder.append(_name_1);
                _builder.append(" = 0;");
                _builder.newLineIfNotEmpty();
              }
            }
            {
              final Function1<Map.Entry<Distance, Expression>, String> _function_3 = (Map.Entry<Distance, Expression> it) -> {
                return it.getKey().getName();
              };
              List<Map.Entry<Distance, Expression>> _sortBy_3 = IterableExtensions.<Map.Entry<Distance, Expression>, String>sortBy(motionSettings.getValue().getDistanceSettings(), _function_3);
              for(final Map.Entry<Distance, Expression> distanceSettings : _sortBy_3) {
                _builder.append("var ");
                String _distanceID = Common.getDistanceID(physicalSettings, distanceSettings.getKey());
                _builder.append(_distanceID);
                _builder.append("_");
                String _name_2 = motionSettings.getKey().getName();
                _builder.append(_name_2);
                _builder.append(" = ");
                BigDecimal _evaluate = distanceSettings.getValue().evaluate();
                _builder.append(_evaluate);
                _builder.append(";");
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
      }
    }
    _builder.newLine();
    _builder.append("// Auto generated, do not modify!");
    _builder.newLine();
    {
      final Function1<PhysicalSettings, String> _function_4 = (PhysicalSettings it) -> {
        return it.fqn();
      };
      List<PhysicalSettings> _sortBy_4 = IterableExtensions.<PhysicalSettings, String>sortBy(model.getTransitivePhysicalSettings(), _function_4);
      for(final PhysicalSettings physicalSettings_1 : _sortBy_4) {
        {
          Collection<? extends IResource> _itemsOrResource = Common.getItemsOrResource(physicalSettings_1.getResource());
          for(final IResource resource : _itemsOrResource) {
            {
              EList<SymbolicPosition> _positions = physicalSettings_1.getPeripheral().getPositions();
              for(final SymbolicPosition position : _positions) {
                _builder.append("var ");
                String _positionID_2 = Common.getPositionID(resource, physicalSettings_1.getPeripheral(), position);
                _builder.append(_positionID_2);
                _builder.append(" = {");
                {
                  EList<Axis> _axes = physicalSettings_1.getPeripheral().getType().getAxes();
                  boolean _hasElements = false;
                  for(final Axis axis : _axes) {
                    if (!_hasElements) {
                      _hasElements = true;
                    } else {
                      _builder.appendImmediate(", ", "");
                    }
                    String _name_3 = axis.getName();
                    _builder.append(_name_3);
                    _builder.append(":");
                    String _positionID_3 = Common.getPositionID(physicalSettings_1, position.getPosition(axis));
                    _builder.append(_positionID_3);
                    _builder.append("_");
                    String _name_4 = axis.getName();
                    _builder.append(_name_4);
                  }
                }
                _builder.append("};");
                _builder.newLineIfNotEmpty();
              }
            }
            {
              boolean _isEmpty_1 = physicalSettings_1.getPeripheral().getDistances().isEmpty();
              boolean _not_1 = (!_isEmpty_1);
              if (_not_1) {
                _builder.append("var ");
                String _positionID_4 = Common.getPositionID(resource, physicalSettings_1.getPeripheral(), Common.INITIAL_POSITION);
                _builder.append(_positionID_4);
                _builder.append(" = {");
                {
                  EList<Axis> _axes_1 = physicalSettings_1.getPeripheral().getType().getAxes();
                  boolean _hasElements_1 = false;
                  for(final Axis axis_1 : _axes_1) {
                    if (!_hasElements_1) {
                      _hasElements_1 = true;
                    } else {
                      _builder.appendImmediate(", ", "");
                    }
                    String _name_5 = axis_1.getName();
                    _builder.append(_name_5);
                    _builder.append(":");
                    String _positionID_5 = Common.getPositionID(physicalSettings_1, Common.INITIAL_POSITION);
                    _builder.append(_positionID_5);
                    _builder.append("_");
                    String _name_6 = axis_1.getName();
                    _builder.append(_name_6);
                  }
                }
                _builder.append("};");
                _builder.newLineIfNotEmpty();
              }
            }
            {
              EList<Distance> _distances = physicalSettings_1.getPeripheral().getDistances();
              for(final Distance distance : _distances) {
                _builder.append("var ");
                String _distanceID_1 = Common.getDistanceID(resource, physicalSettings_1.getPeripheral(), distance);
                _builder.append(_distanceID_1);
                _builder.append(" = {");
                {
                  EList<Axis> _axes_2 = physicalSettings_1.getPeripheral().getType().getAxes();
                  boolean _hasElements_2 = false;
                  for(final Axis axis_2 : _axes_2) {
                    if (!_hasElements_2) {
                      _hasElements_2 = true;
                    } else {
                      _builder.appendImmediate(", ", "");
                    }
                    String _name_7 = axis_2.getName();
                    _builder.append(_name_7);
                    _builder.append(":");
                    String _distanceID_2 = Common.getDistanceID(physicalSettings_1, distance);
                    _builder.append(_distanceID_2);
                    _builder.append("_");
                    String _name_8 = axis_2.getName();
                    _builder.append(_name_8);
                  }
                }
                _builder.append("};");
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
      }
    }
    return _builder;
  }
}
