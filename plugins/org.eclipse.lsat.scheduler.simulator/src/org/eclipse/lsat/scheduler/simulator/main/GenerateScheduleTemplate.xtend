/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.main

import activity.Move
import activity.SimpleAction
import java.math.BigDecimal
import java.math.RoundingMode
import lsat_graph.PeripheralActionTask
import machine.Machine
import org.eclipse.lsat.common.scheduler.graph.Task
import org.eclipse.lsat.common.scheduler.schedule.Schedule
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask

import static extension org.eclipse.lsat.common.xtend.Queries.*
import static extension org.eclipse.lsat.scheduler.simulator.common.Common.*

class GenerateScheduleTemplate {
    static def generateScheduleHtml(Machine machine) '''
        <!DOCTYPE html>
        <html>
          <head>
            <meta charset="UTF-8">
            <title>«machine.type»</title>
        
            <!-- Designed against Paper.js v0.12.15, which can be downloaded from: http://paperjs.org/download/ -->
            <script type="text/javascript" src="http://paperjs.org/assets/js/paper.js"></script>
            <script type="text/javascript">
              paper.install(window);
            </script>
        
            <script src="settings.js" type="text/javascript"></script>
            <script src="machine.js" "text/javascript"></script>
            <script src="schedule.js" type="text/javascript"></script>
        
            <script>
              window.onload = function() {
                paper.setup('mcanvas');
                «FOR resource : machine.resources.xcollect[itemsOrResource].sortBy[fqn]»
                    «FOR peripheral : resource.resource.peripherals.sortBy[name]»
                      «resource.getID(peripheral)».init(«IF !peripheral.positions.isEmpty»/*TODO: position*/«ENDIF»);
                    «ENDFOR»
                «ENDFOR»
        
                var message;
                var count = 0; // The total number frames
                view.onFrame = function(event) {
                  count++;
                  // Default frame rate approximately 60fps, reducing it to 20fps
                  if (count % 3 !== 0) {
                    return;
                  }
        
                  if (typeof schedule == 'function') {
                    schedule(event.time);
                    if (typeof setAnimationTime == 'function') {
                      setAnimationTime(event.time);
                    }
                  } else if (message == undefined) {
                    message = new PointText({
                      point : new Point(10, 50),
                      content : 'Please generate a schedule to start the animation',
                      fillColor : 'Black',
                      strokeColor: 'White',
                      strokeWidth: 1,
                      fontFamily : 'Courier New',
                      fontWeight : 'bold',
                      fontSize : 25
                    });
                  }
                }
              }
            </script>
          </head>
          <body>
            <canvas id="mcanvas" width="800" height="600" hidpi="off" keepalive="true" style="background:white"></canvas>
          </body>
        </html>
    '''

    static def <T extends Task> generateScheduleJavaScript(Schedule<T> schedule) {
        val paTasks = schedule.sequences.flatMap[scheduledTasks].select[task instanceof PeripheralActionTask].map[it as ScheduledTask<PeripheralActionTask>].toList
        val distancePeripherals = paTasks.map[task.action].filter(Move).reject[positionMove].map[resource -> peripheral].unique
        
        return '''
            // Auto generated, do not modify!
            function schedule(time) {
            «FOR rp : distancePeripherals AFTER System.lineSeparator»
                «'  '»var «getPositionID(rp.key, rp.value, CURRENT_POSITION)» = {«FOR axis : rp.value.type.axes SEPARATOR ', '»«axis.name»:«getPositionID(rp.key, rp.value, INITIAL_POSITION)»_«axis.name»«ENDFOR»};
            «ENDFOR»
            «FOR task : paTasks»
                «'  '»«generateTask(task)»
            «ENDFOR»
            }
        '''
    }
    
    static def private generateTask(ScheduledTask<PeripheralActionTask> scheduledTask) {
        scheduledTask.generateTask(scheduledTask.task.action)
    }
    
    static def private dispatch generateTask(ScheduledTask<PeripheralActionTask> task, SimpleAction action) '''
        «action.type.name»(«action.ID», «task.startTime.roundToOneDecimal», «task.endTime.roundToOneDecimal», time);
    '''

    static def private dispatch generateTask(ScheduledTask<PeripheralActionTask> task, Move move) '''
        «IF move.positionMove»
            move(«move.ID», «move.getPositionID(move.sourcePosition)», «move.getPositionID(move.targetPosition)», «task.startTime.roundToOneDecimal», «task.endTime.roundToOneDecimal», time);
        «ELSE»
            move(«move.ID», clone(«move.getPositionID(CURRENT_POSITION)»), plusIs(«move.getPositionID(CURRENT_POSITION)», «move.getDistanceID(move.distance)»), «task.startTime.roundToOneDecimal», «task.endTime.roundToOneDecimal», time);
        «ENDIF»
    '''

    static def private roundToOneDecimal(BigDecimal input) {
        return input.setScale(1, RoundingMode.HALF_UP)
    }
}
