/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.scheduler.simulator.main

import setting.Settings

import static extension org.eclipse.lsat.scheduler.simulator.common.Common.*

class GenerateSettingsTemplate {

    static def generateSettingsJavaScript(Settings model) '''
        // TODO: Adapt these locations to match the screen resolution
        «FOR physicalSettings : model.transitivePhysicalSettings.sortBy[fqn]»
            «FOR motionSettings : physicalSettings.motionSettings.sortBy[key.name]»
                «FOR locationSettings : motionSettings.value.locationSettings.sortBy[key.name]»
                    var «getPositionID(physicalSettings, locationSettings.key)»_«motionSettings.key.name» = «locationSettings.value.^default»;
                «ENDFOR»
                «IF !physicalSettings.peripheral.distances.isEmpty»
                    var «getPositionID(physicalSettings, INITIAL_POSITION)»_«motionSettings.key.name» = 0;
                «ENDIF»
                «FOR distanceSettings : motionSettings.value.distanceSettings.sortBy[key.name]»
                    var «getDistanceID(physicalSettings, distanceSettings.key)»_«motionSettings.key.name» = «distanceSettings.value.evaluate»;
                «ENDFOR»
            «ENDFOR»
        «ENDFOR»
        
        // Auto generated, do not modify!
        «FOR physicalSettings : model.transitivePhysicalSettings.sortBy[fqn]»
            «FOR resource : physicalSettings.resource.itemsOrResource»
                «FOR position : physicalSettings.peripheral.positions»
                    var «getPositionID(resource, physicalSettings.peripheral, position)» = {«FOR axis : physicalSettings.peripheral.type.axes SEPARATOR ', '»«axis.name»:«getPositionID(physicalSettings, position.getPosition(axis))»_«axis.name»«ENDFOR»};
                «ENDFOR»
                «IF !physicalSettings.peripheral.distances.isEmpty»
                    var «getPositionID(resource, physicalSettings.peripheral, INITIAL_POSITION)» = {«FOR axis : physicalSettings.peripheral.type.axes SEPARATOR ', '»«axis.name»:«getPositionID(physicalSettings, INITIAL_POSITION)»_«axis.name»«ENDFOR»};
                «ENDIF»
                «FOR distance : physicalSettings.peripheral.distances»
                    var «getDistanceID(resource, physicalSettings.peripheral, distance)» = {«FOR axis : physicalSettings.peripheral.type.axes SEPARATOR ', '»«axis.name»:«getDistanceID(physicalSettings, distance)»_«axis.name»«ENDFOR»};
                «ENDFOR»
            «ENDFOR»
        «ENDFOR»
    '''
}
