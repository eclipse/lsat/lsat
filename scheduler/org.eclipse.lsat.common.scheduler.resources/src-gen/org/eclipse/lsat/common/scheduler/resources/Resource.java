/**
 */
package org.eclipse.lsat.common.scheduler.resources;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getResource()
 * @model
 * @generated
 */
public interface Resource extends AbstractResource {
} // Resource
