/**
 */
package org.eclipse.lsat.common.scheduler.resources;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource#getContainer <em>Container</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource#getStart <em>Start</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getAbstractResource()
 * @model abstract="true"
 * @generated
 */
public interface AbstractResource extends NamedResource {
	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(ResourceContainer)
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getAbstractResource_Container()
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourceContainer#getResources
	 * @model opposite="resources" required="true" transient="false"
	 * @generated
	 */
	ResourceContainer getContainer();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.resources.AbstractResource#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ResourceContainer value);

	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns the start time of this resource (i.e. the sum of offsets of all containers).
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see org.eclipse.lsat.common.scheduler.resources.ResourcesPackage#getAbstractResource_Start()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	BigDecimal getStart();

} // AbstractResource
