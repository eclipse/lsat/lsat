/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.resources.util;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.Iterator;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.scheduler.resources.Resource;
import org.eclipse.lsat.common.scheduler.resources.ResourceModel;

public final class ResourcesQueries {
    private ResourcesQueries() {
        /* Empty */
    }

    public static final Iterator<Resource> allResources(ResourceModel resourceModel) {
        return from(EcoreUtil.getAllContents(resourceModel, true)).objectsOfKind(Resource.class).iterator();
    }
}
