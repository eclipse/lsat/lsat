/**
 */
package org.eclipse.lsat.common.scheduler.graph.impl;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import org.eclipse.lsat.common.graph.directed.impl.DirectedGraphImpl;

import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.GraphPackage;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;

import org.eclipse.lsat.common.scheduler.resources.ResourceModel;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Dependency Graph</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.graph.impl.TaskDependencyGraphImpl#getResourceModel <em>Resource Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskDependencyGraphImpl<T extends Task> extends DirectedGraphImpl<T, Dependency> implements TaskDependencyGraph<T> {
	/**
     * The cached value of the '{@link #getResourceModel() <em>Resource Model</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #getResourceModel()
     * @generated
     * @ordered
     */
	protected ResourceModel resourceModel;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected TaskDependencyGraphImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return GraphPackage.Literals.TASK_DEPENDENCY_GRAPH;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific element type known in this context.
     * @generated
     */
	@Override
	public EList<Dependency> getEdges() {
        if (edges == null)
        {
            edges = new EObjectContainmentWithInverseEList<Dependency>(Dependency.class, this, GraphPackage.TASK_DEPENDENCY_GRAPH__EDGES, DirectedGraphPackage.EDGE__GRAPH) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Edge.class; } };
        }
        return edges;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * This is specialized for the more specific element type known in this context.
     * @generated
     */
	@Override
	public EList<T> getNodes() {
        if (nodes == null)
        {
            nodes = new EObjectContainmentWithInverseEList<T>(Task.class, this, GraphPackage.TASK_DEPENDENCY_GRAPH__NODES, DirectedGraphPackage.NODE__GRAPH) { private static final long serialVersionUID = 1L; @Override public Class<?> getInverseFeatureClass() { return Node.class; } };
        }
        return nodes;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public ResourceModel getResourceModel() {
        if (resourceModel != null && resourceModel.eIsProxy())
        {
            InternalEObject oldResourceModel = (InternalEObject)resourceModel;
            resourceModel = (ResourceModel)eResolveProxy(oldResourceModel);
            if (resourceModel != oldResourceModel)
            {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, GraphPackage.TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL, oldResourceModel, resourceModel));
            }
        }
        return resourceModel;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public ResourceModel basicGetResourceModel() {
        return resourceModel;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void setResourceModel(ResourceModel newResourceModel) {
        ResourceModel oldResourceModel = resourceModel;
        resourceModel = newResourceModel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, GraphPackage.TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL, oldResourceModel, resourceModel));
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID)
        {
            case GraphPackage.TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL:
                if (resolve) return getResourceModel();
                return basicGetResourceModel();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID)
        {
            case GraphPackage.TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL:
                setResourceModel((ResourceModel)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID)
        {
            case GraphPackage.TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL:
                setResourceModel((ResourceModel)null);
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID)
        {
            case GraphPackage.TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL:
                return resourceModel != null;
        }
        return super.eIsSet(featureID);
    }

} //TaskDependencyGraphImpl
