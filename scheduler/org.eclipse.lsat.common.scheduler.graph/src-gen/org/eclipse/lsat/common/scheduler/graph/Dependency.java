/**
 */
package org.eclipse.lsat.common.scheduler.graph;

import org.eclipse.lsat.common.graph.directed.Edge;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage#getDependency()
 * @model
 * @generated
 */
public interface Dependency extends Edge {
} // Dependency
