/**
 */
package org.eclipse.lsat.common.scheduler.graph;

import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.scheduler.graph.GraphFactory
 * @model kind="package"
 * @generated
 */
public interface GraphPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "graph";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.eclipse.org/lsat/scheduler/graph";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "graph";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	GraphPackage eINSTANCE = org.eclipse.lsat.common.scheduler.graph.impl.GraphPackageImpl.init();

	/**
     * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.graph.impl.TaskDependencyGraphImpl <em>Task Dependency Graph</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.lsat.common.scheduler.graph.impl.TaskDependencyGraphImpl
     * @see org.eclipse.lsat.common.scheduler.graph.impl.GraphPackageImpl#getTaskDependencyGraph()
     * @generated
     */
	int TASK_DEPENDENCY_GRAPH = 0;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH__NAME = DirectedGraphPackage.DIRECTED_GRAPH__NAME;

	/**
     * The feature id for the '<em><b>Sub Graphs</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH__SUB_GRAPHS = DirectedGraphPackage.DIRECTED_GRAPH__SUB_GRAPHS;

	/**
     * The feature id for the '<em><b>Parent Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH__PARENT_GRAPH = DirectedGraphPackage.DIRECTED_GRAPH__PARENT_GRAPH;

	/**
     * The feature id for the '<em><b>Edges</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH__EDGES = DirectedGraphPackage.DIRECTED_GRAPH__EDGES;

	/**
     * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH__NODES = DirectedGraphPackage.DIRECTED_GRAPH__NODES;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' containment reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH__ASPECTS = DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS;

	/**
     * The feature id for the '<em><b>Resource Model</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 0;

	/**
     * The number of structural features of the '<em>Task Dependency Graph</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH_FEATURE_COUNT = DirectedGraphPackage.DIRECTED_GRAPH_FEATURE_COUNT + 1;

	/**
     * The operation id for the '<em>All Nodes In Topological Order</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER = DirectedGraphPackage.DIRECTED_GRAPH___ALL_NODES_IN_TOPOLOGICAL_ORDER;

	/**
     * The number of operations of the '<em>Task Dependency Graph</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_DEPENDENCY_GRAPH_OPERATION_COUNT = DirectedGraphPackage.DIRECTED_GRAPH_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl <em>Task</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl
     * @see org.eclipse.lsat.common.scheduler.graph.impl.GraphPackageImpl#getTask()
     * @generated
     */
	int TASK = 1;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK__NAME = DirectedGraphPackage.NODE__NAME;

	/**
     * The feature id for the '<em><b>Outgoing Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK__OUTGOING_EDGES = DirectedGraphPackage.NODE__OUTGOING_EDGES;

	/**
     * The feature id for the '<em><b>Incoming Edges</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK__INCOMING_EDGES = DirectedGraphPackage.NODE__INCOMING_EDGES;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK__GRAPH = DirectedGraphPackage.NODE__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK__ASPECTS = DirectedGraphPackage.NODE__ASPECTS;

	/**
     * The feature id for the '<em><b>Execution Time</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK__EXECUTION_TIME = DirectedGraphPackage.NODE_FEATURE_COUNT + 0;

	/**
     * The feature id for the '<em><b>Resources</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK__RESOURCES = DirectedGraphPackage.NODE_FEATURE_COUNT + 1;

	/**
     * The number of structural features of the '<em>Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_FEATURE_COUNT = DirectedGraphPackage.NODE_FEATURE_COUNT + 2;

	/**
     * The number of operations of the '<em>Task</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int TASK_OPERATION_COUNT = DirectedGraphPackage.NODE_OPERATION_COUNT + 0;

	/**
     * The meta object id for the '{@link org.eclipse.lsat.common.scheduler.graph.impl.DependencyImpl <em>Dependency</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.lsat.common.scheduler.graph.impl.DependencyImpl
     * @see org.eclipse.lsat.common.scheduler.graph.impl.GraphPackageImpl#getDependency()
     * @generated
     */
	int DEPENDENCY = 2;

	/**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DEPENDENCY__NAME = DirectedGraphPackage.EDGE__NAME;

	/**
     * The feature id for the '<em><b>Source Node</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DEPENDENCY__SOURCE_NODE = DirectedGraphPackage.EDGE__SOURCE_NODE;

	/**
     * The feature id for the '<em><b>Target Node</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DEPENDENCY__TARGET_NODE = DirectedGraphPackage.EDGE__TARGET_NODE;

	/**
     * The feature id for the '<em><b>Graph</b></em>' container reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DEPENDENCY__GRAPH = DirectedGraphPackage.EDGE__GRAPH;

	/**
     * The feature id for the '<em><b>Aspects</b></em>' reference list.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DEPENDENCY__ASPECTS = DirectedGraphPackage.EDGE__ASPECTS;

	/**
     * The number of structural features of the '<em>Dependency</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DEPENDENCY_FEATURE_COUNT = DirectedGraphPackage.EDGE_FEATURE_COUNT + 0;

	/**
     * The number of operations of the '<em>Dependency</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int DEPENDENCY_OPERATION_COUNT = DirectedGraphPackage.EDGE_OPERATION_COUNT + 0;


	/**
     * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph <em>Task Dependency Graph</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Task Dependency Graph</em>'.
     * @see org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph
     * @generated
     */
	EClass getTaskDependencyGraph();

	/**
     * Returns the meta object for the reference '{@link org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph#getResourceModel <em>Resource Model</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference '<em>Resource Model</em>'.
     * @see org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph#getResourceModel()
     * @see #getTaskDependencyGraph()
     * @generated
     */
	EReference getTaskDependencyGraph_ResourceModel();

	/**
     * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.graph.Task <em>Task</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Task</em>'.
     * @see org.eclipse.lsat.common.scheduler.graph.Task
     * @generated
     */
	EClass getTask();

	/**
     * Returns the meta object for the attribute '{@link org.eclipse.lsat.common.scheduler.graph.Task#getExecutionTime <em>Execution Time</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Execution Time</em>'.
     * @see org.eclipse.lsat.common.scheduler.graph.Task#getExecutionTime()
     * @see #getTask()
     * @generated
     */
	EAttribute getTask_ExecutionTime();

	/**
     * Returns the meta object for the reference list '{@link org.eclipse.lsat.common.scheduler.graph.Task#getResources <em>Resources</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Resources</em>'.
     * @see org.eclipse.lsat.common.scheduler.graph.Task#getResources()
     * @see #getTask()
     * @generated
     */
	EReference getTask_Resources();

	/**
     * Returns the meta object for class '{@link org.eclipse.lsat.common.scheduler.graph.Dependency <em>Dependency</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Dependency</em>'.
     * @see org.eclipse.lsat.common.scheduler.graph.Dependency
     * @generated
     */
	EClass getDependency();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	GraphFactory getGraphFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.graph.impl.TaskDependencyGraphImpl <em>Task Dependency Graph</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see org.eclipse.lsat.common.scheduler.graph.impl.TaskDependencyGraphImpl
         * @see org.eclipse.lsat.common.scheduler.graph.impl.GraphPackageImpl#getTaskDependencyGraph()
         * @generated
         */
		EClass TASK_DEPENDENCY_GRAPH = eINSTANCE.getTaskDependencyGraph();

		/**
         * The meta object literal for the '<em><b>Resource Model</b></em>' reference feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference TASK_DEPENDENCY_GRAPH__RESOURCE_MODEL = eINSTANCE.getTaskDependencyGraph_ResourceModel();

		/**
         * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl <em>Task</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see org.eclipse.lsat.common.scheduler.graph.impl.TaskImpl
         * @see org.eclipse.lsat.common.scheduler.graph.impl.GraphPackageImpl#getTask()
         * @generated
         */
		EClass TASK = eINSTANCE.getTask();

		/**
         * The meta object literal for the '<em><b>Execution Time</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute TASK__EXECUTION_TIME = eINSTANCE.getTask_ExecutionTime();

		/**
         * The meta object literal for the '<em><b>Resources</b></em>' reference list feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EReference TASK__RESOURCES = eINSTANCE.getTask_Resources();

		/**
         * The meta object literal for the '{@link org.eclipse.lsat.common.scheduler.graph.impl.DependencyImpl <em>Dependency</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see org.eclipse.lsat.common.scheduler.graph.impl.DependencyImpl
         * @see org.eclipse.lsat.common.scheduler.graph.impl.GraphPackageImpl#getDependency()
         * @generated
         */
		EClass DEPENDENCY = eINSTANCE.getDependency();

	}

} //GraphPackage
