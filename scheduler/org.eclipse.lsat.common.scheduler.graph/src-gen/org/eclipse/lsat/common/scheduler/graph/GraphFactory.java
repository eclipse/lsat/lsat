/**
 */
package org.eclipse.lsat.common.scheduler.graph;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage
 * @generated
 */
public interface GraphFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	GraphFactory eINSTANCE = org.eclipse.lsat.common.scheduler.graph.impl.GraphFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Task Dependency Graph</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Task Dependency Graph</em>'.
     * @generated
     */
	<T extends Task> TaskDependencyGraph<T> createTaskDependencyGraph();

	/**
     * Returns a new object of class '<em>Task</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Task</em>'.
     * @generated
     */
	Task createTask();

	/**
     * Returns a new object of class '<em>Dependency</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Dependency</em>'.
     * @generated
     */
	Dependency createDependency();

	/**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	GraphPackage getGraphPackage();

} //GraphFactory
