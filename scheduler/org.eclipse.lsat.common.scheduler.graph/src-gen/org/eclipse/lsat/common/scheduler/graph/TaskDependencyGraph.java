/**
 */
package org.eclipse.lsat.common.scheduler.graph;

import org.eclipse.lsat.common.graph.directed.DirectedGraph;

import org.eclipse.lsat.common.scheduler.resources.ResourceModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Dependency Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph#getResourceModel <em>Resource Model</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage#getTaskDependencyGraph()
 * @model
 * @generated
 */
public interface TaskDependencyGraph<T extends Task> extends DirectedGraph<T, Dependency> {
	/**
     * Returns the value of the '<em><b>Resource Model</b></em>' reference.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Resource Model</em>' reference.
     * @see #setResourceModel(ResourceModel)
     * @see org.eclipse.lsat.common.scheduler.graph.GraphPackage#getTaskDependencyGraph_ResourceModel()
     * @model required="true"
     * @generated
     */
	ResourceModel getResourceModel();

	/**
     * Sets the value of the '{@link org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph#getResourceModel <em>Resource Model</em>}' reference.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Resource Model</em>' reference.
     * @see #getResourceModel()
     * @generated
     */
	void setResourceModel(ResourceModel value);

} // TaskDependencyGraph
