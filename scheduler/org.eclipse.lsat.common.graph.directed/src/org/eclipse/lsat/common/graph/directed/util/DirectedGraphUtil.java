/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.graph.directed.util;

import java.util.ArrayList;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

public final class DirectedGraphUtil {
    private DirectedGraphUtil() {
        /* Empty */
    }

    public interface EdgeFactory {
        Edge create();
    }

    public static void addEdge(DirectedGraph<? extends Node, ? extends Edge> graph, Node source, Node target, EdgeFactory factory) {
        if (!edgeExists(graph, source, target)) {
            Edge edge = factory.create();
            edge.setSourceNode(source);
            edge.setTargetNode(target);
            edge.setGraph(graph);
        }
    }

    public static void delete(Node node) {
        for (Edge edge: node.getIncomingEdges()) {
            delete(edge);
        }
        for (Edge edge: node.getOutgoingEdges()) {
            delete(edge);
        }
        EcoreUtil.delete(node);
    }

    public static void delete(Edge edge) {
        // this is really needed to clean up all relations:
        edge.setSourceNode(null);
        edge.setTargetNode(null);
        EcoreUtil.delete(edge);
    }

    public static void shortcutAndRemoveNode(Node node, EdgeFactory edgeFactory) {
        DirectedGraph<? extends Node, ? extends Edge> graph = (DirectedGraph<?, ?>)node.eContainer();
        for (Edge in: node.getIncomingEdges()) {
            for (Edge out: node.getOutgoingEdges()) {
                addEdge(graph, in.getSourceNode(), out.getTargetNode(), edgeFactory);
            }
        }
        new ArrayList<>(node.getIncomingEdges()).forEach(e -> delete(e));
        new ArrayList<>(node.getOutgoingEdges()).forEach(e -> delete(e));
        delete(node);
    }

    private static boolean edgeExists(DirectedGraph<? extends Node, ? extends Edge> graph, Node source, Node target) {
        return graph.getEdges().stream().anyMatch(e -> (e.getSourceNode() == source && e.getTargetNode() == target));
    }

}
