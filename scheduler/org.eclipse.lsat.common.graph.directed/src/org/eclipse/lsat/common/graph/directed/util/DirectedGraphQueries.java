/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.graph.directed.util;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

public final class DirectedGraphQueries {
    private DirectedGraphQueries() {
        /* Empty */
    }

    public static final <N extends Node, E extends Edge> Iterable<N> allSubNodes(DirectedGraph<N, E> graph) {
        return from(graph).closure(true, DirectedGraph::getSubGraphs).xcollect(DirectedGraph::getNodes);
    }

    public static final <N extends Node> EList<N> topologicalOrdering(Iterable<N> nodes) {
        return topologicalOrdering(nodes, new LinkedList<N>());
    }

    public static final <N extends Node> EList<N> topologicalOrdering(Iterable<N> nodes,
            Comparator<? super N> preference)
    {
        return topologicalOrdering(nodes, new PriorityQueue<N>(11, preference));
    }

    @SuppressWarnings("unchecked")
    private static final <N extends Node> EList<N> topologicalOrdering(Iterable<N> nodes,
            Queue<N> nodesWithoutIncomingEdges)
    {
        assert nodesWithoutIncomingEdges.isEmpty() : "Expected an empty queue";
        final Map<N, Integer> notVisitedIncomingEdges = new HashMap<N, Integer>();
        for (N node: nodes) {
            int incomingEdgesSize = node.getIncomingEdges().size();
            notVisitedIncomingEdges.put(node, incomingEdgesSize);
            if (0 == incomingEdgesSize) {
                nodesWithoutIncomingEdges.add(node);
            }
        }

        final EList<N> topologicalOrder = new BasicEList<N>(notVisitedIncomingEdges.size());
        while (!nodesWithoutIncomingEdges.isEmpty()) {
            // Take a node from the queue
            N node = nodesWithoutIncomingEdges.remove();
            // Add to ordering
            topologicalOrder.add(node);

            // For all targets, remove this task from the incoming edges count
            for (Edge outgoingEdge: node.getOutgoingEdges()) {
                Node sucessor = outgoingEdge.getTargetNode();
                Integer notVisitedIncomingEdgesSize = notVisitedIncomingEdges.get(sucessor);
                if (null == notVisitedIncomingEdgesSize) {
                    // Strange, but might happen while typing in the XText editor, just skip
                    continue;
                }
                notVisitedIncomingEdgesSize -= 1;
                // As successor was found in the notVisitedIncomingEdgesSize
                // it should be of type N, so we can safely cast here
                notVisitedIncomingEdges.put((N)sucessor, notVisitedIncomingEdgesSize);
                if (0 == notVisitedIncomingEdgesSize) {
                    nodesWithoutIncomingEdges.add((N)sucessor);
                }
            }
        }

        if (topologicalOrder.size() != notVisitedIncomingEdges.size()) {
            // "Cycle exists in graph!!"
            return null;
        }

        return topologicalOrder;
    }

    public static final <N extends Node> EList<N> reverseTopologicalOrdering(Iterable<N> nodes) {
        return reverseTopologicalOrdering(nodes, new LinkedList<N>());
    }

    public static final <N extends Node> EList<N> reverseTopologicalOrdering(Iterable<N> nodes,
            Comparator<? super N> preference)
    {
        return reverseTopologicalOrdering(nodes, new PriorityQueue<N>(11, preference));
    }

    @SuppressWarnings("unchecked")
    private static final <N extends Node> EList<N> reverseTopologicalOrdering(Iterable<N> nodes,
            Queue<N> nodesWithoutOutgoingEdges)
    {
        assert nodesWithoutOutgoingEdges.isEmpty() : "Expected an empty queue";
        final Map<N, Integer> notVisitedOutgoingEdges = new HashMap<N, Integer>();
        for (N node: nodes) {
            int outgoingEdgesSize = node.getOutgoingEdges().size();
            notVisitedOutgoingEdges.put(node, outgoingEdgesSize);
            if (0 == outgoingEdgesSize) {
                nodesWithoutOutgoingEdges.add(node);
            }
        }

        final EList<N> reverseTopologicalOrder = new BasicEList<N>(notVisitedOutgoingEdges.size());
        while (!nodesWithoutOutgoingEdges.isEmpty()) {
            // Take a node from the queue
            N node = nodesWithoutOutgoingEdges.remove();
            // Add to ordering
            reverseTopologicalOrder.add(node);

            // For all targets, remove this task from the incoming edges count
            for (Edge incomingEdge: node.getIncomingEdges()) {
                Node predecessor = incomingEdge.getSourceNode();
                Integer notVisitedOutgoingEdgesSize = notVisitedOutgoingEdges.get(predecessor);
                if (null == notVisitedOutgoingEdgesSize) {
                    // Strange, but might happen while typing in the XText editor, just skip
                    continue;
                }
                notVisitedOutgoingEdgesSize -= 1;
                // As predecessor was found in the notVisitedOutgoingEdgesSize
                // it should be of type N, so we can safely cast here
                notVisitedOutgoingEdges.put((N)predecessor, notVisitedOutgoingEdgesSize);
                if (0 == notVisitedOutgoingEdgesSize) {
                    nodesWithoutOutgoingEdges.add((N)predecessor);
                }
            }
        }

        if (reverseTopologicalOrder.size() != notVisitedOutgoingEdges.size()) {
            // "Cycle exists in graph!!"
            return null;
        }

        return reverseTopologicalOrder;
    }
}
