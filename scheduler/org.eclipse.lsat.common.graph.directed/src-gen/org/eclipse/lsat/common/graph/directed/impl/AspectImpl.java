/**
 */
package org.eclipse.lsat.common.graph.directed.impl;

import org.eclipse.lsat.common.graph.directed.Aspect;
import org.eclipse.lsat.common.graph.directed.DirectedGraph;
import org.eclipse.lsat.common.graph.directed.DirectedGraphPackage;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aspect</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.AspectImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.AspectImpl#getEdges <em>Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.AspectImpl#getNodes <em>Nodes</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.impl.AspectImpl#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AspectImpl<N extends Node, E extends Edge> extends MinimalEObjectImpl.Container implements Aspect<N, E> {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEdges() <em>Edges</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<E> edges;

	/**
	 * The cached value of the '{@link #getNodes() <em>Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<N> nodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AspectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DirectedGraphPackage.Literals.ASPECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectedGraphPackage.ASPECT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<E> getEdges() {
		if (edges == null) {
			edges = new EObjectWithInverseResolvingEList.ManyInverse<E>(Edge.class, this, DirectedGraphPackage.ASPECT__EDGES, DirectedGraphPackage.EDGE__ASPECTS);
		}
		return edges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<N> getNodes() {
		if (nodes == null) {
			nodes = new EObjectWithInverseResolvingEList.ManyInverse<N>(Node.class, this, DirectedGraphPackage.ASPECT__NODES, DirectedGraphPackage.NODE__ASPECTS);
		}
		return nodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DirectedGraph<N, E> getGraph() {
		if (eContainerFeatureID() != DirectedGraphPackage.ASPECT__GRAPH) return null;
		return (DirectedGraph<N, E>)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraph(DirectedGraph<N, E> newGraph, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newGraph, DirectedGraphPackage.ASPECT__GRAPH, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGraph(DirectedGraph<N, E> newGraph) {
		if (newGraph != eInternalContainer() || (eContainerFeatureID() != DirectedGraphPackage.ASPECT__GRAPH && newGraph != null)) {
			if (EcoreUtil.isAncestor(this, newGraph))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newGraph != null)
				msgs = ((InternalEObject)newGraph).eInverseAdd(this, DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS, DirectedGraph.class, msgs);
			msgs = basicSetGraph(newGraph, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DirectedGraphPackage.ASPECT__GRAPH, newGraph, newGraph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DirectedGraphPackage.ASPECT__EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEdges()).basicAdd(otherEnd, msgs);
			case DirectedGraphPackage.ASPECT__NODES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNodes()).basicAdd(otherEnd, msgs);
			case DirectedGraphPackage.ASPECT__GRAPH:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetGraph((DirectedGraph<N, E>)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DirectedGraphPackage.ASPECT__EDGES:
				return ((InternalEList<?>)getEdges()).basicRemove(otherEnd, msgs);
			case DirectedGraphPackage.ASPECT__NODES:
				return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
			case DirectedGraphPackage.ASPECT__GRAPH:
				return basicSetGraph(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DirectedGraphPackage.ASPECT__GRAPH:
				return eInternalContainer().eInverseRemove(this, DirectedGraphPackage.DIRECTED_GRAPH__ASPECTS, DirectedGraph.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DirectedGraphPackage.ASPECT__NAME:
				return getName();
			case DirectedGraphPackage.ASPECT__EDGES:
				return getEdges();
			case DirectedGraphPackage.ASPECT__NODES:
				return getNodes();
			case DirectedGraphPackage.ASPECT__GRAPH:
				return getGraph();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DirectedGraphPackage.ASPECT__NAME:
				setName((String)newValue);
				return;
			case DirectedGraphPackage.ASPECT__EDGES:
				getEdges().clear();
				getEdges().addAll((Collection<? extends E>)newValue);
				return;
			case DirectedGraphPackage.ASPECT__NODES:
				getNodes().clear();
				getNodes().addAll((Collection<? extends N>)newValue);
				return;
			case DirectedGraphPackage.ASPECT__GRAPH:
				setGraph((DirectedGraph<N, E>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DirectedGraphPackage.ASPECT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DirectedGraphPackage.ASPECT__EDGES:
				getEdges().clear();
				return;
			case DirectedGraphPackage.ASPECT__NODES:
				getNodes().clear();
				return;
			case DirectedGraphPackage.ASPECT__GRAPH:
				setGraph((DirectedGraph<N, E>)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DirectedGraphPackage.ASPECT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DirectedGraphPackage.ASPECT__EDGES:
				return edges != null && !edges.isEmpty();
			case DirectedGraphPackage.ASPECT__NODES:
				return nodes != null && !nodes.isEmpty();
			case DirectedGraphPackage.ASPECT__GRAPH:
				return getGraph() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AspectImpl
