/**
 */
package org.eclipse.lsat.common.graph.directed;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Directed Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getSubGraphs <em>Sub Graphs</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getParentGraph <em>Parent Graph</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getEdges <em>Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getNodes <em>Nodes</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getAspects <em>Aspects</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getDirectedGraph()
 * @model
 * @generated
 */
public interface DirectedGraph<N extends Node, E extends Edge> extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getDirectedGraph_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Sub Graphs</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.DirectedGraph}<code>&lt;N, E&gt;</code>.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getParentGraph <em>Parent Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Graphs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Graphs</em>' containment reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getDirectedGraph_SubGraphs()
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraph#getParentGraph
	 * @model opposite="parentGraph" containment="true"
	 * @generated
	 */
	EList<DirectedGraph<N, E>> getSubGraphs();

	/**
	 * Returns the value of the '<em><b>Parent Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getSubGraphs <em>Sub Graphs</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Graph</em>' container reference.
	 * @see #setParentGraph(DirectedGraph)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getDirectedGraph_ParentGraph()
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraph#getSubGraphs
	 * @model opposite="subGraphs" transient="false"
	 * @generated
	 */
	DirectedGraph<N, E> getParentGraph();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getParentGraph <em>Parent Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Graph</em>' container reference.
	 * @see #getParentGraph()
	 * @generated
	 */
	void setParentGraph(DirectedGraph<N, E> value);

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' containment reference list.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Edge#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' containment reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getDirectedGraph_Edges()
	 * @see org.eclipse.lsat.common.graph.directed.Edge#getGraph
	 * @model opposite="graph" containment="true"
	 * @generated
	 */
	EList<E> getEdges();

	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Node#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getDirectedGraph_Nodes()
	 * @see org.eclipse.lsat.common.graph.directed.Node#getGraph
	 * @model opposite="graph" containment="true"
	 * @generated
	 */
	EList<N> getNodes();

	/**
	 * Returns the value of the '<em><b>Aspects</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.lsat.common.graph.directed.Aspect}<code>&lt;N, E&gt;</code>.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Aspect#getGraph <em>Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspects</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspects</em>' containment reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getDirectedGraph_Aspects()
	 * @see org.eclipse.lsat.common.graph.directed.Aspect#getGraph
	 * @model opposite="graph" containment="true"
	 * @generated
	 */
	EList<Aspect<N, E>> getAspects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns all the nodes of the root graph and all its subgraphs in a topological order.
	 * Returns null if the graph contains cycles due to which the topological order could not be defined.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	EList<N> allNodesInTopologicalOrder();

} // DirectedGraph
