/**
 */
package org.eclipse.lsat.common.graph.directed.impl;

import org.eclipse.lsat.common.graph.directed.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DirectedGraphFactoryImpl extends EFactoryImpl implements DirectedGraphFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DirectedGraphFactory init() {
		try {
			DirectedGraphFactory theDirectedGraphFactory = (DirectedGraphFactory)EPackage.Registry.INSTANCE.getEFactory(DirectedGraphPackage.eNS_URI);
			if (theDirectedGraphFactory != null) {
				return theDirectedGraphFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DirectedGraphFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectedGraphFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DirectedGraphPackage.DIRECTED_GRAPH: return createDirectedGraph();
			case DirectedGraphPackage.NODE: return createNode();
			case DirectedGraphPackage.EDGE: return createEdge();
			case DirectedGraphPackage.ASPECT: return createAspect();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public <N extends Node, E extends Edge> DirectedGraph<N, E> createDirectedGraph() {
		DirectedGraphImpl<N, E> directedGraph = new DirectedGraphImpl<N, E>();
		return directedGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Node createNode() {
		NodeImpl node = new NodeImpl();
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Edge createEdge() {
		EdgeImpl edge = new EdgeImpl();
		return edge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public <N extends Node, E extends Edge> Aspect<N, E> createAspect() {
		AspectImpl<N, E> aspect = new AspectImpl<N, E>();
		return aspect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DirectedGraphPackage getDirectedGraphPackage() {
		return (DirectedGraphPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DirectedGraphPackage getPackage() {
		return DirectedGraphPackage.eINSTANCE;
	}

} //DirectedGraphFactoryImpl
