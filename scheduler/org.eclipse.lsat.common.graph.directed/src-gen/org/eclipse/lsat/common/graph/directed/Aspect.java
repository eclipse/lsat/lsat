/**
 */
package org.eclipse.lsat.common.graph.directed;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aspect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Aspect#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Aspect#getEdges <em>Edges</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Aspect#getNodes <em>Nodes</em>}</li>
 *   <li>{@link org.eclipse.lsat.common.graph.directed.Aspect#getGraph <em>Graph</em>}</li>
 * </ul>
 *
 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getAspect()
 * @model
 * @generated
 */
public interface Aspect<N extends Node, E extends Edge> extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getAspect_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.Aspect#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Edges</b></em>' reference list.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Edge#getAspects <em>Aspects</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getAspect_Edges()
	 * @see org.eclipse.lsat.common.graph.directed.Edge#getAspects
	 * @model opposite="aspects"
	 * @generated
	 */
	EList<E> getEdges();

	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' reference list.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.Node#getAspects <em>Aspects</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' reference list.
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getAspect_Nodes()
	 * @see org.eclipse.lsat.common.graph.directed.Node#getAspects
	 * @model opposite="aspects"
	 * @generated
	 */
	EList<N> getNodes();

	/**
	 * Returns the value of the '<em><b>Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.lsat.common.graph.directed.DirectedGraph#getAspects <em>Aspects</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graph</em>' container reference.
	 * @see #setGraph(DirectedGraph)
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraphPackage#getAspect_Graph()
	 * @see org.eclipse.lsat.common.graph.directed.DirectedGraph#getAspects
	 * @model opposite="aspects" required="true" transient="false"
	 * @generated
	 */
	DirectedGraph<N, E> getGraph();

	/**
	 * Sets the value of the '{@link org.eclipse.lsat.common.graph.directed.Aspect#getGraph <em>Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graph</em>' container reference.
	 * @see #getGraph()
	 * @generated
	 */
	void setGraph(DirectedGraph<N, E> value);

} // Aspect
