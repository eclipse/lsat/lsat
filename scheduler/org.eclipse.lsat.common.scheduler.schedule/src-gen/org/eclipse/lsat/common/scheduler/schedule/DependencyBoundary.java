/**
 */
package org.eclipse.lsat.common.scheduler.schedule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Dependency Boundary</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getDependencyBoundary()
 * @model
 * @generated
 */
public enum DependencyBoundary implements Enumerator {
	/**
	 * The '<em><b>In Resource</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IN_RESOURCE_VALUE
	 * @generated
	 * @ordered
	 */
	IN_RESOURCE(0, "inResource", "inResource"),

	/**
	 * The '<em><b>Cross Resource</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROSS_RESOURCE_VALUE
	 * @generated
	 * @ordered
	 */
	CROSS_RESOURCE(1, "crossResource", "crossResource"),

	/**
	 * The '<em><b>Cross Resource Container</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROSS_RESOURCE_CONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	CROSS_RESOURCE_CONTAINER(2, "crossResourceContainer", "crossResourceContainer");

	/**
	 * The '<em><b>In Resource</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>In Resource</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IN_RESOURCE
	 * @model name="inResource"
	 * @generated
	 * @ordered
	 */
	public static final int IN_RESOURCE_VALUE = 0;

	/**
	 * The '<em><b>Cross Resource</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Cross Resource</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROSS_RESOURCE
	 * @model name="crossResource"
	 * @generated
	 * @ordered
	 */
	public static final int CROSS_RESOURCE_VALUE = 1;

	/**
	 * The '<em><b>Cross Resource Container</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Cross Resource Container</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROSS_RESOURCE_CONTAINER
	 * @model name="crossResourceContainer"
	 * @generated
	 * @ordered
	 */
	public static final int CROSS_RESOURCE_CONTAINER_VALUE = 2;

	/**
	 * An array of all the '<em><b>Dependency Boundary</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DependencyBoundary[] VALUES_ARRAY =
		new DependencyBoundary[] {
			IN_RESOURCE,
			CROSS_RESOURCE,
			CROSS_RESOURCE_CONTAINER,
		};

	/**
	 * A public read-only list of all the '<em><b>Dependency Boundary</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DependencyBoundary> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Dependency Boundary</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DependencyBoundary get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DependencyBoundary result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Dependency Boundary</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DependencyBoundary getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DependencyBoundary result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Dependency Boundary</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DependencyBoundary get(int value) {
		switch (value) {
			case IN_RESOURCE_VALUE: return IN_RESOURCE;
			case CROSS_RESOURCE_VALUE: return CROSS_RESOURCE;
			case CROSS_RESOURCE_CONTAINER_VALUE: return CROSS_RESOURCE_CONTAINER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DependencyBoundary(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DependencyBoundary
