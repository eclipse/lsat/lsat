/**
 */
package org.eclipse.lsat.common.scheduler.schedule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Scheduled Dependency Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.lsat.common.scheduler.schedule.SchedulePackage#getScheduledDependencyType()
 * @model
 * @generated
 */
public enum ScheduledDependencyType implements Enumerator {
	/**
	 * The '<em><b>Source Node Ends Before Target Node Starts</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS_VALUE
	 * @generated
	 * @ordered
	 */
	SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS(0, "sourceNodeEndsBeforeTargetNodeStarts", "sourceNodeEndsBeforeTargetNodeStarts"),

	/**
	 * The '<em><b>Source Node Ends Before Target Node Ends</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS_VALUE
	 * @generated
	 * @ordered
	 */
	SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS(1, "sourceNodeEndsBeforeTargetNodeEnds", "sourceNodeEndsBeforeTargetNodeEnds"),

	/**
	 * The '<em><b>Source Node Starts Before Target Node Starts</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS_VALUE
	 * @generated
	 * @ordered
	 */
	SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS(2, "sourceNodeStartsBeforeTargetNodeStarts", "sourceNodeStartsBeforeTargetNodeStarts"),

	/**
	 * The '<em><b>Source Node Starts Before Target Node Ends</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS_VALUE
	 * @generated
	 * @ordered
	 */
	SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS(3, "sourceNodeStartsBeforeTargetNodeEnds", "sourceNodeStartsBeforeTargetNodeEnds");

	/**
	 * The '<em><b>Source Node Ends Before Target Node Starts</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Source Node Ends Before Target Node Starts</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS
	 * @model name="sourceNodeEndsBeforeTargetNodeStarts"
	 * @generated
	 * @ordered
	 */
	public static final int SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS_VALUE = 0;

	/**
	 * The '<em><b>Source Node Ends Before Target Node Ends</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Source Node Ends Before Target Node Ends</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS
	 * @model name="sourceNodeEndsBeforeTargetNodeEnds"
	 * @generated
	 * @ordered
	 */
	public static final int SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS_VALUE = 1;

	/**
	 * The '<em><b>Source Node Starts Before Target Node Starts</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Source Node Starts Before Target Node Starts</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS
	 * @model name="sourceNodeStartsBeforeTargetNodeStarts"
	 * @generated
	 * @ordered
	 */
	public static final int SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS_VALUE = 2;

	/**
	 * The '<em><b>Source Node Starts Before Target Node Ends</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Source Node Starts Before Target Node Ends</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS
	 * @model name="sourceNodeStartsBeforeTargetNodeEnds"
	 * @generated
	 * @ordered
	 */
	public static final int SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS_VALUE = 3;

	/**
	 * An array of all the '<em><b>Scheduled Dependency Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ScheduledDependencyType[] VALUES_ARRAY =
		new ScheduledDependencyType[] {
			SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS,
			SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS,
			SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS,
			SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS,
		};

	/**
	 * A public read-only list of all the '<em><b>Scheduled Dependency Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ScheduledDependencyType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Scheduled Dependency Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ScheduledDependencyType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ScheduledDependencyType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Scheduled Dependency Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ScheduledDependencyType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ScheduledDependencyType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Scheduled Dependency Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ScheduledDependencyType get(int value) {
		switch (value) {
			case SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS_VALUE: return SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS;
			case SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS_VALUE: return SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_ENDS;
			case SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS_VALUE: return SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_STARTS;
			case SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS_VALUE: return SOURCE_NODE_STARTS_BEFORE_TARGET_NODE_ENDS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ScheduledDependencyType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ScheduledDependencyType
