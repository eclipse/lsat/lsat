/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.schedule.impl;

import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.scheduler.resources.Resource;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;

class ScheduleHelper {
    private ScheduleHelper() {
        // Empty
    }

    static final Resource safeGetResource(Node node) {
        if (node instanceof ScheduledTask) {
            Sequence<?> sequence = ((ScheduledTask<?>)node).getSequence();
            if (null != sequence) {
                return sequence.getResource();
            }
        }
        return null;
    }
}
