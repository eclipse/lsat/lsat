/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.algorithm;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;
import static org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType.SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS;

import java.util.List;

import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.graph.directed.Node;
import org.eclipse.lsat.common.queries.QueryableIterable;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduleFactory;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependencyType;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;
import org.eclipse.lsat.common.util.PairwiseIterable;

public final class CompleteScheduledDependencies {
    private CompleteScheduledDependencies() {
        // Empty constructor for utilities
    }

    public static final <T extends Task> void perform(Schedule<T> schedule) {
        schedule.getSequences().forEach(s -> schedule.getEdges().addAll(createScheduledDependencies(s)));
    }

    public static final <T extends Task> List<ScheduledDependency> createScheduledDependencies(Sequence<T> sequence) {

        return from(PairwiseIterable.of(sequence.getScheduledTasks())).xcollectOne(pair -> {
            final QueryableIterable<Node> predecessors = from(pair.getRight().getIncomingEdges())
                    .objectsOfKind(ScheduledDependency.class)
                    .select(d -> SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS.equals(d.getType()))
                    .collectOne(Edge::getSourceNode);
            if (predecessors.includes(pair.getLeft())) {
                return null;
            }

            ScheduledDependency dependency = ScheduleFactory.eINSTANCE.createScheduledDependency();
            dependency.setSourceNode(pair.getLeft());
            dependency.setTargetNode(pair.getRight());
            dependency.setType(ScheduledDependencyType.SOURCE_NODE_ENDS_BEFORE_TARGET_NODE_STARTS);
            return dependency;
        }).asList();
    }
}
