/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.algorithm.internal;

import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.lsat.common.scheduler.graph.Task;

public class ExecutiontimesValidator implements EValidator {
    public static final ExecutiontimesValidator INSTANCE = new ExecutiontimesValidator();

    @Override
    public boolean validate(EDataType eDataType, Object value, DiagnosticChain diagnostics,
            Map<Object, Object> context)
    {
        return true;
    }

    @Override
    public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return validate(eObject, diagnostics, context);
    }

    @Override
    public boolean validate(EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
        if (eObject instanceof Task && null == ((Task)eObject).getExecutionTime()) {
            diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, "org.eclipse.lsat.common.scheduler.algorithm", 0,
                    "Execution time should be set before scheduling", new Object[]
                    {eObject}));
            return false;
        }
        return true;
    }
}
