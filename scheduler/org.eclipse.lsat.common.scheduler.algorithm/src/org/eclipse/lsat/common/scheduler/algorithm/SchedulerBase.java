/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.common.scheduler.algorithm;

import static org.eclipse.lsat.common.queries.QueryableIterable.from;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EValidatorRegistryImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.lsat.common.emf.common.util.DiagnosticUtil;
import org.eclipse.lsat.common.emf.validation.EValidatorUtil;
import org.eclipse.lsat.common.graph.directed.Edge;
import org.eclipse.lsat.common.scheduler.graph.Dependency;
import org.eclipse.lsat.common.scheduler.graph.GraphPackage;
import org.eclipse.lsat.common.scheduler.graph.Task;
import org.eclipse.lsat.common.scheduler.graph.TaskDependencyGraph;
import org.eclipse.lsat.common.scheduler.resources.Resource;
import org.eclipse.lsat.common.scheduler.resources.ResourceModel;
import org.eclipse.lsat.common.scheduler.resources.util.ResourcesQueries;
import org.eclipse.lsat.common.scheduler.schedule.Schedule;
import org.eclipse.lsat.common.scheduler.schedule.ScheduleFactory;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledDependency;
import org.eclipse.lsat.common.scheduler.schedule.ScheduledTask;
import org.eclipse.lsat.common.scheduler.schedule.Sequence;
import org.slf4j.Logger;

public abstract class SchedulerBase<T extends Task> implements IScheduler<T> {
    protected static final ScheduleFactory SCHEDULE_FACTORY = ScheduleFactory.eINSTANCE;

    /**
     * For performance optimization a Task to ScheduledTask Cache is introduced
     */
    private final Map<T, ScheduledTask<T>> itsTaskToScheduledTaskCache = new HashMap<>();

    private TaskDependencyGraph<? extends T> graph = null;

    private Schedule<T> schedule = null;

    public TaskDependencyGraph<? extends T> getGraph() {
        return graph;
    }

    public Schedule<T> getSchedule() {
        return schedule;
    }

    @Override
    public synchronized Schedule<T> createSchedule(TaskDependencyGraph<? extends T> taskDependencyGraph)
            throws SchedulerException
    {
        try {
            graph = taskDependencyGraph;

            validateGraph();
            schedule = initializeSchedule();
            performScheduling();
            postProcessSchedule();
            return schedule;
        } finally {
            itsTaskToScheduledTaskCache.clear();
        }
    }

    protected abstract Logger getLog();

    /**
     * Validates the Graph.
     *
     * @throws SchedulerException When validation fails
     */
    protected void validateGraph() throws SchedulerException {
        // Create a 'private' registry to add our constraints (which are only valid for this validation)
        EValidator.Registry validatorRegistry = new EValidatorRegistryImpl(EValidator.Registry.INSTANCE);
        for (EValidator eValidator: getComplementaryGraphValidators()) {
            EValidatorUtil.registerValidations(GraphPackage.eINSTANCE, eValidator, validatorRegistry);
        }

        // Customize the diagnostician to pretty print the graph
        Diagnostician diagnostician = new Diagnostician(validatorRegistry) {
            @Override
            public String getObjectLabel(EObject eObject) {
                if (eObject instanceof TaskDependencyGraph<?>) {
                    @SuppressWarnings("rawtypes")
                    String name = ((TaskDependencyGraph)eObject).getName();
                    return null == name ? "graph" : "graph " + name;
                }
                return super.getObjectLabel(eObject);
            }
        };

        Diagnostic diagnostic = diagnostician.validate(getGraph());
        switch (diagnostic.getSeverity()) {
            case Diagnostic.WARNING:
                getLog().warn(DiagnosticUtil.getFullMessage(diagnostic));
                break;
            case Diagnostic.ERROR:
                throw new SchedulerException(DiagnosticUtil.getFullMessage(diagnostic));
            default:
                break;
        }
    }

    /**
     * Allows subclasses to add/override graph validations
     */
    protected EValidator[] getComplementaryGraphValidators() {
        return new EValidator[0];
    }

    /**
     * Initializes the platform part of the Schedule (i.e. {@link Sequence}s).
     *
     * @return initialized schedule
     */
    protected Schedule<T> initializeSchedule() {
        // Create schedule model
        Schedule<T> schedule = SCHEDULE_FACTORY.createSchedule();
        schedule.setName(getGraph().getName());
        ResourceModel resourceModel = getGraph().getResourceModel();
        schedule.setResourceModel(resourceModel);

        for (Iterator<Resource> i = ResourcesQueries.allResources(resourceModel); i.hasNext();) {
            Sequence<T> sequence = SCHEDULE_FACTORY.createSequence();
            sequence.setResource(i.next());
            sequence.setName(sequence.getResource().getName());
            schedule.getSequences().add(sequence);
        }

        return schedule;
    }

    /**
     * Schedules all tasks.
     *
     * @throws SchedulerException When scheduling fails
     */
    protected abstract void performScheduling() throws SchedulerException;

    /**
     * Add additional information to the schedule.
     */
    protected void postProcessSchedule() {
        CompleteScheduledDependencies.perform(getSchedule());
    }

    /**
     * Utility method.<br>
     * <b>CONSTRAINT: </b> Can be called after {@link #initializeSchedule()}.
     *
     * @param resources search keys
     * @return the corresponding Sequences
     * @see #initializeSchedule()
     */
    protected Iterable<Sequence<T>> getSequences(final Collection<Resource> resources) {
        return from(getSchedule().getSequences()).select(sequence -> resources.contains(sequence.getResource()));
    }

    /**
     * Creates a ScheduledTask, adds it to the schedule and links it to <tt>task</tt>
     *
     * @param task
     * @return
     */
    protected final ScheduledTask<T> createScheduledTask(T task, boolean createIncomingDependencies) {
        ScheduledTask<T> target = SCHEDULE_FACTORY.createScheduledTask();
        target.setName(task.getName());
        target.setTask(task);
        target.setGraph(getSchedule());
        itsTaskToScheduledTaskCache.put(task, target);

        if (createIncomingDependencies) {
            for (Edge edge: task.getIncomingEdges()) {
                // We can safely cast this source to a T as the TaskDependencyGraph enforces this
                @SuppressWarnings("unchecked")
                T sourceT = (T)edge.getSourceNode();
                ScheduledTask<T> source = resolveScheduledTask(sourceT);

                ScheduledDependency dependency = SCHEDULE_FACTORY.createScheduledDependency();
                dependency.setSourceNode(source);
                dependency.setTargetNode(target);
                // We can safely cast this edge to a dependency as the TaskDependencyGraph enforces this
                dependency.setDependency((Dependency)edge);
                dependency.setGraph(getSchedule());
            }
        }

        return target;
    }

    /**
     * Returns the ScheduledTask originally created by {@link #createScheduledTask(Task)}.<br>
     */
    protected final ScheduledTask<T> resolveScheduledTask(T task) {
        ScheduledTask<T> scheduledTask = itsTaskToScheduledTaskCache.get(task);
        if (null == scheduledTask) {
            throw new IllegalArgumentException("Task not found in schedule: " + task.getName());
        }
        return scheduledTask;
    }
}
