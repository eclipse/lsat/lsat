/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.lsat.motioncalculator.MotionCalculator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileParameter;
import org.eclipse.lsat.motioncalculator.MotionProfileProvider;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.json.JsonRequest;
import org.eclipse.lsat.motioncalculator.json.JsonResponse;
import org.eclipse.lsat.motioncalculator.json.JsonSerializer;
import org.eclipse.lsat.motioncalculator.json.JsonServer;

public class JsonMotionCalculatorServer implements JsonServer {
    private static enum Print {
        DontPrint, ToSysOut, ToFile
    }

    private static final Print PRINT = Print.DontPrint;

    private final MotionCalculator motionCalculator;

    private final MotionProfile profile = new MotionProfile("test", "test", null,
            Stream.of("V", "A", "J").map(s -> new MotionProfileParameter(s, s, true)).collect(Collectors.toList()));

    public JsonMotionCalculatorServer(MotionCalculator calculator) {
        this.motionCalculator = calculator;
    }

    @Override
    public String request(String requestText) {
        print(requestText);
        JsonRequest request = JsonSerializer.createRequest(requestText);
        JsonResponse response = request(request);
        String responseText = JsonSerializer.toJson(response);
        print(responseText);
        return responseText;
    }

    public JsonResponse request(JsonRequest request) {
        JsonResponse response = new JsonResponse();
        response.setRequestType(request.getRequestType());

        try {
            switch (request.getRequestType()) {
                case Validate:
                    motionCalculator.validate(request.getSegments());
                    break;
                case CalculateTimes:
                    response.setTimes(motionCalculator.calculateTimes(request.getSegments()));
                    break;
                case PositionInfo:
                    response.setPositions(motionCalculator.getPositionInfo(request.getSegments()));
                    break;
                case SupportedProfiles:
                    if (motionCalculator instanceof MotionProfileProvider) {
                        MotionProfileProvider motionProfileProvider = (MotionProfileProvider)motionCalculator;
                        response.setMotionProfiles(motionProfileProvider.getSupportedProfiles());
                    } else {
                        response.setMotionProfiles(Collections.singleton(profile));
                    }
                    break;
            }
        } catch (MotionValidationException e) {
            response.setErrorMessage(e.getMessage());
            response.setErrorSegments(
                    e.getSegments().stream().map(s -> s.getId()).collect(Collectors.toCollection(LinkedHashSet::new)));
        } catch (MotionException e) {
            response.setErrorMessage(e.getMessage());
        }

        return response;
    }

    private void print(String jsonText) {
        if (PRINT.equals(Print.ToSysOut)) {
            System.out.println(jsonText);
        } else if (PRINT.equals(Print.ToFile)) {
            try {
                try (FileWriter writer = new FileWriter("c:/temp/is.json", true)) {
                    writer.write(jsonText);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
