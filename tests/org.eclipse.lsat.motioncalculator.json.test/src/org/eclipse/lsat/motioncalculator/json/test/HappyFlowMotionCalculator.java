/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.lsat.motioncalculator.MotionCalculator;
import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileProvider;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.PositionInfo;

public final class HappyFlowMotionCalculator implements MotionCalculator, MotionProfileProvider {
    @Override
    public Set<MotionProfile> getSupportedProfiles() throws MotionException {
        return Collections.singleton(MotionDataFactory.createMotionProfile());
    }

    @Override
    public void validate(List<MotionSegment> segments) throws MotionValidationException {
    }

    @Override
    public List<Double> calculateTimes(List<MotionSegment> segments) throws MotionException {
        return Arrays.asList(1.0, 2.0);
    }

    @Override
    public Collection<PositionInfo> getPositionInfo(List<MotionSegment> segments) throws MotionException {
        return Arrays.asList("a", "b", "c").stream().map(id -> {
            PositionInfo positionInfo = new PositionInfo(id);
            Arrays.asList(1.0, 2.0, 3.0).stream().forEach(d -> positionInfo.addTimePosition(d, d));
            return positionInfo;
        }).collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
