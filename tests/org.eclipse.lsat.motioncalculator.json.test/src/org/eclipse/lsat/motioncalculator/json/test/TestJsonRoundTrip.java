/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.logging.Logger;

import org.eclipse.lsat.motioncalculator.json.JsonRequest;
import org.eclipse.lsat.motioncalculator.json.JsonRequestType;
import org.eclipse.lsat.motioncalculator.json.JsonResponse;
import org.eclipse.lsat.motioncalculator.json.JsonSerializer;
import org.junit.jupiter.api.Test;

public class TestJsonRoundTrip {
    private static final Logger LOG = Logger.getLogger(TestJsonRoundTrip.class.getName());

    @Test
    public void testRoundTripRequest() {
        JsonRequest request = MotionDataFactory.createRequestWithSegments(JsonRequestType.Validate, "s1", "s2");
        String jsonString = JsonSerializer.toJson(request);
        LOG.info(jsonString);
        JsonRequest request1 = JsonSerializer.createRequest(jsonString);
        String jsonRoundTripString = JsonSerializer.toJson(request1);
        LOG.info(jsonRoundTripString);
        assertEquals(jsonString, jsonRoundTripString);
    }

    @Test
    public void testRoundTripResponse() {
        JsonResponse response = MotionDataFactory.createResponse(JsonRequestType.PositionInfo, "s1", "s2");
        String jsonString = JsonSerializer.toJson(response);
        LOG.info(jsonString);
        JsonResponse response1 = JsonSerializer.createResponse(jsonString);
        String jsonRoundTripString = JsonSerializer.toJson(response1);
        LOG.info(jsonRoundTripString);
        assertEquals(jsonString, jsonRoundTripString);
    }
}
