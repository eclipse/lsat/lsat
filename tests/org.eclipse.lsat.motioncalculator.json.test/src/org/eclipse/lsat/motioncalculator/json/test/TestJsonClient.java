/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.json.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.eclipse.lsat.motioncalculator.MotionException;
import org.eclipse.lsat.motioncalculator.MotionProfile;
import org.eclipse.lsat.motioncalculator.MotionProfileParameter;
import org.eclipse.lsat.motioncalculator.MotionSegment;
import org.eclipse.lsat.motioncalculator.MotionValidationException;
import org.eclipse.lsat.motioncalculator.json.JsonMotionCalculatorClient;
import org.junit.jupiter.api.Test;

public class TestJsonClient {
    private static final List<MotionSegment> SEGMENTS = Arrays.asList(
            MotionDataFactory.createMotionSegment("one", "x", "y"),
            MotionDataFactory.createMotionSegment("two", "x", "y"));

    JsonMotionCalculatorClient happyClient = new JsonMotionCalculatorClient(
            new JsonMotionCalculatorServer(new HappyFlowMotionCalculator()));

    JsonMotionCalculatorClient errorClient = new JsonMotionCalculatorClient(
            new JsonMotionCalculatorServer(new ErrorMotionCalculator()));

    @Test
    public void testValidate() throws MotionValidationException {
        happyClient.validate(SEGMENTS);
    }

    @Test
    public void testCalculateTimes() throws MotionException {
        happyClient.calculateTimes(SEGMENTS);
    }

    @Test
    public void testPositionInfo() throws MotionException {
        happyClient.getPositionInfo(SEGMENTS);
    }

    @Test
    public void testSupportedProfiles() throws MotionException {
        MotionProfile compare = MotionDataFactory.createMotionProfile();
        Set<MotionProfile> supportedProfiles = happyClient.getSupportedProfiles();
        assertEquals(1, supportedProfiles.size());
        MotionProfile profile = supportedProfiles.iterator().next();
        assertEquals(compare.getKey(), profile.getKey());
        assertEquals(compare.getName(), profile.getName());
        assertEquals(compare.getUrl(), profile.getUrl());
        for (MotionProfileParameter comparePar: compare.getParameters()) {
            MotionProfileParameter par = profile.getParameter(comparePar.getKey());
            assertNotNull(par);
            assertEquals(comparePar.getKey(), par.getKey());
            assertEquals(comparePar.getName(), par.getName());
            assertEquals(comparePar.isRequired(), par.isRequired());
        }
    }

    @Test
    public void testErrorValidate() {
        try {
            errorClient.validate(SEGMENTS);
            fail("Error expected");
        } catch (MotionValidationException e) {
            assertEquals(e.getSegments(), SEGMENTS);
        }
    }

    @Test
    public void testErrorCalculateTimes() {
        try {
            errorClient.calculateTimes(SEGMENTS);
            fail("Error expected");
        } catch (MotionValidationException e) {
            assertEquals(e.getSegments(), SEGMENTS);
        } catch (MotionException e) {
            assertEquals("error", e.getMessage());
        }
    }

    @Test
    public void testErrorPositionInfo() {
        try {
            errorClient.getPositionInfo(SEGMENTS);
            fail("Error expected");
        } catch (MotionValidationException e) {
            assertEquals(e.getSegments(), SEGMENTS);
        } catch (MotionException e) {
            assertEquals("error", e.getMessage());
        }
    }

    @Test
    public void testErrorSupportedProfiles() {
        try {
            errorClient.getSupportedProfiles();
            fail("Error expected");
        } catch (MotionValidationException e) {
            assertEquals(e.getSegments(), SEGMENTS);
        } catch (MotionException e) {
            assertEquals("error", e.getMessage());
        }
    }
}
