/*
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.lsat.motioncalculator.jna.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

import com.sun.jna.Library;
import com.sun.jna.Memory;
import com.sun.jna.Native;

/**
 * Simple test case that check whether JNA environment is correctly available.
 *
 */
public class TestJNA {
    public static interface DLL extends Library {
        DLL INSTANCE = (DLL)Native.loadLibrary(System.getProperty("os.name").startsWith("Windows") ? "msvcrt" : "c",
                DLL.class, new HashMap<String, Object>());

        long atol(String ascii);

        void strncpy(Memory dest, byte[] src, int len);
    }

    @Test
    public void testAtol() {
        assertEquals(100L, DLL.INSTANCE.atol("100"));
    }

    @Test
    public void testStrnCpy() {
        Memory b = new Memory(1000);
        String testString = "Hello \u02DF world";
        byte[] bytes = testString.getBytes(StandardCharsets.UTF_8);
        byte[] plusZero = Arrays.copyOf(bytes, bytes.length + 1);
        DLL.INSTANCE.strncpy(b, plusZero, 1000);
        assertEquals(testString, b.getString(0, "UTF-8"));
    }
}
