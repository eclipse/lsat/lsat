/**
 * Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
#ifndef JSON_SERVER_H
#define JSON_SERVER_H

 /*
	Interface call to a Motion Calculator Json server.
	See the LSAT design documentation for the Json data to be exchanged.

	request contains the UTF-8 encoded  Json request data
	response is a buffer of size response_size that can be used to write
	the Json response to. response must be UTF-8 encoded.

	the function should return 0 except when the response does not fit
	into the response buffer then it should return a non 0 value.

	errormessages not related to `buffer overflows` should be returned into
	the Json response.
 */
extern "C" __declspec(dllexport) int request(char* response, const char* request, size_t response_size);

#endif // JSON_SERVER_H